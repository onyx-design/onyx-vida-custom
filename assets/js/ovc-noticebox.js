// OVC Notice Box Class
// by Ray Sarno of ONYX Design
// Version 0.1.0

if ( typeof $ === 'undefined' ) {
	var $ = jQuery.noConflict();
}

var ovcNoticeBox = false;

$(document).ready(function() {
	if( $('#ovc-header-notice-box').length ) {
		ovcNoticeBox = new OVC_noticeBox( $('#ovc-header-notice-box') );	
	}
	
});

function OVC_noticeBox( outerWrap ) {

	this.status = '';

	// Initialize the Notice Box
	outerWrap.empty().append('<div class="notifications-list"></div><i class="fa fa-chevron-down expand-toggle"></i>');

	outerWrap.children( 'i.expand-toggle' ).click( function() {
		$(this).toggleClass('fa-times').toggleClass('fa-chevron-down').parents('.ovc-notice-box').toggleClass('expanded').find('.notification-wrap').removeClass('expanded');
	});


	this.noticeList = function() { 
		return outerWrap.children('.notifications-list');
	};


	this.notice = function( msg, error, submsgs ) {

		// Initialize parameters and variables
		if( typeof( error ) === 'undefined' ) {error = false;}
		var errorClass = error ? ' notice-error' : '';

		// Maybe build submsgs html
		var submsgsHTML = '';
		var submsgErrorClass = '';
		var submsgCount = 0;

		if( submsgs instanceof Array ) {
			submsgCount = submsgs.length;
			for( var i = 0; i < submsgCount; i++ ) {
				submsgErrorClass = ( 'error' == submsgs[ i ].type ? ' notice-error' : '' );
				submsgsHTML += '<div class="notification-row ' + submsgErrorClass + '">' + submsgs[ i ].msg + '</div>';
			}
		}

		// Add the notification
		this.noticeList().prepend('<div class="notification-wrap' + ( submsgCount ? ' submsgs' : '' ) + '" onclick="OVC_noticeBox.expandNotice(this);"><div class="notification-row' + errorClass + '">' + msg + '</div>' + submsgsHTML + '</div>');

	};


	this.addNotices = function( notices ) {

		if( typeof( notices ) === 'undefined' ) {notices = [];}

		for( var i = 0; i < notices.length; i++ ) {

			this.notice( notices[ i ].main_msg, !notices[ i ].success, notices[ i ].messages );
		}
	};

	this.setStatus = function( status ) {
		if( $('.ovcdt-status-row').length ) {
			$('.ovcdt-status').addClass('ox-hidden');
			$('.ovcdt-status-'+status ).removeClass('ox-hidden');
			this.status = status;
		}
	};
}

OVC_noticeBox.expandNotice = function( target ) {

	if( $(target).hasClass('submsgs') ) {

		// Expand the OVC Notice Box outerWrap
		$(target).parents('.ovc-notice-box').eq(0).addClass('expanded').children('i').removeClass('fa-chevron-down').addClass('fa-times');

		$(target).toggleClass('expanded');
	}
};

