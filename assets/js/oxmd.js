// oxmd by Ray Sarno of ONYX Design
// Version 0.1.0

if ( typeof $ === 'undefined' ) {
	var $ = jQuery.noConflict();
}

$(document).ready(function() {
	oxmd = new ONYX_Modal();
});

function ONYX_Modal() {
	var self = this;

	this.modalDefaults = {
		type		: "confirm", 	
		cancellable	: true,			
		callback	: "",
		modalClass 	: "",
		responses 	: [
			{label:"OK",value:1,primary:true},
			{label:"Cancel",value:0}
		],
		passData	: {},
		saveFields  : false,
		title		: "Please Confirm",
		elements 	: [
			"This is a test message.  Isn't it cool?", // String values will automatically be added as a basic paragraph element with this string as its content
			{	
				wrapElem 	: {}, // An option HTML Element Object to be used as the wrap element.  If omitted, a standard wrapper will be used
				innerElems 	: [] // An array of HTML Element Objects that to be put inside the wrap element
			}
			/*,
			{
				template 	: 'ovc-shared-data',
				wrapID		: 'oxmd_ovc_shared_data',
				requestData : {}
			}
			*/
		]	
	};

	this.HTMLElementDefaults = {
		type 		: "div",
		attrs		: {},
		content 	: ""
	};

	this.status = {
		callback 	: "",
		passData	: "",
		saveFields	: false
	};

	// Build OXMD Elements
	if( !$('#oxmd-wrap-outer').length ) {
		$('body').append('<div id="oxmd-wrap-outer"><div id="oxmd-modal"></div></div>');
		
	}

	this.wrap = $('#oxmd-wrap-outer');
	this.modalElem = $('#oxmd-modal');


	this.resizeTime = false;
	this.resizeTimeout = false;

	this.templateArgs = {};

	this.data = {};

	$(window).resize(function() {
		self.resizeTime = new Date();
		if ( self.resizeTimeout === false ) {
			self.resizeTimeout = true;
			setTimeout( self.resizeEnd, 200 );
		}
		if( self.active() ) {
			self.modal.align();
		}
	});

	this.resizeEnd = function() {
	    if ( new Date() - self.resizeTime < 200 ) {
	        setTimeout( self.resizeEnd, 200 );
	    } else {
	        self.resizeTimeout = false;
	        self.modal.align();
	    }               
	};

	this.active = function() {return $('body').hasClass('oxmd-locked');};

	// OXMD Modal Generation & Manipulation

	this.modal = function( modalData, showModal ) {
		if( typeof( modalData ) === 'undefined' ) { modalData = {};}
		if( typeof showModal === 'undefined' ) {showModal = true;}
		var data = $.extend( {}, this.modalDefaults, modalData );

		self.modal.reset(); // Reset Modal

		// Build and append modal's title element
		this.modal.elem().attr( 'class', data.modalClass );
		this.modal.elem().append( self.buildModalElement( self.buildHTMLElement({type:"h3", attrs: { class: "oxmd-modal-h"}, content: data.title }) ) );

		// Build modal's contents
		for( var e = 0; e < data.elements.length; e++ ) {
			// Handle simple modal element build (just add a text paragraph)
			if( typeof data.elements[ e ] === 'string' ) { 
				this.modal.elem().append( self.buildModalElement( self.buildHTMLElement({type:"p", attrs: {}, content: data.elements[ e ] }) ) );
			}
			// AJAX-Loaded template Modal
			else if( typeof data.elements[ e ].template !== 'undefined' ) {
				var templateArgs = data.elements[ e ];
				this.modal.elem().append( self.buildModalElement('', {attrs: { id: templateArgs.wrapID, class: 'oxmd-template-loading' } } ) );
				self.loadTemplate( templateArgs );
			}
			// Handle complex non-AJAX modal element build
			else { 
				if( typeof data.elements[ e ].wrapElem === 'undefined' ) { data.elements[ e ].wrapElem = null;}
				if( typeof data.elements[ e ].innerElems === 'undefined' ) { data.elements[ e ].innerElems = {};}

				var devElems = data.elements[ e ];

				var elementContents = '';
				for( var ie = 0; ie < data.elements[ e ].innerElems.length; ie++ ) {
					elementContents += self.buildHTMLElement( data.elements[ e ].innerElems[ ie ] );
				}

				this.modal.elem().append( self.buildModalElement( elementContents, data.elements[ e ].wrapElem ) );
			}
			
		}

		// Build Modal Answer Buttons
		var buttons = '';
		if( 'notice' == data.type ) {
			buttons += self.buildHTMLElement({type: "li", attrs: { class: "oxmd-response-button"}, content: "OK"});
		}
		else if( 'confirm' == data.type ) {
			buttons += self.buildHTMLElement({type: "li", attrs: { class: "oxmd-response-button"}, content: "No"});
			buttons += self.buildHTMLElement({type: "li", attrs: { class: "oxmd-response-button primary-response",  "data-response-val":1}, content: "Yes"});
		}
		else if( 'custom' == data.type ) {
			for( var b = 0; b < data.responses.length; b++ ) {
				var extraClasses = ( ( typeof data.responses[ b ].primary !== 'undefined' && data.responses[ b ].primary ) ? " primary-response" : "" );

				var buttonAttrs = {
					class 				: "oxmd-response-button" + extraClasses,
					'data-response-val'	: data.responses[ b ].value
				};

				if( true === data.responses[ b ].retainModal ) {
					buttonAttrs[ 'data-retain-modal' ] = true;
				}

				buttons += self.buildHTMLElement({type:"li", attrs:buttonAttrs, content: data.responses[ b ].label });
				//buttons += self.buildHTMLElement({type: "li", attrs: { class: "oxmd-response-button"+extraClasses, "data-response-val":data.responses[ b ].value}, content: data.responses[ b ].label });
			}
		}

		this.modal.elem().append( self.buildModalElement( buttons, {type:"ul", attrs: {class: 'oxmd-response-buttons'}}) );		
		
		// Prepare current modal's data and set event handlers
		this.status.callback 	= data.callback;
		this.status.passData 	= data.passData;
		this.status.saveFields 	= data.saveFields;
		this.setEventHandlers();
	
		// Maybe Show Modal
		if( showModal ) { this.modal.show();}
	};

	this.modal.elem 	= function() {return $('#oxmd-modal');};
	this.modal.wrap 	= function() {return $('#oxmd-wrap-outer');};
	
	this.modal.hide 	= function() {$('body').removeClass('oxmd-locked');};
	this.modal.show 	= function() {self.modal.align();$('body').addClass('oxmd-locked');};

	this.modal.reset 	= function() {self.modal.elem().empty().removeAttr('class').removeAttr('style');};

	this.modal.align 	= function() {
		if( self.modalElem.height() > self.modal.wrap().height() ) {
			self.modalElem.addClass('top-align');
		}
		else {
			self.modalElem.removeClass('top-align');
		}
	};


	this.buildModalElement = function( contents, wrapData ) {
		if( typeof contents === 'undefined' ) {contents = '';}
		if( typeof wrapData === 'undefined' ) {wrapData = {};}
		var wrapElem = $.extend( {}, this.HTMLElementDefaults, wrapData );

		// Make sure that the oxmd-modal-elem class is included
		if( typeof wrapElem.attrs.class === 'undefined' ) {wrapElem.attrs.class = 'oxmd-modal-elem';}
		else if( -1 == wrapElem.attrs.class.indexOf("oxmd-modal-elem") ) {wrapElem.attrs.class += ' oxmd-modal-elem';}

		wrapElem.content = contents;

		return self.buildHTMLElement( wrapElem );
	};

	this.buildHTMLElement = function( elemData ) {
		if( typeof( elemData ) === 'undefined' ) { elemData = {};}
		var elem = $.extend( {}, this.HTMLElementDefaults, elemData );

		html = '<' + elem.type; 

		// Add any supplied attributes and their values to the tag
		for( var attr in elem.attrs ) {
			html += " " + attr + '="' + elem.attrs[ attr ] + '"';
		}

		return html+'>'+elem.content+'</'+elem.type+'>'; // Close opening tag, Append Content, and add closing tag
	};

	this.setEventHandlers = function() {
		$('#oxmd-modal .oxmd-response-button').click( function(e) {
			if( typeof self.status.callback === 'function' ) {
				var response = {
					value 		: $(e.target).data('response-val'),
					passData	: self.status.passData
				};

				// Maybe save OXMD field data
				if( self.status.saveFields ) {
					response.fieldData = {};
					$('.oxmd-field').each(function() {


						if( $(this).is('select, input, textarea') ) {
							if( $(this).is(':radio, :checkbox') && !$(this).is(':checked') ) {
								return;
							}
							response.fieldData[ $(this).attr('name') ] = $(this).val();
						}
					});
				}

				self.status.callback( response );
			} 

			if( !$(e.target).data('retain-modal' ) ) {
				self.status.callback = "";
				self.status.passData = "";
				self.modal.hide();
			}
		});
	};

	this.loadTemplate = function( args ) {
		args = 'refresh' ==  args ? self.templateArgs : args;
		self.templateArgs = args;

		$( '#' + args.wrapID ).html( '<p>Loading Interface...</p>' );

		var ajax_data = {
			'ovc_ajax'	: true,
			'action' 	: 'ovc_ajax',
			'command'	: 'oxmd',
			'oxmd'		: args
		};

		var ajax_args = {
			url			: location.origin + ajaxurl,
			type		: "POST",
			data 		: ajax_data,
			cache		: false,
			dataType	: "json",
			complete	: function( response, status ) {
				if ( 'success' == status ) {
					var rjson = response.responseJSON;

					// Put the HTML response into the modal
					$('#' + rjson.wrapID ).empty().html( rjson.template_html );
					self.modal.align();
					setTimeout( function() {self.modal.align();}, 200);

					// Maybe record response data
					if( typeof rjson.extra_data !== 'undefined' ) {
						self.data = onyx_clone( rjson.extra_data );
						
					}

					// Trigger modal event
					self.modal.elem().trigger( 'oxmd-loaded', [ self.data ] );
				}
				else if ( 'timeout' == status ) {
					console.log('OXMD Load Template failed. AJAX Timeout :(' );
				}
				else {
					console.log('OXMD Load Template failed. Unknown Error');
					console.log( response );
				}
			}
		};

		$.ajax( ajax_args );
	};

}
