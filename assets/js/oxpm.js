// oxmd by Ray Sarno of ONYX Design
// Version 0.0.1

if ( typeof $ === 'undefined' ) {
	var $ = jQuery.noConflict();
}

$(document).ready(function() {
	oxpm = new ONYX_PageMaster();
});

function ONYX_PageMaster() {
	var self = this;
	this.qArgs = false;
	this.origPage = location.href;

	this.getURLQueryArgs = function( rawValues ) {
		if( typeof rawValues === 'undefined' ) {rawValues = false;}
		var qArgs = {};
		
		if( location.search.length ) {
			var rawArgs = location.search.substr( 1 );
			var key, val;
			rawArgs = rawArgs.split( '&' );

			for ( var i = 0; i < rawArgs.length; i++ ) {
				rawArgs[ i ] = rawArgs[ i ].split( '=' );

				key = rawArgs[ i ][ 0 ];
				val = rawArgs[ i ][ 1 ];

				if ( !rawValues ) {
					val = maybeParseJSON( decodeURIComponent( val ) );
				}

				qArgs[ key ] = val;
			}
		}

		this.qArgs = qArgs;
		return qArgs;
	};
	this.getURLQueryArgs();

	function updateURLQuery( qArgs ) {
		var qStr = '';

		for ( var key in qArgs ) {
			if( qArgs.hasOwnProperty( key ) ) {
				qStr += "&" + key + "=" + URLEncode( qArgs[ key ] );
			}
		}

		qStr = qStr.length > 0 ? '?' + qStr.substr( 1 ) : '';

		history.pushState( {orig:oxpm.origPage}, document.title, location.pathname + qStr + location.hash );

		self.getURLQueryArgs();
	}

	this.getQArg = function( key ) {
		return this.keyExists( key ) ? this.qArgs[ key ] : null ;
	};

	this.setQArg = function( key, val, updateURL ) {
		if( typeof updateURL === 'undefined' ) {updateURL = true;}
		
		this.qArgs[ key ] = onyx_clone( val );

		if( updateURL ) {
			updateURLQuery( this.qArgs );
		}	
	};

	this.unsetQArg = function( key, updateURL ) {
		if( typeof updateURL === 'undefined' ) {updateURL = true;}

		delete this.qArgs[ key ];

		if( updateURL ) {
			updateURLQuery( this.qArgs );
		}	
	};

	this.keyExists = function( key ) {
		return typeof this.qArgs[ key ] === 'undefined' ? false : true ;
		/*
		if ( location.search.indexOf( '&' + key + '=' ) !== -1 || location.search.indexOf( '?' + key + '=' ) !== -1 ) {
			return true;
		}
		else {return false;}
		*/
	};



	function maybeParseJSON( str ) {
		try {str = JSON.parse( str );}
		catch (e) {}
		return str;
	}

	function URLEncode( val ) {
		if( typeof val === 'object' ) {
			val = JSON.stringify( val );
		}
		return encodeURIComponent( val );
	}

	//this.
}