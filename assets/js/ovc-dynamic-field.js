if ( typeof $ === 'undefined' ) {
	var $ = jQuery.noConflict();
}

window.ovc = window.ovc || {};

ovc.dynamic_field = (function() {

	var dynamicFields = [],
		selectors;

	function OvcDynamicFieldManager() {
		
		selectors = {
			wrap 		: '.ovc-dynamic-field-wrap',
			rowDock		: '.ovc-dynamic-field-row-dock',
			row 		: '.ovc-dynamic-field-row',
			addRow		: '.ovc-dynamic-field-action--add',
			deleteRow	: '.ovc-dynamic-field-action--delete',
			newRow		: '.ovc-dynamic-field-row--new',
			field 		: '.ovc-dynamic-field',
			fieldLabel	: '.ovc-dynamic-field-label',
			fieldValue	: '.ovc-dynamic-field-value',
			templateRow : '#ovc-dynamic-field-row-template'
		};

		function OvcDynamicField( dynamicField ) {

			var self = this,
				$container = ( this.$container = $(dynamicField) );

			function deleteRow() {

				var iconBtn = $(this),
					$row = iconBtn.parent();

				iconBtn.find( '.fa' )
					.toggleClass( 'fa-times' )
					.toggleClass( 'fa-times-circle' );

				$row.toggleClass( 'ovc-dynamic-field-row-deleted' );

				$row.find( selectors.field ).each(function(input){

					var input = $(this);

					input.prop( 'disabled', function( i, v ) { return !v; } );
				});
			}

			function addRow() {

				var $newRow = $( selectors.templateRow ).clone(),
					rowCount = $( selectors.row ).length;

				$newRow.removeAttr('id')
					.addClass( 'ovc-dynamic-field-row' )
					.appendTo( $( selectors.rowDock ) );

				$newRow.find( selectors.fieldLabel )
					.attr( 'name', 'additional_sizing_guides[' + rowCount + '][note]' );

				$newRow.find( selectors.fieldValue )
					.attr( 'name', 'additional_sizing_guides[' + rowCount + '][value]')
			}
		
			$container.on( 'click', selectors.deleteRow, deleteRow );
			$container.on( 'click', selectors.addRow, addRow );
		}

		$( selectors.wrap ).each(function() {
			dynamicFields.push( new OvcDynamicField( $(this) ) );
		});
	}

	return OvcDynamicFieldManager;
})();