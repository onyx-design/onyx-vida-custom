if ( typeof $ === 'undefined' ) {
	var $ = jQuery.noConflict();
}


function ovcImageManager() {
	var self = this;

	// Initalize Image Manager OVCDT
	this.ovcdt = new ovcdtAdminPageHandler( $('#ovcdt-oxmd-img-mgr'), false );

	/*
	this.ovcdt.table.getFilters();
	this.ovcdt.setStatus( 'ready' );
	this.ovcdt.loadData();
	*/

	// Initialize other variables
	this.wpMedia = null;
	this.imageSetData = {};

	this.responseImgData = {};

	this.localImgData = {};

	this.freshImgSetID = null;

	// Variables declared for scope reasons so they can be used in .each functions // dev:improve
	var color_img_set_ids = [];
	var img_set_colors = [];


	

	

	this.setEventHandlers = function() {

		// DRAGGABLE / DROPPABLE OBJECTS
		var draggableOptions = {
			helper 			: 'clone',
			appendTo 		: '#ovc-screen__content',
			cursor			: '-webkit-grabbing',
			revert 			: "invalid",
			revertDuration 	: 400
		};

		// Draggable SKUs
		$('#ovc-screen-wrap .ovc-draggable-sku').draggable( draggableOptions );

		$('#ovc-screen-wrap .img-set-skus-droppable').droppable({
			accept 			: ".ovc-draggable-sku",
			activeClass 	: 'droppable-active',
			hoverClass 		: 'droppable-hover',
			drop 			: function( event, ui ) {
				//console.log( event );
				//$( this ).append( ui.helper );
				var appendTarget = $(this).hasClass('unset-skus-td') ? $(this).children('.unset-skus-wrap').eq(0) : $(this); 
				ui.draggable.appendTo( appendTarget );
				self.setInterface();
			}
		});

		// Draggable IMGs
		$('#ovc-screen-wrap .ovc-img-wrap:not([data-image-id="0"])').draggable( draggableOptions );

		$('#ovc-screen-wrap .ovc-img-wrap, .ovcim-gallery-select').droppable({
			accept 			: ".ovc-img-wrap",
			activeClass 	: 'droppable-active',
			hoverClass 		: 'droppable-hover',
			drop 			: function( event, ui ) {

				if( $(this).hasClass( 'ovcim-gallery-select' ) ) {

					ui.draggable.clone().insertBefore( $(this) );
				}
				else {
					var imageID = ui.draggable.data( 'image-id' );
					
					$(this).attr('data-image-id', ui.draggable.attr('data-image-id') );
					$(this).data('image-id', imageID ); 
					$(this).css('background-image', ui.draggable.css('background-image') );
				}

				self.setInterface();
			}
		});

		// Draggable Colors
		$('#ovc-screen-wrap .ovc-draggable-color:not(.color-legend)').draggable( draggableOptions );

		$('#ovc-screen-wrap .img-set-colors-droppable').droppable({
			accept 			: '.ovc-draggable-color',
			activeClass 	: 'droppable-active',
			hoverClass 		: 'droppable-hover',
			drop 			: function( event, ui ) {

				var imgSetID = $(this).data('img-set-id');
				var color = ui.draggable.data('color');
				var skusTD = $('#ovc-screen-wrap tr.img-set-skus-row td[data-img-set-id="' + imgSetID + '"]');

				$('#ovc-screen-wrap .ovc-draggable-sku[data-color="' + color + '"]').appendTo( $( skusTD ) );

				self.setInterface();
			}

		}); 
	};
	

	this.setInterface = function() {

		// Check if fresh-img-set column has been used
		if( $('tr.img-set-skus-row td.fresh-img-set .ovc-draggable-sku').length ) {

			$('td.fresh-img-set').each(function() {

				// Update the new fresh img-set-id
				imgSetID = $(this).data('img-set-id');
				self.freshImgSetID = "new" + ( parseInt( imgSetID.replace( 'new', '' ) ) + 1 );
				
				$(this).removeClass('fresh-img-set'); // No longer 'fresh', it's had SKUs added to it!

				// Add ovc_img_select elements to this image set's cells
				$('.ovc-img-set-table tr[data-img-set-field] td[data-img-set-id="' + imgSetID + '"]').each(function() {
					if( 'img_gallery_ids' == $(this).parent().data( 'img-set-field' ) ) {
						$(this).html('<div class="ovcim-gallery-select" onclick="ovcim.imgSelect(this);"></div>');
					}
					else if( 'skus' != $(this).parent().data('img-set-field') ){
						$(this).html( self.imgSelectorFromAttachment() );
					}
				});
			});
		}

		// Make sure there is always a fresh-img-set column ready
		if( !$('#ovc-screen-wrap tr.img-set-skus-row td.fresh-img-set').length ) {
			
			$('#ovc-screen-wrap .ovc-img-set-table tr').each(function() {

				var cellClone = $(this).children('td').last().clone().empty().attr('data-img-set-id', self.freshImgSetID );

				//cellClone.empty();

				// Special handling for certain rows
				if( $(this).hasClass( 'img-set-id-row' ) ) {
					cellClone.html( "New Image Set " + self.freshImgSetID.replace('new', ''));
				}
				else if( $(this).hasClass( 'img-set-skus-row' ) ) {
					cellClone.addClass( 'fresh-img-set' );
				}

				$(this).append( cellClone );
			});

			
		}

		// ADD / REMOVE COLORS IN IMAGE SETS (Stats and Classes are updated after this)
		$('#ovc-screen-wrap tr.img-set-colors-row .img-set-colors-droppable').empty();

		$('#ovc-screen-wrap tr.img-set-skus-row td[data-img-set-id]').each(function(index,skusTD) {
			img_set_colors = [];

			$(skusTD).children('.ovc-draggable-sku').each(function(index,skuElem) {
				if( -1 == img_set_colors.indexOf( $(skuElem).data('color') ) ) {
					img_set_colors.push( $(skuElem).data('color') );
				}
			});

			var imgSetColorsTD = $('#ovc-screen-wrap tr.img-set-colors-row td[data-img-set-id="' + $(skusTD).data('img-set-id') + '"]').eq(0);

			for( var i = 0; i < img_set_colors.length; i++ ) {
				$('#ovc-screen-wrap .img-set-all-colors-td > .ovc-draggable-color[data-color="' + img_set_colors[ i ] + '"]').eq(0).clone().appendTo( $( imgSetColorsTD ) );
			}

			img_set_colors = [];

		});

		// UPDATE SKU / COLOR COUNTS

		// Update Unset SKUs count
		$('#ovc-screen-wrap .skus-wo-imgs-count').html( $('#ovc-screen-wrap .unset-skus-wrap').children('.ovc-draggable-sku').length );

		// Update img set counts on ovc-color blocks
		$('#ovc-screen-wrap .img-set-all-colors-td > .ovc-draggable-color').each(function(index,colorElem) {
			var color = $(colorElem).data('color');


			// Determine how many img sets contain this color
			color_img_set_ids = [];
			
			$('#ovc-screen-wrap .ovc-img-set-table .img-set-skus-row .ovc-draggable-sku[data-color="' + color + '"]').each(function(index,skuElem) {
				

				var img_set_id = $(skuElem).parent().data('img-set-id');

				if( -1 === color_img_set_ids.indexOf( img_set_id ) ) {
					color_img_set_ids.push( img_set_id );
				}
				
			});

			// SET STATS AND RELATED CLASSES

			// Remove all stat-related classes
			$('#ovc-screen-wrap .ovc-draggable-color[data-color="' + color + '"]').removeClass('img-set-count-1 img-set-count-multiple skus-assigned-none skus-assigned-all skus-assigned-some');

			// Set # img sets contain this color stat & class
			$('#ovc-screen-wrap .ovc-draggable-color[data-color="' + color + '"] .color-sku-stats .img-set-count').html( "(" + color_img_set_ids.length + ")" );

			if( 1 == color_img_set_ids.length ) {
				$('#ovc-screen-wrap .ovc-draggable-color[data-color="' + color + '"]').addClass( 'img-set-count-1' );	
			}
			else if( color_img_set_ids.length > 1 ) {
				$('#ovc-screen-wrap .ovc-draggable-color[data-color="' + color + '"]').addClass('img-set-count-multiple');
			}

			// Set assigned SKUs count and class
			var total_skus = parseInt( $(colorElem).data('total-skus') );
			var skus_assigned = total_skus - $('#ovc-screen-wrap .unset-skus-wrap .ovc-draggable-sku[data-color="' + color + '"]').length;
			

			$('#ovc-screen-wrap .ovc-draggable-color[data-color="' + color + '"] .skus-assigned').html( skus_assigned );

			if( 0 === skus_assigned ) {
				$('#ovc-screen-wrap .ovc-draggable-color[data-color="' + color + '"]').addClass( 'skus-assigned-none' );
			}
			else if ( skus_assigned == total_skus ) {
				$('#ovc-screen-wrap .ovc-draggable-color[data-color="' + color + '"]').addClass( 'skus-assigned-all' );
			}
			else {
				$('#ovc-screen-wrap .ovc-draggable-color[data-color="' + color + '"]').addClass( 'skus-assigned-some' );	
			}

			

			
		});

		color_img_set_ids = [];

		self.setEventHandlers();

		self.getImageSetData();
	};

	this.getImageSetData = function() {

		// Target ID
		self.localImgData.targetID = $('.ovc-img-not-set-table-outer-wrap').data( 'target-id' );

		// OVC IDs without image sets
		self.localImgData.ovc_ids_no_img_set = [];
		$('#unset-skus .ovc-draggable-sku').each(function() {
			
			self.localImgData.ovc_ids_no_img_set.push( $(this).data( 'ovc-id' ) );
		});

		// Image set data, including OVC IDs associated with that image set
		self.localImgData.image_sets = {'parent':{}};
		$('.ovc-img-set-table tr[data-img-set-field] td[data-img-set-id]').each(function() {

			

			var imgSetField = $(this).parent().data('img-set-field');
			var imgSetID 	= $(this).data('img-set-id');

			// SKUs row is first, so create the image_set object
			if( 'skus' == imgSetField && ( $(this).children('.ovc-draggable-sku').length || !$(this).hasClass('fresh-img-set') ) ) {
				$(this).removeClass( 'fresh-img-set' );

				self.localImgData.image_sets[ imgSetID ] = {ovc_ids : []};

				$( this ).children('.ovc-draggable-sku').each(function() {
					self.localImgData.image_sets[ imgSetID ].ovc_ids.push( $(this).data( 'ovc-id' ) );
				});
			}
			else if( self.localImgData.image_sets.hasOwnProperty( imgSetID ) ) {

				//console.log( imgSetID );

				if( 'img_gallery_ids' == imgSetField ) {

					self.localImgData.image_sets[ imgSetID ].img_gallery_ids = [];

					$( this ).children('.ovc-img-wrap').each(function() {
						self.localImgData.image_sets[ imgSetID ].img_gallery_ids.push( $(this).data( 'image-id' ) );
					});

					self.localImgData.image_sets[ imgSetID ].img_gallery_ids.join(',');
				}
				else {
					self.localImgData.image_sets[ imgSetID ][ imgSetField ] = $( this ).children( '.ovc-img-wrap' ).eq(0).data( 'image-id' );
				}
			}
				
				
			return self.localImgData;

			


		});
	};



	this.imgSelect = function( target ) {



		var wpMediaOptions = {
			frame 		: 'post', 
			state 		: 'insert',
			library 	: { type : 'image' },
			target		: target,
			multiple	: false
		};


		if( 'img_gallery_ids' != $( target ).closest('tr').data('img-set-field') ) {

			wpMediaOptions.multiple = false;
			wpMediaOptions.title = "Select an Image";
			wpMediaOptions.button = {text:"Use this Image"};


		}
		else {

			wpMediaOptions.multiple = true;
			wpMediaOptions.title = "Select multipe Images";
			wpMediaOptions.button = {text:"Use these Images"};


		}



		self.wpMedia = wp.media( wpMediaOptions );



		self.wpMedia.on( "open", function() {

			var target = self.wpMedia.options.target;

			$( target ).parent().children('.ovc-img-wrap:not([data-image-id="0"])').each(function() {
				
				var ovcimAttachment = wp.media.attachment( $(this).data('image-id') );
				ovcimAttachment.fetch();

				self.wpMedia.state().get("selection").add( ovcimAttachment );
				//selectedImgIDs.push( $(this).data('image-id') );
			});


		});

	    self.wpMedia.on( "insert", function () {
	    	// Save the parent Element into a variable because the target element will probably get destroyed
	    	var parentElem = $( self.wpMedia.options.target ).parent();

	    	// Clear the gallery
	    	parentElem.children('.ovc-img-wrap').remove();

	    	self.wpMedia.state().get("selection").each(function( attachment ) {
	    		parentElem.prepend( self.imgSelectorFromAttachment( attachment.toJSON() ) );
	    	});

	    	self.setInterface();

	    });

	    self.wpMedia.open();
	};

	this.imgSelectorFromAttachment = function( mediaAttachment ) {

		console.log( mediaAttachment );

		var elem = $( '<div>', {class: 'ovc-img-wrap', 'onclick':'ovcim.imgSelect(this);'} )
			.html( '<i class="remove-image fa fa-times" title="Remove Image" onclick="ovcim.removeImg(event,this);"></i>');

		if( !mediaAttachment ) {
			elem.attr( 'data-image-id', 0 );
		}
		else {
			elem.attr( 'data-image-id', mediaAttachment.id );

			// Fix for potential missing sizes
			if( typeof mediaAttachment.sizes.thumbnail !== 'undefined' ) {
				elem.css( 'background-image', "url('" + mediaAttachment.sizes.thumbnail.url + "')" );	
			}
			else {
				elem.css( 'background-image', "url('" + mediaAttachment.sizes[Object.keys(mediaAttachment.sizes)[0]].url + "')" );		
			}
			
		}

		return elem;
	};

	this.removeImg = function( event, target ) {
		event.preventDefault();
		event.stopPropagation();

		var parentTD = $( target ).closest('td');

		$( target ).parent().remove();

		// if not a gallery, reappend a fresh img_select
		if( 'img_gallery_ids' != parentTD.parent().data('img-set-field') ) {
			parentTD.prepend( self.imgSelectorFromAttachment() );	
		}
		
		self.getImageSetData();
		
	};

	this.saveImageSets = function() {

		self.getImageSetData();

		var productImageRequest = {
			request	: 'save_productimages',
			args	: {
				check_only		: ovcdtMain.checkOnlyOnSave,
				ovcdt_version 	: ovcdtMain.results.meta.ovcdt_version,
				data 			: self.localImgData
			}
		};

		ovcdtMain.setStatus( 'saving' );
		ovcdtMain.ajaxRequest( productImageRequest );
	};

	self.setInterface(); // Also executes self.setEventHandlers() and self.getImageSetData()
}