// oxmd by Ray Sarno of ONYX Design
// Version 0.1.0

if ( typeof $ === 'undefined' ) {
	var $ = jQuery.noConflict();
}

window.ovc = window.ovc || {};

ovc.screen = (function() {

	var self = this,
		ajaxStart = '';

	this.selectors = {
		wrap 		: '#ovc-screen-wrap',
		tabs 		: '#ovc-screen__tabs',
		title 		: '#ovc-screen__title',
		subtitle 	: '#ovc-screen__subtitle',
		//header 		: '#ovc-screen__header',
		body 		: '#ovc-screen__content',
		footer 		: '#ovc-screen__footer'
	};

	this.currentScreen = false;
	this.currentOptions = {};

	function init() {

		if( !$(self.selectors.wrap).length ) {
			$('#wpbody-content').append(
				'<div id="ovc-screen-wrap">'+
					'<div id="ovc-screen__close"><i class="fa fa-close"></i></div>'+
					'<div id="ovc-screen__tabs"></div>'+
					'<div id="ovc-screen__header">'+
						'<h3 id="ovc-screen__title"></h3>'+
						'<div id="ovc-screen__subtitle"></div>'+
					'</div>'+
					'<div id="ovc-screen__content"></div>'+
					'<div id="ovc-screen__footer"></div>'+
				'</div>'
			);
		}

		$('#ovc-screen__close').click(hideScreen);
		$(document).on('ovc-screen-hide ovc-screen-close',hideScreen);

	}

	function setContentMulti( contentObj ) {
		for( var location in contentObj ) {
			setContent( location, contentObj[ location ] );
		}
	}

	function setContent( location, content ) {
		if( 'wrap' != location && self.selectors.hasOwnProperty( location ) ) {
			var toggle = (content === false || content === null) ? false : true;
			$(self.selectors[ location ]).html(content).toggle(toggle);
		}
		
	}

	function clearScreen(contentObj) {
		if( typeof contentObj === 'undefined' ) {
			contentObj = {
				tabs 		: '',
				title 		: '',
				subtitle 	: '',
				//header 		: '',
				body 		: '',
				footer 		: ''
			};
		}
		this.setContentMulti(contentObj);

		$(self.selectors.wrap).trigger('ovcscreen.clearScreen');
	}
	
	function showScreen() {
		$(self.selectors.wrap).addClass('show-screen');
	}

	function hideScreen(clearScreen) {
		if( typeof clearScreen === 'undefined' ) {clearScreen = true;}

		oxpm.unsetQArg('screen');
		oxpm.unsetQArg('screenid');
		
		$(self.selectors.wrap).removeClass('show-screen');

		if( clearScreen ) {
			this.currentScreen = false;
			self.currentOptions = {};
			ovc.screen.clearScreen();
		}
	};

	function saveScreen() {

		
		var fieldData = {};
		$(self.selectors.wrap).find('.ovc-screen-field').each(function() {

			if( $(this).is('select, input, textarea') ) {
				if( $(this).is(':radio, :checkbox') && !$(this).is(':checked') ) {
					return;
				}
				fieldData[ $(this).attr('name') ] = $(this).val();
			}
		});

		// Add dynamic fields to the fieldData
		$(self.selectors.wrap).find('.ovc-dynamic-field-row-dock .ovc-dynamic-field-row:not(.ovc-dynamic-field-row-deleted)').each(function() {

			var $row = $(this),
				$dynamicFieldNote = $row.find('.ovc-dynamic-field[data-field-type="note"]'),
				$dynamicFieldValue = $row.find('.ovc-dynamic-field[data-field-type="value"]'),
				$dynamicField = {
					note 	: $dynamicFieldNote.val(),
					value 	: $dynamicFieldValue.val()
				};

			if( $row.data('fieldName') in fieldData ) {
				fieldData[ $row.data('fieldName') ].push( $dynamicField );
			} else {
				fieldData[ $row.data('fieldName') ] = [ $dynamicField ];
			}
		});

		var saveProductMetaRequest = {
			request	: 'save_productmeta',
			args	: {
				check_only		: ovcdtMain.checkOnlyOnSave,
				ovcdt_version 	: ovcdtMain.results.meta.ovcdt_version,
				data 			: fieldData
			}
		};

		ovcdtMain.setStatus( 'saving' );
		ovcdtMain.ajaxRequest( saveProductMetaRequest );
	}

	function saveImageManager() {

		ovcim.saveImageSets();
	}

	function saveInventoryAssembly() {

		ovcia.saveAssemblies();
	}

	function refresh() {
		var options = self.currentOptions;
		options.refresh = true;
		loadScreen( self.currentScreen, options );
	}

	function loadScreen( screen, options ) {
		if( typeof options === 'undefined' ) {options:{};}
		// args = 'refresh' ==  args ? self.templateArgs : args;
		// self.templateArgs = args;

		var ovc_screen = {
			'request'	: 'load',
			'screen'	: screen,
			'options'	: options
		};

		self.currentScreen = screen;
		self.currentOptions = options;
		

		var ajax_data = {
			'ovc_ajax'	: true,
			'action' 	: 'ovc_ajax',
			'command'	: 'ovc_screen',
			'ovc_screen': ovc_screen
		};

		var ajax_args = {
			url			: location.origin + ajaxurl,
			type		: "POST",
			data 		: ajax_data,
			cache		: false,
			dataType	: "json",
			complete	: function( response, status ) {
				if ( 'success' == status ) {
					var rjson = response.responseJSON;

					ovc.screen.setContentMulti( rjson.content );

					oxpm.setQArg('screen', self.currentScreen);
					oxpm.setQArg('screenid', options.ovc_id);

					ovc.screen.showScreen();

					$('#ovc-screen-wrap').trigger('ovc-screen-loaded', self.currentscreen);

					ovcNoticeBox.setStatus('ready');

				
				}
				else if ( 'timeout' == status ) {
					console.log('OVC Screen Load Template failed. AJAX Timeout :(' );
				}
				else {
					console.log('OVC Screen Load Template Error!');
					console.log( response );
				}
			}
		};

		ovcNoticeBox.setStatus('loading');

		$.ajax( ajax_args );
	}

	function uploadFile() {

		var file = $( self.selectors.body ).find( '#override_image' )[0].files[0],
			ovc_id = $( self.selectors.body ).find( '#ovc_id' ).val(),
			ajax_data = new FormData();

		ajax_data.append( 'ovc_ajax', true );
		ajax_data.append( 'action', 'ovc_ajax' );
		ajax_data.append( 'command', 'ovc_screen' );
		ajax_data.append( 'ovc_screen', 'file_upload' );
		ajax_data.append( 'ovc_id', ovc_id );
		ajax_data.append( 'check_only', ovcdtMain.checkOnlyOnSave );
		ajax_data.append( 'ovcdt_version', ovcdtMain.results.meta.ovcdt_version );
		ajax_data.append( 'uploaded_file', file, file.name );

		var ajax_args = {
			url			: ajax_object.ajax_url,
			type		: 'POST',
			data 		: ajax_data,
			cache		: false,
			dataType	: 'json',
			processData : false,
			contentType : false,
			complete	: function( response, status ) {
				console.log( response );
				console.log( status );

				if ( 'success' == status ) {
					// Handle response
					// ovcNoticeBox.notice( 'Image Upload Successful.' );
					ovcdtMain.handleResponse( response.responseJSON );
				}
				else {
					// Handle fail
					ovcNoticeBox.notice( 'Image Upload Failed!', true );

					console.log( "AJAX request failed with status: " + status );
					console.log( response );
				}

				// jQuery( '#override_image_upload' ).html( '<i class="fa fa-upload ovc-screen-upload-icon"></i>Upload' ).prop( 'disabled',false );
			}
		};

		$.ajax( ajax_args );
	};

	return {
		init : init,
		showScreen : showScreen,
		hideScreen : hideScreen,
		loadScreen : loadScreen,
		clearScreen: clearScreen,
		setContentMulti : setContentMulti,
		setContent : setContent,
		refresh : refresh,
		saveScreen : saveScreen,
		uploadFile : uploadFile,
		saveImageManager : saveImageManager,
		saveInventoryAssembly: saveInventoryAssembly,
	};
})();

$(document).ready(function() {
	ovc.screen.init();
});