if ( typeof $ === 'undefined' ) {
	var $ = jQuery.noConflict();
}

window.ovc = window.ovc || {};

ovc.assembly = (function() {


	var ovciaVersion, selectors;


	function OvcAssemblyManager(ovciaVersionLatest) {

		var lodash = window.lodash;

		// Check OVCIA Version against server
		ovciaVersion = lodash.defaultTo(ovciaVersion, ovciaVersionLatest);

		if( ovciaVersion != ovciaVersionLatest ) {
			oxmd.modal({
				type 		: "custom",
				callback 	: function() {location.reload( true );},
				responses 	: [{label:"Reload Page",value:1,primary:true}],
				title 		: "OVC Inventory Assembly Manager Version Mismatch",
				elements 	: [
					"You are not running the most recent version of the OVC Inventory Assembly Manager.  Please refresh the page to update to the latest version.",
					{innerElems:[{type:"p",attrs:{},content:('<span class="oxmd-label-span">Your Version:</span>'+ovciaVersion+'<br /><span class="oxmd-label-span">Current Version:</span>'+ovciaVersionLatest)}]}
				]
			});
		}

		var selectors 			= this.selectors = {
			$screenWrap 		: $('#ovc-screen--assemblies'),
			$contentWrap 		: $('#ovc-screen__content > div').eq(0),
			$ovciaJson 			: $('#ovcia-data'),
			$skuTemplate 		: $('#ovcia-sku-template'),
			$skuDock 			: $('#ovcia-sku-dock'),
			$assembliesWrap 	: $('#ovcia-assemblies'),
			assemblyItemsWrap	: '.ovcia-assembly-items-wrap',
			skuWrap 			: '.ovcia-sku-wrapper',
			$totalSkusCount 	: $('.ovcia-count--total-skus'),
			$filteredSkusCount 	: $('.ovcia-count--filtered-skus'),
			$filterInputs 		: $('.ovcia-filter-input'),
			$filterLabels 		: $('.ovcia-filter-label')
		};

		// Get raw JSON of OVC Product and Assemblies dat
		var ovciaData 			= JSON.parse( selectors.$ovciaJson.text() ),
			topLevelAssemblies 	= this.topLevelAssemblies 	= {}, 
			assemblyTrees 		= this.assemblyTrees 		= [],
			assemblyStacks 		= this.assemblyStacks 		= {};

		var defaults 			= {
			assemblyData 		: {
				ID 					: null,
				ovc_id 				: null,
				primary_case 		: null,
				total_quantity 		: null
			},
			assemblyItemData 	: {
				ID 					: null,
				ovc_id 				: null,
				assembly_id 		: null,
				quantity 			: null
			},
			filters 			:{
				sku_search			: '',
				is_raw 				: null,
				is_case 			: null,
				is_primary 			: null,
				count_asmi 			: null,
				count_asm 			: null
			}
		};

		// SKU Filters vars
		var filtersCleared = this.filtersCleared = true;
		var activeFilters = this.activeFilters = $.extend({}, defaults.filters );

		function updateDomValue(attrs, value, ancestor) {

			ancestor = lodash.defaultTo( ancestor, selectors.$screenWrap);

			var selectorString = '';

			lodash.forEach( attrs, function( attrValue, attr ) {

				var fullAttr = 'data-' + lodash.kebabCase( attr );
				selectorString += '['+fullAttr+'="'+attrValue+'"]';
			});

			var $target = $(ancestor).find(selectorString);

			if( $target.is('input') ) {
				$target.val(value);
			}
			else {
				$target.html(value);	
			}
		}

		var OvciaModel = {
			updateResourceValue: function(field, value, target) {
				updateDomValue({
					ovcFs: this.alias,
					ovcId: this.data.ID,
					ovcField: field
				}, value, target );
			}
		};
		/**
		 * OVC SKU Block Class
		 * 
		 * Controls a SKU Block DOM element
		 */
		// function DomSkuBlock(product) {

		// }

		/**
		 * OVC Product Handler Class
		 *
		 * @class      OvciaProduct (name)
		 * @param      {object}  product  The raw JSON product data
		 */
		function OvciaProduct(product) {
			
			// // Initial data (so we can track what has changed)
			// this.initialData = {
			// 	pr : lodash.clone(product),
			// 	asm : {},
			// 	asmi : {}
			// };

			// Give all properties of product to this

			// lodash.assign(this, product);
			this.model 			= 'product';
			this.alias 			= 'pr';
			this.data 			= lodash.assign({}, product);
			this.origData 		= lodash.assign({}, product);

			this.assemblies 	= this.asm 	= {};
			this.assemblyItems 	= this.asmi = {};
			this.filterPass 	= true; 	// Whether the product has passed the activeFilter checks
			this.tree 			= null;

			this.$primarySku 	= this.initDomSku();
			this.$primarySku.data('ovciaProduct', this);

			this.$skus 		 	= [ this.$primarySku ];
		}

		OvciaProduct.prototype = $.extend({}, OvciaProduct.prototype, OvciaModel, {

			joinAssembly: function(assembly) {
				if( assembly.data.ovc_id != this.data.ID) {
					console.log( 'Cannot join assembly to product. OVC ID mismatch');
				} else {
					this.assemblies[assembly.data.ID] = assembly;


					var assembliesCount = this.countAssemblies();
					this.$primarySku.attr('data-count-asm', assembliesCount)
						.find('.count-asm span').text(assembliesCount);

				}
			},
			joinAssemblyItem: function(assemblyItem) {
				if( assemblyItem.data.ovc_id != this.data.ID) {
					console.log( 'Cannot join assembly_item to product. OVC ID mismatch');
				} else {
					this.assemblyItems[assemblyItem.data.ID] = assemblyItem;

					var assemblyItemsCount = this.countAssemblyItems();
					this.$primarySku.attr('data-count-asmi', assemblyItemsCount)
						.find('.count-asmi span').text(assemblyItemsCount);
				}
			},
			initDomSku : function(product) {
				product = lodash.defaultTo(product, this);
				var $sku = selectors.$skuTemplate.clone();

				$sku.removeAttr('id')
					.attr({
						'data-ovc-id'	: product.data.ID,
						'data-is-case'	: product.data.is_case,
						'data-is-raw'	: product.data.is_raw,
						'data-size'		: product.data.size
					})
					.data({
						ovciaProduct 	: product
					})
					// Initialize Resource DOM fields
					.children('.ovcia-sku-block').find('[data-ovc-fs="pr"][data-ovc-field]').each(function() {
						var $target = $(this);
						var pr_field = $(this).data('ovc-field');

						$(this)
							.html(product.data[pr_field])
							.attr('data-ovc-id', product.data.ID);
					});
				
				$sku.appendTo( selectors.$skuDock );
				return $sku;
			},
			cloneDomSku : function(registerSku) {

				var $clonedSku = this.$primarySku.clone();

				if( lodash.defaultTo( registerSku, true) ) {
					this.$skus.push( $clonedSku );
				}

				return $clonedSku;
			},
			registerSku : function($sku) {

				if( lodash.defaultTo( $sku, false) 
					&& $sku.attr('data-ovc-id') == this.data.ID
				) {
					this.$skus.push( $sku );
					return true;
				}
			},
			filterSKU: function(filters) {

				filters = lodash.defaultTo( filters, activeFilters );
				var hideSku = false;
				var product = this;

				if( !filtersCleared ) {

					lodash.forEach( filters, function( filterValue, filterKey ) {

						switch( filterKey ) {
							case 'sku_search':

								if( filterValue 
									&& filterValue.length
									&& !product.data.ID.toString().includes(filterValue.toString())
									&& !product.data.sku.toLowerCase().includes(filterValue.toLowerCase())
								
								) {

									hideSku = true;
								}

							break;
							case 'count_asmi':
								if( lodash.isInteger( filterValue ) ) {

									var assemblyItemsCount = lodash.size( product.assemblyItems );

									if( ( filterValue < 2 && assemblyItemsCount != filterValue )
										|| ( filterValue >= 2 && assemblyItemsCount < 2 ) 
									) {

										hideSku = true;
									}
								}
								
							break;
							case 'count_asm':

								if( lodash.isInteger( filterValue ) ) {

									var assembliesCount = lodash.size( product.assemblies );

									if( ( filterValue < 2 && assembliesCount != filterValue )
										|| ( filterValue >= 2 && assembliesCount < 2 ) 
									) {

										hideSku = true;
									}
								}
							break;
							default:

								// This means we are testing for a boolean filter (e.g. is_case or is_raw)
								// If filterValue is true, then we filter
								// Otherwise we ignore the filter //dev:improve - add optional true/false toggling
								if( filterValue && lodash.get(product.data, filterKey) != 1) {
									hideSku = true;
								}
							break;
						}

						return !hideSku;
					// });
					});
				}


				this.filterPass = !hideSku;
				
				this.$primarySku.toggle(this.filterPass);
				
				// if( !search || !search.length || this.ID.toString().includes(search) || this.sku.toLowerCase().includes(search.toLowerCase()) ) {
				// 	this.$primarySku.show();
				// }
				// else {
				// 	this.$primarySku.hide();
				// }
			},
			isUnattached: function() {
				return ( lodash.isEmpty(this.assemblies) && lodash.isEmpty(this.assemblyItems));
			},
			isTopLevel: function() {
				return ( !lodash.isEmpty(this.assemblies) && lodash.isEmpty(this.assemblyItems));
			},
			isBottomLevel: function() {
				return ( lodash.isEmpty(this.assemblies) && !lodash.isEmpty(this.assemblyItems));
			},
			countAssemblies: function() {
				return lodash.size(this.assemblies);
			},
			countAssemblyItems: function() {
				return lodash.size(this.assemblyItems);
			},
			getAssemblyParentProducts: function() {
				var assemblyParentProducts = {};

				lodash.forEach( this.assemblyItems, function(assemblyItem) {
					assemblyParentProducts[ assemblyItem.assembly.product.data.ID ] = assemblyItem.assembly.product;
				});
				
				return assemblyParentProducts;
			},
			getSiblingAssemblyItemProducts: function() {
				var siblingAssemblyItemProducts = {};

				lodash.forEach( this.assemblyItems, function(assemblyItem) {
					lodash.forEach( assemblyItem.getSiblings(), function(assemblyItemSibling) {
						siblingAssemblyItemProducts[assemblyItemSibling.product.data.ID] = assemblyItemSibling.product;
					});
				});

				return siblingAssemblyItemProducts;
			},
			getChildAssemblyItemProducts: function() {
				var childAssemblyItemProducts = {};

				lodash.forEach( this.assemblies, function(assembly) {
					lodash.forEach( assembly.childProducts, function(childProduct) {
						childAssemblyItemProducts[childProduct.data.ID] = childProduct;
					});
				});

				return childAssemblyItemProducts;
			},
			getCloseRelatedProducts:function() {
				return lodash.assignIn({}, this.getAssemblyParentProducts(), this.getSiblingAssemblyItemProducts(), this.getChildAssemblyItemProducts() );
			},
			getAllRelatedProducts: function(allRelatedProducts, includeSelf) {
				includeSelf = lodash.defaultTo(includeSelf, true);
				allRelatedProducts = lodash.defaultTo(allRelatedProducts, {});

				var closeRelatedProducts = this.getCloseRelatedProducts();

				lodash.forEach(closeRelatedProducts, function(closeRelatedProduct) {

					if( !lodash.has( allRelatedProducts, closeRelatedProduct.data.ID ) ) {

						allRelatedProducts[closeRelatedProduct.data.ID] = closeRelatedProduct;

						allRelatedProducts = closeRelatedProduct.getAllRelatedProducts(allRelatedProducts);
					}
				});

				if( !includeSelf ) {
					lodash.unset( allRelatedProducts[this.data.ID]);
				}

				return allRelatedProducts;
			},
			getMaxDepth: function() {
				// SKU has no assemlies or assembly items: return null
				if( this.isUnattached() ) { 
					return null;
				}
				// SKU is at least one assembly parent but is not an assembly item: return 0 (top level depth)
				else if( this.isTopLevel() ) { 
					return 0;
				}
				// SKU is at least one assembly item
				else {
					return Math.max( lodash.invokeMap( this.assemblyItems, 'getMaxDepth') );
				}
			},
			getPotentialStock: function( calledBy, updateDom ) {
				calledBy 	= lodash.defaultTo( calledBy, false );
				updateDom 	= lodash.defaultTo( updateDom, true );

				var product = this;
				var potentialStock = 0;

				lodash.forEach(product.assemblyItems, function(assemblyItem) {
					if( !calledBy || ( 'asmi' == calledBy.alias && lodash.get( calledBy, 'data.ID', false ) != assemblyItem.data.ID ) ) {
						potentialStock += assemblyItem.getDisassembledPotentialStock();
					}
					else {
						console.log('skipped adding stock');
					}
				});

				lodash.forEach(product.assemblies, function(assembly) {
					if( !calledBy || ( 'asmi' == calledBy.alias && lodash.get( calledBy, 'assembly.data.ID', false ) != assembly.data.ID ) ) {
						potentialStock += assembly.getAssembledPotentialStock();
					}
				});

				potentialStock += product.data.avail_qty;

				if( updateDom ) {
					this.updateResourceValue( 'potential_qty', potentialStock );
					// updateDomValue({
					// 	ovcFs:'pr',
					// 	ovcId:product.data.ID,
					// 	ovcField:'potential_qty'
					// },
					// potentialStock);
					// selectors.$screenWrap.find('[data-ovc-field="potential_qty"][data-ovc-id="'+product.data.ID+'"]').html(potentialStock);
				}

				return potentialStock;
			}
		});
		
		/**
		 * OVC Assembly Handler Class
		 *
		 * @class      OvciaAssembly (name)
		 * @param      {object}  assembly  The raw JSON assembly data
		 */
		function OvciaAssembly(assembly) {

			this.model 			= 'assembly';
			this.alias 			= 'asm';
			this.data 			= lodash.assign({}, assembly);
			this.origData 		= lodash.assign({}, assembly);
			// this.origData       = lodash.assign({}, this.data)

			this.product 		= this.pr 	= ovciaData.pr[this.data.ovc_id];
			this.childProducts 	= {};
			this.assemblyItems 	= this.asmi = {};
			this.potential_qty 	= null;
			this.$domAssembly 	= null;
			this.$domAssemblyHead 		= null;
			this.$domAssemblyTotalQty = null;
			

			this.stack 			= null;

			// Associate this assembly with it's product
			this.product.joinAssembly(this);

			
		}

		OvciaAssembly.prototype = $.extend({}, OvciaAssembly.prototype, OvciaModel, {
			getTree:function() {
				return this.product.tree;
			},
			joinAssemblyItem: function(assemblyItem) {
				if( assemblyItem.data.assembly_id != this.data.ID) {
					console.log( 'Cannot join assembly_item to assembly. Assembly ID mismatch');
				} else {
					this.assemblyItems[assemblyItem.data.ID] = assemblyItem;
					this.childProducts[assemblyItem.product.data.ID] = assemblyItem.product;
				}
			},
			initDomAssembly: function(assembly, assemblyItem) {
				assembly = lodash.defaultTo(assembly, this);
				assemblyItem = lodash.defaultTo(assemblyItem, false);


				// Determine $domAssembly //dev:improve
				if( assembly.isTopLevel() ) {
					assembly.$domAssembly = assembly.product.cloneDomSku();

					assembly.$domAssembly
						.removeClass('ovcia-sku-primary')
						.addClass('ovcia-sku-assembly')
						.attr('data-asm-id', assembly.data.ID)
						.data('ovciaAssembly', assembly)
						.appendTo(assembly.product.tree.$treeWrap);

					assembly.$domAssemblyHead = assembly.$domAssembly.children('.ovcia-assembly-wrap').children('.ovcia-assembly-head');
						
					assembly.$domAssemblyHead.find('[data-ovc-fs="asm"][data-ovc-field]').each(function() {

						var $field = $(this);
						var ovcField = $field.data('ovc-field');

						$field.attr({
							'data-ovc-id' 		: assembly.data.ovc_id,
							'data-asm-id' 		: assembly.data.ID
						});

						if( lodash.has( assembly, 'data.'+ovcField ) ) {

							//dev:improve //dev:virtualdom
							if( $field.is('input') ) {
								$field.val( lodash.get( assembly, 'data.'+ovcField, '') );
							}
							else {
								$field.html( lodash.get( assembly, 'data.'+ovcField, '') );
							}
						}

					});


				}
				else if( assemblyItem ){
					assembly.$domAssembly = assemblyItem.$domAssemblyItem;

					assembly.$domAssembly.addClass('ovcia-sku-assembly').attr('data-asm-id', assembly.data.ID);
				}

				
				

				assembly.$domAssemblyTotalQty = assembly.$domAssembly.find('input.ovcia-input--asm__total_quantity').eq(0);
				assembly.$domAssemblyTotalQty
					.attr({
						'data-ovc-id' 		: assembly.data.ovc_id,
						'data-asm-id' 		: assembly.data.ID
					})
					.data({
						ovciaAssembly 		: assembly
					});

				assembly.$domAssembly.add(assembly.$domAssemblyTotalQty).data('ovciaAssembly', assembly);				

				lodash.invokeMap(assembly.assemblyItems, 'initDomAssemblyItem');



				return assembly.$domAssembly;
			},
			isTopLevel: function() {
				return this.product.isTopLevel();
			},
			countAssemblyItems: function() {
				return lodash.size(this.assemblyItems);
			},

			getAssemblyItemCousins: function() {
				return this.product.assemblyItems;
			},
			getAssemblyCousins: function() {
				return lodash.omit( this.product.assemblies, this.data.ID );
			},
			getMaxDepth: function() {
				return this.product.getMaxDepth();
			},
			// Validate OVC ID uniqueness across assembly items (siblings)
			// Optionally supply a new product to see if it can be added
			validateAssemblyItemsUnique: function(checkProduct) {

				checkProduct = lodash.defaultTo( checkProduct, false );

				var uniqueOvcIds = lodash.uniq( lodash.map(this.assemblyItems, 'data.ovc_id') );

				if( uniqueOvcIds.length != this.countAssemblyItems() ) {

				}

			},
			validateOvcIdRecusion: function(checkProduct) {
				checkProduct = lodash.defaultTo(checkProduct, false);
				var valid = true;
				var self = this;

				lodash.forEach(this.assemblyItems, function(assemblyItem) {
					if( assemblyItem.data.ovc_id == self.data.ovc_id ) {
						valid = false;
					}

					if( valid && checkProduct ) {
						valid = assemblyItem.validateOvcIdRecusion(checkProduct);
					}

					return valid;
				});

				return valid;
			},
			calculateTotalQuantity: function(updateDom) {
				updateDom = lodash.defaultTo(updateDom, true);
				var assemblyItem = $(this).closest(selectors.skuWrap).data('ovciaAssemblyItem');


				var totalQuantity = 0;

				lodash.forEach(this.assemblyItems, function(assemblyItem) {
					if( lodash.defaultTo( ))
					totalQuantity += parseInt( lodash.defaultTo( assemblyItem.$domAssemblyItemQty.val() ) );
				});

				if( updateDom ) {
					// this.$domAssemblyTotalQty.val(totalQuantity);	
				}

				return totalQuantity;
			},
			getAssembledPotentialStock: function() {

				var assembledPotentialStock = null;
				var self = this;

				// console.log( 'ASMPT: ' + this.data.ID );

				lodash.forEach( this.assemblyItems, function(assemblyItem) {

					var assemblyItemPotentialStock = assemblyItem.product.getPotentialStock(assemblyItem, false);

					var potentialAssemblyQty = assemblyItem.data.quantity 
						? Math.floor( assemblyItemPotentialStock / assemblyItem.data.quantity )
						: 0;

					assembledPotentialStock = assembledPotentialStock === null 
						? potentialAssemblyQty
						: Math.min( assembledPotentialStock, potentialAssemblyQty );
				});

				// Update DOM


				return lodash.defaultTo( assembledPotentialStock, 0);
			}
		});

		/**
		 * OVC Assembly Item Handler Class
		 *
		 * @class      OvciaAssemblyItem (name)
		 * @param      {object}  assemblyItem  The raw JSON assembly item data
		 */
		function OvciaAssemblyItem(assemblyItem) {

			this.model 		= 'assemblyItem';
			this.alias 		= 'asmi';
			this.data 		= lodash.assign({}, assemblyItem);
			this.origData 	= lodash.assign({}, assemblyItem);

			this.product 	= this.pr 	= ovciaData.pr[this.data.ovc_id];
			this.assembly 	= this.asm 	= ovciaData.asm[this.data.assembly_id];

			// Associate this assembly item with its product and assembly
			this.product.joinAssemblyItem(this);
			this.assembly.joinAssemblyItem(this);

			this.$domAssemblyItem = null;
			this.$domAssemblyItemQty = null;

		}

		OvciaAssemblyItem.prototype = $.extend({}, OvciaAssemblyItem.prototype, OvciaModel, {
			getTree:function() {
				return this.product.tree;
			},
			getMaxDepth: function() {
				return this.assembly.product.getMaxDepth() + 1;
			},
			initDomAssemblyItem: function(assemblyItem) {
				assemblyItem = lodash.defaultTo(assemblyItem, this);

				// dev:validation/error handling maybe?


				// Get the element of the assembly
				// $domAssembly = this.assembly.$domAssembly;


				// $domAssembly = this.assembly.$domAssembly;
				// Dev:imrove - handling if we dont' need to create a new item

				assemblyItem.$domAssemblyItem = assemblyItem.product.cloneDomSku();


				assemblyItem.$domAssemblyItem
					.removeClass('ovcia-sku-primary')
					.addClass('ovcia-sku-assembly-item')
					.attr('data-asmi-id', assemblyItem.data.ID)
					.children('.ovcia-sku-block').find('[data-ovc-fs="asmi"]');
					// .each(function() {
					// 	var asmi_field = $(this).data('ovc-field');
					// 	if( lodash.has( assemblyItem, 'data.'+asmi_field) ) {
					// 		if( $(this).is('input') ) {
					// 			$(this).val(assemblyItem.data[asmi_field]);
					// 		}
					// 		else {
					// 			$(this).html(assemblyItem.data[asmi_field]);	
					// 		}
					// 	}
					// });
				
				assemblyItem.$domAssemblyItem
					.appendTo(assemblyItem.assembly.$domAssembly.find(selectors.assemblyItemsWrap).eq(0))
					.data('ovciaAssemblyItem', assemblyItem);

				// Initialize Assembly Item quantity input
				assemblyItem.$domAssemblyItemQty = assemblyItem.$domAssemblyItem.find('input.ovcia-input--asmi__quantity').first();

				console.log( 'OVCIA ASMI');
				console.log( ovcia );
				console.log( assemblyItem );

				assemblyItem.$domAssemblyItemQty
					.attr({
						'data-ovc-id'  		: assemblyItem.data.ovc_id,
						'data-asmi-id' 		: assemblyItem.data.ID
					})
					.data({
						ovciaAssemblyItem 	: assemblyItem
					})
					.on('input change', function(event) {
						assemblyItem.data.quantity = parseInt( $(this).val() );
						assemblyItem.assembly.calculateTotalQuantity();
						lodash.invoke( assemblyItem.getTree(), 'recalculatePotentialStocks' );
					});
					// .attr('data-ovc-id', assemblyItem.ovc_id).attr('data-asmi-id', assemblyItem.ID);

				// Update the assembly counts
				assemblyItem.assembly.calculateTotalQuantity();

				// Add this as a data attribute to the dom wrap and the 
				// assemblyItem.$domAssemblyItem.add(assemblyItem.$domAssemblyItemQty).data('ovciaAssemblyItem', assemblyItem);
					

				lodash.forEach(assemblyItem.getAssemblyCousins(), function(assembly) {
					// Only initialize the rest of the stack if it hasns't been intitialized //dev:improve //dev:trees/stacks
					if( !assembly.$domAssembly ) {
						assembly.initDomAssembly(assembly, assemblyItem);	
					}
				});

				return assemblyItem.$domAssemblyItem;

			},
			createNewAssemblyItem: function(product, assembly) {

				var assemblyItem = {
					ID : 'new1',
					ovc_id: product.data.ID,
					assembly_id: assembly.data.ID,
					quantity: 1
				};

				assemblyItem = new OvciaAssemblyItem(assemblyItem);
				// product.joinAssemblyItem(assemblyItem);
				// assemblies[assemblyItem.data.ID].joinAssemblyItem(assemblyItem);
				assemblyItem.initDomAssemblyItem();
				assemblyItem.assembly.calculateTotalQuantity();
				lodash.invoke( assemblyItem.getTree(), 'recalculatePotentialStocks' );
				// assemblyItem.assembly.tree.recalculatePotentialStocks();
			},
			getSiblings: function() {
				return lodash.omit( this.assembly.assemblyItems, this.data.ID);
			},
			// Returns other assembly items of this assembly item's product (omitting self)
			getAssemblyItemCousins: function() {
				return lodash.omit( this.product.assemblyItems, this.data.ID );
			},
			getAssemblyCousins: function() {
				return this.product.assemblies;
			},
			validateOvcIdRecusion: function(checkProduct) {
				checkProduct = lodash.defaultTo(checkProduct, false);
				var valid = true;

				lodash.forEach(this.getAssemblyItemCousins, function(assemblyItem) {

					if( assemblyItem.ovc_id == checkProduct.data.ID ) {
						valid = false;
						return valid;
					}
				});

				return valid;
			},
			// Gets the (dis)assembly potential fort this assembly item within it
			getDisassembledPotentialStock: function() {
				// return Math.floor( this.assembly.product.getPotentialStock(this, false) * this.data.quantity );
				return Math.floor( this.assembly.product.getPotentialStock(this, false) * this.data.quantity );
			}
		});

		function OvciaAssemblyTree(relatedProducts) {

			
			var self = this;
			var index = this.index = assemblyTrees.length;
			var products = this.products = lodash.defaultTo(relatedProducts, {});
			var $treeWrap = this.$treeWrap = null;

			lodash.map(products, function(product) {
				product.tree = self;
			});
			
			assemblyTrees.push(this); // This should make this.index true

			return this;
		}

		OvciaAssemblyTree.prototype = $.extend({}, OvciaAssemblyTree.prototype, {

			getTopLevelProducts: function() {

				return lodash.filter( this.products, function(product) {return product.isTopLevel()});
			},
			getTopLevelAssemblies: function() {
				var topLevelAssemblies = {};
				
				lodash.forEach( this.getTopLevelProducts(), function(topLevelProduct) {
					lodash.forEach(topLevelProduct.assemblies, function(assembly) {
						topLevelAssemblies[assembly.data.ID] = assembly;
					});
				});

				return topLevelAssemblies;
			},
			recalculatePotentialStocks: function() {
				lodash.invokeMap( this.products, 'getPotentialStock' );
			}

		});
		//asdfasdf;alsdkjfa;sldkjfa;slkdjfa;slkdjfa;slkdjfa;slkdjf;alskdjf;alskdjfa;lskdjfa;lsj


		this.getData = function(path) {
			// return path = lodash.defaultTo(path, '');

			return lodash.get(ovciaData, lodash.defaultTo(path, null));
		};


		// Initialize Products, Assemblies, and Assembly Items objects
		var products = this.products = ovciaData.pr = lodash.mapValues(ovciaData.pr, function(product) {return new OvciaProduct(product); });
		var assemblies = this.assemblies = ovciaData.asm = lodash.mapValues(ovciaData.asm, function(assembly) {return new OvciaAssembly(assembly); });
		var assemblyItems = this.assemblyItems = ovciaData.asmi = lodash.mapValues(ovciaData.asmi, function(assemblyItem) {return new OvciaAssemblyItem(assemblyItem); });

		// console.log('ovciaData after mapValues', ovciaData);
		topLevelAssemblies = this.getTopLevelAssemblies();

		// Initialize Assembly Trees
		var topLevelProducts = this.topLevelProducts = lodash.filter( products, function(product) {return product.isTopLevel()});

		lodash.forEach(topLevelProducts, function(topLevelProduct) {
			if( !topLevelProduct.tree ) {
				new OvciaAssemblyTree( topLevelProduct.getAllRelatedProducts() );
			}
		});

		// Build out DOM Assemblys (Trees)
		lodash.each(assemblyTrees, function(assemblyTree) {

			var $treeWrap = assemblyTree.$treeWrap = $('<div class="ovcia-assembly-tree"></div>');
			selectors.$assembliesWrap.append( $treeWrap );

			lodash.forEach(assemblyTree.getTopLevelAssemblies(), function(topLevelAssembly) {
				topLevelAssembly.initDomAssembly();
			});
		});

		// lodash.invokeMap( topLevelAssemblies, 'determineAssemblyStack' );
		
		// Init top-level assemblies
		// lodash.invokeMap(this.getTopLevelAssemblies(), 'initDomAssembly');


		// INITIALIZE FILTERS
		selectors.$filteredSkusCount.text( products.length );

		

		// this.saveAssemblies = function() {

		// 	var assemblyData = ovciaData;

		// 	var productAssemblyRequest = {
		// 		request : 'save_productassemblies',
		// 		args	: {
		// 			check_only		: ovcdtMain.checkOnlyOnSave,
		// 			ovcdt_version	: ovcdtMain.results.meta.ovcdt_version,
		// 			data 			: assemblyData
		// 		}
		// 	}
		
		// 	ovcdtMain.setStatus( 'saving' );
		// 	ovcdtMain.ajaxRequest( productAssemblyRequest );
		// }

		// function getFiltersFromUI() {

		// 	activeFilters = {

		// 	}
		// }

		function setFilters() {

			// Detect if we should be back on 'Show All'
			filtersCleared = lodash.isEqual( activeFilters, defaults.filters);

			$('#ovcia-filter__show_all').prop('checked', filtersCleared);
			lodash.invokeMap(products, 'filterSKU');

			// Update product counts
			$('.ovcia-count--total-skus').text( lodash.size(products));
			$('.ovcia-count--filtered-skus').text( lodash.defaultTo( lodash.countBy(products, 'filterPass').true, 0 ) ) ;
		}

		function clearFilters() {

			selectors.$filterInputs.filter('[type="text"]').val('');
			selectors.$filterInputs.not('[type="text"]').prop('checked', false);

			activeFilters = $.extend({}, defaults.filters );
			filtersCleared = true;

			setFilters();
		}

		$('.ovcia-input--asm__total_quantity').on('input click focus change', function(e) {
				$(this).blur();
				e.preventDefault();
				e.stopPropagation();
			});
		// selectors.$assembliesWrap.find(selectors.skuWrap)
		// 	.on('change input', '.ovcia-input--asmi__quantity', function() {

		// 		var assemblyItem = $(this).closest(selectors.skuWrap).data('ovciaAssemblyItem');


		// 		var totalQuantity = 0;

		// 		lodash.forEach(assemblyItem.assembly.assemblyItems, function(sibling) {
		// 			totalQuantity += parseInt( sibling.$domAssemblyItemQty.val() );
		// 		});

		// 		assemblyItem.assembly.$domAssemblyTotalQty.val(totalQuantity);
		// 	});

		$('#ovcia-filter-sku-search').on('input change', function () {

			activeFilters.sku_search = $(this).val();

			setFilters();
		});
		
		selectors.$filterInputs.not('#ovcia-filter-sku-search').on('change', function() {


			$input = $(this);
			

			// Determine input handling
			if( $input.is('#ovcia-filter__show_all') ) {
				clearFilters();
			}
			else {

				if( 'checkbox' == $input.attr('type') ) {

					activeFilters[ $input.data('filter-prop') ] = $input.prop('checked') ? true : null;
				}

				else if( 'radio' == $input.attr('type') ) {

					var inputValue = parseInt( $input.val() );
					var currentFilterValue = activeFilters[ $input.data('filter-prop') ];

					// Deselect radio if selected
					if( inputValue === currentFilterValue ) {
						$input.prop('checked', false );
						activeFilters[ $input.data('filter-prop') ] = null;
					}
					else {
						activeFilters[ $input.data('filter-prop') ] = inputValue;	
					}
				}
			}

			

			setFilters();
		});

		// Unchecking selected radios from label clicks
		selectors.$filterLabels.click(function() {
			// Determine attached input
			$input = $('input#' + $(this).attr('for') + ':radio' );

			if( $input.length && $input.prop('checked') ) {

				var inputValue = parseInt( $input.val() );
				var currentFilterValue = activeFilters[ $input.data('filter-prop') ];

				if( inputValue === currentFilterValue ) {

					$input.prop('checked', false ); // This will trigger the change event on it's own appparently?
				}
			}
		});

		setFilters();

		
		

		// DRAGGABLE / DROPPABLE OBJECTS
		var $droppableTargets = selectors.$assembliesWrap.find(selectors.skuWrap);

		var draggableOptions = {
			helper 			: 'clone',
			appendTo 		: '#ovc-screen--assemblies',
			cursor			: '-webkit-grabbing',
			revert 			: "invalid",
			revertDuration 	: 400,
			drag 			: function(event, ui) {
				// $(ui).addClass('ovc-dragga')
			}
		};

		// Draggable SKUs
		selectors.$skuDock.find('.ovcia-sku-wrapper').draggable( draggableOptions );

		$droppableTargets.droppable({
			accept 			: selectors.skuWrap,
			greedy 			: true,
			classes 		: 'droppable-active',
			activeClass 	: 'droppable-active',
			hoverClass 		: 'droppable-hover',
			drop 			: function( event, ui ) {

				var assembly 	= $(this).data('ovciaAssembly');
				var product 	= ui.draggable.data('ovciaProduct')

				if( !assembly || !product ) {
					console.log( 'CANNOT DROP SKU INTO A NON-ASSEMBLY');
				}
				else {

					OvciaAssemblyItem.prototype.createNewAssemblyItem( product, assembly );
				}

				console.log( 'dropped', ui.draggable );
				console.log( 'product Obj', ui.draggable.data('ovciaProduct'));
				console.log( 'assembly Obj', $(this).data('ovciaAssembly'));
				// //console.log( event );
				// //$( this ).append( ui.helper );
				// var appendTarget = $(this).hasClass('unset-skus-td') ? $(this).children('.unset-skus-wrap').eq(0) : $(this); 
				// ui.draggable.appendTo( appendTarget );
				// self.setInterface();
			}
		});
		
		/*
		// Draggable IMGs
		$('#ovcia-assemblies .ovc-img-wrap:not([data-image-id="0"])').draggable( draggableOptions );

		$('#ovcia-assemblies .ovc-img-wrap, .ovcim-gallery-select').droppable({
			accept 			: ".ovc-img-wrap",
			activeClass 	: 'droppable-active',
			hoverClass 		: 'droppable-hover',
			drop 			: function( event, ui ) {

				if( $(this).hasClass( 'ovcim-gallery-select' ) ) {

					ui.draggable.clone().insertBefore( $(this) );
				}
				else {
					var imageID = ui.draggable.data( 'image-id' );
					
					$(this).attr('data-image-id', ui.draggable.attr('data-image-id') );
					$(this).data('image-id', imageID ); 
					$(this).css('background-image', ui.draggable.css('background-image') );
				}

				self.setInterface();
			}
		});

		// Draggable Colors
		$('#ovcia-assemblies .ovc-draggable-color:not(.color-legend)').draggable( draggableOptions );

		$('#ovcia-assemblies .img-set-colors-droppable').droppable({
			accept 			: '.ovc-draggable-color',
			activeClass 	: 'droppable-active',
			hoverClass 		: 'droppable-hover',
			drop 			: function( event, ui ) {

				var imgSetID = $(this).data('img-set-id');
				var color = ui.draggable.data('color');
				var skusTD = $('#ovcia-assemblies tr.img-set-skus-row td[data-img-set-id="' + imgSetID + '"]');

				$('#ovcia-assemblies .ovc-draggable-sku[data-color="' + color + '"]').appendTo( $( skusTD ) );

				self.setInterface();
			}

		}); 
		/* */
	}

	OvcAssemblyManager.prototype = $.extend({}, OvcAssemblyManager.prototype, {
		
		getTopLevelAssemblies: function() {
			return lodash.filter( this.assemblies, function(assembly) {return assembly.isTopLevel();});
		}
	});

	return OvcAssemblyManager;
})();

// $(document).ready(function() {
// 	console.log('init assembly');
// 	ovc.assembly.init();
// });