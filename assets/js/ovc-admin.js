if ( typeof $ === 'undefined' ) {
	var $ = jQuery.noConflict();
}

window.ovc = window.ovc || {};

var ovcdtMain, oxmd, ovcdtLive = false;

function onyx_clone( variable ) {
	if ( typeof variable === 'object' && null !== variable ) {
		if( $.isArray( variable ) ) {
			return variable.slice();
		}
		else {
			return $.extend( true, {}, variable );
		}
	}
	return variable;
}




$(document).ready(function() {
	ovcdtMain = new ovcdtAdminPageHandler( $('#ovcdt-table-main'), true );
	

	ovcdtMain.setStatus( 'ready' );
	ovcdtMain.loadData();


});

function ovcAction(action,data) {
	$('body').trigger(action,data);
}



// Main ovcdt page handler object
function ovcdtAdminPageHandler( $outerWrap, isMainTable ) {

	if( typeof isMainTable === 'undefined' ) {isMainTable = false;}
	this.isMainTable = isMainTable;

	var self = this;

	this.$outerWrap = $outerWrap;
	//var $tableWrap = $outerWrap.find('.ovcdt-table-wrap');
	//this.$tableWrap = $tableWrap;

	// Attach instance to outerWrap so it can be found dynamically
	$outerWrap.data( 'ovcdtInstance', this );

	this.table 	= new ovcdtTable( self );

	// Disable clicks while data is loading
	// outerWrap.find('.ovcdt-table-wrap.loadingData *').click(function(e) {
	// 	e.preventDefault();
	// 	e.stopPropagation();
	// });


	this.results = {
		meta : {
			page_num : 1,
			per_page : 50,
			filters  : {},
			field_groups : [],
			scopes 		: [],
			forced_reload : false,
			cs_mode_enabled : false,
			top_scrollbar : true
		}
	};

	this.debug 	= false;
	this.ajaxStart = ''; // Used for timing requests

	this.noticeBox = ovcNoticeBox ? ovcNoticeBox : new OVC_noticeBox( $outerWrap.find('.ovc-notice-box').eq(0) );
	// if( !noticeBox ) {

	// }
	//this.noticeBox = new OVC_noticeBox( $outerWrap.find('.ovc-notice-box').eq(0) );
	this.oxmd_noticeBox = null;

	

	this.disableSave = false;
	this.saveDataArgs = [];
	this.checkOnlyOnSave = false; // Whether or not to actually save changes or just to simulate them without changing any info in the database

	this.user_cs_changed = false;

	this.frozenCols = {
		wrapOffset 	: 0,
		cols 		: {}
	};

	// These are the self.results.meta keys that affect the OVCDT Query (via loadData() )
	this.sqlMetaKeys = [ 'field_groups', 'filters', 'page_num', 'per_page' ];

	if( typeof( this.table.$tableWrap.data('request-defaults') ) !== 'undefined') {
		this.results.meta = this.table.$tableWrap.data('request-defaults');
	}

	// Maybe get table from query args
	if( oxpm.getQArg( 'ovcdt' ) ) {
		this.results.meta.type = oxpm.getQArg( 'ovcdt' );
	}
	
	this.status = 'init';


	this.setStatus = function( status ) {
		
		this.noticeBox.setStatus( status );

		if( 'loading' == status ) {
			self.table.$tableWrap.addClass('loadingData');
		}
		else if ( 'saving' == status ) {
			self.table.$tableWrap.addClass('savingData');
		}
		else {
			setTimeout( function() {self.table.$tableWrap.removeClass('loadingData').removeClass('savingData');}, 300);
		}

		self.status = status;
	};

	// load / reload / refresh OVCDT data
	// This function is not a prelim check function that calls the actual data-loading callback function if checks pass
	// Broken into 2 functions becuase it will use OXMD if there are pending changes
	this.loadData = function() {
		if( 'ready' != self.status && !self.results.meta.forced_reload ) {
			return;
		}
		else if ( self.saveDataArgs.length ) {
			var oxmd_loadDataConfirm = {
				type 		: "confirm",
				title 		: "Confirm Data Reload - Lose Unsaved Changes",
				callback 	: self.loadDataCB,
				elements 	: [
					( "Are you sure you want to reload the data?  Your pending changes will be lost." )
				]
			};
			oxmd.modal( oxmd_loadDataConfirm );
		}
		else {
			var response = {value:1}; // Create dummy OXMD callback response object
			self.loadDataCB(response);
		}
	};

	this.loadDataCB = function( response ) {
		if( response.value ) {
			self.setStatus( 'loading' );

			self.saveDataArgs = [];

			var args = onyx_clone( self.results.meta );

			// Maybe get field groups
			if( false !== args.field_groups ) {
				args.field_groups = [];
				$outerWrap.find('input.fg-select:checked').each(function(){
					args.field_groups.push( $(this).data( 'field-group' ) );
				});
			}
			
			var loadDataRequest = {
				request			: 'load_data',
				args			: args
			};

			self.ajaxRequest( loadDataRequest );
		}
		// Should only be able to get here if wait to save changes was on and user cancelled a table load (in order to not lose their changes)
		else {
			// Revert query meta args 
			self.metaFromURL();
		}
	};

	this.saveData = function() {
		if( self.disableSave || 'ready' != self.status || $outerWrap.find('.ovcdt-wait-to-save').prop('checked') ) {return false;}
		self.setStatus( 'saving' );

		var saveDataRequest = {
			request	: 'save_data',
			args	: {
				check_only		: self.checkOnlyOnSave,
				ovcdt_version 	: self.results.meta.ovcdt_version,
				data 			: self.saveDataArgs
			}
		};

		this.ajaxRequest( saveDataRequest );
	};

	this.viewUserCS = function() {
		self.table.clearFilters();
		self.results.meta.cs_mode_enabled = true;
		self.results.meta.page_num = 1;
		self.table.$tableElem.removeClass( 'cs-mode-disabled' );
		$outerWrap.find('#cs-view-filter').prop( 'checked', true );
		self.table.getFilters();
		self.loadData();
	};

	this.userCSRequest = function( action ) {

		var requestObj = {
			request : '',
			args 	: {}
		};

		switch( action ) {
			
			case 'save':

				requestObj.request = 'save_user_custom_selection';

				// Get selected rows
				$outerWrap.find('input.ovcdt-cs-toggle:not(#cs-view-filter)').each(function() {
					var rowID = parseInt( $(this).val() );
					var realValue = $(this).is(':checked') ? rowID : '';
					self.results.user_cs_ids[ rowID ] = realValue; 
				});

				requestObj.args.data = self.results.user_cs_ids;
			break;
			case 'clear':

				requestObj.request = 'clear_user_custom_selection';
				requestObj.args.data = {};
			break;
			case 'add_all':
			case 'set_all':
			case 'remove_all':
				
				requestObj.request = 'bulk_user_custom_selection_by_filters';

				requestObj.args = onyx_clone( self.results.meta );

				requestObj.args.bulk_action = action;
			break;

		}

		requestObj.args.check_only 		= self.checkOnlyOnSave;
		requestObj.args.ovcdt_version 	= self.results.meta.ovcdt_version;

		this.ajaxRequest( requestObj );
	};


	this.revertUserCS = function() {
		if( typeof self.results.user_cs_ids !== 'undefined' ) {
			for ( var id in self.results.user_cs_ids ) {
				var checked = id == self.results.user_cs_ids[ id ] ? true : false;
				self.table.$tableElem.find('input.ovcdt-cs-toggle[name="cs-toggle['+id+']').prop( 'checked', checked );
			}
			$outerWrap.find('#ovcdt_cs_save, #ovcdt_cs_revert').prop( 'disabled', true );
		}
	};

	this.addSaveRowDataObj = function( rowID ) {
		var rowData = {};
		self.table.$tableElem.find('tr[data-row-id='+rowID+'] .otdi.saving-cell').each(function() {
			var col = self.table.getCellCol( $(this) ); 
			rowData[ col ] = self.table.getCellValue( rowID, col );
		});

		var SaveRowDataObjExisted = false;
		for( var i = 0; i < self.saveDataArgs.length; i++ ) {
			if( rowID == self.saveDataArgs[ i ].where.ID ) {
				self.saveDataArgs[ i ].data = rowData;
				SaveRowDataObjExisted = true;
			}
		}

		if( !SaveRowDataObjExisted ) {
			self.saveDataArgs.push({
				type : 'save_row',
				data : rowData,
				where: { 'ID' : rowID }
			});
		}
		
	};

	this.addNewRowDataObj = function( rowData ) {
		var newRowArgs = {
			type : 'new_row',
			data : rowData,
			where: {}
		};

		self.saveDataArgs.push( newRowArgs );
	};

	this.addDeleteRowDataObj = function( rowID ) {
		self.saveDataArgs.push({
			type : 'delete_row',
			data : {},
			where: { 'ID' : rowID }
		});
	};

	this.addDuplicateRowDataObj = function( rowID ) {
		self.saveDataArgs.push({
			type : 'duplicate_row',
			data : {},
			where: { 'ID' : rowID }
		});
	};

	this.addAPIRefreshRowDataObj = function( rowID ) {
		self.saveDataArgs.push({
			type : 'api_refresh',
			data : {},
			where: { 'ID' : rowID }
		});
	};

	this.addCheckDataFixRowObj = function( rowID ) {
		self.saveDataArgs.push({
			type : 'check_fields_to_fix',
			data : {},
			where: { 'ID' : self.table.getCellValue( rowID, 'fix.ovc_id' ) }
		});
	};

	this.addDeleteImgOrphanDataObj = function( rowID ) {
		self.saveDataArgs.push({
			type : 'delete_img_orphan',
			data : {},
			where: { 'ID' : rowID }
		});
	};

	this.addDeleteImageObj = function( rowID ) {
		self.saveDataArgs.push({
			type : 'delete_image',
			data : {},
			where: { 'ID' : rowID }
		});
	};

	this.addDeleteOverrideImageObj = function( rowID ) {
		self.saveDataArgs.push({
			type : 'delete_override_image',
			data : {},
			where: { 'ID' : rowID }
		});
	};

	this.ajaxRequest = function( requestObj ) {
		if( ovcdtLive && 'ovcdt_ping' != requestObj.request ) {
			ovcdtLive.pausePing();
		}

		if( typeof self.results.meta.type === 'undefined' ) {
			requestObj.type 	= self.table.$tableWrap.data('ovcdt-type');	
		}
		else {
			requestObj.type 	= self.results.meta.type;
		}
		

		var ajax_data = {
			'ovc_ajax'	: true,
			'action' 	: 'ovc_ajax',
			'command'	: 'ovcdt',
			'ovcdt'		: requestObj
		};

		if( self.debug ) {console.log("AJAX Request Data: ");console.log( ajax_data );}

		var ajax_args = {
			url			: ajax_object.ajax_url,
			type		: "POST",
			data 		: ajax_data,
			cache		: false,
			dataType	: "json",
			complete	: function( response, status ) {
				if( self.debug ) {console.log('Raw AJAX Resonse:');console.log( response );}

				if ( 'success' == status ) {
					self.handleResponse( response.responseJSON );
				}
				else if ( 'timeout' == status ) {
			
					self.noticeBox.notice( 'AJAX Timeout :(', true );
				}
				else {
					self.setStatus( 'error' );
					self.noticeBox.notice('Error!', true );
					console.log( response );
				}

			}
		};

		self.ajaxStart = Date.now();
		$.ajax( ajax_args );
	};

	this.handleResponse = function( response ) {
		if( self.debug ) {
			console.log( "AJAX Response Data:" );
			console.log( "Request Time: " + ( Date.now() - self.ajaxStart ) );
			console.log( response );
		}
		console.log( "Request Time: " + ( Date.now() - self.ajaxStart ) ); // DEV

		if( typeof( response.request ) === 'undefined' ) {
			self.setStatus( 'error' );
			self.noticeBox.notice('Error! response.request is undefined.', true );
		}
		else {

			// Before processing request, check for version mismatch signal
			if( 'version_mismatch' == response.status ) {
				self.noticeBox.addNotices( response.data.result_sets );

				var oxmd_data = {
					type 		: "custom",
					callback 	: self.version_mismatch_callback,
					responses 	: [{label:"Reload Page",value:1,primary:true}],
					title 		: "OVC Data Table Version Mismatch",
					elements 	: [
						"You are not running the most recent version of the OVC Data Manager.  Please refresh the page to update to the latest version.",
						{innerElems:[{type:"p",attrs:{},content:('<span class="oxmd-label-span">Your Version:</span>'+response.data.request_version+'<br /><span class="oxmd-label-span">Current Version:</span>'+response.data.current_version)}]}
					]
				};

				oxmd.modal( oxmd_data );
			}

			switch( response.request ) {
				case 'load_data':
					self.results = response.data;
					//self.metaToURL();
					self.table.rebuildTable();
					self.user_cs_changed = false;
					//self.initLiveData(); // dev:ovcdt ping
					
					//self.setStatus( 'ready' );

					// Show notices unless this was a successful forced_reload
					if ( !( 'success' == response.status && self.results.meta.forced_reload ) ) {
						self.noticeBox.addNotices( response.data.result_sets );

						if( 'ovc' == oxpm.getQArg( 'page' ) ) {
							// Update document title with table nice_name prefix
							var table_name = $('.ovcdt-table-select option[value="'+response.data.meta.type+'"]').html();
							document.title = table_name + ' - OVC Product Data Manager';	
						}
					}

					// Update the query arg to track what table we're on
					oxpm.setQArg( 'ovcdt', self.results.meta.type );
					
					setTimeout( function() {self.table.$tableElem.removeClass('loadingData');}, 500); //Make sure this gets removed in case statuses are not being used on this page
				break;
				case 'save_data':
					var resultClass = ( 'success' == response.status ? 'save-success force-transition-3s' : 'save-fail force-transition-9s' );
					var transitionTimer = ( 'success' == response.status ? 3200 : 9300 );

					if( 'success' == response.status ) {
						self.table.updateFromResponseData( response.data, resultClass, false );
						self.saveDataArgs = [];
					}
					else if( 'fail' == response.status ) {
						self.table.revertAllChanges( resultClass );
					}
					
					setTimeout( function() {$outerWrap.find('.otdi.save-success, .otdi.save-fail').removeClass('save-success save-fail');}, 300);
					
					setTimeout( function() {$outerWrap.find('.force-transition-3s, .force-transition-9s').removeClass('force-transition-3s force-transition-9s');}, transitionTimer);

					self.noticeBox.addNotices( response.data.result_sets );

				break;
				case 'ovcdt_ping':
					self.table.updateFromResponseData( response.data, 'live-updated force-transition-3s', true );
					setTimeout( function() {$outerWrap.find('.live-updated').removeClass('live-updated');}, 500);
					setTimeout( function() {$outerWrap.find('.force-transition-3s, .force-transition-9s').removeClass('force-transition-3s force-transition-9s');}, 3200);
				break;
				case 'upload_override_image':
				case 'save_productmeta':
				case 'pricing_helper_save':
				case 'edit_productmeta_key':
				case 'save_user_custom_selection':
				case 'clear_user_custom_selection':
				case 'bulk_user_custom_selection_by_filters':
					self.noticeBox.addNotices( response.data.result_sets );

					if( 'success' != response.status ) {
						self.table.revertAllChanges( resultClass );
					}

				break;
				case 'save_productimages': // dev:improve
					self.noticeBox.addNotices( response.data.result_sets );
					
				break;
				case 'load_actions_menu':

					self.table.loadActionsMenu( response.data );
				break;
				default:
					self.setStatus( 'error' );
					self.noticeBox.notice('Error! Invalid request type in response: ' + response.request, true );
				break;
			}

			self.results.meta.forced_reload = false;
			if( response.data.force_reload ) {
				self.results.meta.forced_reload = true;
				self.loadData();
			}
			else if( ovcdtLive && ovcdtLive.paused ) {
				ovcdtLive.resumePing( 10000 );
			}

			if ( !response.data.force_reload && 'error' != self.status ) {
				self.setStatus( 'ready' );
			}
			
		}
	};

	this.initLiveData = function() {
		if( false !== ovcdtLive || typeof self.results.meta.live_data === 'undefined' || !self.results.meta.live_data ) {
			return false;
		}
		
		ovcdtLive = new ovcdtLiveData();
		$(document).idle({
			onIdle: function() {
				ovcdtLive.setStateIdle();
			},
			onActive: function() {
				ovcdtLive.setStateActive();
			},
			onHide: function() {
				ovcdtLive.setStateIdle();
			},
			onShow: function() {
				ovcdtLive.setStateActive();
			},
			startAtIdle: true,
			idle:60000
		});
	};

	this.version_mismatch_callback = function() {
		location.reload( true );
	};


	// This function updates the 'ovcdt' URL query param based on relevant keys from self.results.meta
	// NOTE: This function DOES NOT update interface values (e.g. filters, field_groups, pagination )
	this.metaToURL = function() {
		
		if( !self.isMainTable ) {
			return null;
		}

		var ovcdtQArg = {};
		var key;

		for ( var i = 0; i < self.sqlMetaKeys.length; i++ ) {
			key = self.sqlMetaKeys[ i ];

			ovcdtQArg[ key ] = onyx_clone( self.results.meta[ key ] );

		}

		oxpm.setQArg( 'ovcdt', ovcdtQArg );
	};

	// This function populates relevant self.results.meta keys from the 'ovcdt' URL query param (if it exists)
	// NOTE: This DOES update interface values, because those are the values used by loadData() (e.g. filters, field_groups, pagination )
	this.metaFromURL = function() {

		if( !self.isMainTable ) {
			return null;
		}

		if( oxpm.keyExists( 'ovcdt' ) ) {

			var ovcdtQArg = oxpm.getQArg( 'ovcdt' );

			for ( var key in ovcdtQArg ) {
				if ( self.sqlMetaKeys.indexOf( key ) > -1 ) {
					self.results.meta[ key ] = onyx_clone( ovcdtQArg[ key ] );
				}
			}

			// UPDATE INTERFACE VALUES
			if( 'init' != self.status ) {

				$outerWrap.find('select.ovcdt-per-page-select').val( self.results.meta.per_page ); // per_page

				$outerWrap.find('input.ovcdt-page-select').val( self.results.meta.page_num ); // page_num

				if( typeof self.results.meta.filters === 'object' ) { // filters
					for ( var filterField in self.results.meta.filters ) {
						if( self.results.meta.filters.hasOwnProperty( filterField ) ) {
							// dev:bool_handling
							$outerWrap.find('.otdi--filter[data-filter-field="'+filterField+'"]').val( self.results.meta.filters[ filterField ] );
						}
					}
				}

				if ( $.isArray( self.results.meta.field_groups ) ) { // field_groups
					$outerWrap.find('input.fg-select:not(:disabled)').each(function() {
						var checked = self.results.meta.field_groups.indexOf( $(this).val() ) > -1 ? true : false;
						$(this).prop( 'checked', checked );
					});
				}
			}
		}
	};

	this.setScope = function( scope, clearScopes ) {
		if( clearScopes ) {
			self.results.meta.scopes = [];
		}

		self.results.meta.scopes.push( scope );

		self.loadData();

	};

	this.changeType = function( newType ) {

		self.results = {
			meta : {
				type 	: newType,
				page_num : 1,
				per_page : 50,
				filters  : {},
				field_groups : [],
				scopes : [],
				forced_reload : false,
				cs_mode_enabled : false,
				ovcdt_version 	: self.results.meta.ovcdt_version
			}
		};

		self.table.$tableWrap.attr('data-ovcdt-type', newType ).data( 'ovcdt-type', newType );

		self.loadData();
	};


	// Single-set event handlers (table select)
	$('.ovcdt-table-select').change(function() {
		ovc.screen.hideScreen();
		self.changeType( $(this).val() );
	});

	// Initialization
	self.metaFromURL();



	// Maybe deep-link to ovc_screen 
	if( self.isMainTable && oxpm.getQArg('screen') && oxpm.getQArg('screenid') ) {

		ovc.screen.loadScreen(oxpm.getQArg('screen'), {ovc_id:oxpm.getQArg('screenid')});
	}


}


function ovcdtTable( ovcdt ) {

	var self = this;
	this.$tableWrap = null;
	this.$tightWrap = null;
	this.$tableElem = null;
	this.$topScroll = null;

	//this.DOM = function() {return ovcdt.$tableWrap;};
	//this.tightWrap = function() {return self.DOM().children('.ovcdt-tight-wrap').eq(0);};
	//this.tableElem = function() {return self.DOM().find('table.ovcdt-table');};

	this.initElems = function() {

		self.$tableWrap = ovcdt.$outerWrap.find('.ovcdt-table-wrap');
		self.$tightWrap = self.$tableWrap.children('.ovcdt-tight-wrap').eq(0);
		self.$tableElem = self.$tableWrap.find('table.ovcdt-table');
		self.$topScroll = self.$tableWrap.children('.ovcdt-top-scrollbar');
	};

	this.initElems();

	

	this.rebuildTable = function() {
		// Record scrollLeft
		var scrollLeft = 0;
		if ( self.$tightWrap.length ) {
			scrollLeft = self.$tightWrap.scrollLeft();
		}
		
		// Place HTML
		self.$tableWrap.empty().append( ovcdt.results.html );

		// Re-initialize
		self.initElems();

		// Init Scrollbar (used for top scrollbar)
		self.initScrollbars();

		// Set event handlers
		this.setEventHandlers();

		// Maybe set scrollLeft
		if ( self.$tightWrap.length ) {
			self.$tightWrap.scrollLeft( scrollLeft );

			if( self.$topScroll.length ) {
				self.$topScroll.scrollLeft( scrollLeft );	
			}
		}

			
	};

	this.initScrollbars = function() {




		if( self.$tightWrap.width() < self.$tableElem.width() ) {
			
			self.$tableWrap.addClass('ovcdt--scrollable');

			// Set width of dummy content div
			if( self.$topScroll.length ) {
				self.$topScroll.children('.ovcdt-dummy-content').width( self.$tableElem.width() );
			}


			
		}
		else {
			self.$tableWrap.removeClass('ovcdt--scrollable');
		}
	};

	this.setEventHandlers = function() {
		// Set scroll event handlers
		if( self.$topScroll.length ) {
			self.$tightWrap.scroll(function() {
				self.$topScroll.scrollLeft( $(this).scrollLeft() );
			});

			self.$topScroll.scroll(function() {
				self.$tightWrap.scrollLeft( $(this).scrollLeft() );
			});
		}
		

		$(window).resize(function() {
			self.initScrollbars();
		});

		// Simple pagination links
		self.$tableWrap.find('a.ovcdt-pagination-link').click(function(e) {
			e.preventDefault();
			e.stopPropagation();

			var paginationAction = $(this).data('goto');

			if 		( 'first'== paginationAction ) {ovcdt.results.meta.page_num=1;}
			else if ( 'prev' == paginationAction ) {ovcdt.results.meta.page_num--;}
			else if ( 'next' == paginationAction ) {ovcdt.results.meta.page_num++;}
			else if ( 'last' == paginationAction ) {ovcdt.results.meta.page_num = ovcdt.results.meta.total_pages;}
			ovcdt.$outerWrap.find('input.ovcdt-page-select').val( ovcdt.results.meta.page_num );
			ovcdt.loadData();
		});

		// Keep values of results per page selectors synced on top & bottom of table
		self.$tableWrap.find('select.ovcdt-per-page-select').change(function() {
			ovcdt.$outerWrap.find('select.ovcdt-per-page-select').val( $(this).val() );
			ovcdt.results.meta.page_num = $(this).val();
		});

		// Keep values of page number selectors synced on top & bottom of table
		self.$tableWrap.find('input.ovcdt-page-select').change(function() {
			ovcdt.$outerWrap.find('input.ovcdt-page-select').val( $(this).val() );
		});

		// Enter KeyPresses that cause refresh data ( on Filters and page number selector )
		self.$tableWrap.find('.otdi--filter').keypress( function(e) {
			if( 13 == e.which ) {
				$(this).blur();
				ovcdt.loadData();
			}
		});

		self.$tableWrap.find('.otdi--filter, input#cs-view-filter').blur( function(e) {
			self.getFilters();
		});

		self.$tableWrap.find('input.ovcdt-page-select').blur( function(e) {
			ovcdt.results.meta.page_num = $(this).val();
			ovcdt.loadData();
		});

		// Refresh data
		self.$tableWrap.find('input.ovcdt-per-page-refresh').click(function() {
			var new_per_page = ovcdt.$outerWrap.find('select.ovcdt-per-page-select').eq(0).val();

			ovcdt.results.meta.page_num = Math.floor( ovcdt.results.meta.offset / new_per_page ) + 1;
			ovcdt.results.meta.per_page = new_per_page;
			ovcdt.loadData();
		});

		// Field Groups
		self.$tableWrap.find('input.fg-select').change( function() {
			var checked = $(this).prop('checked');
			var arrIndex = ovcdt.results.meta.field_groups.indexOf( $(this).data( 'field-group' ) );

			if( $(this).hasClass('fg-select-all') ) {
				$('input.fg-select:not([disabled])').prop('checked',checked);
			}
			else if( checked && arrIndex == -1 ) {
				ovcdt.results.meta.field_groups.push( $(this).data( 'field-group' ) );
			}
			else if ( !checked && arrIndex > -1 ) {
				ovcdt.results.meta.field_groups = ovcdt.results.meta.field_groups.slice( arrIndex, 1 );
			}

		});

		// EDIT AND SAVE CELL VALUE & MAYBE CALC FORMULAS
		self.$tableWrap.find('.otdi:not(.otdi--filter)')
		.focus( function(e) {
			$(this).removeClass('force-transition-3s force-transition-9s');
		})
		.click( function(e) {
			if( $(this).prop( 'readonly') ) { // Auto-select contents of disabled cells
				$(this).select();
			}
		})
		.dblclick( function(e) {
			if( 'ready' == ovcdt.status && 'ovc_products' == ovcdt.results.meta.type ) {
				var col = self.getCellCol( $(this) );
				var field = col.split('.');
				
				if( 'pa' === field[0] ) {
					ovc.screen.loadScreen('screen_parent_data',{ovc_id:self.getCellRowID($(this))});
				}
				else if ( 'st' === field[0] ) {
					ovc.screen.loadScreen('screen_style_data',{ovc_id:self.getCellRowID($(this))});	
				}
			}
		})
		.keyup( function(e) {
			//if( ovcdt.debug ) {console.log( e );}			

			if( 27 == e.which ) { // Escape key on data cell input
				if( !$(this).prop('readonly') && !$(this).hasClass('saving-cell') ) { // If not disabled & not saving cell, revert the cell
					self.revertCell( self.getCellRowID( $(this) ), self.getCellCol( $(this) ) );
				}
				//$(this).blur();	
			}
		})
		.change( function() {
			if( !self.$tableWrap.find('table.ovcdt-table').hasClass('read-only') && $(this).is(':valid') && !$(this).hasClass('ovcdt-cs-toggle') ) {
				self.saveCellLock( $(this) );

				var field = self.getCellCol( $(this) );
				var rowID = self.getCellRowID( $(this) );

				// Maybe Calc Formulas
				// Do not update formulas if the change event was fired on a 'disabled' element
				if( !self.getCellElem( rowID, field ).prop('readonly') ) {
					self.calcFormulas( rowID, field );
				}

				// Execute any special handling for this field if it has it. Conditions: 1) check that there is any special handling present 2) Check if there is special handling for this field 3) Check that this field's special handling is an existing function
				if( ovcdt.results.hasOwnProperty('special_handling') && ovcdt.results.special_handling.hasOwnProperty( field ) ) {
					
					

					// Check for vertical match requirement special handling
					if( ovcdt.results.special_handling[ field ].hasOwnProperty('shared_data_key') ) {
						editSharedProductDataKey( rowID, field, ovcdt );
						return; // Vertical Update Handler handles whether to complete normal saveCellChanges or not
					}


				}

				self.saveCellChanges( rowID, field );
			}
		});

		// Up and Down arrows on inputs
		self.$tableWrap.find('.otdi').keydown(function(e) {

			if( 38 == e.which || 40 == e.which ) { // Up/Down Arrow Keys respectively
				e.preventDefault();

				var delta = 38 == e.which ? 'prev' : 'next';
				
				var currentRow = self.$tableWrap.find('tr[data-row-id=' + self.getCellRowID( $(this) ) + ']');

				var newRow = currentRow.eq(0)[ delta ]();

				if( newRow.length ) {
					self.getCellElem( newRow.eq(0).data('row-id'), self.getCellCol( $(this) ) ).eq(0).focus().select();
				}

				
			}
		});

		// Custom Selection Event Handlers
		if( self.$tableWrap.find('#ovcdt_cs_enable').length ) {
			self.$tableWrap.find('input.ovcdt-cs-toggle:not(#cs-view-filter)').change(function() {

				self.$tableWrap.find('input.ovcdt-cs-toggle:not(#cs-view-filter)').each(function() {
					var rowID = parseInt( $(this).val() );
					var realValue = $(this).is(':checked') ? rowID : '';
					if( ovcdt.results.user_cs_ids[ rowID ] != realValue ) {
						ovcdt.user_cs_changed = true;
						return false;
					}
				});
				
				self.$tableWrap.find('#ovcdt_cs_save, #ovcdt_cs_revert').prop( 'disabled', !ovcdt.user_cs_changed );
			});

		
			self.$tableWrap.find('#ovcdt_cs_enable').click( function() {
				var checked = $(this).is(':checked');
				self.$tableElem.toggleClass( 'cs-mode-disabled', !checked );
				ovcdt.results.meta.cs_mode_enabled = checked;

				// Recalc freeze panes values when frozen columns are hidden / shown
				self.calcFrozenCols();
				self.setFrozenCols();
			});
		}

		self.$tableWrap.find('.ovcdt-wait-to-save').change( function() {
			if( !$(this).prop('checked') && ovcdt.saveDataArgs.length ) {
				ovcdt.saveData();
			}
		});

		if( self.$tableWrap.find('.ovcdt-show-in-use-imgs').length ) {

			// Add ovc-img-in-use class to rows that have already been checked
			self.$tableWrap.find('td[data-col="unimgs.in_use"] input.otdi').each(function() {
				var checked = $(this).is(':checked');
				$(this).closest('tr').toggleClass('ovc-img-in-use',checked);
			});

			self.$tableWrap.find('.ovcdt-show-in-use-imgs').change( function() {
				var checked = $(this).is(':checked');
				self.$tableElem.toggleClass( 'show-in-use-imgs-disabled', !checked );
			});

			self.$tableWrap.find('td[data-col="unimgs.in_use"] input.otdi').change( function() {
				var checked = $(this).is(':checked');
				$(this).closest('tr').toggleClass('ovc-img-in-use',checked);
			});
		}

		self.$tableWrap.find('a[disabled="disabled"]').click( function(e) {
			e.preventDefault();
			e.stopPropagation();
		});

		// Header clicks (Sorting)
		self.$tableWrap.find('tr.ovcdt-headers th').click( function() {
			if( $(this).data('col') ) {
				var direction = ( $(this).hasClass('sort-asc') ? 'DESC' : 'ASC' );

				ovcdt.results.meta.sorting = $(this).data('col') + " " + direction;
				ovcdt.loadData();
			}
		});

		// ROW ACTIONS
		self.$tableWrap.find('td.actions').hover( 
			function(event) {
				$('td.actions .oxmn-submenu').empty().addClass('oxmn-force-hide oxmn-loading');
				
				var submenuElem = $(this).find('.oxmn-submenu');

				submenuElem.addClass('oxmn-hover');

				self.positionActionsSubmenu( submenuElem );

				submenuElem.removeClass('oxmn-force-hide');

				var loadActionMenuRequest = {
					request	: 'load_actions_menu',
					args	: {
						check_only		: ovcdt.checkOnlyOnSave,
						ovcdt_version 	: ovcdt.results.meta.ovcdt_version,
						data 			: {
							rowID 			: self.getCellRowID( $(this) )
						}
					}
				};

				ovcdt.ajaxRequest( loadActionMenuRequest );
			},
			function(event) {
				$(this).find('.oxmn-submenu').addClass('oxmn-force-hide oxmn-loading').removeClass('oxmn-hover oxmn-fixed-positioned').empty();
			}
		);

		// Scope toggle buttons
		self.$tableWrap.find('.ovcdt-setscope').click(function() {

			if( $(this).hasClass( 'ovcdt-clearscopes' ) ) {
				self.$tableWrap.find('.ovcdt-clearscopes').not($(this)).prop('checked', false);	
			}

			var scopes = [];

			self.$tableWrap.find( '[data-set-scope]' ).each(function() {
				if( $(this).prop('checked') ) {
					scopes.push( $(this).data('set-scope') );
				}
			});
			
			ovcdt.results.meta.scopes = scopes;

			ovcdt.loadData();
		});

		self.positionActionsSubmenu = function( submenuElem ) {

			var tdElem = submenuElem.closest('td.actions');

			//var tableElem = 

			submenuElem.css({ top: tdElem.offset().top + tdElem.outerHeight() - window.scrollY, left: tdElem.offset().left}).addClass('oxmn-fixed-positioned');
		};

		// Action button handler functions
		this.getRowActionsWrap = function( rowID ) {
			return self.$tableWrap.find('tr[data-row-id='+rowID+'] td.actions .oxmn-submenu');
		};

		this.loadActionsMenu = function( responseData ) {
			var actionsMenu = self.getRowActionsWrap( responseData.actions_menu_row_id );

			if( actionsMenu.hasClass('oxmn-hover') ) {

				actionsMenu.empty().removeClass('oxmn-force-hide').append( responseData.actions_menu_html ).removeClass('oxmn-loading');
			}
		};

		self.$tableWrap.find('td.actions .oxmn-submenu').on( 'click', '.oxmn-li', function(event) {

			var oxmnLi = $(event.target).parent();

			if( !oxmnLi.hasClass('oxmn-disabled') && 'ready' == ovcdt.status ) {

				var rowID = self.getCellRowID( $(this) );
				var rowAction = oxmnLi.data('row-action');
				var actionData = {
					rowID 	: rowID
				};
				
				switch( rowAction ) {
					case 'duplicate_row':
						var oxmd_duplRow_data = {
							type 		: "confirm",
							title 		: "Confirm Duplicate Row",
							passData 	: actionData,
							callback 	: self.duplicateRowCB,
							elements 	: [
								( "Are you sure you want to duplicate this Row?" ),
								( '<span class="oxmd-label-span">Row ID:</span>' + rowID )
							]
						};
						oxmd.modal( oxmd_duplRow_data );
					break;
					case 'delete_row':
						var oxmd_deleteRow_data = {
							type 		: "confirm",
							title 		: "Confirm Delete Row",
							passData 	: actionData,
							callback 	: self.deleteRowCB,
							elements 	: [
								( "You are about to <b>PERMANENTLY DELETE</b> this row.<br />Are you SURE you want to do this?" ),
								( '<span class="oxmd-label-span">ID:</span>' + rowID +'<br />' )
							]
						};
						oxmd.modal( oxmd_deleteRow_data );
					break;
					case 'view_operation':
						ovcop.viewOp( rowID );
					break;
					case 'reset_operation':
						ovcop.resetOp( rowID );
					break;
					case 'abort_operation':
						ovcop.abortOp( rowID );
					break;
					case 'commence_operation':
						var opType = self.getCellValue( rowID, 'ol.type' );
						ovcop.commenceOperation( rowID, opType );
					break;
					case 'api_refresh':
						ovcdt.addAPIRefreshRowDataObj( rowID );
						ovcdt.saveData();
					break;
					case 'check_data_fix':
						ovcdt.addCheckDataFixRowObj( rowID );
						ovcdt.saveData();
					break;
					case 'price_formula_tool':
						priceHelper( rowID, ovcdt );
					break;
					case 'edit_product_details':
						sharedProductData( rowID, ovcdt );
					break;
					// Screen Load Functions
					case 'screen_upload_image':
					case 'screen_shared_data':
					case 'screen_category_data':
					case 'screen_parent_data':
					case 'screen_style_data':
					case 'screen_inventory_assembly_data':
					case 'screen_amazon_style_data':
					case 'screen_image_manager':
						ovc.screen.loadScreen(rowAction, {ovc_id:rowID});
					break;
					case 'product_image_manager':
						productImageManager( rowID, ovcdt );
					break;
					case 'delete_img_orphan':
						ovcdt.addDeleteImgOrphanDataObj( rowID );
						ovcdt.saveData();
					break;
					case 'delete_image':
						var oxmd_delete_image = {
							type 		: 'confirm',
							title 		: 'Confirm Delete Image',
							passData	: actionData,
							callback 	: self.deleteImageCB,
							elements 	: [
								( 'You are about to <b>PERMANENTLY DELETE</b> this image.<br />Are you SURE you want to do this?' ),
								( '<span class="oxmd-label-span">ID:</span>' + rowID +'<br />' )
							]
						};
						oxmd.modal( oxmd_delete_image );
					break;
					case 'delete_override_image':
						var oxmd_deleteOverride_image = {
							type 		: 'confirm',
							title 		: 'Confirm Delete Override Image',
							passData 	: actionData,
							callback 	: self.deleteOverrideImageCB,
							elements 	: [
								( 'You are about to <b>PERMANENTLY DELETE</b> this image.<br />Are you SURE you want to do this?' ),
								( '<span class="oxmd-label-span">ID:</span>' + rowID +'<br />' )
							]
						};
						oxmd.modal( oxmd_deleteOverride_image );
					break;
					case 'regenerate_image':
					case 'sync_variant':
					case 'force_shopify_sync':
					case 'reset_shopify_images':
					case 'shopify_recheck_images':
					case 'resolve_error':
					case 'check_error':
						ovcdt.saveDataArgs.push({
							type : rowAction,
							data : {},
							where: { 'ID' : rowID }
						});
						ovcdt.saveData();
					break;
					case 'show_feed_item_errors':
					//else if( $(this).hasClass('action-show_item_errors') ) {

						
						var oxmd_feedItemErrors_data = {
							type 		: "custom",
							modalClass 	: "super-wide",
							passData 	: { walmart_product_id : rowID },
							responses 	: [
								{label:"Close",value:0}
							],
							title 		: "View Item Errors",
							elements 	: [{
								template	: 'ovc-api-item-errors',
								requestData : { walmart_product_id : rowID }
							}]
						};
						oxmd.modal( oxmd_feedItemErrors_data );
					break;
					case 'show_feed_errors':
					//else if( $(this).hasClass('action-show_feed_errors') ) {
						
						var oxmd_feedErrors_data = {
							type 		: "custom",
							modalClass 	: "super-wide",
							passData 	: { api_feed_id : rowID },
							responses 	: [
								{label:"Close",value:0}
							],
							title 		: "View Feed Errors",
							elements 	: [{
								template	: 'ovc-api-feed-errors',
								requestData : { api_feed_id : rowID }
							}]
						};
						oxmd.modal( oxmd_feedErrors_data );
					break;
					default:
						console.log("INVALID data-row-action at row " + rowID );
					break;
				}	
			}
			
			
		});

		



		/*
		self.$tableWrap.find('td.actions i').click( function() {
			if( 'ready' == ovcdt.status ) {
				var actnData = {
					rowID 	: self.getCellRowID( $(this) )
				};
				actnData.sku = self.getCellValue( actnData.rowID, self.fieldAsOVCDT( 'sku' ) );

				if ( $(this).hasClass('fa-files-o') ) {
					var oxmd_duplRow_data = {
						type 		: "confirm",
						title 		: "Confirm Duplicate OVC Product",
						passData 	: actnData,
						callback 	: self.duplicateRowCB,
						elements 	: [
							( "Are you sure you want to duplicate this OVC Product?" ),
							( '<span class="oxmd-label-span">OVC ID:</span>' + actnData.rowID +'<br /><span class="oxmd-label-span">OVC SKU:</span>' + actnData.sku )
						]
					};
					oxmd.modal( oxmd_duplRow_data );
				}
				else if ( $(this).hasClass('fa-times') && !$(this).hasClass('disabled') ) {
					var oxmd_deleteRow_data = {
						type 		: "confirm",
						title 		: "Confirm Delete Row",
						passData 	: actnData,
						callback 	: self.deleteRowCB,
						elements 	: [
							( "You are about to <b>PERMANENTLY DELETE</b> this row.<br />Are you SURE you want to do this?" ),
							( '<span class="oxmd-label-span">ID:</span>' + actnData.rowID +'<br />' )
						]
					};
					oxmd.modal( oxmd_deleteRow_data );
				}
				else if( $(this).hasClass('api-refresh') ) {
					ovcdt.addAPIRefreshRowDataObj( self.getCellRowID( $(this) ) );
					ovcdt.saveData();
				}
				else if( $(this).hasClass('check-data-fix') ) {
					ovcdt.addCheckDataFixRowObj( self.getCellRowID( $(this) ) );
					ovcdt.saveData();
				}
				else if ( $(this).hasClass('action-price_tool') ) {
					priceHelper( self.getCellRowID( $(this) ), ovcdt );
				}
				else if ( $(this).hasClass('action-product_type_data' ) ) {
					sharedProductData( self.getCellRowID( $(this) ), ovcdt );
				}
				else if ( $(this).hasClass('action-product_images') ) {
					productImageManager( self.getCellRowID( $(this) ), ovcdt );
				}
				else if( $(this).hasClass('action-show_item_errors') ) {

					
					var oxmd_data = {
						type 		: "custom",
						modalClass 	: "super-wide",
						passData 	: { walmart_product_id : actnData.rowID },
						responses 	: [
							{label:"Close",value:0}
						],
						title 		: "View Item Errors",
						elements 	: [{
							template	: 'ovc-api-item-errors',
							requestData : { walmart_product_id : actnData.rowID }
						}]
					};
					oxmd.modal( oxmd_data );
				}
				else if( $(this).hasClass('action-show_feed_errors') ) {
					
					var oxmd_data = {
						type 		: "custom",
						modalClass 	: "super-wide",
						passData 	: { api_feed_id : actnData.rowID },
						responses 	: [
							{label:"Close",value:0}
						],
						title 		: "View Feed Errors",
						elements 	: [{
							template	: 'ovc-api-feed-errors',
							requestData : { api_feed_id : actnData.rowID }
						}]
					};
					oxmd.modal( oxmd_data );
				}
			}

		});
		*/
			

		// FROZEN COLUMNS (FREEZE PANES)
		if( self.$tableWrap.find('th.freeze-pane').length ) {

			self.calcFrozenCols();

			self.$tightWrap.scroll(function() {

				self.setFrozenCols();
			});
		}
	};

	this.getFilters = function() {
		var filters = {};

		// Ensure that old filters are an object for comparison
		ovcdt.results.meta.filters = ( ovcdt.results.meta.filters === null ? {} : ovcdt.results.meta.filters );

		
		
		self.$tableWrap.find('.otdi--filter').each(function() {
			var filterField = $(this).data('filter-field');

			// Get Filter value
			var val = $(this).val();

			if( $(this).hasClass('otdi--bool') ) {
				val = $(this).prop( 'checked' ) ? 1 : 0;
			}

			if( val ) {
				if( ovcdt.debug ) {console.log( "Filter: " + filterField + " = " + val );}
				filters[ filterField ] = val;
			}

			// Set page number to one if filters change
			if( ovcdt.results.meta.filters[ filterField ] != filters[ filterField ] ) {
				if( ovcdt.debug ) {console.log( 'Filter Change for ' + filterField + ' : (current then new)');console.log( ovcdt.results.meta.filters[ filterField ] );console.log( filters[ filterField ] );}
				ovcdt.results.meta.page_num = 1;
			}
		});

		// Merge any forced filters
		if( typeof ovcdt.results.meta.forced_filters === 'object' ) {
			$.extend( filters, ovcdt.results.meta.forced_filters );
		}

		// Custom Selection Filter Handling
		if( ovcdt.results.meta.cs_mode_enabled && self.$tableWrap.find('#cs-view-filter').is(':checked') ) {
			filters[ 'cs-view' ] = true;
		}

		ovcdt.results.meta.filters = filters;
	};

	this.clearFilters = function() {
		self.$tableWrap.find('.otdi--filter').val('');
		self.$tableWrap.find('.otdi--filter.otdi--bool, input#cs-view-filter').prop( 'checked', false );
		ovcdt.results.meta.filters = {};
	};

	//dev:img_orphans
	this.showImgs = function() {
		$('tbody td[data-col="ior.guid"]').each(function() {

			var input = $(this).children('input.otdi').eq(0);

			var img_src = input.val();

			input.hide();


			var img_html = '<img src="' + img_src + '" style="height:50px;width:auto;" />';

			$(this).append(img_html);
		});
	};

	// Frozen Columns Functions
	this.calcFrozenCols = function() {
		ovcdt.frozenCols = {
			wrapOffset 	: self.$tightWrap.offset().left,
			cols 		: {}
		};

		// Initialize frozen columns data
		self.$tableWrap.find('th.freeze-pane').each( function() {

			if( 'none' !== $(this).css('display') ) {

				var col = $(this).data( 'col' );	

				ovcdt.frozenCols.cols[ col ] = $(this);

				var tableOffset = 0;
				var colIndex = $(this).index();

				$(this).siblings().each(function( index, element ) {
					if( index >= colIndex ) {
						return false;
					}
					else if( 'none' !== $( element ).css('display') ) {
						tableOffset += $( element ).outerWidth();	
					}

					
				});

				ovcdt.frozenCols.cols[ col ].data( 'table-offset', tableOffset );
			}
		});
	};

	this.setFrozenCols = function() {

		var tableScroll = self.$tightWrap.scrollLeft();
		var frozenColsWidth = 0;

		for ( var col in ovcdt.frozenCols.cols ) {

			var th_elem = ovcdt.frozenCols.cols[ col ];
			
			var colOrigOffset = th_elem.data( 'table-offset' );

			if( tableScroll > ( colOrigOffset - frozenColsWidth ) ) {
				
				self.$tableWrap.find('[data-col="' + col + '"]').addClass('frozen').css('transform', 'translate3d(' + ( tableScroll - colOrigOffset + frozenColsWidth ) + 'px,0,0)' );
				frozenColsWidth += th_elem.width() + 4;
			}
			else if( th_elem.hasClass('frozen') ) {
				
				self.$tableWrap.find('[data-col="' + col + '"]' ).removeClass('frozen').css('transform', 'translate3d(0,0,0)' );
			}
		}
	};




	this.duplicateRowCB = function( response ) {
		var actnData = response.passData;

		if( response.value ) {
			ovcdt.addDuplicateRowDataObj( actnData.rowID );
			ovcdt.saveData();
		}
	};

	this.deleteRowCB = function( response ) {
		var actnData = response.passData;

		if( response.value ) {
			ovcdt.addDeleteRowDataObj( actnData.rowID );
			ovcdt.saveData();
		}
	};

	this.deleteImageCB = function( response ) {
		var actnData = response.passData;

		if( response.value ) {
			ovcdt.addDeleteImageObj( actnData.rowID );
			ovcdt.saveData();
		}
	};

	this.deleteOverrideImageCB = function( response ) {
		var actnData = response.passData;

		if( response.value ) {
			ovcdt.addDeleteOverrideImageObj( actnData.rowID );
			ovcdt.saveData();
		}
	};


	// Fieldmeta calc_formula handler
	this.calcFormulas = function( rowID, changedField ) {
		if( typeof( changedField ) === 'undefined' ) {changedField = false;}
		if( !ovcdt.results.hasOwnProperty('formulas') ) {return false;} // Leave if this OVCDT doesn't have formulas to calc
		var row = self.$tableWrap.find('tr[data-row-id='+rowID+']');

		// Loop through and calculate formulas
		for( var targetField in ovcdt.results.formulas ) {
			// Conditions:
			// 	1) Verify that this property of the formulas object is a formula and not some system property
			// 	2) Ignore the formula if it does not contain a reference to the field that was changed
			if( ovcdt.results.formulas.hasOwnProperty( targetField ) && -1 != ovcdt.results.formulas[ targetField ].indexOf( changedField ) ) { 
				var formula = ovcdt.results.formulas[ targetField ];
				var numeric = ( '::=' == formula.substr( 0, 3 ) ? true : false ); // Detect if this formula should be evaluated numerically or as a string (numeric formulas begin with '=')
				formula = ( numeric ? formula.substr( 3 ) : formula ); // Remove the '=' prefix if numeric

				var exploded = formula.split(/(:{2}\[[a-zA-Z0-9_:.]+\])/); // Break the formula into pieces in order to separate the vars

				// Replace cell references with values
				for( var i = 0; i < exploded.length; i++ ) {
					if( '::[' == exploded[i].substr( 0, 3 ) ) {
						exploded[i] = self.getCellValue( rowID, exploded[i].slice( 3, -1 ) );
					}
				}
				formula = exploded.join('');

				new_value = ( numeric ? eval( formula ) : formula ); // jshint ignore:line

				if( Number( new_value ) === new_value && new_value % 1 !== 0 ) { // Maybe fix floating point rounding errors
					// Determine number of decimals to round based on 'step' attribute of targetField (default is 2) // dev:generalize!
					var decimals = 2;
					if( typeof self.$tableElem.find('tr[data-row-id='+rowID+'] td[data-col="'+targetField+'"] .otdi').attr('step') !== 'undefined' ) {
						var step = self.$tableElem.find('tr[data-row-id='+rowID+'] td[data-col="'+targetField+'"] .otdi').attr('step');
						if( '0.001' == step ) {decimals = 3;}
						else if( '0.0001' == step ) {decimals = 4;}
					}
					
					new_value = parseFloat( new_value ).toFixed( decimals );
				}

				// Only change cell value if new_value is different from current value
				if( new_value != self.getCellValue( rowID, targetField ) ) {
					self.setCellValue( rowID, targetField, new_value );
					self.saveCellLock( self.getCellElem( rowID, targetField ) );
				}
			}
		}
	};

	// Cell and Data retrieval and manipulation functions
	this.saveCellLock = function( elem ) {
		elem.prop('disabled',true).addClass('saving-cell').blur();
	};

	this.saveCellUnlock = function( elem, addClass ) {
		if( typeof( addClass ) === 'undefined' ) {addClass = '';}
		elem.prop('disabled',false).removeClass('saving-cell').addClass( addClass );
	};

	this.getCellElem = function( row, col ) {
		return self.$tableElem.find('tr[data-row-id='+row+'] td[data-col="'+col+'"] .otdi').eq(0);
	};

	this.getCellRowID = function( elem ) {
		return elem.closest('tr').data('row-id');
	};

	this.getCellCol = function( elem ) {
		return elem.closest('td').data('col');
	};

	this.getCellValue = function( row, col ) {
		var otdi = self.getCellElem( row, col );

		if( otdi.hasClass( 'otdi--bool' ) ) {
			return otdi.prop( 'checked' ) ? 1 : 0;
		}
		else {
			return otdi.val();
		}
	};

	this.setCellValue = function ( row, col, val, overwriteOrigVal ) {
		if( typeof( overwriteOrigVal ) === 'undefined' ) {overwriteOrigVal = false;}
		
		var cellElem = self.getCellElem( row, col );

		cellElem.val( val ).attr( 'value', val );

		if( cellElem.hasClass( 'otdi--bool' ) ) {
			var checked = val ? true : false;
			cellElem.prop( 'checked', checked );
		}

		if( overwriteOrigVal ) {
			cellElem.data( 'og', val );
		}
	};

	this.getOrigCellValue = function ( row, col ) {
		return self.$tableElem.find('tr[data-row-id='+row+'] td[data-col="'+col+'"] .otdi').data( 'og' );
	};

	this.revertCell = function( row, col, unlock, addClass ) { // dev:bool_handling
		if( typeof( unlock ) === 'undefined' ) {unlock = true;}
		if( typeof( addClass ) === 'undefined' ) {addClass = '';}
		self.setCellValue( row, col, self.getOrigCellValue( row, col ), false );
		if( unlock ) {self.saveCellUnlock( self.getCellElem( row, col ), addClass );}
	};

	this.revertAllChanges = function( addClass ) {
		if( typeof( addClass ) === 'undefined' ) {addClass = '';}

		ovcdt.saveDataArgs = [];
		self.$tableElem.find('.otdi.saving-cell').each(function() {
			self.revertCell( self.getCellRowID( $(this) ), self.getCellCol( $(this) ) , true, addClass );
		});
	};

	this.saveCellChanges = function( rowID, field ) {
		var cellElem = self.getCellElem( rowID, field );

		/*/ Do not update formulas if the change event was fired on a 'disabled' element
		if( !cellElem.prop('readonly') ) {
			self.calcFormulas( rowID, field );
		}
		*/

		ovcdt.addSaveRowDataObj( rowID );
		ovcdt.saveData();
	};

	this.updateFromResponseData = function( data, resultClass, liveUpdating ) {
		if ( typeof liveUpdating === 'undefined' ) { liveUpdating = false; }


		// Determine which columns should be highlighted even if their values didn't change based on the request data
		var forceHighlightCols = [];
		if( !liveUpdating ) {
			for( var i = 0; i < data.request_data.data.length; i++ ) {
				
					for( var requestCol in data.request_data.data[i].data ) {
						requestCol = ( 'title_text' == requestCol ? 'product_title' : requestCol ); // Maybe turn the special save_parent title_text request back into the product_title field
						forceHighlightCols.push( requestCol );
					}
				
			}
		}

		// Update values from the [response] data array and handle accompanying UI styling / visualization
		var currVal = '';
		for( var rowID in data.updated_data ) {
			var row = data.updated_data[ rowID ];

			if( !self.$tableElem.find('tr[data-row-id='+rowID+']').length ) {
				continue;
			}

			if( liveUpdating ) {
				self.$tableElem.find('tr[data-row-id='+rowID+']').addClass( resultClass );

				// Remove any save data row objects for this row (only applies if in wait-to-save mode and there are changes pending)
				for( var x = 0; x < ovcdt.saveDataArgs.length; x++ ) {
					if( rowID == ovcdt.saveDataArgs[ x ].where.ID ) {
						ovcdt.saveDataArgs.splice( x, 1 );
					}
				}
			}

			for( var col in row ) {
				if( 'ID' != col ) {
					currVal = self.getCellValue( rowID, col );
					if( row[ col ] != currVal || -1 !== $.inArray( col, forceHighlightCols ) ) {
						self.setCellValue( rowID, col, row[ col ], true ); // Set the cell value and overwrite the backup 'original' value in the invisible span element
						self.saveCellUnlock( self.getCellElem( rowID, col ), resultClass );
					}
				}
			}
		}
	};


	// Field name formatting functions
	this.fieldAsOVC = function( field ) {
		var pcs = field.split( '.' );
		if( 1 == pcs.length ) {return pcs[0];}
		else if ( 2 == pcs.length ) {return pcs[1];}
		else return false;
	};

	this.fieldAsOVCDT = function( field ) {
		var pcs = field.split( '.' );
		if( 1 == pcs.length ) {return ( ovcdt.results.meta.sql_alias + '.' + field );}
		else if ( 2 == pcs.length ) {return field;}
		else return false;
	};
}






function editSharedProductDataKey( rowID, field, ovcdt ) {
	// Declare OXMD Callback
	this.oxmdCallback = function( response ) {
		if( ovcdtMain.debug ) {console.log( response );}

		if( !response.value ) {
			ovcdt.table.revertAllChanges();
		}
		else if ( 1 === response.value ) {
			var editSharedProductDataKeyRequest = {
				request 	: 'edit_productmeta_key',
				args 		: {
					check_only		: ovcdt.checkOnlyOnSave,
					ovcdt_version 	: ovcdt.results.meta.ovcdt_version,
					data 			: response.fieldData
				}
			};

			console.log( editSharedProductDataKeyRequest );

			ovcdt.setStatus( 'saving' );
			ovcdt.ajaxRequest( editSharedProductDataKeyRequest );
		}
	};

	var spdku = {
		rowID 		: rowID,
		field 		: field,
		newValue 	: ovcdt.table.getCellValue( rowID, field ),
		oldValue 	: ovcdt.table.getOrigCellValue( rowID, field )
	};

	var oxmd_data = {
		type 		: "custom",
		callback 	: this.oxmdCallback,
		modalClass 	: "extra-wide",
		passData 	: spdku,
		saveFields 	: true,
		responses 	: [
			{label:"Cancel",value:0},
			{label:"Save",value:1,primary:true}
		],
		title 		: "Edit Shared Product Data Key Value",
		elements 	: [{
			template	: 'ovc-change-shared-data-key',
			wrapID 		: 'oxmd-ovc-change-shared-data-key',
			requestData : spdku
		}]
	};
	oxmd.modal( oxmd_data );
}

function priceHelper( rowID, ovcdt ) {
	// Declare OXMD callback
	this.oxmdCallback = function( response ) {
		if( ovcdt.debug ) {console.log( response );}

		if( 1 === response.value ) {
			var priceFormulaRequest = {
				request : 'pricing_helper_save',
				args 	: {
					check_only		: ovcdt.checkOnlyOnSave,
					ovcdt_version 	: ovcdt.results.meta.ovcdt_version,
					data 			: response.fieldData
				}
			};

			ovcdt.setStatus( 'saving' );
			ovcdt.ajaxRequest( priceFormulaRequest );
		}
	};

	var oxmd_data = {
		type 		: "custom",
		callback 	: this.oxmdCallback,
		modalClass 	: "extra-wide",
		passData 	: { ovc_id : rowID },
		saveFields 	: true,
		responses 	: [
			{label:"Cancel",value:0},
			{label:"Save",value:1,primary:true}
		],
		title 		: "Pricing Formula Helper",
		elements 	: [{
			template	: 'ovc-price-helper',
			wrapID 		: 'oxmd-ovc-shared-data',
			requestData : { ovc_id : rowID }
		}]
	};
	oxmd.modal( oxmd_data );
}

function sharedProductData( rowID, ovcdt ) {
	// Declare OXMD callback
	this.oxmdCallback = function( response ) {
		if( ovcdt.debug ) {console.log( response );}

		if( 'refresh' === response.value ) {
			oxmd.loadTemplate( 'refresh' );
		}
		else if( 1 === response.value || 2 === response.value ) {
			var saveProductMetaRequest = {
				request	: 'save_productmeta',
				args	: {
					check_only		: ovcdt.checkOnlyOnSave,
					ovcdt_version 	: ovcdt.results.meta.ovcdt_version,
					data 			: response.fieldData
				}
			};

			ovcdt.setStatus( 'saving' );
			ovcdt.ajaxRequest( saveProductMetaRequest );
		}
	};


	var oxmd_data = {
		type 		: "custom",
		callback 	: this.oxmdCallback,
		modalClass 	: "super-wide",
		passData 	: { ovc_id : rowID },
		saveFields 	: true,
		responses 	: [
			{label:"Close",value:0},
			{label:"Refresh",value:'refresh',retainModal:true},
			{label:"Save",value:1,primary:true,retainModal:true}
			//{label:"Save &amp; Close",value:2,primary:true}
		],
		title 		: "View / Edit Shared Product Details",
		elements 	: [{
			template	: 'ovc-shared-data',
			wrapID 		: 'oxmd-ovc-shared-data',
			requestData : { ovc_id : rowID }
		}]
	};
	oxmd.modal( oxmd_data );

}


function productImageManager( rowID, ovcdt ) {
	// Declare OXMD callback
	this.oxmdCallback = function( response ) {
		if( ovcdt.debug ) {console.log( response );}

		if( 'refresh' === response.value ) {
			oxmd.loadTemplate( 'refresh' );
		}
		else if( 1 === response.value || 2 === response.value ) {
			ovcim.saveImageSets();

			/*
			var productImageRequest = {
				request	: 'save_productimages',
				args	: {
					check_only		: ovcdt.checkOnlyOnSave,
					ovcdt_version 	: ovcdt.results.meta.ovcdt_version,
					data 			: response.fieldData
				}
			};

			ovcim.saveImageSets( productImageRequest );
			ovcdt.setStatus( 'saving' );
			ovcdt.ajaxRequest( productImageRequest );
			*/

		}
	};


	var oxmd_data = {
		type 		: "custom",
		callback 	: this.oxmdCallback,
		modalClass 	: "super-wide",
		passData 	: { ovc_id : rowID },
		saveFields 	: true,
		responses 	: [
			{label:"Close",value:0},
			{label:"Refresh",value:'refresh',retainModal:true},
			{label:"Save",value:1,primary:true,retainModal:true},
			{label:"Save &amp; Close",value:2,primary:true}
		],
		title 		: "Manage Product Images",
		elements 	: [{
			template	: 'ovc-image-manager',
			wrapID 		: 'oxmd-ovc-image-manager',
			requestData : { ovc_id : rowID }
		}]
	};
	oxmd.modal( oxmd_data );

}



/**
 *  JQuery Idle.
 *  A dead simple jQuery plugin that executes a callback function if the user is idle.
 *  About: Author
 *  Henrique Boaventura (hboaventura@gmail.com).
 *  About: Version
 *  1.2.6
 **/
!function(n){"use strict";n.fn.idle=function(e){var t,i,o={idle:6e4,events:"mousemove keydown mousedown touchstart",onIdle:function(){},onActive:function(){},onHide:function(){},onShow:function(){},keepTracking:!0,startAtIdle:!1,recurIdleCall:!1},c=e.startAtIdle||!1,d=!e.startAtIdle||!0,l=n.extend({},o,e),u=null;return n(this).on("idle:stop",{},function(){n(this).off(l.events),l.keepTracking=!1,t(u,l)}),t=function(n,e){return c&&(e.onActive.call(),c=!1),clearTimeout(n),e.keepTracking?i(e):void 0},i=function(n){var e,t=n.recurIdleCall?setInterval:setTimeout;return e=t(function(){c=!0,n.onIdle.call()},n.idle)},this.each(function(){u=i(l),n(this).on(l.events,function(){u=t(u,l)}),(l.onShow||l.onHide)&&n(document).on("visibilitychange webkitvisibilitychange mozvisibilitychange msvisibilitychange",function(){document.hidden||document.webkitHidden||document.mozHidden||document.msHidden?d&&(d=!1,l.onHide.call()):d||(d=!0,l.onShow.call())})})}}(jQuery); // jshint ignore:line


function ovcdtLiveData() {
	this.idle = false;
	this.ping = false;
	this.paused = false;
	this.resumeTimeout = false;

	this.setStateIdle = function() {
		ovcdtLive.idle = true;
		if( ovcdt.debug ) {console.log( "user idle" );}

		clearInterval( ovcdtLive.ping );
	};

	this.setStateActive = function() {
		ovcdtLive.idle = false;
		if( ovcdt.debug ) {console.log( "user active" );}

		clearInterval( ovcdtLive.ping );
		ovcdtLive.ping = setInterval( function() {
			if( !ovcdtLive.paused ) {
				if( ovcdt.debug ) {console.log( "ping" );}
				ovcdtLive.sendPing();
			}
			else if( ovcdt.debug ) {
				console.log( "ping skipped (paused)" );
			}
		}, 9000 );
	};

	this.pausePing = function() {
		ovcdtLive.paused = true;
		if( ovcdt.debug ) {console.log( "pinging paused" );}

	};

	this.resumePing = function( timeout ) {
		if( typeof timeout === 'undefined' || !timeout ) {
			if( ovcdt.debug ) {console.log( "pinging resumed" );}
			ovcdtLive.paused = false;
		}
		else {
			clearTimeout( ovcdtLive.resumeTimeout );
			ovcdtLive.resumeTimeout = setTimeout( function() {ovcdtLive.resumePing( false );}, timeout );
		}
		
		
	};

	this.sendPing = function() {
		var ovcdtPingRequest = {
			request	: 'ovcdt_ping',
			args	: {
				ovcdt_version 	: ovcdt.results.meta.ovcdt_version,
				session_id		: ovcdt.results.meta.session_id
			}
		};

		ovcdt.ajaxRequest( ovcdtPingRequest );
	};
}





