// oxmd by Ray Sarno of ONYX Design
// Version 0.1.0

if ( typeof $ === 'undefined' ) {
	var $ = jQuery.noConflict();
}

$(document).ready(function() {

	ovcop = new ovcopAdminPageHandler();
	ovcop.loadOpSelection();
});

function ovcopAdminPageHandler() {

	var self = this;

	this.debug 		= true;
	this.last_response = {};
	this.ajaxStart 	= ''; // Used for timing requests

	this.log_url = false;
	this.log_interval = false;
	this.log_requesting = false;

	this.ping_interval = false;
	this.ping_requesting = false;
	this.noMorePing = false;

	this.opData = false;

	this.userInputData = {};

	this.reader = false;
	this.fileList = {};

	this.noticeBox = new OVC_noticeBox( $('#ovcop-notice-box') );

	this.reset = function() {
		self.stopLog();
		self.stopPing();

		self.log_url = false;
		self.log_interval = false;
		self.log_requesting = false;

		self.ping_interval = false;
		self.ping_requesting = false;
		self.noMorePing = false;

		self.opData = false;

		self.userInputData = {};
		self.fileList = {};
	};

	// Load the OVCOP type selection template
	this.loadOpSelection = function() {
		self.reset();

		var requestData = {
			request 	: 'new_operation',
			args 		: {
				load_op_selection : true
			}
		};

		self.ajaxRequest( requestData, true );
	};

	// Submit selected OVCOP type
	this.selectOp = function( opType ) {
		var selectOpRequest = {
			request : 'new_operation',
			args 	: {
				type 	: opType
			}
		};
		
		self.ajaxRequest( selectOpRequest, true );
	};

	// Submit Operation configuration options and initialize operation
	this.initOp = function( opType ) {
		
		var initOpRequest = {
			request : 'new_operation',
			args 	: {
				type 	: opType,
				config 	: self.getUserInputData()
			}
		};
		
		self.ajaxRequest( initOpRequest, true );
	};

	this.viewOp = function( operation_ID ) {
		var viewOpRequest = {
			request : 'view_operation',
			args 	: {
				opID 	: operation_ID
			}
		};

		var lockBlocks = 'view_operation' == $('#ovcop-block-html').attr( 'data-template-type' ) ? false : true;

		self.ajaxRequest( viewOpRequest, lockBlocks );
	};

	this.resetOp = function( operation_ID ) {
		var resetOpRequest = {
			request : 'reset_operation',
			args 	: {
				opID 	: operation_ID
			}
		};

		self.ajaxRequest( resetOpRequest, false );
	}

	this.abortOp = function( operation_ID ) {
		var abortOpRequest = {
			request : 'abort_operation',
			args 	: {
				opID 	 : operation_ID
			}
		};

		//var lockBlocks = 'view_operation' == $('#ovcop-block-html').attr( 'data-template-type' ) ? false : true;

		self.ajaxRequest( abortOpRequest, false );
	};

	this.resetAllOps = function() {
		var resetOpsRequest = {
			request : 'reset_all_operations',
			args 	: {}
		};

		self.ajaxRequest( resetOpsRequest, false );
	};

	this.commenceOperation = function( operation_ID, opType ) {
		var commenceOperationRequest = {
			request : 'commence_operation',
			args 	: {
				type : opType,
				ID 	: operation_ID
			}
		};
		console.log( commenceOperationRequest );

		self.ajaxRequest( commenceOperationRequest, false );
	};

	// General OVCOP AJAX Request method
	this.ajaxRequest = function( requestObj, lockBlocks ) {
		if ( typeof lockBlocks === 'undefined' ) {lockBlocks = false;}

		var action = 'commence_operation' == requestObj.request ? 'ovc_ajax_nopriv' : 'ovc_ajax';

		var ajax_data = {
			'ovc_ajax'	: true,
			'action' 	: action,
			'command'	: 'ovcop',
			'ovcop'		: requestObj
		};

		if( self.debug ) {console.log("AJAX Request Data: ");console.log( ajax_data );}

		var ajax_args = {
			url			: ajax_object.ajax_url,
			type		: "POST",
			data 		: ajax_data,
			cache		: false,
			dataType	: "json",
			complete	: function( response, status ) {
				if( self.debug ) {console.log('Raw AJAX Resonse:');console.log( response );}

				if ( 'success' == status ) {
					self.handleResponse( response.responseJSON );
				}
				else if ( 'timeout' == status ) {
					self.noticeBox.notice('AJAX Timeout :(', true );
				}
				else {
					self.noticeBox.notice('Error!', true, [{ msg : "AJAX request failed with status: " + status , type : "error" }] );
					console.log( response );
				}

			}
		};

		if ( lockBlocks ) {
			$('#ovcop-block-html-wrap').addClass( 'loading-locked' );
		}

		self.ajaxStart = Date.now();
		$.ajax( ajax_args );
	};

	this.handleResponse = function( response ) {
		if( self.debug ) {
			console.log( "AJAX Response Data:" );
			console.log( "Request Time: " + ( Date.now() - self.ajaxStart ) );
			console.log( response );
		}
		console.log( "Request Time: " + ( Date.now() - self.ajaxStart ) ); // DEV

		if( typeof( response.request ) === 'undefined' ) {
			self.noticeBox.notice('Error! response.request is undefined.', true );
		}
		else {
			self.last_response = response;

			var response_data = response.response_data;

			if( response.result_sets.length ) {
				self.noticeBox.addNotices( response.result_sets );
			}
			
			if ( 'success' == response.status ) {

				

				/*
				// Handle init_operation request response
				if ( 'init_operation' == response.request ) {
					/*
					this.opData.ID 				= response_data.opData.ID;
					this.opData.opType 			= response_data.opData.opType;
					this.opData.verification 	= response_data.opData.verification;
					* /

					// Maybe handle file upload
					if( typeof response_data.do_file_upload !== 'undefined' && response_data.do_file_upload ) {
						self.uploadFile( response_data.csv_upload_name );
					}

					
				}
				else if ( 'ping_operation' == response.request ) {
					self.ping_requesting = false;
				}
				*/

				// Maybe replace ovcop-block-html contents
				if ( response_data.hasOwnProperty( 'html' ) ) {
					// Fade if changing template types
					if ( $('#ovcop-block-html').attr( 'data-template-type' ) != response_data.template_type ) {

						$('#ovcop-block-html-wrap').addClass('reloading-content');
						$('#ovcop-block-html').attr( 'data-template-type', response_data.template_type );

						setTimeout(function() {
							$('#ovcop-block-html').html( ovcop.last_response.response_data.html );
							
							self.setEventHandlers();

							setTimeout( function() {
								
								$('.ovcop-block').removeClass('loading-locked reloading-content');
								self.handleLog();
							}, 550 );
						}, 300);
					}
					// Otherwise just replace
					else {
						$('#ovcop-block-html-wrap').removeClass('loading-locked reloading-content');
						$('#ovcop-block-html').html( response_data.html );
						self.setEventHandlers();
						self.handleLog();
					}	
				}

				// Maybe refresh OVCDT Operations Table
				if( response_data.hasOwnProperty( 'refresh_ovcop_table' ) ) {
					ovcdtMain.loadData();
				}
			
				// Maybe record log_url
				if ( response_data.hasOwnProperty( 'log_url' ) ) {

					self.log_url = response_data.log_url;

					self.logRequest();

					if( 'running' == response_data.opData.status ) {

						self.startLog();
						self.startPing();
					}
					else if( 'queued' == response_data.opData.status ) {

						self.startPing();
						self.stopLog();
					}
					else {
						self.stopPing();
						self.stopLog();

						// force one last ping
						if( self.opData.ID && self.opData.status != response_data.opData.status ) {
							self.viewOp( response_data.opData.ID );
						}
						

						
					}
				}
				else {
					self.stopPing();
					self.stopLog();
				}

				if( response_data.hasOwnProperty( 'opData' ) ) {
					self.opData = onyx_clone( response_data.opData );


				}
				else {
					self.opData = false;
				}

				self.ping_requesting = false;
				self.log_requesting = false;
			}




		}

		self.userInputData = {};
	};

	// File Upload Request
	this.uploadFile = function( csvUploadName ) {

		var ajax_data = new FormData();
		ajax_data.append( 'ovc_ajax', true );
		ajax_data.append( 'action', 'ovc_ajax' );
		ajax_data.append( 'command', 'ovcop' );
		ajax_data.append( 'ovcop', 'file_upload' );
		ajax_data.append( 'ID', self.opData.ID );
		ajax_data.append( 'type', self.opData.opType );
		ajax_data.append( 'verification', self.opData.verification );
		ajax_data.append( 'uploaded_file', $('#import_csv_upload')[0].files[0], csvUploadName );


		if( self.debug ) {console.log("AJAX Request Data: ");console.log( ajax_data );}

		var ajax_args = {
			url			: ajax_object.ajax_url,
			type		: "POST",
			data 		: ajax_data,
			cache		: false,
			dataType	: "json",
			processData : false,
			contentType : false,
			complete	: function( response, status ) {
				if( self.debug ) {console.log('Raw AJAX Resonse:');console.log( response );}

				if ( 'success' == status ) {
					self.handleResponse( response.responseJSON );
				}
				else if ( 'timeout' == status ) {
					self.noticeBox.notice('AJAX Timeout :(', true );
				}
				else {
					self.noticeBox.notice('Error!', true, [{ msg : "AJAX request failed with status: " + status , type : "error" }] );
					console.log( response );
				}

			}
		};

		$.ajax( ajax_args );
	};

	// OVCOP Input Handling
	this.setEventHandlers = function() {
		$('.ovcop-user-input').change(function() {
			if ( typeof $(this).data('inputToggle') !== 'undefined' ) {
				$( '.togglee-'+$(this).data('inputToggle') ).toggleClass('ox-hidden', !$(this).is(':checked') );
			}

			if( 'radio' == $(this).attr('type') ) {
				var name = $(this).attr('name');

				$('input[name="' + name + '"]').each(function() {
					if ( typeof $(this).data('inputToggle') !== 'undefined' ) {
						$( '.togglee-'+$(this).data('inputToggle') ).toggleClass('ox-hidden', !$(this).is(':checked') );
					}
				});
			}
		});

		$('#ovcop-type-select tr').click( function() {
			$(this).children('td').eq(0).children('input.ovcop-user-input').eq(0).prop("checked",true).change();
		});

		$('input.ovcop-user-input[type="file"]').change(function(event) {
			if( $(this).data('ovcFileReader') ) {
				$(this).data('ovcFileReader').abort();
			}

			var ovcFileReader = new FileReader();
			ovcFileReader.readAsText( event.target.files[0] );
			$(this).data( 'ovcFileReader', ovcFileReader );
			/*
			ovcop.fileList = event.target.files;
			$(this).data( 'files', event.target.files );
			console.log( event.target.files );
			*/
		});

		$('input.ovcop-file-path').click(function() {
			$(this).select();
		});

		// new Clipboard('.ovc-clipboard-btn');
	};

	this.getUserInputData = function() {
		self.userInputData = {};
		//self.userInputData = new FormData();

		$('.ovcop-user-input:not(.ox-hidden)').each(function() {
			if( $(this).is('select, input, textarea') ) {
				// Determine value based on input type
				var name = $(this).attr('name');

				var value = $(this).val(); // Most input types (select, text, textarea, number)

				if( $(this).is(':checkbox') ) {
					value = $(this).is(':checked');
				}
				else if ( $(this).is(':radio') ) {
					value = $('input[name="'+name+'"]:checked').val();
				}
				else if( 'file' == $(this).attr('type') ) {

					value = $(this).data( 'ovcFileReader' ).result;
				}

				self.userInputData[ $(this).attr('name') ] = value;
				//self.userInputData.append( $(this).attr('name'), value );
			}
		});

		return self.userInputData;
	};

	this.readFiles = function() {
		self.reader = new FileReader();

		self.reader.onload = function(e) {
			console.log( 'file reading done!' );

		};

		self.reader.onprogress = function(e) {
			console.log('progress');
			console.log( e );
		};

		//self.fileList = $('#import_csv_upload').data('files');
		self.reader.readAsText( self.fileList[0] );
	};

	/**
	 * OVCOP Ping Request Interval
	 */
	this.startPing = function() {
		if( false === self.ping_interval ) {
			clearInterval( self.ping_interval );
			self.ping_interval = setInterval( self.pingOp, 5200 );
		}
	};
	this.stopPing = function() {
		clearInterval( self.ping_interval );
		self.ping_interval = false;
	};
	this.pingOp = function() {
		if( !self.ping_requesting )  {
			self.ping_requesting = true;
			self.viewOp( self.opData.ID );
		}
	};

	/**
	 * OVCOP Log Request Interval
	 */

	this.handleLog = function() {

		if( !self.opData ) {
			this.stopLog();
			$("#ovcop-log").empty();
			$("#ovcop-log-wrap").addClass('ox-hidden');
			return false;
		}

		var opStatus = self.opData.status;

		if( opStatus == 'running' ) {
			self.startLog();
		}
		else {
			self.stopLog();
			self.logRequest();
		}
	};

	this.startLog = function() {
		if( false === self.log_interval ) {
			self.logRequest();
			clearInterval( self.log_interval );
			self.log_interval = setInterval( self.logRequest, 3000 );
		}
	};
	this.stopLog = function() {
		clearInterval( self.log_interval );
		self.log_interval = false;
	};
	
	this.logRequest = function() {
		$("#ovcop-log-wrap").removeClass('ox-hidden');

		if ( !self.log_requesting ) {
			self.log_requesting = true;

			$.ajax({
				url			:self.log_url,
				dataType	:'text',
				cache 		:false,
				timeout		:2500,
				complete	:function(data, status) {
					self.log_requesting = false;

					if( status == 'success' ) {
						var log_contents = data.responseText;
						log_contents = log_contents.replace(/(?:\r\n|\n)/g, '<br />');
						$("#ovcop-log").empty().append( log_contents );
						
						if( $("#ovcop-log .op-log-finished").length && !self.noMorePing ) {
							self.noMorePing = true;
							self.stopLog();
							self.stopPing();
							self.pingOp();
							self.logRequest();
						}
					}
					else {
						$("#ovcop-log").empty().append( "Error retreiving log file..." );
						ovcop.stopLog();
					}
				}
			});
		}
	};
}