<?php
/* 
Plugin Name: VIDA Enterprise Custom Tools by ONYX Design
Description: Adds various functionality, tweaks, and fixes for vida-us.com
Version: 0.9.6
Author: Ray Sarno
Author URI: http://onyxdesign.net
Max WP Version: 5.4.2
Text Domain: ovc
License: GPL2+
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

define( 'OVC_DEBUG', false );

if( OVC_DEBUG ) {
	// Define start time for debugging & performance measurement
	define( 'OVC_START_EXEC', ( microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"] ) );	
}


include_once( 'core/class-ovc-singleton-class-abstract.php' );

final class OVC {
	use OVC_Singleton_Class_Abstract;

	protected $request_type = null;

	public $ovc_admin = null;

	/**
	 * Class Constructor
	 */
	protected function __construct() {
		
		$this->define_constants();

		// Determine request type
		if( is_admin() ) {
			$this->request_type = 'admin';
		} else if ( defined( 'DOING_AJAX' ) ) {
			$this->request_type = 'ajax';
		} else if ( defined( 'DOING_CRON' ) ) {
			$this->request_type = 'cron';
		} else if ( ( ! is_admin() || defined( 'DOING_AJAX' ) ) && ! defined( 'DOING_CRON' ) ) {
			$this->request_type = 'frontend';
		}

		$this->includes();

		// Make sure OVCOP Background Operations WP-Cron job is scheduled //dev:improve //dev:move //dev:ovc_cron //dev:cron
		if( !wp_next_scheduled( 'ovcop_cron_auto_sync' ) ) {
			wp_schedule_event( time(), 'hourly', 'ovcop_cron_auto_sync' );
		}
		
		add_action( 'ovcop_cron_auto_sync', array( 'OVCOP_manager', 'queue_auto_sync_ops' ) );

		$this->init_hooks();
	}

	/**
     * Hook into actions and filters
     */
	private function init_hooks() {

		// Run after WooCommerce
		add_action( 'init', array( $this, 'init' ) );
	}

	/**
	 * Auto-load in-accessible properties on demand.
	 * @param mixed $key
	 * @return mixed
	 */
	public function __get( $key ) {
		if ( in_array( $key, array( 'request_type' ) ) ) {
			return $this->$key();
		}
	}

	/**
	 * Define OVC Constants
	 */
	private function define_constants() {

		// OVC Request types //dev:improve
		define( 'OVC_AJAX', !empty( $_POST['ovc_ajax'] ) );
		define( 'OVC_CRON', !empty( $_POST['ovc_cron'] ) );

		define( 'OVC_VERSION', '0.9.6' );
		define( 'OVC_SLUG', 'ovc' );

		define( 'OVC_PATH', realpath( dirname(__FILE__) ) );
		define( 'OVC_URL', plugins_url() . '/' . basename(dirname(__FILE__)) . '/' );

		define( 'OVCOP_CONTENT_DIR', WP_CONTENT_DIR . '/ovcop' );
		define( 'OVCOP_TEMPLATE_DIR', WP_CONTENT_DIR . '/ovcop-templates' );
		define( 'OVCOP_ARCHIVED_CONTENT_DIR', WP_CONTENT_DIR . '/archived-ovcop' );
		define( 'OVC_CACHE_DIR', WP_CONTENT_DIR . '/ovc-cache' );

		// Directory for class-ovc-image files
		define( 'OVC_IMAGE_DIR', WP_CONTENT_DIR . '/ovc-images' );
	}

	/**
	 * Include required core files
	 */
	private function includes() {

		// OVC Autoloader
		include_once( 'core/class-ovc-autoloader.php' );

		// Composer Autoloader
		require_once( 'libraries/vendor/autoload.php' );
		
		include_once( 'core/ovc-functions.php' );

		// If not on live, make sure error_reporting and logging are on
		if( !on_vida_live() && OVC_DEBUG ) {

			ovc_debug()->set_active();
		}

		// Initialize OVC Schema
		OVCSC::init(); //dev:schema2 //dev:remove

		if( OVC_AJAX ) {
			include_once( 'core/ovc-ajax.php' );
		}

		switch( $this->request_type ) {
			case 'admin':
				$this->ovc_admin = new OVC_Admin();
			break;
			case 'ajax':
				new OVC_Admin_WC_Product(); //dev:instance_specific
			break;
			default:
			break;
		}
		
	}

	/**
	 * Init Functions
	 */
	public function init() {

		switch( $this->request_type ) {
			case 'admin':

				OVC_Migrations::check_migrations();
				break;
			case 'ajax':
				break;
			default:
				break;
		}
	}

	/**
	 * What type of request is this?
	 *
	 * @param  string $type admin, ajax, cron or frontend.
	 * @return bool
	 */
	public function is_request( $type ) {
		return ( $type == $this->request_type );
	}
}

function OVC() { // dev
	return OVC::instance();
}
OVC(); //Set OVC in motion

if( OVC_DEBUG ) {
	$ovc_load_time = (float) ox_exec_time() - OVC_START_EXEC;	
}


// OVC Dev Log
function ovc_dev_log( $msg, $clear_log = false ) {
	$msg = ( is_object( $msg ) || is_array( $msg ) ) ? print_r( $msg, true ) : $msg ;
	if( $clear_log ) {file_put_contents( realpath( dirname(__FILE__) ).'/ovc-dev-log.txt', "\r\n".date( 'Y-m-d H:i:s' ).substr( (string) microtime() , 1, 8 ).' - '.$msg );	}
	else file_put_contents( realpath( dirname(__FILE__) ).'/ovc-dev-log.txt', "\r\n".date( 'Y-m-d H:i:s').substr( (string) microtime() , 1, 8 ).' - '.$msg, FILE_APPEND );
}
