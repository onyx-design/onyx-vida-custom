<?php
// Exit if accessed directly
if (!defined('ABSPATH')) exit;

echo '<?xml version="1.0" encoding="UTF-8"?>';
?>

<MPItemFeed xmlns="http://walmart.com/">
	<MPItemFeedHeader>
		<version>3.2</version>
		<mart>WALMART_US</mart>
	</MPItemFeedHeader>

	<?php foreach ($ovc_ids as $ovc_id) {

		$product = OVCDB()->get_row('ovc_products', $ovc_id, $results);

		$sku_update = OVC_External::filter_walmart_sku_update($product);
		$upc_update = OVC_External::filter_walmart_upc_update($product);
		?>
		<MPItem>
			<processMode>REPLACE_ALL</processMode>

			<sku><?php echo OVC_External::filter_walmart_sku($product); ?></sku>

			<productIdentifiers>
				<productIdentifier>
					<productIdType>UPC</productIdType>
					<productId><?php echo OVC_External::filter_walmart_upc($product); ?></productId>
				</productIdentifier>
			</productIdentifiers>

			<MPProduct>
				<SkuUpdate><?php echo $sku_update; ?></SkuUpdate>
				<ProductIdUpdate><?php echo $sku_update == 'Yes' ? 'No' : $upc_update; ?></ProductIdUpdate>

				<productName><?php ovc_cdata_wrap(OVC_External::filter_walmart_product_name($product)); ?></productName>

				<msrp><?php echo $product->price_msrp; ?></msrp>

				<category>
					<ClothingCategory>
						<Clothing>
							<shortDescription><?php ovc_cdata_wrap($product->filter_walmart_short_descrip()); ?></shortDescription>

							<keyFeatures>
								<?php
									foreach (array('st.fast_facts1', 'st.fast_facts2', 'st.fast_facts3', 'st.fast_facts4', 'st.sizing') as $feature_field) {
										if (!empty($product->data($feature_field))) {
											?>
										<keyFeaturesValue><?php echo ovc_cdata_wrap($product->data($feature_field), false); ?></keyFeaturesValue>
								<?php
										}
									}
									?>
							</keyFeatures>

							<brand><?php echo $product->brand; ?></brand>

							<multipackQuantity>1</multipackQuantity>
							<countPerPack><?php echo $product->data('pr.sku_pkqty'); ?></countPerPack>
							<count><?php echo $product->data('pr.sku_pkqty'); ?></count>

							<modelNumber><?php echo $product->sku; ?></modelNumber>

							<?php
								$main_img_id = isset($product->img_marketplace_featured) && $product->img_marketplace_featured ? $product->img_marketplace_featured : $product->img_main;
								$main_img = OVCDB()->get_row_by_valid_id('ovc_images', 'post_id', $main_img_id);
								?>
							<mainImageUrl><?php echo $main_img->get_walmart_image_url(); ?></mainImageUrl>


							<?php if ($product->img_gallery_ids) { ?>
								<productSecondaryImageURL>
									<?php
											$img_gallery_ids = explode(',', $product->img_gallery_ids);

											foreach ($img_gallery_ids as $img_id) {

												$gallery_img = OVCDB()->get_row_by_valid_id(FS('ovc_images'), 'post_id', $img_id);
												?>
										<productSecondaryImageURLValue><?php echo $gallery_img->get_walmart_image_url(); ?></productSecondaryImageURLValue>
									<?php
											} ?>
								</productSecondaryImageURL>
							<?php } ?>

							<color><?php ovc_cdata_wrap($product->data('pr.color_full_name')); ?></color>
							<clothingSize><?php echo $product->get_full_size_name(); ?></clothingSize>

							<gender><?php ovc_cdata_wrap($product->filter_walmart_gender()); ?></gender>

							<variantGroupId><?php echo $product->parent_sku; ?></variantGroupId>

							<variantAttributeNames>
								<variantAttributeName>clothingSize</variantAttributeName>
								<variantAttributeName>color</variantAttributeName>
							</variantAttributeNames>

							<?php if ($materials = $product->get_material_content_array(3)) { ?>
								<fabricContent>
									<?php foreach ($materials as $material_name => $material_percent) { ?>
										<fabricContentValue>
											<materialName><?php echo $material_name; ?></materialName>
											<materialPercentage><?php echo $material_percent; ?></materialPercentage>
										</fabricContentValue>
									<?php } // end foreach( $materials ... ) 
											?>
								</fabricContent>
							<?php } // end if( $materials ) 
								?>

							<?php if (!empty($product->data('st.care'))) { ?>
								<fabricCareInstructions>
									<fabricCareInstruction><?php ovc_cdata_wrap($product->data('st.care')); ?></fabricCareInstruction>
								</fabricCareInstructions>
							<?php } ?>

							<swatchImages>
								<swatchImage>
									<swatchVariantAttribute>color</swatchVariantAttribute>
									<swatchImageUrl><?php $img_attrs = wp_get_attachment_image_src($product->get_best_swatch_id(), 'thumbnail');
														echo $img_attrs[0]; ?></swatchImageUrl>
								</swatchImage>
							</swatchImages>

							<?php if (!empty($product->data('st.keywords'))) { ?>
								<keywords><?php echo $product->data('st.keywords'); ?></keywords>
							<?php } ?>

						</Clothing>
					</ClothingCategory>
				</category>
			</MPProduct>

			<MPOffer>
				<price><?php echo $product->data('pr.price_walmart'); ?></price>
				<MinimumAdvertisedPrice><?php echo $product->data('pr.price_walmart'); ?></MinimumAdvertisedPrice>

				<ShippingWeight>
					<measure><?php echo $product->unit_weight; ?></measure>
					<unit>lb</unit>
				</ShippingWeight>
				<ProductTaxCode><?php echo $product->filter_walmart_tax_code(); ?></ProductTaxCode>
				<MustShipAlone>No</MustShipAlone>
				<fulfillmentLagTime>1</fulfillmentLagTime>
			</MPOffer>
		</MPItem>
	<?php } ?>
</MPItemFeed>