<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

$create_feed = $this->feed_type == 'walmart_bulk_create_items' ? true : false;
?>
<MPItem>
	<?php 
		if( $create_feed ) {
			?>
				<processMode>CREATE</processMode>
			<?php
		} else {
			?>
				<processMode>REPLACE_ALL</processMode>
			<?php
		}
	?>
	
	<sku><?php echo OVC_External::filter_walmart_sku( $product ); ?></sku>

	<productIdentifiers> 
		<productIdentifier>
			<productIdType>UPC</productIdType>
			<productId><?php echo OVC_External::filter_walmart_upc( $product ); ?></productId>
		</productIdentifier>
	</productIdentifiers>

	<?php
		
		include( 'MPProduct.php' ); 	
		
		
	?>

	<MPOffer>
		<price><?php echo $product->data( 'pr.price_walmart' ); ?></price>
		<MinimumAdvertisedPrice><?php echo $product->data( 'pr.price_walmart' ); ?></MinimumAdvertisedPrice>

		<?php /*
			if ( $create_feed ) {
				?>
					<StartDate><?php echo date("Y-m-d") ;?></StartDate>
				<?php
			}*/
		?>
		
		<ShippingWeight>
			<measure><?php echo $product->unit_weight; ?></measure>
			<unit>lb</unit>
		</ShippingWeight>
		<ProductTaxCode><?php echo $product->filter_walmart_tax_code(); ?></ProductTaxCode>
		<MustShipAlone>No</MustShipAlone>
	</MPOffer>
</MPItem>
