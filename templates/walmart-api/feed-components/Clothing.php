<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>

<Clothing>

	<swatchImages>
		<swatchImage>
			<swatchImageUrl><?php $img_attrs = wp_get_attachment_image_src( $product->get_best_swatch_id(), 'thumbnail' ); echo $img_attrs[0]; ?></swatchImageUrl>
			<swatchVariantAttribute>color</swatchVariantAttribute>
		</swatchImage>
	</swatchImages>

	<VariantAttributeNames>
		<variantAttributeName>clothingSize</variantAttributeName>
		<variantAttributeName>color</variantAttributeName>
	</VariantAttributeNames>

	<variantGroupId><?php echo $product->parent_sku ; ?></variantGroupId>

	<?php /*
	
	<isPrimaryVariant></isPrimaryVariant>
	*/ ?>

	<?php 
		// Maybe Output Materials & Material Percents

		$materials = $product->get_material_content_array( 3 );

		if( $materials ) {
		?>
			
			<fabricContent>

			<?php
				foreach( $materials as $material_name => $material_percent ) {
				?>

					<fabricContentValue>
						<materialName><?php echo $material_name ; ?></materialName>
						<materialPercentage><?php echo $material_percent ; ?></materialPercentage>
					</fabricContentValue>

				<?php
				} // end foreach( $materials ... )
			?>

			</fabricContent>

		<?php
		} // end if( $materials )
	?>
	
	<fabricCareInstructions>
		<fabricCareInstruction><?php ovc_cdata_wrap( $product->data( 'st.care' ) ); ?></fabricCareInstruction>
	</fabricCareInstructions>

	<brand><?php echo $product->brand; ?></brand>

	<modelNumber><?php echo $product->sku; ?></modelNumber>

	<clothingSize><?php echo $product->get_full_size_name(); ?></clothingSize>

	<?php

		/*/if( !in_array( $product->data( 'st.clothing_type' ), array( 'Socks', 'Panties', 'Bras' ) ) ) {

		$clothingSize = in_array( $product->data( 'st.clothing_type' ), array( 'Socks', 'Panties', 'Bras' ) ) ? '' : $product->get_full_size_name();
		?>

			<clothingSize><?php ovc_cdata_wrap( $product->get_full_size_name() ); ?></clothingSize>
		<?php
		//}
		*/
	?>	

	<?php 

		if( $product->department ) {
		?>

			<gender><?php echo $product->filter_walmart_gender(); ?></gender>

		<?php
		}
	?>
	

	<color>
		<colorValue><?php ovc_cdata_wrap( $product->data( 'pr.color_full_name' ) ); ?></colorValue>
	</color>

	<?php
	/*

	<ageGroup>
		<ageGroupValue></ageGroupValue>
	</ageGroup>

	<clothingSizeType>Regular</clothingSizeType>

	<apparelCategory>Regular</apparelCategory>
	*/
	?>

	<?php 
		/*
		switch ( $product->data( 'st.clothing_type' ) ) {
			
			case 'Socks':
			?>
				<Socks>
					<sockSize><?php ovc_cdata_wrap( $product->get_full_size_name() ); ?></sockSize>
					<?php
						if( $product->data( 'st.sock_style') ) {
						?>
					<sockStyle><?php echo $product->data( 'st.sock_style' ); ?></sockStyle>
						<?php
						}
					?>
				</Socks>
			<?php
			break;

			case 'Panties':
			?>
				<Panties>
					<pantySize><?php ovc_cdata_wrap( $product->get_full_size_name() ); ?></pantySize>

					<?php
						if( $product->data( 'st.silhouette' ) ) {
						?>
							<pantyStyle><?php echo $product->data( 'st.silhouette' ); ?></pantyStyle>
						<?php
						}
					?>
				</Panties>
			<?php
			break;

			case 'Bras':
			?>
				<Bras>
				<?php
					$walmart_bra_style_pieces = array();

					if( $product->data( 'st.back' ) ) {
						$walmart_bra_style_pieces[] = $product->data( 'st.back' );
					}

					if( $product->data( 'st.frame' ) ) {
						$walmart_bra_style_pieces[] = $product->data( 'st.frame' );
					}


					if( $walmart_bra_style_pieces ) {
					?>
							<braStyle><?php echo trim( implode( ' ', $walmart_bra_style_pieces ) ); ?></braStyle>
					<?php
					}
				?>
					
					<braSize>
						<braBandSize><?php echo $product->filter_walmart_bra_size_part( 'band' ); ?></braBandSize>
						<braCupSize><?php echo $product->filter_walmart_bra_size_part( 'cup' ); ?></braCupSize>
					</braSize>
				</Bras>
			<?php
			break;

			/*

			NOT USED

			case 'Skirts':
			?>
			<Skirts>
				<waistSize>
					<unit></unit>
					<measure></measure>
				</waistSize>
				<skirtAndDressCut></skirtAndDressCut>
			</Skirts>
			<?php
			break;

			case 'Pants and Shorts':
			?>
			<PantsAndShorts>
				<pantSize></pantSize>
				<pantRise></pantRise>
				<pantStyle></pantStyle>
				<pantPanelStyle></pantPanelStyle>
			</PantsAndShorts>
			<?php
			break;

			case 'Shirts and Tops':
			?>
			<ShirtsAndTops>
			<shirtSize></shirtSize>
			<shirtNeckStyle></shirtNeckStyle>
			<sleeveStyle></sleeveStyle>
			</ShirtsAndTops>
			<?php
			break;

			case 'Accessories':
			?>
			<ClothingAccessories>
			<material>
			<materialValue></materialValue>
			</material>
			</ClothingAccessories>
			<?php
			break;

			case 'Dresses':
			?>
			<Dresses>
			<skirtAndDressCut></skirtAndDressCut>
			<dressStyle></dressStyle>
			<sleeveStyle></sleeveStyle>
			</Dresses>
			<?php
			break;

			case 'Swimwear':
			?>
				<WomensSwimsuits>
					<braSize>
						<braBandSize></braBandSize>
						<braCupSize></braCupSize>
					</braSize>
					<swimSuitStyle></swimSuitStyle>
				</WomensSwimsuits>
			<?php
			break;

			case 'Dress Shirts':
			?>
				<DressShirts>
					<dressShirtSize>
						<neckSize></neckSize>
					</dressShirtSize>
					<collarType></collarType>
					<sleeveStyle></sleeveStyle>
				</DressShirts>
			<?php
			break;

			* /

			default:
			break;
		}
		*/
	?>
</Clothing>
