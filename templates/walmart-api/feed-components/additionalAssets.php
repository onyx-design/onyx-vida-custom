<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

// Only output additional assets if we have gallery images or a size guide image
if( $product->img_gallery_ids || $product->img_sizeguide ) {
	?>

	<additionalAssets>

	<?php
	if( $product->img_gallery_ids ) {

		$img_gallery_ids = explode( ',', $product->img_gallery_ids );

		foreach( $img_gallery_ids as $index => $img_id ) {
			$gal_index = $index + 1;
			?>
				<additionalAsset>
					<altText><?php ovc_cdata_wrap( $product->filter_walmart_img_alt_text( "Extra Image {$gal_index}" ) ); ?></altText>
					<assetUrl><?php echo wp_get_attachment_url( intval( $img_id ) ); ?></assetUrl>
					<assetType>SECONDARY</assetType>
				</additionalAsset>
			<?php
		}
	}

	if( $product->img_sizeguide ) {
	?>
		<additionalAsset>
			<altText><?php ovc_cdata_wrap( $product->filter_walmart_img_alt_text( "Sizing Guide" ) ); ?></altText>
			<assetUrl><?php echo wp_get_attachment_url( intval( $product->img_sizeguide ) ); ?></assetUrl>
			<assetType>SECONDARY</assetType>
		</additionalAsset>

	<?php
	}
	?>

	</additionalAssets>

	<?php
}
?>
