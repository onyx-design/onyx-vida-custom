<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>

<ProductEnvelope>
	<mart>WALMART_US</mart>
	<sku><?php echo $product->sku; ?></sku>
	<wpid><?php echo $product->data( 'wa.walmart_id' ) ; ?></wpid>

	<?php
		include( 'MPProduct.php' ); 
	?>
</ProductEnvelope>
