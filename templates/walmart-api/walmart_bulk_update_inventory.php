<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

// Initialize out of stock tracking stat
$results->out_of_stock_count = 0;
// V3 (https://developer.walmart.com/#/apicenter/marketPlace/latest#bulkUpdateInventory)
echo '<?xml version="1.0" encoding="UTF-8"?>';
?>

<InventoryFeed xmlns="http://walmart.com/">
  	<InventoryHeader>
      	<version>1.4</version>
  	</InventoryHeader>
  	<?php

  		foreach( $ovc_ids as $ovc_id ) {

        $ovc_pr = OVCDB()->get_row( FS( 'ovc_products' ), $ovc_id, $results );

        if( !$ovc_pr->data( 'wa.sku' ) ) {
          continue;
        }

        $walmart_stock = $ovc_pr->get_online_stock();

        if( 0 == $walmart_stock ) {
          $results->out_of_stock_count = $results->out_of_stock_count + 1;
        }

  			?>
  				<inventory>
  				    <sku><?php echo $ovc_pr->data( 'wa.sku' ); ?></sku>
  				    <quantity>
  				        <unit>EACH</unit>
  				        <amount><?php echo $walmart_stock; ?></amount>
  				    </quantity>
  				</inventory>
  			<?php
  		}
  	?>
</InventoryFeed>