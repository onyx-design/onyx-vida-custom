<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
// V3 (https://developer.walmart.com/#/apicenter/marketPlace/latest#updateBulkPrices)
echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<PriceFeed xmlns="http://walmart.com/">
	<PriceHeader>
		<version>1.5.1</version>
	</PriceHeader>

	<?php foreach( $ovc_ids as $ovc_id ) {

		$product = OVCDB()->get_row( 'ovc_products', $ovc_id, $results );

		if( !$product->data( 'wa.sku' ) ) {
			continue;
		}
	?>
	<Price>
		<itemIdentifier>
			<sku><?php echo OVC_External::filter_walmart_sku( $product ); ?></sku>
		</itemIdentifier>
		<pricingList>
			<pricing>
				<currentPrice>
					<value currency="USD" amount="<?php echo $product->data( 'pr.price_walmart' ); ?>" />
				</currentPrice>
			</pricing>
		</pricingList>
	</Price> 
	<?php } ?>
</PriceFeed>