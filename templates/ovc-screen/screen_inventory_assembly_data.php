<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

$ovcia_version = '0.2';
$product = OVCDB()->get_row( FS( 'ovc_products' ), $this->args['options']['ovc_id'] );
?>
<div id="ovc-screen--assemblies" class="ovc-screen-content-inner--full-width">
	<input type="hidden" name="ovc_id" class="ovc-screen-field" value="<?php echo $product->ID ;?>" />

	<div id="ovcia-sku-dock-outer-wrap" class="ovcia-card ox-row">
		<div class="ovcia-card-header">

			<div class="ovcia-card-filter-bar">
				<div class="ovcia-filter-group">
						<h4 class="ovcia-card-title">Variation SKUs</h4>
						

					<div class="ovcia-filter-group-label">
						Total: <span class="ovcia-count--total-skus"></span><br />
						Shown: <span class="ovcia-count--filtered-skus"></span>
					</div>

				</div>
				<div class="ovcia-filter-group">
					<div class="ovcia-filter-group-label">
						<b>FILTERS</b>
					</div>
					<div class="ovcia-filter-button">
						<input type="checkbox" class="ovcia-filter-input" name="ovcia_filter__show_all" id="ovcia-filter__show_all" />
						<label class="ovcia-filter-label" for="ovcia-filter__show_all">
							<div class="ovcia-filter-label-text">Show All</div>
							<div class="ovcia-filter-label-count"></div>
						</label>
					</div>
				</div>

				<div class="ovcia-filter-group">
					<div class="ovcia-filter-group-label">
						Search
					</div>
					<div class="ovcia-filter-button">
						<input type="text" class="ovcia-filter-input" id="ovcia-filter-sku-search" placeholder="Filter SKUs" />
					</div>
				</div>

				<div class="ovcia-filter-group">
					<div class="ovcia-filter-group-label">
						Assembly Property
					</div>
					<div class="ovcia-filter-button-group">
						<div class="ovcia-filter-button ovcia-filter-button--checkbox">
							<input type="checkbox" class="ovcia-filter-input" id="ovcia-filter__is_raw" data-filter-prop="is_raw" name="ovcia_filter__is_raw" />
							<label class="ovcia-filter-label" for="ovcia-filter__is_raw">
								<div class="ovcia-filter-label-icon">R</div>
								<div class="ovcia-filter-label-text">Raw</div>
								<div class="ovcia-filter-label-count"></div>
							</label>
						</div>
						<div class="ovcia-filter-button ovcia-filter-button--checkbox">
							<input type="checkbox" class="ovcia-filter-input" id="ovcia-filter__is_case" data-filter-prop="is_case" name="ovcia_filter__is_case" />
							<label class="ovcia-filter-label" for="ovcia-filter__is_case">
								<div class="ovcia-filter-label-icon"><i class="fa fa-archive"></i></div>
								<div class="ovcia-filter-label-text">Case</div>
								<div class="ovcia-filter-label-count"></div>
							</label>
						</div>
						<div class="ovcia-filter-button ovcia-filter-button--checkbox">
							<input type="checkbox" class="ovcia-filter-input" id="ovcia-filter__is_primary" data-filter-prop="is_primary" name="ovcia_filter__is_primary" />
							<label class="ovcia-filter-label" for="ovcia-filter__is_primary">
								<div class="ovcia-filter-label-icon"><i class="fa fa-star"></i></div>
								<div class="ovcia-filter-label-text">Primary</div>
								<div class="ovcia-filter-label-count"></div>
							</label>
						</div>
					</div>
				</div>

				<div class="ovcia-filter-group">
					<div class="ovcia-filter-group-label ">
						<i class="fa fa-level-down rotate--180"></i>
						SKU's count as<br />
						Assembly Item
					</div>
					<div class="ovcia-filter-button-group buttons--equal-width">
						<div class="ovcia-filter-button ovcia-filter-button--radio">
							<input type="radio" class="ovcia-filter-input" id="ovcia-filter__asmi_count_0" data-filter-prop="count_asmi" name="ovcia_filter__asmi_count" value="0" />
							<label class="ovcia-filter-label" for="ovcia-filter__asmi_count_0">
								<div class="ovcia-filter-label-text">0</div>
								<div class="ovcia-filter-label-count"></div>
							</label>
						</div>
						<div class="ovcia-filter-button ovcia-filter-button--radio">
							<input type="radio" class="ovcia-filter-input" id="ovcia-filter__asmi_count_1" data-filter-prop="count_asmi" name="ovcia_filter__asmi_count" value="1" />
							<label class="ovcia-filter-label" for="ovcia-filter__asmi_count_1">
								<div class="ovcia-filter-label-text">1</div>
								<div class="ovcia-filter-label-count"></div>
							</label>
						</div>
						<div class="ovcia-filter-button ovcia-filter-button--radio">
							<input type="radio" class="ovcia-filter-input" id="ovcia-filter__asmi_count_2" data-filter-prop="count_asmi" name="ovcia_filter__asmi_count" value="2" />
							<label class="ovcia-filter-label" for="ovcia-filter__asmi_count_2">
								<div class="ovcia-filter-label-text">&gt; 1</div>
								<div class="ovcia-filter-label-count"></div>
							</label>
						</div>
					</div>
				</div>

				<div class="ovcia-filter-group">
					<div class="ovcia-filter-group-label">
						<i class="fa fa-level-up rotate--180"></i>
						SKU's count as<br />
						Assembly Parent
					</div>
					<div class="ovcia-filter-button-group buttons--equal-width">
						<div class="ovcia-filter-button ovcia-filter-button--radio">
							<input type="radio" class="ovcia-filter-input" id="ovcia-filter__asm_count_0" data-filter-prop="count_asm" name="ovcia_filter__asm_count" value="0" />
							<label class="ovcia-filter-label" for="ovcia-filter__asm_count_0">
								<div class="ovcia-filter-label-text">0</div>
								<div class="ovcia-filter-label-count"></div>
							</label>
						</div>
						<div class="ovcia-filter-button ovcia-filter-button--radio">
							<input type="radio" class="ovcia-filter-input" id="ovcia-filter__asm_count_1" data-filter-prop="count_asm" name="ovcia_filter__asm_count" value="1" />
							<label class="ovcia-filter-label" for="ovcia-filter__asm_count_1">
								<div class="ovcia-filter-label-text">1</div>
								<div class="ovcia-filter-label-count"></div>
							</label>
						</div>
						<div class="ovcia-filter-button ovcia-filter-button--radio">
							<input type="radio" class="ovcia-filter-input" id="ovcia-filter__asm_count_2" data-filter-prop="count_asm" name="ovcia_filter__asm_count" value="2" />
							<label class="ovcia-filter-label" for="ovcia-filter__asm_count_2">
								<div class="ovcia-filter-label-text">&gt; 1</div>
								<div class="ovcia-filter-label-count"></div>
							</label>
						</div>
					</div>
				</div>
			</div>
			
			
		</div>

		<div class="ovcia-card-body">
			<div class="ovcia-scroll-wrap">
				<div id="ovcia-sku-dock"></div>
			</div>
		</div>
		
	</div>
	
	<div id="ovcia-assemblies-outer-wrap" class="ovcia-card">

		<div class="ovcia-card-body">
			<div class="ovcia-scroll-wrap">
				<div id="ovcia-assemblies"></div>
			</div>
		</div>
	</div>
	
	

	<div id="ovcia-sku-template" class="ovcia-sku-wrapper ovcia-sku-primary" data-ovc-id="null" data-asm-id="null" data-asmi-id="null" data-is-raw="0" data-is-case="0" data-count-asm="0" data-count-asmi="0" data-depth="null">
		<div class="ovcia-sku-block">

			<div class="ovcia-block-inner ">
				<div class="ovcia-label--top ovc-sku ovcia-sku-data">
					<div class="show-info">
						<i class="fa fa-info show-info"></i>
						<div class="ovcia-sku-block__hover-info">
							<div class="hover-data-field-wrap"><label>pr.case_sku</label><span data-ovc-fs="pr" data-ovc-field="case_sku"></span></div>
							<div class="hover-data-field-wrap"><label>pr.stock</label><span data-ovc-fs="pr" data-ovc-field="stock"></span></div>
						</div>
					</div>
					<div class="pr-ID" data-ovc-fs="pr" data-ovc-field="ID" title="OVC ID"></div>
					<div class="count-asmi" title="Total number of Assemblies for which this SKU is a child member."><i class="fa fa-level-down"></i><span>0</span></div>
					<div class="pr-is_raw" title="Indicates that SKU is a raw component of a case.">R</div>
					<div class="count-asm" title="Total number of Assemblies for which this SKU is the parent"><i class="fa fa-level-up"></i><span>0</span></div>
					<div class="pr-is_case" title="Indicates that this SKU is a case."><i class="fa fa-archive"></i></div>
					

					
				</div>
				<div class="ovcia-block-content">
					<div data-ovc-fs="pr" data-ovc-field="sku"></div>		
				</div>
			</div>

			<div class="ovcia-fb-spacer"></div>

			
			<div class="ovcia-block-inner ovcia-stat-wrap ovcia-stock-wrap ovcia-stock-available">
				<div class="ovcia-label ovcia-label--top">Available</div>
				<div class="ovcia-block-content" data-ovc-fs="pr" data-ovc-field="avail_qty"></div>
			</div>

			<div class="ovcia-block-inner ovcia-stat-wrap ovcia-stock-wrap ovcia-stock-potential">
				<div class="ovcia-label ovcia-label--top">Potential</div>
				<div class="ovcia-block-content" data-ovc-fs="pr" data-ovc-field="potential_qty"></div>
			</div>

				
			

			<div class="ovcia-block-inner ovcia-block--assembly-item">
				<div class="ovcia-label ovcia-label--top ovcia-label--assembly-item-qty" data-ovc-fs="asmi" data-ovc-field="quantity"></div>
				<div class="ovcia-block-content">
					<input type="number" value="1" min="1" step="1" class="ovcia-label ovcia-input ovcia-input--asmi__quantity" data-ovc-fs="asmi" data-ovc-field="quantity" data-ovc-id="null" data-asm-id="null" data-asmi-id="null" />
				</div>
			</div>

			
		</div>
		<div class="ovcia-assembly-wrap">
			<div class="ovcia-assembly-items-wrap">
			</div>
			<div class="ovcia-assembly-head">
				<div class="ovcia-block-inner ovcia-stat-wrap">
					

				</div>


				<div class="ovcia-block-inner ovcia-assembly-labels">
					<div class="ovcia-label ovcia-label--top" data-ovc-fs="asm" data-ovc-field="ID"></div>	
				</div>
				
				<div class="ovcia-fb-spacer"></div>

				<div class="ovcia-block-inner ovcia-stat-wrap ovcia-stock-wrap ovcia-stock-potential">
					<div class="ovcia-label ovcia-label--top">Assembled Potential</div>
					<div class="ovcia-block-content" data-ovc-fs="asm" data-ovc-field="assembled_potential"></div>
				</div>
				
				<div class="ovcia-block-inner ovcia-block--assembly-item">
					<div class="ovcia-label ovcia-label--top ovcia-label--assembly-item-qty" data-ovc-fs="asmi" data-ovc-field="quantity"></div>
					<div class="ovcia-block-content">
						<input type="number" value="1" min="1" step="1" class="ovcia-label ovcia-input ovcia-input--asm__total_quantity" data-ovc-fs="asmi" data-ovc-field="total_quantity" data-ovc-id="null" data-asm-id="null" data-asmi-id="null" />
					</div>
				</div>
				
			</div>
			
		</div>
	</div>

	<script type="application/json" id="ovcia-data">
		<?php echo json_encode( $product->get_parent_children_and_assemblies() ); ?>
	</script>
	<script type="text/javascript">
		if( typeof ovcia === 'undefined' ) {
			var ovcia;
		}
		ovcia = new ovc.assembly('<?php echo $ovcia_version; ?>');
	</script>
</div>