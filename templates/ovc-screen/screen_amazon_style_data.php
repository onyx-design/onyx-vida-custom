<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

$product = OVCDB()->get_row( FS( 'ovc_products' ), $this->args['options']['ovc_id'] );

$pd_field_order = array(
	'amazon_feed_product_type',
	'amazon_pattern_type',
	'amazon_color_map',
	'amazon_size_map',
	'amazon_size_name',

	'amazon_style_name',
	'amazon_strap_type',
	'amazon_top_style',
	'amazon_underwire_type',
	'amazon_closure_type',

	'amazon_bottom_style',

	'amazon_bullet_point_1',
	'amazon_bullet_point_2',
	'amazon_bullet_point_3',
	'amazon_bullet_point_4',
	'amazon_bullet_point_5',
	'amazon_product_description',
);
$pd_field_order = array_flip( $pd_field_order );
?>
<div class="ovc-screen-content-inner">
	<input type="hidden" name="ovc_id" class="ovc-screen-field" value="<?php echo $product->ID ;?>" />

	<h3>Amazon Style Data - Style: <?php echo $product->data( 'st.style' ); ?></h3>
	<div class="ovc-screen-col-half">
		<?php
			$st_fs = OVCSC::get_field_set( 'style_data' );

			$pd_types = array( 'General', 'Bras', 'Panties', 'Socks' );

			// Get Amazon Fields
			$pd_fieldmeta = array_filter( $st_fs->get_single_meta_key_values( 'product_type', true, false ), function( $key ) {

				if( 0 === strpos( $key, 'amazon' ) ) {
					return true;
				}

				return false;
			}, ARRAY_FILTER_USE_KEY );

			$pd_fields_count = array_count_values( array_column( $pd_fieldmeta, 'product_type' ) );

			foreach( $pd_types as $pd_type ) {

				if( isset( $pd_fields_count[ $pd_type ] ) ) {
				
					echo '<h4 class="ovc-screen-modal-subh">' . $pd_type . '</h4>';	

					$pd_fieldmeta = array_merge( $pd_field_order, $pd_fieldmeta );
					foreach( $pd_fieldmeta as $field => $meta ) {

						if( !isset( $pd_field_order[ $field ] ) ) {
							continue;
						}

						$field = OVCSC::get_field( "st.{$field}" );
						$value = $product->data( "st.{$field}" );

						$display_value = stripcslashes( $value );

						if( $pd_type == $meta['product_type'] ) {

							echo '<div class="ovc-screen-field-wrap">';
							echo "<label for=\"st.{$field}\" class=\"ovc-screen-label--wide\">{$meta['nice_name']}</label>";

							if( isset( $meta['var_type'] ) ) {

								if( 0 === strpos( $meta['var_type'], 'list' ) ) {

									$list_name = str_replace( 'list:', '', $meta['var_type'] );

									echo "<select class=\"ovc-screen-field\" name=\"{$field->global_name}\">";

									if( !$value || OVCSC::get_meta_value( 'ovc_list', $list_name, 'allow_blank' ) ) {

										$selected = $value ? '' : ' selected';

										echo "<option value=\"\" {$selected}>[None]</options>";
									}

									foreach( OVC_Lists()->get_list( $list_name ) as $list_item ) {

										$selected = $list_item == $value ? ' selected' : ''
										;
										echo "<option value=\"{$list_item}\" {$selected}>{$list_item}</option>";
									}

									echo '</select>';
								}
							} else {

								$maxlength = $field->meta( 'maxlength' ) ? "maxlength=\"{$field->meta( 'maxlength' )}\"" : '';

								if( 'amazon_product_description' == $field->local_name ) {
									echo "<textarea class=\"ovc-screen-field\" name=\"{$field->global_name}\" rows=\"4\"{$disabled}>{$display_value}</textarea>";
								} else {
									echo "<input type=\"text\" class=\"ovc-screen-field\" name=\"{$field->global_name}\" value=\"{$display_value}\" {$maxlength} />";
								}
							}
			
							echo '</div>';
						}
					}
				}
			}
		?>
	</div>

	<div class="ovc-screen-col-half">
		<?php
			$pd_types = array( 'Bullet Points / Description' );

			foreach( $pd_types as $pd_type ) {

				if( isset( $pd_fields_count[ $pd_type ] ) ) {
				
					echo '<h4 class="ovc-screen-modal-subh">' . $pd_type . '</h4>';	

					$pd_fieldmeta = array_merge( $pd_field_order, $pd_fieldmeta );
					foreach( $pd_fieldmeta as $field => $meta ) {

						if( !isset( $pd_field_order[ $field ] ) ) {
							continue;
						}

						$field = OVCSC::get_field( "st.{$field}" );
						$value = $product->data( "st.{$field}" );

						$display_value = stripcslashes( $value );

						if( $pd_type == $meta['product_type'] ) {

							echo '<div class="ovc-screen-field-wrap">';
							echo "<label for=\"st.{$field}\" class=\"ovc-screen-label--wide\">{$meta['nice_name']}</label>";

							if( isset( $meta['var_type'] ) ) {

								if( 0 === strpos( $meta['var_type'], 'list' ) ) {

									$list_name = str_replace( 'list:', '', $meta['var_type'] );

									echo "<select class=\"ovc-screen-field\" name=\"{$field->global_name}\">";

									if( !$value || OVCSC::get_meta_value( 'ovc_list', $list_name, 'allow_blank' ) ) {

										$selected = $value ? '' : ' selected';

										echo "<option value=\"\" {$selected}>[None]</options>";
									}

									foreach( OVC_Lists()->get_list( $list_name ) as $list_item ) {

										$selected = $list_item == $value ? ' selected' : ''
										;
										echo "<option value=\"{$list_item}\" {$selected}>{$list_item}</option>";
									}

									echo '</select>';
								}

							} else {

								$maxlength = $field->meta( 'maxlength' ) ? "maxlength=\"{$field->meta( 'maxlength' )}\"" : '';
								$disabled = false;

								if( 'Bullet Points / Description' == $pd_type ) {

									switch( $field->local_name ) {
										case 'amazon_bullet_point_1':
											$placeholder = OVC_External::filter_amazon_bullet_point_1( $product );
											break;
										case 'amazon_bullet_point_2':
											$placeholder = OVC_External::filter_amazon_bullet_point_2( $product );
											break;
										case 'amazon_bullet_point_3':
											$placeholder = OVC_External::filter_amazon_bullet_point_3( $product );
											break;
										case 'amazon_bullet_point_4':
											$placeholder = OVC_External::filter_amazon_bullet_point_4( $product );
											break;
										case 'amazon_bullet_point_5':
											$placeholder = OVC_External::filter_amazon_bullet_point_5( $product );
											$disabled = true;
											break;
										case 'amazon_product_description':
											$placeholder = OVC_External::filter_amazon_product_description( $product );
											break;
										default:
											$placeholder = '';
											break;
									}

									echo "<textarea class=\"ovc-screen-field\" name=\"{$field->global_name}\" rows=\"4\" placeholder=\"{$placeholder}\" {$maxlength} " . ( $disabled ? 'disabled="disabled"' : '' ) . ">{$display_value}</textarea>";
								} else {
									echo "<input type=\"text\" class=\"ovc-screen-field\" name=\"{$field->global_name}\" value=\"{$display_value}\" {$maxlength} " . ( $disabled ? 'disabled="disabled"' : '' ) . " />";
								}
							}
			
							echo '</div>';
						}
					}
				}
			}

		?>
	</div>
</div>