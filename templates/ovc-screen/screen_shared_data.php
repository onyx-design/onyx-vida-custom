<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

$product = OVCDB()->get_row( FS( 'ovc_products' ), $this->args['options']['ovc_id'] );
?>
<div class="ovc-screen-content-inner--full-width no-wrap">
	<input type="hidden" name="ovc_id" class="ovc-screen-field" value="<?php echo $product->ID ;?>" />

	<!-- Parent Data -->
	<div class="ovc-screen-col-600">
		<h3>Parent Data - SKU: <?php echo $product->parent_sku; ?></h3>
	<?php 

		$parent_data_fields = array(
			'product_title',
			'fast_facts1',
			'fast_facts2',
			'fast_facts3',
			'fast_facts4',
			'description',
			'description_spanish',
			'care',
			'care_old',
			'sizing',
			'brand',
		);

		foreach( $parent_data_fields as $field_name ) {
			
			$field = OVCSC::get_field( "pa.{$field_name}" );
			$value = $product->data( $field );
			
			$display_value = stripslashes( $value );

			echo '<div class="ovc-screen-field-wrap">';
			echo "<label for=\"{$field->global_name}\" class=\"ovc-screen-label--wide\">" . $field->meta( 'nice_name' ) . "</label>";

			if( 'brand' == $field->local_name ) {
				echo '<select class="ovc-screen-field" name="'.$field->global_name.'" >';

				if( !$value ) {
					echo '<option value ="" selected>Select a Brand...</option>';
				}

				foreach( OVC_Lists()->get_list( 'brand' ) as $brand ) {
					$selected = ( $brand == $value ? " selected" : "" );
					echo "<option value=\"{$brand}\" {$selected}>{$brand}</option>";
				}
				echo '</select>';
			}
			elseif( 'care' == $field->local_name ) {

				echo '<select class="ovc-screen-field" name="'. $field->global_name .'" style="width:100%;">';

				if( !$display_value || $field->meta( 'allow_blank' ) ) {
					$selected = ( $display_value ? "" : " selected" );

					echo "<option value=\"\" selected>[None]</option>";
				}

				foreach( OVC_Lists()->get_list( $field->local_name ) as $oc_name ) {
					$selected = ( $oc_name == $display_value ? " selected" : "" );
					echo "<option value=\"{$oc_name}\" {$selected}>{$oc_name}</option>";
				}
				echo '</select>';

				// $input_elem_args = array(
				// 	'type'	=> 'input',
				// 	'attrs'	=> array(
				// 		'type'	=> 'text',
				// 		'class'	=> array( 'ovc-screen-field' ),
				// 		'name'	=> $field->global_name,
				// 		'value'	=> $value
				// 	)
				// );

				// $input_elem = new OVC_HTML_Tag_Builder( $input_elem_args );
				// $input_elem->attr( 'list', 'datalist-' . $field->local_name );

				// echo $input_elem->get_html();

				// echo '<datalist id="datalist-' . $field->local_name . '" class="ovcdt-datalist">';

				// foreach( OVC_Lists()->get_list( $field->local_name ) as $dl_item ) {
				// 	echo '<option value="' . $dl_item . '">';
				// }

				// echo '</datalist>';
			}
			else {
				
				$maxlength = $field->meta( 'maxlength' ) ? " maxlength=\"{$field->meta( 'maxlength' )}\"" : '';

				$disabled = 'care_old' == $field->local_name ? ' disabled' : '';

				if( in_array( $field->local_name, array( 'fast_facts1', 'fast_facts2', 'fast_facts3', 'fast_facts4', 'description', 'description_spanish' ) ) ) {
				// if( 'description' == $field->local_name || 'description_spanish' == $field->local_name ) {
					echo "<textarea class=\"ovc-screen-field\" name=\"{$field->global_name}\" rows=\"4\"{$disabled}>{$display_value}</textarea>";
				}
				else {
					echo "<input type=\"text\" class=\"ovc-screen-field\" name=\"{$field->global_name}\" value=\"{$display_value}\" {$maxlength}{$disabled} />";	
				}
			}

			echo '</div>';
			
		}
	?>
	</div>

	<!-- Style Data -->
	<div class="ovc-screen-col-600">

		<h3>Style Data - Style: <?php echo $product->data( 'st.style' ); ?></h3>
		<?php 
			$style_data_fields = array(
				'product_title',
				'fast_facts1',
				'fast_facts2',
				'fast_facts3',
				'fast_facts4',
				'description',
				'description_spanish',
				'care',
				'care_old',
				'sizing',
				'additional_sizing_guides',
				// 'wc_product_category', // Duplicate field in Category Data
				'origin_country',
				'material1_name',
				'material2_name',
				'material3_name',
				'material4_name',
				'material5_name',
				'keywords',
				'internal_notes',
			);

			foreach( $style_data_fields as $field_name ) {

				$field = OVCSC::get_field( "st.{$field_name}" );
				$value = $product->data( $field );

				$display_value = stripslashes( $value );

				$label = 0 === strpos( $field->local_name, 'material' ) ? str_replace( ' Name', '', $field->meta( 'nice_name' ) ) : $field->meta( 'nice_name' );

				echo '<div class="ovc-screen-field-wrap" data-ovc-field="' . $field_name . '">';

				// Display these fields differently from the rest
				// if( !in_array( $field->local_name, array( 'additional_sizing_guides' ) ) ) {
					echo "<label for=\"{$field->global_name}\" class=\"ovc-screen-label--wide\">" . $field->meta( 'nice_name' ) . "</label>";
				// }

				if( in_array( $field->local_name, array( 'origin_country', 'product_type', 'care' ) ) ) { // dev:generalize dev:html_input-output
					echo '<select class="ovc-screen-field" name="'. $field->global_name .'" style="width:100%;">';

					if( !$display_value || $field->meta( 'allow_blank' ) ) {
						$selected = ( $display_value ? "" : " selected" );

						echo "<option value=\"\" selected>[None]</option>";
					}

					foreach( OVC_Lists()->get_list( $field->local_name ) as $oc_name ) {
						$selected = ( $oc_name == $display_value ? " selected" : "" );
						echo "<option value=\"{$oc_name}\" {$selected}>{$oc_name}</option>";
					}
					echo '</select>';
				}
				else if( in_array( $field->local_name, array( 'fast_facts1', 'fast_facts2', 'fast_facts3', 'fast_facts4', 'description', 'description_spanish', 'internal_notes', 'keywords' ) ) ) {
					$maxlength = $field->meta('maxlength') ?: '2000';

					echo "<textarea class=\"ovc-screen-field\" name=\"{$field->global_name}\" rows=\"4\" maxlength=\"{$maxlength}\">{$display_value}</textarea>";
				}
				else if( in_array( $field->local_name, array( 'wc_product_category' ) ) ) {

					$args = array(
						'hide_empty'	=> 0,
						'show_count'	=> 0,
						'selected'		=> $product->data( 'st.wc_product_category' ),
						'value_field'	=> 'name',
						'show_uncategorized' => 0,
						'name'				 => 'st.wc_product_category',
						'class'              => 'dropdown_product_cat ovc-screen-field',
					);

					wc_product_dropdown_categories( $args );
				}
				/*
				else if( 'product_cats' == $meta_key ) {

					echo "<ul id=\"product_catchecklist\" data-wp-lists=\"list:product_cat\" class=\"ovc-screen-field categorychecklist form-no-clear ovc-screen-sl-wrap\">";
					echo wp_terms_checklist( 0, array( 'taxonomy' => 'product_cat' ) );
					echo "</ul>";
				}
				*/
				else if( 'additional_sizing_guides' === $field->local_name ) {
				?>
					<div class="ovc-dynamic-field-wrap">
						<div class="ovc-dynamic-field-row">
							<div class="ovc-dynamic-field-action"><span class="fa fa-times" style="visibility: hidden;"></span></div>
							<div class="ovc-dynamic-field-label ovc-dynamic-field-header">Note</div>
							<div class="ovc-dynamic-field-label ovc-dynamic-field-header ovc-dynamic-field-value">Value</div>
						</div>

						<div class="ovc-dynamic-field-row-dock">
							<?php
							if( $value && unserialize( $value ) ) {
								foreach( unserialize( $value ) as $dynamic_field ) {
									?>
									<div class="ovc-dynamic-field-row" data-field-name="<?php echo $field->global_name; ?>">
										<div class="ovc-dynamic-field-action ovc-dynamic-field-action--delete">
											<span class="fa fa-times"></span>
										</div>
										<input class="ovc-dynamic-field" type="text" data-field-type="note" value="<?php echo $dynamic_field['note']; ?>" />
										<input class="ovc-dynamic-field" type="text" data-field-type="value" value="<?php echo $dynamic_field['value']; ?>" />
									</div>
									<?php
								}
							}
							?>
						</div>

						<div id="ovc-dynamic-field-row-template" data-field-name="<?php echo $field->global_name; ?>">
							<div class="ovc-dynamic-field-action ovc-dynamic-field-action--delete">
								<span class="fa fa-times"></span>
							</div>
							<input class="ovc-dynamic-field" type="text" data-field-type="note" />
							<input class="ovc-dynamic-field" type="text" data-field-type="value" />
						</div>

						<div class="ovc-dynamic-field-row ovc-dynamic-field-row--new">
							<div class=" ovc-dynamic-field-action ovc-dynamic-field-action--add">
								<span class="fa fa-plus"></span>
							</div>
							<input class="ovc-dynamic-field-label" type="text" disabled />
							<input class="ovc-dynamic-field-value" type="text" disabled />
						</div>
					</div>
				<?php
				}
				else {
					$maxlength = $field->meta( 'maxlength' ) ? " maxlength=\"{$field->meta( 'maxlength' )}\"" : '';

					$disabled = 'care_old' == $field->local_name ? ' disabled' : '';

					echo "<input type=\"text\" class=\"ovc-screen-field\" name=\"{$field->global_name}\" value=\"{$display_value}\"{$maxlength}{$disabled} />";

					if( 0 === strpos( $field->local_name, 'material' ) ) {
						$material_percent_field = str_replace( 'name', 'percent', $field->global_name );
						echo '<input class="ovc-screen-field ovc-material-percent" name="'.$material_percent_field.'"type="number" min="1" max="100" step="1" value="' . $product->data( $material_percent_field ) . '" />';
					}
				}
				
				
				echo '</div>';
			}

		?>
	</div>

	<!-- Category Data -->
	<div class="ovc-screen-col-600">
		<h3>Category Data</h3>

		<div class="ovc-screen-field-wrap">
			<label class="ovc-screen-label--wide">OMS Release Code</label>
			<select class="ovc-screen-field" name="st.oms_release_code">
			<?php

				$selected = empty( $product->data( 'st.oms_release_code' ) ) ? " selected" : "";

				echo "<option value=\"\" {$selected}>[None]</option>";

				foreach( OVC_Lists()->get_list( 'oms_release_code' ) as $release_code => $release_label ) {

					$selected = ( $release_code == $product->data( 'st.oms_release_code' ) ? " selected" : "" );

					$label = "{$release_label} ({$release_code})";

					echo "<option value=\"{$release_code}\" label=\"{$label}\"{$selected}>{$release_label}</option>";
				}
			?>
			</select>
		</div>

		<div class="ovc-screen-field-wrap">
			<label class="ovc-screen-label--wide">Primary Category</label>
			<?php 

				$args = array(
					'hide_empty'			=> 0,
					'show_count'			=> 0,
					'selected'				=> $product->data( 'st.wc_product_category' ),
					'value_field'			=> 'name',
					'show_uncategorized' 	=> 0,
					'name'				 	=> 'st.wc_product_category',
					'class'					=> 'dropdown_product_cat ovc-screen-field',
				);

				wc_product_dropdown_categories( $args );
			?>
		</div>

		<div class="ovc-screen-field-wrap">
			<label class="ovc-screen-label--wide">Clothing Type</label>
			<select class="ovc-screen-field" name="st.clothing_type">
			<?php

				$selected = $product->data( 'st.clothing_type' ) ? "" : " selected";

				echo "<option value=\"\" {$selected}>[None]</option>";

				foreach( OVC_Lists()->get_list( 'clothing_type' ) as $clothing_type ) {
					$selected = ( $clothing_type == $product->data( 'st.clothing_type' ) ? " selected" : "" );

					echo "<option value=\"{$clothing_type}\" {$selected}>{$clothing_type}</option>";
				}
			?>
			</select>
		</div>

		<?php
			$st_fs = OVCSC::get_field_set( 'style_data' );

			$pd_types = array( 'General', 'Bras', 'Panties', 'Socks' );

			$pd_fieldmeta = $st_fs->get_single_meta_key_values( 'product_type', true, false );


			foreach( $pd_types as $pd_type ) {

				echo '<h4 class="ovc-screen-subh">' . $pd_type . '</h4>';	

				foreach( $pd_fieldmeta as $field => $meta ) {

					// Don't include Amazon fields here!
					if( 0 === strpos( $field, 'amazon' ) ) {
						continue;
					}

					$field = OVCSC::get_field( "st.{$field}" );
					$value = $product->data( "st.{$field}" );

					$display_value = stripcslashes( $value );

					if( $pd_type == $meta['product_type'] ) {

						echo '<div class="ovc-screen-field-wrap">';
						echo "<label for=\"st.{$field}\" class=\"ovc-screen-label--wide\">{$meta['nice_name']}</label>";

						if( isset( $meta['var_type'] ) ) {

							if( 0 === strpos( $meta['var_type'], 'list' ) ) {

								$list_name = str_replace( 'list:', '', $meta['var_type'] );

								echo "<select class=\"ovc-screen-field\" name=\"{$field->global_name}\">";

								if( !$value || OVCSC::get_meta_value( 'ovc_list', $list_name, 'allow_blank' ) ) {

									$selected = $value ? '' : ' selected';

									echo "<option value=\"\" {$selected}>[None]</options>";
								}

								foreach( OVC_Lists()->get_list( $list_name ) as $list_item ) {

									$selected = $list_item == $value ? ' selected' : ''
									;
									echo "<option value=\"{$list_item}\" {$selected}>{$list_item}</option>";
								}

								echo '</select>';
							}

						} else {

							$maxlength = $field->meta( 'maxlength' ) ? "maxlength=\"{$field->meta( 'maxlength' )}\"" : '';

							echo "<input type=\"text\" class=\"ovc-screen-field\" name=\"{$field->global_name}\" value=\"{$display_value}\" {$maxlength} />";
						}
		
						echo '</div>';
					}
				}
			}

		?>
	</div>
</div>

<script type="text/javascript">
	var dynamicField = new ovc.dynamic_field();
</script>