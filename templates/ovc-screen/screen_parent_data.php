<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>
<?php
	
	$product = OVCDB()->get_row( FS( 'ovc_products' ), $this->args['options']['ovc_id'] );
?>
<div class="ovc-screen-content-inner">
	<input type="hidden" name="ovc_id" class="ovc-screen-field" value="<?php echo $product->ID ;?>" />
<?php 

	$parent_data_fields = array(
		'brand',
		'product_title',
		'fast_facts1',
		'fast_facts2',
		'fast_facts3',
		'fast_facts4',
		'sizing',
		'care',
		'description'
	);

	foreach( $parent_data_fields as $field_name ) {
		
		$field = OVCSC::get_field( "pa.{$field_name}" );
		$value = $product->data( $field );

		echo '<div class="ovc-screen-field-wrap">';
		echo "<label for=\"{$field->global_name}\" class=\"ovc-screen-label--wide\">" . $field->meta( 'nice_name' ) . "</label>";

		if( 'brand' == $field->local_name ) {
			echo '<select class="ovc-screen-field" name="'.$field->global_name.'" >';

			if( !$value ) {
				echo '<option value ="" selected>Select a Brand...</option>';
			}

			foreach( OVC_Lists()->get_list( 'brand' ) as $brand ) {
				$selected = ( $brand == $value ? " selected" : "" );
				echo "<option value=\"{$brand}\" {$selected}>{$brand}</option>";
			}
			echo '</select>';
		}
		else {
			$display_value = stripslashes( $value );

			if( 'description' == $field->local_name ) {
				echo "<textarea class=\"ovc-screen-field\" name=\"{$field->global_name}\" rows=\"4\">{$display_value}</textarea>";
			}
			else {
				echo "<input type=\"text\" class=\"ovc-screen-field\" name=\"{$field->global_name}\" value=\"{$display_value}\" />";	
			}
		}

		echo '</div>';
		
	}
?>
</div>