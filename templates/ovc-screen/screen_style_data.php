<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

$product = OVCDB()->get_row( FS( 'ovc_products' ), $this->args['options']['ovc_id'] );
?>
<div class="ovc-screen-content-inner">
	<input type="hidden" name="ovc_id" class="ovc-screen-field" value="<?php echo $product->ID ;?>" />


	<?php 
		$style_data_fields = array(
			'product_title',
			'origin_country',
			'fast_facts1',
			'fast_facts2',
			'fast_facts3',
			'fast_facts4',
			'material1_name',
			'material2_name',
			'material3_name',
			'material4_name',
			'material5_name',
			'sizing',
			'care',
			'care_old',
			'description',
			'keywords',
			'internal_notes'
		);

		foreach( $style_data_fields as $field_name ) {

			$field = OVCSC::get_field( "st.{$field_name}" );
			$value = $product->data( $field );

			$display_value = stripslashes( $value );

			$label = 0 === strpos( $field->local_name, 'material' ) ? str_replace( ' Name', '', $field->meta( 'nice_name' ) ) : $field->meta( 'nice_name' );

			echo '<div class="ovc-screen-field-wrap">';
			echo "<label for=\"{$field->global_name}\" class=\"ovc-screen-label--wide\">" . $field->meta( 'nice_name' ) . "</label>";

			if( in_array( $field->local_name, array( 'origin_country', 'product_type', 'care' ) ) ) { // dev:generalize dev:html_input-output
				echo '<select class="ovc-screen-field" name="'. $field->global_name .'" style="width:100%;">';

				if( !$display_value || $field->meta( 'allow_blank' ) ) {
					$selected = ( $display_value ? "" : " selected" );

					echo "<option value=\"\" selected>[None]</option>";
				}

				foreach( OVC_Lists()->get_list( $field->local_name ) as $oc_name ) {
					$selected = ( $oc_name == $display_value ? " selected" : "" );
					echo "<option value=\"{$oc_name}\" {$selected}>{$oc_name}</option>";
				}
				echo '</select>';
			}
			else if( in_array( $field->local_name, array( 'description', 'internal_notes', 'keywords' ) ) ) {
				$maxlength = $field->meta('maxlength') ?: '2000';

				echo "<textarea class=\"ovc-screen-field\" name=\"{$field->global_name}\" rows=\"4\" maxlength=\"{$maxlength}\">{$display_value}</textarea>";
			}
			/*
			else if( 'product_cats' == $meta_key ) {

				echo "<ul id=\"product_catchecklist\" data-wp-lists=\"list:product_cat\" class=\"ovc-screen-field categorychecklist form-no-clear ovc-screen-sl-wrap\">";
				echo wp_terms_checklist( 0, array( 'taxonomy' => 'product_cat' ) );
				echo "</ul>";
			}
			*/
			else {
				$maxlength = $field->meta( 'maxlength' ) ? " maxlength=\"{$field->meta( 'maxlength' )}\"" : '';

				$disabled = 'care_old' == $field->local_name ? ' disabled' : '';

				echo "<input type=\"text\" class=\"ovc-screen-field\" name=\"{$field->global_name}\" value=\"{$display_value}\"{$maxlength}{$disabled} />";

				if( 0 === strpos( $field->local_name, 'material' ) ) {
					$material_percent_field = str_replace( 'name', 'percent', $field->global_name );
					echo '<input class="ovc-screen-field ovc-material-percent" name="'.$material_percent_field.'"type="number" min="1" max="100" step="1" value="' . $product->data( $material_percent_field ) . '" />';
				}
			}
			
			
			echo '</div>';
		}

	?>
</div>

