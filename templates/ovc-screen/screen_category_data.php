<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
	
	$st_fs = OVCSC::get_field_set( 'style_data' );
	$product = OVCDB()->get_row( FS('ovc_products'), $this->args['options']['ovc_id'] );
?>
<div class="ovc-screen-content-inner">
	<input type="hidden" name="ovc_id" class="ovc-screen-field" value="<?php echo $product->ID ;?>" />

	<div class="ovc-screen-col-half">
		<div class="ovc-screen-field-wrap">
			<label class="ovc-screen-label--wide">OMS Release Code</label>
			<select class="ovc-screen-field" name="st.oms_release_code">
			<?php

				$selected = $product->data( 'st.oms_release_code' ) ? "" : " selected";

				echo "<option value=\"\" {$selected}>[None]</option>";

				foreach( OVC_Lists()->get_list( 'oms_release_code' ) as $release_code => $release_label ) {

					$selected = ( $release_code == $product->data( 'st.oms_release_code' ) ? " selected" : "" );

					$label = "{$release_label} ({$release_code})";

					echo "<option value=\"{$release_code}\" label=\"{$label}\"{$selected}>{$release_label}</option>";
				}
			?>
			</select>
		</div>

		<div class="ovc-screen-field-wrap">
			<label class="ovc-screen-label--wide">Primary Category</label>
			<?php 

				$args = array(
					'hide_empty'	=> 0,
					'show_count'	=> 0,
					'selected'		=> $product->data( 'st.wc_product_category' ),
					'value_field'	=> 'name',
					'show_uncategorized' => 0,
				);

				wc_product_dropdown_categories( $args );
			?>
		</div>

		<div class="ovc-screen-field-wrap">
			<label class="ovc-screen-label--wide">Clothing Type</label>
			<select class="ovc-screen-field" name="st.clothing_type">
			<?php

				$selected = $product->data( 'st.clothing_type' ) ? "" : " selected";

				echo "<option value=\"\" {$selected}>[None]</option>";

				foreach( OVC_Lists()->get_list( 'clothing_type' ) as $clothing_type ) {
					$selected = ( $clothing_type == $product->data( 'st.clothing_type' ) ? " selected" : "" );

					echo "<option value=\"{$clothing_type}\" {$selected}>{$clothing_type}</option>";
				}
			?>
			</select>
		</div>

		<?php
			$pd_types = array( 'General', 'Bras', 'Panties', 'Socks' );

			$pd_fieldmeta = $st_fs->get_single_meta_key_values( 'product_type', true, false );


			foreach( $pd_types as $pd_type ) {

				if( 'General' != $pd_type ) {
					echo '<h4 class="ovc-screen-modal-subh">' . $pd_type . '</h4>';	
				}

				foreach( $pd_fieldmeta as $field => $meta ) {

					$field = OVCSC::get_field( "st.{$field}" );
					$value = $product->data( "st.{$field}" );

					$display_value = stripcslashes( $value );

					if( $pd_type == $meta['product_type'] ) {

						echo '<div class="ovc-screen-field-wrap">';
						echo "<label for=\"st.{$field}\" class=\"ovc-screen-label--wide\">{$meta['nice_name']}</label>";

						if( isset( $meta['var_type'] ) ) {

							if( 0 === strpos( $meta['var_type'], 'list' ) ) {

								$list_name = str_replace( 'list:', '', $meta['var_type'] );

								echo "<select class=\"ovc-screen-field\" name=\"{$field->global_name}\">";

								if( !$value || OVCSC::get_meta_value( 'ovc_list', $list_name, 'allow_blank' ) ) {

									$selected = $value ? '' : ' selected';

									echo "<option value=\"\" {$selected}>[None]</options>";
								}

								foreach( OVC_Lists()->get_list( $list_name ) as $list_item ) {

									$selected = $list_item == $value ? ' selected' : ''
									;
									echo "<option value=\"{$list_item}\" {$selected}>{$list_item}</option>";
								}

								echo '</select>';
							}

						} else {

							$maxlength = $field->meta( 'maxlength' ) ? "maxlength=\"{$field->meta( 'maxlength' )}\"" : '';

							echo "<input type=\"text\" class=\"ovc-screen-field\" name=\"{$field->global_name}\" value=\"{$display_value}\" {$maxlength} />";
						}
		
						echo '</div>';
					}
				}
			}

		?>
	</div>
</div>