<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

$ovc_image = OVCDB()->get_row( FS( 'ovc_images' ), $this->args['options']['ovc_id'] );
?>
<div class="ovc-screen-content-inner">
	<input type="hidden" id="ovc_id" name="ovc_id" class="ovc-screen-field" value="<?php echo $ovc_image->ID ;?>" />

	<div class="ovc-screen-centered">
		<label for="override_image"> Choose a File <input id="override_image" name="override_image" type="file" /></label>
	</div>

</div>