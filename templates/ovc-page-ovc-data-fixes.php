<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>

<div class="ox-row">
	<h2>OVC Local Walmart Product Data</h2>
</div>


<div id="ovcdt-table-main" class="ox-row ovcdt-outer-wrap">
	<div class="ox-row ovcdt-status-row">
		<div class="ovcdt-status-wrap">
			OVC Table Status:
			<span class="ovcdt-status ovcdt-status-initializing ox-hidden">Initializing</span>
			<span class="ovcdt-status ovcdt-status-ready ox-hidden">Ready</span>
			<span class="ovcdt-status ovcdt-status-loading ox-hidden">Loading Data... <span class="ajax-loader-bg"></span></span>
			<span class="ovcdt-status ovcdt-status-saving ox-hidden">Saving Changes... <span class="ajax-loader-bg"></span></span>
			<span class="ovcdt-status ovcdt-status-error ox-hidden">ERROR!</span>
		</div>
		<div class="ovc-notice-box">
		</div>
	</div>

	<div class="ox-row ovcdt-table-wrap" data-ovcdt-type="data_fixes" data-request-defaults='{"show_filters":true,"ovcdt_version":"<?php echo OVC_Table::OVCDT_VERSION;?>"}'>
		
		
	</div>
</div>



