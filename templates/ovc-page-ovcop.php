<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>

<div class="ox-row" style="margin-bottom:3px;">
	<h2 style="margin-bottom:3px;">OVC Operations</h2>
</div>

<div class="ox-row op-window-commands">
	<div id="ovcop-notice-box" class="ovc-notice-box" style="display:inline-block;margin-bottom:6px;">
	</div>

	<br />

	<input type="submit" class="button button-primary" value="Initialize new operation" onclick="ovcop.loadOpSelection();" />
	<input type="submit" class="button button-secondary" value="Reset all operations" onclick="ovcop.resetAllOps();" />
</div>



<div id="ovcop-block-html-wrap" class="ox-row ovcop-block">
	<div id="ovcop-block-html" class="ovcop-block-inner" data-template-type="closed">
	</div>

	<div id="ovcop-log-wrap" class="ox-row ox-hidden">
		<h3>Operation Log</h3>
		<pre id="ovcop-log" class="ovcop-block-inner">
		</pre> 
	</div>
</div>




<div class="ox-row">
	<h2>OVC Operation History</h2>
</div>

<div id="ovcdt-table-main" class="ox-row ovcdt-outer-wrap">
	<div class="ox-row ovcdt-status-row">
		<div class="ovcdt-status-wrap">
			OVC Table Status:
			<span class="ovcdt-status ovcdt-status-initializing ox-hidden">Initializing</span>
			<span class="ovcdt-status ovcdt-status-ready ox-hidden">Ready</span>
			<span class="ovcdt-status ovcdt-status-loading ox-hidden">Loading Data... <span class="ajax-loader-bg"></span></span>
			<span class="ovcdt-status ovcdt-status-saving ox-hidden">Saving</span>
			<span class="ovcdt-status ovcdt-status-error ox-hidden">ERROR!</span>
		</div>
		<div class="ovc-notice-box">
		</div>
	</div>

	<div class="ox-row ovcdt-table-wrap" data-ovcdt-type="ovcop_log" data-request-defaults='{"show_filters":true,"ovcdt_version":"<?php echo OVC_Table::OVCDT_VERSION;?>"}'>
				
	</div>
</div>




