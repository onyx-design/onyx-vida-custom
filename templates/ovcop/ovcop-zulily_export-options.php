<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>
<?php
	// Get User Custom Selection (ovc_products)
	$user_cs_ids = get_ovc_user_custom_selection( get_current_user_id(), 'ovc_products' );
	
	if( count( $user_cs_ids ) ) {
		$filter_parent_checked = '';
		$hide_parent_skus_field = ' ox-hidden';
		$cs_disabled = 'checked="checked"';
	}
	else {
		$filter_parent_checked  = 'checked="checked"';
		$hide_parent_skus_field = '';
		$cs_disabled = 'disabled="disabled"';
	}

?>

<div class="ox-row">
	<h4 style="font-size:120%;">Zulily Export Options</h4>
	
	<strong>Template Format</strong><br />
	<label for="template_format-2018">
		<input type="radio" name="template_format" value="2018" id="template_format-2018" class="ovcop-user-input" data-input-toggle="2018-format" checked="checked" />
		2018
	</label>
	<br />
	<label for="template_format-event_type">
		<input type="radio" name="template_format" value="event_type" id="template_format-event_type" class="ovcop-user-input" data-input-toggle="event_type-format" />
		Event Type (Old)
	</label>
	<br />
	<br />

	<div class="togglee-2018-format">
		<div class="ox-row ovcop-field-group">
			<strong>Template Type</strong><br />
			<label for="template_type-ia">
				<input type="radio" name="template_type" value="ia" id="template_type-ia" class="ovcop-user-input" checked="checked" />
				IA
			</label>
			<br />
			<label for="template_type-kids_apparel">
				<input type="radio" name="template_type" value="kids_apparel" id="template_type-kids_apparel" class="ovcop-user-input" />
				Kids Apparel
			</label>
			<br />
			<label for="template_type-kids_sleep_undergarments">
				<input type="radio" name="template_type" value="kids_sleep_undergarments" id="template_type-kids_sleep_undergarments" class="ovcop-user-input" />
				Kids Sleep/Undergarments
			</label>
			<br />
			<label for="template_type-kids_personal_accessories">
				<input type="radio" name="template_type" value="kids_personal_accessories" id="template_type-kids_personal_accessories" class="ovcop-user-input" />
				Kids Personal/Accessories
			</label>
			<br />
			<br />
		</div>
	</div>

	<div class="togglee-event_type-format ox-hidden">
		<div class="ox-row ovcop-field-group">
			<strong>Event Type</strong><br />
			<label for="event_type-intimates">
				<input type="radio" name="event_type" value="intimates" id="event_type-intimates" class="ovcop-user-input" checked="checked" />
				Intimates
			</label>
			<br />
			<label for="event_type-sexy">
				<input type="radio" name="event_type" value="sexy" id="event_type-sexy" class="ovcop-user-input" />
				Sexy
			</label>
			<br />
			<br />
		</div>
	</div>

	<div class="ox-row ovcop-field-group">
		<strong>Output Format</strong><br />
		<label for="format-xlsx">
			<input type="radio" name="format" value="xlsx" id="format-xlsx" class="ovcop-user-input" checked="checked" />
			Excel (Recommended)
		</label>
		<br />
		<label for="format-csv">
			<input type="radio" name="format" value="csv" id="format-csv" class="ovcop-user-input" />
			CSV
		</label>
		<br />
		<br />
	</div>

	<div class="ox-row ovcop-field-group">
		<strong>SKU Filtering</strong><br />
		<label for="sku_filter-parent_skus">
			<input type="radio" name="sku_filter" value="parent_skus" id="sku_filter-parent_skus" class="ovcop-user-input" data-input-toggle="parent-skus" <?php echo $filter_parent_checked; ?> />
			Filter by parent SKUs
		</label>
				<input type="text" name="filter_parent_skus" value="" id="parent-sku-input" class="ovcop-user-input togglee-parent-skus<?php echo $hide_parent_skus_field; ?>" placeholder="Enter parent SKUs separated by commas" size="100" style="margin:5px 0;" />
		<br />
		<label for="sku_filter-custom_selection">
			<input type="radio" name="sku_filter" value="custom_selection" id="sku_filter-custom_selection" class="ovcop-user-input" <?php echo $cs_disabled ; ?> /> 
			Filter by Custom Selection? ( <?php echo count( $user_cs_ids ); ?> SKUs in custom selection )
		</label>
		<br />
		<br />
	</div>

	<button id="start-zulily_export" class="button-primary" onclick="ovcop.initOp('zulily_export');">Start Zulily Export</button>
	<button id="reselect-operation-type" class="button-secondary" onclick="ovcop.loadOpSelection();">Cancel</button>
</div>
