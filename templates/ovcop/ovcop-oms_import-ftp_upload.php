<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>

<div class="ox-row">
	<h4 style="font-size:120%;margin-top:2em;margin-bottom:0;">OMS Import FTP Initialization</h4>

	<p>You selected to use an automatic FTP upload as the CSV source for this OMS Import operation.  Please copy and paste the path below as the path to use when exporting the Inventory File from OMS.</p>

	<div class="ovc-clipboard-container">
		<input id="ovcpath" type="text" class="ovcop-file-path oms_import-remote-file" value="<?php echo "{$ovcop->meta('vida_ftp_csv_path')}" ; ?>" readonly />
		<button class="ovc-clipboard-btn" data-clipboard-target="#ovcpath" style="height: 26px; line-height: 23px;">
		    <span alt="copy to clipboard">Copy to Clipboard</span>
		</button>
	</div>
	<br />

	<input type="submit" id="start-oms_import-ftp" class="button-primary" onclick="ovcop.sendOpRequest('init_source_file');" value="Start OMS Import" />
</div>