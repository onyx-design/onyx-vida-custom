<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>

<div class="ox-row">
	<h4 style="font-size:120%;margin-top:2em;margin-bottom:0;">OMS Export Operation complete</h4>

	<?php 

		$vida_ftp_csv_path = $ovcop->meta( 'vida_ftp_csv_path' );
		if( $vida_ftp_csv_path ) {
		?>

			<p>Copy and paste the path below as the path to use when importing the OMS Export CSV into OMS.</p>
			<div class="ovc-clipboard-container">
				<input id="ovcpath" type="text" class="ovcop-file-path oms_import-remote-file" value="<?php echo $vida_ftp_csv_path ; ?>" readonly />
				<button class="ovc-clipboard-btn" data-clipboard-target="#ovcpath" style="height: 26px; line-height: 23px;">
				    <span alt="copy to clipboard">Copy to Clipboard</span>
				</button>
			</div>

		<?php
		}
		else {
			echo '<a href="' . $ovcop->op_file()->url() . '" class="button-primary" target="_blank">Download Export File</a>';
		}

	?>
</div>

<div class="ox-row">
	<?php 

		if( 'complete' == $ovcop->status ) {
		?>

			<input type="submit" id="start-oms_import" class="button-primary" onclick="ovcop.loadOpSelection('oms_import');" value="Start an OMS Import" />&nbsp;&nbsp;

		<?php
		}
	?>

	<input type="submit" id="start-oms_export" class="button-primary" onclick="ovcop.loadOpSelection('oms_export');" value="Start another OMS Export" />

	&nbsp;&nbsp;<button id="start-new-ovcop" class="button-secondary" onclick="ovcop.loadOpSelection();">Start Another Operation</button> 
</div>
