<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>
<?php 
	$ovcftp = new OVC_OMS_FTP;
	$ftp_connected = $ovcftp->is_connected();
?>

<div class="ox-row">
	<h4 style="font-size:120%;margin-top:2em;margin-bottom:0;">OMS Inventory Update Options</h4>

	<div class="ox-row ovcop-field-group">
		Import CSV Source Method<br />
		<input type="radio" name="file_source" value="ftp" id="file_source-ftp" class="ovcop-user-input" data-input-toggle="csv-ftp" <?php echo $ftp_connected ? 'checked' : 'disabled' ; ?> />Automatically retrieve file from VIDA OVC OMS FTP? &nbsp;&nbsp;<b>FTP Connection <?php echo $ftp_connected ? 'Active' : 'Failed' ; ?></b><br />
		<input type="radio" name="file_source" value="db" id="file_source-db" class="ovcop-user-input" />Use existing import data from previous OMS Inventory update<br />
		<input type="radio" name="file_source" value="upload" id="file_source-upload" class="ovcop-user-input" data-input-toggle="csv-upload" <?php echo $ftp_connected ? '' : 'checked' ; ?> />Upload CSV File<br />
		
		<label for="import_csv_upload" class="togglee-csv-upload ox-hidden">Select CSV File for upload</label>
		<input type="file" name="import_csv_upload" id="import_csv_upload" accept="text/csv" class="ovcop-user-input togglee-csv-upload <?php echo $ftp_connected ? 'ox-hidden' : '' ; ?>" />
	</div>

	<div class="ox-row ovcop-field-group">
		Import Mode:<br />
		<input type="radio" name="import_mode" value="standard" class="ovcop-user-input" checked /> Standard (RECOMMENDED!) - Stock and OMS data will not be updated if OVC tracking data does not match<br />
		<input type="radio" name="import_mode" value="force" class="ovcop-user-input" /> Force Import (CAUTION!) -  Update OVC Data even if OVC Tracking data does not match.<br />
		<input type="radio" name="import_mode" value="test" class="ovcop-user-input" /> Test Mode (Safe) - Find data issues by simulating an import to reveal data errors<br />
	</div>

	<div class="ox-row ovcop-field-group">
		<label for="check_data_errors"><input type="checkbox" name="check_data_errors" value="check_data_errors" id="check_data_errors" class="ovcop-user-input" checked /> Enable OVC Data Error checks?</label>
	</div>

	<!--div class="ox-row ovcop-field-group">
		<input type="checkbox" name="update_wc_stock" value="update_wc_stock" class="ovcop-user-input" />Update WooCommerce stock during import? (Significantly increases import time)<br />
	</div-->

	<!--button id="start-oms_import-ftp" class="button-primary togglee-csv-ftp" onclick="ovcop.initOp('oms_import');">Start OMS Import via FTP</button>
	<button id="start-oms_import-upload" class="button-primary togglee-csv-upload" onclick="ovcop.initOp('oms_import');">Start OMS Import via File Upload</button-->

	<button id="start-oms_import" class="button-primary" onclick="ovcop.initOp('oms_inventory');">Start OMS Import</button>
	<button id="reselect-operation-type" class="button-secondary" onclick="ovcop.loadOpSelection();">Cancel</button>
</div>