<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>

<div class="ox-row">
	<h4 style="font-size:120%;margin-top:2em;margin-bottom:0;"><?php echo $ovcop->optype_meta('nice_name') ; ?> Operation complete</h4>

</div>

<div class="ox-row">
	<?php 
		if( $ovcop->op_file() ) {
			echo '<a href="' . $ovcop->op_file()->url() . '" class="button-primary" target="_blank">Download Export File</a>';
		}
	?>
	<button id="start-new-ovcop" class="button-secondary" onclick="ovcop.loadOpSelection();">Start Another Operation</button> 
</div>