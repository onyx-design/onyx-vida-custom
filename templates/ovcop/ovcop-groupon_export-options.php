<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>

<div class="ox-row">
	<h4 style="font-size:120%;">Groupon Export Options</h4>

	<div class="ox-row ovcop-field-group">
		<input type="text" name="filter_parent_skus" value="" id="parent-sku-input" class="ovcop-user-input" placeholder="Enter parent SKUs separated by commas" size="100" style="margin:5px 0;" /><br />
	</div>

	<button id="start-groupon_export" class="button-primary" onclick="ovcop.initOp('groupon_export');">Start Groupon Export</button>
	<button id="reselect-operation-type" class="button-secondary" onclick="ovcop.loadOpSelection();">Cancel</button>
</div>
