<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>

<div class="ox-row">
	<h4 style="font-size:120%;">UPC Import Options</h4>

	<div class="ox-row ovcop-field-group">
		<input type="radio" name="file_source" value="upload" id="file_source-upload" class="ovcop-user-input" data-input-toggle="csv-upload" checked />Upload CSV File<br />
		
		<label for="import_csv_upload" class="togglee-csv-upload" style="margin-left: 20px;">Select CSV File for upload
			<input type="file" name="import_csv_upload" id="import_csv_upload" accept="text/csv" class="ovcop-user-input togglee-csv-upload" />
		</label>
	</div>

	<br />

	<button id="start-upc_import" class="button-primary" onclick="ovcop.initOp('upc_import');">Start UPC Import</button>
	<button id="reselect-operation-type" class="button-secondary" onclick="ovcop.loadOpSelection();">Cancel</button>
</div>
