<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

global $wpdb;

// Pre-create Walmart SKUs
$skus_to_create = OVC_Sync_Manager::get_auto_sync_count( 'walmart_bulk_create_items' );
$skus_to_cancel = OVC_Sync_Manager::get_auto_sync_count( 'walmart_cancel_items' );
$skus_to_init   = OVC_Sync_Manager::get_auto_sync_count( 'walmart_init_items' );
?>

<div class="ox-row">
	<h4 style="font-size:120%;">Walmart Sync Options</h4>

	<div class="ox-row">
		<strong>Walmart Auto Sync Counts:</strong><br />
		OVC SKUs set to sync to Walmart: <?php echo $wpdb->get_var( "SELECT COUNT(ID) FROM wp_ovc_products WHERE sync_walmart = 1" ); ?> <br />

		<br />
		Total ACTIVE Local Walmart Items: <?php echo $wpdb->get_var( "SELECT COUNT(ID) FROM wp_ovc_walmart_products WHERE sync_status = 'ACTIVE'" ); ?> <br />
		Total RETIRED Local Walmart Items: <?php echo $wpdb->get_var( "SELECT COUNT(ID) FROM wp_ovc_walmart_products WHERE sync_status = 'RETIRED'" ); ?> <br />
		Total Existing Local Walmart Items: <?php echo $wpdb->get_var( "SELECT COUNT(ID) FROM wp_ovc_walmart_products" ); ?> <br />

		<br />
		SKUs to Create (Pending): <?php echo $skus_to_create ; ?> <br />
		SKUs to Create (Pre-init): <?php echo $skus_to_init ; ?> <br />
		SKUs to Cancel Creation: <?php echo $skus_to_cancel ; ?> <br />
		NET SKUs to Create: <?php echo ( $skus_to_create + $skus_to_init - $skus_to_cancel ) ; ?> ( Pending + Pre-init - Cancel) <br />

		<br />
		SKUs to update data: <?php echo OVC_Sync_Manager::get_auto_sync_count( 'walmart_bulk_update_items' ); ?> <br />
		SKUs to update SKU: <?php echo OVC_Sync_Manager::get_auto_sync_count( 'walmart_bulk_update_skus' ); ?> <br />
		SKUs to update price: <?php echo OVC_Sync_Manager::get_auto_sync_count( 'walmart_bulk_update_prices' ); ?> <br />
		SKUs to update inventory: <?php echo OVC_Sync_Manager::get_auto_sync_count( 'walmart_bulk_update_inventory' ); ?> <br />

		<br />
		Manual SKUs to update: <?php echo OVC_Sync_Manager::get_auto_sync_count( 'walmart_bulk_update_manual' ); ?> <br/>

		<br />
		SKUS to Retire: <?php echo OVC_Sync_Manager::get_auto_sync_count( 'walmart_retire_items' ); ?> <br />
	</div>

	<!-- <div class="ox-row ovcop-field-group">

		<label for="refresh_only">
			<input type="radio" name="sync_mode" value="refresh_only" id="refresh_only" class="ovcop-user-input" checked /> 
			Refresh Only - Only update local Walmart Feed and Product Data
		</label>
		<br />

		<label for="auto_sync">
			<input type="radio" name="sync_mode" value="auto_sync" id="auto_sync" class="ovcop-user-input" data-input-toggle="sync-all-inventory" /> 
			Auto Sync - Automatically Sync Product, Price and Inventory data to Walmart
		</label>
		<br />

		<div class="ox-row ovcop-field-group">

			<div class="ovcop-field-group-children" style="margin-left: 10px;">
				<label for="sync_all" class="ox-hidden togglee-sync-all-inventory" >
					<input type="radio" name="update_mode" value="sync_all" id="sync_all" class="ovcop-user-input togglee-sync-all-inventory" /> 
					Force Update All
				</label>
				<br />

				<label for="inventory_only" class="ox-hidden togglee-sync-all-inventory" >
					<input type="radio" name="update_mode" value="inventory_only" id="inventory_only" class="ovcop-user-input togglee-sync-all-inventory" />
					Inventory Only - Only sync Walmart Inventory Data
				</label>
				<br />
			</div>

			<label for="sync_all_inventory" class="ox-hidden togglee-sync-all-inventory" >
				<input type="checkbox" name="sync_all_inventory" value="sync_all_inventory" id="sync_all_inventory" class="ovcop-user-input togglee-sync-all-inventory" />
				Force Sync All Inventory
			</label>
			<br /> 
		</div>
	</div> -->

	<div class="ox-row ovcop-field-group">

		<label for="refresh_only">
			<input type="radio" name="sync_mode" value="refresh_only" id="refresh_only" class="ovcop-user-input" />
				Refresh Only - Only update local Walmart Feed and Product Data
		</label>
		<br />

		<label for="auto_sync">
			<input type="radio" name="sync_mode" value="auto_sync" id="auto_sync" class="ovcop-user-input" data-input-toggle="auto-sync-all" checked /> 
				Auto Sync - Automatically Sync Product, Price and Inventory data to Walmart
		</label>
		<br />

		<div class="ox-row ovcop-field-group togglee-auto-sync-all">

			<div class="ovcop-field-group-children" style="margin-left: 20px;">

				<label for="sync_all">
					<input type="checkbox" name="sync_all" value="sync_all" id="sync_all" class="ovcop-user-input" />
						Force Sync All
				</label>
				<br/>
			</div>
		</div>

		<label for="custom_sync">
			<input type="radio" name="sync_mode" value="custom_sync" id="custom_sync" class="ovcop-user-input" data-input-toggle="custom-sync-options" />
				Custom Sync
		</label>
		<br />

		<div class="ox-row ovcop-field-group togglee-custom-sync-options ox-hidden">

			<div class="ovcop-field-group-children" style="margin-left: 20px;">

				<label for="sync_walmart_bulk_create_items">
					<input type="checkbox" name="sync_walmart_bulk_create_items" value="sync_walmart_bulk_create_items" id="sync_walmart_bulk_create_items" class="ovcop-user-input" checked />
						Create Items
				</label>
				<br />

				<label for="sync_walmart_bulk_update_skus">
					<input type="checkbox" name="sync_walmart_bulk_update_skus" value="sync_walmart_bulk_update_skus" id="sync_walmart_bulk_update_skus" class="ovcop-user-input" checked />
						Update SKUs
				</label>
				<br />

				<label for="sync_walmart_bulk_update_items">
					<input type="checkbox" name="sync_walmart_bulk_update_items" value="sync_walmart_bulk_update_items" id="sync_walmart_bulk_update_items" class="ovcop-user-input" data-input-toggle="update-items-sync-all" checked />
						Update Items
				</label>
				<br />

				<div class="ox-row ovcop-field-group togglee-update-items-sync-all">

					<div class="ovcop-field-group-children" style="margin-left: 20px;">

						<label for="sync_all">
							<input type="checkbox" name="sync_all" value="sync_all" id="sync_all" class="ovcop-user-input" />
								Force Sync All
						</label>
						<br/>
					</div>
				</div>

				<label for="sync_walmart_bulk_update_prices">
					<input type="checkbox" name="sync_walmart_bulk_update_prices" value="sync_walmart_bulk_update_prices" id="sync_walmart_bulk_update_prices" class="ovcop-user-input" checked />
						Update Prices
				</label>
				<br />

				<label for="sync_walmart_bulk_update_inventory">
					<input type="checkbox" name="sync_walmart_bulk_update_inventory" value="sync_walmart_bulk_update_inventory" id="sync_walmart_bulk_update_inventory" class="ovcop-user-input" checked />
						Update Inventory
				</label>
				<br />

				<?php 
					global $current_user;
					if( array_intersect( array( 'administrator', 'vida_sr_data_tech' ), $current_user->roles ) ): 
				?>
					<label for="sync_walmart_bulk_update_manual">
						<input type="checkbox" name="sync_walmart_bulk_update_manual" value="sync_walmart_bulk_update_manual" id="sync_walmart_bulk_update_manual" class="ovcop-user-input" />
							Manual Updates
					</label>
					<br />
				<?php endif; ?>
			</div>
		</div>
	</div>

	<button id="start-wc_sync" class="button-primary" onclick="ovcop.initOp('walmart_sync');">Start Walmart Sync</button>
	<button id="reselect-operation-type" class="button-secondary" onclick="ovcop.loadOpSelection();">Cancel</button>
</div>
