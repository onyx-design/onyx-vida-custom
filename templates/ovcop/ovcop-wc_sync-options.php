<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>

<div class="ox-row">
	<h4 style="font-size:120%;">WC Sync Options</h4>

	<div class="ox-row ovcop-field-group">
		<label for="auto_sync"><input type="radio" name="sync_mode" value="auto_sync" id="auto_sync" class="ovcop-user-input" checked /> Auto Sync <strong>( <?php echo OVC_Sync_Manager::get_auto_sync_count( 'wc_sync' ); ?> OVC IDs need to be synced )</strong></label> <br />
		<label for="sync_all"><input type="radio" name="sync_mode" value="sync_all" id="sync_all" class="ovcop-user-input" /> Sync All</label> <br />
<!-- 		<label for="ovc_ids"><input type="radio" name="sync_mode" value="ovc_ids" id="ovc_ids" class="ovcop-user-input" data-input-toggle="input-ovc-ids" /> Enter OVC IDs</label>
			<input type="text" name="sync_mode_input" value="" id="sync_mode_input" class="ovcop-user-input togglee-input-ovc-ids ox-hidden" placeholder="12005,12006,12007,..." size="100" /><br /> -->
		<label for="parent_skus"><input type="radio" name="sync_mode" value="parent_skus" id="parent_skus" class="ovcop-user-input" data-input-toggle="input-parent-ids" /> Enter Parent SKUs</label>
			<input type="text" name="sync_mode_input" value="" id="sync_mode_input" class="ovcop-user-input togglee-input-parent-ids ox-hidden" placeholder="001,002,003,..." size="100" /><br />
	</div>

	<button id="start-wc_sync" class="button-primary" onclick="ovcop.initOp('wc_sync');">Start WC Sync</button>
	<button id="reselect-operation-type" class="button-secondary" onclick="ovcop.loadOpSelection();">Cancel</button>
</div>
