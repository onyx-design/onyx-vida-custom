<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>
<?php 
	// Check FTP Connection
	$ovcftp = new OVC_OMS_FTP;
	$ftp_connected = $ovcftp->is_connected();

	// Get User Custom Selection (ovc_products)
	$user_cs_ids = get_ovc_user_custom_selection( get_current_user_id(), 'ovc_products' );
	$cs_disabled = count( $user_cs_ids ) ? '' : 'disabled="disabled"';

	// Determine if user can export to format1
	$format1_disabled = 'disabled';
	global $current_user;
	if( array_intersect( array( 'administrator', 'vida_sr_data_tech' ), $current_user->roles ) ) {
		$format1_disabled = '';
	}
?>

<div class="ox-row">
	<h4 style="font-size:120%;">OMS Export Options</h4>

	<div class="ox-row ovcop-field-group">
		<input type="checkbox" name="use_ftp" value="use-ftp" id="use-ftp" class="ovcop-user-input" <?php echo $ftp_connected ? '' : 'disabled' ; ?> />Autosave file to VIDA OVC OMS FTP? &nbsp;&nbsp;<b>FTP Connection <?php echo $ftp_connected ? 'Active' : 'Failed' ; ?></b><br />
		<br />
	</div>

	<div class="ox-row ovcop-field-group">
		<strong>Export Format</strong><br />
		<input type="radio" name="export_format" value="oms_product_data" id="export_format-oms_product_data" class="ovcop-user-input" checked /> Export OMS Product Data<br />
		<input type="radio" name="export_format" value="_oms_field" id="export_format-_oms_field" class="ovcop-user-input" <?php echo $format1_disabled ; ?> /> Export OMS Format 1<br />
		<br />
	</div>

	<div class="ox-row ovcop-field-group">
		<strong>SKU Filtering</strong><br />
		<input type="radio" name="sku_filter" value="auto" id="sku_filter-auto" class="ovcop-user-input" checked /> Auto export all SKUs that have been updated since the last export. <strong>( <?php echo OVC_Sync_Manager::get_auto_sync_count( 'oms_product_data' ) ; ?> OVC IDs need to get UPDATED, <?php echo OVC_Sync_Manager::get_auto_sync_count( 'oms_product_data_import' ) ; ?> OVC IDs need to get IMPORTED )</strong><br />
		<input type="radio" name="sku_filter" value="none" id="sku_filter-none" class="ovcop-user-input" /> Export All Approved SKUs<br />
		<input type="radio" name="sku_filter" value="parent_skus" id="sku_filter-parent_skus" class="ovcop-user-input" data-input-toggle="parent-skus" /> Filter by parent SKUs
			<input type="text" name="filter_parent_skus" value="" id="parent-sku-input" class="ovcop-user-input togglee-parent-skus ox-hidden" placeholder="Enter parent SKUs separated by commas" size="100" style="margin:5px 0;" /><br />
		<input type="radio" name="sku_filter" value="custom_selection" id="sku_filter-custom_selection" class="ovcop-user-input" <?php echo $cs_disabled ; ?> /> Filter by Custom Selection? ( <?php echo count( $user_cs_ids ); ?> SKUs in custom selection )<br />
		<br />
	</div>

	<!--div class="ox-row ovcop-field-group">
		<strong>Output Chunking</strong><br />
		<input type="checkbox" name="chunk_output" value="chunk_output" id="chunk-output" class="ovcop-user-input" /> Split export into multiple CSVs? (2000 SKUs per CSV)
	</div-->

	<button id="start-oms_export" class="button-primary" onclick="ovcop.initOp('oms_export');">Start OMS Export</button>
	<button id="reselect-operation-type" class="button-secondary" onclick="ovcop.loadOpSelection();">Cancel</button>
</div>
