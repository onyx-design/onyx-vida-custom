<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;
?>

<div class="ox-row">
	<h4 style="font-size:120%;">SKU Change Options</h4>

	<div class="ox-row ovcop-field-group">
		<strong>Change Mode</strong><br />
		<input type="radio" name="change_mode" value="input_change" id="input_change" class="ovcop-user-input" data-input-toggle="input-skus" /> <label for="input_change">Enter SKUs</label>
			<input type="text" name="change_mode_input" value="" id="change-sku-input" class="ovcop-user-input togglee-input-skus ox-hidden" placeholder="old_sku1:new_sku1,old_sku2:new_sku2,..." size="100" style="margin:5px 0;" /><br />
	</div>

	<button id="start-sku_change" class="button-primary" onclick="ovcop.initOp('sku_change');">Start SKU Change</button>
	<button id="reselect-operation-type" class="button-secondary" onclick="ovcop.loadOpSelection();">Cancel</button>
</div>