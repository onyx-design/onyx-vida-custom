<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>
<?php
	// Get User Custom Selection (ovc_products)
	$user_cs_ids = get_ovc_user_custom_selection( get_current_user_id(), 'ovc_products' );
	
	if( count( $user_cs_ids ) ) {
		$filter_parent_checked = '';
		$hide_parent_skus_field = ' ox-hidden';
		$cs_disabled = 'checked="checked"';
	}
	else {
		$filter_parent_checked  = 'checked="checked"';
		$hide_parent_skus_field = '';
		$cs_disabled = 'disabled="disabled"';
	}

?>

<div class="ox-row">
	<h4 style="font-size:120%;">Dropship Export Options</h4>

	<div class="ox-row ovcop-field-group">
		<strong>SKU Filtering</strong><br />
		<input type="radio" name="sku_filter" value="parent_skus" id="sku_filter-parent_skus" class="ovcop-user-input" data-input-toggle="parent-skus" <?php echo $filter_parent_checked; ?> /> Filter by parent SKUs
			<input type="text" name="filter_parent_skus" value="" id="parent-sku-input" class="ovcop-user-input togglee-parent-skus<?php echo $hide_parent_skus_field; ?>" placeholder="Enter parent SKUs separated by commas" size="100" style="margin:5px 0;" /><br />
		<input type="radio" name="sku_filter" value="custom_selection" id="sku_filter-custom_selection" class="ovcop-user-input" <?php echo $cs_disabled ; ?> /> Filter by Custom Selection? ( <?php echo count( $user_cs_ids ); ?> SKUs in custom selection )<br />
		<br />
	</div>

	<button id="start-dropship_export" class="button-primary" onclick="ovcop.initOp('dropship_export');">Start Dropship Export</button>
	<button id="reselect-operation-type" class="button-secondary" onclick="ovcop.loadOpSelection();">Cancel</button>
</div>
