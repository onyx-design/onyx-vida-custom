<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>
<?php
	global $wpdb;
	$ovcop_meta = OVCSC::get_field_set( 'ovcop' );
?>

	<div class="ox-row">
		<h4 style="font-size:120%;margin-top:2em;margin-bottom:0;">Select an Operation Type</h4>

		<table id="ovcop-type-select" class="">
			<thead>
				<tr>
					<th></th>
					<th>Operation Type</th>
					<th>Description</th>
					<th>Last Run</th>
					<th>Time Since Last Run</th>
				</tr>
			</thead>

			<tbody>

				<?php
				// Output a row for each operation type
				$op_start_buttons = '';
				foreach( $ovcop_meta->get_fields() as $op_type ) {

					// Check if current user has access to this operation type
					$user_can_run_op_type = false;
					foreach( (array) $op_type->meta( 'allowed_roles' ) as $allowed_role ) {
						if( current_user_can( $allowed_role ) ) {
							$user_can_run_op_type = true;
							break;
						}
					}

					if( !$user_can_run_op_type ) {
						continue;
					}

					$last_run = $wpdb->get_var( "SELECT MAX( start_time ) FROM wp_ovcop_log WHERE type = '{$op_type}'" );

					$since_last_run = 'n/a';
					if( $last_run ) {
						$last_run = new DateTime( $last_run );
						$since_last_run = $last_run->diff( new DateTime( current_time( 'mysql', 1 ) ) )->format( '%a Days, %h Hours, %i Minutes, %s Seconds ago' );
						$last_run = $last_run->format(DateTime::COOKIE);
					} 
					else {
						$last_run = 'Never Run';
					}

					echo '<tr>';
						echo '<td><input type="radio" class="ovcop-user-input" name="operation-type-select" value="' . $op_type . '" data-input-toggle="' . $op_type . '" />';
						echo "<td>{$op_type->meta( 'nice_name' )}</td>";
						echo "<td>{$op_type->meta( 'descrip' )}</td>";
						echo "<td>{$last_run}</td>";
						echo "<td>{$since_last_run}</td>";
					echo '</tr>';

					$op_start_buttons .= '<input type="submit" id="start-' . $op_type . '" class="button-primary togglee-' . $op_type . ' ox-hidden" onclick="ovcop.selectOp(\'' . $op_type . '\')" value="Start ' . $op_type->nice_name . '"  />';
				}

				?>

			</tbody>
		</table>
		
		<div class="ox-row">
			<?php echo $op_start_buttons; ?>
		</div>
	</div>

	<?php 
	//}
	?>