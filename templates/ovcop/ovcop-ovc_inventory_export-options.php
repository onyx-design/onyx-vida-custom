<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>

<div class="ox-row">
	<h4 style="font-size:120%">OVC Inventory Export Options</h4>

	<div class="ox-row ovcop-field-group">
		<strong>Output Format</strong>
		<br />
		<label for="format-inventory">
			<input type="radio" name="export_format" value="inventory" id="format-inventory" class="ovcop-user-input" checked />
			Inventory
		</label>
		<br />
		<label for="format-inventory_assembly">
			<input type="radio" name="export_format" value="inventory_assembly" id="format-inventory_assembly" class="ovcop-user-input" />
			Inventory with Assemblies
		</label>
		<br />
	</div>
	<div class="ox-row ovcop-field-group">
		<label for="use_in_stock_quantity">
			<input type="checkbox" name="use_in_stock_quantity" value="use_in_stock_quantity" id="use_in_stock_quantity" class="ovcop-user-input" />Use In Stock Quantity
		</label>
		<br />
		<br />
	</div>

	<button id="start-ovc_inventory_export" class="button-primary" onclick="ovcop.initOp('ovc_inventory_export');">Start OVC Inventory Export</button>
	<button id="reselect-operation-type" class="button-secondary" onclick="ovcop.loadOpSelection();">Cancel</button>
</div>