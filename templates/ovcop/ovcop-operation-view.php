<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

// Determine user name to display

$user_name = 'AUTO SYNC';
if( $ovcop->user_id ) {
	$op_userdata = get_userdata( $ovcop->user_id );
	$user_name = $op_userdata->user_firstname;	
}

?>

<div class="ox-row">
	<h3>View Operation Details - OVCOP ID: <?php echo $ovcop->ID ; ?></h3>

	<p class="ox-row ovcop-field-group">
		<b>Latest active or unfinished operation information:</b><br />
		
		<span style="display:inline-block;width:70px;">OP ID:</span> <?php echo $ovcop->ID ; ?><br />
		<span style="display:inline-block;width:70px;">User:</span> <?php echo $user_name ; ?><br />
		<span style="display:inline-block;width:70px;">Type:</span> <?php echo $ovcop->type ; ?><br />
		<span style="display:inline-block;width:70px;">Status:</span> <?php echo $ovcop->status ; ?><br />
		<span style="display:inline-block;width:70px;">Started:</span> <?php echo $ovcop->start_time ; ?><br />
		<span style="display:inline-block;width:70px;">Ended:</span> <?php echo $ovcop->end_time ; ?><br />
		<span style="display:inline-block;width:70px;">Last Seen:</span> <?php echo $ovcop->last_seen ; ?>
	</p>

	<?php

		if( in_array( $ovcop->status, OVCOP::$running_statuses ) ) {
			?>

			<button id="user-abort-ovcop" class="button-primary" onclick="ovcop.abortOp(<?php echo $ovcop->ID; ?>);">Abort Operation</button>

			<?php
		}
		else if( in_array( $ovcop->status, OVCOP::$finished_statuses ) ) {

			echo $this->load_template_generalized( 'complete', $ovcop );
		}

	?>

	
	
</div>

