<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>

<div class="ox-row">
	<h4 style="font-size:120%;">Shopify Sync Options</h4>

	<div class="ox-row ovcop-field-group">
		<label for="auto_sync">
			<input type="radio" name="sync_mode" value="auto_sync" id="auto_sync" class="ovcop-user-input" checked /> Auto Sync
		</label> 
		<br />

		<label for="sync_all">
			<input type="radio" name="sync_mode" value="sync_all" id="sync_all" class="ovcop-user-input" /> Force Sync All
		</label>
		<br />

		<label for="parent_skus">
			<input type="radio" name="sync_mode" value="parent_skus" id="parent_skus" class="ovcop-user-input" data-input-toggle="input-parent-ids" /> Enter Parent SKUs
		</label>
		<input type="text" name="sync_mode_input" value="" id="sync_mode_input" class="ovcop-user-input togglee-input-parent-ids ox-hidden" placeholder="1,2,3,..." size="100" style="margin:5px 0;" />
		<br />

		<!-- <label for="force_sync"><input type="checkbox" name="force_sync" value="force_sync" id="force_sync" class="ovcop-user-input" checked /> Force Sync</label> <br /> -->
		<label for="shopify_back_sync"><input type="checkbox" name="shopify_back_sync" value="shopify_back_sync" id="shopify_back_sync" class="ovcop-user-input" /> Back Sync from Shopify</label>
		<br />
	</div>

	<button id="start-shopify_sync" class="button-primary" onclick="ovcop.initOp('shopify_sync');">Start Shopify Sync</button>
	<button id="reselect-operation-type" class="button-secondary" onclick="ovcop.loadOpSelection();">Cancel</button>
</div>
