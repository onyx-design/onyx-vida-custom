<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>

<div class="ox-row">
	<h4 style="font-size:120%;">Global SKU Change Export Options</h4>

	<div class="ox-row ovcop-field-group">
		<strong>Output Format</strong><br />
		<input type="radio" name="format" value="csv" id="format-csv" class="ovcop-user-input" checked="checked" />CSV<br />
		<br />
	</div>

	<button id="start-global_sku_change_export" class="button-primary" onclick="ovcop.initOp('global_sku_change_export');">Start Global SKU Change Export</button>
	<button id="reselect-operation-type" class="button-secondary" onclick="ovcop.loadOpSelection();">Cancel</button>
</div>
