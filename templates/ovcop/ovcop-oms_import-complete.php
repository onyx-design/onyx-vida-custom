<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>

<div class="ox-row">
	<h4 style="font-size:120%;margin-top:2em;margin-bottom:0;">OMS Import Operation complete</h4>

</div>

<div class="ox-row">
	<button id="start-new-ovcop" class="button-secondary" onclick="ovcop.loadOpSelection();">Start Another Operation</button> 
</div>