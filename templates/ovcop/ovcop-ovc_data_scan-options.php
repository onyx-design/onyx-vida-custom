<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>

<div class="ox-row">
	<h4 style="font-size:120%;">OVC Data Scan Options</h4>

	<div class="ox-row ovcop-field-group">
		<label for="data_errors">
			<input type="checkbox" name="data_errors" value="data_errors" id="data_errors" class="ovcop-user-input" data-input-toggle="data-errors" checked />
			Data Scan for Errors
		</label>
		<br />

		<div class="ox-row ovcop-field-group togglee-data-errors">
			<div class="ovcop-field-group-children" style="margin-left: 20px;">

				<label for="auto_scan">
					<input type="radio" name="scan_mode" value="auto_scan" id="auto_scan" class="ovcop-user-input" checked />
					Auto Scan <strong>( <?php echo OVC_Sync_Manager::get_auto_sync_count( 'ovc_data_scan' ); ?> OVC IDs need to be synced )</strong>
					<br />
				</label>
				<label for="scan_all">
					<input type="radio" name="scan_mode" value="scan_all" id="scan_all" class="ovcop-user-input" />
					Scan All IDs
					<br />
				</label>
			</div>
		</div>
		<?php if( 2 == get_current_user_id() ) { ?>
		<label for="setup_assemblies"><input type="checkbox" name="setup_assemblies" value="setup_assemblies" id="setup_assemblies" class="ovcop-user-input" /> Setup Assemblies</label>
		<br />
		<label for="clean_archive_ovcops"><input type="checkbox" name="clean_archive_ovcops" value="clean_archive_ovcops" id="clean_archive_ovcops" class="ovcop-user-input" /> Clean and Archive OVC OPs</label>
		<br />
		<?php } ?>
	</div>

	<button id="start-ovc_data_scan" class="button-primary" onclick="ovcop.initOp('ovc_data_scan');">Start OVC Data Scan</button>
	<button id="reselect-operation-type" class="button-secondary" onclick="ovcop.loadOpSelection();">Cancel</button>
</div>
