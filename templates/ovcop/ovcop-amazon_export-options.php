<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

// Get User Custom Selection (ovc_products)
$user_cs_ids = get_ovc_user_custom_selection( get_current_user_id(), 'ovc_products' );

if( count( $user_cs_ids ) ) {
	$filter_parent_checked = '';
	$hide_parent_skus_field = ' ox-hidden';
	$cs_disabled = 'checked="checked"';
}
else {
	$filter_parent_checked  = 'checked="checked"';
	$hide_parent_skus_field = '';
	$cs_disabled = 'disabled="disabled"';
}
?>

<div class="ox-row">
	<h4 style="font-size:120%;">Amazon Export Options</h4>

	<div class="ox-row ovcop-field-group">
		<strong>Output Format</strong>
		<br />
		<label for="format-excel">
			<input type="radio" name="format" value="excel" id="format-excel" class="ovcop-user-input" data-input-toggle="excel-options" checked="checked" />
			Excel (Recommended)
		</label>
		<br />
		<div class="ox-row ovcop-field-group togglee-excel-options">
			<div class="ovcop-field-group-children" style="margin-left: 20px;">
				<label for="excel_2019_02_08">
					<input type="radio" name="excel_template_file" value="excel_2019_02_08" id="excel_2019_02_08" class="ovcop-user-input" checked />
					Version 2019.0208 (Recommended)
				</label>
				<br />

				<label for="excel_2016_07_21">
					<input type="radio" name="excel_template_file" value="excel_2016_07_21" id="excel_2016_07_21" class="ovcop-user-input" />
					Version 2016.0721
				</label>
			</div>
		</div>
		<label for="format-csv">
			<input type="radio" name="format" value="csv" id="format-csv" class="ovcop-user-input" />
			CSV
		</label>
		<br />
		<br />
	</div>

	<div class="ox-row ovcop-field-group">
		<strong>SKU Filtering</strong><br />
		<label for="sku_filter-parent_skus">
			<input type="radio" name="sku_filter" value="parent_skus" id="sku_filter-parent_skus" class="ovcop-user-input" data-input-toggle="parent-skus" <?php echo $filter_parent_checked; ?> />
			Filter by parent SKUs
			<input type="text" name="filter_parent_skus" value="" id="parent-sku-input" class="ovcop-user-input togglee-parent-skus<?php echo $hide_parent_skus_field; ?>" placeholder="Enter parent SKUs separated by commas" size="100" style="margin:5px 0;" />
		</label>
		<br />
		<label for="sku_filter-custom_selection">
			<input type="radio" name="sku_filter" value="custom_selection" id="sku_filter-custom_selection" class="ovcop-user-input" <?php echo $cs_disabled ; ?> />
			Filter by Custom Selection? ( <?php echo count( $user_cs_ids ); ?> SKUs in custom selection )
		</label>
		<br />
	</div>

	<button id="start-amazon_export" class="button-primary" onclick="ovcop.initOp('amazon_export');">Start Amazon Export</button>
	<button id="reselect-operation-type" class="button-secondary" onclick="ovcop.loadOpSelection();">Cancel</button>
</div>
