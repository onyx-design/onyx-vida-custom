<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>

<div class="ox-row">

	<h4 style="font-size:120%;">Analyze Images Options</h4>

	<div class="ox-row ovcop-field-group">
		<strong>Analysis Type</strong><br />
		<label for="analysis_type-generation">
			<input type="radio" name="analysis_type" value="generation" id="analysis_type-generation" class="ovcop-user-input" checked="checked" /> Refresh Image Tables
		</label>
		<br />
		<label for="analysis_type-cropability">
			<input type="radio" name="analysis_type" value="cropability" id="analysis_type-cropability" class="ovcop-user-input" /> Refresh Image Tables <strong>AND</strong> Analyze Cropability
		</label>
		<br />
	</div>

	<div class="ox-row ovcop-field-group">
		<label for="copy_to_walmart_ftp">
			<input type="checkbox" name ="copy_to_walmart_ftp" value="copy_to_walmart_ftp" id="copy_to_walmart_ftp" class="ovcop-user-input" checked /> Copy Images to Walmart FTP
		</label>
	</div>

	<?php 
		global $current_user;
		if( array_intersect( array( 'administrator', 'vida_sr_data_tech' ), $current_user->roles ) ):
			?>
				<div class="ox-row ovcop-field-group">
					<strong>Image Cleanup</strong><br />
					<label for="delete_unused_images">
						<input type="checkbox" name="delete_unused_images" value="delete_unused_images" id="delete_unused_images" class="ovcop-user-input" /> Delete Unattached Images<br>WARNING! This will delete all images in the 'Unattached Image Post IDs' table that are not marked as 'In Use'
					</label>
				</div>
			<?php 
		endif;
	?>

	<button id="start-analyze_images" class="button-primary" onclick="ovcop.initOp('analyze_images');">Analyze Images</button>
	<button id="reselect-operation-type" class="button-secondary" onclick="ovcop.loadOpSelection();">Cancel</button>
</div>
