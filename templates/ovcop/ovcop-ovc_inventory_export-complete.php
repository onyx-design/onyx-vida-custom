<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>

<div class="ox-row">
	<h4 style="font-size:120%;margin-top:2em;margin-bottom:0;">OVC Inventory Export Operation complete</h4>

</div>

<div class="ox-row">
	<?php
		foreach( array_keys( $ovcop->files ) as $file_id ) {

			$ovc_file = new OVC_File( $file_id );

			if( in_array( $ovc_file->file_type, array( 'inventory_export', 'assembly_parents_export', 'assembly_items_export' ) ) ) {
				echo '<a href="' . $ovc_file->url() . '" class="button-primary" target="_blank">Download ' . ucwords( str_replace( '_', ' ', $ovc_file->file_type ) ) . ' File</a>';
				echo '<br />';
				echo '<br />';
			}
		}
	?>
	<button id="start-new-ovcop" class="button-secondary" onclick="ovcop.loadOpSelection();">Start Another Operation</button> 
</div>