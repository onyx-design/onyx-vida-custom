<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>

<div class="ox-row">
	<button id="user-abort-ovcop" class="button-secondary" onclick="ovcop.sendOpRequest('user_abort');">Abort Operation</button>
	<h4 style="font-size:120%;margin-top:2em;margin-bottom:0;"><?php echo $ovcop->optype_meta('nice_name') ; ?> Operation in Progress</h4>

	View operation log below.
</div>
