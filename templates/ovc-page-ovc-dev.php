<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
ini_set("memory_limit", '512M' );
ini_set("max_execution_time", 600 );
ini_set("auto_detect_line_endings", true);

// Exit if not ONYX Admin
// DEV TODO: Improve & Generalize Admin Roles & Access
if( 2 != get_current_user_id() ) {
	header( "Location: http://vida-us.com/wp-admin/admin.php?page=ovc" );
}
?>

<?php
// Easy Line breaks
define( 'BR', "<br />");
$br = "<br />";
if( !function_exists('br') ) {function br($msg='') {echo ( is_null( $msg ) ? 'NULL' : ( is_scalar( $msg ) ? ( is_bool( $msg ) ? boolstr( $msg )."<br />" : "$msg<br />" ) : print_r( $msg, true ) ) );}}
if( !function_exists('tr') ) {function tb($end=0) {echo $end ? "</table>" : "<table>";}}
if( !function_exists('tr') ) {function tr($end=0) {echo $end ? "</tr>" : "<tr>";}}
if( !function_exists('td') ) {function td($val=0) {echo "<td>{$val}</td>";}}

if( !function_exists('boolstr') ) {function boolstr($b) {return $b ? 'true' : 'false';}}


function ovc_dev_list_results_msgs( $results = false ) {OVC_Results::maybe_init( $results );foreach( $results->get_msgs() as $msg ) {br( $msg );}}

error_reporting(E_ALL);
ini_set("log_errors", 1); 
//ini_set("display_errors", 1);

function ovc_fix_colors( $ovc_id, &$results ) {
	
	$row = OVCDB()->get_row( 'ovc_products', $ovc_id, $results );
	
	if( $row->exists() ) {
		$row->on_update_sku_color_get_color_long_name( $results );
		$row->update_siblings_color_long_name( $results );
		$row->update_siblings_color_full_names( $results );
		return true;
	}
	else {
		return false;
	}
}

function ovc_import_case_skus( $file_name ) {

	//$row_keys = array('ovc_id','sku_color_old','sku__old','sku_color_new','sku_pkqty_new','sku_new','sku_status');
	$row_keys = array('ovc_id','sku_color_new','sku_pkqty_new','sku_new');

	$csv_stream = fopen( OVC_PATH . '/' . $file_name, 'r' );

	$counter = 0;

	global $wpdb;

	while( ( $csv_row = fgetcsv( $csv_stream, 200 ) ) !== false ) {

		$csv_row = array_combine( $row_keys, $csv_row );

		$results = new OVC_Results();

		$row = OVCDB()->get_row( 'ovc_products', $csv_row['ovc_id'], $results );

		if( !$row->exists() ) {
			br( 'NO ROW: ' . $csv_row['ovc_id'] );
			continue;
		}

		$old_sku = $row->sku;

		$new_sku = $csv_row['sku_new'];


		
		$updated_data = array(
			'sku_color'	=> $csv_row['sku_color_new'],
			'sku_pkqty'	=> $csv_row['sku_pkqty_new'],
			'sku'		=> $new_sku,
			'case_sku'	=> $new_sku,
			'oms_sku_confirmed' => $new_sku
		);

		$wpdb->update( 'wp_ovc_products', $updated_data, array( 'ID' => $row->ID ) );


		$case_sku_result = $wpdb->query( "UPDATE wp_ovc_products SET case_sku = '{$new_sku}' WHERE case_sku = '{$old_sku}'" );

		if( $case_sku_result ) {
			br( "Updated {$case_sku_result} case skus to {$new_sku} - OVC ID:" . $row->ID );
		}
		else {
			br( "No case skus updated for OVC ID: " . $row->ID );
		}		
	}

	br();br();
	br( "count:" . $counter );

	fclose( $csv_stream );
}

function check_new_case_skus() {

	global $wpdb;

	$csv_stream = fopen( OVC_PATH . '/case_sku_global_changes.csv', 'r' );

	while( ( $csv_row = fgetcsv( $csv_stream, 200 ) ) !== false ) {

		$new_sku = $csv_row[1];

		$sku_count = (int) $wpdb->get_var( "SELECT COUNT(ID) FROM wp_ovc_products WHERE sku = '{$new_sku}'" );
		if( 1 !== $sku_count ) {
			br( "Error for sku: $new_sku   - count: $sku_count" );
		}
	}

	fclose( $csv_stream );
}

function check_new_case_sku_pkqtys( $filename ) {
	
	$csv_stream = fopen( OVC_PATH . '/' . $filename, 'r' );

	$line = 0;
	while( ( $row = fgetcsv( $csv_stream, 200 ) ) !== false ) {
		$line++;

		$sku = $row[3];
		$sku_pieces = explode( '_', $sku );
		$sku_pkqty = $sku_pieces[2];

		if( $row[2] != $sku_pkqty ) {
			br( "$filename $line - pkqty mismatch" );
		}
	}

	fclose( $csv_stream );
}

function shopify_hard_reset() {

	if( !on_vida_live() && 'true' == $_GET['shopify_hard_reset'] ) {
		global $wpdb;
		$wpdb->query( "TRUNCATE TABLE wp_ovc_shopify_products" );
		$wpdb->query( "ALTER TABLE wp_ovc_shopify_products AUTO_INCREMENT = 1" );
		
		$wpdb->query( "TRUNCATE TABLE wp_ovc_shopify_parents" );
		$wpdb->query( "ALTER TABLE wp_ovc_shopify_parents AUTO_INCREMENT = 1" );

		$wpdb->query( "TRUNCATE TABLE wp_ovc_shopify_images" );
		$wpdb->query( "ALTER TABLE wp_ovc_shopify_images AUTO_INCREMENT = 1" );

		$wpdb->query( "DELETE FROM wp_postmeta WHERE meta_key = 'shopify_img_id'" );	
	}
}

function migration_test() {

	global $wpdb;

	$wpdb->query( "DELETE FROM {$wpdb->prefix}ovc_schema WHERE field_set = 'productmeta'" );

	OVCSC::delete_field_meta( 'style_data', 'oms_release_code', 'after_update' );

	OVCSC::delete_field_meta( 'field_set_meta', 'ovc_products', 'validation_override_func' );
}

function manual_import_oms_release_code() {
	$csv_file = OVCOP_TEMPLATE_DIR . '/20200325-new_release_codes.csv';

	$csv_results_file = OVCOP_TEMPLATE_DIR . '/20200325-oms_release_code-IMPORT_RESULTS.csv';
	file_put_contents( $csv_results_file, "Style,Import OMS Release Code,RESULT,New OMS Release Code (in OVC),Error Message\r\n");
	
	$st_fs = OVCSC::get_field_set( 'style_data' );

	$stats = array(
		'processed' => 0,
		'success' => 0,
		'error' => 0,
		'not_exists' => 0,
		'no_result'	=> 0
	);
	
	if (($handle = fopen($csv_file, "r")) !== FALSE) {
		while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
			$style = trim($data[0]);
			$updated_release = trim($data[1]);
			
			$row_results = new OVC_Results;
			$row_outcome = 'no_result';
			
			$st_row = OVCDB()->get_row_by_valid_id( $st_fs, "style", $style, $row_results );
			
			if ($st_row->exists()){

				$st_row->update_field("st.oms_release_code", $updated_release, $row_results);
				
				if ($row_results->has_error ) {
					ovc_dev_list_results_msgs( $row_results );
					$row_outcome = 'error';
					br("Style: $style \t Updated Release: $updated_release \t " . ($stats['processed'] + 1));
				}
				else {
					$row_outcome = 'success';
				}
			}
			else {
				$row_outcome = 'not_exists';
				br("Row doesn't exist  - Style: $style");
			}
			
			// Output results CSV row
			$error_msg = $row_results->has_error ? ((array) $row_results->get_msgs('error'))[0] : '';

			$results_csv_row = implode(',', array($style, $updated_release, strtoupper( $row_outcome), $st_row->oms_release_code, $error_msg)) . "\r\n";
			file_put_contents( $csv_results_file, $results_csv_row, FILE_APPEND );

			$stats[ $row_outcome ]++;
			$stats['processed']++;
		  }
	}
	br($stats);
}

$allowed_sandbox_commands = array(
	'manual_import_oms_release_code'
)

?>

<div class="ox-row">
	<h2>Development Sandbox</h2>
	<h3>Used for debugging and development purposes</h3>
</div>
<div class="ox-row">
<pre>
<?php

	global $wpdb;

	$results = new OVC_Results;

	if( isset( $_GET['command'] ) && in_array( $_GET['command'], $allowed_sandbox_commands ) ) {
		call_user_func( $_GET['command'] );
	}



	// $st_row = OVCDB()->get_row_by_valid_id( $st_fs, "style", "001", $results );


	// // echo($results);
	// $st_row->update_field("st.oms_release_code", "Y", $results);

	// echo($results);
	
	// get_row_by_valid_id( FS( 'ovc_products' ), 'sku', $row->case_sku );
	// $raw_style = OVCDB()->get_row_by_id( "001", 'ID', 'wp_ovc_style_data' );

	// echo (implode(",", $raw_style));
	// $raw_style->update_field( "oms_release_code", "Y", $results );

	// echo($results);




	// $cpa_response = OVC_API_Walmart::request( 'get_cpa_report' );
	// br( $cpa_response );
	
	// $zip_file = OVCOP_CONTENT_DIR . '/walmart_cpa_report_test.zip';

	// $unzip_dir = OVCOP_CONTENT_DIR . '/walmart-cpa-report';

	// $file_path = $unzip_dir . '/';

	// file_put_contents( $zip_file, $cpa_response->format('raw') );

	// unzip_file( $zip_file, $unzip_dir );

	// $dir_handle = opendir( $unzip_dir );

	// while( $entry = readdir( $dir_handle ) ) {
	// 	if ($entry != "." && $entry != "..") {

	// 		$file_path .= $entry;
	// 		closedir( $dir_handle );
	// 		break;
	// 	}
	// }

	// br( $file_path );

	// $access_token = OVC_API_Walmart::generate_access_token( $results );
	// br( $access_token->access_token );

	// $shopify_parent = OVCDB()->get_row( FS( 'shopify_parents' ), 1, $results );

	// br( $shopify_parent->ovc_parent()->img_main );
	// br( $shopify_parent->ovc_parent()->img_marketplace_featured );

	// br( $shopify_parent->get_shopify_parent_img_ids() );

	// $shopify_product = OVCDB()->get_row( FS( 'shopify_products' ), 1, $results );

	// br( $shopify_product->filter_shopify_variant_metafields( $results ) );

	// br( OVCOP_manager::get_stuck_ovcop() );

	// OVCOP_manager::queue_auto_sync_ops();

	// $shopify_parent = OVCDB()->get_row( FS( 'shopify_parents' ), 1, $results );

	// /// Determine image post_ids that should be synced to Shopify
	// $image_ids = $shopify_parent->get_shopify_parent_img_ids();

	// // Get image sets of children that are set to sync to shopify
	// $image_set_ids = $shopify_parent->ovc_products( 'image_set', $results );

	// foreach( $image_set_ids as $image_set_id ) {

	// 	$image_set = OVCDB()->get_row( FS( 'image_sets' ), $image_set_id );

	// 	$image_set_image_ids = array();
	// 	// Override main image if override exists
	// 	if( !( $main_img_id = $image_set->get_image_ids( array( 'marketplace_featured' ) ) ) ) {
	// 		$main_img_id = $image_set->get_image_ids( array( 'main' ) );
	// 	}
	// 	$image_set_image_ids = array_merge( $main_img_id, $image_set->get_image_ids( 
	// 		array( 
	// 			'swatch', 
	// 			'gallery_ids', 
	// 			'sizeguide' 
	// 		), 
	// 		false 
	// 	) );

	// 	$image_ids = array_merge( 
	// 		$image_ids, 
	// 		$image_set_image_ids
	// 	);
	// }

	// $image_ids = array_values( array_unique( array_filter( $image_ids ) ) );

	// br( $image_ids );

	// $product = OVCDB()->get_row( FS( 'ovc_products' ), 14637, $results );

	// br( OVC_External::filter_wc_parent_product_name( $product ) );
	// br( $product->data( 'pa.brand' ) );

	// $shopify_parent = OVCDB()->get_row( FS( 'shopify_parents' ), 1, $results );

	// br( $shopify_parent->get_variants() );

	// br( OVC_External::filter_external_product_parent_pack_quantity( $shopify_parent, $shopify_parent->get_variants() ) );
	// br( $shopify_parent->get_external_data_set( FS( 'shopify_parent' ) ) );
	// br( $shopify_parent->ovc_products() );

	// if( ( $csv_stream = fopen( WP_CONTENT_DIR . '/vida-order-csv/Shopify_price_update.csv', 'r' ) ) !== false ) {

	// 	$csv_row = 1;
	// 	$last_row = $_GET['last_row'] ?? 2;

	// 	br( "Starting at row {$last_row}" );
	// 	while( false !== ( $csv_line = fgetcsv( $csv_stream ) ) ) {

	// 		$product_results = new OVC_Results;

	// 		if( 1 === $csv_row || empty( array_filter( $csv_line ) ) ) {
	// 			$csv_row++;
	// 			continue;
	// 		}

	// 		if( $last_row > $csv_row ) {
	// 			$csv_row++;
	// 			continue;
	// 		}

	// 		$asin 		= $csv_line[0];
	// 		$sku 		= $csv_line[1];
	// 		$old_price 	= $csv_line[4];
	// 		$new_price 	= $csv_line[5];

	// 		$ovc_product = OVCDB()->get_row_by_valid_id( 'ovc_products', 'sku', $sku, $product_results );

	// 		if( !$ovc_product->exists() ) {
	// 			br( "{$sku}: Can't find product with that sku." );
	// 			br( $product_results );
	// 		}

	// 		if( $ovc_product->data( 'pr.asin' ) != $asin ) {
	// 			br( "{$sku}: Product asin does not match provided asin" );
	// 			continue;
	// 		}

	// 		if( $ovc_product->update_data(
	// 				array(
	// 					'pr.price_retail'		=> $new_price
	// 				),
	// 				$product_results
	// 			) ) {
	// 			// br( "{$sku}: Updated successfully." );
	// 		} else {
	// 			br( "{$sku}: Update unsuccessful." );
	// 			br( $product_results );
	// 		}

	// 		if( 20 < ox_exec_time() ) {	
	// 			br( 'Execution Limit Hit: Ended on row ' . $csv_row );
	// 			br( '<a href="https://vida-us.com/wp-admin/admin.php?page=ovc-dev&last_row=' . $csv_row . '">Continue execution?</a>' );
	// 			break;
	// 		}

	// 		$csv_row++;
	// 	}
	// 	fclose( $csv_stream );
	// } else {
	// 	br( "Can't open Shopify Price Update CSV" );
	// }

	// br( $results );

	// OVC_API_Shopify::set_environment( 'live' );

	// $request_data['url_params']['shopify_variant_id'] = '7437349486625';

	// $product_response = OVC_API_Shopify::request( 'get_variant', $request_data, $results );

	// if( $product_response->succeeded() ) {

	// 	if( $variant_data = $product_response->data() ) {
	// 		br( $variant_data );
			// $shopify_variant = OVC_Row_Shopify_Products::get_from_api_response( $variant_data, $results );

			// if( !$shopify_variant->exists() ) {

			// } else {
			// 	$shopify_variant->handle_api_response( $product_response, $results );
			// }
	// 	}
	// }

	// br( $results );

	// br( OVCDB()->get_row( FS( 'walmart_product' ), 1, $results )->exists() );

	// br( OVCDB()->get_row( FS('ovc_products'), 0 )->exists() );

	// br( ovc_memory_usage_percentage() );

	// $pst = new DateTimeZone( 'America/Los_Angeles' );
	// $date = new DateTime( 'friday 9:30pm', $pst );
	// // br( date( 'm d Y h:i:s' ) );
	// // br( $date->format( 'Y-m-d h:i:s' ) );

	// // $weekend = $date->format( 'N' ) >= 5;
	// // br( $weekend );
	// // br( $date->format( 'H' ) );

	// // br( strtotime( 'friday' ) );
	// $friday_8pm = new DateTime( 'friday 8pm', $pst );
	// $sunday_8pm = new DateTime( 'sunday 8pm', $pst );

	// // br( $date >= $friday_8pm );
	// // br( $date <= $sunday_8pm );
	// // br( ( $date >= $friday_8pm ) && ( $date <= $sunday_8pm ) );

	// $last_analyze_images_auto_sync = get_option( 'ovcop__last_analyze_images_auto_sync' );
	// // br( $last_analyze_images_auto_sync );
	// br( // Check if date is between Friday 8pm and Sunday 8pm
	// 	!( ( $date >= $friday_8pm ) && ( $date <= $sunday_8pm ) )
	// 	// Check that last_analyze_image_auto_sync is set or that it's been more than 48 hours
	// 	|| ( $last_analyze_images_auto_sync && ( time() - $last_analyze_images_auto_sync ) < ( 2 * 24 * 60 * 60 ) )
	// );

	// $pr = OVCDB()->get_row( FS('pr'), 719 );

	// br( $pr->sku );
	// br( $pr->avail_qty );
	// br( 'assemblies count: ' . count( $pr->get_assemblies() ) );
	// br( $pr->get_potential_stock( ) );

	// $ovc_pr = OVCDB()->get_row( FS( 'ovc_products' ), 17162, $results );
	// br( $ovc_pr->is_unattached() );
	// br( $ovc_pr->is_top_level() );
	// br( $ovc_pr->get_max_depth() );

	// br();

	// $ovc_pr = OVCDB()->get_row( FS( 'ovc_products' ), 5934, $results );
	// br( $ovc_pr->is_unattached() );
	// br( $ovc_pr->is_top_level() );
	// br( $ovc_pr->get_max_depth() );

	// br();

	// $asmi_pr = OVCDB()->get_row( FS( 'ovc_assembly_items' ), 1, $results );
	// br( $asmi_pr->get_max_depth() );
	// br( $asmi_pr->get_assembly()->get_max_depth() );

	// br( $results );

	// $asm_pr = OVCDB()->get_row( FS( 'asm' ), 112, $results );
	// br( $asm_pr->init_assembly_items( $results ) );
	// br( $asm_pr->get_assembled_potential_qty() );
	// br( $results );

	// $ovc_pr = OVCDB()->get_row( FS( 'ovc_products' ), 6, $results );
	// $where = array(
	// 	'case_sku'	=> $ovc_pr->data( 'pr.sku' ),
	// 	array(
	// 		'var'	=> 'ID',
	// 		'op'	=> '!='3
	// 		'value'	=> 6
	// 	)
	// );
	// $case_products = OVCDB()->load_rows_where( FS( 'ovc_products' ), $where );
	// br( $case_products );

	// $asm_pr = OVCDB()->get_row( FS( 'asm' ), 110, $results );
	// br( $asm_pr->validate_total_quantity( $results ) );
	// br( $results );

	// $ovc_pr = OVCDB()->get_row( FS( 'ovc_products' ), 4227, $results );
	// br( $ovc_pr->validate_units_per_box( $results ) );

	// $wa_pr = OVCDB()->get_row( FS( 'walmart_product' ), 4695, $results );
	// br( OVC_External::filter_walmart_sku_update( $wa_pr ) );

	// $fs = FS( 'ovcdt' );

	// br( $fs->get_field_object( 'ovc_products' )->meta( 'fg._always' ) );
	
	// br( $fs->get_field_object( 'ovc_products' )->meta( 'fg.essential' ) );
	// br( $fs->get_field_object( 'ovc_products' )->meta( 'fg.details' ) );
	// br( $fs->get_field_object( 'ovc_products' )->meta( 'fg.color' ) );
	// br( $fs->get_field_object( 'ovc_products' )->meta( 'fg.shared_data' ) );
	// br( $fs->get_field_object( 'ovc_products' )->meta( 'fg.inventory' ) );
	// br( $fs->get_field_object( 'ovc_products' )->meta( 'fg.price_cost' ) );
	// br( $fs->get_field_object( 'ovc_products' )->meta( 'fg.alternate_ids' ) );
	// br( $fs->get_field_object( 'ovc_products' )->meta( 'fg.measurements' ) );
	// br( $fs->get_field_object( 'ovc_products' )->meta( 'fg.ovcop_ids' ) );
	// br( $fs->get_field_object( 'ovc_products' )->meta( 'fg.meta' ) );

	// $ovc_parent = OVCDB()->get_row( FS( 'parent_data' ), 1673, $results );

	// $shopify_parent_data = array(
	// 	'sypas.ovc_parent_id' 	=> $ovc_parent->data( 'pa.ID' ),
	// 	'pa.parent_sku'			=> $ovc_parent->data( 'pa.parent_sku' )
	// );
	// $shopify_parent = OVCDB()->new_row( FS( 'shopify_parents' ), $shopify_parent_data );
	// if( $shopify_parent->save( $results ) ) {
		
	// 	br( $shopify_parent->exists() );
	// 	br( $shopify_parent->needs_sync( 'create' ) );
	// 	br( $shopify_parent->ovc_parent()->ID );
	// }

	// br( $results );

	// $ovcpr = OVCDB()->get_row( FS( 'ovc_products' ), 1, $results );
	// br( $ovcpr->get_parent()->ID );
	// br( $ovcpr->get_parent()->data( 'img.img_main' ) );
	// br( $ovcpr->img_main );
	// $main_img = OVCDB()->get_row_by_valid_id( FS( 'ovc_images' ), 'post_id', $ovcpr->img_main );
	// br( $main_img->get_walmart_image_url() );

	// $unsynced_images = OVC_Sync_Manager::get_auto_sync_count( 'walmart_images_requiring_sync', true );
	// br( $unsynced_images );
	// $ovc_image_row = OVCDB()->get_row( FS( 'ovc_images' ), 3, $results );
	// br( $ovc_image_row->get_walmart_image_url() );
	// $image_path = $ovc_image_row->data( 'pic.image_override_path' ) ?: $ovc_image_row->data( 'pic.image_3_4_path' );
	// $local_path = get_ovc_image_path( $image_path );
	// br( $image_path );
	// br( $local_path );

	// $walmart_ftp = new OVC_SFTP( 'walmart', true );
	// br( 'Check Connction:' );
	// br( $walmart_ftp->check_connection() );
	// br( 'Connected:' );
	// br( $walmart_ftp->is_connected() );
	// br( 'Current Directory:' );
	// br( $walmart_ftp->pwd() );
	// br( 'Listing:' );
	// br( $walmart_ftp->nlist_dir() );
	// br( 'Raw Listing:' );
	// br( $walmart_ftp->rawlist_dir() );
	// // br( $walmart_ftp->list_dir() );
	// br( 'Upload File:' );
	// br( $walmart_ftp->put( $image_path, get_ovc_image_path( $image_path ) ) );

	// br( $walmart_ftp->results );
	// br( $results );

	// $ovcop = new OVCOP( 18343 );

	// $feed_type = 'walmart_bulk_update_items';
	// $ovc_ids = OVC_Sync_Manager::get_auto_sync_count( $feed_type, true );

	// $feed_file = $ovcop->new_file( $feed_type, 'xml', true );
	// $api_feed = OVC_Row_api_feeds::init_feed( $feed_type, $feed_file->ID, 18343, $results );

	// $api_feed->write_feed_to_file( $ovc_ids, $results );

	// $main_img = OVCDB()->get_row_by_valid_id( 'ovc_images', 'post_id', 15694 );
	// br( $main_img->get_walmart_image_url() );

	// $main_img = OVCDB()->get_row_by_valid_id( 'ovc_images', 'post_id', 15695 );
	// br( $main_img->get_walmart_image_url() );

	// $walmart_ftp = new OVC_SFTP( 'walmart', true );
	// br( 'Connected:' );
	// br( $walmart_ftp->is_connected() );
	// br( 'Current Directory:' );
	// br( $walmart_ftp->pwd() );
	// // br( $walmart_ftp->list_dir() );

	// br( $walmart_ftp->results );

	// $main_img = OVCDB()->get_row_by_valid_id( 'ovc_images', 'post_id', 99999999 );

	// $feed_type = 'walmart_bulk_update_items';
	// $ovc_ids = OVC_Sync_Manager::get_auto_sync_count( $feed_type, true );

	// // $ovc_ids_chunked = array_chunk( $ovc_ids, 500 );

	// $ovc_ids_chunked = array( array( 17472 ) );

	// foreach( $ovc_ids_chunked as $ovc_ids_chunk ) {

	// 	br( $ovc_ids_chunk );

	// 	// Loop over OVC IDs and make sure all of the images are in the OVC Image table before writing them to the file
	// 	$ovc_ids_chunk = array_filter( $ovc_ids_chunk, function( $ovc_id ) {

	// 		$parent_product = OVCDB()->get_row( FS( 'ovc_products' ), $ovc_id )->get_parent();
	// 		$main_img = OVCDB()->get_row_by_valid_id( FS( 'ovc_images' ), 'post_id', $parent_product->img_main );

	// 		if( !( $main_img->is_image_approved() ) ) {
	// 			return false;
	// 		}

	// 		foreach( explode( ',', $parent_product->img_gallery_ids ) as $img_id ) {

	// 			$gallery_img = OVCDB()->get_row_by_valid_id( FS( 'ovc_images' ), 'post_id', $img_id );

	// 			if( !( $gallery_img->is_image_approved() ) ) {
	// 				return false;
	// 			}
	// 		}

	// 		return true;
	// 	});

	// 	br( $ovc_ids_chunk );
	// }

	// $ovc_schema = array(
	// 	'zulily_2018_ia' => array(
	// 		'Vendor Product Description'				=> array(
	// 			'ovc_field'		=> 'st.description'
	// 		),
	// 	),
	// 	'zulily_2018_kids_apparel' => array(
	// 		'Vendor Product Description'				=> array(
	// 			'ovc_field'		=> 'st.description'
	// 		),
	// 	),
	// 	'zulily_2018_kids_personal_accessories' => array(
	// 		'Vendor Product Description'				=> array(
	// 			'ovc_field'		=> 'st.description'
	// 		),
	// 	),
	// 	'zulily_2018_kids_sleep_undergarments' => array(
	// 		'Vendor Product Description'				=> array(
	// 			'ovc_field'		=> 'st.description'
	// 		),
	// 	),
	// );
	// OVCSC::multi_update_field_meta( $ovc_schema );

	// $ovc_product_id = '1';
	// $ovc_pr = OVCDB()->get_row( FS( 'ovc_products' ), $ovc_product_id );
	// br( $ovc_pr );
	// $children = $ovc_pr->get_parent()->get_children( 'ovc_products', 'raw', $results, array( 'var' => '::[sync_wc]', 'value' => '1' ) );
	// br( $children );
	// br( 'External Product Name' );
	// br( OVC_External::filter_external_product_name( $ovc_pr ) );
	// br();
	// br( 'WC Parent Product Name' );
	// br( OVC_External::filter_wc_parent_product_name( $ovc_pr ) );
	// br();
	// br( 'Shopify Variant Product Name' );
	// br( OVC_External::filter_shopify_variant_product_name( $ovc_pr ) );
	// br();
	// br( 'Amazon Product Name' );
	// br( OVC_External::filter_amazon_product_name( $ovc_pr ) );
	// br();
	// br( 'Walmart Product Name' );
	// br( OVC_External::filter_walmart_product_name( $ovc_pr ) );
	// br();
	// br( 'Zulily Product Name' );
	// br( OVC_External::filter_zulily_product_name( $ovc_pr ) );
	// br();
	// br( 'Groupon Product Name' );
	// br( OVC_External::filter_groupon_product_name( $ovc_pr ) );
	// br();
	// br( 'Dropship Product Name' );
	// br( OVC_External::filter_dropship_product_name( $ovc_pr ) );

	// $wc_row = new OVC_Product_WooCommerce( FS( 'ovc_products' ), $ovc_product_id, $ovc_pr->data_set() );

	// br( wp_get_attachment_url( $ovc_pr->img_main ) );
	// br( OVC_External::filter_walmart_main_image( $ovc_pr ) );

	// br( $wc_row->format_unit_qty_details() );
	// br( OVC_External::filter_external_product_name( $wc_row ) );
	// br( 'External Product Name' );
	// br( OVC_External::filter_external_product_name( $ovc_pr ) );
	// br( 'Walmart Product Name' );
	// br( OVC_External::filter_walmart_product_name( $ovc_pr ) );
	// br( 'WC Parent Product Name' );
	// br( OVC_External::filter_wc_parent_product_name( $ovc_pr ) );
	// br( 'Amazon Product Name' );
	// br( OVC_External::filter_amazon_product_name( $ovc_pr ) );
	// br( 'Shopify Variant Product Name' );
	// br( OVC_External::filter_shopify_variant_product_name( $ovc_pr ) );
	// br( 'Zulily Product Name' );
	// br( OVC_External::filter_zulily_product_name( $ovc_pr ) );
	// br( 'Groupon Product Name' );
	// br( OVC_External::filter_groupon_product_name( $ovc_pr ) );

	// $prfs = FS('pr');

	// $row = OVCDB()->get_row( FS('pr'), 9556, $results );

	// $data =  $row->data_set( false, true );

	// br( boolstr( FS( 'pr', 'external' )));
	// br( boolstr( FS( 'pr', 'database' )));

	// $ovc_ids = OVC_Sync_Manager::get_auto_sync_count( 'walmart_bulk_update_inventory', true );

	// $api_feed = OVC_Row_api_feeds::init_feed( 'walmart_bulk_update_inventory' );

	// $api_feed->write_feed_to_file( $ovc_ids, $results );

	// br( $api_feed->get_feed_file()->path() );


	// migration_test();
	
	// br( OVC_Lists()->get_list_item_name( 'size', 'CS' ) );

	/*
	
	$praf = OVCSC::get_meta_value( 'field_set_meta', 'ovc_products', 'after_validate' );

	foreach( $praf as $logic_name ) {

		br();
		br( $logic_name );

		br( OVCSC::get_field( 'ovc_logic.'.$logic_name )->meta() );
	}

	/* * /
	// OVCSC::update_field_meta('walmart_product','walmart_id','unique','1');
	// OVCSC::update_field_meta('walmart_product','walmart_id','valid_id','1');
	$wa_csv_fs = OVCSC::get_field_set( 'walmart_items_csv' );

		$import_fields = $wa_csv_fs->get_single_meta_key_values( 'ovc_field' );

		br( $import_fields );
	
		
	$response = OVC_API_Walmart::request( 'get_items_report' );

	// br( $response->info() );

	// Array
	// (
	//     'PARTNER ID' => null,
	//     'SKU' => 'wa.sku',
	//     'PRODUCT NAME' => 'wa.productName',
	//     'PRODUCT CATEGORY' => null,
	//     'PRICE' => 'wa.price',
	//     'CURRENCY' => null,
	//     'PUBLISH STATUS' => 'wa.product_status',
	//     'STATUS CHANGE REASON' => 'wa.status_change_reason', //add
	//     'LIFECYCLE STATUS' => 'wa.lifecycle_status', //add
	//     'INVENTORY COUNT' => 'wa.inventory_count', //add
	//     'SHIP METHODS' => null,
	//     'WPID' => 'wa.walmart_id',
	//     'ITEM ID' => 'wa.item_id',//add
	//     'GTIN' => 'wa.gtin',//add
	//     'UPC' => 'wa.upc',
	//     'PRIMARY IMAGE URL' => null,
	//     'SHELF NAME' => 'wa.productType',
	//     'PRIMARY CAT PATH' => null,
	//     'OFFER START DATE' => null,
	//     'OFFER END DATE' => null,
	//     'ITEM CREATION DATE' => 'wa.wa_creation_date',//add
	//     'ITEM LAST UPDATED' => 'wa.wa_last_updated',//add
	//     'ITEM PAGE URL' => 'wa.item_page_url',//add
	//     'REVIEWS COUNT' => 'wa.reviews_count',//add
	//     'AVERAGE RATING' => 'wa.average_rating',//add
	//     'SEARCHABLE?' => 'wa.searchable',//add
	// );

	file_put_contents( OVCOP_CONTENT_DIR . '/walmart_items_report_test.zip' ,$response->format('raw') );

	unzip_file( OVCOP_CONTENT_DIR . '/walmart_items_report_test.zip', OVCOP_CONTENT_DIR . '/walmart-items-report' );

	$dir_files = scandir( OVCOP_CONTENT_DIR . '/walmart-items-report' );

	$dir_handle = opendir( OVCOP_CONTENT_DIR . '/walmart-items-report' );

	$file_path = OVCOP_CONTENT_DIR . '/walmart-items-report/';

	while( $entry = readdir( $dir_handle ) ) {
		if ($entry != "." && $entry != "..") {

            $file_path .= $entry;
            closedir( $dir_handle );
            break;
        }
	}
	br( $file_path );
	$file_handle = fopen( $file_path, 'r' );

	$csv_headers = fgetcsv( $file_handle );
	br( $csv_headers );

	br( $wa_csv_fs->get_fields( 'local_name' ) );

	

	$csv_row = fgetcsv( $file_handle );

	$csv_row = array_combine( $csv_headers, $csv_row );

	br( 'combined row' );
	br( $csv_row );

	$csv_import_data = array_intersect_key( $csv_row, $import_fields );

	br( 'intersect key' );
	br( $csv_import_data );

	

	br( 'count import_fields'.count( $import_fields));
	br( 'count csv_import_data'.count( $csv_import_data));
	$csv_import_data = array_combine(  $import_fields, $csv_import_data );

	$csv_import_data['wa.wa_creation_date'] = date( 'Y-m-d', strtotime( $csv_import_data['wa.wa_creation_date'] ) );
			$csv_import_data['wa.wa_last_updated'] = date( 'Y-m-d', strtotime( $csv_import_data['wa.wa_last_updated'] ) );

	br( 'import fields' );
	br( $csv_import_data );
	// br( fgetcsv( $file_handle ) );
	// br( fgetcsv( $file_handle ) );
	fclose( $file_handle );
	// $zip = new ZipArchve;
	// $zip->open( OVCOP_CONTENT_DIR . '/walmart_items_report_test.zip' );
	// $zip->extractTo( OVCOP_CONTENT_DIR . '/walmart_items_report.csv' );


	// br( $response->format('raw'));


	// br( var_export( $response->format() ) );

/*
	SKU] => wa.sku
    [PRODUCT NAME] => wa.productName
    [PRICE] => wa.price
    [PUBLISH STATUS] => wa.product_status
    [STATUS CHANGE REASON] => wa.status_change_reason
    [LIFECYCLE STATUS] => wa.lifecycle_status
    [INVENTORY COUNT] => wa.inventory_count
    [WPID] => wa.walmart_id
    [ITEM ID] => wa.item_id
    [GTIN] => wa.gtin
    [UPC] => wa.upc
    [SHELF NAME] => wa.productType
    [ITEM CREATION DATE] => wa.wa_creation_date
    [ITEM LAST UPDATED] => wa.wa_last_updated
    [ITEM PAGE URL] => wa.item_page_url
    [REVIEWS COUNT] => wa.reviews_count
    [AVERAGE RATING] => wa.average_rating
    [SEARCHABLE?] => wa.searchable


    SKU] => G6269_STR_6_UM
        [PRODUCT NAME] => Angelina Cotton Mid-Rise Briefs with Anchor Stripe Design (6-Pack)
        [PRICE] => 20.98
        [PUBLISH STATUS] => PUBLISHED
        [STATUS CHANGE REASON] => 
        [LIFECYCLE STATUS] => ACTIVE
        [INVENTORY COUNT] => 44
        [WPID] => 3T4X3XSSSOTV
        [ITEM ID] => 267697971
        [GTIN] => 00192732003500
        [UPC] => 192732003500
        [SHELF NAME] => Panties
        [ITEM CREATION DATE] => 03/06/2018
        [ITEM LAST UPDATED] => 07/17/2018
        [ITEM PAGE URL] => http://www.walmart.com/ip/Angelina-Cotton-Mid-Rise-Briefs-with-Anchor-Stripe-Design-6-Pack/267697971
        [REVIEWS COUNT] => 0
        [AVERAGE RATING] => 
        [SEARCHABLE?] => Y*/
	/* */
	br();
	br();
	br();
	ovc_dev_list_results_msgs( $results );
	br();
	br( "Exec time:" . ox_exec_time() );
?>
</pre>
</div>