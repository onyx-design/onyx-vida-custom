<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>


<div class="ovc-page-header-wrap">
	<div class="ovc-page-header__spacer">
	</div>
	<div class="ovc-page-header">
		<h2>OVC - VIDA Product Data Manager</h2>

		<div class="ovcdt-status-row">
			<div class="ovcdt-status-wrap">
				Status:
				<span class="ovcdt-status ovcdt-status-initializing ox-hidden">Initializing</span>
				<span class="ovcdt-status ovcdt-status-ready ox-hidden">Ready</span>
				<span class="ovcdt-status ovcdt-status-loading ox-hidden">Loading Data... <span class="ajax-loader-bg"></span></span>
				<span class="ovcdt-status ovcdt-status-saving ox-hidden">Saving Changes... <span class="ajax-loader-bg"></span></span>
				<span class="ovcdt-status ovcdt-status-error ox-hidden">ERROR!</span>
			</div>
			<div class="ovc-notice-box" id="ovc-header-notice-box">
			</div>
			<div id="user-activity">
			</div>
			<div class="ovcdt-table-select-wrap">
				<select class="ovcdt-table-select">
					<?php 

						$current_table = $_GET['ovcdt'] ?? 'ovc_products';
						$ovcdts = OVCSC::get_field_set( 'ovcdt' );

						foreach( $ovcdts->get_fields() as $ovcdt ) {

							if( $ovcdt->current_user_can_view() ) {


								$selected = $current_table == $ovcdt ? ' selected="selected"' : '';

								// determine primary field set nice name
								$field_set_name = $ovcdt->meta( 'main_field_set' ) ?: $ovcdt->local_name;
								$nice_name = OVCSC::get_field_set_meta( $field_set_name, 'nice_name' ) ?: $ovcdt->local_name;
								$nice_name = str_replace( '_', ' ', $nice_name );
								$nice_name = str_replace( 'ovcop', 'OVCOP', $nice_name );
								$nice_name = str_replace( 'ovc', 'OVC', $nice_name );
								$nice_name = str_replace( 'api', 'API', $nice_name );
								$nice_name = ucwords( $nice_name );

								echo '<option value="' . $ovcdt->local_name . '"' . $selected . '>' . $nice_name . '</option>';
							}
						}
					?>
				</select>
			</div>
		</div>
	</div>
</div>

<div id="oxmd-wrap-outer"><div id="oxmd-modal"></div></div>

<div id="ovcdt-table-main">
	<div class="ox-row ovcdt-table-wrap" data-ovcdt-type="ovc_products" data-request-defaults='{"show_filters":true,"ovcdt_version":"<?php echo OVC_Table::OVCDT_VERSION;?>"}'>
	
	
	</div>
</div>
