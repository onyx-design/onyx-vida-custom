<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>
<?php


	$price_fields = array(
		'qtyprice1' 	=> 'n/a',
		'price_wc'		=> 'ceil( ( [qtyprice1] / 0.8 ) * 4 ) / 4',
		'price_amazon' 	=> 'ceil( [qtyprice1] / 0.35 ) - 0.01',
		'price_retail' 	=> '[qtyprice1] * 4',
		'price_msrp'	=> '[qtyprice1] * 10'
	);

	$product = OVCDB()->get_row( FS( 'ovc_products' ), $this->args['requestData']['ovc_id'] );

	$user_cs_ids = get_ovc_user_custom_selection( get_current_user_id(), 'ovc_products' );
	$user_cs_sku_warnings = array();
	$user_cs_general_warnings = array();


	// Maybe process OVC Product Data for custom selection
	if( count( $user_cs_ids ) ) {
		global $wpdb;

		$qry_fields = array_merge( array( 'sku', 'sku_style', 'parent_sku' ), array_keys( $price_fields ) );

		$user_cs_data = $wpdb->get_results( "SELECT " . implode( ', ', $qry_fields ) . " FROM wp_ovc_products WHERE ID IN("  . implode( ',', $user_cs_ids ) . ")", ARRAY_A );
		

		// PROCESS WARNINGS

		// Check to see if target OVC ID is in custom selection
		if ( !in_array( $product->ID, $user_cs_ids ) ) {
			$user_cs_general_warnings['id_in_cs'] = "SKU {$product->sku} is not currently in your custom selection, but its prices will be changed regardless of your choice above.";
		}


		// Process Data Mismatch Warnings
		foreach( $user_cs_data as $sku_data ) {
			$user_cs_sku_warnings[ $sku_data['sku'] ] = false;

			
			foreach( $sku_data as $field => $value ) {
				if( 'sku' == $field ) {
					continue;
				}
				// Check for Custom Selection product data that does not match the target ID's product data
				else if ( $value != $product->$field ) {
					$user_cs_sku_warnings[ $sku_data['sku'] ] = true;

					// Price Mismatch
					if( in_array( $field, array_keys( $price_fields ) ) ) {
						$user_cs_general_warnings['price'] = "Not all prices match among the SKUs in your current custom selection.  The prices you see below are based on the SKU that you clicked on ( {$product->sku} ).";
					}

					if( 'sku_style' == $field ) {
						$user_cs_general_warnings['style'] = "There are multiple Style Numbers in your current custom selection.  Make sure your custom selection is correct or you could change prices on the wrong SKUs.";
					}

					if( 'parent_sku' == $field ) {
						$user_cs_general_warnings['parent_sku'] = "There are multiple Parent SKUs in your current custom selection.  Make sure your custom selection is correct or you could change prices on the wrong SKUs.";
					}
				}
				
			}

		}
	}
	

?>
<input type="hidden" name="ovc_id" class="oxmd-field" value="<?php echo $product->ID ;?>" />

<p>You are editing prices for SKU <?php _e( $product->sku ); ?></p>


<h4 class="oxmd-modal-subh">Save Prices for entire custom selection?</h4>

<div class="oxmd-field-wrap">
	<input type="radio" name="update_cs_prices" class="oxmd-field" value="" checked="checked" /><span class="t-bigger t-bold">Only save prices for SKU <?php echo $product->sku ;?></span>
</div>
<div class="oxmd-field-wrap">
	<input type="radio" name="update_cs_prices" class="oxmd-field" value="1" <?php echo count( $user_cs_ids ) ? '' : 'disabled="disabled"';?> /><span class="t-bigger t-bold">Save prices for all SKUs in current custom selection</span>
	<p class="t-bigger">There are currently <?php echo count( $user_cs_ids );?> SKUs in your custom selection.</p>
	<?php 
		if( count( $user_cs_ids ) ) {
			echo '<p>';

			foreach( $user_cs_sku_warnings as $sku => $has_warning ) {
				echo $sku . ' ';
				echo $has_warning ? '<i class="fa fa-exclamation-triangle" style="color:#DA8B15;"></i>' : '';
				echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ';
			}

			echo '</p>';
		}
	?>
</div>



<?php 
	// Output General Warnings
	foreach ( $user_cs_general_warnings as $warning ) {
		?>
			<p style="padding-left:150px;"><span class="t-bigger t-bold" style="display:block;position:absolute;left:0;"><i class="fa fa-exclamation-triangle" style="color:#DA8B15;"></i>&nbsp;&nbsp; WARNING!</span> <?php echo $warning;?></p>
		<?php
	}
?>



<h4 class="oxmd-modal-subh">Select Prices to Save</h4>

<table id="oxmd_ph-price-table" class="ovcdt-table" style="width:100%;">
	<thead>
		<tr>
			<th>
				Price Field
			</th>
			<th>
				Formula
			</th>		
			<th colspan="2">
				Current Value
			</th>
			<th colspan="2">
				Formula Value
			</th>
			<th colspan="2">
				Custom Value
			</th>
		</tr>
		<tr class="oxmd-ph-th-subh">
			<th>
			</th>
			<th>
			</th>		
			<th>
				Save?
			</th>
			<th>
			</th>
			<th>
				Save?
			</th>
			<th>
			</th>
			<th>
				Save?
			</th>
			<th>
			</th>
		</tr>
	</thead>

	<tbody>
		<?php 
			

			foreach ( $price_fields as $price_field => $formula ) {
				$formula_value_disabled = 'qtyprice1' == $price_field ? '' : 'disabled="disabled"';

				?>
					<tr>
						<td>
							<?php _e( OVCSC::get_meta_value( 'ovc_products', $price_field, 'nice_name' ) ); ?>
						</td>
						<td>
							<?php echo $formula;?>
						</td>

						<td>
							
								<input type="radio" class="oxmd-field ovcdt-td-bool" name="save-<?php echo $price_field ;?>" data-price-type="old" value="<?php echo $product->$price_field ;?>" checked="checked" />
							
						</td>
						<td>
							
								<input type="number" class="otdi" name="value-old-<?php echo $price_field ;?>" value="<?php echo $product->$price_field ;?>" data-og="<?php echo $product->$price_field ;?>" step="0.01" disabled="disabled" />
							
						</td>

						<td>
							
								<input type="radio" class="oxmd-field ovcdt-td-bool" name="save-<?php echo $price_field ;?>" data-price-type="formula" value="<?php echo $product->$price_field ;?>" />
							
						</td>
						<td>
							
								<input type="number" class="otdi" name="value-formula-<?php echo $price_field ;?>" value="<?php echo $product->$price_field ;?>" data-og="<?php echo $product->$price_field ;?>" step="0.01" <?php _e( $formula_value_disabled );?> />

								

						</td>

						<td>
							
								<input type="radio" class="oxmd-field ovcdt-td-bool" name="save-<?php echo $price_field ;?>" data-price-type="custom" value="<?php echo $product->$price_field ;?>" />

						</td>
						<td>

								<input type="number" class="otdi" name="value-custom-<?php echo $price_field ;?>" value="<?php echo $product->$price_field ;?>" data-og="<?php echo $product->$price_field ;?>" step="0.01" />

						</td>

					</tr>
				<?php
			} // end <tr> foreach loop
		?>
	</tbody>
</table>

<script>
	var qtyprice1_input = $('#oxmd_ph-price-table input[name="value-formula-qtyprice1"]');

	function oxmd_ph_calc_formulas() {
		var qtyprice1 = qtyprice1_input.val();

		qtyprice1_input.val( parseFloat( qtyprice1 ).toFixed(2) );
		$('#oxmd_ph-price-table input[name="value-formula-price_wc"]').val( parseFloat( Math.ceil( ( qtyprice1 / 0.8 ) * 4 ) / 4 ).toFixed(2) );
		$('#oxmd_ph-price-table input[name="value-formula-price_amazon"]').val( parseFloat( Math.ceil( ( qtyprice1 / 0.35 ) ) - 0.01 ).toFixed(2) );
		$('#oxmd_ph-price-table input[name="value-formula-price_retail"]').val( parseFloat( qtyprice1 * 4 ).toFixed(2) );
		$('#oxmd_ph-price-table input[name="value-formula-price_msrp"]').val( parseFloat( qtyprice1 * 10 ).toFixed(2) );

		oxmd_ph_update_input_vals();
	}

	function oxmd_ph_update_input_vals() {
		$('#oxmd_ph-price-table input[type="number"]').each(function() {
			var formatted_val = parseFloat( $(this).val() ).toFixed(2);
			$(this).val( formatted_val );
		});

		$('#oxmd_ph-price-table input:radio').each(function() {
			var meta = $(this).attr('name').split('-');
			var priceType = $(this).data('price-type');

			var sibling = $('#oxmd_ph-price-table input[name="value-' + priceType + '-' + meta[1] + '"]');
			$(this).val( parseFloat( sibling.val() ).toFixed(2) );
		});
	}
	oxmd_ph_calc_formulas();

	$('#oxmd_ph-price-table input[type="number"]').change(oxmd_ph_calc_formulas);
</script>


