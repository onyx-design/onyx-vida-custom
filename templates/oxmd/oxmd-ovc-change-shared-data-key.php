<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>
<?php

	$request = $this->args['requestData'];

	$product = new OVC_Product( $request['rowID'] );

	$field = OVCSC::get_field( $request['field'] );


	
	$old_key = $request['oldValue'];
	$new_key = $request['newValue'];


	
?>


<h4 class="oxmd-modal-subh"><?php _e( "Changing {$field->nice_name()} might affect shared product data." ); ?></h4>

<p>Overview of your changes:</p>

<div class="oxmd-field-wrap">
	<label for="field">Changing Field:</label>
	<input type="text" name="field" class="oxmd-field" readonly value='<?php echo "{$field->global_name}"; ?>' />
</div>
<div class="oxmd-field-wrap">
	<label for="sku">Target SKU:</label>
	<input type="text" name="sku" class="oxmd-field" readonly value="<?php echo $product->sku ; ?>" />
</div>
<div class="oxmd-field-wrap">
	<label for="ovc_id">Target OVC ID:</label>
	<input type="number" name="ovc_id" class="oxmd-field" readonly value="<?php echo $product->ID ; ?>" />
</div>
<div class="oxmd-field-wrap">
	<label for="old_value">Previous Value:</label>
	<input type="text" name="old_value" class="oxmd-field" readonly value="<?php echo $old_key ; ?>" />
</div>
<div class="oxmd-field-wrap">
	<label for="new_value">New Value:</label>
	<input type="text" name="new_value" class="oxmd-field" readonly value="<?php echo $new_key ; ?>" />
</div>



<?php
	// Determine if other products might be affected
	$old_val_count = OVCDB()->count_results( 'ovc_products', array( $field->local_name => $old_key ) );

	if( $old_val_count > 1 ) {
		?>

		<h4 class="oxmd-modal-subh">Change all values or just one?</h4>

		<p><?php echo "There are {$old_val_count} products with {$field->nice_name()} of {$old_key}"; ?></p>

		<div class="oxmd-field-wrap">
			<input type="radio" name="change_all" class="oxmd-field" value="1" /><span class="t-bigger t-bold">CHANGE ALL</span>
			<p class="t-smaller"><?php echo "All {$field->local_name} values of {$old_key} will be changes to {$new_key}"; ?></p>
		</div>
		<div class="oxmd-field-wrap">
			<input type="radio" name="change_all" class="oxmd-field" value="" checked /><span class="t-bigger t-bold">ONLY CHANGE ONE</span>
			<p class="t-smaller"><?php echo "Only this OVC Product ( {$product->sku}, {$product->ID} ) will have it's {$field->local_name} value of {$old_key} changed to {$new_key}"; ?></p>
		</div>

		<?php
	}
	else {
		?>

		<input type="hidden" name="change_all" class="oxmd-field" value="" />
		
		<?php
	}




	// Does the new value already exist in the corresponding foreign table?
	$foreign_field = OVCSC::get_field( $field->meta( 'foreign_key' ) );

	$new_val_meta = 
		OVCDB()->count_results( 
			$foreign_field->field_set,
			array( $foreign_field->local_name => $new_key )
		);


	if( $new_val_meta ) {
		// If there is existing data for the new key value, determine if there are any differences with the old key value's data, aka do we need to ask user to merge?
		global $wpdb;

		// Get a product that has the $new_key value
		$new_key_match_id = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM wp_ovc_products WHERE {$field->local_name} = %s LIMIT 0,1", $new_key ) );

		$new_key_product = new OVC_Product( $new_key_match_id );


		$new_key_data = $new_key_product->data_set( $foreign_field->field_set );
		$old_key_data = $product->data_set( $foreign_field->field_set );

		// Compare old_key data with new_key data to determine which keys need to be merged
		$merge_data = array();
		foreach ( $old_key_data as $field_name => $old_value ) {

			// Check for mismatched data. Skip match field and certain other fields // dev:generalize!
			if( !in_array( $field_name, array( $foreign_field->local_name, 'wc_parent_id' )	) 
				&& $old_value != $new_key_data[ $field_name ]
			) {
				$merge_data[ $field_name ] = array(
					$old_key => $old_value,
					$new_key => $new_key_data[ $field_name ]
				);
			}
		}

		if( $merge_data ) {
			?>

			<h4 class="oxmd-modal-subh">Merge existing Shared Product Data</h4>

			<p><?php echo "There is existing shared data for {$field->nice_name()}: {$new_key}<br />Select which shared values you would like to use for {$new_key}"; ?></p>

			<table class="oxmd-sData-merge-table">

				<thead>
					<tr>
						<th>
							Shared Data Field
						</th>
						<th>
							<?php echo "{$old_key} (Old Value)"; ?>
						</th>
						<th>
							<?php echo "{$new_key} (New Value)"; ?>
						</th>
					</tr>
				</thead>
				<tbody>

				<?php
				foreach( $merge_data as $field_name => $data_vals ) {

					$merge_field = OVCSC::get_field( "{$foreign_field->fs_alias}.$field_name" );
					?>

					<tr>
						<td>
							<?php echo $merge_field->nice_name(); ?>
						</td>
						<td>
							<?php 
							echo "<input type='radio' name='merge_field_{$field_name}' value='{$old_key}' class='oxmd-field' />";
							echo $data_vals[ $old_key ];
							?>
						</td>
						<td>
							<?php 
							echo "<input type='radio' name='merge_field_{$field_name}' value='{$new_key}' class='oxmd-field' checked />";
							echo $data_vals[ $new_key ];
							?>
						</td>
					</tr>

					<?php
				}
				?>

				</tbody>
			</table>

			<?php
		}

		

		
		
	}

?>


