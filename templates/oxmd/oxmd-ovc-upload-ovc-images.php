<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

$ovc_image = OVCDB()->get_row( FS( 'ovc_images' ), $this->args['requestData']['ovc_id'] );
?>
<div>
		
	<div>	
		<input id="override_image" name="override_image" type="file" />
	</div>

	<div>
	<?php
		if( $image_override_path = $ovc_image->data( 'pic.image_override_path' ) ) {
			echo '<img src="' . ovc_content_url_from_path( $image_override_path ) . '">';
		} else {
			echo 'Upload an image to see the preview here.';
		}
	?>
	</div>
</div>

<script>
	jQuery( '#override_image' ).change( function() {

		var file = $( this )[0].files[0];

		oxmd.uploadFile( file );
	});
</script>