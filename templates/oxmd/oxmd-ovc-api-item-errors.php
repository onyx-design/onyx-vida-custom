<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	$request = $this->args['requestData'];

	$walmart_product_id = $request['walmart_product_id'];

	global $wpdb;

	$walmart_product = $wpdb->get_row("SELECT * FROM wp_ovc_walmart_products WHERE ID = {$walmart_product_id}", ARRAY_A );

?>

<p>Feed errors for Walmart Product ID <?php echo $walmart_product['ID']; ?>, SKU: <?php echo $walmart_product->sku; ?></p>

<pre style="white-space:pre-wrap;">
	<?php 

		$item_errors = maybe_unserialize( $walmart_product['errors'] );

		if( !$item_errors ) {
			echo 'No Errors.';
		}
		else {
			print_r( $item_errors );
		}

	?>
</pre>