<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
	
	global $raw_child_data, $image_sets, $colors;

	$product = OVCDB()->get_row( FS( 'ovc_products' ), $this->args['requestData']['ovc_id'] );

	$parent = OVCDB()->get_row_by_valid_id( FS( 'parent_data' ), 'parent_sku', $product->parent_sku );

	$raw_child_data = $parent->get_children( $product->field_set, 'raw' );

	// Determine Colors with unnassigned SKUs and SKUs without image sets
	$colors = array();
	$image_sets = array();

	$skus_no_image_sets = array();

	foreach( $raw_child_data as $child_data ) {

		// Add to list of colors if not there
		if( !isset( $colors[ $child_data['color'] ] ) ) {

			$colors[ $child_data['color'] ] = array(
				'color'				=> $child_data['color'],
				'color_long_name' 	=> $child_data['color_long_name'],
				'image_set_ids'		=> array(),
				'assigned'			=> 0,
				'total'				=> 0
			);
		}

		// Record assigned stat on color and add image set data to list of image sets if not there

		if( $child_data['image_set'] ) {
			$colors[ $child_data['color'] ]['assigned']++;

			if( !in_array( $child_data['image_set'], $colors[ $child_data['color'] ]['image_set_ids'] ) ) {
				$colors[ $child_data['color'] ]['image_set_ids'][] = $child_data['image_set'];
			}

			if( !isset( $image_sets[ $child_data['image_set' ] ] ) ) {

				$image_sets[ $child_data['image_set' ] ] = OVCDB()->get_row_by_id( $child_data['image_set'], 'ID', 'wp_ovc_image_sets' );
			}
		}
		else {
			$skus_no_image_sets[] = ovc_draggable_sku_html( $child_data, false );
		}

		$colors[ $child_data['color'] ]['total']++;
	}


	global $oxmd_extra_response_data;

	$oxmd_extra_response_data = array(
		'targetID' 		=> intval( $product->ID ),
		'parent_sku'	=> $product->parent_sku,
		'colors'		=> array_keys( $colors ),
		'image_sets'	=> 
			array( 'parent' 			=> array(
					'img_main'			=> intval( $parent->img_main ),
					'img_gallery_ids'	=> array_map( 'intval', explode( ",", $parent->img_gallery_ids ) )
				)
			)
			+ $image_sets
	);



	function ovc_img_select_html( $img_ids, $auto_output = true ) {

		$img_select_html = '';
		


		if( is_numeric( $img_ids ) && intval( $img_ids ) == $img_ids ) {

			$img_id = intval( $img_ids );

			$img_select_obj = new OVC_HTML_Tag_Builder( 
				array( 
					'type' 	=> 'div',
					'attrs' => array(
						'class' => array( 'ovc-img-wrap' ),
						'onclick' => "ovcim.imgSelect(this);",
						'data-image-id' => $img_id
					),
					'contents' => '<i class="remove-image fa fa-times" title="Remove Image" onclick="ovcim.removeImg(event,this);"></i>'
				)
			);

			if( $img_id ) {

				$img_select_obj->css( 'background-image', "url('" . wp_get_attachment_image_url( $img_id, 'thumbnail' ) . "')" );

			}



			$img_select_html .= $img_select_obj->get_html();
		}
		else if( is_array( $img_ids ) ) {

			foreach( $img_ids as $img_id ) {

				$img_select_html .= ovc_img_select_html( $img_id, false );
			}
		}

		if( $auto_output ) {

			echo $img_select_html;
		}

		return $img_select_html;
	}

	function ovc_gallery_select_button_html( $auto_output = false ) {
		$gal_button = new OVC_HTML_Tag_Builder( 'div', array( 'class' => array( 'ovcim-gallery-select' ), 'onclick' => "ovcim.imgSelect(this);" ) );

		if( $auto_output ) {
			echo $gal_button->get_html();
		}
		return $gal_button->get_html();
	}

	function ovc_draggable_sku_html( $data, $auto_output = true ) {

		$draggable_sku_obj = new OVC_HTML_Tag_Builder( 'div', array( 'class' => array( 'ovc-draggable-sku' ), 'data-sku' => $data['sku'], 'data-ovc-id' => $data['ID'], 'data-color' => $data['color'] ) );
		$draggable_sku_obj->contents( $data['sku'] );

		if( $auto_output ) {
			echo $draggable_sku_obj->get_html();
		}

		return $draggable_sku_obj->get_html();
	}

	function ovc_draggable_color_html( $data, $auto_output = true ) {

		$ovc_draggable_color_obj = new OVC_HTML_Tag_Builder( 'div', array( 'class' => array( 'ovc-draggable-color' ), 'data-color' => $data['color'] ) );

		

		$short_color_obj = new OVC_HTML_Tag_Builder( array( 'type' => 'div', 'attrs' => array( 'class' => array( 'short-color') ), 'contents' => $data['color'] ) );
		$long_color_obj = new OVC_HTML_Tag_Builder( array( 'type' => 'div', 'attrs' => array( 'class' => array( 'long-color') ), 'contents' => $data['color_long_name'] ) );
		$color_sku_stats_obj = new OVC_HTML_Tag_Builder( array( 'type' => 'div', 'attrs' => array( 'class' => array( 'color-sku-stats' ) ) ) );

		// Add special class if this is the legend so it's not draggable
		if( is_string( $data['total'] ) ) {
			$ovc_draggable_color_obj->add_class( 'color-legend' );
			$color_sku_stats_obj->contents( '# SKUs (# Img Sets)' );
		}
		else {
			$color_sku_stats_obj->contents( 
				'<span class="sku-count"><span class="skus-assigned">' . $data['assigned'] . "</span> / " .
				$data['total'] . "</span> " .
				'<span class="img-set-count">' . "(" . count( $data['image_set_ids'] ) . ")</span>"
			);

			// Add data attribute with total sku count
			$ovc_draggable_color_obj->attr( 'data-total-skus', $data['total'] );
		}

		// Determine extra classes to add based on # skus assigned and # img sets the color is spread across
		if( 0 == $data['assigned'] ) {
			$ovc_draggable_color_obj->add_class( 'skus-assigned-none' );
		}
		else if( $data['assigned'] == $data['total'] ) {
			$ovc_draggable_color_obj->add_class( 'skus-assigned-all' );
		}
		else {
			$ovc_draggable_color_obj->add_class( 'skus-assigned-some' );
		}

		if( 1 == count( $data['image_set_ids'] ) ) {
			$ovc_draggable_color_obj->add_class( 'img-set-count-1' );
		}
		else if( count( $data['image_set_ids'] ) > 1 ) {
			$ovc_draggable_color_obj->add_class( 'img-set-count-multiple' );
		}

		

		$ovc_draggable_color_obj->contents( $short_color_obj->get_html() . $long_color_obj->get_html() . $color_sku_stats_obj->get_html() );


		$draggable_color_html = $ovc_draggable_color_obj->get_html();
		if( $auto_output ) {
			echo $draggable_color_html;
		}
		
		return $draggable_color_html;

	}

	function ovc_image_set_data_cells_loop( $output_type ) {

		global $image_sets, $raw_child_data, $colors;
		

		foreach( $image_sets as $image_set_id => $image_set ) {

			$cell_elem = new OVC_HTML_Tag_Builder( 'td', array( 'class' => 'img-set-col', 'data-img-set-id' => $image_set_id ) );

			switch( $output_type ) {
				case 'skus':

					$cell_elem->add_class( 'img-set-skus-droppable' );
					
					foreach( $raw_child_data as $child_data ) {
						
						if( $child_data['image_set'] == $image_set_id ) {	

							$cell_elem->contents( ovc_draggable_sku_html( $child_data, false ), true );
						}
					}
				break;
				case 'colors':

					$cell_elem->add_class( 'img-set-colors-droppable' );

					foreach( $colors as $color ) {

						if( in_array( $image_set_id, $color['image_set_ids'] ) ) {
							$cell_elem->contents( ovc_draggable_color_html( $color, false ), true );
						}
					}
				break;
				case 'image_set_id':

					$cell_elem->contents( $image_set_id );
				break;
				case 'img_main':
				case 'img_oms1':
				case 'img_oms2':
				case 'img_swatch':
				case 'img_case':
				case 'img_amazon':
				case 'img_sizeguide':

					$cell_elem->contents( ovc_img_select_html( $image_set[ $output_type ], false ) );
				break;
				case 'img_gallery_ids':

					$img_gallery_ids = explode( ',', $image_set['img_gallery_ids'] );
					$cell_elem->contents( ovc_img_select_html( $img_gallery_ids, false ) );
					$cell_elem->contents( ovc_gallery_select_button_html(), true );
				break;
				default:
				break;
			}

			echo $cell_elem->get_html();
		}
	}
?>


<input type="hidden" name="ovc_id" class="oxmd-field" value="<?php echo $product->ID ;?>" />

<div class="oxmd-field-wrap">
	<label class="oxmd-label--wide">Parent SKU</label>
	<input type="text" disabled="disabled" value="<?php echo $product->parent_sku ;?>" />
</div>
<div class="oxmd-field-wrap">
	<label class="oxmd-label--wide">Product Title [Parent]</label>
	<input type="text" disabled="disabled" value="<?php echo $product->data( 'pa.product_title' ) ;?>" />
</div>




<div id="ovc-img-set-tables-outer-wrap" class="ox-row">
<?php
	include( 'oxmd-ovc-image-manager-img-set-tables.php' );
?>	
</div>

<script type="text/javascript">
	var ovcim = new ovcImageManager();
</script>

