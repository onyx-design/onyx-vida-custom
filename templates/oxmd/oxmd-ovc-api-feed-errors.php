<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	$request = $this->args['requestData'];

	$api_feed = OVCDB()->get_row( FS('api_feeds'), $request['api_feed_id'] );
?>

<p>Feed errors for API Feed ID <?php echo $api_feed->ID; ?>, Feed type: <?php echo $api_feed->feed_type; ?></p>

<pre style="white-space:pre-wrap;">
	<?php print_r( $api_feed->get_ingestion_errors() );?>
</pre>