<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>


<div class="ox-row ovc-img-not-set-table-outer-wrap" data-target-id="<?php echo intval( $product->ID ) ?>">

	<table class="ovc-colors-skus-table">
		<tbody>
			<tr class="img-set-colors-incomplete-row">
				<td>
					Colors in Parent SKU <?php echo $parent->parent_sku . " ( " . count( $colors ) . " ) "; ?>
				</td>
				<td class="img-set-colors-legend">
					Legend:
				</td>
			</tr>
			<tr class="img-set-all-colors-row">
				<td class="img-set-all-colors-td">
					<?php 
						// Display color blocks (only unfully assigned ones)
						foreach( $colors as $color_data ) {

							//if( $color_data['assigned'] != $color_data['total'] ) {

								ovc_draggable_color_html( $color_data );	
							//}
						}
					?>
				</td>
				<td class="img-set-colors-legend">
					<?php
						$legend_data = array(
							'color'				=> 'COLOR',
							'color_long_name' 	=> 'Color Long Name',
							'image_set_ids'		=> array(),
							'assigned'			=> '# SKUs: (Assigned)',
							'total'				=> '(Total)'
						);
						ovc_draggable_color_html( $legend_data );
					?>
				</td>
			</tr>
			<tr class="img-set-unset-skus-row">
				<td colspan="2">
					SKUs without image sets ( <span class="skus-wo-imgs-count"><?php echo count( $skus_no_image_sets ); ?></span> )
				</td>
			</tr>
			<tr>
				<td id="unset-skus" class="unset-skus-td img-set-skus-droppable" colspan="2">
					<div class="unset-skus-wrap">
						<?php echo implode( '', $skus_no_image_sets ); ?>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
</div>


<div class="ox-row ovc-img-table-outer-wrap">

	<table class="ovc-img-set-table">
		<tbody>
			<tr class="img-set-id-row">
				<td class="img-set-col">Image Set ID</td>

				<td class="img-set-col">
					Parent Product
				</td>

				<?php ovc_image_set_data_cells_loop( 'image_set_id' ); ?>

				<td class="img-set-col img-set-col-new" data-img-set-id="new1">
					New Image Set 1
				</td>
			</tr>
			<tr class="img-set-skus-row" data-img-set-field="skus">
				<td>SKUs in Image Set</td>

				<td class="img-set-col">
					<?php
						echo $parent->parent_sku;
					?>
				</td>

				<?php ovc_image_set_data_cells_loop( 'skus' ); ?>

				<td class="img-set-col img-set-col-new img-set-skus-droppable fresh-img-set" data-img-set-id="new1">
				</td>
			</tr>
			<tr class="img-set-colors-row">
				<td>Colors in Image Set</td>

				<td class="img-set-col not-applicable">
				</td>

				<?php ovc_image_set_data_cells_loop( 'colors' ); ?>

				<td class="img-set-col img-set-col-new img-set-colors-droppable" data-img-set-id="new1">
				</td>
			</tr>
			<tr class="img-set-img_main-row" data-img-set-field="img_main">
				<td>Featured Image</td>

				<td class="img-set-col" data-img-set-id="parent">
					<?php ovc_img_select_html( $parent->img_main ); ?>
				</td>

				<?php ovc_image_set_data_cells_loop( 'img_main' ); ?>

				<td class="img-set-col img-set-col-new" data-img-set-id="new1">

				</td>
			</tr>
<!-- 			<tr class="img-set-img_oms1-row" data-img-set-field="img_oms1">
				<td>OMS Image 1</td>

				<td class="img-set-col not-applicable">
				</td>

				<?php // ovc_image_set_data_cells_loop( 'img_oms1' ); ?>

				<td class="img-set-col img-set-col-new" data-img-set-id="new1">
				</td>
			</tr>
			<tr class="img-set-img_oms2-row" data-img-set-field="img_oms2">
				<td>OMS Image 2</td>

				<td class="img-set-col not-applicable">
				</td>

				<?php // ovc_image_set_data_cells_loop( 'img_oms2' ); ?>

				<td class="img-set-col img-set-col-new" data-img-set-id="new1">
				</td>
			</tr> -->
			<tr class="img-set-img_swatch-row" data-img-set-field="img_swatch">
				<td>Swatch Image</td>

				<td class="img-set-col not-applicable">
				</td>

				<?php ovc_image_set_data_cells_loop( 'img_swatch' ); ?>

				<td class="img-set-col img-set-col-new" data-img-set-id="new1">
				</td>
			</tr>
			<tr class="img-set-img_amazon-row" data-img-set-field="img_amazon">
				<td>Amazon Image</td>

				<td class="img-set-col not-applicable">
				</td>

				<?php ovc_image_set_data_cells_loop( 'img_amazon' ); ?>

				<td class="img-set-col img-set-col-new" data-img-set-id="new1">
				</td>
			</tr>
			<tr class="img-set-img_case-row" data-img-set-field="img_case">
				<td>Case Image</td>

				<td class="img-set-col not-applicable">
				</td>

				<?php ovc_image_set_data_cells_loop( 'img_case' ); ?>

				<td class="img-set-col img-set-col-new" data-img-set-id="new1">
				</td>
			</tr>
			<tr class="img-set-img_sizeguide-row" data-img-set-field="img_sizeguide">
				<td>Size Guide Image</td>

				<td class="img-set-col not-applicable">
				</td>

				<?php ovc_image_set_data_cells_loop( 'img_sizeguide' ); ?>

				<td class="img-set-col img-set-col-new" data-img-set-id="new1">
				</td>
			</tr>
			<tr class="img-set-img_gallery_ids-row" data-img-set-field="img_gallery_ids">
				<td>Additional Images</td>

				<td class="img-set-col" data-img-set-id="parent">
					<?php
						ovc_img_select_html( $parent->get_img_gallery_ids() ); 
						ovc_gallery_select_button_html( true );
					?>
				</td>

				<?php ovc_image_set_data_cells_loop( 'img_gallery_ids' ); ?>

				<td class="img-set-col img-set-col-new" data-img-set-id="new1">
				</td>
			</tr>
			<tr class="img-set-img_marketplace_featured-row" data-img-set-field="img_marketplace_featured">
				<td>Marketplace Featured Image Override</td>

				<td class="img-set-col" data-img-set-id="parent">
					<?php ovc_img_select_html( $parent->img_marketplace_featured ); ?>
				</td>

				<?php ovc_image_set_data_cells_loop( 'img_marketplace_featured' ); ?>

				<td class="img-set-col img-set-col-new" data-img-set-id="new1">
				</td>
			</tr>
		</tbody>
	</table>
</div>
