<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>
<?php
global $current_user;	
?>

<div id="fg-filters" class="oxmn-container oxmn-has-submenu">
	<div class="oxmn-li oxmn-li-toplevel">
		Field Groups
	</div>
	<div class="oxmn-submenu">

		<?php

		$menu_html = '';
		$all_checked = true;


		foreach( $this->dt_meta->field_groups as $fg => $fg_nice_name ) {
			// Maybe Skip field group display option if user does not have sufficient view_access
			if( $fgmeta = $this->dt_meta->meta( '_fg.'.$fg )
				 && isset( $fgmeta['view_access'] )
				 && !array_intersect( array_merge( array( 'administrator' ), $fgmeta['view_access'] ), $current_user->roles )
			) {
				continue;
			}


			$disabled = ( $this->dt_meta->required_field_groups && in_array( $fg, $this->dt_meta->required_field_groups ) ? ' disabled="disabled"' : '' );
			
			$checked = ( in_array( $fg, $this->field_groups ) ? ' checked="checked"' : '' );
			$all_checked = ( !$all_checked || !$checked ) ? false : true;

			$menu_html .= '<div class="oxmn-li oxmn-li-checkbox">';
			$menu_html .= 	'<input id="fg-'.$fg.'" class="fg-select" data-oxmn-list="field-group-select" data-field-group="'.$fg.'" type="checkbox" name="field_groups[]" value="'.$fg_nice_name.'"'.$checked.$disabled.' />';
			$menu_html .= 	'<label for="fg-'.$fg.'">'.$fg_nice_name.'</label>';
			$menu_html .= '</div>';
		} 


		// Now add the select all menu item, this has to be done last because we need to know if they are all selected for the checked attribute of the select all

		$all_checked = $all_checked ? ' checked="checked"' : '';

		$check_all = '<div class="oxmn-li oxmn-li-checkbox oxmn-section-break">';
		$check_all .= 	'<input id="fg-select-all" class="fg-select fg-select-all" data-oxmn-list="field-group-select" type="checkbox" name="fg_check_all" value="check_all"'.$all_checked.' />';
		$check_all .= 	'<label for="fg-select-all">Toggle All</label>';
		$check_all .= '</div>';

		echo $check_all . $menu_html;

		?>

	</div>
</div>