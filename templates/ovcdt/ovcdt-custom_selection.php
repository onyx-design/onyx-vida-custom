<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

$enable_checked = $this->cs_mode_enabled ? 'checked="checked"' : '';

$filters_active = $this->filters ? '' : 'disabled="disabled"';
?>



<div id="ovcdt-custom-selection-row" class="oxmn-container oxmn-has-submenu">
	<div class="oxmn-li oxmn-li-toplevel">
		Custom Selection ( <?php echo count( $this->user_cs_ids ); ?> )
	</div>

	<div class="oxmn-submenu">
		<div class="oxmn-li oxmn-li-checkbox">
			<input type="checkbox" id="ovcdt_cs_enable" <?php echo $enable_checked; ?> />
			<label for="ovcdt_cs_enable">Enable Custom Selection Mode</label>
		</div>
		<div class="oxmn-li oxmn-section-break">
			<input type="submit" id="ovcdt_cs_view" onclick="ovcdtMain.viewUserCS();" />
			<label for="ovcdt_cs_view">View Custom Selection</label>
		</div>
		<div class="oxmn-li">
			<input type="submit" id="ovcdt_cs_add_all" onclick="ovcdtMain.userCSRequest('add_all');" <?php echo $filters_active; ?> />
			<label for="ovcdt_cs_add_all">Add filtered products to CS</label>
		</div>
		<div class="oxmn-li">
			<input type="submit" id="ovcdt_cs_remove_all" onclick="ovcdtMain.userCSRequest('remove_all');" <?php echo $filters_active; ?> />
			<label for="ovcdt_cs_remove_all">Remove filtered products from CS</label>
		</div>
		<div class="oxmn-li oxmn-section-break">
			<input type="submit" id="ovcdt_cs_set_all" onclick="ovcdtMain.userCSRequest('set_all');" <?php echo $filters_active; ?> />
			<label for="ovcdt_cs_set_all">Set CS to current filters</label>
		</div>
		<div class="oxmn-li">
			<input type="submit" id="ovcdt_cs_save" disabled="disabled" onclick="ovcdtMain.userCSRequest('save');" />
			<label for="ovcdt_cs_save">Save Changes</label>
		</div>
		<div class="oxmn-li">
			<input type="submit" id="ovcdt_cs_revert" disabled="disabled" onclick="ovcdtMain.revertUserCS();" />
			<label for="ovcdt_cs_revert">Revert Changes</label>
		</div>
		<div class="oxmn-li">
			<input type="submit" id="ovcdt_cs_clear" onclick="ovcdtMain.userCSRequest('clear');" />
			<label for="ovcdt_cs_clear">Clear All</label>
		</div>
	</div>
</div>
