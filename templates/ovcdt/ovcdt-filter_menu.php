<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>
<div id="ovcdt-clear-filters-wrap" class="oxmn-container">
	<div class="oxmn-li">
		<input type="submit" id="ovcdt_clear_filters" onclick="ovcdtMain.table.clearFilters();" />
		<label for="ovcdt_clear_filters">Clear Filters</label>
	</div>
</div>
