<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>
<div class="oxmn-container ovcdt-show-in-use-imgs-wrap">
	<div class="oxmn-li oxmn-li-toplevel oxmn-li-checkbox">
		<input type="checkbox" id="ovcdt-show-in-use-imgs" name="ovcdt-show-in-use-imgs" class="ovcdt-show-in-use-imgs" checked />
		<label for="ovcdt-show-in-use-imgs">Show In Use</label>
	</div>
</div>
