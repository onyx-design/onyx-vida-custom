<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>
<div class="oxmn-container ovcdt-wait-to-save-wrap">
	<div class="oxmn-li oxmn-li-toplevel oxmn-li-checkbox">
		<input type="checkbox" id="ovcdt-wait-to-save" name="ovcdt-wait-to-save" class="ovcdt-wait-to-save" checked />
		<label for="ovcdt-wait-to-save">Wait to Save Changes?</label>
	</div>
</div>
