<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

// $ovc_row should always be available when this is included from OVC_Table

	// DELETE ROW
	if( $ovc_row->can_delete() ) {
		?><div class="oxmn-li oxmn-li-ovcdt-actions oxmn--delete-row oxmn-section-break" data-row-action="delete_row">
			<i class="fa fa-times"></i>
			<label>Delete Row</label>
		</div><?php
	}


	// API REFRESH
	global $current_user;
	if( array_intersect( array( 'administrator', 'vida_sr_data_tech', 'vida_data_tech' ), $current_user->roles ) ) {
		?><div class="oxmn-li oxmn-li-ovcdt-actions oxmn--api-refresh" data-row-action="api_refresh">
			<i class="fa fa-refresh api-refresh"></i>
			<label title="Refresh local API feed data from Walmart via the Walmart API">Refresh API Feed</label>
		</div><?php
	}

	// FEED FILE (View and download)
	//$files_fs = OVCSC::init_field_set('ovcop_files');
	$feed_file = new OVC_File( $ovc_row->file_id );

	if( $feed_file->exists() ) {
		?><div class="oxmn-li oxmn-li-ovcdt-actions oxmn--view-api-feed-file">
			<i class="fa fa-eye can-view"></i>
			<label><a class="ovcdt-action-link" href="<?php echo $feed_file->url(); ?>" target="_blank">View feed file</a></label>
		</div>
		<div class="oxmn-li oxmn-li-ovcdt-actions oxmn--download-api-feed-file">
			<i class="fa fa-download can-download"></i>
			<label><a class="ovcdt-action-link" href="<?php echo $feed_file->url(); ?>" download>Download feed file</a></label>
		</div><?php
	}
	else {
		?><div class="oxmn-li oxmn-li-ovcdt-actions oxmn--view-api-feed-file oxmn-disabled">
			<i class="fa fa-eye"></i>
			<label>Feed file not found</label>
		</div><?php
	}


