<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

// $ovc_row should always be available when this is included from OVC_Table

	// DUPLICATE ROW
	if( $ovc_row->can_duplicate() ) {
		?><div class="oxmn-li oxmn-li-ovcdt-actions oxmn--duplicate-row" data-row-action="duplicate_row">
			<i class="fa fa-files-o"></i>
			<label>Duplicate Row</label>
		</div><?php
	}

