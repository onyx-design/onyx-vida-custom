<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

// $ovc_row should always be available when this is included from OVC_Table

	// DELETE ROW
	if( $ovc_row->can_delete() ) {
		?><div class="oxmn-li oxmn-li-ovcdt-actions oxmn--delete-row oxmn-section-break" data-row-action="delete_row">
			<i class="fa fa-times"></i>
			<label>Delete Row</label>
		</div><?php
	}


	// API REFRESH
	global $current_user;
	if( array_intersect( array( 'administrator', 'vida_sr_data_tech', 'vida_data_tech' ), $current_user->roles ) ) {
		?><div class="oxmn-li oxmn-li-ovcdt-actions oxmn--api-refresh" data-row-action="api_refresh">
			<i class="fa fa-refresh api-refresh"></i>
			<label title="Refresh local data from Walmart via the Walmart API">Walmart API Refresh</label>
		</div><?php
	}
?>