<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

// $ovc_row should always be available when this is included from OVC_Table

	// DELETE ROW
	global $current_user;
	if( array_intersect( array( 'administrator', 'vida_sr_data_tech' ), $current_user->roles ) ) {
		?>
		<div class="oxmn-li oxmn-li-ovcdt-actions oxmn--delete-row oxmn-section-break" data-row-action="delete_row">
			<i class="fa fa-times"></i>
			<label>Delete Row</label>
		</div>
	
		<div class="oxmn-li oxmn-li-ovcdt-actions oxmn--api-refresh" data-row-action="force_shopify_sync">
			<i class="fa fa-refresh force-shopify-sync" style="color:#ca4a1f;"></i>
			<label title="Force sync this row to Shopify via the Shopify API">Force Shopify Sync</label>
		</div>
		<?php
	}

	// API REFRESH
	?>
	<div class="oxmn-li oxmn-li-ovcdt-actions oxmn--api-refresh" data-row-action="api_refresh">
		<i class="fa fa-refresh api-refresh"></i>
		<label title="Refresh local data from Shopify via the Shopify API">Shopify API Refresh</label>
	</div>

	<div class="oxmn-li oxmn-li-ovcdt-actions oxmn--api-refresh" data-row-action="reset_shopify_images">
		<i class="fa fa-refresh reset-shopify-images"></i>
		<label title="Reset the Shopify Images to force sync new image sets">Reset Shopify Images</label>
	</div>