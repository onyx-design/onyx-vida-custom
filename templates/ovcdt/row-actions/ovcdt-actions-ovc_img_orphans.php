<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

// $ovc_row should always be available when this is included from OVC_Table

	// DELETE ROW

	?><div class="oxmn-li oxmn-li-ovcdt-actions oxmn-section-break" data-row-action="delete_img_orphan">
		<i class="fa fa-times"></i>
		<label>Delete This Image</label>
	</div><?php
	

