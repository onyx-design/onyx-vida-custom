<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

// $ovc_row should always be available when this is included from OVC_Table

	// DELETE ROW
	if( $ovc_row->can_delete() && ( 1 !== $ovc_row->data( 'unimgs.in_use' ) ) ) {
		?><div class="oxmn-li oxmn-li-ovcdt-actions oxmn--delete-row oxmn-section-break" data-row-action="delete_image">
			<i class="fa fa-times"></i>
			<label>Delete Image</label>
		</div><?php
	}
