<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

// $ovc_row should always be available when this is included from OVC_Table

	// DELETE ROW
	if( $ovc_row->can_delete() && 2 == get_current_user_id() ) {
		?><div class="oxmn-li oxmn-li-ovcdt-actions oxmn--delete-row" data-row-action="delete_row">
			<i class="fa fa-times"></i>
			<label>Delete Row</label>
		</div><?php
	}
