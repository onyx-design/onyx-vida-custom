<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

// $ovc_row should always be available when this is included from OVC_Table


	// FILE (VIEW AND DOWNLOAD)
	$file = new OVC_File( $ovc_row->ID );

	if( $file->exists() ) {
		?><div class="oxmn-li oxmn-li-ovcdt-actions oxmn--view-file">
			<i class="fa fa-eye can-view"></i>
			<label><a class="ovcdt-action-link" href="<?php echo $file->url(); ?>" target="_blank">View file</a></label>
		</div><?php

			global $current_user;

			if( array_intersect( array( 'administrator', 'vida_sr_data_tech' ), $current_user->roles ) ) {
				?><div class="oxmn-li oxmn-li-ovcdt-actions oxmn--download-file">
					<i class="fa fa-download can-download"></i>
					<label><a class="ovcdt-action-link" href="<?php echo $file->url(); ?>" download>Download file</a></label>
				</div><?php
			}
	}
	else {
		?><div class="oxmn-li oxmn-li-ovcdt-actions oxmn--view-file oxmn-disabled">
			<i class="fa fa-eye"></i>
			<label>File not found</label>
		</div><?php
	}
