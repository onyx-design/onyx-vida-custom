<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

// Delete OVC Image
global $current_user;
if( array_intersect( array( 'administrator' ), $current_user->roles ) ) { ?>
	<div class="oxmn-li oxmn-li-ovcdt-actions oxmn--delete-row" data-row-action="delete_row">
		<i class="fa fa-times"></i>
		<label>Delete Row</label>
	</div>
<?php } ?>

<?php if( array_intersect( array( 'administrator', 'vida_sr_data_tech' ), $current_user->roles ) ) { ?>
	<div class="oxmn-li oxmn-li-ovcdt-actions oxmn--resolve_error" data-row-action="resolve_error">
		<i class="fa fa-check"></i>
		<label>Resolve Error</label>
	</div>
<?php } ?>

<?php // dev:improve dev:generalize THIS IS HARD CODED TO NOT SHOW CHECK ERROR ON OMS ERRORS, THIS NEEDS TO BE CHANGED ASAP ?>
<?php if( strpos( $ovc_row->data( 'errcd.error_code' ), 'oms' ) !== 0 ) { ?>
	<div class="oxmn-li oxmn-li-ovcdt-actions oxmn--check_error" data-row-action="check_error">
		<i class="fa fa-eye"></i>
		<label>Check Error</label>
	</div>
<?php } ?>