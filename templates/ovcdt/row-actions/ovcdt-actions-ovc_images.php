<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

// Delete OVC Image
global $current_user;
if( array_intersect( array( 'administrator' ), $current_user->roles ) ) {
?>
	<div class="oxmn-li oxmn-li-ovcdt-actions oxmn--delete-row" data-row-action="delete_row">
		<i class="fa fa-times"></i>
		<label>Delete Row</label>
	</div>
<?php
}
?>
<div class="oxmn-li oxmn-li-ovcdt-actions oxmn--regenerate-image" data-row-action="regenerate_image">
	<i class="fa fa-refresh"></i>
	<label>Regenerate Image</label>
</div>
<?php
// If there isn't an override, show this
if( !$ovc_row->data( 'pic.image_override_path' ) ) {
?>
<div class="oxmn-li oxmn-li-ovcdt-actions oxmn--upload-override" data-row-action="screen_upload_image">
	<i class="fa fa-upload"></i>
	<label>Upload Override</label>
</div>
<?php
}
// If there is an override, show this
if( $ovc_row->data( 'pic.image_override_path' ) ) {
?>
<div class="oxmn-li oxmn-li-ovcdt-actions oxmn--delete-override" data-row-action="delete_override_image">
	<i class="fa fa-times"></i>
	<label>Delete Override</label>
</div>
<?php
}
//