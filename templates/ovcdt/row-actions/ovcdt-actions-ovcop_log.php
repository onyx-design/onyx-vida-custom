<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

// $ovc_row should always be available when this is included from OVC_Table

	// VIEW OPERATION
	?>
	<div class="oxmn-li oxmn-li-ovcdt-actions oxmn--view-operation" data-row-action="view_operation">
		<i class="fa fa-eye can-view"></i>
		<label title="View Operation Details">View Operation</label>
	</div>
	<?php if( $ovc_op_log = $ovc_row->get_log_file() ) { ?>
	<div class="oxmn-li oxmn-li-ovcdt-actions oxmn--view-log">
		<i class="fa fa-eye can-view"></i>
		<label title="View Log">
			<a class="ovcdt-action-link" href="<?php echo $ovc_op_log->url(); ?>" target="_blank">View Log</a>
		</label>
	</div>
	<?php
	}

	// CSV FILE
	$op_csv_ids = $ovc_row->get_children( 'ovcop_files', 'ID', $this->results, array( 'var' => '::[file_type]', 'value' => 'csv' ) );

	if( count( $op_csv_ids ) ) {
		$file_id = $op_csv_ids[0];
		$csv_file = new OVC_File( $file_id );

		if( $csv_file->exists() ) {
			?><div class="oxmn-li oxmn-li-ovcdt-actions oxmn--download-op-csv">
				<i class="fa fa-download can-download"></i>
				<label><a class="ovcdt-action-link" href="<?php echo $csv_file->url(); ?>" download>Download CSV</a></label>
			</div><?php
		}
		else {
			?><div class="oxmn-li oxmn-li-ovcdt-actions oxmn--download-op-csv oxmn-disabled">
				<i class="fa fa-download"></i>
				<label>CSV Not Found</label>
			</div><?php
		}
	}

	// START OPERATION
	// if( 'queued' == $ovc_row->data('ol.status') ) {
	// 	// Allow start operation if this is the
	// 	global $wpdb;
	// 	$queued_op_id = $wpdb->get_var( "SELECT ID FROM wp_ovcop_log WHERE status = 'queued' ORDER BY ID ASC LIMIT 0,1" ) ;

	// 	if( $queued_op_id == $ovc_row->ID ) {
	// 		<div class="oxmn-li oxmn-li-ovcdt-actions oxmn--commence-operation" data-row-action="commence_operation">
	// 			<i class="fa fa-times-circle"></i>
	// 			<label>Commence Operation</label>
	// 		</div>
	// 	}
	// }

	global $current_user;
	if( array_intersect( array( 'administrator', 'vida_sr_data_tech' ), $current_user->roles )
		&& $ovc_row->is_stuck() 
		&& in_array( $ovc_row->data( 'ol.status'), OVCOP::$running_statuses ) 
	) {
		?>
			<div class="oxmn-li oxmn-li-ovcdt-actions oxmn--reset-operation" data-row-action="reset_operation">
				<i class="fa fa-undo"></i>
				<label>Reset Operation</label>		
			</div>
		<?php
	}

	// CANCEL / ABORT OPERATION
	if( in_array( $ovc_row->data( 'ol.status' ), array_merge( OVCOP::$queued_statuses, OVCOP::$running_statuses  ) ) ) {
		?><div class="oxmn-li oxmn-li-ovcdt-actions oxmn--abort-operation" data-row-action="abort_operation">
			<i class="fa fa-times-circle"></i>
			<label>Cancel / Abort Operation</label>
		</div><?php
	}
