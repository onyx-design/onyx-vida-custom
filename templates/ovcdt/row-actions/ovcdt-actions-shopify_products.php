<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

// $ovc_row should always be available when this is included from OVC_Table

	// DELETE ROW
	global $current_user;
	if( array_intersect( array( 'administrator' ), $current_user->roles ) ) {
		?>
		<div class="oxmn-li oxmn-li-ovcdt-actions oxmn--delete-row oxmn-section-break" data-row-action="delete_row">
			<i class="fa fa-times"></i>
			<label>Delete Row</label>
		</div>
		<?php
	}

	
	?>
	<div class="oxmn-li oxmn-li-ovcdt-actions oxmn--sync-variant" data-row-action="sync_variant">
		<i class="fa fa-refresh api-refresh"></i>
		<label title="Force sync this variant to Shopify">Sync Variant</label>
	</div>

	<div class="oxmn-li oxmn-li-ovcdt-actions oxmn--api-refresh" data-row-action="api_refresh">
		<i class="fa fa-refresh api-refresh"></i>
		<label title="Refresh local data from Walmart via the Shopify API">Shopify API Refresh</label>
	</div>