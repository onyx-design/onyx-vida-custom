<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

// $ovc_row should always be available when this is included from OVC_Table

?>

<?php 
	// DELETE ROW
	// Include delete row action if user can delete by role
	if( $ovc_row->can_delete() ) {

		/*/ Maybe disable the option
		$delete_row_disabled = '';
		$prevent_delete_fields = array(
			'pr.op_oms_export_unconfirmed'
		);

		foreach( $prevent_delete_fields as $prevent_delete_field ) {

			if( $ovc_row->data( $prevent_delete_field ) ) {

				$delete_row_disabled = ' oxmn-disabled';
				break;
			}
		}
		/* */
		?>
		<div class="oxmn-li oxmn-li-ovcdt-actions oxmn--delete-row" data-row-action="delete_row">
			<i class="fa fa-times"></i>
			<label for="action_delete_row_<?php echo $ovc_row->ID; ?>">Delete Row</label>
		</div><?php
	}


	// DUPLICATE ROW
	if( $ovc_row->can_duplicate() ) {
		?><div class="oxmn-li oxmn-li-ovcdt-actions oxmn--duplicate-row oxmn-section-break" data-row-action="duplicate_row">
			<i class="fa fa-files-o"></i>
			<label>Duplicate Row</label>
		</div><?php
	}


	// PRICE FORMULA HELPER
	global $current_user;
	if( array_intersect( array( 'administrator', 'vida_sr_data_tech' ), $current_user->roles ) ) {
		?><div class="oxmn-li oxmn-li-ovcdt-actions oxmn--price-helper" data-row-action="price_formula_tool">
			<i class="fa fa-usd action-price_tool"></i>
			<label>Price Formula Tool</label>
		</div><?php

		?><div class="oxmn-li oxmn-li-ovcdt-actions oxmn--inventory_assembly_data" data-row-action="screen_inventory_assembly_data">
			<i class="fa fa-pie-chart action-inventory_assembly_data"></i>
			<label>Inventory Assembly</label>
		</div><?php
	}

	// SHARED PRODUCT DETAILS
	?><div class="oxmn-li oxmn-li-ovcdt-actions oxmn--shared_product_data" data-row-action="screen_shared_data">
		<i class="fa fa-tag action-product_type_data"></i>
		<label>Edit Shared Data</label>
	</div><?php

	?><div class="oxmn-li oxmn-li-ovcdt-actions oxmn--amazon_style_data" data-row-action="screen_amazon_style_data">
		<i class="fa fa-tag action-product_type_data"></i>
		<label>Edit Amazon Style Data</label>
	</div><?php

	// IMAGE MANAGER
	?>
	<!-- <div class="oxmn-li oxmn-li-ovcdt-actions oxmn--image_manager" data-row-action="product_image_manager"> -->
	<div class="oxmn-li oxmn-li-ovcdt-actions oxmn--image_manager" data-row-action="screen_image_manager">
		<i class="fa fa-picture-o action-product_images"></i>
		<label>Image Manager</label>
	</div>