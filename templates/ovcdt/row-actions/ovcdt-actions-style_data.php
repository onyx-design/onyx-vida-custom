<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

// $ovc_row should always be available when this is included from OVC_Table

	// DUPLICATE ROW
	if( $ovc_row->can_duplicate() ) {
		?><div class="oxmn-li oxmn-li-ovcdt-actions oxmn--duplicate-row" data-row-action="duplicate_row">
			<i class="fa fa-files-o"></i>
			<label>Duplicate Row</label>
		</div><?php
	}

?>

<!-- <div class="oxmn-li oxmn-li-ovcdt-actions oxmn--screen_amazon-style-data" data-row-action="screen_amazon_style_data">
	<i class="fab fa-amazon"></i>
	<label>Amazon Data</label>
</div> -->
