<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
?>
<div class="oxmn-container ovcdt-wa-show-duplicate-skus-wrap">
	<div class="oxmn-li oxmn-li-toplevel oxmn-li-checkbox">
		<?php 
			$checked = in_array( 'show_duplicate_skus', (array) $this->scopes ) ? ' checked' : '';
		?>
		<input type="checkbox" class="ovcdt-setscope ovcdt-clearscopes" id="ovcdt-wa-show-duplicate-skus" data-set-scope="show_duplicate_skus" <?php echo $checked ?> />
		<label for="ovcdt-wa-show-duplicate-skus">Show Duplicate SKUs</label>
	</div>
</div>

<div class="oxmn-container ovcdt-wa-show-duplicate-upcs-wrap">
	<div class="oxmn-li oxmn-li-toplevel oxmn-li-checkbox">
		<?php 
			$checked = in_array( 'show_duplicate_upcs', (array) $this->scopes ) ? ' checked' : '';
		?>
		<input type="checkbox" class="ovcdt-setscope ovcdt-clearscopes" id="ovcdt-wa-show-duplicate-upcs" data-set-scope="show_duplicate_upcs" <?php echo $checked ?> />
		<label for="ovcdt-wa-show-duplicate-upcs">Show Duplicate UPCs</label>
	</div>
</div>

<div class="oxmn-container ovcdt-wa-show-duplicate-waids-wrap">
	<div class="oxmn-li oxmn-li-toplevel oxmn-li-checkbox">
		<?php 
			$checked = in_array( 'show_duplicate_waids', (array) $this->scopes ) ? ' checked' : '';
		?>
		<input type="checkbox" class="ovcdt-setscope ovcdt-clearscopes" id="ovcdt-wa-show-duplicate-waids" data-set-scope="show_duplicate_waids" <?php echo $checked ?> />
		<label for="ovcdt-wa-show-duplicate-waids">Show Duplicate Walmart IDs</label>
	</div>
</div>

<div class="oxmn-container ovcdt-wa-show-duplicate-ovc_ids-wrap">
	<div class="oxmn-li oxmn-li-toplevel oxmn-li-checkbox">
		<?php 
			$checked = in_array( 'show_duplicate_ovc_ids', (array) $this->scopes ) ? ' checked' : '';
		?>
		<input type="checkbox" class="ovcdt-setscope ovcdt-clearscopes" id="ovcdt-wa-show-duplicate-ovc_ids" data-set-scope="show_duplicate_ovc_ids" <?php echo $checked ?> />
		<label for="ovcdt-wa-show-duplicate-ovc_ids">Show Duplicate OVC IDs</label>
	</div>
</div>
