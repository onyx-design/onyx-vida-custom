<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

final class OVC_Lists {
	use OVC_Singleton_Class_Abstract;

	public $lists = array();


	

 	/*/	1.	OVC Meta Data Retrieval - Full Lists /*/

 	public function get_list( $raw_list_name, $format = false, $force_refresh = false ) {

 		// Process $type in case it contains additional information
 		$list_name = explode( ':', $raw_list_name );

 		if( !is_array( $list_name ) ) {
 			return false;
 		}
 		else if ( count( $list_name ) > 1 ) {
 			$format = $list_name[ 1 ];
 		} 		

 		$list_name = $list_name[ 0 ];

 		// Get list meta information
 		if( !( $list_meta = OVCSC::get_field( $list_name, 'ovc_list' ) ) ) { // dev:ovcsc
 			return false;
 		}

 		// Finalize and verify $format
 		if( !$format ) {
 			$format = $list_meta->meta( 'default_format' ) ? $list_meta->meta( 'default_format' ) : $format; // dev:ovcsc
 		}
 		//$format = isset( $list_meta['default_format'] ) && !$format ? $list_meta['default_format'] : $format; // dev:ovcsc
 		
 		if( !in_array( $format, array( 'name', 'code', false ) ) ) {
 			return false;
 		}

 		if( 'simple' == $list_meta->meta( 'type' ) ) {
 			return $list_meta->meta( 'valid_values' );
 		}
 		else {
 			// Do we need to initialize the list?
 			if( !array_key_exists( $list_name, $this->lists ) || $force_refresh ) {
 				$raw_list = $this->ovc_get_terms( $list_meta->meta( 'type' ) );
 				$list = array();

 				// Maybe turn raw list of term slugs=>names into codes=>names
 				if( $list_meta->meta( 'slug_prefix' ) ) {
 					foreach( $raw_list as $term_slug => $term_name ) {
 						$code = strtoupper( str_replace( $list_meta->meta( 'slug_prefix' ), '', $term_slug ) );

 						$list[ $code ] = $term_name;
 					}
 				}
 				else {
 					$list = $raw_list;
 				}

 				$this->lists[ $list_name ] = $list;
 			}

 			if( 'code' == $format ) {
 				return array_keys( $this->lists[ $list_name ] );
 			}
 			else {
 				return $this->lists[ $list_name ];
 			}
 		}
 	}

 	public function get_list_item_code( $list_name = false, $list_item_name = false ) {
 		if( !$list_name || !$list_item_name ) {
 			return false;
 		}

 		$list = $this->get_list( $list_name, 'name' );

 		return array_search( $list_item_name, $list );
 	}

 	public function get_list_item_name( $list_name = false, $list_item_code = false ) {
 		if( !$list_name || !$list_item_code ) {
 			return false;
 		}

 		$list = $this->get_list( $list_name, 'name' );

 		return isset( $list[ $list_item_code ] ) ? $list[ $list_item_code ] : null;
 	}

 	public function ovc_get_terms( $taxonomy = '' ) {
 		$term_data = array();

 		global $wpdb;

 		$raw_term_data = $wpdb->get_results( $wpdb->prepare("SELECT slug, name FROM {$wpdb->terms} t INNER JOIN {$wpdb->term_taxonomy} tt ON t.term_id = tt.term_id WHERE tt.taxonomy = %s", $taxonomy ), ARRAY_A );
 	
 		if( is_array( $raw_term_data ) ) {
 			$term_data = array_column( $raw_term_data, 'name', 'slug' );	
 		}

 		return $term_data;
 	}

 	// DEV:GENERALIZE! (across all list types)
 	public function get_size_code_slug( $size_code = '' ) {
 		if( !$size_code || !OVCDB()->validate_value_by_var_type( $size_code, 'list:size:code' ) ) { // dev:validation
 			return false;
 		}
 		else {
 			return ( 'size-' . strtolower( $size_code ) );
 		}
 	}

 	// DEV:GENERALIZE! (across all list types)
 	public function get_brand_name_slug( $brand_name = '' ) {
 		if( !$brand_name || !OVCDB()->validate_value_by_var_type( $brand_name, 'list:brand:name' ) ) { // dev:validation
 			return false;
 		}
 		else {
 			return ( sanitize_title( $brand_name ) );
 		}
 	}
	
}