<?php
/* 
Handles all OVC Migrations
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Migrations {

	static $migration_dir = OVC_PATH . '/instance/migrations/';

	public static function check_migrations() {

		// Get OVC Migration Version
		// 20180705 was the file we were up to when this Migration class was created
		$ovc_migration_version = get_option( 'ovc_migration_version', '20180705' );

		$migration_files = array_filter( scandir( self::$migration_dir, SCANDIR_SORT_DESCENDING ), function( $value ) {

			if( 0 === strpos( $value, '.' ) ) {
				return false;
			}

			return true;
		} );

		// If the greatest Migration file has a newer version than ovc_migration version, we're running migrations
		if( explode( '_', $migration_files[0], 2 )[0] > $ovc_migration_version ) {

			sort( $migration_files );

			foreach( $migration_files as $migration_file ) {

				$migration_data = self::get_migration_data( $migration_file );

				// Only run future migrations with this system
				if( $migration_data['version'] <= $ovc_migration_version ) {
					continue;
				}

				require_once( self::$migration_dir . $migration_file );

				$migration = new $migration_data['class']();
				$migration->up();

				update_option( 'ovc_migration_version', $migration_data['version'] );
			}
		}

		return true;
	}

	private static function get_migration_data( $migration_file ) {

		$migration_pieces = explode( '_', $migration_file, 2 );

		$version 	= $migration_pieces[0];
		$file 		= $migration_pieces[1];
		$class 		= 'OVC_Migration_' . explode( '.', $migration_pieces[1] )[0];

		return array(
			'version' 	=> $version,
			'file'		=> $file,
			'class'		=> $class
		);
	}

	public static function col_exists( $field_set, $col_name ) {
		$field_set = OVCSC::get_field_set( $field_set, 'database' );

		$col_exists = null;

		if( $field_set ) {
			global $wpdb;

			$sql = " SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS ".
				" WHERE table_name = '{$field_set->table_name()}' ".
				" AND table_schema = '" . DB_NAME . "' ".
				" AND column_name = '{$col_name}';";

			$col_exists = $wpdb->get_var( $sql );
		}
		
		return $col_exists;
	}

	public static function add_col_if_not_exists( $field_set, $col_name, $col_definition, $after_col = null ) {
		$field_set = OVCSC::get_field_set( $field_set, 'database' );
		
		if( $field_set && !self::col_exists( $field_set, $col_name ) ) {

			global $wpdb;

			$sql = "ALTER TABLE `{$field_set->table_name()}` ADD `{$col_name}` {$col_definition} ";

			if( !empty( $after_col ) ) {
				$sql .= "AFTER `{$after_col}`";
			}

			$sql .= ";";

			return $wpdb->query( $sql );
		}
	}

	public static function update_col( $field_set, $col_name, $col_definition, $after_col = null ) {
		$field_set = OVCSC::get_field_set( $field_set, 'database' );

		if( $field_set && self::col_exists( $field_set, $col_name ) ) {

			global $wpdb;

			$sql = "ALTER TABLE `{$field_set->table_name()}` MODIFY `{$col_name}` {$col_definition} ";

			if( !empty( $after_col ) ) {
				$sql .= "AFTER `{$after_col}`";
			}

			$sql .= ";";

			return $wpdb->query( $sql );
		}
	}

	public static function drop_col_if_exists( $field_set, $col_name ) {
		$field_set = OVCSC::get_field_set( $field_set, 'database' );

		if( $field_set && self::col_exists( $field_set, $col_name ) ) {

			global $wpdb;

			$sql = "ALTER TABLE `{$field_set->table_name()}` DROP COLUMN `{$col_name}`";

			return $wpdb->query( $sql );
		}
	}

	public static function add_table_if_not_exists( $field_set ) {
		$field_set = OVCSC::get_field_set( $field_set, 'database' );

		if( $field_set ) {

			global $wpdb;

			$charset_collate = $wpdb->get_charset_collate();

			$sql = "CREATE TABLE IF NOT EXISTS `{$field_set->table_name()}` (
				`ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
				PRIMARY KEY  (`ID`)
			) $charset_collate;";

			return $wpdb->query( $sql );
		}
	}
}