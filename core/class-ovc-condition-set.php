<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Condition_Set {

	public $conditions;
	public $row = null;

	public $fields = null;

	public $result = null;

	public $results = null;

	//public $extract_fields = true;

	public function __construct( $conditions, $row = array(), &$results = false, $extract_fields = true ) {
		OVC_Results::maybe_init( $results );

		$this->conditions = $conditions;
		$this->row = $row;

		/*
		if( !empty( $options['extract_fields'] ) ) {
			$this->extract_fields = $options['extract_fields'];
		}
		*/

		$this->evaluate( $extract_fields, $results );
	}

	public function is_valid() {

		return is_array( $this->conditions );
	}

	public function evaluate( $extract_fields = false, &$results = false ) {
		OVC_Results::maybe_init( $results );

		// Maybe init fields
		if( !isset( $this->fields ) ) {
			$this->fields = array();
		}


		$this->result = $this->evaluate_condition( $this->conditions, $extract_fields, $results );

		$this->fields = array_unique( $this->fields );

		return $this->result;
	}

	public function evaluate_condition( $condition, $extract_fields = false, $results = false ) {
		OVC_Results::maybe_init( $results );

		if( !$condition ) {
			return false;
		}
		// We are down to just evaluating a value (but it could be an expression!)
		else if( is_scalar( $condition ) ) {

			$condition_expression = new OVC_Expression( $condition, $this->row, $results );

			$condition_output = $condition_expression->evaluate( true, $extract_fields, $results );

			$this->fields = array_merge( $this->fields, $condition_expression->get_fields( false, $results ) );

			return (bool) $condition_output;
			//return (bool) $condition_expression->evaluate( true, true );
		}
		// We already know that it has to be an array with a length greater than zero
		else if( is_array( $condition ) ) {

			// If array of length 1, evaluate single value
			if( 1 == count( $condition ) ) {

				if( isset( $condition[0] ) ) {

					return $this->evaluate_condition( $condition[0], $extract_fields, $results );
				}
				else if( isset( $condition['var'] ) ) {

					return $this->evaluate_condition( $condition['var'], $extract_fields, $results );	
				}
				// Invalid condition
				else {

					return null;
				}
			}
			// Is this a full condition definition?
			else if( isset( $condition['var'] ) ) {

				return $this->evaluate_single_condition( $condition, $extract_fields, $results );
			}
			// Is this a condition group?
			else if( isset( $condition['relation'] ) ) {

				// Validate the relation value
				if( in_array( $condition['relation'], array( 'AND', 'OR' ) ) ) {

					$relation = $condition['relation'];	
				}
				else {
					// INVALID: Invalid relation value
					return null;
				}

				// Negated defaults to false
				$negated = empty( $condition['negated'] ) ? false : true;

				if( empty( $condition['conditions'] ) || !is_array( $condition['conditions'] )  ) {
					// INVALID: Missing or invalid conditions
					return null;
				}
				else {

					$group_result = null;

					foreach( $condition['conditions'] as $condition_segment ) {

						$segment_result = $this->evaluate_condition( $condition_segment, $extract_fields, $results );

						if( 'AND' == $relation && !$segment_result ) {
							$group_result = false;
						}
						else if( 'OR' == $relation && $segment_result ) {
							$group_result = true;
						}

						// We can't exit the loop early if we need to extract all of the fields
						if( isset( $group_result ) && !$extract_fields ) {
							break;
							//$group_result = $negated ? !$group_result : $group_result;
							//return $group_result;
							//return ( $negated ? !$group_result : $group_result );
						}
					}

					// 
					if( !isset( $group_result ) && 'AND' == $relation ) {
						$group_result = true;
					}

					return (bool) ( $negated ? !$group_result : $group_result );

					return $group_result;
					//return ( $negated ? !$group_result : $group_result );
				}
				
			}
			// Invalid condition
			else {

				return null;
			}
		} 
	}

	public function evaluate_single_condition( $condition, $extract_fields = true, &$results = false ) {
		OVC_Results::maybe_init( $results );
		// Operator defaults to equals if not set
		$operator = isset( $condition['op'] ) ? $condition['op'] : '==';

		// Negation defaults to false if not set
		$negated = empty( $condition['negated'] ) ? false : true;




		// Validate and evaluate var and test value
		foreach( array( 'var', 'value') as $key ) {

			// dev:generalize
			if( in_array( $operator, array( 'valid_nonzero', 'in_array', 'exists_in' ) ) && 'var' == $key ) {
				$var_value = (array) $condition['var'];
				continue;
			}

			if( !isset( $condition[ $key ] ) ) {
				// INVALID condition format
				return null;
			}

			// Dynamic var name (e.g. $var_expression )
			${"${key}_expression"} = new OVC_Expression( $condition[ $key ], $this->row, $results, $extract_fields );

			if( !${"${key}_expression"}->is_valid() ) {
				// INVALID: Invalid expression; could not evaluate //dev:error_handling
				return null;
			}
			else {

				${"${key}_value"} = ${"${key}_expression"}->evaluate();

				$this->fields = array_merge( $this->fields, ${"${key}_expression"}->get_fields() );
			}
		}

		$result = null;

		switch( $operator ) {

			case '=':
			case '==':
				$result = ( $var_value == $value_value );
			break;
			case '!=':
				$result = ( $var_value != $value_value );
			break;
			case '>':
				$result = ( $var_value > $value_value );
			break;
			case '<':
				$result = ( $var_value < $value_value );
			break;
			case '>=':
				$result = ( $var_value >= $value_value );
			break;
			case '<=':
				$result = ( $var_value <= $value_value );
			break;
			case 'in_array':
				$result = in_array( $value_value, $var_value );
			break;
			case 'is_changed':
				$result = $this->row->is_field_changed( $var_value );
			break;
			case 'exists_in':

				// Process Query WHERE data
				$where_data = array();

				foreach( $var_value as $where_key => $where_vars ) {


					if( is_array( $where_vars ) ) {

						$where_value_expression = new OVC_Expression( $where_vars['value'], $this->row, $results, $extract_fields );
						$where_vars['value'] = $where_value_expression->evaluate();
					}

					$where_data[ $where_key ] = $where_vars;
				}

				$search_field = OVCSC::get_field( $value_value );

				//$result = (bool) count( OVCDB()->get_table_data( $search_field->field_set, $where_data, $results ) );
				$result = (bool) count( OVCDB()->get_table_data( $search_field->field_set, $where_data, $results ) );

			break;
			case 'valid_nonzero':


				$result = true;

				foreach( $var_value as $field_name ) {

					$field = OVCSC::get_field( $field_name );

					//dev:error_handling

					//ovc_dev_log( $field->local_name . " " . $field->global );

					$new_value = $this->row->new_value( $field );

					

					
					if( !$this->row->new_value( $field ) ) {

						$result = false;
						$results->notice( "WARNING! Dependency failure: Field {$value_value} requires that field {$field->global_name} is nonzero" );
					}
				}
			break;
			case 'is_valid':
			
				$result = OVCDB()->validate_value_by_var_type( $value_value, $var_value, $results );
			break;

			default:
				//INVALID: Invalid operator
				//dev:error_handling
				return null;
			break;
		}

		return (bool) ( $negated ? !$result : $result );
		
	}
}