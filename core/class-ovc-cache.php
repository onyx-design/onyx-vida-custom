<?php
/* 
Handles OVC Caching
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Cache {
	use OVC_Singleton_Class_Abstract;

	protected static $schema = null;

	protected static $list = null;

	protected static $data = null;

	protected static $html = null;

	protected function __construct() {}

	public static function set( $key, $address = '', $data, $raw = false ) {

		$data = $raw ? $data : maybe_serialize( $data );

		self::create_dirs( $address );

		file_put_contents( self::get_path( $key, $address, $raw ), $data );
	}

	public static function get( $key, $address, $callback = false ) {

		$data = null;

		if( self::key_exists( $key, $address ) ) {

			$data = maybe_unserialize( file_get_contents( self::get_path( $key, $address ) ) );
		}

		return $data;
	}

	public static function delete( $key, $address, $raw = false ) {

		$path = self::get_path( $key, $address, $raw );

		return self::delete_path( self::get_path( $key, $address, $raw ) );
	}

	

	protected static function key_exists( $key = '', $address = '', $create_dirs = false ) {

		$key_exists = file_exists( self::get_path( $key, $address ) );

		if( !$key_exists && $create_dirs ) {

			self::create_dirs( $address ); //dev:error_handling //dev:results
		}

		return $key_exists;
	}

	protected static function create_dirs( $address = '' ) {

		$path = self::get_path( '', $address );

		if( !file_exists( $path ) ) {

			return mkdir( $path, 0770, true );
		}
	}

	public static function get_path( $key = '', $address = '', $raw = false ) {

		if( !$raw && $key && false === strpos( $key, '.ovc' ) ) {
			$key .= ".ovc";
		}

		return rtrim( rtrim( OVC_CACHE_DIR . "/{$address}", '/' ) . "/{$key}", '/' );
	}

	protected static function delete_path( $path ) {

		if( is_dir( $path ) ) {
			return self::delete_dir( $path );
		}
		else if( is_file( $path ) ) {
			return unlink( $path );
		}
	}



	public static function empty_dir( $address = '' ) {

		$dirname = self::get_path( '', $address );

		if( is_dir( $dirname ) ) {
			$dir_handle = opendir($dirname);
		}

		if( !$dir_handle ) {
			return false;
		}

		while( $file = readdir($dir_handle) ) {

			if( $file != "." && $file != ".." ) {
				if( !is_dir( $dirname.'/'.$file ) ) {
					unlink( $dirname.'/'.$file );
				}
				else {
					self::delete_dir( $dirname.'/'.$file );
				}
			}
		}

		closedir($dir_handle);
		
		return true;
	}

	protected static function delete_dir( $dirname ) {

		if( !is_dir( $dirname ) ) {
			return false;
		}

		self::empty_dir( $dirname );

		rmdir( $dirname );

		return true;
	}
}