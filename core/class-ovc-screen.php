<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Screen {
	public $args = false;
	public $default_response = array(
		'config'	=> array(
			'show_tabs' => true,
			'active_tab'=> ''
		),
		'content' => array(
			'tabs'		=> '',
			'title' 	=> '',
			'subtitle'	=> '',
			'body'		=> '',
			'footer' 	=> ''
		)
	);
	public $response = array();


	public function __construct( $args ) {

		$this->args = $args;

		if( 'load' == $args['request'] ) {
			$this->load_screen( $args );
		}

		$this->default_response['content']['tabs'] = $this->get_default_tabs();
		$this->default_response['content']['footer'] = $this->get_default_footer();
		
	}

	public function load_screen( $args ) {

		$this->response = $this->default_response; //dev:improve

		$this->response['content']['body'] = $this->load_template( $this->args['screen'] );

		$this->response['config']['active_tab'] = $this->args['screen'];

		$ovc_product = OVCDB()->get_row( FS( 'ovc_products' ), $args['options']['ovc_id'] );

		switch( $args['screen'] ) {
			case 'screen_shared_data':
				$this->response['content']['title'] = 'Edit Shared Data for Style: ' . $ovc_product->data( 'st.style' ) . ', Parent SKU: ' . $ovc_product->parent_sku;
				$this->response['content']['subtitle'] = "You are viewing / editing shared data for OVC ID: {$ovc_product->ID}, SKU: {$ovc_product->sku}";
			break;
			// case 'screen_style_data':
			// 	$this->response['content']['title'] = 'Edit Shared Style Data for Style ' . $ovc_product->data( 'st.style' );
			// 	$this->response['content']['subtitle'] = "You are viewing / editing shared product data for OVC ID: {$ovc_product->ID}, SKU: {$ovc_product->sku}";
			// break;
			// case 'screen_parent_data':
			// 	$this->response['content']['title'] = 'Parent Product Data - Shared Data for Parent SKU: ' . $ovc_product->parent_sku ;
			// 	$this->response['content']['subtitle'] = 'This information is shared by all products with Parent SKU: ' . $ovc_product->parent_sku ;
			// break;
			case 'screen_category_data':
				$this->response['content']['title'] = 'Edit Shared Protuct Category Data for Style ' . $ovc_product->data( 'st.style' );
				$this->response['content']['subtitle'] = "You are viewing / editing shared product data for OVC ID: {$ovc_product->ID}, SKU: {$ovc_product->sku}";
			break;
			case 'screen_upload_image':
				$this->response['content']['title'] = 'Image Override Upload';
				$this->response['config']['show_tabs'] = false;

				$footer = '<div class="oxmd-response-button" onclick="ovcAction(\'ovc-screen-close\');">Close</div>';
				$footer .= '<div class="oxmd-response-button primary-response" onclick="ovc.screen.uploadFile();">Upload</div>';

				$this->response['content']['footer'] = $footer;
			break;
			case 'screen_inventory_assembly_data':
				$this->response['content']['title'] = 'Inventory Assembly for Parent SKU: ' . $ovc_product->data( 'pr.parent_sku' );
				$this->response['content']['subtitle'] = "You are viewing / editing inventory assemblies for OVC ID: {$ovc_product->ID}, SKU: {$ovc_product->sku}";

				$footer = '<div class="oxmd-response-button" onclick="ovcAction(\'ovc-screen-close\');">Close</div>';
				$footer .= '<div class="oxmd-response-button" onclick="ovc.screen.refresh();">Refresh</div>';
				$footer .= '<div class="oxmd-response-button primary-response" onclick="ovc.screen.saveInventoryAssembly();">Save</div>';
				
				$this->response['content']['footer'] = $footer;
			break;
			case 'screen_amazon_style_data':
				$this->response['content']['title'] = 'Edit Amazon Style Data for Style: ' . $ovc_product->data( 'st.style' ) . ', Parent SKU: ' . $ovc_product->parent_sku;
				$this->response['content']['subtitle'] = "You are viewing / editing amazon style data for OVC ID: {$ovc_product->ID}, SKU: {$ovc_product->sku}";
			break;
			case 'screen_image_manager':
				$this->response['content']['title'] = 'Manage Product Images';
				$this->response['content']['subtitle'] = "You are viewing / editing image sets for SKU: {$ovc_product->sku}, Product Title [Parent]: {$ovc_product->data( 'pa.product_title' )}";
				// $this->response['config']['show_tabs'] = false;

				$footer = '<div class="oxmd-response-button" onclick="ovcAction(\'ovc-screen-close\');">Close</div>';
				$footer .= '<div class="oxmd-response-button" onclick="ovc.screen.refresh();">Refresh</div>';
				$footer .= '<div class="oxmd-response-button primary-response" onclick="ovc.screen.saveImageManager();">Save</div>';

				$this->response['content']['footer'] = $footer;
			break;
			default:
				$this->response['content']['title'] = 'Invalid OVC Screen';
			break;
		}


	}

	public function load_template( $template ) {
		$template_html = $this->get_include_contents( OVC_PATH . "/templates/ovc-screen/{$template}.php" );

		return ( $template_html !== false ? $template_html : "Failed to load OXMD template: {$template}" );
	}

	private function get_include_contents( $filename ) {
	    if (is_file($filename)) {
	        ob_start();
	        include $filename;
	        return ob_get_clean();
	    }
	    return false;
	}

	private function get_default_tabs() {
		$tabs = array(
			'screen_shared_data'				=> 'Shared Data',
			'screen_image_manager'				=> 'Image Manager',
			'screen_amazon_style_data' 			=> 'Amazon Style Data',
			'screen_inventory_assembly_data'	=> 'Inventory Assembly'
		);

		$tabs_html = '';

		foreach( $tabs as $tab_id => $tab_title ) {

			if( $tab_id == $this->args['screen'] ) {
				$tabs_html .= '<div class="ovc-screen-tab active-tab">'.$tab_title.'</div>';
			}
			else {
				$tabs_html .= '<div class="ovc-screen-tab" onclick="ovc.screen.loadScreen(\''.$tab_id.'\',{ovc_id:'.$this->args['options']['ovc_id'].'});">'.$tab_title.'</div>';
			}
		}

		return $tabs_html;

	}

	private function get_default_footer() {
		$footer = '';
		$footer .= '<div class="oxmd-response-button" onclick="ovcAction(\'ovc-screen-close\');">Close</div>';
		$footer .= '<div class="oxmd-response-button" onclick="ovc.screen.refresh();">Refresh</div>';
		$footer .= '<div class="oxmd-response-button primary-response" onclick="ovc.screen.saveScreen();">Save</div>';
		//$footer .= '<div class="oxmd-response-button primary-response" onclick="ovc.screen.save(true);">Save &amp; Close</div>';
		return $footer;
	}



	public function ajax_response() {

		// $this->response = array_merge_recursive( $this->default_response, $this->response );

		if( !empty( $this->args['options']['refresh'] ) ) {

			unset( $this->response['content']['tabs'] );
			unset( $this->response['content']['footer'] );
			unset( $this->response['content']['title'] );
			unset( $this->response['content']['subtitle'] );
		}
		else {

			if( !empty( $this->response['config']['show_tabs'] ) 
				&& $this->response['config']['show_tabs'] ) {
				$this->response['content']['tabs'] = !empty( $this->response['content']['tabs'] ) ? $this->response['content']['tabs'] : $this->get_default_tabs();	
			}
			$this->response['content']['footer'] = !empty( $this->response['content']['footer'] ) ? $this->response['content']['footer'] : $this->get_default_footer();
		}

		return $this->response;
	}
}