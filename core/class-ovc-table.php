<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Table {
	const OVCDT_VERSION = "0.12.0";

	// Request and response
	public $request;
	public $request_status = 'unset';

	public $response_data = array();
	public $results;

	// Relevant OVC Schema
	public $dt_meta = false; // OVC Field for this OVCDT type (OVCDT meta)
	public $field_set = 'none';
	public $pKey = false;
	
	// Front-end UI options and features
	public $pre_data_col = false;
	public $advanced_controls = false;
	public $show_filters = true;
	public $pagination = true;
	public $top_scrollbar = true;
	public $wait_to_save = true;

	// User custom selection
	public $custom_selection = false;
	public $cs_mode_enabled = false;
	public $filter_cs_ids = false;
	public $user_cs_ids = false;

	// Access and permissions
	public $check_only = true;
	public $view_access = true;
	public $edit_access = null;

	// Query parameters
	public $offset;
	public $page_num 	= 1;
	public $per_page 	= 50;
	public $sorting;
	public $filters;
	public $forced_filters;

	public $qry = array( 
		'fields'	=> array(),
		'select'	=> '',
		'from'		=> '',
		'where'		=> '',
		'order'		=> '',
		'sql'		=> ''
	);

	// Query results and data
	protected $table_data = null;
	
	public $total_rows;
	public $total_pages;

	public $forced_reload = false;
	public $field_groups = false;
	public $scopes = false;

	public $datalists = array();
		
	// Live Data (In Development)
	public $live_data = false;
	public $session_id = '';
	public $session = false;

	public $header_overrides = array();

	public $field_tags = array();

	// Dev:generalize! This reference shouldn't be here
	public $numeric_var_types = array(
		'int' 		=> array( 'step' => '1'),
		'stock'		=> array( 'step' => '0.01'),
		'price'		=> array( 'step' => '0.01'),
		'float'		=> array( 'step' => '0.001'),
		'volume' 	=> array( 'step' => '0.0001'),
		'float6' 	=> array( 'step' => '0.000001')
	);

	public function __construct( $request = '', $args = array(), OVC_Field $dt_meta ) {
		
		$this->response_data['result_sets'] = array();
		$this->results = new OVC_Results();
		
		$this->dt_meta = $dt_meta;

		$field_set = $dt_meta->main_field_set ? $dt_meta->main_field_set : $dt_meta->local_name;
		$this->field_set = FS( $field_set );

		$this->pKey = $this->field_set->get_primary_key();
		
		if( $this->version_check( $args['ovcdt_version'] ) ) { // Verify version match before processing request
			if( method_exists( $this, $request) ) {

				$this->check_only = ( isset( $args['check_only'] ) && NULL !== filter_var( $args['check_only'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE ) ? filter_var( $args['check_only'], FILTER_VALIDATE_BOOLEAN ) : true );

				$this->request = $request;
				$this->$request( $args );
			}
			else {
				die( "INVALID OVCDT REQUEST TYPE: {$request}" );
			}
		}
	}

	public function __get( $var_name ) {

		if( 'sql_alias' == $var_name ) {
			return $this->field_set->sql_alias;
		}
		else if( 'db_table' == $var_name || 'table_name' == $var_name || 'table' == $var_name ) {
			return $this->field_set->table_name();
		}
	}

	protected function version_check( $request_version ) {
		if( 0 != version_compare( self::OVCDT_VERSION, $request_version ) ) {
			$this->request_status = 'version_mismatch';
			$this->results->success = false;
			$this->results->main_msg = 'Request failed: Version Mismatch.';
			$this->response_data['result_sets'][] = get_object_vars( $this->results );

			$this->response_data['request_version'] = $request_version;
			$this->response_data['current_version'] = self::OVCDT_VERSION;

			return false;
		}
		else {
			return true;
		}
	}

	protected function check_ovcdt_session() {
		if( $this->live_data ) {
			$this->session = new OVCDT_Session( $this->session_id, $this->field_set->name );
			$this->session_id = $this->session->sesh_id;
		}
	}

	public function load_data( $args = array() ) {
		// Maybe override default property values with supplied args
		foreach ( $args as $arg => $value ) {
			if( property_exists( $this, $arg ) ) {
				$this->$arg = ox_maybe_str_to_bool( $value );
			}
		}
		$this->offset = ( $this->page_num - 1 ) * $this->per_page; // Set offset

		/*/ OVCDT Session Handling // dev:improve // development of this on hold
		if( $this->live_data && 2 == get_current_user_id() ) { // dev:indev - remove admin id check
			$this->check_ovcdt_session();
		}
		else {
			$this->live_data = false;
		}
		*/

		// Get User Custom Selection for this table
		$this->cs_mode_enabled = $this->custom_selection && $this->cs_mode_enabled ? true : false;
		if( $this->custom_selection ) {
			$this->get_user_custom_selection();
		}

		// Initialize Query Pieces
		$this->initialize_qry_values();
		
		// Prepare response data
		$this->request_status = 'success';
		$this->results->success = true;
		$this->results->main_msg = "Data Loaded.";
		$this->response_data['result_sets'][] = get_object_vars( $this->results );

		$meta_data = array(
			'type'			=> $this->dt_meta->local_name,
			'page_num' 		=> intval( $this->page_num ),
			'offset'		=> $this->offset,
			'count'			=> intval( $this->total_rows ),
			'per_page'		=> intval( $this->per_page ),
			'total_pages'	=> $this->total_pages,
			'show_filters'	=> $this->show_filters,
			'filters'		=> $this->filters,
			'forced_filters'=> $this->forced_filters,
			'sorting'		=> $this->sorting,
			'sql_alias'		=> $this->sql_alias,
			'field_groups'	=> $this->field_groups,
			'scopes'		=> $this->scopes,
			'cs_mode_enabled'=> $this->cs_mode_enabled,
			'top_scrollbar' => $this->top_scrollbar,
			'wait_to_save'	=> $this->wait_to_save,
			'live_data'		=> (int) $this->live_data,
			'session_id'	=> $this->session_id,
 			'ovcdt_version' => self::OVCDT_VERSION,
 			'forced_reload' => $this->forced_reload
		);

		/*/ ONLY NEEDED FOR DEBUGGING
		$meta_data['last_query'] = $this->qry['sql'];
		$meta_data['qry_where'] = $this->qry['where'];
		*/
		//ovc_dev_log( 'before build table: ' . ox_exec_time() );
		// Add response data
		$more_response_data = array(
			'meta'		=> $meta_data,
			'html'		=> $this->advanced_controls_meta_row_html() . $this->results_meta_row_html() . $this->table_data_html() . $this->results_meta_row_html()
		);
		//ovc_dev_log( 'after build table: ' . ox_exec_time() );

		if( $this->live_data && false !== $this->session ) {
			$this->session->update();
		}

		// Maybe add OVCDT live session data
		// DEV:BUILD!!

		$this->response_data = array_merge( $this->response_data, $more_response_data );
	}

	public function ovcdt_ping( $args = array() ) {

		if( $this->live_data && isset( $args['session_id'] ) ) {
			$this->session_id = $args['session_id'];
			$this->check_ovcdt_session();
			$this->results->success = true;
			$this->request_status = 'success';

			$this->response_data['updated_data'] = $this->session->get_updated_data(  );
		}
	}

	public function load_actions_menu( $requestData = array() ) {

		$ovc_row = OVCDB()->get_row( $this->field_set, $requestData['data']['rowID'], $this->results );

		if( $ovc_row->exists() ) {

			$actions_menu_html = "";

			$filename = OVC_PATH . "/templates/ovcdt/row-actions/ovcdt-actions-{$this->field_set->name}.php";

			if ( is_file( $filename ) ) {
				ob_start();
				include $filename;
				$actions_menu_html = trim( ob_get_clean() );
			}

			$actions_menu_html = $actions_menu_html ? $actions_menu_html : "No Actions...";

			$this->response_data['actions_menu_html'] = $actions_menu_html;

			$this->response_data['actions_menu_row_id'] = $ovc_row->ID;
		}
	}

	public function save_data( $requestData = array() ) {
		if( !is_array( $requestData ) || !is_array( $requestData['data'] ) ) {
			// DEV TODO: CREATE STANDARDIZED ERROR RESPONSE FORMAT
			die( "Improper data sent for OVCDT save_data request." );
		}
		else {
			//$this->qry['table']	= $this->field_set->table_name();
			
			$this->response_data['updated_data'] = array();
			$this->response_data['request_data'] = $requestData;
			$this->response_data['force_reload'] = false;

			foreach( $requestData['data'] as $request ) {

				$request_type = $request['type'];

				// If multiple request objects, check to see if one has failed yet; if so, cancel the rest
				if( 'fail' == $this->request_status ) {
					$this->results->error( "Unable to continue the save_data request for {$request['type']} portion because previous {$failed_request_portion} portion failed." );
				}
				else if( in_array( $request['type'], array( 'save_row', 'delete_row', 'duplicate_row', 
					// 'amazon_style_data', 
					'api_refresh', 'force_shopify_sync', 'reset_shopify_images', 'regenerate_image', 'check_fields_to_fix', 'delete_img_orphan', 'save_productimages', 'save_productassemblies', 'sync_variant', 'upload_override_image', 'delete_override_image', 'delete_image', 'resolve_error', 'check_error' ) ) ) {
					// Before processing the request, make sure fields are in OVC format (as opposed to OVCDT) // dev:revamp!!
					//ovc_dev_log( $request );


					if( !isset( $request['where'], $request['where']['ID'] ) 
						|| 1 != count( $request['where'] ) 
						|| !( intval( $request['where']['ID'] ) 
								&& !is_array( $request['where']['ID'] ) 
							) 
					) {
						$this->results->error( "Invalid 'where' data sent for 'save_row' request." );
					}
					else {

						$row_id = intval( $request['where']['ID'] );

						if( 'save_row' == $request_type ) {

							$this->save_row( $row_id, $request['data'] );
						}
						else {
							$this->$request_type( $row_id );
						}
					}

					if( $this->results->ovcdt_force_reload ) {
						$this->response_data['force_reload'] = true;	
					}

					$this->request_status = $this->results->success ? 'success' : 'fail';
					$this->response_data['result_sets'][] = get_object_vars( $this->results );
					$failed_request_portion = ( 'fail' == $this->request_status ? $request['type'] : false );
				}
				else {
					$this->results->success = false;
					$this->results->error( "Unknown save_data request type: {$request['type']}" );
				}
			}

			if( 'success' == $this->request_status && $this->check_only ) {
				$this->results->notice( "Check Only mode active: save_data request results are simulated." );
			}
		}
	}

	public function save_row( $ID = 0, $data = array() ){
		
		$row = OVCDB()->get_row( $this->field_set, $ID, $this->results );

		if( !$row->exists() ) {
			return $this->results->error( "Invalid Row ID in OVCDT save_row request" );
		}

		$this->results->success = $row->update_data( $data, $this->results );
		$this->results->main_msg = ( $this->results->success ? "Changes saved successfully. ({$this->results->outcome})" : "Error saving changes!" );

		$this->merge_updated_data_row( $row->ID, $row->data_set( false, true ) );

		// Check the $results object for other IDs that got updated
		if( isset( $this->results->updated_ids ) && is_array( $this->results->updated_ids ) ) {

			foreach( $this->results->updated_ids as $updated_id ) {

				$updated_data = OVCDB()->get_row_by_id( $updated_id, $this->pKey->local_name, $this->db_table, true );

				$this->merge_updated_data_row( $updated_id, $updated_data );
			}
		}
	}

	public function delete_row( $ID = 0 ) {
		$this->results->main_msg = "Unable to delete row."; // This will be changed below if the request was successful

		if( $this->check_only ) {
			$this->results->notice( "Cannot delete rows while 'Check Only' mode is activated." );
		}
		else {

			$row = OVCDB()->get_row( $this->field_set, $ID, $this->results );

			$this->results->success = $row->delete( $this->results );

			$this->results->main_msg = $this->results->success ? "Row {$ID} Deleted" : "Error deleting ID {$ID}";

			$this->request_status = $this->results->success ? 'success' : 'fail';

			if( $this->results->success ) {
				$this->response_data['force_reload'] = true;
			}
		}
	}

	public function duplicate_row( $row_id = 0 ) {
		if( $this->check_only ) {
			$this->results->notice( "Cannot duplicate rows while 'Check Only' mode is activated." );
		}
		else {
			
			// Initialize duplicated row
			$ovc_row = OVCDB()->get_row( $this->field_set, $row_id, $this->results );
	
			$this->results->main_msg = "Duplicate row request failed."; // Will get changed if it succeeds

			if( !$ovc_row || !$ovc_row->exists() ) {
				$this->results->error( "Error trying to duplicate row. Source row to be copied not found.  {$this->pKey->global_name} : {$row_id}" );
			}
			else {

				// Copy the row's data
				$duplicate_data = $ovc_row->data_set( $this->field_set, true );

				// Remove Primary Key and reset any other fields that need to be reset
				//unset( $duplicate_data[ $ovc_row->field_set->get_primary_key()->global_name ] );

				// Remove / set field values on duplicated row data
				foreach( $this->field_set->get_single_meta_key_values( 'on_duplicate_value', false, false, true ) as $field_global_name => $duplicate_value ) {

					if( isset( $duplicate_data[ $field_global_name ] ) ) {

						$duplicate_data[ $field_global_name ] = $duplicate_value;
					}
				}				

				// special handling to ensure sku is unique (field meta changes sku_pkqty to 0 ) // dev:generalize
				if( 'ovc_products' == $this->field_set->name ) {
					
					$sku_expression = new OVC_Expression( $this->field_set->field_meta( 'sku', 'calc_formula' ), $duplicate_data );
					$duplicate_data['pr.sku'] = $sku_expression->evaluate();
				}

				$duplicate_row = OVCDB()->new_row( $this->field_set, $duplicate_data )->save( $this->results );

				if( 'inserted' == $this->results->outcome ) {
					$this->results->success = true;
					$this->results->main_msg = "Duplicate row request succeeded!";
					$this->response_data['force_reload'] = true;
					global $wpdb;
					$this->results->notice( "Duplicated Row. {$this->pKey->global_name}: {$row_id} successfully duplicated. {$this->pKey->local_name} of duplicate: {$wpdb->insert_id}" );
				}
			}
		}
	}

	public function save_productimages( $requestData = array() ) {

		$img_set_request_data = $requestData['data'];
		$img_sets = $img_set_request_data['image_sets'];

		// Save parent images (via the target variation //dev:improve )
		$product = OVCDB()->get_row( FS( 'ovc_products' ), $img_set_request_data['targetID'], $this->results );

		$parent_img_data = array(
			'pa.img_main' 						=> $img_sets['parent']['img_main'],
			'pa.img_gallery_ids'				=> isset( $img_sets['parent']['img_gallery_ids'] ) ? implode( ',', $img_sets['parent']['img_gallery_ids'] ) : '',
			'pa.img_marketplace_featured' 		=> $img_sets['parent']['img_marketplace_featured'],
		);

		$product->get_parent()->update_data( $parent_img_data, $this->results );

		// Remove image_set ids from specified OVC IDs
		if( isset( $img_set_request_data['ovc_ids_no_img_set'] ) ) {
			foreach( $img_set_request_data['ovc_ids_no_img_set'] as $ovc_id ) {

				$product = OVCDB()->get_row( FS( 'ovc_products' ), $ovc_id , $this->results );
				$product->update_field( 'pr.image_set', 0, $this->results );
			}
		}

		// Loop through image_sets
		foreach( $img_sets as $img_set_id => $img_set ) {

			// Skip parent and image sets that don't have any OVC IDs (they will be cleaned up later)
			if( 'parent' == $img_set_id || !isset( $img_set['ovc_ids'] ) || !count( $img_set['ovc_ids'] ) ) {
				continue;
			}

			$img_set_id = is_numeric( $img_set_id ) ? intval( $img_set_id ) : null;

			// Workaround for empty arrays being removed from JSON
			$img_gallery_ids = isset( $img_set['img_gallery_ids'] ) && is_array( $img_set['img_gallery_ids'] ) ? implode( ',', $img_set['img_gallery_ids'] ) : '';

			// Prepare updated img set data
			$updated_img_set_data = array(
				'img.img_main' 					=> $img_set['img_main'],
				// 'img.img_oms1'		=> $img_set['img_oms1'],
				// 'img.img_oms2'		=> $img_set['img_oms2'],
				'img.img_swatch'				=> $img_set['img_swatch'],
				'img.img_case'					=> $img_set['img_case'],
				'img.img_amazon'				=> $img_set['img_amazon'],
				'img.img_sizeguide'				=> $img_set['img_sizeguide'],
				'img.img_gallery_ids'			=> $img_gallery_ids,
				'img.img_marketplace_featured' 	=> $img_set['img_marketplace_featured'],
			);

			//$row_results = new OVC_Results();

			$image_set = OVCDB()->get_row( FS( 'image_sets' ), $img_set_id, $this->results );

			$image_set->update_data( $updated_img_set_data, $this->results );

			$img_set_id = $image_set->ID;
			

			// Update the image sets for the rest of the products
			foreach( $img_set['ovc_ids'] as $ovc_id ) {

				$product = OVCDB()->get_row( FS( 'ovc_products' ), $ovc_id, $this->results );
				$product->update_field( 'pr.image_set', $img_set_id, $this->results );
			}
		}

		$this->results->success = !$this->results->has_error;
		$this->results->main_msg = $this->results->success ? "Image Sets Saved Successfully" : "Error saving image sets.";

		$this->response_data['request_data'] = $requestData;

		$this->request_status = $this->results->success ? 'success' : 'fail';

		$this->response_data['force_reload'] = $this->results->success;
	}

	public function save_productassemblies( $requestData = array() ) {

		$inventory_assembly_request_data = $requestData['data'];
		$product_assemblies = $inventory_assembly_request_data['asm'];
		$product_assembly_items = $inventory_assembly_request_data['asmi'];
		$products = $inventory_assembly_request_data['pr'];

		$resultData = array();
		$assembly_map = array();

		foreach( $product_assemblies as $product_assembly_id => $product_assembly ) {

			$updated_product_assembly_data = array(
				'asm.ovc_id'		=> $product_assembly['ovc_id'],
				'asm.total_quantity'=> $product_assembly['total_quantity'],
				'asm.primary_case'	=> $product_assembly['primary_case'],
			);

			$assembly = OVCDB()->get_row( FS( 'ovc_assemblies' ), $product_assembly_id, $this->results );
			$assembly->update_data( $updated_product_assembly_data, $this->results );

			$assembly_map[ $product_assembly_id ] = $assembly->ID;

			$resultData['asm'][ $assembly->ID ] = array(
				'ovc_id'		=> $assembly->ovc_id,
				'total_quantity'=> $assembly->total_quantity,
				'primary_case'	=> $assembly->primary_case,
				'creating_id'	=> $product_assembly_id
			);
		}

		foreach( $product_assembly_items as $product_assembly_item_id => $product_assembly_item ) {

			$updated_product_assembly_item_data = array(
				'asmi.assembly_id'	=> $assembly_map[ $product_assembly_item['assembly_id'] ],
				'asmi.ovc_id'		=> $product_assembly_item['ovc_id'],
				'asmi.quantity'		=> $product_assembly_item['quantity'],
			);

			$assembly_item = OVCDB()->get_row( FS('ovc_assembly_items' ), $product_assembly_item_id, $this->results );
			$assembly_item->update_data( $updated_product_assembly_item_data, $this->results );

			$resultData['asmi'][ $assembly_item->ID ] = array(
				'assembly_id'	=> $assembly_item->assembly_id,
				'ovc_id'		=> $assembly_item->ovc_id,
				'quantity'		=> $assembly_item->quantity,
				'creating_id'	=> $product_assembly_item_id
			);
		}

		$this->results->success = !$this->results->has_error;
		$this->results->main_msg = $this->results_success ? "Inventory Assemblies Saved Successfully" : "Error saving inventory assemblies.";

		$this->response_data['request_data'] = $requestData;

		$this->request_status = $this->results->success ? 'success' : 'fail';

		$this->response_data['data'] = $resultData;
		// $this->response_data['force_reload'] = $this->results->success;
	}

	public function api_refresh( $ID = '' ) {

		$ovc_row = OVCDB()->get_row( $this->field_set, $ID, $this->results );

		if( !$ovc_row->exists() ) {
			
			$this->response_data['force_reload'] = true;
			return $this->results->error( "API Refresh failed, row does not exist." );
		} else if ( !method_exists( $ovc_row, 'api_refresh' ) ) {
			return $this->results->error( "API Refresh failed, Class " . get_class( $ovc_row ) . 'does not have an api_refresh() method' );
		} else {

			$ovc_row->api_refresh( $this->results );
		}

		if( $this->results->has_error )  {
			$this->results->success = false;
			$this->results->main_msg = "API Refresh failed!";
		} else {
			$this->results->success = true;
			$this->results->main_msg = "API Refresh succeeded!";
			$this->response_data['force_reload'] = true;
		}
	}

	public function regenerate_image( $ID = '' ) {

		$ovc_row = OVCDB()->get_row( $this->field_set, $ID, $this->results );

		if( !$ovc_row->exists() ) {

			$this->response_data['force_reload'] = true;
			return $this->results->error( "Force Shopify Sync failed, row does not exist." );
		} elseif( !method_exists( $ovc_row, 'regenerate_image' ) ) {
			return $this->results->error( "Regenerate Image failed, Class " . get_class($ovc_row) . " does not have a regenerate_image() method" );
		} else {

			if( !$ovc_row->regenerate_image( $this->results ) ) {
				$this->response_data['force_reload'] = true;
			}
		}

		if( $this->results->has_error ) {

			$this->results->success = false;
			$this->results->main_msg = "Regenerate Image failed!";
		} else {

			$this->results->success = true;
			$this->results->main_msg = "Regenerate Image succeeded!";
			$this->response_data['force_reload'] = true;
		}

		$this->response_data['result_sets'][] = get_object_vars( $this->results );
	}

	public function delete_override_image( $ID = '' ) {

		$ovc_image = OVCDB()->get_row( $this->field_set, $ID, $this->results );

		$this->results->success = $ovc_image->delete_override( $this->results );

		$this->results->main_msg = $this->results->success ? "Override Image for Row {$ID} Deleted" : "Error deleting Override Image for Row {$ID}";

		$this->request_status = $this->results->success ? 'success' : 'fail';

		if( $this->results->success ) {
			$this->response_data['force_reload'] = true;
		}
	}

	public function force_shopify_sync( $ID = '' ) {

		$ovc_row = OVCDB()->get_row( $this->field_set, $ID, $this->results );

		if( !$ovc_row->exists() ) {

			$this->response_data['force_reload'] = true;
			return $this->results->error( "Force Shopify Sync failed, row does not exist." );
		}
		else if( !method_exists( $ovc_row, 'force_shopify_sync' ) ) {
			return $this->results->error( "Force Shopify Sync failed, Class " . get_class($ovc_row) . " does not have a force_shopify_sync() method" );
		}
		else {

			$ovc_row->force_shopify_sync( $this->results );
		}

		if( $this->results->has_error ) {

			$this->results->success = false;
			$this->results->main_msg = "Force Shopify Sync failed!";
		}
		else {

			$this->results->success = true;
			$this->results->main_msg = "Force Shopify Sync succeeded!";
			$this->response_data['force_reload'] = true;
		}

	}

	public function reset_shopify_images( $ID = '' ) {

		$ovc_row = OVCDB()->get_row( $this->field_set, $ID, $this->results );

		if( !$ovc_row->exists() ) {

			$this->response_data['force_reload'] = true;
			return $this->results->error( "Reset Shopify Images failed, row does not exist." );
		} elseif( !method_exists( $ovc_row, 'reset_shopify_images' ) ) {
			return $this->results->error( "Reset Shopify Images failed, Class " . get_class($ovc_row) . " does not have a reset_shopify_images() method" );
		} else {

			$ovc_row->reset_shopify_images( $this->results );
		}

		if( $this->results->has_error ) {

			$this->results->success = false;
			$this->results->main_msg = "Reset Shopify Images failed!";
		} else {

			$this->results->success = true;
			$this->results->main_msg = "Reset Shopify Images succeeded!";
			$this->response_data['force_reload'] = true;
		}
	}

	public function sync_variant( $ID ) {

		if( 'shopify_products' != $this->field_set->name ) {
			$this->results->success = false;
			$this->results->main_msg = "Sync Variant failed!";
			$this->results->error( "Can only perform sync_variant row action on shopify_products rows" );
		} else {

			$shopify_variant = OVCDB()->get_row( FS( 'shopify_products' ), $ID, $this->results );

			$shopify_variant->sync_variant( $this->results );

			if( $this->results->has_error ) {

				$this->results->success = false;
				$this->results->main_msg = "Sync Variant failed!";
			} else {

				$this->results->success = true;
				$this->results->main_msg = "Sync Variant succeeded!";
				$this->response_data['force_reload'] = true;
			}
		}
	}

	public function merge_updated_data_row( $ID, $updated_data = array() ) {
		if( !isset( $ID ) || !is_array( $updated_data ) ) {
			return false;
		}

		if( !is_array( $this->response_data['updated_data'] ) ) {
			$this->response_data['updated_data'] = array();
		}

		$ID = strval( $ID ); // Cast ID to string for use as an associative array key (numeric keys can get messed up by AJAX / JSON conversion)

		// Maybe initialize updated_data array for this row ID
		if( !array_key_exists( $ID, $this->response_data['updated_data'] ) ) {
			$this->response_data['updated_data'][ $ID ] = array();
		}

		// Sanitize output data
		foreach( $updated_data as $field => $value ) {

			if( is_string( $value ) ) {
				$updated_data[ $field ] = htmlspecialchars( stripslashes( $value ) );
			}
		}

		$this->response_data['updated_data'][ $ID ]	= array_merge( $this->response_data['updated_data'][ $ID ], $updated_data );

		OVCDB()->make_fields_array_ovcdt_format( $this->response_data['updated_data'][ $ID ], $this->field_set, true );
	}

	// CUSTOM SELECTION HANDLER FUNCTIONS

	protected function get_user_custom_selection() {

		if( false === $this->user_cs_ids ) {
			$user_cs_ids = maybe_unserialize( get_user_meta( get_current_user_id(), "_ovcdt_custom_select_{$this->field_set->name}", true ) );

			$this->user_cs_ids = $user_cs_ids ? $user_cs_ids : array();
		}
		
		return $this->user_cs_ids;
	}

	public function save_user_custom_selection( $requestData = array() ) {
		$new_user_cs_ids = $requestData['data'];

		$this->get_user_custom_selection();

		foreach( $new_user_cs_ids as $cs_id => $selected ) {
			
			// Will be true if $cs_id is selected
			$selected = (bool) $selected;

			// Does the state of $cs_id's selection in supplied data match whether it is selected in teh current user custom selection?
			if( $selected != in_array( $cs_id, $this->user_cs_ids ) ) {
				if ( $selected ) {
					$this->user_cs_ids[] = $cs_id;
				}
				else {
					$this->user_cs_ids = array_diff( $this->user_cs_ids, array( $cs_id ) );
				}
			}
		}

		$this->user_cs_ids = array_map( 'intval', $this->user_cs_ids );

		update_user_meta( get_current_user_id(), "_ovcdt_custom_select_{$this->field_set->name}", $this->user_cs_ids );

		$this->results->success = true;
		$this->results->main_msg = "User custom selection saved.";
		$this->response_data['result_sets'][] = get_object_vars( $this->results );
		$this->response_data['force_reload'] = true;
	}

	public function bulk_user_custom_selection_by_filters( $args = array() ) {

		global $wpdb;

		// THIS TOP SECTION IS A DUPLICATE OF THE LOAD_DATA METHOD // dev:improve
		// Maybe override default property values with supplied args
		foreach ( $args as $arg => $value ) {
			if( property_exists( $this, $arg ) ) {
				$this->$arg = ox_maybe_str_to_bool( $value );
			}
		}
		$this->offset = ( $this->page_num - 1 ) * $this->per_page; // Set offset

		$this->get_user_custom_selection();

		// Initialize Query Pieces
		$this->initialize_qry_values();

		// Get all IDs in current filters

		$sql = "SELECT {$this->pKey->global_name} {$this->qry['from']}{$this->qry['where']}";

		$selected_ids = $wpdb->get_col( $sql );

		switch( $args['bulk_action'] ) {
			case 'add_all':
				$this->user_cs_ids = array_unique( array_merge( $this->user_cs_ids, $selected_ids ) );
			break;
			case 'set_all':
				$this->user_cs_ids = $selected_ids;
			break;
			case 'remove_all':
				$this->user_cs_ids = array_diff( $this->user_cs_ids, $selected_ids );
			break;
			default:
			break;
		}

		update_user_meta( get_current_user_id(), "_ovcdt_custom_select_{$this->field_set->name}", $this->user_cs_ids );

		$this->results->success = true;
		$this->results->main_msg = "User custom selection saved.";
		$this->response_data['result_sets'][] = get_object_vars( $this->results );
		$this->response_data['force_reload'] = true;
	}

	public function clear_user_custom_selection( $requestData = array() ) {
		update_user_meta( get_current_user_id(), "_ovcdt_custom_select_{$this->field_set->name}", array() );

		$this->results->success = true;
		$this->results->main_msg = "User custom selection cleared.";
		$this->response_data['result_sets'][] = get_object_vars( $this->results );
		$this->response_data['force_reload'] = true;
	}

	public function get_table_data() {

		// Maybe init table data
		if( !isset( $this->table_data ) ) {
			global $wpdb;
			$this->table_data = $wpdb->get_results( $this->qry['sql'], ARRAY_A );

			if( count( $this->table_data ) ) {
				
				// Calculate string maxlengths
				foreach( $this->qry['fields'] as $field => $meta ) {
					
					if( !isset( $meta['var_type'] ) 
						|| !array_key_exists( $meta['var_type'], $this->numeric_var_types )
					) {

						$_max_strlen = 0;
						
						foreach( array_column( $this->table_data, $field ) as $value ) {
							$_max_strlen = max( $_max_strlen, strlen( $value ) );
						}

						$this->qry['fields'][ $field ]['_max_strlen'] = $_max_strlen;
					}
				}
			}
		}

		return (array) $this->table_data;
	}

	public function ajax_response() {

		//( 'before ovcdt ajax_response: ' . ox_exec_time() );
		$response = array( 
			'request'	=> $this->request,
			'status'	=> $this->request_status,
			'data'		=> $this->response_data
		);

		return $response;
	}

	public function table_data_html() {

		//$special_handling_keys = array( 'shared_data_key' ); // dev:deprecated

		// Record any special field meta that needs to be sent to the front end
		foreach( $this->qry['fields'] as $field => $meta ) {
			if( isset( $meta['calc_formula'] ) ) {
				if( !isset( $this->response_data['formulas'] ) ) {
					$this->response_data['formulas'] = array();
				}

				$this->response_data['formulas'][ $field ] = $meta['calc_formula'];
			}
			/* //dev:deprecated
			else if( array_intersect( array_keys( $meta ), $special_handling_keys ) ) {
				if( !isset( $this->response_data['special_handling'] ) ) {
					$this->response_data['special_handling'] = array();
				}

				$this->response_data['special_handling'][ $field ] = array();

				
				foreach ( $special_handling_keys as $key ) {
					if( isset( $meta[ $key ] ) ) {
						$this->response_data['special_handling'][ $field ][ $key ] = $meta[ $key ];
					}
				}
			}
			*/
		}

		// Prepare additional classes
		$table_classes = 'loadingData ovcdt-table';
		
		$table_classes .= $this->cs_mode_enabled ? '' : ' cs-mode-disabled';

		$html_table = '<div class="ox-row ovcdt-tight-wrap"><table id="ovcdt-main-table" class="' . $table_classes . '" border="0" cellpadding="0" cellspacing="0">';

		// Top scrollbar
		if( $this->top_scrollbar ) {
			$top_scrollbar = '<div class="ovcdt-top-scrollbar"><div class="ovcdt-dummy-content"></div></div>';
			$html_table = $top_scrollbar . $html_table;	
		}
		
		$html_table .= $this->table_headers_html();

		$html_table .= '<tbody>';

		// Maybe get ready to build front-end user_cs_id reference array
		if( $this->custom_selection ) {
			$this->response_data['user_cs_ids'] = array();
		}

		// Reference objects & arrays used in loops
		$td_elem = new OVC_HTML_Tag_Builder( 'td' );

		foreach( $this->get_table_data() as $row ) {
			$row_id = $row[ $this->pKey->global_name ];
			$html_table .= "<tr data-row-id=\"{$row_id}\">";

			// PRE DATA COLUMNS
			
			
			$pre_data_row_html = 
				'<td class="actions">'.
					'<div class="oxmn-container oxmn-has-submenu">'.
						'<div class="oxmn-li oxmn-li-toplevel">Actions</div>'.
						'<div class="oxmn-submenu oxmn-fixed oxmn-force-hide oxmn-loading"></div>'.
					'</div>'.
				'</td>';

			// Add cell for custom selection
			if( $this->custom_selection ) {


				//$pre_data_row_html .= '<td class="custom-select-toggle-id" data-col="cs-toggle"><div class="input-wrap">'; // dev:pre_optimize
				$pre_data_row_html .= '<td class="custom-select-toggle-id" data-col="cs-toggle">';

				$cs_checked = '';
				$orig_value = '';

				if( in_array( $row_id, $this->user_cs_ids ) ) {
					$cs_checked = 'checked="checked"';
					$orig_value = $row_id;
				}

				$pre_data_row_html .= '<input type="checkbox" class="otdi otdi--bool ovcdt-cs-toggle" name="cs-toggle[' . $row_id . ']" value="' . $row_id . '" ' . $cs_checked . '/>';

				//$pre_data_row_html .= '</div></td>'; // dev:pre_optimize
				$pre_data_row_html .= '</td>';

				$this->response_data['user_cs_ids'][ "{$row_id}" ] = $orig_value;
			}

			// Maybe add pre data column (Actions column)
			$pre_data_row_html .= ( method_exists( $this, 'get_pre_data_column_row_html') ? $this->get_pre_data_column_row_html( $row, false ) : '' ); //dev:improv

			$html_table .= $pre_data_row_html;


			// MAIN DATA COLUMNS
			foreach ( $row as $index => $cell ) {


				if( !isset( $this->qry['fields'][ $index ]['ovcdt_hide'] )
				 || !$this->qry['fields'][ $index ]['ovcdt_hide']
				) {

					// determine $var_type // dev:use fields!
					$var_type = isset( $this->qry['fields'][ $index ]['var_type'] ) ? $this->qry['fields'][ $index ]['var_type'] : 'string';

					// If not cached, build a tag object into the field tag cache
					if( !array_key_exists( $index, $this->field_tags ) ) {

						$input_elem_args = array(
							'type'	=> 'input',
							'attrs' => array(
								'type'	=> 'text',
								'class'	=> array( 'otdi' )
							)
						);

						$input_elem = new OVC_HTML_Tag_Builder( $input_elem_args );

						// Special Var Type handling for numeric or list types
						if( array_key_exists( $var_type, $this->numeric_var_types ) 
							&& $index != $this->pKey->global_name
						) {
							$numeric_attrs = array(
								'type' 	=> 'number',
								'min'	=> '0'
							);

							$numeric_attrs = array_merge( $numeric_attrs, $this->numeric_var_types[ $var_type ] );

							$input_elem->multi_attr( $numeric_attrs );
						}
						else if( 0 === strpos( $var_type, 'list:' ) ) {
							$raw_list_name = str_replace( 'list:', '', $var_type );

							$datalist = "datalist-{$raw_list_name}";

							$input_elem->attr( 'list', $datalist );

							if( !in_array( $datalist, $this->datalists ) ) {
								$this->datalists[] = $datalist;
							}
						}
						else if( 'bool' == $var_type ) {
							$input_elem->attr( 'type', 'checkbox' );
							$input_elem->add_class( 'otdi--bool' );
						}

						// Maybe incorporate any input_attr fieldmeta overrides
						if( isset( $this->qry['fields'][ $index ]['input_attr'] ) 
							&& is_array( $this->qry['fields'][ $index ]['input_attr'] )
						) {
							$input_elem->multi_attr( $this->qry['fields'][ $index ]['input_attr'] );
						}
						
						// Detect field meta keys that should automatically be added to input_attr
						foreach( array( 'pattern', 'maxlength', 'required', 'readonly' ) as $magic_meta ) { // DEV TODO: GENERALIZE!!!!
							if( isset( $this->qry['fields'][ $index ][ $magic_meta ] ) ) {
								$input_elem->attr( $magic_meta, stripslashes( $this->qry['fields'][ $index ][ $magic_meta ] ) );
							}
						}
							
						// Maybe force readonly if user does not have edit_access
						if( !$this->edit_access ) {
							$input_elem->attr( 'readonly', 'true' );
						}

						//$this->field_tags[ $index ] = $input_elem->export();
						$this->field_tags[ $index ] = $input_elem;
					}


					// Get the element from the cache and process individual handling (setting value)
					$input_elem = clone $this->field_tags[ $index ];

					$value = is_string( $cell ) ? htmlspecialchars( stripslashes( $cell ) ) : strval( $cell ); // DEV:FIGURE OUT STRING DISPLAY

					// Special individual handling by var_type
					if( 'stock' == $var_type ) {
						$value = rtrim( rtrim( $value, '0 ' ), '.' );
					}
					else if( 'bool' == $var_type ) {
						if( intval( $value ) > 0 ) {
							$input_elem->attr( 'checked', 'checked' );
						}
						else {
							$input_elem->attr( 'checked', false );
						}
					}

					// Maybe 'lock' the cell based on intra-row conditions
					$locked_by_condition = false;

					if( null === $input_elem->attr( 'readonly' )
						 && isset( $this->qry['fields'][ $index ]['locked_if'] )
					) {

						$locked_conditional = new OVC_Condition_Set( $this->qry['fields'][ $index ]['locked_if'], $row );

						// Evaluate locked conditions (failsafe locks the cell if conditional is invalid)
						if( $locked_conditional->evaluate() ) {

							$input_elem->attr( 'readonly', 'true' );
						} 
						
						/*
						foreach( $this->qry['fields'][ $index ]['locked_if'] as $locked_condition ) {

							if( is_string( $locked_condition ) && array_key_exists( $locked_condition, $row ) ) {

								if( $row[ $locked_condition ] ) {

									$locked_by_condition = true;
									break;
								}
							}
						}

						if( $locked_by_condition ) {
							$input_elem->attr( 'readonly', 'true' );
						}
						*/
					}

					// Maybe add classes to the cell based on intra-rwo conditions
					

					if( isset( $this->qry['fields'][ $index ]['add_class_if'] ) ) {

						$condition_fulfilled = false;

						foreach( $this->qry['fields'][ $index ]['add_class_if']['conditions'] as $condition ) {

							if( is_string( $condition ) && array_key_exists( $condition, $row ) ) {

								if( $row[ $condition ] ) {

									$condition_fulfilled = true;
									break;
								}
							}
	
						}

						if( $condition_fulfilled ) {
							$input_elem->add_class( $this->qry['fields'][ $index ]['add_class_if']['class'] );
						}

					}

					// Set the cell value
					$input_elem->attr( 'value', $value );
					$input_elem->attr( 'data-og', $value );


					// Build TD elem with otdi <input> elem in it, then add it to the html
					$td_elem->attr( 'data-col', $index );

					/*/ Maybe add freeze_pane class
					if( isset( $this->qry['fields'][ $index ]['ovcdt_freeze_pane'] )
						&& $this->qry['fields'][ $index ]['ovcdt_freeze_pane']
					) {
						$td_elem->add_class( 'freeze-pane' );
					}
					else {
						$td_elem->remove_class( 'freeze-pane' );
					}
					*/

					$td_elem->contents( $input_elem->get_html() );

					$html_table .= $td_elem->get_html();

				}
			}

			$html_table .= '</tr>'; // End of row
		}

		$html_table .= '</tbody></table></div>';

		// Build datalist elements if there are any
		if( $this->datalists ) {

			$datalist_html = '';
			foreach ( $this->datalists as $datalist ) {
				$dl_items = array();
				$list_type = str_replace( 'datalist-', '', $datalist );

				$list_meta = OVCSC::get_field( $list_type, 'ovc_list' );

				$dl_items = OVC_Lists()->get_list( $list_type );

				$datalist_html .= '<datalist id="'.$datalist.'" class="ovcdt-datalist">';

				// Maybe add blank option
				if( $list_meta && $list_meta->meta( 'allow_blank' ) ) {
					$datalist_html .= '<option value="">';
				}

				foreach( $dl_items as $dl_item ) {
					$datalist_html .= '<option value="'.$dl_item.'">';
				}
				$datalist_html .= '</datalist>';
			}
			$html_table .= $datalist_html;
		}

		return $html_table;
	}


	public function table_headers_html() {
		$table_headers_html = '<thead>';

		if( $this->field_groups ) {
			$table_headers_html .= '<tr class="ovcdt-headers" id="ovcdt-field-group">';

			$table_headers_html .= '<th></th>';

			foreach( array_filter( $this->field_groups ) as $fg ) {

				$fg_nice_name = $this->dt_meta->field_groups[ $fg ];
				$fg_data = $this->dt_meta->meta( 'fg.' . $fg );

				$table_headers_html .= '<th colspan="' . count( $fg_data ) . '">' . $fg_nice_name . '</th>';
			}

			$table_headers_html .= '</tr>';
		}

		// Maybe Show filters
		if( $this->show_filters ) {
			$table_headers_html .= '<tr id="ovcdt-filters">';

			// DEV:GENERALIZE!
			// Maybe add extra header if there is a pre-data col header or custom selection is enabled
			//$table_headers_html .= ( $this->pre_data_col ? '<th>Filters:</th>' : '' ); //dev:deprecated
			$table_headers_html .= '<th>Filters:</th>';

			$table_headers_html .= ( method_exists( $this, 'get_pre_data_column_filters_html') ? $this->get_pre_data_column_filters_html() : '' ); //dev:improve

			if( $this->custom_selection ) {

				$table_headers_html .= '<td class="freeze-pane" data-col="cs-toggle" data-filter-field="cs-view">';

				$cs_filter_checked = $this->filter_cs_ids ? 'checked="checked"' : '';

				$table_headers_html .= '<input id="cs-view-filter" type="checkbox" class="otdi otdi--bool ovcdt-cs-toggle" name="cs-view-filter" value="cs-view-filter" ' . $cs_filter_checked . '/>';

				$table_headers_html .= '</td>';
			}

			foreach( $this->qry['fields'] as $field => $meta ) {
				if( !isset( $meta['ovcdt_hide'] ) || !$meta['ovcdt_hide'] ) { // Don't show ovcdt_hidden cells

					// Initialize TD elem
					$td_elem = new OVC_HTML_Tag_Builder( 'td' );

					$td_elem->attr( 'data-col', $field );

					$td_elem->attr( 'data-filter-field', $field );

					// Maybe add special filter (td) classes
					if( isset( $this->qry['fields'][ $field ]['ovcdt_freeze_pane'] )
						&& $this->qry['fields'][ $field ]['ovcdt_freeze_pane']
					) {
						$td_elem->add_class( 'freeze-pane' );
					}

					// Initialize Filter Elem
					$filter_elem_args = array(
						'type'		=> 'input',
						'attrs'		=> array(
							'type'				=> 'text',
							'value'				=> isset( $this->filters[ $field ] ) ? $this->filters[ $field ] : '',
							'class'				=> array( 'otdi', 'otdi--filter' ),
							'data-filter-field'	=> $field,
							'placeholder' 		=> '&#xf002'
						)
					);

					$filter_elem = new OVC_HTML_Tag_Builder( $filter_elem_args );

					// Special handling for booleans
					if( isset( $meta['var_type'] ) && 'bool' == $meta['var_type'] ) {
						$filter_elem->attr( 'type', 'checkbox' );
						$filter_elem->add_class( 'otdi--bool' );

						if( 1 == $filter_elem->attr( 'value' ) ) {
							$filter_elem->attr( 'checked', 'checked' );
						}
					}

					// Compile TD elem and add to $table_headers_html
					$td_elem->contents( $filter_elem->get_html() );
					
					$table_headers_html .= $td_elem->get_html();

					//$filter_value = isset( $this->filters[ $field ] ) ? $this->filters[ $field ] : '';
					//$table_headers_html .= "<td data-filter-field=\"{$field}\"{$td_class}><div class=\"input-wrap\"><input type=\"text\" value=\"{$filter_value}\" class=\"ovc-filter\" data-filter-field=\"{$field}\" placeholder=\"&#xf002\"/></div><span>&nbsp;</span></td>"; // dev:pre_optimize
					//$table_headers_html .= "<td data-filter-field=\"{$field}\"{$td_class}><input type=\"text\" value=\"{$filter_value}\" class=\"otdi otdi--filter\" data-filter-field=\"{$field}\" placeholder=\"&#xf002\"/></td>";
				}
			}

			$table_headers_html .= '</tr>';
		}

		// We need to get the table data now in order to do dynamic col widths
		$this->get_table_data();

		// Build regular table headers row
		$table_headers_html .= '<tr class="ovcdt-headers">';

		// Maybe add pre data col html
		//$table_headers_html .= ( method_exists( $this, 'get_pre_data_column_header_html') ? $this->get_pre_data_column_header_html() : '' ); //dev:deprecated
		$table_headers_html .= '<th class="actions" data-col="actions">Actions</th>';

		if( $this->custom_selection ) {
			$table_headers_html .= '<th class="freeze-pane" data-col="cs-toggle"></th>';
		}

		// Maybe add custom cols
		$table_headers_html .= ( method_exists( $this, 'get_pre_data_column_row_html') ? $this->get_pre_data_column_row_html( array(), true ) : '' ); //dev:improve

		// Determine if ORDER classes needed
		$order_col = false;
		$order_class = '';
		if( isset( $this->sorting ) && $this->sorting ) { // DEV TODO: MAKE THIS MORE ROBUST
			$order_pieces 	= explode( " ", trim( $this->sorting ) );
			$order_col 		= $order_pieces[0];
			$order_dir 		= $order_pieces[1];

			//$order_class 	= "sort sort-" . strtolower( $order_dir ); // dev:deprecated
			$order_sort_class 	= "sort-" . strtolower( $order_dir );
		}

		// Build Table Headers HTML (use nice_names where possible)
		foreach ( $this->qry['fields'] as $field => $meta ) {

			if( !isset( $meta['ovcdt_hide'] ) || !$meta['ovcdt_hide'] ) { // Don't show ovcdt_hidden fields

				// determine $var_type // dev:use fields!
				$var_type = isset( $meta['var_type'] ) ? $meta['var_type'] : 'string';

				// Use nice name for header if there is one
				$nice_name = isset( $meta['nice_name'] ) ? $meta['nice_name'] : $field; 

				// Maybe override visible nice_name
				$displayed_header = isset( $this->header_overrides[ $field ] ) ? $this->header_overrides[ $field ] : ( isset( $meta['ovcdt_header'] ) ? $meta['ovcdt_header'] : $nice_name );

				// Initialize OVCDT Table Header Elem Args
				$th_elem_args = array(
					'type'		=> 'th',
					'attrs'		=> array(
						'data-col'	=> $field,
						'title' 	=> "Click to sort by {$nice_name}. Field: {$field}"
					),
					'contents'	=> $displayed_header
				);

				/**
				 * Build Classes
				 */

				$th_classes = array( 'ovcdt-th', 'ovcdt-th--' . $var_type ); // default

				// freezable columns
				if( isset( $meta['ovcdt_freeze_pane'] ) 
					&& $meta['ovcdt_freeze_pane']
				) {
					$th_classes[] = 'freeze-pane';
				}

				// Sorting Classes
				if( $order_col == $field ) {
					$th_classes[] = 'sort';
					$th_classes[] = $order_sort_class; // from above
				}

				// Field type / column width classes
				if( isset( $meta['ovcdt_header_class'] ) ) {
					$th_classes[] = $meta['ovcdt_header_class'];
				}
				else if( 'int' == $var_type ) {
					$th_classes[] = 'ovcdt-th--integer';
				}
				else if( array_key_exists( $var_type, $this->numeric_var_types ) ) {
					$th_classes[] = 'ovcdt-th--numeric';
				}
				else if( 'string' == $var_type && isset( $meta['_max_strlen'] ) ) {

					$min_width = min( 2000, 10 + ( 9 * intval( $meta['_max_strlen'] ) ) );
					$th_elem_args['attrs']['style'] = "min-width:{$min_width}px";
				}

				// Finish args and generate htmls
				$th_elem_args['attrs']['class'] = $th_classes;


				$th_elem = new OVC_HTML_Tag_Builder( $th_elem_args );



				$table_headers_html .= $th_elem->get_html();

	
			}
		}

		$table_headers_html .= '</tr><thead>';

		return $table_headers_html;
	}


	public function advanced_controls_meta_row_html() {
		if( $this->advanced_controls ) {
			$acm_html = '<div id="ovcdt-advanced-controls" class="ovcdt-meta-row oxmn-menubar">';

			if( $this->wait_to_save ) {
				$acm_html .= $this->load_template( 'wait_to_save' );
			}
			
			// Maybe Include Field Groups select controls
			if( $this->dt_meta->meta( 'field_groups' ) && $this->field_groups ) {
				$acm_html .= $this->load_template( 'field_groups' );
			}
			/* DEV:DEPRECATED
			if( method_exists( $this, 'additional_custom_controls_html') ) {
				$acm_html .= $this->additional_custom_controls_html();
			}
			*/

			if( $this->custom_selection ) {
				$acm_html .= $this->load_template( 'custom_selection' );
			}

			if( $this->show_filters ) {
				$acm_html .= $this->load_template( 'filter_menu' );
			}

			if( method_exists( $this, 'custom_advanced_controls' ) ) {
				$acm_html .= $this->custom_advanced_controls();
			}

			$acm_html .= '</div>';

			return $acm_html;
		}
		else {
			return '';
		}
	}

	public function results_meta_row_html() {

		$rmr_html = '<div class="ovcdt-meta-row ovcdt-results-meta-row">';

		// Add basic results meta info
		$page_max_row = ($this->page_num * $this->per_page) > $this->total_rows ? $this->total_rows : ($this->page_num * $this->per_page);
		$rmr_html .= '<span class="ovcdt-results-meta">Showing results '.((($this->page_num - 1) * $this->per_page)+1).' - '.$page_max_row.' of '.$this->total_rows.' (Page ' . $this->page_num . ' of ' . $this->total_pages . ')</span>';

		if ( $this->pagination ) {
			//Build Results Page Info & Navigation 
			$pagination = '<span class="ovcdt-pagination">';

			if( $this->page_num > 1 ) {
				$pagination .= '<a href="" class="ovcdt-pagination-link" data-goto="first">First</a><a href="" class="ovcdt-pagination-link" data-goto="prev">Prev</a>';
			} 

			$pagination .= 'Go to page: <input type="number" class="ovcdt-page-select" min="1" max="' . $this->total_pages . '" step="1" name="ovcdt_current_page_num" value="'.$this->page_num.'" />';

			if( $this->page_num < $this->total_pages ) {
				$pagination .= '<a href="" class="ovcdt-pagination-link" data-goto="next">Next</a><a href="" class="ovcdt-pagination-link" data-goto="last">Last</a>';
			}

			$pagination .= '</span>';

			// Build Results Per page selector
			$per_page_select = '<span class="ovcdt-per-page-select-wrap">Results per page: <select class="ovcdt-per-page-select">';

			$per_page_options = array( '25', '50', '100', '250' );
			foreach ( $per_page_options as $option ) {
				$per_page_select .= "<option value=\"{$option}\" ";
				$per_page_select .= $this->per_page == $option ? "selected" : "";
				$per_page_select .= ">{$option}</option>";
			}

			$per_page_select .= "</select>".'<input type="submit" class="ovcdt-per-page-refresh button" value="Refresh Data" /></span>';

			$rmr_html .= $pagination . $per_page_select;
		}

		$rmr_html .= '</div>';

		return $rmr_html;
	}

	protected function load_template( $ovcdt_template ) {
		$filename = OVC_PATH . "/templates/ovcdt/ovcdt-{$ovcdt_template}.php";

		if ( is_file( $filename ) ) {
			ob_start();
			include $filename;
			return ob_get_clean();
		}
		else {
			return $this->results->error( "Unable to load OVCDT template: {$ovcdt_template}" );
		}
	}


	// METHODS FOR INITIALIZING / BUILDING QUERY PIECES

	protected function initialize_qry_values() {

		//ovc_dev_log( 'before init qry_vals: ' . ox_exec_time() );
		// INITIALIZE RELEVANT META DATA
		global $wpdb, $current_user;

		// Get some preliminary essential values
		//$this->qry['table'] 	= $this->field_set->table_name;
		//$this->qry['sql_alias'] = $this->field_set->alias;
		//$ovcdt_meta 			= $this->dt_meta->meta();

		// Determine User Access level // dev: this shouldn't be here eventually!
		if( !isset( $this->edit_access )
			&& ( 
				( 
					!is_null( $this->dt_meta->meta( 'edit_access' ) )
			 		&& array_intersect( array_merge( array( 'administrator' ), (array) $this->dt_meta->edit_access ), $current_user->roles ) 
			 	)
			 	|| in_array( 'administrator', $current_user->roles )
			 )
		) {
			$this->edit_access = true;
		}
		else {
			$this->edit_access = false;
		}

		// Determine fields to be used in the query
		$db_fields = array();

		// If OVCDT meta has a 'fields' value, use that for the fields
		if( $this->dt_meta->fields ) {
			$db_fields = $this->dt_meta->fields;	
		}
		// If OVCDT meta has field_groups
		else if( $field_groups = $this->dt_meta->field_groups ) {
			
			// If field groups are being used, make sure advanced controls are turned on
			$this->advanced_controls = true;

			// Determine which field groups to add to query
			if( false === $this->field_groups ) {
				$this->field_groups = oxd( $this->dt_meta->default_field_groups, array_keys( $field_groups ) );
			}

			foreach( $field_groups as $fg => $fg_nice_name ) {
				// Skip field group if user does not have view_access permissions
				if( $this->dt_meta->meta( '_fg.'.$fg ) 
					 && array_key_exists( 'view_access', $this->dt_meta->meta( '_fg.'.$fg ) )
					 && !array_intersect( array_merge( array( 'administrator' ), array_values( $this->$dt_meta->meta( '_fg.'.$fg ) )['view_access'], $current_user->roles ) )
				) {
					continue;
				}

				// Add field group
				if( ( $this->dt_meta->required_field_groups && in_array( $fg, $this->dt_meta->required_field_groups ) ) || in_array( $fg, $this->field_groups ) ) {
					$db_fields = array_merge( $db_fields, $this->dt_meta->meta( 'fg.'.$fg ) );
				}
			}
		}
		// Just grab all fields from the main field set
		else {
			$db_fields = $this->field_set->get_fields();
		}

		// Parse fields and get their meta (also prepare data needed for joins if necessary)
		$joins = array();

		foreach( $db_fields as &$db_field ) {
			OVCSC::init_field( $db_field );

			// Store field meta
			$this->qry['fields'][ $db_field->global_name ] = oxd( $db_field->meta(), array() );

			if( !$db_field->current_user_can_edit() ) {
				$this->qry['fields'][ $db_field->global_name ]['readonly'] = 'true';
			}

			// Make sure we always have locked_if field dependencies included in the field list
			if( $locked_conditions = $db_field->meta( 'locked_if' ) ) {

				$locked_conditional = new OVC_Condition_Set( $locked_conditions, null );

				$locked_if_fields = $locked_conditional->fields;


				//$locked_ifs = $db_field->meta( 'locked_if' );

				foreach( $locked_if_fields as $locked_if_field ) {

					// If a field we need for locked_ifs is not already in our field list, add it and hide it
					if( !in_array( $locked_if_field, $db_fields ) 
						&& !array_key_exists( $locked_if_field, $this->qry['fields'] ) 
					) {

						if( OVCSC::init_field( $locked_if_field ) ) {

							$this->qry['fields'][ $locked_if_field->global_name ] = oxd( $locked_if_field->meta(), array() );
							$this->qry['fields'][ $locked_if_field->global_name ]['ovcdt_hide'] = true;	
						}
					}
				}
			}

			/**
			 * Check if field should be readonly. Field should be readonly if:
			 * 	- Field is a primary key
			 *  - force_readonly_fields is set at the table level of ovcdt_meta 
			 *  	^- This can be overriden with override_readonly meta on the field.
			 * 	- Current User does not have edit_access on this field
			 **/
			if( $db_field->global_name == $this->pKey->global_name 
				|| ( $this->dt_meta->force_readonly_fields 
					&& !$db_field->meta( 'override_readonly' ) 
					)
			) {
				$this->qry['fields'][ $db_field->global_name ]['readonly'] = 'true';
			}
			
			// Detect if this is a foreign field
			if( $this->sql_alias != $db_field->fs_alias ) {

				$this->qry['fields'][ $db_field->global_name ]['readonly'] = 'true';
				
				$join = $this->field_set->get_join( $db_field->field_set );

				if( !$join ) {
					return $this->results->error( "Unable to create join for field: {$db_field->global_name}" );
				}

				$join_id = $join->get_join_id();

				if( !array_key_exists( $join_id, $joins ) ) {

					$joins[ $join_id ] = $join;


				}
			}
		}

		// Add _always fields that are not already in $db_fields with special meta ( fields that must always be queried even if not shown )
		if( $this->dt_meta->meta( 'fg._always' ) ) {
			foreach( $this->dt_meta->meta( 'fg._always' )  as $always_field ) {
				if( !isset( $this->qry['fields'][ $always_field ] ) ) {
					$this->qry['fields'][ $always_field ] = array( 'ovcdt_hide' => true );
				}
			}
		}

		// Force hide _hidden fields
		if( $this->dt_meta->meta( 'fg._hidden' ) ) {
			foreach( $this->dt_meta->meta( 'fg._hidden' ) as $hidden_field ) {
				if( isset( $this->qry['fields'][ $hidden_field ] ) ) {
					$this->qry['fields'][ $hidden_field ]['ovcdt_hide'] = true ;
				}
			}
		}

		// Remove fields that cannot be viewed by this user (based on role)
		foreach( $this->qry['fields'] as $field => $meta ) {
			if( isset( $meta['view_access'] ) ) {
				$edit_access_roles = isset( $meta['edit_access'] ) ? $meta['edit_access'] : array();
				$view_access_roles = array_merge( array( 'administrator' ), $meta['view_access'], $edit_access_roles );

				if( !array_intersect( $view_access_roles, $current_user->roles ) ) {
					unset( $this->qry['fields'][ $field ] );
				}
			}
		}

		// BUILD QUERY PORTIONS
		// Build Query Portion: SELECT
		$sql_fields = array();
		foreach( $this->qry['fields'] as $field => $meta ) {
			$sql_fields[] = "{$field} AS '{$field}'";
		}
		$this->qry['select'] = "SELECT " . implode( ', ', $sql_fields ) . " ";

		// Build Query Portion: FROM (and maybe JOIN)
		$this->qry['from'] = "FROM {$this->db_table} {$this->sql_alias} ";
		
		// HANDLE JOINS
		$join_aliases = array(); // Track used aliases to avoid duplicates
		
		foreach( $joins as $join_id => $join ) {

			$foreign_alias = $join->get_foreign_alias( $this->field_set );
			$foreign_table = $join->get_foreign_table( $this->field_set );
			$foreign_field = $join->get_foreign_field( $this->field_set );

			$local_field   = $join->get_local_field( $this->field_set );

			if( in_array( $foreign_alias, $join_aliases ) ) {
				$foreign_alias .= count( $join_aliases );
			}

			$this->qry['from'] .= " LEFT JOIN {$foreign_table} {$foreign_alias}";
			$this->qry['from'] .= " ON {$foreign_alias}.{$foreign_field->local_name} = {$this->sql_alias}.{$local_field->local_name}";
		}

		/*
		foreach( $joins as $join_table_name => $join_info ) {
			$join_alias = $join_info['sql_alias'];
			$join_ons = $join_info['ons'];

			$this->qry['from'] .= " LEFT JOIN {$join_table_name} {$join_alias}";

			$first_condition = true;
			foreach( $join_ons as $join_on_field1 => $join_on_field2 ) {
				$this->qry['from'] .= $first_condition ? " ON " : " AND ";
				$first_condition = false;

				$this->qry['from'] .= "{$join_on_field1} = {$join_on_field2}";
			}
		}
		*/

		// Build Query Portion: ORDER BY
		if ( !isset( $this->sorting ) && $this->dt_meta->default_sorting ) {
			$this->sorting = $this->dt_meta->default_sorting;
		}

		if( isset( $this->sorting ) && $this->sorting ) {
			$this->qry['order'] = " ORDER BY {$this->sorting} ";
		}

		// Build Query Portion: WHERE
		if( is_array( $this->filters ) && count( $this->filters ) ) {
			// special handling for Custom Selection
			if( isset( $this->filters['cs-view'] ) ) {
				//unset( $this->filters['cs-view'] );

				if( $this->custom_selection ) {
					$this->filter_cs_ids = true;
					$this->cs_mode_enabled = true;

					if( strlen( $this->qry['where'] ) ) {
						$this->qry['where'] .= "AND ";
					}

					// Avoid MySQL error if Custom Selection is empty
					array_unshift( $this->user_cs_ids, 0 );

					$this->qry['where'] .= $this->pKey->global_name . " IN(" . implode( ',', $this->user_cs_ids ) . ") ";

					// Remove the zero
					array_shift( $this->user_cs_ids );
				}	
			}

			foreach( $this->filters as $field => $filter_value ) {
				if( in_array( $field, array_keys( $this->qry['fields'] ) ) ) {
					if( strlen( $this->qry['where'] ) ) {
						$this->qry['where'] .= "AND ";
					}

					// Parse filter input to build query WHERE // dev:generalize!!
					$pieces = array( $filter_value );
					$multi_filter = '';
					if( preg_match( '/([\|\&])/', $filter_value ) ) {
						if( false !== strpos( $filter_value, '|' ) ) {
							$multi_filter = ' OR ';
							$pieces = explode( '|', $filter_value );
						}
						else if( false !== strpos( $filter_value, '&' ) ) {
							$multi_filter = ' AND ';
							$pieces = explode( '&', $filter_value );
						}
					}

					// Change each filter piece into a SQL clause
					foreach( $pieces as $index => $piece ) {
						$negated = false;
						if( 0 === strpos( $piece, '!' ) ) {
							$piece = ltrim( $piece, '!' );
							$negated = true;		
						}

						if( 0 === strpos( $piece, '=' ) ) {
							$piece = ltrim( $piece, "=" );
							$negated = $negated ? '!' : '';
							$pieces[ $index ] = "{$field} {$negated}= '" . sanitize_text_field( $piece ) . "'";
						}
						else {
							$negated = $negated ? 'NOT ' : '';
							$pieces[ $index ] = "{$field} {$negated}LIKE '%{$wpdb->esc_like($piece)}%'";
						}	
					}

					$this->qry['where'] .= "( " . implode( $multi_filter, $pieces ) . " ) ";
					//$this->qry['where'] .= "{$field} LIKE '%{$wpdb->esc_like($filter_value)}%' ";
				}
			}

			if( strlen( $this->qry['where'] ) ) {
				$this->qry['where'] = " WHERE ".$this->qry['where'];
			}
		}

		// Build full SQL Query
		$this->qry['sql'] = "{$this->qry['select']}{$this->qry['from']}{$this->qry['where']}{$this->qry['order']}LIMIT {$this->offset},{$this->per_page}";

		// Maybe do qry scopes
		if( !empty( $this->scopes ) && is_array( $this->scopes ) ) {
			foreach( $this->scopes as $qry_scope ) {
				$qry_scope_method = 'qry_scope__'.$qry_scope;
				if( method_exists( $this, $qry_scope_method ) ) {
					$this->$qry_scope_method();
				}
			}
		}

		// GET QUERY RESULTS META INFORMATION
		// Determine total (unpaginated) results count
		$this->total_rows = $wpdb->get_var( "SELECT COUNT(*) FROM ({$this->qry['select']}{$this->qry['from']}{$this->qry['where']}) AS t" );

		// Maybe update OVCDT session
		if( false !== $this->session ) {
			$this->session->field_set = $this->field_set->name;
			$this->session->qry_head = "SELECT {$this->pKey->global_name} {$this->qry['from']}";
			$this->session->qry_where = "{$this->qry['where']}";
			$this->session->qry_tail = "{$this->qry['order']}LIMIT {$this->offset},{$this->per_page}";
			$this->session->qry_count = $this->total_rows;
		}

		// Determine total pages
		$this->total_pages = ceil( $this->total_rows / $this->per_page );
	}
}