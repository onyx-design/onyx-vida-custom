<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

final class OVC_Data_Manager {
	use OVC_Singleton_Class_Abstract;

	public $db_data = array();

	public function get_row( $field_set, $row_data = array(), $results = false, $auto_new = true ) {
		
		$row = false;

		// Validate $field_set
		$field_set = OVCSC::get_field_set( $field_set, 'database' );

		if( !$field_set ) {

			OVC_Results::maybe_init( $results );
			return $results->error( 'Unable to get_row, invalid $field_set supplied' );
		}
		// Validate $row_id
		else if( empty( $row_data ) && !$auto_new ) {
			
			OVC_Results::maybe_init( $results );
			return $results->error( 'Unable to get_row, invalid $row_id supplied' );
		}
		else {


  			// Determine row ID / row data
  			if( $this->validate_value_by_var_type( $row_data, 'int', $results ) ) { // Check if $row_data is the $row_id

  				$row_id = $row_data;
  			}
  			// If $row_data is an array, see if the primary_key is present
  			else if( is_array( $row_data ) ) {

  				$row_id = $row_data[ $field_set->pKey ] 
  					?? $row_data[ $field_set->get_primary_key()->global_name ] 
  					?? null;
  			}

  			// If we were able to get a $row_id, get or load the row
  			if( !empty( $row_id ) ) {

  				if( $this->is_row_loaded( $field_set, $row_id) ) {

  					$row = $this->db_data[ $field_set->name ][ $row_id ];
  				}
  				else {

  					$row = $this->load_row( $field_set, $row_id );
  				}
  			}

  			// If no row, maybe init a new one
  			if( !$row && $auto_new ) {

  				$row = $this->new_row( $field_set, $row_data );
  			}
		}

		return $row;
	}

	public function is_row_loaded( OVCFS $field_set, $row_id ) {

		return ( !empty( $row_id )
			&& isset( $this->db_data[ $field_set->name ] )
			&& isset( $this->db_data[ $field_set->name ][ $row_id ] )
		);
	}

	protected function load_row( OVCFS $field_set, $row_id, $force_refresh = false ) {

		$row = false;

		// Make sure $db_data is initialized for $field_Set
		$this->get_fs_rows( $field_set );

		// Initialize data
		$row_id = intval( $row_id );
		$row_data = $this->get_table_row_by_id( $row_id, $field_set->pKey, $field_set->table_name() );
		return $this->merge_local_row_data( $field_set, $row_data );


		// if( $row_data ) {

		// 	if( $this->is_row_loaded( $field_set, $row_id ) ) {
		// 		$results = new OVC_Results();
		// 		$row = $this->get_row( $field_set, $row_id, $results, false );
		// 		$row->update_local_data( $row_data );
		// 	}
		// 	else {

		// 		$row_class = $field_set->get_row_class();
		// 		$row = ( $this->db_data[ $field_set->name ][ $row_id ] = new $row_class( $field_set, $row_id, $row_data ) );
		// 	}
		// }

		// return $row;
	}

	public function load_rows_where( OVCFS $field_set, $where = array(), $results = false ) {
		

		// Make sure $db_data is initialized for $field_Set
		$this->get_fs_rows( $field_set );

		$loaded_rows = array();

		$table_data = $this->get_table_data( $field_set, $where, $results );

		foreach( $table_data as $table_row ) {

			$ovc_row = $this->merge_local_row_data( $field_set, $table_row );

			if( $ovc_row ) {

				$loaded_rows[ $ovc_row->ID ] = $ovc_row;
			}
			

			// $row_id = $table_row[ $field_set->get_primary_key()->local_name ];

			// if( $this->is_row_loaded( $field_set, $row_id ) ) {

			// 	$ovc_row = $this->get_row( $field_set, $row_id );
			// }
			// else {

			// 	$row_class = $field_set->get_row_class();
			// 	$ovc_row = ( $this->db_data[ $field_set->name ][ $row_id ] = new $row_class( $field_set, $row_id, $table_row ) );
			// }

			// $loaded_rows[ $row_id ] = $ovc_row;
		}

		return $loaded_rows;
	}

	protected function merge_local_row_data( OVCFS $field_set, $row_data ) {

		$row_id = $row_data[ $field_set->get_primary_key()->local_name ];
		$ovc_row = false;
		$results = false; //dev:improve //dev:results

		if( $row_id ) {

			if( $this->is_row_loaded( $field_set, $row_id ) ) {

				$ovc_row = $this->get_row( $field_set, $row_id, $results, false );
				$ovc_row->update_local_data( $row_data );
			}
			else {

				$row_class = $field_set->get_row_class();
				$ovc_row = ( $this->db_data[ $field_set->name ][ $row_id ] = new $row_class( $field_set, $row_id, $row_data ) );
			}
		}

		return $ovc_row;
	}

	public function get_row_by_valid_id( $field_set = false, $field, $value, $results = false, $auto_new = true ) {
		OVC_Results::maybe_init( $results );

		$field_set = OVCSC::get_field_set( $field_set, 'database' );

		if( $field_set && $field = OVCSC::get_field( $field, $field_set ) ) {

			if( $field->meta( 'valid_id' ) ) {

				$row_id = $this->get_id_by_valid_id( $value, $field, $field_set );

				return $this->get_row( $field_set, $row_id, $results, $auto_new );

				// $classname = $field_set->get_row_class();
				// return new $classname( $field_set, $row_id, $results );
			}
			else {
				return $results->error( "Cannot get row from {$field_set->name} by field {$field->global_name} because field is not a valid id" );
			}
			
		}
		else {
			return $results->error( "Cannot initialize row. Invalid field set or field sent." );
		}
	}

	public function get_joined_rows( OVC_Row $row, OVCFS $joined_fs, $results = false, $force_refresh = false ) {

		$joined_rows = array();

		$source_fs = $row->field_set;

		$join = $source_fs->get_join( $joined_fs );

		if( $join ) {

			$ref_relation = $join->get_ref_relation( $joined_fs );

			$foreign_field = $join->get_foreign_field( $source_fs );

			$foreign_key_value = $row->data( $join->get_local_field( $source_fs ) );

			if( 'parent' == $ref_relation 
				|| ( 'child' == $ref_relation && '11' == $join->cardinality )
			) {
				
				// If this row doesn't have the primary key value of the joined row, we need to get it
				if( $foreign_field->local_name != $joined_fs->p_key ) {

					$foreign_key_value = OVCDB()->get_id_by_valid_id( $foreign_key_value, $foreign_field, $joined_fs );
				}

				$joined_rows = OVCDB()->get_row( $joined_fs, $foreign_key_value, $results );
			}
			else if ( $foreign_key_value && 'child' == $ref_relation ) {

				// Build where clause data array for OVCDB
				$where = array(
					$foreign_field->local_name => $foreign_key_value
				);

				$joined_rows = $this->load_rows_where( $joined_fs, $where, $results );
			}
		}

		return $joined_rows;
	}

	public function get_fs_rows( $field_set ) {

		if( $field_set = FS( $field_set, 'database' ) ) {

			// Maybe initialize $db_data index for $field_Set
			if( !isset( $this->db_data[ $field_set->name ] ) ) {
				$this->db_data[ $field_set->name ] = array();
			}

			return $this->db_data[ $field_set->name ];
		}
	}

	public function get_loaded_rows_where( OVCFS $field_set, $where = array(), $limit = null ) {

		return $this->filter_row_objects( $this->get_fs_rows( $field_set ), $where, $limit );
	}

	public function filter_row_objects( $rows = array(), $where = array(), $limit = null ) {

		$filtered_rows = array();

		foreach( $rows as $row ) {

			if( $this->row_matches_where( $row, $where ) ) {

				$filtered_rows[ $row->ID ] = $row;
			}

			if( isset( $limit ) && count( $filtered_rows ) >= $limit ) {

				break;
			}
		}

		return $filtered_rows;
	}

	public function new_row( OVCFS $field_set, $row_data = array(), $strip_id = true ) {

		$fs_rows = $this->get_fs_rows( $field_set );

		$new_index = -1;

		if( !empty( $fs_rows ) ) {

			$new_index = min( 0, min( array_keys( $fs_rows ) ) ) - 1;
		}

		// Process 
		if( $row_data && $strip_id ) {

			if( is_array( $row_data ) ) {
				unset( $row_data[ $field_set->get_primary_key()->local_name ] );
				unset( $row_data[ $field_set->get_primary_key()->global_name ] );
			}
			else {
				$row_data = array();
			}			
		}

		$row_class = $field_set->get_row_class();

		return $this->db_data[ $field_set->name ][ $new_index ] = new $row_class( $field_set, $new_index, (array) $row_data );
	}

	public function row_matches_where( OVC_Row $row, $where = array() ) {

		foreach( $where as $field_name => $field_value ) {

			if( $row->data( $field_name ) != $field_value ) {

				return false;
			}
		}

		return true;
	}

	public function flush_db_data( $field_set = null ) {

		if( !empty( $field_set ) && $field_set = FS( $field_set, 'database' ) ) {
			
			unset( $this->db_data[ $field_set->name ] );

		}
		else {
			unset( $this->db_data );
			$this->db_data = array();
		} 
	}

 	/**
 	 * Method Groups 
	 *
	 * 1.	OVC Meta Data Retrieval - Full Lists
	 * 2.	OVC Meta Data Retrieval and Modification - Custom and Partial Lists / Field Lookup
	 * 3. 	OVC Data Validation and Sanitization
	 * 4.	OVC Data Retrieval
	 * 5.	OVC Data Manipulation - Inserts, Updates, and Deletes
 	 */

	/*/	2.	OVC Meta Data Retrieval - Custom and Partial Lists / Field Lookup /*/

	// Check if a value is unique, optionally ignoring primary key ID
	public function is_value_unique( $field = '', $value = '', $id_to_ignore = false, $field_set = null ) {

		if( $field && '' === $value ) {
			return true;
		}

		if( OVCSC::init_field( $field, $field_set ) && $value ) {

			$pKey 		= $field->field_set->p_key;

			// BUILD CORRESPONDING WHERE / VALUE ARRAYS: 1) Associative array of $field => $wpdb_format pairs 2) numeric array of $values for $wpdb->prepare
			// First add the field and value to be checked for uniqueness
			$where = array(
				$field->local_name => array( 'operator' => '=', 'format' => $field->wpdb_format() )
			);
			$values = array( $value );

			// Optionally add a primary key ID to ignore
			if( !empty( $id_to_ignore ) && is_numeric( $id_to_ignore ) ) {
				$where[ $pKey ] = array( 'operator' => '!=', 'format' => '%d' );
				$values[] = $id_to_ignore;
			}

			// BUILD SQL
			$sql = "SELECT COUNT(*) FROM {$field->table_name()}";

			$first_iteration = true;
			foreach( $where as $where_field => $where_data ) {
				$sql .= ( $first_iteration ? ' WHERE ' : ' AND ' );
				$sql .= "{$where_field} {$where_data['operator']} {$where_data['format']}";
				$first_iteration = false;
			}

			// EXECUTE SQL
			global $wpdb;
			$count = $wpdb->get_var( $wpdb->prepare( $sql, $values) );

			return ( null === $count ? null : !boolval( $count ) );
		}
	}

	// Count number of OVC Products, optionally filtered with associative array of column => value pairs
	public function count_results( $field_set, $where = array() ) {
		OVCSC::init_field_set( $field_set );
		global $wpdb;

		$sql = "SELECT COUNT(*) FROM {$field_set->table_name()}";
		
		if( count( $where ) ) {
			$first_iteration = true;
			foreach( $where as $where_field => $where_value ) {
				$field = OVCSC::get_field( $where_field, $field_set );

				$sql .= ( $first_iteration ? ' WHERE ' : ' AND ' );
				$sql .= "{$where_field} = " . $field->wpdb_format();
				$first_iteration = false;
			}
		}

		return $wpdb->get_var( $wpdb->prepare( $sql, array_values( $where ) ) );
	}

	// Determine the primary key of a table, this function manages this data in the WP_Cache for efficiency
	public function get_table_primary_key( $table = '', $force_refresh = false ) {
		if( !$table ) {return false;}
		$primary_key = false;

		if( !$force_refresh ) {
			$primary_key = wp_cache_get( $table, 'ovc_wpdb_primary_keys' );
		}
		
		if( $primary_key ) {
			return $primary_key;
		}
		else {
			global $wpdb;

			$table_exists = $wpdb->get_var( "SHOW TABLES LIKE '%{$wpdb->esc_like($table)}%'" ); // Check if table exists
			if( !$table_exists ) {return false;}

			$table_index = $wpdb->get_row( $wpdb->prepare( "SHOW INDEX FROM {$table} WHERE Key_name = %s", 'PRIMARY') , ARRAY_A );
			if( !$table_index ) {return false;}
			else {
				$primary_key = $table_index['Column_name'];
				wp_cache_set( $table, $primary_key, 'ovc_wpdb_primary_keys' );
			}
		}

		return $primary_key;
	}

	// OVC FIELDMETA RETRIEVAL / MODIFICATION

	public function make_fields_array_ovcdt_format( &$fields = false, $field_set = null, $format_array_keys = true ) { 
		// dev:field_set class
		if( !OVCSC::init_field_set( $field_set ) ) {return null;}
		
		$fixed = array();

		foreach ( $fields as $key => $value ) {
			$to_fix = ( $format_array_keys ? 'key' : 'value' );
			$$to_fix = ( 0 !== strpos( $$to_fix, "{$field_set->alias}." ) ? "{$field_set->alias}.{$$to_fix}" : $$to_fix );
			$fixed[ $key ] = $value;
		}
		$fields = $fixed;
		return $fields;
	}

	/*/	3. 	OVC Data Validation and Sanitization /*/

	// Returns Bool or String: True if input value is
	public function validate_value_by_var_type( $value, $var_type = 'string', $results = false, $field_name = '' ) {
		OVC_Results::maybe_init( $results );

		if( 'bool' == $var_type ) {
			if ( !in_array( $value, array( '0', 0, '1', 1, true, false ), true ) ) {
				return $results->error( "Invalid Boolean value. {$field_name}" );
			}
		}
		else if( 'int' == $var_type ) {
			// Determine row ID / row data
			if( !empty( $value ) && 
				!( is_int( $value ) 
					|| ( is_numeric( $value ) 
						&& intval( $value ) == $value 
						&& strlen( intval( $value ) ) == strlen( $value ) )
				)
			) {
				return $results->error( "Invalid {$var_type} value: " . strval( $value ) );
			}
		}
		else if ( in_array( $var_type, array( 'stock', 'price', 'float', 'volume', 'float6' ) ) ) {
			if( "" === $value || false === filter_var( $value, FILTER_VALIDATE_FLOAT ) ) {
				return $results->error( "Invalid {$var_type} (decimal) value: " . strval( $value ) );
			}
		}
		else if( 'date' == $var_type && DateTime::createFromFormat('Y-m-d', $value) === false ) {

			return $results->error( "Invalid DATE : {$value}. Must be in Y-m-d format." );
		}
		else if ( 'datetime' == $var_type ) { //dev:improve
			if( strtotime( $value ) === false && DateTime::createFromFormat('Y-m-d H:i:s', $value ) === false ) {
				return $results->error( "Invalid DATETIME {$value}. Must be in Y-m-d H:i:s format." );	
			}
			
			// $utime = strtotime( $value );
			// //$date = date( 'j/n/Y', $utime );
			// if( date( 'j/n/Y', $utime ) != $value && date("Y-m-d H:i:s", $utime ) != $value ) {
			// 	return $results->error( "Invalid Date. Must be in dd/mm/YYYY or Y-m-d H:i:s format." );
			// }
		}
		else if ( in_array( $var_type, array( 'string', 'asin' ) ) ) {
			if( trim($value) != sanitize_text_field( $value ) ) {
				return $results->error( "Sanitized text does not match input value.  RAW: {$value}  SANITIZED: ".sanitize_text_field( $value ) );
			}
		}
		else if ( 'upc' == $var_type && "" !== $value ) { // UPCs can be blank //dev:improve
			return $this->validate_upc( $value, $results );
		}
		else if ( 0 === strpos( $var_type, 'list:' ) ) {
			$raw_list_name = str_replace( 'list:', '', $var_type );

			$valid_values = OVC_Lists()->get_list( $raw_list_name );

			if( !$valid_values ) {
				return $results->error( "Error: could not find list of valid values for var type list:{$raw_list_name}" );
			}
			else if( !in_array( $value, $valid_values ) ) {
				if( empty( $value ) && 'true' != OVCSC::get_field_meta( 'ovc_list', $raw_list_name, 'allow_blank' ) ) {
					return $results->error( "Invalid blank value for var type list: {$raw_list_name}, which doesn't allow blank values" );
				}
				else if( !empty( $value ) ) {
					return $results->error( "Invalid value for var type list: {$raw_list_name}, {$value} is not in the list." );
				}
				// if( !( '' === $value && OVCSC::get_meta_value( 'ovc_list', $raw_list_name, 'allow_blank' ) ) ) {
					
				// }	
			}
		}

		return true;
	}

	public function sanitize_value_by_field( $value = '', OVC_Field $field ) {

		return $this->sanitize_value_by_var_type( $value, $field->var_type() );
	}
	
	public function sanitize_value_by_var_type( $value = '', $var_type = 'string' ) {
		if( false !== strpos( $var_type, ":" ) ) {

		}

		switch ( $var_type ) {
			case 'stock':
			case 'price':
				return floatval( number_format( (float) $value, 2, '.', '' ) );
			break;
			case 'float':
				return floatval( number_format( $value, 3, '.', '' ) );
			break;
			case 'volume':
				return floatval( number_format( $value, 4, '.', '' ) );
			break;
			case 'float6':
				return floatval( number_format( $value, 6, '.', '' ) );
			break;
			case 'upc':
				return trim( strval( $value ) ) ;
			break;
			case 'bool':
			case 'int':
				return intval( $value );
			break;
			case 'date':
				return date( 'Y-m-d', strtotime( $value ) );
			break;
			case 'datetime':
				return date("Y-m-d H:i:s",strtotime( $value ) );
			break;
			case 'string':
			default: 
				if( is_array( $value ) || is_object( $value ) ) {
					$value = serialize( $value );
				}

				return sanitize_text_field( $value );
			break;
		}
	}

	/**
	 * Validate that the UPC is a valid string and that it's a valid UPC
	 * 
	 * @param 	string 				$upc
	 * @param 	OVC_Results|bool 	$results
	 * 
	 * @return 	bool
	 */
	public function validate_upc( $upc = '', $results = false ) {
		OVC_Results::maybe_init( $results );

		if( !is_string( $upc ) ) {
			return $results->error( "Invalid UPC - UPC must be a string." );
		}
		else if( !preg_match('/^\d{12}$/', $upc) ) {
			return $results->error( "Invalid UPC: {$upc} - UPC must consist of exactly 12 digits [0-9]." );
		}

        /**
         * Even digits are added
         * Odd digits are multiplied by three then added
         * REMEMBER: $i starts at 0
         */
        $accumulator = 0;
        foreach( str_split( substr( $upc, 0, -1 ) ) as $i => $digit ) {
        	$accumulator += $i % 2 ? (int) $upc[$i] : intval( $upc[$i] ) * 3; // Even digits added, Odd digits multiplied by 3 and added ( $i starts
        }

        $checksum = ( 10 - ( $accumulator % 10 ) ) % 10;

        if ( $checksum !== intval( $upc[11] ) ) {
            return $results->error( "Invalid UPC: {$upc} - Checksum ( {$checksum} ) does not match Check Digit ( {$upc[11]} )" );
        }

        return true;
	}

	/**
	 * Sanitize an entire OVC row (because wpdb only returns strings)
	 * 
	 * @param array 	$row 		The data to sanitize
	 * @param mixed 	$field_set 	[optional] If a valid OVCFS or field set ID, the data can have $field->local_name keys. if false, it will be assumed that the keys are $field->global_names
	 **/ 
	public function sanitize_row( $row, OVCFS $field_set ) {
		
		if( is_array( $row ) && $row ) {

			foreach( $row as $field_name => $value ) {

				if( $field = OVCSC::get_field( $field_name, $field_set ) ) {
					$row[ $field_name ] = $this->sanitize_value_by_field( $value, $field );
				}
				else {
					return false;
				}
			}

			return $row;
		}

		return false;		
	}

	public function get_where_sql( $where_data = array(), $prepend_WHERE = true ) {

		$where_pieces = array();

		foreach( $where_data as $where_key => $where_vars ) {

			$where_field 	= $where_key;
			$where_op 		= '=';
			$where_value 	= $where_vars;

			if( is_array( $where_value ) ) {

				$where_field 	= $where_vars['var'];
				$where_op 		= $where_vars['op'];
				$where_value 	= $where_vars['value'];
			}

			$where_value = is_string( $where_value ) ? "'{$where_value}'" : $where_value;

			$where_pieces[] = "{$where_field} {$where_op} {$where_value}";
		}

		$where_sql = implode( " AND ", $where_pieces );

		if( $where_sql && $prepend_WHERE ) {
			$where_sql = " WHERE {$where_sql}";
		}

		return $where_sql;
	}

	/*/ 4. OVC Data Retrieval /*/

	// Get full data rows from a table, optionally filtered with a WHERE clause
	public function get_table_data( OVCFS $field_set, $where = array(), $results = false, $return_strings = false ) {
		OVC_Results::maybe_init( $results );

		if( 'database' == $field_set->type ) {
			
			/*
			$where_sql = '';

			foreach( $where as $field_name => $value ) {

				$where_sql .= " {$field_name} = ";
				$where_sql .= is_string( $value ) ? "'{$value}'" : $value;
			}

			$where_sql = $where_sql ? " WHERE{$where_sql}" : $where_sql;
			*/

			//$where_sql = $this->get_where_sql( $where );

			$sql = "SELECT * FROM {$field_set->table_name()} {$field_set->alias}" . $this->get_where_sql( $where );

			

			global $wpdb;

			$table_data = $wpdb->get_results( $sql, ARRAY_A );

			if( $table_data && !$return_strings ) {

				foreach( $table_data as $key => $row ) {
					$table_data[ $key ] = $this->sanitize_row( $row, $field_set );
					//$row = $this->sanitize_row( $row, $field_set );
				}
			}

			return $table_data;
		}
		else {
			return $results->error( "Cannot get table data from non-database field set: {$field_set->name}" );
		}
	}

	public function get_table_row_by_id( $id = false, $id_field = 'ID', $table = '', $return_strings = false ) {
		return $this->get_row_by_id( $id, $id_field, $table, $return_strings );
	}
	// Get a single OVC Product Data Row as an associative array using an ID. Valid id_field values (for wp_ovc_products only): ID, post_id, asin, upc


	public function get_row_by_id( $id = false, $id_field = 'ID', $table = '', $return_strings = false ) {
		if( !$id ) {return false;}
		global $wpdb;
		$id_format = ( is_string( $id ) ? '%s' : '%d' );
		$row = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$table} WHERE {$id_field} = {$id_format}", $id ), ARRAY_A );
		$field_set = OVCSC::get_field_set( $table );

		if( $field_set && $row && !$return_strings ) {
			$row = $this->sanitize_row( $row, $field_set );
		}

		return $row;
	}

	public function get_id_by_valid_id( $alt_id, $alt_id_field, $field_set ) { // dev:robuster // dev:ovcsc

		if( empty( $alt_id ) ) {
			return null;
		}
		
		$field_set = OVCSC::get_field_set( $field_set, 'database' );

		if( $field_set && $alt_id_field = $field_set->get_field_object( $alt_id_field ) ) {

			$alt_id_wpdb_format = $alt_id_field->wpdb_format();
			$pKey = $field_set->pKey;

			global $wpdb;
			return $wpdb->get_var( $wpdb->prepare( "SELECT {$pKey} FROM {$field_set->table_name()} WHERE {$alt_id_field} = {$alt_id_wpdb_format}", $alt_id ) );
		}
		
		return null;
	}

	// Get an OVC ID by SKU - SINGLE ID FROM SINGLE SKU (returns false if SKU does not exist)
	//dev:deprecated
	public function get_id_by_sku( $sku = '', $table = '', $strict = true ) {
		global $wpdb;
		$strict = $strict ? " AND sync_oms = 1" : "";
		return $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM {$table} WHERE sku = %s {$strict} LIMIT 0,1", $sku) );
	}

	// Get a SKU by OVC ID - SINGLE SKU FROM SINGLE ID (returns false if SKU does not exist)
	public function get_sku_by_id( $id = -1 ) {
		global $wpdb;
		return $wpdb->get_var( $wpdb->prepare( "SELECT sku FROM wp_ovc_products WHERE ID = %d", $id) );
	}

	// Get OVC IDs by SKUs - ARRAY OF IDs FROM ARRAY OF SKUs
	public function get_ids_by_skus( $skus = array() ) {
		// Chunk through SKUs to avoid long query errors
		$chunked_skus = array_chunk( $skus, 96 );

		// Run ID by SKU lookup queries by chunk
		global $wpdb;
		$ovc_ids = array();
		foreach( $chunked_skus as $chunk ) {
			$ovc_ids_chunk = $wpdb->get_col( "SELECT ID FROM wp_ovc_products WHERE sku IN(\"" . implode( '", "', $chunk ) . "\")" );
			if( false === $ovc_ids_chunk ) {
				return false;
			}
			else {
				$ovc_ids = array_merge( $ovc_ids, $ovc_ids_chunk );
			}
		}

		return $ovc_ids;
	}

	// Get OVC SKUs by IDs
	// dev:robuster!
	public function get_skus_by_ids( $ids = array() ) {
		global $wpdb;
		return $wpdb->get_col( "SELECT SKU FROM wp_ovc_products WHERE ID IN(" . implode( ',', $ids ) . ")" );
	}

	public function get_ovc_data_by_case_sku( $case_sku = '', $fields = '*', $include_case_product = true ) {

		if( !$case_sku || !$fields ) {
			return false;
		}

		global $wpdb;

		$query = "SELECT {$fields} FROM {$wpdb->prefix}ovc_products WHERE case_sku = %s";

		if( !$include_case_product ) {
			$query .= " AND size != 'CS'";
		}

		$case_children = $wpdb->get_results( 
			$wpdb->prepare( 
				$query, 
				$case_sku 
			), 
			ARRAY_A 
		); 

		return $case_children;
	}

	// Get an individual value by OVC row ID
	public function get_value_by_id( $ovc_id = 0, $ovc_field = 'sku', $table = '' ) {
		global $wpdb;
		return $wpdb->get_var( $wpdb->prepare( "SELECT {$ovc_field} FROM {$table} WHERE ID = %d", $ovc_id ) );
	}

	/*/ 5.	OVC Data Manipulation - Inserts, Updates, and Deletes, and relevant helper functions /*/

	public function validate_value_by_field( OVC_Field $field, $value = null, OVC_Results $results ) {
		
		OVC_Results::maybe_init( $results );

		$valid = true; // Innocent until proven guilty

		// This will update $valid and $results
		if( !$this->validate_value_by_var_type( $value, $field->var_type(), $results, $field->global_name ) ) {
			$valid = false;
			$results->field_errors['invalid'][] = $field->global_name; // dev:improve
		}

		// dev:centralize? dev:generalize?
		$innerfield_validation_keys = array( 'required', 'maxlength', 'pattern' );

		foreach( $innerfield_validation_keys as $meta_key ) {

			if( $meta_value = $field->meta( $meta_key ) ) {

				if( 'required' == $meta_key && !$value ) {

					$results->field_errors['required'][] = $field->global_name; // dev:improve //dev:data_fixes
					$valid = $results->error( "Required field is missing or blank: {$field->global_name}" );
				}
				else if( 'maxlength' == $meta_key 
					&& strlen( $value ) > intval( $meta_value ) 
				) {

					$valid = $results->error( "Value for field: {$field->global_name} exceeds character limit: {$meta_value}" );
				}
				else if( 'pattern' == $meta_key 
					&& $value
					&& 0 == preg_match( '/'.$meta_value.'/', $value ) 
				) {

					$valid = $results->error( "Value: {$value} for field: {$field->global_name} does not pass its regular expression pattern matching validation: ".$meta_value );
				}
			}
		}

		return $valid;
	}

	public function validate_row( OVC_Row $row, OVC_Results $results ) {

		//ovc_debug( memory_get_usage() . ' ' . __FUNCTION__ . ' - Field Set: ' . $row->field_set->name . ' Row ID: ' . $row->ID );

		$valid = true; // Innocent until proven guilty

		foreach( $row->field_set->get_fields() as $field ) {

			$value = $row->new_value( $field );

			// INTRA-FIELD VALIDATION
			if( !( 	// Ignore certain validation if row is new and field is not supplied //dev:improve
					!$row->exists() 
					&& empty( $value ) 
					&& !$row->is_field_changed( $field ) 
					&& !$field->meta('required')
				) 
				&& !$this->validate_value_by_field( $field, $value, $results ) ) {
				$valid = false;
			}

			// INTER-FIELD VALIDATION

			// If changed, does user have edit access? //dev:improve //dev:locked
			if( $row->is_field_changed( $field ) 
				&& !$field->current_user_can_edit()
			) {
				$valid = $results->error( "Current user does not have edit access to field: {$field->global_name}" );
			}

			// Foreign Key Check if field is required or $value is non-zero //dev:improve //dev:joins
			if( $foreign_key = $field->meta( 'foreign_key' ) ) {

				if( $value ) {

					$foreign_field = OVCSC::get_field( $foreign_key );

					if( !$foreign_field ) {
						$valid = $results->error( "Invalid foreign_key meta_value ( {$foreign_key} ) for field: {$field->global_name} " );
					}
					else if( !$this->get_id_by_valid_id( $value, $foreign_field, $foreign_field->field_set ) ) {
						$valid = $results->error( "Invalid foreign_key value: {$value} for field: {$field->global_name} - {$value} must exist in field {$foreign_field->global_name}" );
					}
					
				}
				else if( $field->meta( 'required' ) ) {
					$valid = $results->error( "Invalid foreign_key value for field: {$field->global_name} - Cannot be a zero value" );
				}
			}

			// Unique check //dev:improve //dev:unique_multi_field
			if( $field->meta( 'unique' ) 
				&& ( !empty( $value ) || $field->meta( 'required' ) )
				&& !$this->is_value_unique( $field, $value, $row->ID ) 
			) {

				$results->field_errors['not_unique'][] = $field->global_name; //dev:improve //dev:data_fixes
				$valid = $results->error( "Value: {$value} is not unique for field: {$field->global_name}" . ( $row->ID ? " (not including ID: {$row->ID})" : "" ) ); 
			}

			// Calc_formula check
			if( $calc_formula = $field->meta( 'calc_formula' ) ) {

				$expression = new OVC_Expression( $calc_formula, $row );
				$calc_value = $this->sanitize_value_by_field( $expression->evaluate(), $field );

				if( $value != $calc_value ) {
					$valid = $results->error( "Value for field {$field->global_name} does not match output of its calc_formula. Supplied value: {$value}, calculated value: {$calc_value}" );
				}
			}
		}

		// dev:improve //dev:optimize
		if( $after_validate = $row->field_set->fs_meta( 'after_validate' ) ) {

			// dev:improve //dev:generalize //dev:error_handling
			foreach( $after_validate as $ovc_logic_name ) {

				if( $valid ) {
					$valid = $this->check_validation_logic( $row, $ovc_logic_name, $results );
				}

				/*

				$ovc_logic = OVCSC::get_field( $ovc_logic_name, FS('ovc_logic') );

				if( !$ovc_logic ) {
					$valid = $results->error( "Error validating row! Custom validation condition {$ovc_logic_name} not found." );
					continue;
				}

				// Maybe check pre_conditions first
				if( $ovc_logic->meta( 'pre_conditions' ) ) {

					$pre_conditions = new OVC_Condition_Set( $ovc_logic->meta( 'pre_conditions' ), $row, $results, false );

					if( !$pre_conditions->result ) {
						
						continue;
					}
				}

				$condition_set = new OVC_Condition_Set( $ovc_logic->meta( 'conditions' ), $row, $results, false );

				// Do any relevant post condition processing
				$operations = array();
				
				if( $condition_set->result && $ovc_logic->meta( 'if_true' ) ) {
					
					$operations = $ovc_logic->meta( 'if_true' );
				}
				else if( !$condition_set->result && $ovc_logic->meta( 'if_false' ) ) {
					
					$operations = $ovc_logic->meta( 'if_false' );	
				}

				// dev:generalize // dev:improve
				foreach( $operations as $action => $opargs ) {

					if( 'set_value' == $action ) {
						$field_name = $opargs['var'];
						$set_value 	= $opargs['value'];

						$set_field = OVCSC::get_field( $opargs['var'] );

						$row->set_new_data( array( $set_field->global_name => $set_value ), $results );

						$results->notice( "Setting value of {$field_name} to {$set_value} as a result of {$ovc_logic->nice_name}" );
					}
					else if( 'results_msg' == $action ) {
						$notice_type = $opargs['level'];

						$msg_expression = new OVC_Expression( $opargs['msg'], $row, $results, false );

						$msg = $msg_expression->evaluate();

						$results->$notice_type( $msg );
					}
					else if( 'row_valid' == $action ) {

						$valid = $opargs['value'];
					}
					else if( 'row_method' == $action ) {

						$method_name = $opargs['method'];

						if( method_exists( $row, $method_name ) ) {
							$row->$method_name( $results );
						}
						else {
							$valid = $results->error( "Invalid method for row_method operation on OVC_Row: {$method_name}" );
						}
					}
				}

				// Only send a failed notice if the row validation actually failed.
				if( !$valid && !$condition_set->result ) {
					$results->notice( "Condition set failed: {$ovc_logic->local_name}" );	
				}
				*/
			}
		}

		return $valid;
	}

	public function check_validation_logic( OVC_Row $row, $ovc_logic_name, $results = false ) {
		OVC_Results::maybe_init( $results );

		$ovc_logic = OVCSC::get_field( $ovc_logic_name, FS('ovc_logic') );

		if( !$ovc_logic ) {
			return $results->error( "Error validating row! Custom validation condition {$ovc_logic_name} not found." );
		}

		// Maybe check pre_conditions first
		if( $ovc_logic->meta( 'pre_conditions' ) ) {

			$pre_conditions = new OVC_Condition_Set( $ovc_logic->meta( 'pre_conditions' ), $row, $results, false );

			if( !$pre_conditions->result ) {
				
				return true;
			}
		}

		$row_invalidated = false;

		$condition_set = new OVC_Condition_Set( $ovc_logic->meta( 'conditions' ), $row, $results, false );

		// Do any relevant post condition processing
		$operations = array();
		
		if( $condition_set->result && $ovc_logic->meta( 'if_true' ) ) {
			
			$operations = $ovc_logic->meta( 'if_true' );
		}
		else if( !$condition_set->result && $ovc_logic->meta( 'if_false' ) ) {
			
			$operations = $ovc_logic->meta( 'if_false' );	
		}

		// dev:generalize // dev:improve
		foreach( $operations as $action => $opargs ) {

			if( 'set_value' == $action ) {
				$field_name = $opargs['var'];
				$set_value 	= $opargs['value'];

				$set_field = OVCSC::get_field( $opargs['var'] );

				$row->set_new_data( array( $set_field->global_name => $set_value ), $results );

				// This causes an infinite loop when called from OVCDB()->save_row()!!
				// $row->save( $results );

				$results->notice( "Setting value of {$field_name} to {$set_value} as a result of {$ovc_logic->nice_name}" );
			}
			else if( 'results_msg' == $action ) {
				$notice_type = $opargs['level'];

				$msg_expression = new OVC_Expression( $opargs['msg'], $row, $results, false );

				$msg = $msg_expression->evaluate();

				$results->$notice_type( $msg );
			}
			else if( 'row_valid' == $action ) {

				$row_invalidated = !( (bool) $opargs['value'] );
			}
			else if( 'row_method' == $action ) {

				$method_name = $opargs['method'];

				if( method_exists( $row, $method_name ) ) {
					$row->$method_name( $results );
				}
				else {
					$row_invalidated = !$results->error( "Invalid method for row_method operation on OVC_Row: {$method_name}" );
				}
			}
			else if( 'activate_data_error' == $action ) {

				$error_code = $opargs['error_code'];
				$force = $opargs['force'] ?? false;

				$row->activate_data_error( $error_code, null, $results, $force );
			}
			else if( 'resolve_data_error' == $action ) {

				$error_code = $opargs['error_code'];
				$force = $opargs['force'] ?? false;

				$row->resolve_data_error( $error_code, $results, $force );
			}
		}

		// Only send a failed notice if the row validation actually failed.
		if( $row_invalidated && !$condition_set->result ) {
			$results->notice( "Condition set failed: {$ovc_logic->local_name}" );	
		}

		return !$row_invalidated;
	}

	public function save_row( OVC_Row $row, OVC_Results $results ) {

		// ovc_debug( memory_get_usage() . ' ' . __FUNCTION__ . ' Row ID: ' . $row->ID );

		if( !$this->validate_row( $row, $results ) ) {
			return $results->error( "Update row failed: validation errors." );
		}

		// Maybe update custom_meta_updated fields
		// dev:generalize? dev:does this belong here?
		if( $custom_meta_updated = $row->field_set->get_single_meta_key_values( 'custom_meta_updated' ) ) {

			$meta_updated_groups = array(); // Record which groups to update

			foreach( $custom_meta_updated as $field_name => $custom_meta_updated_group ) {

				if( !in_array( $custom_meta_updated_group, $meta_updated_groups ) 
					&& $row->is_field_changed( $field_name, $row->field_set )
				) {
					$meta_updated_groups[] = $custom_meta_updated_group;
				}
			}

			if( count( $meta_updated_groups ) ) {
				$now = current_time( 'mysql', 1 );

				foreach( $meta_updated_groups as $meta_updated_group ) {

					$row->set_new_data( array( "_{$meta_updated_group}_updated" => $now ) );
				}
			}
		}

		/** 
		 * Final preparation of data
		 */
		$new_data = $row->get_full_row_new_values();

		// Magic _meta field for user id of last update
		if( $row->field_set->has_field( '_meta_uid' ) ) {
			$new_data['_meta_uid'] = get_current_user_id();
		}

		// Magic _meta fields for date updated and date created
		unset( 	$new_data['_meta_updated'], 
				$new_data['_meta_created'], 
				$new_data[ $row->field_set->p_key ] 
			);

		// Force UTC timestamps on local environments
		//dev:improve
		if( !on_vida_live() && $row->field_set->has_field( '_meta_updated' ) ) {
			$new_data['_meta_updated'] = current_time( 'mysql', 1 );
		}

		/**
		 * Let's do this
		 */

		global $wpdb;
		$success = true;

		if( $row->exists() ){

			$results->outcome = 'updated';
			$where = array( $row->field_set->p_key => $row->ID );

			$success = $wpdb->update( $row->table, $new_data, $where );
		}
		else {

			$results->outcome = 'inserted';

			$success = $wpdb->insert( $row->table, $new_data );

			// If insert was successful, update index of row //dev:improve
			if( $success ) {

				$temp_id = $row->ID;
				$insert_id = $wpdb->insert_id;

				// Move row to new index in db_data
				$this->db_data[ $row->field_set->name ][ $insert_id ] = $row;
				$row->ID = $insert_id;

				// Unset the temp/new index
				unset( $this->db_data[ $row->field_set->name ][ $temp_id ] );
				
				$this->load_row( $row->field_set, $insert_id, true );
			}
		}

		if( false === $success ) {

			$verb = 'updated' == $results->outcome ? 'updating' : 'inserting';
			return $results->error( "Error {$verb} row. DB error msg: {$wpdb->last_error}" );
		} else {

			// Special Handling when nothing was changed
			if( 0 === $success && 'updated' == $results->outcome ) {
				$results->outcome = 'unchanged';
			}
			
			// After insert
			//$row_id = isset( $insert_id ) ? $insert_id : $row->ID;

			// Special Handling for ovc_schema // dev:improve // dev:generalize
			if( $success && 'ovc_schema' == $row->field_set ) {
				OVCSC::init( true );
			}

			// Check for after_update meta
			if( $after_update_fields = $row->field_set->get_single_meta_key_values( 'after_update', false, false, true ) ) {

				foreach( $after_update_fields as $field_name => $after_update_methods ) {

					if( $row->is_field_changed( $field_name ) ) {

						foreach( $after_update_methods as $method_name ) {

							if( method_exists( $row, $method_name ) ) {

								$row->$method_name( $results );
							}
							else {
								$results->error( "Could not execute after_update method: {$method_name} after updating field: {$field_name} - Method not found on row class" );
							}
						}
					}
				}
			}
		}

		return $results->outcome;
	}

	/**
	 * Delete a row
	 * 
	 * @param OVC_Row 			$row
	 * 
	 * @return mixed
	 **/
	public function delete_row( OVC_Row $row, $results = false ) {
		OVC_Results::maybe_init( $results );
		global $wpdb;

		$delete_result = 
			$wpdb->delete( 
				$row->field_set->table_name(),
				array( $row->field_set->p_key => $row->ID )
			);

		if( false === $delete_result ) {
			return $results->error( "There was an error trying to delete your row." );
		}
		else if ( 0 === $delete_result ) {
			$results->warning( "There was no row deleted for ID: {$row->ID} in table: {$row->field_set->table_name()}" );
		}
		else {
			$results->notice( "Successfully deleted ID: {$row->ID} from table: {$row->field_set->table_name()}" );

			// Special handling for ovc_schema // dev:improve // dev:generalize
			if( 'ovc_schema' == $row->field_set->name ) {
				OVCSC::init( true );
			}
		}

		return $delete_result;
	}
}