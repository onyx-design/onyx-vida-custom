<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_File {
	public $ID	 = null;

	public $file_type;

	public $op_id;

	protected $file_name = null;
	protected $file_size = null;

	public $archived = false;

	protected $_meta_completed;
	protected $_meta_created;

	public $file_meta = null;

	public $line_endings = "\r\n";

	public function __construct( $ID = false ) {

		// Attempt to retrieve file information if an ID is passed
		if( $ID && is_numeric( $ID ) ) {
			
			$this->ID = intval( $ID );
			$this->init_file_meta();
		}

		
	}

	public function init_file_meta( $force_refresh = false ) {

		// Try to initialize from db if we have an ID
		if( $this->ID && ( !isset( $this->file_meta ) || $force_refresh ) ) {

			$raw_file_meta = OVCDB()->get_row_by_id( $this->ID, 'ID', 'wp_ovcop_files' );



			$this->file_meta = oxd( $raw_file_meta, false );

			if( is_array( $this->file_meta ) ) {

				foreach( $this->file_meta as $field => $value ) {

					if( property_exists( $this, $field ) ) {
						$this->$field = $value;
					}
				}
			}
		}

		return $this->file_meta;
	}

	public function name() {
		return $this->file_name;
	}

	public function path() {

		if( $this->archived ) {
			return $this->file_name ? OVCOP_ARCHIVED_CONTENT_DIR . '/' . $this->file_name : false;
		}

		return $this->file_name ? OVCOP_CONTENT_DIR . '/' . $this->file_name : false;
	}

	public function exists() {
		return $this->path() ? file_exists( $this->path() ) : false;
	}

	public function size() {
		return $this->exists() ? filesize( $this->path() ) : null;
	}

	public function url() {
		return $this->path() ? ovc_content_url_from_path( $this->path() ) : false;
	}

	public function extension() {
		return $this->file_name ? substr( $this->file_name, strrpos( $this->file_name, '.' ) + 1 ) : false;
	}

	public function delete() {
		return $this->exists() ? unlink( $this->path() ) : false;
	}

	public function erase_file() {
		file_put_contents( $this->path(), '' );
	}

	public function write( $data = '', $add_line_ending = true ) {

		$data .= $add_line_ending ? $this->line_endings : '';

		file_put_contents( $this->path(), $data, FILE_APPEND );
	}

	public function convert_line_endings( $line_endings = "\r\n" ) {

		$this->line_endings = $line_endings;

		if( $this->exists() ) {
			file_put_contents( $this->path(), preg_replace('~\R~u', $this->line_endings, file_get_contents( $this->path() ) ) );
		}
	}

	public function complete_file() {

		global $wpdb;

		$file_meta = array(
			'file_size'			=> $this->size(),
			'_meta_completed'	=> current_time( 'mysql', 1 )
		);

		$wpdb->update( 'wp_ovcop_files', $file_meta, array( 'ID' => $this->ID ) );
	}

	public function archive_file( $save_files = true, $force = false ) {
		global $wpdb;

		if( ( $save_files || $force ) && $this->exists() ) {
			$moved = rename( OVCOP_CONTENT_DIR . '/' . $this->file_name, OVCOP_ARCHIVED_CONTENT_DIR . '/' . $this->file_name );
		} else {
			$this->delete();
		}

		$file_meta = array(
			'archived'		=> '1'
		);

		$wpdb->update( 'wp_ovcop_files', $file_meta, array( 'ID' => $this->ID ) );
	}

	public static function init_file( $file_name = '', $file_type = '', $op_id = 0 ) {

		if( is_string( $file_name )
			&& is_string( $file_type )
		) {
			global $wpdb;

			$file_meta = array(
				'file_type'	=> $file_type,
				'file_name'	=> $file_name,
				'op_id'		=> (int) $op_id
			);

			if( $wpdb->insert( 'wp_ovcop_files', $file_meta ) ) {
				return new OVC_File( $wpdb->insert_id );
			}
		}

		return false;
	}
}