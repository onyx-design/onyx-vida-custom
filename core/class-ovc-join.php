<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Join {
	public $parent_fs		= false;
	public $child_fs  		= false;

	public $parent_field 	= false;
	public $child_field 	= false;

	public $cardinality 	= '1M';

	public $parent_required = true;
	public $child_required 	= false;

	public $is_valid 		= false;
	public $join_id 		= null;

	public $results 		= false;



	public function __construct( OVCFS $parent_fs, OVCFS $child_fs, $child_field = false, $results = false ) {
		OVC_Results::maybe_init( $results );
		$this->results = $results;

		// Validate join field sets
		if( 'parent' == $child_fs->get_ref_relation( $parent_fs ) ) {

			$this->parent_fs 	= $parent_fs;
			$this->child_fs 	= $child_fs;

			/**
			 * Validate / Determine join fields
			 **/

			// If no $child_field supplied, find it within child_fs
			if( !$child_field 
				&& $foreign_key_fields = $child_fs->get_single_meta_key_values( 'foreign_key' )
			) {


				foreach( $foreign_key_fields as $child_fs_field => $foreign_key_field ) {

					if( $parent_fs->is_local( $foreign_key_field ) ) {

						$this->is_valid = true;
						$this->parent_field = OVCSC::get_field( $foreign_key_field, $parent_fs );
						$this->child_field 	= OVCSC::get_field( $child_fs_field, $child_fs );
						break;
					}
				}
				

			}
			// If $child_field supplied, validate
			else if( 
				OVCSC::init_field( $child_field ) 
				&& $foreign_key_field = $child_field->meta( 'foreign_key' )
				&& $parent_fs->is_local( $foreign_key_field )
			) {

				$this->is_valid = true;
				$this->parent_field = OVCSC::get_field( $foreign_key_field, $parent_fs );
				$this->child_field 	= $child_field;
			} 

			if( $this->is_valid && $this->child_field->meta( 'foreign_key_cardinality' ) ) {
				$this->cardinality = $this->child_field->meta( 'foreign_key_cardinality' );
			}
		}
		else {
			$results->error( "Invalid join parameters." );
		}

	}

	public function get_join_id() {

		if( $this->is_valid && !$this->join_id ) {

			$this->join_id = "{$this->child_fs->alias}-{$this->child_field->local_name}--{$this->parent_fs->alias}-{$this->parent_field->local_name}";
		}

		return $this->join_id;
	}

	public function get_ref_relation( $reference ) {

		$ref_relation = false;
		
		if( $this->is_valid	) {

			if( $this->child_fs->is_local( $reference ) ) {
				$ref_relation = 'child';
			}
			else if( $this->parent_fs->is_local( $reference ) ) {
				$ref_relation = 'parent';
			}
		}

		return $ref_relation;
	}

	public function get_foreign_entity( $reference, $entity_type = 'field' ) {

		if( $this->parent_fs->is_local( $reference ) ) {

			return 'field' == $entity_type ? $this->child_field : $this->child_fs->$entity_type;
		}
		else if( $this->child_fs->is_local( $reference ) ) {
			return 'field' == $entity_type ? $this->parent_field : $this->parent_fs->$entity_type;
		}
		else return null;
	}

	public function get_foreign_alias( $reference ) {
		return $this->get_foreign_entity( $reference, 'alias' );
	}

	public function get_foreign_field( $reference ) {
		return $this->get_foreign_entity( $reference, 'field' );
	}

	public function get_foreign_table( $reference ) {
		return $this->get_foreign_entity( $reference, 'table_name' );
	}	

	public function get_local_entity( $reference, $entity_type = 'field' ) {

		if( $this->parent_fs->is_local( $reference ) ) {

			return 'field' == $entity_type ? $this->parent_field : $this->parent_fs->$entity_type;
		}
		else if( $this->child_fs->is_local( $reference ) ) {
			return 'field' == $entity_type ? $this->child_field : $this->child_fs->$entity_type;
		}
		else return null;
	}

	public function get_local_alias( $reference ) {
		return $this->get_local_entity( $reference, 'alias' );
	}

	public function get_local_field( $reference ) {
		return $this->get_local_entity( $reference, 'field' );
	}

	public function get_local_table( $reference ) {
		return $this->get_local_entity( $reference, 'table_name' );
	}	

	/**
	 * Initialize the table join object
	 * 
	 * @param string 	$table_left 	name of the left table
	 * @param string	$table_right 	name of the right table
	 * 
	 * @return bool 
	 ** /
	public function __construct( $table_left, $table_right ) {

		$this->meta = 	OVCSC::get_field_set_meta(
							OVCSC::get_field_set( $table_left ),
							"join.{$table_right}"
					 	);

		if( $this->meta ) {
			$this->table_left 	= $table_left;
			$this->table_right 	= $table_right;

			foreach( $this->meta as $field_left => $field_right ) {
				$this->field_left 	= OVCSC::get_field( $field_left );
				$this->field_right 	= OVCSC::get_field( $field_right );
			}

			if( $this->field_left && $this->field_right ) {
				$this->is_valid = true;
			}
		}
	}
	/* */
}
