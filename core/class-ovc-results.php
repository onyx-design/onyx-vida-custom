<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Results {
	public $main_msg 	= '';
	public $messages 	= array();
	
	public $success 	= false;
	public $has_error 	= false;
	public $outcome 	= '';

	public $field_errors = array( // dev:generalize // this is mainly used for data fixes
		'required' 		=> array(),
		'invalid'		=> array(),
		'not_unique'	=> array(),
		'unsynced'		=> array()
	); 

	private $custom_vars = array();

	public function __set( $name, $value ) {
		$this->custom_vars[ $name ] = $value;
	}

	public function __get( $name ) {
		return array_key_exists( $name, $this->custom_vars ) ? $this->custom_vars[ $name ] : null;
	}

	public function __isset( $name ) {
		return isset( $this->custom_vars[ $name ] );
	}

	public function __unset( $name ) {
		unset( $this->custom_vars[ $name ] );
	}


	public function error( $msg ) {
		$this->messages[] = array(
			'type'	=> 'error',
			'when'	=> date( 'Y-m-d H:i:s'),
			'msg'	=> ( $msg ? $msg : 'ERROR!' )
		);
		$this->has_error = true;
		return false; // Always return false to simplify code where called (usually you want to get a false boolean when there is an error)
	}

	public function notice( $msg ) {
		$this->messages[] = array(
			'type'	=> 'notice',
			'when'	=> date( 'Y-m-d H:i:s'),
			'msg'	=> ( $msg ? $msg : 'NOTICE!' )
		);
	}

	public function warning( $msg ) {
		$this->messages[] = array(
			'type'	=> 'warning',
			'when'	=> date( 'Y-m-d H:i:s'),
			'msg'	=> ( $msg ? $msg : 'WARNING!' )
		);
	}

	public function get_msgs( $type = 'all', $just_msgs = true ) {
		$return_msgs = array();
		foreach( $this->messages as $message ) {
			if( 'all' == $type || $message['type'] == $type ) {
				$return_msgs[] = $just_msgs ? $message['msg'] : $message;
			}
		} 
		return $return_msgs;
	}

	public function set_var( $name, $value ) {

	}

	public static function maybe_init( &$results_obj ) {
		if( !( $results_obj instanceof OVC_Results ) ) {
			$results_obj = new OVC_Results;
		}
		return $results_obj;
	}
}