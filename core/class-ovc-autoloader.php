<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Autoloader {

	/**
	 * Possible subdirectories for OVC Classes
	 * @var array
	 */
	private $subdirs = array(
		'',
		'/ovcfs',
		'/ovc-row',
		'/ovc-api',
		'/ovc-table',
		'/ovc-external',
		'/ovcop'
	);

	/**
	 * The Constructor
	 */
	public function __construct() {
		if ( function_exists( "__autoload" ) ) {
			spl_autoload_register( "__autoload" );
		}

		spl_autoload_register( array( $this, 'autoload' ) );
	}

	/**
	 * Take a class name and turn it into a file name
	 * @param  string $class
	 * @return string
	 */
	private function get_file_path_from_class( $class ) {

		// Convert class name to class file name format
		$class = '/class-' . str_replace( '_', '-', strtolower( $class ) ) . '.php';

		foreach( $this->subdirs as $subdir ) {

			// Look in /instance first, then /core
			foreach( array( 'instance', 'core' ) as $main_dir ) {

				$include_path = OVC_PATH . '/' . $main_dir;

				$path = "{$include_path}{$subdir}{$class}";

				if( is_readable( $path ) ) {
					return $path;
				}
			}
		}
	}

	/**
	 * Auto-load WC classes on demand to reduce memory consumption.
	 *
	 * @param string $class
	 */
	public function autoload( $class ) {

		if ( $path = $this->get_file_path_from_class( $class ) ) {
			include_once( $path );
			return true;
		}
		return false;
	}
}
new OVC_Autoloader();