<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

// Define global singletion function accessors
function OVCDB() { // dev:ovcsc
	return OVC_Data_Manager::instance();
}

function OVC_Lists() {
	return OVC_Lists::instance();
}

function FS( $fs_reference, $fs_type = null ) {
	return OVCSC::get_field_set( $fs_reference, $fs_type );
}

function ovc_content_url_from_path( $path = false ) {
	if( !$path || false == strpos( $path, 'wp-content') ) {
		return false;
	}

	$path_parts = explode( 'wp-content', $path );
	return content_url() . $path_parts[ count( $path_parts ) - 1 ];
}

function ovc_path_from_url( $url ) {
	
	if( 0 === strpos( $url, site_url() ) ) {

		$path = ltrim( $url, site_url() );
		$path = ltrim( $path, '/' );

		return ABSPATH . $path;
	}
	else {
		return false;
	}
}

function get_ovc_image_path( $filename = '' ) {

	return file_exists( OVC_IMAGE_DIR . '/' . $filename ) ? OVC_IMAGE_DIR . '/' . $filename : false;
}

function get_ovc_image_url( $filename = '' ) {

	return file_exists( OVC_IMAGE_DIR . '/' . $filename ) ? content_url( '/ovc-images/' . $filename ) : false;
}

// Get script execution time
function ox_exec_time() {
	return ( microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"] );
}

function log_print_r( $log, $message = '' ) {

	$log = print_r( $log, true );

	if( $message ) {
		$log = "$message: $log";
	}

	error_log( $log );
}

function log_var( $log, $message = '' ) {

	$log = var_export( $log, true );

	if( $message ) {
		$log = "$message: $log";
	}

	error_log( $log );
}

// Auto-init and access OVC_Debug class
function ovc_debug($arg=null) {
	$instance = OVC_Debug::instance();
	return func_num_args() ? $instance($arg) : $instance;
}

// get calling function/class::method name
function debug_backtrace_str( $limit = 4 ) {
	$debug_backtrace = debug_backtrace();
	$backtrace_pieces = array();

	for( $i = 1; $i <= $limit; $i++ ) {

		if( !isset( $debug_backtrace[ $i ] ) ) {
			break;
		}

		$backtrace_piece = $debug_backtrace[ $i ]['function'];

		if( isset( $debug_backtrace[ $i ]['class'] ) ) {
			$backtrace_piece = $debug_backtrace[$i]['class'] . $debug_backtrace[ $i ]['type'] . $backtrace_piece;
		}

		array_push( $backtrace_pieces, $backtrace_piece );
	}

	return implode( ' < ', $backtrace_pieces );
}
function get_calling_function() {
	$debug_backtrace = debug_backtrace();

	ovc_debug( $debug_backtrace );
}

if( !function_exists( 'log_print_r' ) ) {
	function log_print_r( $log, $message = '' ) {

		$log = print_r( $log, true );

		if( $message ) {
			$log = "$message: $log";
		}

		error_log( $log );
	}
}

// Check if currently on VIDA Live site
function on_vida_live() {
	return false !== strpos( get_site_url(), 'vida-us.com' );
}


// Convert strings to bools and bools to strings
function ox_boolstr( $b = null, $force = true ) {
	if( !$force && !is_bool( $b ) ) {
		return $b;
	}
	return $b ? 'true' : 'false';
}

function ox_maybe_str_to_bool( $str = '' ) {
	if( "true" === $str ) {return true;}
	else if( "false" === $str ) {return false;}
	else {return $str;}
}

function ox_maybe_init_class( &$class, $class_name ) {
	if( !( $class instanceof $class_name ) ) {
		if( class_exists( $class_name ) ) {
			$class = new $class_name;
		}
		else {
			$class = false;
		}
	}
}

// Set a default value if not set
function oxd( $var = null, $default = null ) {
	$var = isset( $var ) ? $var : $default;
	return $var;
}
function oxdr( &$var, $default = null ) {
	$var = isset( $var ) ? $var : $default;
	return $var;
}

// Retrieve a user's custom selection
function get_ovc_user_custom_selection( $user_id = 0, $field_set = 'ovc_products' ) {
	global $wpdb;
	$user_cs_ids = maybe_unserialize( $wpdb->get_var( $wpdb->prepare( "SELECT meta_value FROM {$wpdb->usermeta} WHERE user_id = %d AND meta_key='_ovcdt_custom_select_{$field_set}'", $user_id ) ) );
	return $user_cs_ids ? $user_cs_ids : array();
}

function ovc_cdata_wrap( $string = '', $echo = true ) {

	$string = '<![CDATA[' . $string . ']]>';

	if( $echo ) {
		echo $string;
	}

	return $string;
}

function ovc_memory_limit_in_bytes() {

	$memory_limit = trim( ini_get( 'memory_limit' ) );

    $last = strtolower( $memory_limit[ strlen( $memory_limit ) -1 ] );

	$memory_limit = str_replace( array( 'G', 'M', 'K' ), '', $memory_limit );
    switch( $last ) {
        case 'g':
            $memory_limit *= 1024;
        case 'm':
            $memory_limit *= 1024;
        case 'k':
            $memory_limit *= 1024;
    }

    return $memory_limit;
}

function ovc_memory_usage_percentage() {

	$memory_usage_in_bytes = memory_get_usage();
	$memory_limit = ovc_memory_limit_in_bytes();

	return $memory_usage_in_bytes / $memory_limit;
}

// Override WooCommerce functionality

function ovc_force_variation_titles( $variation_id = 0 ) {
	global $wpdb;
	$ovc_id = OVCDB()->get_id_by_valid_id( $variation_id, 'post_id', FS( 'ovc_products' ) );

	$ovcp = new OVC_Product_WooCommerce( FS( 'ovc_products' ), $ovc_id );
	$ovc_wc_variation_title = $ovcp->format_wc_variation_title();

	$wpdb->update( $wpdb->posts, array( 'post_title' => $ovc_wc_variation_title ), array( 'ID' => $variation_id ) );
}
add_action( 'woocommerce_save_product_variation', 'ovc_force_variation_titles', 9999, 1 );

/**
 * Sort the variations of a product when getting them from WooCommerce
 * 
 * @param 	array 		$children 
 * @param 	WC_Product 	$parent 
 * @param 	bool 		$visible_only
 * 
 * @return 	array
 */
function ovc_size_sort_variations( $children, $parent, $visible_only ) {

	usort( $children, function( $a, $b ) {

		$variation_a = wc_get_product( $a );
		$variation_b = wc_get_product( $b );

		if( ( $attribute_a = $variation_a->get_attribute( 'pa_wc-size' ) ) && ( $attribute_b = $variation_b->get_attribute( 'pa_wc-size' ) ) ) {

			$attribute_a = get_term_by( 'name', $attribute_a, 'pa_wc-size' );
			$attribute_b = get_term_by( 'name', $attribute_b, 'pa_wc-size' );

			if( !$attribute_a || !$attribute_b ) {
				return 0;
			}

			$attribute_a_order = get_term_meta( $attribute_a->term_id, 'order_pa_wc-size', true );
			$attribute_b_order = get_term_meta( $attribute_b->term_id, 'order_pa_wc-size', true );

			return $attribute_a_order > $attribute_b_order;
		}

		return 0;
	});

	return $children;
}
add_filter( 'woocommerce_get_children', 'ovc_size_sort_variations', 10, 3 );