<?php
/* 
Handles all backend admin page output for OVC
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Admin {

	public $ovc_admin_notices = array();

	public $admin_notices = '';

	public $page_map = array(
		'ovc'					=> array( 'js' => array( ':jquery-ui-droppable', 'ovc-noticebox', 'oxpm', 'oxmd', 'ovc-admin', 'ovc-image-manager', 'ovc-assembly', 'ovc-screen', 'ovc-dynamic-field' ), 'css' => array( 'ovc-admin', 'ovcdt', 'oxmd', 'ovc-image-manager' ) ),
		'ovcop'					=> array( 'js' => array( 'ovc-noticebox', 'oxpm', 'oxmd', 'ovcop', 'clipboard', 'ovc-admin' ), 'css' => array( 'ovc-admin', 'oxmd', 'ovcdt', 'ovcop' ) ),
		'ovc-schema'			=> array( 'js' => array( 'ovc-noticebox', 'oxpm', 'oxmd', 'ovc-admin' ), 'css' => array( 'ovc-admin', 'ovcdt', 'oxmd' ) ),
		'ovc-dev'				=> array( 'js' => array( ), 'css' => array( 'ovc-admin' ) )
	);

	public $page_slug = '';

	public function __construct() {
		
		$this->page_slug = isset( $_GET['page'] ) ? $_GET['page'] : '';

		add_action( 'admin_init', array( $this, 'update_auto_sync_option' ), 0 );

		add_action( 'admin_menu', array( $this, 'register_menus' ) ); //Admin Menu link
		add_action( 'admin_head', array( $this, 'include_styles' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

		add_action( 'in_admin_header', array($this, 'hide_admin_notices_vida_users'), 999999998 );
		add_action( 'in_admin_header', array($this, 'show_ovc_admin_notices'), 999999999 );
	}

	public function update_auto_sync_option() {

		$auto_sync = $_GET['update-auto-sync'] ?? false;

		if( false === $auto_sync ) {
			return;
		}

        $user = wp_get_current_user();

        if ( isset( $user->roles ) && is_array( $user->roles ) ) {

            if( array_intersect( array( 'administrator', 'vida_sr_data_tech', 'vida_data_tech' ), $user->roles ) ) {
		
				update_option( 'ovc_auto_sync_ops', $auto_sync );
            }
        }

	}

	public function register_menus() {
		
		add_menu_page( 'VIDA - OVC', 'VIDA - OVC', 'ovcdt_ovc_products', OVC_SLUG, array( $this, 'build_admin_page' ), OVC_URL . 'assets/img/vida-menu-icon.png', '1.11111' );

		add_submenu_page( OVC_SLUG, 'OVC Product Data Manager', 'OVC Product Manager', 'ovcdt_ovc_products', OVC_SLUG, array( $this, 'build_admin_page' ) );
		add_submenu_page( OVC_SLUG, 'OVC Operations', 'OVC Operations', 'ovcop_access', OVC_SLUG.'op', array( $this, 'build_admin_page' ) );

		if( 2 == get_current_user_id() ) { // DEV TODO: IMPROVE / GENERALIZE ADMIN ROLES & ACCESS

			// DEV DEV DEV
			//add_action( 'wp_before_admin_bar_render', array($this, 'admin_bar_dev'), 9999 );
			add_submenu_page( OVC_SLUG, 'OVC Schema', 'OVC Schema', 'ovcdt_ovc_fieldmeta', OVC_SLUG.'-schema', array( $this, 'build_admin_page' ) );

			add_submenu_page( OVC_SLUG, 'OVC Development Testing Area', 'Dev / Testing', 'edit_pages', OVC_SLUG.'-dev', array( $this, 'build_admin_page' ) );
		}
	}

	

	public function build_admin_page() {
		$page_path = OVC_PATH."/templates/ovc-page-{$this->page_slug}.php";

		if( is_readable( $page_path ) ) {
			include_once( $page_path );
		}
		else {
			echo "Unable to locate page template. Path: {$page_path}";
		}
	}
	
	public function include_styles() {
		// Include these styles on every admin page (currently only a small css file for the OVC Auto sync toolbar indicator)
		echo '<link rel="stylesheet" type="text/css" href="'.OVC_URL.'assets/css/ovc-admin.css" />'."\n";

		/*
		$css_file_names = isset( $this->page_map[ $this->page_slug ]['css'] ) ? $this->page_map[ $this->page_slug ]['css'] : array();

		// Force show OVC admin notices (overrides User Role Editor plugin's hiding of admin messages for non-administrator users)
		echo '<style>.notice-ovc{display:block !important;}</style>';

		if( $css_file_names ) {
			foreach ( $this->page_map[ $this->page_slug ]['css'] as $file_name ) {
				$css_url =  OVC_URL."assets/css/{$file_name}.css";
				
				echo '<link rel="stylesheet" type="text/css" href="'.$css_url.'" />'."\n";
			}
		}*/
	}

	public function enqueue_scripts() {
		$js_file_names = isset( $this->page_map[ $this->page_slug ]['js'] ) ? $this->page_map[ $this->page_slug ]['js'] : array();

		if( $js_file_names ) {
			foreach ( $this->page_map[ $this->page_slug ]['js'] as $file_name ) {

				// Check if it's just a WP script
				if( 0 === strpos( $file_name, ':' ) ) {
					wp_enqueue_script( str_replace( ':', '', $file_name ) );
				}
				else {
					$js_url =  OVC_URL."assets/js/{$file_name}.js";
					
					wp_enqueue_script( $file_name, $js_url, array( 'jquery', 'lodash' ), NULL );
					wp_localize_script( $file_name, 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
					//wp_localize_script( $file_name, 'ovc_data', array( 'ovc_url' => OVC_URL ) );
				}
			}
		}

		wp_enqueue_style( 'ovc', OVC_URL . '/assets/css/ovc.css' );
	}




	/**
	 * ADMIN NOTICE HANDLING
	 **/

	// Queue an OVC admin notice
	public function add_ovc_admin_notice( $notice_message , $notice_type ){
		array_push($this->ovc_admin_notices, array( $notice_message, $notice_type ) );
	}	

	// FORCE-HIDE ALL ADMIN NOTICES FOR ALL NON-ADMINISTRATOR USERS
	public function hide_admin_notices_vida_users() {
		$user = wp_get_current_user();
		if ( isset( $user->roles ) && is_array( $user->roles ) ) {

			if( ! in_array( 'administrator', $user->roles ) ) {
				remove_all_actions('admin_notices'); // remove all default admin notices from anyone who isn't the administrator
			}
			
		}
	}

	/** Add back our saved Admin Notices for certain VIDA Roles
	*	To use this method, parameters take an array. The array should contain the message followed by the notice type.
	*	our methods will create a notice based on the notice type and then include our message
	*/
	public function show_ovc_admin_notices(){
		$user = wp_get_current_user();
		$force_notices = array('');
		
		if ( isset( $user->roles ) && is_array( $user->roles ) ) {
			
			//add our custom admin notices to these user roles
			foreach( $this->ovc_admin_notices as $notice ){
				
				$this->print_ovc_admin_notice( $notice[0], $notice[1] );
			}
		}
	}

	// Print an OVC admin notice //dev:improve
	public function print_ovc_admin_notice( $notice_message, $notice_type ) {
		echo '<div class="notice notice-ovc notice-' . $notice_type . '">' . $notice_message . '</div>';
	}





	/**
	 * DEV / UNUSED / DEPRECATED CODE
	 **/

	// DEV DEV DEV
	public function admin_bar_dev() {
		global $wp_admin_bar;
		global $ovc_admin_bar;
		$ovc_admin_bar = clone( $wp_admin_bar );
	}

	// Function for displaying admin notices
	public function ovc_admin_notice( $message = "No text was sent for this OVC admin notice.", $class = "updated" ) {
		if( !$this->admin_notices ) {
			add_action( 'admin_notices', array( $this, 'ovc_display_admin_notices' ) );
		}

		$this->admin_notices .= "<div class=\"{$class}\"><p>{$message}</p></div>";
	}

	public function ovc_display_admin_notices() {
		echo $this->ovc_admin_notices;
	}
}