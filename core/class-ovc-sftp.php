<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;

use phpseclib\Net\SFTP;

class OVC_SFTP {

	/**
	 * Current sFTP Credentials, based on environment
	 * 
	 * @var 	array|null
	 */
	protected $sftp_credentials = null;

	/**
	 * Array of all sFTP Environments
	 * 
	 * @var 	array
	 */
	protected $sftp_environments = array(
		'walmart'	=> array(
			'name'	=> 'Walmart sFTP',
			'host'	=> 'seller-ft.walmart.com',
			'port'	=> 22,	// sftp
			'user'	=> '10000000364',
			'pass'	=> 'Walmart123'
		)
	);

	/**
	 * sFTP Connection Stream
	 * 
	 * @var 	sFTP Stream
	 */
	protected $sftp_instance = false;

	/**
	 * sFTP System Type
	 * 
	 * @var 	string
	 */
	protected $sftp_sys_type;

	/**
	 * @var 	OVC_Results
	 */
	public $results = false;

	public function __construct( $environment, $autoconnect = true ) {
		OVC_Results::maybe_init( $this->results );

		$this->set_environment( $environment );
		$this->check_connection( $autoconnect );

		// Register PHP Shutdown function that will ensure the sFTP connection is closed
		register_shutdown_function( array( $this, 'close_connection' ) );
	}

	/**
	 * Set the sFTP Credentials based on the input $environment,
	 * or use the first $environment if the input is not found
	 * 
	 * @param 	string 	$environment
	 * 
	 * @return 	array
	 */
	public function set_environment( $environment ) {

		if( !isset( $environment ) && isset( $this->sftp_credentials ) ) {
			return $this->sftp_credentials;
		}

		// Set the sFTP Credentials to the selected environment, or the first if that one is not found
		return $this->sftp_credentials = $this->sftp_environments[ $environment ] ?? current( $this->sftp_environments );
	}

	/**
	 * Check whether we're already connected to sFTP or not, or autoconnect if true
	 * 
	 * @param 	bool $autoconnect 
	 * 
	 * @return 	type
	 */
	public function check_connection( $autoconnect = true ) {

		if( $this->is_connected() ) {
			return true;
		} else if( $autoconnect ) {
			return $this->connect();
		}

		return $this->results->error( "{$this->sftp_credentials['name']} not connected." );
	}

	/**
	 * Returns whether we have a connection or not
	 * 
	 * @return 	bool
	 */
	public function is_connected() {

		if( $this->sftp_instance ) {
			return $this->sftp_instance->isAuthenticated();
		} else {
			return false;
		}
	}

	/**
	 * Connect to our sFTP
	 * 
	 * @return 	OVC_Results|bool
	 */
	public function connect() {
		OVC_Results::maybe_init( $this->results );

		$this->sftp_instance = new SFTP( $this->sftp_credentials['host'], $this->sftp_credentials['port'], 10 );

		for( $t = 3; $t > 0; $t-- ) {

			if( $this->sftp_instance->login( $this->sftp_credentials['user'], $this->sftp_credentials['pass'] ) ) {

				return true;
			}
		}

		return $this->results->error( "{$this->sftp_credentials['name']} login failed." );
	}

	/**
	 * Close the connection to our sFTP
	 * 
	 * @param 	OVC_Results|bool 	&$results 
	 */
	public function close_connection() {

		if( $this->is_connected() ) {

			$this->sftp_instance->disconnect();
			$this->sftp_instance = false;
			$this->results->notice( "{$this->sftp_credentials['name']} connection closed." );
		}
	}

	/**
	 * Return the sFTP Connections current directory
	 * 
	 * @return 	string
	 */
	public function pwd() {

		if( $this->is_connected() ) {

			return $this->sftp_instance->pwd() ?: $this->results->error( "Cannot get current directory for {$this->sftp_credentials['name']}." );
		}

		return $this->results->error( "{$this->sftp_credentials['name']} not connected." );
	}

	/**
	 * Change the sFTP Connection to a different directory
	 * 
	 * @param 	string 	$directory
	 * 
	 * @return 	OVC_Results|bool
	 */
	public function chdir( $directory = '.' ) {

		if( $this->is_connected() ) {

			return $this->sftp_instance->chdir( $directory ) ?: $this->results->error( "Unable to change to directory: {$directory} for {$this->sftp_credentials['name']}." );
		}

		return $this->results->error( "{$this->sftp_credentials['name']} not connected." );
	}

	/**
	 * Get the list of files in the $directory
	 * 
	 * @param 	string 	$directory
	 * 
	 * @return 	array|bool
	 */
	public function nlist_dir( $directory = '.' ) {

		if( $this->is_connected() ) {

			return $this->sftp_instance->nlist() ?: $this->results->error( "Unable to change to directory: {$directory} for {$this->sftp_credentials['name']}." );
		}

		return $this->results->error( "{$this->sftp_credentials['name']} not connected." );
	}

	/**
	 * Get the detailed list of files in the $directory
	 * 
	 * @param 	string	$directory
	 * 
	 * @return 	array|bool
	 */
	public function rawlist_dir( $directory = '.' ) {

		if( $this->is_connected() ) {

			return $this->sftp_instance->rawlist() ?: $this->results->error( "Unable to change to directory: {$directory} for {$this->sftp_credentials['name']}." );
		}

		return $this->results->error( "{$this->sftp_credentials['name']} not connected." );
	}

	/**
	 * List the files in the specified $directory
	 * 
	 * @param 	string 	$directory 
	 * @param 	bool 	$files_only 
	 * @param 	bool 	$sort_key 
	 * @return type
	 */
	public function list_dir( $directory = '.', $files_only = false, $sort_key = false ) {

	}

	/**
	 * Get the newest file in the specified $directory
	 * 
	 * @param 	string 	$directory
	 * @param 	bool 	$name_only
	 * 
	 * @return 	string
	 */
	public function get_newest_file( $directory = '.', $name_only = true ) {

	}

	/**
	 * Get the list of files in the specified $directory sorted by last updated
	 * 
	 * @param 	string 	$directory
	 * 
	 * @return 	array
	 */
	public function list_dir_mdtm( $directory = '.' ) {

	}

	/**
	 * Get the last modified time of the specified $file_name
	 * 
	 * @param 	string	$file_name
	 * 
	 * @return 	OVC_Results|string
	 */
	public function mdtm( $file_name ) {

	}

	public function file_stats( $file_name ) {

		if( $this->is_connected() ) {
			return $this->sftp_instance->stat( $file_name ) ?: $this->results->error( "Unable to find file: {$file_name} for {$this->sftp_credentials['name']}." );
		}

		return $this->results->error( "{$this->sftp_credentials['name']} not connected." );
	}

	/**
	 * Get the file size of the specified $file_name
	 * 
	 * @param 	string	$file_name
	 * 
	 * @return 	OVC_Results|int
	 */
	public function file_size( $file_name ) {

		if( $this->is_connected() ) {
			return $this->sftp_instance->size( $file_name ) ?: $this->results->error( "Unable to find file: {$file_name} for {$this->sftp_credentials['name']}." );
		}

		return $this->results->error( "{$this->sftp_credentials['name']} not connected." );
	}

	/**
	 * Rename the file from $old_name to $new_name
	 * 
	 * @param 	string	$old_name 
	 * @param 	string	$new_name
	 * 
	 * @return 	bool
	 */
	public function rename( $old_name, $new_name ) {

		if( $this->is_connected() ) {
			return $this->sftp_instance->rename( $old_name, $new_name ) ?: $this->results->error( "Unable to rename file: {$old_name} for {$this->sftp_credentials['name']}." );
		}

		return $this->results->error( "{$this->sftp_credentials['name']} not connected." );
	}

	/**
	 * Upload the specified $local_file to the $remote_file location
	 * 
	 * @param 	string	$remote_file
	 * @param 	string	$local_file
	 * 
	 * @return 	OVC_Results|bool
	 */
	public function put( $remote_file, $local_file ) {

		if( !$remote_file || !$local_file ) {
			return $this->results->error( 'sFTP File Upload Failed. Invalid file names supplied.' );
		}

		if( $this->is_connected() ) {

			if( !$this->sftp_instance->put( $remote_file, $local_file, SFTP::SOURCE_LOCAL_FILE ) ) {
				return $this->results->error( 'Failed to upload file.' );
			} else {
				$this->results->notice( 'File uploaded successfully.' );
				return true;
			}
		}

		return $this->results->error( "{$this->sftp_credentials['name']} not ocnnected." );
	}

	/**
	 * Download the specified $remote_file to the $local_file location
	 * 
	 * @param 	string	$remote_file 
	 * @param 	string	$local_file
	 * 
	 * @return 	OVC_Results|bool
	 */
	public function get( $remote_file, $local_file ) {

		if( !$remote_file || !$local_file ) {
			return $this->results->error( 'sFTP File Download Failed. Invalid file names supplied.' );
		}

		if( $this->is_connected() ) {

			if( !$this->sftp_instance->get( $remote_file, $local_file ) ) {
				return $this->results->error( 'Failed to download file.' );
			} else {
				$this->results->notice( 'File downloaded successfully.' );
				return true;
			}
		}

		return $this->results->error( "{$this->sftp_credentials['name']} not ocnnected." );
	}
}