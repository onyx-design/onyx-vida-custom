<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

error_reporting(E_ALL);
ini_set("log_errors", 1); 

class OVC_API_Shopify extends OVC_API_Request {

	public static $api_name = 'Shopify API';

	protected static $api_environments = array(
		'live' 	=> array( 
			'api_key' 		=> 'c0bd52cb33fafbe9643c38a7231c609e',
			'api_password' 	=> '917e89664fe1fb45dc23cea0579191a5',
			'base_url'		=> 'shop-angelina.myshopify.com/admin/',
			'shop_id'		=> 'shop-angelina'
		),
		'dev'	=> array( 
			'api_key' 		=> '94bbb694a653739d395466e04e58b425',
			'api_password' 	=> 'd79cebd6df13e672a15c43a960704688',
			'base_url'		=> 'angelina-hosiery.myshopify.com/admin/',
			'shop_id'		=> 'angelina-hosiery'
		),
		'mgs'	=> array(
			'api_key'		=> '304aaa3cb970e155c8940142af85f714',
			'api_password'	=> '1cb9fb5e4f1700e29c4a178a1d649a62',
			'base_url'		=> 'marygraceswim.myshopify.com/admin/',
			'shop_id'		=> 'marygraceswim',
		),
		'rss'	=> array(
			'api_key'		=> '5d32c56803449256d3cb89f7e58bd18f',
			'api_password'	=> 'c77612c4acc0fb74dd613df72ced0b0d',
			'base_url'		=> 'robert-siegel-studio.myshopify.com/admin/',
			'shop_id'		=> 'robert-siegel-studio',
		)
	);

	public static $request_types = array(

		/*
		 * Product Endpoints
		 */

		// Get Products
		'get_products'				=> array(
			'method'			=> 'GET',
			'endpoint'			=> 'products.json',
			'query_params'		=> array( 'ids', 'limit', 'page', 'collection_id', 'since_id', 'published_status' )
		),
		// Get Single Product
		'get_product'				=> array(
			'method'			=> 'GET',
			'endpoint'			=> 'products/[shopify_product_id].json',
			'url_params'		=> array( 'shopify_product_id' ),
			'response_wrap'		=> 'product'
		),
		// Get Product Count
		'get_product_count'			=> array(
			'method'			=> 'GET',
			'endpoint'			=> 'products/count.json',
			'query_params'		=> array( 'collection_id' )
		),
		// Create Product
		'create_product'			=> array(
			'method'			=> 'POST',
			'endpoint'			=> 'products.json',
			'response_wrap'		=> 'product'
		),
		// Update Product
		'update_product'			=> array(
			'method'			=> 'PUT',
			'endpoint'			=> 'products/[shopify_product_id].json',
			'url_params'		=> array( 'shopify_product_id' ),
			'response_wrap'		=> 'product'
		),
		// Delete Product
		'delete_product'			=> array(
			'method'			=> 'DELETE',
			'endpoint'			=> 'products/[shopify_product_id].json',
			'url_params'		=> array( 'shopify_product_id' )
		),

		/*
		 * Variant Endpoints
		 */

		// Get Variants
		'get_variants'				=> array(
			'method' 			=> 'GET',
			'endpoint'			=> 'products/[shopify_product_id]/variants.json',
			'url_params'		=> array( 'shopify_product_id' ),
			'response_wrap'		=> 'variants'
		),
		// Get Single Variant
		'get_variant'				=> array(
			'method' 			=> 'GET',
			'endpoint'			=> 'variants/[shopify_variant_id].json',
			'url_params'		=> array( 'shopify_variant_id' ),
			'response_wrap'		=> 'variant'
		),
		// Create Variant
		'create_variant'			=> array(
			'method'			=> 'POST',
			'endpoint'			=> 'products/[product_id]/variants.json',
			'url_params'		=> array( 'product_id' ),
			'response_wrap'		=> 'variant'
		),
		// Update Variant
		'update_variant'			=> array(
			'method' 			=> 'PUT',
			'endpoint'			=> 'variants/[shopify_variant_id].json',
			'url_params'		=> array( 'shopify_variant_id' ),
			'response_wrap'		=> 'variant'
		),
		// Delete Variant
		'delete_variant'			=> array(
			'method'			=> 'DELETE',
			'endpoint'			=> 'products/[shopify_product_id]/variants/[shopify_variant_id].json',
			'url_params'		=> array( 'shopify_product_id', 'shopify_variant_id' )
		),

		/*
		 * Inventory Item Endpoints
		 */

		// Get Inventory Items
		'get_inventory_items'		=> array(
			'method'			=> 'GET',
			'endpoint'			=> 'inventory_items.json',
			'query_params'		=> array( 'ids', 'page', 'limit' ),
			'response_wrap'		=> 'inventory_items'
		),
		// Get Inventory Item
		'get_inventory_item'		=> array(
			'method'			=> 'GET',
			'endpoint'			=> 'inventory_items/[inventory_item_id].json',
			'url_params'		=> array( 'inventory_item_id' ),
			'response_wrap'		=> 'inventory_item'
		),
		// Update Inventory Item
		/*
			"inventory_item": {
				"id": 808950810,
				"sku": "new sku",
				"created_at": "2018-05-07T15:33:38-04:00",
				"updated_at": "2018-05-07T15:33:38-04:00",
				"tracked": true
			}
		 */
		'update_inventory_item'		=> array(
			'method'			=> 'PUT',
			'endpoint'			=> 'inventory_items/[inventory_item_id].json',
			'url_params'		=> array( 'inventory_item_id' ),
			'response_wrap'		=> 'inventory_item'
		),

		/*
		 * Inventory Level Endpoints
		 */

		// Get Inventory Levels
		'get_inventory_levels'		=> array(
			'method'			=> 'GET',
			'endpoint'			=> 'inventory_levels.json',
			'query_params'		=> array( 'inventory_item_ids', 'location_ids', 'page', 'limit' ),
			'response_wrap'		=> 'inventory_levels'
		),
		// Create Inventory Level
		'create_inventory_level'	=> array(
			'method'			=> 'POST',
			'endpoint'			=> 'inventory_levels/connect.json',
			'response_wrap'		=> 'inventory_level'
		),
		// Update Inventory Level
		'update_inventory_level'	=> array(
			'method'			=> 'POST',
			'endpoint'			=> 'inventory_levels/adjust.json',
			'response_wrap'		=> 'inventory_level'
		),
		// Delete Inventory Level
		/*
			Deleting an inventory level for an inventory item removes that item from the specified location. Every inventory item must have at least one inventory level. To move inventory to another location, first connect the inventory item to another location, and then delete the previous inventory level
		 */
		'delete_inventory_level'	=> array(
			'method'			=> 'DELETE',
			'endpoint'			=> 'inventory_levels.json',
			'query_params'		=> array( 'inventory_item_id', 'location_id' ),
		),

		/*
		 * Location Endpoints
		 */

		// Get Locations
		'get_locations'				=> array(
			'method'			=> 'GET',
			'endpoint'			=> 'locations.json',
			'response_wrap'		=> 'locations'
		),
		// Get Single Locations
		'get_location'				=> array(
			'method'			=> 'GET',
			'endpoint'			=> 'locations/[location_id].json',
			'url_params'		=> array( 'location_id' ),
			'response_wrap'		=> 'location'
		),
		// Get Inventory Levels
		'get_location_inventory_levels'	=> array(
			'method'			=> 'GET',
			'endpoint'			=> 'locations/[location_id]/inventory_levels.json',
			'url_params'		=> array( 'location_id' ),
			'response_wrap'		=> 'inventory_levels'
		),

		/*
		 * Collection Endpoints
		 */

		// Get Custom Collections
		'get_custom_collections'	=> array(
			'method'			=> 'GET',
			'endpoint'			=> 'custom_collections.json',
			'query_params'		=> array( 'limit', 'since_id', 'fields' ),
			'response_wrap'		=> 'custom_collections'
		),
		// Get Smart Collections
		'get_smart_collections'		=> array(
			'method'			=> 'GET',
			'endpoint'			=> 'smart_collections.json',
			'query_params'		=> array( 'limit', 'since_id', 'fields' ),
			'response_wrap'		=> 'smart_collections'
		),

		/*
		 * Metafield Endpoints
		 */

		// Get Metafields
		'get_metafields'			=> array(
			'method'			=> 'GET',
			'endpoint'			=> 'metafields.json'
		),
		// Get Variant Metafields
		'get_variant_metafields'	=> array(
			'method'			=> 'GET',
			'endpoint'			=> 'variants/[shopify_variant_id]/metafields.json',
			'url_params'		=> array( 'shopify_variant_id' )
		),
		// Update/Create Variant Metafields
		'update_variant_metafields'	=> array(
			'method'			=> 'POST',
			'endpoint'			=> 'products/[shopify_product_id]/variants/[shopify_variant_id]/metafields.json',
			'url_params'		=> array( 'shopify_product_id', 'shopify_variant_id' )
		),

		/*
		 * Image Endpoints
		 */

		// Get Images
		'get_images' 				=> array(
			'method'			=> 'GET',
			'endpoint'			=> 'products/[shopify_product_id]/images.json',
			'url_params'		=> array( 'shopify_product_id' ),
			'response_wrap'		=> 'images'
		),
		// Get Single Image
		'get_image' 				=> array(
			'method'			=> 'GET',
			'endpoint'			=> 'products/[shopify_product_id]/images/[shopify_image_id].json',
			'url_params'		=> array( 'shopify_product_id', 'shopify_image_id' ),
			'response_wrap'		=> 'image'
		),
		// Delete Image
		'delete_image' 				=> array(
			'method'			=> 'DELETE',
			'endpoint'			=> 'products/[shopify_product_id]/images/[shopify_image_id].json',
			'url_params'		=> array( 'shopify_product_id', 'shopify_image_id' )
		)	
	);

	public static function request_url( $request_type, $request_data = array(), &$results = false ) {
		$endpoint = static::request_endpoint( $request_type, $request_data, $results );

		return "https://" . static::$api_credentials['api_key'] . ':' . static::$api_credentials['api_password'] . '@' . static::$api_credentials['base_url'] . $endpoint;
	}

	public static function build_curl( $request_url, $request_type, $request_data = array(), &$results = false ) {

		$request_method = static::$request_types[ $request_type ]['method'];

		$curl = curl_init( $request_url );

		//curl_setopt( $curl, CURLINFO_HEADER_OUT, true );
		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $curl, CURLOPT_SSL_VERIFYHOST, false );
		curl_setopt( $curl, CURLOPT_USERPWD, static::$api_credentials['api_key'] . ":" . static::$api_credentials['api_password'] );
		curl_setopt( $curl, CURLOPT_HEADER, false );
		curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, $request_method );

		$headers = array( 'Content-Type: application/json' );

		curl_setopt( $curl, CURLOPT_HTTPHEADER, $headers );
		
		if( 'POST' == $request_method || 'PUT' == $request_method ) {
			$request_body = json_encode( $request_data['request_body'] );
			// ovc_debug( $request_body );
			curl_setopt( $curl, CURLOPT_POSTFIELDS, $request_body );
		}

		return $curl;
	}

	public static function request_throttling() {
		
		// API call throttling
		if( static::$last_api_call ) {
			$last_call_udiff = microtime( true ) - static::$last_api_call;
			if( $last_call_udiff < 1000000 ) {
				usleep( min( 1500000 - $last_call_udiff, 1000000 ) );
			}
		// Wait a second so that jumps don't overwrite
		} else {
			usleep( 1000000 );
		}
	}
}