<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

error_reporting(E_ALL);
ini_set("log_errors", 1); 

class OVC_API_Walmart extends OVC_API_Request {
	
	public static $api_name = 'Walmart API';

	protected static $api_environments = array(
		'live' 	=> array( 
			'base_url'		=> 'https://marketplace.walmartapis.com/',
			/**
			 * OAuth
			 **/
			'client_id'		=> '5e982a50-165f-4bb2-ba3a-17d730b7a9f6',
			'client_secret'	=> 'OE7Rqm_dvOmPzKvcuyV7SsVO_n9g2eB2Nt6Ns7-2ftqptZiP3CCVg5p40t7oKQRn7ks-sIZiRQQGVtUJg4_DRQ',
			/**
			 * @deprecated
			 * Digital Signature
			 **/
			'consumer_id' 	=> '7900dd4d-d6df-4bb5-af96-adca7f3cd6f6',
			'private_key' 	=> 'MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAILQfiub2CF3gU5CBWTx4XGImT0gDUPQERReTpl7c7fsIKsnVlV26lD9q3ylyKJM5KXfDP8mbkgqv7GF0Xhrmh59NsWBOdvGz1ANAh5lD2IOJJANNInu3w/BMEZq7ChyhID40fOK2NPKLWfJrrhKZH4vq7NbgiIuOQ2Q7OFH/uUVAgMBAAECgYBOEzgquPIvT086sQbdL8x5YajsJZQlNCrteDf7PQZc8K6y2GCb08SYID7Yv23XCIOzSEROiNZrdjBBOPhDoh0yVqFF7+iIb3TXNl1M0TwP9iF4QTzAT1ZAWpTJ9Pjg0af9D9Qe9WCfUX4PEWISfiSYI+bunMi+RsZeQ/FPSdO0oQJBANM6EiszN3AsZmzf+KLN9rettgCWqbEnz3JmWG0Q594bNzrr3rzKEo/oS2Y/RpxAX4gMjYiEMM53BRWUDNh7vw0CQQCeivRlkRhTVFZ9jcg65Y4dlkwJFsmg0fMRLNFwLqJqHQyquKYM7FgeCICZfOoBStE7qHoumOWx2gu2ghzpb3wpAkAKGQOXVxp2XVkEKWzcc5Ywz7gcrRctpeaP5H6jsCQ1hP2N7oq2Yb6jbIp2N94rSBSJD3iQpykiJaRzlKFMlBXdAkBfqYh+IGaFIUZuy7Ydk5WE1MNcWJugpg38NwbIodruHEkgWfoXoiQpOwgdlPm9fd9yF95hb3BpOCAJJ8k6MULBAkEAzQ6ROuJMaLf34vjG4YtffVjEdTareOENj2+YFKyI5+rL7qQ4iD0fxrNPkhy04VaGZI9VMKv94ILj0FYJ+vIiuA==',
		),
		'dev' => array(
			'base_url'		=> 'https://sandbox.walmartapis.com/',
			/**
			 * OAuth
			 **/
			'client_id'		=> '976af1c4-5470-4af2-a76e-5de120992789',
			'client_secret'	=> 'LpjQLYrmmHkqP2zqlETk-e3JRwyTjMKO-XuaFcZa6-__U5Jy4whw7M8VX6iwpylI4zueVJnLMyR2XER9MxR58Q',
		
		)
	);


	public static $response_class = 'OVC_API_Response_Walmart';

	public static $request_types = array(

		// Get OAuth token
		'generate_token'	=> array(
			'method'			=> 'POST',
			'endpoint'			=> 'v3/token',
		),

		// Get a single item
		'get_item'			=> array(
			'method'			=> 'GET',
			'endpoint'			=> 'v3/items/[sku]',
			'url_params'		=> array( 'sku' ),
			'response_wrap'		=> 'ItemResponse'
		),
		// Get Items(s) status. Supply feedId for single feed, or [ limit, [ offset ]] for multiple (20 max)
		'get_items'			=> array(
			'method'			=> 'GET',
			'endpoint'			=> 'v3/items',
			'query_params'		=> array( 'sku', 'limit', 'offset' ),
			'response_wrap'		=> 'ItemResponse'
		),
		// Get full report of items as CSV 
		'get_items_report'	=> array(
			'method'			=> 'GET',
			'endpoint'			=> 'v3/getReport?type=item'
		),
		// Get full Competitive Price Adjustment report as CSV
		'get_cpa_report'	=> array(
			'method'			=> 'GET',
			'endpoint'			=> 'v3/getReport?type=cpa'
		),
		// Get a single Item's stock
		'get_item_stock'  	=> array(
			'method'			=> 'GET'
		),
		// Get Feed(s) status. Supply feedId for single feed, or [ limit, [ offset ]] for multiple (50 max)
		'get_feeds_status'	=> array(
			'method'			=> 'GET',
			'endpoint'			=> 'v3/feeds',
			'query_params'		=> array( 'feedId', 'limit', 'offset' )
		),
		// Get a Feed's feed items' statuses.
		'get_feed_items_status'	=> array(
			'method'			=> 'GET',
			'endpoint'			=> 'v3/feeds/[remote_feed_id]',
			'url_params'		=> array( 'remote_feed_id' ),
			'query_params'		=> array( 'includeDetails', 'limit', 'offset' )
		),
		// Retire an item by SKU
		'retire_item'		=> array(
			'method'			=> 'DELETE',
			'endpoint'			=> 'v3/items/[sku]',
			'url_params'		=> array( 'sku' )
		),
		// Bulk create items
		'walmart_bulk_create_items'	=> array(
			'method'			=> 'POST',
			'endpoint'			=> 'v3/feeds?feedType=item'
		),
		// Bulk update SKUs
		'walmart_bulk_update_skus'	=> array(
			'method'			=> 'POST',
			'endpoint'			=> 'v3/feeds?feedType=item'
		),
		// Bulk update items
		'walmart_bulk_update_items'	=> array(
			'method'			=> 'POST',
			'endpoint'			=> 'v3/feeds?feedType=item'
		),
		// Bulk update item prices
		'walmart_bulk_update_prices'=> array(
			'method'			=> 'POST',
			'endpoint'			=> 'v3/feeds?feedType=price'
		),
		'walmart_bulk_update_inventory' => array(
			'method'			=> 'POST',
			'endpoint'			=> 'v3/feeds?feedType=inventory'
		),
		'walmart_bulk_update_inventory_all' => array(
			'method'			=> 'POST',
			'endpoint'			=> 'v3/feeds?feedType=inventory'
		)
	);

	public static function build_curl( $request_url, $request_type, $request_data = array(), &$results = false ) {

		$request_method = static::$request_types[ $request_type ]['method'];

		// $headers = self::build_digital_signature_headers( $request_url, $request_method );
		$headers = self::build_oauth_headers( $request_method );

		$curl = curl_init( $request_url );

		//curl_setopt( $curl, CURLINFO_HEADER_OUT, true );
		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $curl, CURLOPT_SSL_VERIFYHOST, false );
		curl_setopt( $curl, CURLOPT_HEADER, false );

		switch( $request_method ) {
			case 'POST':
				curl_setopt( $curl, CURLOPT_POST, true );
			break;
			case 'PUT':
			case 'DELETE':
				curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, $request_method );
			break;
			default:
			break;
		}

		// Conditionally add xml or request parameters
		$curl_postfields = null;
		if( isset( $request_data['file'] ) && $request_data['file'] ) {
			$curl_postfields = array( 'xml' => new CURLFile( $request_data['file']->path() ) );
		}

		if( isset( $curl_postfields ) ) {
			curl_setopt( $curl, CURLOPT_POSTFIELDS, $curl_postfields );
		}

		curl_setopt( $curl, CURLOPT_HTTPHEADER, $headers );

		return $curl;
	}

	private static function get_headers() {

		$headers = array();
		$headers[] = 'WM_SVC.NAME: Walmart Marketplace';
		$headers[] = 'WM_QOS.CORRELATION_ID: ' . self::correlation_generator();

		return $headers;
	}

	private static function build_oauth_headers( $request_method ) {

		$access_token_response = self::generate_access_token();

		$headers = self::get_headers();
		$headers[] = 'Authorization: Basic ' . base64_encode( static::$api_credentials['client_id'] . ':' . static::$api_credentials['client_secret'] );
		$headers[] = 'WM_SEC.ACCESS_TOKEN: ' . $access_token_response->access_token;
		$headers[] = 'Content-Type: ' . ( 'POST' == $request_method ? 'multipart/form-data' : 'application/xml' );
		$headers[] = 'Accept: application/xml';

		return $headers;
	}

	/**
	 * OAuth Access Token - replaces the old Digital Signature format
	 * 
	 * @return 	string
	 */
	public static function generate_access_token( &$results = false ) {
		OVC_Results::maybe_init( $results );

		static::set_environment();

		$request_type = 'generate_token';
		$request_data = 'grant_type=client_credentials';

		$request_url = static::$api_credentials['base_url'] . static::$request_types[ $request_type ]['endpoint'];

		$headers = self::get_headers();
		$headers[] = 'Authorization: Basic ' . base64_encode( static::$api_credentials['client_id'] . ':' . static::$api_credentials['client_secret'] );
		$headers[] = 'Content-Type: application/x-www-form-urlencoded';
		$headers[] = 'Accept: application/json';

		$curl = curl_init( $request_url );

		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $curl, CURLOPT_SSL_VERIFYHOST, false );
		curl_setopt( $curl, CURLOPT_HEADER, false );
		curl_setopt( $curl, CURLOPT_POST, true );
		curl_setopt( $curl, CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $curl, CURLOPT_POSTFIELDS, $request_data );

		static::$last_api_call = microtime( true );

		$call_result = curl_exec( $curl );
		$curl_info = curl_getinfo( $curl );
		curl_close( $curl );

		$request_type = static::$request_types[ $request_type ];
		$request_type['type'] = $request_type;

		return new OVC_API_Response( $call_result, $curl_info, $request_data, $request_type, $results );
	}

	/**
	 * Build headers for Digital Signature authentication
	 * 
	 * @deprecated 
	 * @see 		https://developer.walmart.com/forums/index.php/releases/walmart-us-marketplace-api-changes
	 * 
	 * @param 		string 	$request_url 
	 * @param 		string 	$request_method 
	 * 
	 * @return 		array
	 */
	private static function build_digital_signature_headers( $request_url, $request_method ) {

		// Generate Signature
		$sigstamp = self::generate_signature( static::$api_credentials['consumer_id'], static::$api_credentials['private_key'], $request_url, $request_method );
		$signature = $sigstamp[0]; //This is our authorized Signature
		$timestamp = $sigstamp[1]; //This is our timestamp

		// Prepare Headers
		$headers = self::get_headers();
		$headers[] = 'WM_SEC.TIMESTAMP: '. $timestamp;
		$headers[] = 'WM_SEC.AUTH_SIGNATURE: ' . $signature;
		$headers[] = 'WM_CONSUMER.ID: ' . static::$api_credentials['consumer_id'];
		$headers[] = 'WM_CONSUMER.CHANNEL.TYPE: ' . '0f3e4dd4-0514-4346-b39d-af0e00ea066d';
		$headers[] = 'Content-Type: ' . ( 'POST' == $request_method ? 'multipart/form-data' : 'application/xml' );
		$headers[] = 'Accept: application/xml';

		return $headers;
	}

	/**
	 * A wrapper to handle generating our authentication key and timestamp using the signature class
	 * This is to ease development so we dont have to interact with our libraries anywhere but here
	 * 
	 * @deprecated 	
	 * @see 		https://developer.walmart.com/forums/index.php/releases/walmart-us-marketplace-api-changes
	 * 
	 * @param 		string 	$consumer_id
	 * @param 		string 	$private_key
	 * @param 		string 	$request_url
	 * @param 		string 	$request_method
	 * 
	 * @return 		array
	 */
	private static function generate_signature( $consumer_id, $private_key, $request_url, $request_method ) {

		@include_once OVC_PATH . '/libraries/phpseclib/Math/BigInteger.php';
		@include_once OVC_PATH . '/libraries/phpseclib/Crypt/RSA.php';
		@include_once OVC_PATH . '/libraries/walmart/Signature.php';

		$authSignature = new Signature( $consumer_id, $private_key, $request_url, $request_method );
		$signatureString = $authSignature->getSignature();

		return $signatureString;
	}

	/**
	 * A random sequence generator to handle our correlation id
	 * 
	 * @param 	int 	$length 
	 * @return 	string
	 */
	private static function correlation_generator( $length = 16 ){

		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen( $characters );
		$randomString = '';

		for( $i = 0; $i < $length; $i++ ) {
			$randomString .= $characters[ rand( 0, $charactersLength - 1 ) ];
		}

		return "vida" . $randomString;
	}

}