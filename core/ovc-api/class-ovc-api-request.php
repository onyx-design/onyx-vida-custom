<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

error_reporting(E_ALL);
ini_set("log_errors", 1); 

class OVC_API_Request {

	public static $api_name = 'OVC API';

	public static $last_api_call = null;

	public static $response_class = 'OVC_API_Response';

	public static $request_types = array();

	protected static $api_credentials = null;

	protected static $api_environments = array();	

	protected static $api_environment_default = 'live';

	public static function set_environment( $environment = null ) {

		// Default to previously set environment
		if( !isset( $environment ) && isset( static::$api_credentials ) ) {
			return static::$api_credentials;
		}

		$environment = $environment ?: ( on_vida_live() ? static::$api_environment_default : 'dev' );

		return static::$api_credentials = ( static::$api_environments[ $environment ] ?? current( static::$api_environments ) );
	}

	public static function request( $request_type, $request_data = array(), &$results = false ) {
		OVC_Results::maybe_init( $results );

		$response_class = static::$response_class;

		$request_url = static::request_url( $request_type, $request_data, $results );

		// Build CURL request object
		$request_curl = static::build_curl( $request_url, $request_type, $request_data, $results );

		// Throttle the first call due to OVCOP jumps being too fast
		if( !static::$last_api_call ) {
			static::$last_api_call = microtime( true );
		}

		// Maybe throttle request
		static::request_throttling();

		static::$last_api_call = microtime( true );

		// Execute request
		$call_result = curl_exec( $request_curl );
		$curl_info = curl_getinfo( $request_curl );
		curl_close( $request_curl );

		$request_type_name = $request_type;
		$request_type = static::$request_types[ $request_type_name ];
		$request_type['type'] = $request_type_name;

		return new $response_class( $call_result, $curl_info, $request_data, $request_type, $results );		
	}

	public static function request_url( $request_type, $request_data = array(), &$results = false ) {

		$endpoint = static::request_endpoint( $request_type, $request_data, $results );

		return static::$api_credentials['base_url'] . $endpoint;
	}

	public static function request_endpoint( $request_type, $request_data = array(), &$results = false ) {
		OVC_Results::maybe_init( $results );
		
		static::set_environment();

		// Initialize and validate $request_type
		$request_template = static::$request_types[ $request_type ] ?? null;

		if( !$request_type || !$request_template ) {
			return $results->error( "Invalid " . static::$api_name . " request type: {$request_type}" );
		}

		$endpoint = $request_template['endpoint'];

		// Maybe add URL params to endpoint
		if( isset( $request_data['url_params'] ) ) {

			foreach( $request_data['url_params'] as $param_key => $param_value ) {

				$endpoint = str_replace( "[{$param_key}]", $param_value, $endpoint, $params_count );
				if( $params_count < 1 ) {
					$results->notice( "{$param_key} not found in URL: {$endpoint}." );
				}
			}
		}

		// Maybe add query params to endpoint	
		if( isset( $request_data['query_params'] ) ) {

			$http_query = http_build_query( $request_data['query_params'] );
			$endpoint .= "?{$http_query}";			
		}

		return $endpoint;
	}

	public static function build_curl( $request_url, $request_type, $request_data = array(), &$results = false ) {

	}

	public static function request_throttling() {
		return true;
	}

	public static function get_api_credentials() {

		return static::$api_credentials;
	}
}