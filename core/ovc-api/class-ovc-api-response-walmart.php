<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_API_Response_Walmart extends OVC_API_Response {

	public static $walmart_error_codes = false;



	/**
	 *	Converts a curl response to an array
	 */
	public function curl_to_array( $xml ){

		if( !( $xml instanceof SimpleXMLElement ) ) {
			$xml = new SimpleXMLElement( $xml );
		}

		$namespaces = $xml->getNameSpaces( true );

		// Add null value to namespaces in order 
		if( !in_array( null, $namespaces, true ) ) {
			$namespaces[] = null;
		}

		return $this->xml_to_array_value( $xml, $namespaces );
	}

	public function xml_to_array_value( $xml, $namespaces = false ) {

		$xml_array_value = false;

		// Determine if there are children
		foreach( $namespaces as $namespace ) {

			if( count( $xml->children( $namespace ) ) ) {

				$xml_array_value = array();
				break;
			}
		}

		// Loop through children if found, otherwise 
		if( is_array( $xml_array_value ) ) {

			foreach( $namespaces as $namespace ) {

				foreach( $xml->children( $namespace ) as $key => $child ) {

					if( array_key_exists( $key, $xml_array_value ) ) {

						if( !is_array( $xml_array_value[ $key ] )
							|| !array_key_exists( 0, $xml_array_value[ $key ] )
						) {


							$xml_array_value[ $key ] = array( $xml_array_value[ $key ] );
						}

						$xml_array_value[ $key ][] = $this->xml_to_array_value( $child, $namespaces );
					} else {
						$xml_array_value[ $key ] = $this->xml_to_array_value( $child, $namespaces );
					}
				}
			}
		} else {
			$xml_array_value = (string) $xml;
		}
		
		return $xml_array_value;	
	}

	/**
	 * WALMART FEED ITEM ERROR CODE HANDLING
	 */
	public static function init_error_codes( $force_refresh = false ) {

		if( !self::$walmart_error_codes || $force_refresh ) {

			global $wpdb;

			self::$walmart_error_codes = $wpdb->get_results( "SELECT * FROM wp_ovc_walmart_error_codes", ARRAY_A );
		}

		return self::$walmart_error_codes;
	}

	public static function maybe_add_error_code( $type, $code, $description ) {
		self::init_error_codes();

		// Check if error code exists
		if( !in_array( $code, array_column( self::$walmart_error_codes, 'code' ) ) ) {

			global $wpdb;

			$error_code_data = array(
				'type'			=> $type,
				'code'			=> $code,
				'description'	=> $description
			);

			$wpdb->insert( 'wp_ovc_walmart_error_codes', $error_code_data, '%s' );

			self::init_error_codes( true );
		}
	}

	public static function get_error_id_by_code( $error_code ) {
		self::init_error_codes();

		foreach( self::$walmart_error_codes as $error_code_info ) {

			if( $error_code == $error_code_info['code'] ) {
				return $error_code_info['ID'];
			}
		}

		return false;
	}
}