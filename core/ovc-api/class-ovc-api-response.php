<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_API_Response {

	protected $response_raw 	= null;

	protected $response_data 	= null;

	protected $response_info 	= null;

	protected $request_type		= null;

	protected $request_data 	= null;

	protected $content_type 	= null;

	protected $results;

	public function __construct( $response_raw = null, $response_info = null, $request_data = null, $request_type = null, &$results = false ) {

		$this->response_raw		= $response_raw;
		$this->response_info 	= $response_info;
		$this->request_type 	= $request_type;
		$this->request_data 	= $request_data;

		$this->results 			= OVC_Results::maybe_init( $results );
		
		if( ovc_debug()->is_active() ) {
			ovc_debug($this);
		}
	}

	public function __get( $name ) {

		$this->format();

		if( 'xml' == $this->type() 
			&& is_array( $this->response_data ) 
			&& array_key_exists( $name, $this->response_data ) 
		) {

			return $this->response_data[ $name ];
		}
		else if( 
			'json' == $this->type()
			&& is_object( $this->response_data )
			&& property_exists( $this->response_data, $name ) 
		) {

			return $this->response_data->$name;
		}

		return null;
	}


	public function format( $format = 'auto' ) { //dev:improve

		switch( $format ) {
			case 'raw':
				return $this->response_raw;
			break;
			case 'xml':

				if( !isset( $this->response_data ) ) {
					$this->response_data = $this->curl_to_array( $this->response_raw );

					if( isset( $this->request_type['response_wrap'] )
						&& array_key_exists( $this->request_type['response_wrap'], $this->response_data ) 
					) {
						$response_wrap = $this->request_type['response_wrap'];
						$this->response_data = $this->response_data[ $response_wrap ];
					}
				}

				return $this->response_data;
			break;
			case 'json':

				if( !isset( $this->response_data ) ) {
					$this->response_data = json_decode( $this->response_raw );

					if( isset( $this->request_type['response_wrap'] )
						&& property_exists( $this->response_data, $this->request_type['response_wrap'] ) 
					) {
						$response_wrap = $this->request_type['response_wrap'];
						$this->response_data = $this->response_data->$response_wrap;
					}
				}

				return $this->response_data;
			case 'auto':
			default:
				return $this->format( $this->type() );			
			break;
		}

	}

	public function data( ...$keys ) {

		$data = $this->format();

		foreach( $keys as $key ) {

			$data = property_exists( $data, $key ) ? $data->$key : null;

			if( !isset( $data ) ) {
				break;
			}
		}

		return $data;
	}

	public function succeeded() {
		return ('2' == substr( $this->info( 'http_code' ), 0, 1 ) );
	}

	public function get_errors() {
		return (array) $this->data( 'errors' );
		
	}

	public function type( $force_refresh = false ) {

		if( !isset( $this->content_type ) || $force_refresh ) {

			$content_type = (string) $this->info( 'content_type' );

			if( false !== strpos( $content_type, 'application/json' ) ) {
				$this->content_type = 'json';
			}
			else if( false !== strpos( $content_type, 'application/xml' ) ) {
				$this->content_type = 'xml';
			}
		}

		return $this->content_type;
	}

	public function info( $key = null ) {

		if( empty( $key ) ) {
			return $this->response_info;
		}
		else if( array_key_exists( $key, $this->response_info ) ) {
			return $this->response_info[ $key ];
		}
	}

	public function request_data( $key = null ) {

		if( empty( $key ) ) {
			return $this->request_data;
		}
		else if( array_key_exists( $key, $this->request_data ) ) {
			return $this->request_data[ $key ];
		}
	}

	public function request_type( $key = null ) {

		if( empty( $key ) ) {
			return $this->request_type;
		}
		else if( array_key_exists( $key, $this->request_type ) ) {
			return $this->request_type[ $key ];
		}
	}

	/**
	*	Converts a curl response to an array
	*/
	public function curl_to_array( $xml ){



		if( !( $xml instanceof SimpleXMLElement ) ) {
			$xml = new SimpleXMLElement( $xml );
		}

		//$xml_object = new SimpleXMLElement( $xml );

		$namespaces = $xml->getNameSpaces(true);
		// Add null value to namespaces in order 
		if( !in_array( null, $namespaces, true ) ) {
			$namespaces[] = null;
		}

		//print_r( $namespaces );

		return $this->xml_to_array_value( $xml, $namespaces );

	}

	public function xml_to_array_value( $xml, $namespaces = false ) {

		$xml_array_value = false;

		// Determine if there are children
		foreach( $namespaces as $namespace ) {

			if( count( $xml->children( $namespace ) ) ) {

				$xml_array_value = array();
				break;
			}
		}


		// Loop through children if found, otherwise 
		if( is_array( $xml_array_value ) ) {

			foreach( $namespaces as $namespace ) {

				foreach( $xml->children( $namespace ) as $key => $child ) {

					if( array_key_exists( $key, $xml_array_value ) ) {



						if( !is_array( $xml_array_value[ $key ] )
							|| !array_key_exists( 0, $xml_array_value[ $key ] )
						) {


							$xml_array_value[ $key ] = array( $xml_array_value[ $key ] );
						}

						$xml_array_value[ $key ][] = $this->xml_to_array_value( $child, $namespaces );
					}
					else {
						$xml_array_value[ $key ] = $this->xml_to_array_value( $child, $namespaces );
					}
				}
			}
		}
		else {
			$xml_array_value = (string) $xml;
		}
		
		return $xml_array_value;
		
	}

	public static function get_metafield( $data, $namespace, $key ) {

		if( is_object( $data ) && property_exists( $data, 'metafields' ) ) {
			$data = $data->metafields;
		}

		if( is_array( $data ) ) {
			foreach( $data as $metafield ) {
				if( property_exists( $metafield, 'namespace' ) 
					&& $namespace == $metafield->namespace
					&& property_exists( $metafield, 'key' )
					&& $key == $metafield->key 
					&& property_exists( $metafield, 'value' )
				) {
					$metafield_value = $metafield->value;

					if( property_exists( $metafield, 'value_type' )
						&& 'integer' == $metafield->value_type 
					) {
						$metafield_value = intval( $metafield->value );
					}

					return $metafield_value;
				}
			}
		}
	}

}