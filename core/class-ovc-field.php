<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Field {



	protected $local_name 	= false;
	protected $global_name 	= false;

	protected $field_set 	= false;

	protected $meta 		= false;


	/**
	 * Initialize the field object
	 * 
	 * @param string 	$ovc_field field name in full ovc format
	 **/
	public function __construct( OVCFS $field_set, $field_name_local, $meta ) {

		$this->field_set = $field_set;


		$this->local_name = $field_name_local;
		$this->global_name = "{$this->field_set->alias}.{$this->local_name}";

		$this->meta = $meta;


	}


	public function __get( $var_name ) {

		if( property_exists( $this, $var_name ) ) {
			return $this->$var_name;
		}
		else if( method_exists( $this, $var_name ) ) {
			return $this->$var_name();
		}
		else if( 'fs_alias' == $var_name ) {
			return $this->field_set->alias;
		}
		else if( 'field_type' == $var_name ) {
			return $this->field_set->fs_meta( 'type' );
		}
		else if( $meta_value = $this->meta( $var_name ) ) {
			return $meta_value;
		}

	}

	public function __toString() {
		return $this->local_name;
	}


	/**
	 * Get field's var_type
	 * 
	 * @return string 
	 **/
	public function var_type() {

		$var_type = $this->meta( 'var_type' );
		return ( $var_type ? $var_type : 'string' );
	}


	/**
	 * Get field's var_type
	 * 
	 * @return string 
	 **/
	public function field_type() {

		$var_type = $this->meta( 'var_type' );
		return ( $var_type ? $var_type : 'string' );
	}


	/**
	 * Get table name from field's field set
	 * 
	 * @return string
	 **/
	public function table_name() {

		return $this->field_set->table_name();
	}
	
	/**
	 * Get field's nice name
	 * 
	 * @return string
	 **/
	public function nice_name() {

		$nice_name = $this->meta( 'nice_name' );
		return $nice_name ? $nice_name : $this->local_name;
	}

	public function meta( $meta_key = null ) {

		if( !isset( $meta_key ) ) {
			return $this->meta;
		}
		else if( isset( $this->meta[ $meta_key ] ) ) {
			return $this->meta[ $meta_key ];
		}

		return null;
		//return OVCSC::get_field_meta( $this->field_set, $this->local_name, $meta_key );
	}

	public function wpdb_format() {
		$wpdb_format = '%s';

		if( in_array( $this->var_type, array( 'int', 'bool') ) ) {
			$wpdb_format = '%d';
		}
		else if( in_array( $this->var_type, array( 'price', 'stock', 'float', 'float6', 'volume' ) ) ) {
			$wpdb_format = '%f';
		}

		return $wpdb_format;
	}



	public function current_user_can_edit() {
		global $current_user;
		
		if( is_null( $this->meta( 'edit_access' ) )
			|| in_array( 'administrator', $current_user->roles )
			|| array_intersect( (array) $this->meta( 'edit_access' ), $current_user->roles )
			// Or if the OVC_CRON is set
			|| ( OVC_CRON && OVC_CRON == true )
			|| ( !empty( $_POST['ovc_cron'] ) && $_POST['ovc_cron'] == true )
		) {

			return true;
		}

		return false;
	}

	// Determine if current user can view this table (view_access) // dev:optimize
	public function current_user_can_view() {
		global $current_user;

		if( is_null( $this->meta( 'view_access' ) )
			|| in_array( 'administrator', $current_user->roles )
			|| array_intersect( array_merge( (array) $this->meta( 'edit_access' ), (array) $this->meta( 'view_access' ) ), $current_user->roles )
		) {
			return true;
		}

		return false;
	}



}