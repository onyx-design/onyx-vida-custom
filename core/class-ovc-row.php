<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Row {

	protected $field_set = null;

	protected $ID = null;

	protected $local_data = array();

	protected $new_data = array();

	protected $joined_data = array();


	public function __construct( OVCFS $field_set, $row_id = null, $row_data = array() ) {

		$this->field_set = $field_set;

		// If $row_id is negative, this is a new row
		$this->ID = $row_id;

		// Init new data
		$this->reset_new_data();

		if( $this->exists() ) {

			$this->local_data = $row_data;	
		}
		else {
			$this->set_new_data( $row_data );
		}
	}

	public function update_local_data( $local_data ) {

		//dev:improve //dev:validation
		$this->local_data = $local_data;
	}


	public function __toString() {
		return get_class( $this );
	}

	/**
	 * DATA READ METHODS
	 ******************************************/

	public function __get( $name ) {

		if( property_exists( $this, $name ) ) {
			return $this->$name;
		}
		else if( 'id' == $name || 'ID' == $name ) {
			return $this->ID; //return ( !empty($this->ID) && $this->ID > 0 ) ? $this->ID : null;
		}
		else if( 'table' == $name ) {
			return $this->field_set->table_name();
		}
		else if( isset( $this->local_data[ $name ] ) ) {
			return $this->local_data[ $name ];
		}
	}

	public function __set( $name, $value ) {

		if( 'ID' == $name && !$this->exists() && is_numeric( $value ) ) {
			$this->ID = (int) $value;
		}
	}

	public function data( $field = '' ) {
		if( !$this->field_set 
			|| !$field 
			|| !( OVCSC::init_field( $field ) 
					|| OVCSC::init_field( $field, $this->field_set )
				)
		) {
			return null;
		}

		if( $data_set = $this->data_set( $field->field_set ) ) {

			if( isset( $data_set[ $field->local_name ] ) ) {

				return $data_set[ $field->local_name ];
			}
		}		

		return null;
	}

	/**
	 * Retrieve local or foreign data sets
	 * 
	 * 
	 * 
	 **/
	public function data_set( $reference = false, $use_global_keys = false ) {

		$data_set = null;

		// Determine field_set from $reference
		if( false === $reference ) {
			$reference = $this->field_set;
		}		

		if( $field_set = OVCSC::get_field_set( $reference ) ) {

			// If local field set is requested, return local data
			if( $this->field_set->is_local( $field_set ) ) {

				$data_set = $this->local_data;
			}
			// Otherwise we're returning joined data
			else if( $data_set = $this->get_joined_data( $field_set ) ) {

				// If it's a parent join or 1:1 cardinality join, just return the local data of the attached row
				if( $data_set instanceof OVC_Row ) {

					$data_set = $data_set->data_set( false, $use_global_keys );
				}
				// This means the requested field set is a child join with multiple rows potentially
				else {

					return $data_set;
				}
			}

			if( $use_global_keys ) {

				// Add fs prefix to field keys
				$fs_alias = $field_set->alias;

				foreach( $data_set as $key => $value ) {
					
					$data_set[ "{$fs_alias}.{$key}" ] = $value;
					unset( $data_set[ $key ] );
				}
			}
		}

		return $data_set;		
	}

	public function reload() {

		if( $this->exists() ) {

			$row_data = OVCDB()->get_table_row_by_id( $this->ID, $this->field_set->pKey, $this->field_set->table_name() );

			if( $row_data ) {

				$this->local_data = $row_data;
			}
			else {

				$this->local_data = array();
				$this->ID = null;
			}
		}

		return $this;
	}

	/**
	 * Initializes joined row(s) from related tables
	 * 
	 * @param OVCFS 		$joined_fs
	 * @param bool 			$force_refresh
	 * 
	 * @return array
	 **/
	public function get_joined_data( $joined_fs, $results = false, $force_refresh = false ) {
		OVC_Results::maybe_init( $results );
		OVCSC::init_field_set( $joined_fs );

		if( $this->field_set->is_local( $joined_fs ) ) {

			return $this;
		}
		else if( $force_refresh || !isset( $this->joined_data[ $joined_fs->name ] ) ) {

			$this->joined_data[ $joined_fs->name ] = OVCDB()->get_joined_rows( $this, $joined_fs, $results, $force_refresh );

		}

		if( isset( $this->joined_data[ $joined_fs->name ] ) ) {
			return $this->joined_data[ $joined_fs->name ];
		}
	}


	public function get_children( $field_set, $return_type = 'object', $results = false, $filter = true, $force_refresh = false ) {
		OVC_Results::maybe_init( $results );

		$field_set = OVCSC::get_field_set( $field_set, 'database' );

		if( !$field_set ) {
			return $results->error( "OVC_Row->get_children() error: Invalid field set sent." );
		}
		else if( 'child' != $this->field_set->get_ref_relation( $field_set ) ) {
			return $results->error( "Cannot use get_children() - {$field_set->name} is not a child relation to {$this->field_set->name}" );
		}
		else {

			$joined_children = $this->get_joined_data( $field_set, $results, $force_refresh );

			// Determine Filters
			$filter = true === $filter ? $field_set->fs_meta( 'default_get_children_filter' ) : $filter;

			// Shortcut to skip processing if no filters and return_type == 'object'
			if( empty( $filter ) && 'object' == $return_type ) {
				return $joined_children;
			}

			// Build an array of children
			$children = array();

			foreach( $joined_children as $index => $child_object ) {

				// $raw_child_data = $child_object->data_set( $field_set );

				// Maybe skip based on $filter
				if( $filter ) {

					$child_filter = new OVC_Condition_Set( $filter, $child_object, $results, false );

					if( !$child_filter->result ) {
						continue;
					}
				}

				// Return correct type of data
				if( 'object' == $return_type ) {
					$children[] = $child_object;
				}
				else if( 'raw' == $return_type || 'raw_data' == $return_type ) {
					$children[] = $child_object->data_set( $field_set, true );
				}
				else if( is_subclass_of( $return_type, $field_set->get_row_class() ) ) {

					$children[] = new $return_type( $field_set, $child_object->ID, $child_object->data_set() );
				}
				else if( $field_set->has_field( $return_type ) ) {

					$children[] = $child_object->data( $return_type );
				}
				else {
					return $results->error( "OVC_Row->get_children() error: Invalid return_type ( {$return_type} ) for external field set {$field_set->name}" );
				}
			}

			return $children;
		}
	}


	
	public function matches_where( $where = array() ) {
		return OVCDB()->row_matches_where( $this, $where );
	}
	

	public function get_external_value( $field, $results = false ) {
		OVC_Results::maybe_init( $results );
		
		if( !OVCSC::init_field( $field ) ) {
			return $results->error( "Cannot get external value: Invalid field: {$field}" );
		}

		$default_value = $field->meta('default_value');

		$external_value = '';
		if( $filter_func = $field->meta('filter_func') ) {

			// If this class has this filter_func method, use this method non-statically
			if( method_exists( $this, $filter_func ) ) {

				$external_value = $this->$filter_func();
			}
			// Else check for a static method on OVC_External or another specified external class
			else {

				$class = method_exists( $this, $filter_func ) ? get_class( $this ) : oxd( $field->field_set->fs_meta( 'classname' ), "OVC_External" );

				if( method_exists( $class, $filter_func ) ) {
					
					$external_value = $class::$filter_func( $this );
				}
				else {
					return $results->error( "Invalid classname::filter_func ( {$class}::{$filter_func} ) for field {$field->global_name}" );
				}
			}
		}
		else if( $ovc_field = $field->meta('ovc_field') ) {
			
			// Check for output filters
			$filters = null;
			if( false !== strpos( $ovc_field, '||' ) ) {

				$ovc_field_meta = explode( '||', $ovc_field );
				$ovc_field = $ovc_field_meta[0];
				$filters = $ovc_field_meta[1];
			}

			$external_value = $this->data( $ovc_field );

			if( isset( $filters ) ) {

				foreach( (array) explode( '|', $filters ) as $filter ) {

					// Check for filter variables
					$filter_value = null;

					if( strpos( $filter, ':' ) ) {

						$filter_info = explode( ':', $filter );
						$filter = $filter_info[0];
						$filter_value = $filter_info[1];
					}

					switch ( $filter ) {
						case 'price':
							$external_value = "$" . number_format( $external_value, 2, '.', '' );
						break;
						case 'decimal':
							$decimal_places = $filter_value ?? 2;
							$external_value = number_format( $external_value, $decimal_places, '.', '' );
						break;
						case 'date':
							$external_value = date( $filter_value, strtotime( $external_value ) );
						break;
						case 'maxlength':
							$external_value = (string) substr( $external_value, 0, intval( $filter_value ) );
						break;
						default:
							if( function_exists( $filter ) ) {
								$external_value = $filter( $external_value );
							}
						break;
					}
				}
			}
		}
		else if( isset( $default_value ) ) {
			$external_value = $field->meta('default_value');
		}

		// Additional processing from special field_meta //dev:improve - this should be combined with the filters somehow
		if( is_numeric( $field->meta('maxlength') ) && strlen( $external_value ) > $field->meta('maxlength') ) {
			$external_value = (string) substr( $external_value, 0, intval( $field->meta( 'maxlength' ) ) );
		}

		return $external_value;
	}

	public function get_external_data_set( $external_field_set, $results = false ) {
		OVC_Results::maybe_init( $results );
		OVCSC::init_field_set( $external_field_set );

		$data_set = array();

		foreach( $external_field_set->get_fields() as $field ) {

			$external_value = $this->get_external_value( $field, $results );

			if( !empty( $external_value ) || !$field->meta( 'ignore_if_false' ) ) {

				$data_set[ $field->local_name ] = $external_value;
			}
		}

		return $data_set;
	}



	/**
	 * DATA UPDATING METHODS
	 ******************************************/

	public function set_new_data( $data = array(), $results = false ) {
		OVC_Results::maybe_init( $results );

		// Make sure $this->new data is always at least initialized with a blank array for the local field set
		if( !isset( $this->new_data[ $this->field_set->alias ] ) ) {
			$this->reset_new_data();
		}

		foreach( $data as $field_name => $value ) {

			// Initialize the field
			if( OVCSC::field_name_is_global( $field_name ) ) {
				$field = OVCSC::get_field( $field_name );
			}
			else {
				// If field name is local, assume it's part of this row's field set
				$field = OVCSC::get_field( $field_name, $this->field_set );
			}
			
			// Check that this is a valid field
			if( $field ) {

				// If this is not a local field, verify that it's a valid join for updating
				if( !$this->field_set->is_local( $field ) 
					&& !isset( $this->new_data[ $field->fs_alias ] ) // if this is set, we've already passed these checks for this joined field set
				) {
					
					$join = $this->field_set->get_join( $field->field_set );
					$ref_relation = $this->field_set->get_ref_relation( $field->field_set );

					if( !$join ) {
						// Join doesn't exist
						$results->notice( "WARNING! Invalid join (no join): Cannot update data in {$field->field_set->name} from {$this->field_set->name}" );
						continue;
					}
					else if( 'child' == $ref_relation && '11' != $join->cardinality ) {
						// Can't update child fields from parent on 1 to Many join
						$results->notice( "WARNING! Invalid join (cardinality): Cannot update data in {$field->field_set->name} from {$this->field_set->name}" );
						continue;
					}
				}

				// Maybe initialize the updated data for this field set
				if( !isset( $this->new_data[ $field->fs_alias ] ) ) {
					$this->new_data[ $field->fs_alias ] = array();
				}

				$this->new_data[ $field->fs_alias ][ $field->local_name ] = OVCDB()->sanitize_value_by_field( $value, $field ); // dev: should we be sanitizing here?
			}
			else {
				$results->notice( "WARNING! Invalid field in updated data set: {$field_name}" );
			}
		}

		return true;
	}

	public function reset_new_data() {

		$this->new_data = array(
			"{$this->field_set->alias}" => array()
		);
	}


	public function update_data( $data = array(), $results = false, $reset_new_data = false ) {
		OVC_Results::maybe_init( $results );

		if( $reset_new_data ) {
			$this->reset_new_data();
		}
		
		if( !$this->set_new_data( $data, $results ) ) {
			return false;
		}

		// Sort $data into field sets
		$rows_to_update = array();

		// Add updated data arrays to relevant OVC_Row objects (local or external)
		foreach( $this->new_data as $field_set_alias => $updated_data_for_fs ) {

			$rows_to_update[ $field_set_alias ] = $this->get_joined_data( $field_set_alias, $results );

			$rows_to_update[ $field_set_alias ]->set_new_data( $updated_data_for_fs, $results );
		}

		// Check if we are updating multiple field sets
		if( count( $rows_to_update ) > 1 ) {

			foreach( $rows_to_update as $field_set_alias => $row_to_update ) {

				$update_field_set = $row_to_update->field_set;

				// Check if this updated data is external
				if( !$this->field_set->is_local( $update_field_set ) ) {

					$join = $this->field_set->get_join( $update_field_set );
					$ref_relation = $this->field_set->get_ref_relation( $update_field_set );

					// Check if external field set is a parent
					if( 'parent' == $ref_relation
						&& $this->is_updating()
						&& ( $this->is_field_changed( $join->child_field )
							|| $this->is_field_changed( $join->parent_field )
							)
					) {
						return $results->error( "Cannot update a foreign key field ({$update_field_set->name}) on either side of a join when both sides of the join are present in the updated_data. Join ID: {$join->join_id}." );
					}
				}
			}
		}
		
		// Attempt updates
		// dev:improve should this be done in a certain order? what about fields that affect / rely on external data? (oms_release_code, )
		foreach( $rows_to_update as $row_to_update ) {

			if( !$row_to_update->save( $results ) ) {
				break;
			}
		}		
		
		return !$results->has_error;
	}

	public function save( $results = false ) {
		OVC_Results::maybe_init( $results );

		if( OVCDB()->save_row( $this, $results ) ) {

			// If any foreign key fields were updated, unset any of their joined data
			$foreign_keys = $this->field_set->get_single_meta_key_values( 'foreign_key' );

			foreach( $foreign_keys as $local_field_name => $foreign_key_field ) {

				if( $this->is_field_changed( $local_field_name, $this->field_set ) ) {

					$joined_fs = OVCSC::get_field_set( $foreign_key_field );

					unset( $this->joined_data[ $joined_fs->alias ] );
				}
			}

			// Merge new data in with local_data //dev:improve Do we need to reload this data?
			$this->local_data = array_merge( $this->local_data, $this->new_data[ $this->field_set->alias ] );

			// This should only affect newly created rows
			if( $this->exists() ) {
				$this->ID = $this->data( 'ID' );	
			}
			
			// Reset $this->new_data
			$this->reset_new_data();

			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Set / Update a single data field
	 * 
	 * @param mixed 			$field
	 * @param mixed 			$value
	 * @param OVC_Results 		$results
	 * 
	 * @return bool
	 **/
	public function update_field( $field, $value, $results = false ) {
		OVC_Results::maybe_init( $results );
		OVCSC::init_field( $field );

		return $this->update_data( array( $field->global_name => $value ), $results );
	}



	public function is_updating() {
		return (bool) count( $this->new_data );
	}

	public function new_value_strict( $field, $field_set = null ) {

		if( !isset( $field_set ) && $this->field_set->is_local( $field ) ) {

			$field_set = $this->field_set;
		}

		if( $field = OVCSC::get_field( $field, $field_set ) ) {

			if( isset( $this->new_data[ $field->fs_alias ] ) ) {

				return $this->new_data[ $field->fs_alias ][ $field->local_name ] ?? null;
			}
		}

		return null;
	}

	// Return the expected new value 
	public function new_value( $field, $field_set = null ) {

		if( !isset( $field_set ) && $this->field_set->is_local( $field ) ) {

			$field_set = $this->field_set;
		}

		if( $field = OVCSC::get_field( $field, $field_set ) ) {

			if( isset( $this->new_data[ $field->fs_alias ] ) ) {

				return $this->new_data[ $field->fs_alias ][ $field->local_name ]
					?? $this->data( $field->global_name );
			}
			else {

				return $this->data( $field->global_name );
			}
		}

		return null;
	}

	public function is_field_changed( $field, $field_set = null ) {

		if( !isset( $field_set ) && $this->field_set->is_local( $field ) ) {

			$field_set = $this->field_set;
		}

		if( $field = OVCSC::get_field( $field, $field_set ) ) {


			return !( $this->new_value( $field, $field_set ) == $this->data( $field ) );
		}

		return false;
	}

	public function get_full_row_new_values( $only_new_values_if_new_row = true ) {

		return 
			( $only_new_values_if_new_row && !$this->exists() )
			? (array) $this->new_data[ $this->field_set->alias ]
			: array_merge( (array) $this->data_set(), (array) $this->new_data[ $this->field_set->alias ] );
	}


	/**
	 * DELETE METHODS
	 ******************************************/

	public function can_delete() {
		if( !$this->exists() ) {
			return false;
		}

		global $current_user;

		$roles_can_delete = (array) $this->field_set->fs_meta( 'delete_access' );
		
		return (bool) array_intersect( $roles_can_delete, $current_user->roles );
	}

	public function delete( $results = false, $force_delete = false ) {
		OVC_Results::maybe_init( $results );

		// If row doesn't exist, return 0 (zero rows deleted)
		if( !$this->exists() ) {
			return 0;
		}

		if( method_exists( $this, 'before_delete' ) ) {
			$this->before_delete( $results );
		}

		if( $force_delete || $this->can_delete() ) {

			return OVCDB()->delete_row( $this, $results );
		}

		return false;
	}

	/**
	 * ADDITIONAL ROW ACTIONS METHODS
	 ******************************************/

	public function can_duplicate() {

		if( !$this->exists() ) {
			return false;
		}

		global $current_user;

		$roles_can_duplicate = (array) $this->field_set->fs_meta( 'duplicate_access' );
		
		return (bool) array_intersect( $roles_can_duplicate, $current_user->roles );
	}


	/**
	 * GENERAL HELPER METHODS
	 ******************************************/

	/**
	 * Check if row exists in database
	 * 
	 * @return bool
	 **/
	public function exists() {
		return !empty( $this->ID ) && $this->ID > 0;
	}


	public function is_new() {
		return !empty( $this->ID ) && $this->ID < 0;
	}
}
