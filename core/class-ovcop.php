<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

ini_set("auto_detect_line_endings", true); // Ensure proper reading of csv files
ini_set("memory_limit", '512M' );
ini_set("max_execution_time", 300 );

ini_set( "error_reporting", E_ALL ); //dev:debug // MOVE TO OVC_Debug class?

//session_write_close();  // Allows simultaneous asynchronous script execution and database calls

class OVCOP {

	public $ID = 0;
	public $type = '';
	public $status;
	
	public $start_time;
	public $end_time;
	public $last_seen;
	public $user_id = 0;

	public $files = array();
	protected $log_file_id;

	public $jumped = false;

	protected static $running_op = false;
	
	//protected $optype_meta_fields = false; // dev:ovcsc

	public static $valid_op_types = array( 'oms_export', 'oms_import', 'zulily_export', 'groupon_export', 'amazon_export', 'dropship_export', 'wc_sync', 'walmart_sync', 'shopify_sync', 'ovc_data_scan', 'oms_inventory', 'sku_change', 'global_sku_change_export', 'analyze_images', 'upc_import', 'ovc_inventory_export', 'ovc_import', 'woot_export' );

	public static $multi_queue_ops = array( 'oms_export', 'zulily_export', 'groupon_export', 'amazon_export', 'dropship_export', 'global_sku_change_export', 'ovc_inventory_export', 'woot_export' );

	// OVCOP Statuses

	public static $queued_statuses = array(
		'config', 				// Operation configuration is partially complete ( mainly for OMS Import because config can take multiple steps )
		'queued'				// Operation is setup and queued to start
	);

	public static $running_statuses = array(
		'running',				// Operation is running (only one operation can be running at any given moment)
		'jumping',				// Operation is currently jumping (if no error has occured, the operation is still in progress)
		'user_abort_requested'	// User has aborted the operation, which should be honored
	);

	public static $finished_statuses = array(
		'complete',				// Operation completed successfully
		'cancelled',			// Operation was cancelled while it was queued (cancelled before it began)
		'aborted',				// Operation aborted itself due to error
		'user_aborted',			// Operation was aborted by user
		'reset',				// Operation status was forcefully reset
		'aborted_after_reset'	// Operation detected that it was reset and aborted itself
	);

	protected $op_data = array(
		'config' => array(),
		'meta'	=> array(),
		'data'	=> array(),
		'stats'	=> array()
	);

	public $request;

	public $result_sets = array();
	public $results;
	public $response_data = array();

	public $use_log_prefix = false;

	public $last_status_check = false;

	// DEPRECATED // dev:ovcop_files
	//public $log_file = '';
	//public $csv_file = '';
	//protected $log_stream = false;

	// Pre-constructor function that will determine whether a child class should be called
	public static function constructify( $request, $args = array() ) {
		//$child_class_requests = array( 'init_source_file', 'commence_operation', 'jump_operation', 'ping_operation', 'user_abort', 'start_from_queue' ); //dev:opqueue
		$child_class_requests = array( 'commence_operation', 'user_abort' );
		
		if( in_array( $request, $child_class_requests ) ) {

			$classname = OVCSC::get_meta_value( 'ovcop', $args['type'], 'classname' );
		}
		else {
			$classname = 'OVCOP';
		}


		$ovcop = new $classname( $request, $args );
		
		return $ovcop;
	}

	// OVCOP Parent Class constructor
	public function __construct( $ID ) {
		$this->results = new OVC_Results();

		$this->ID = $ID;
		$this->unpack_op();

		/*
		if( 'init_operation' == $request ) {


		}
		else if( method_exists( $this, $request) ) {
			$this->request = $request;
			$this->$request( $args );
			$this->ajax_response();
		}
		else {
			$this->results->main_msg = "Invalid OVCOP request type: {$request}";
			$this->results->success = false;
		}
		*/
	}

	// Shared initialization for child classes
	protected function child_construct( $request, $args ) {
		session_write_close(); // Allows for asynchronous execution and db calls

		$this->results = new OVC_Results();
		$this->request = $request;
		$this->ID = $args['ID'];
		$this->unpack_op();

		// Verify Request
		$this->results->success = false;
		if ( ! $this->optype_meta() ) {
			
			$this->results->main_msg = "Invalid OVCOP type requested: {$this->type}";
			$this->results->success = false;
		}
		else if ( 'user_abort' == $this->request ) {

			$this->user_abort();
		}
		else if ( 
			'commence_operation' == $this->request 
			&& in_array( $this->status, array( 'queued', 'jumping', 'user_abort_requested' ) )
		) {

			$this->check_user_abort( true );

			if( !empty( $this->user_id ) ) {
				wp_set_current_user( $this->user_id );
			}

			// Check if we are landing a jump
			if( 'jumping' == $this->status ) {

				$this->jumped = true;
				$log_verb = 'Jump landed. Resuming';
			}
			// Otherwise we are commencing the operation for the first time
			else if( 'queued' == $this->status ) {

				$this->jumped = false;
				$log_verb = 'Commencing';
			}

			$this->set_status('running');
			self::$running_op = $this->ID;
			$this->log( "{$log_verb} {$this->type} operation." );

			return $this->results->success = true;
		}
	}



	public function __call( $name, $args ) {
		
		if( array_key_exists( $name, $this->op_data ) ) {
			
			// Set an op_data magic property value
			if( 2 == count( $args ) ) {
				if( null === $args[1] ) {
					unset( $this->op_data[ $name ][ $args[0] ] );
				}
				else {
					$this->op_data[ $name ][ $args[0] ] = $args[ 1 ];
				}
				return true;
			}
			// Get an op_data magic property value (or null if not exists)
			else if ( 1 == count( $args ) ) {

				return $this->op_data[ $name ][ $args[0] ] ?? null;
			}
			// Get entire array of op_data magic property category
			else if ( !count( $args ) ) {

				return $this->op_data[ $name ];
			}
		}
	}

	public function __get( $name ) {
		if( array_key_exists( $name, $this->op_data['data'] ) ) {
			return $this->op_data['data'][ $name ];
		}
		return null;
	}

	public function __set( $name, $value ) {
		$this->op_data['data'][ $name ] = $value;
	}

	public static function get_running_ovcop_id() {
		return self::$running_op;
	}

	// Auto-initialize and retrieve ovcop fieldmeta for this operation type
	public function optype_meta( $key = null ) {

		return OVCSC::get_field_meta( 'ovcop', $this->type, $key );

	}

	public function user_abort() {

		
		$this->log( "User Abort Requested..." );

		if ( in_array( $this->status, OVCOP::$finished_statuses ) ) {

			$this->results->main_msg = "Cannot abort: Operation already complete.";
			$this->results->success = true;
		}
		else {

			$where_data = array( 'ID' => $this->ID );

			// Are we cancelling a queued operation?
			if( in_array( $this->status, OVCOP::$queued_statuses ) ) {

				$success_message = 'Operation cancelled successfully.';
				$new_status = 'cancelled';
			}
			// ...or requesting a user abort on a running operation?
			else if( in_array( $this->status, OVCOP::$running_statuses ) ) {

				$success_message = 'User abort requested successfully.';
				$new_status = 'user_abort_requested';
			}

			global $wpdb;
			
			if( $wpdb->update( 'wp_ovcop_log', array( 'status' => $new_status ), $where_data ) ) {

				$this->results->main_msg = $success_message;
				$this->log( $this->results->main_msg );
				$this->results->success = true;
			}
			else {

				$this->results->main_msg = "User abort request failed...";
				$this->results->success = false;
			}
		}
	}

	// Check the DB for an updated op status
	// Used to see if the operation was aborted / reset remotely
	public function check_user_abort( $force_check = false ) {

		$exec_time = ox_exec_time();

		if( !$force_check ) {

			if( false === $this->last_status_check ) {
				$this->last_status_check = $exec_time;
			}

			if( $exec_time - $this->last_status_check < 5 ) {
				return;
			}
		}

		$this->last_status_check = $exec_time;

		// Update operation status from database
		global $wpdb;

		$current_status = $wpdb->get_var( "SELECT status FROM wp_ovcop_log WHERE ID = {$this->ID}" );

		if( in_array( $current_status, array( 'reset', 'user_abort_requested' ) ) ) {

			$this->status = $current_status;
			$this->abort_operation( "Operation execution halted. Status check: {$current_status}" );
		}
	} 

	

	protected function jump_operation() {
		$this->check_user_abort( true );

		$this->set_status( 'jumping' );
		$this->pack_op();
		$this->use_log_prefix = false;
		$this->commence_operation();
	}

	public function commence_operation() {

		session_write_close();  // Allows simultaneous asynchronous script execution and database calls

		$jump_post_data = array(
			'ovc_ajax'	=> true,
			'ovc_cron'	=> (bool) $this->config( 'cron_queued' ),
			'action'	=> 'ovc_ajax_nopriv',
			'command'	=> 'ovcop',
			'ovcop'		=> array(
				'request'	=> 'commence_operation',
				'args'		=> array(
					'ID'			=> $this->ID,
					'type'			=> $this->type,
					'verification'	=> $this->meta( 'verification' )
				)
			)
		);

		if( 'jumping' == $this->status ) {

			// Calc total request time
			$op_start = new DateTime( $this->start_time );
			$op_exec_time = $op_start->diff( new DateTime( current_time( 'mysql', 1 ) ) )->format( '%H:%I:%S' );

			// /$log_verb = 'jumping' == $this->status ? 'Jumping' : 'Commencing';
			$this->log( "" );
			$this->log( "Jumping the operation! Request exec time (seconds): " . ox_exec_time() . "  - Total operation time: " . $op_exec_time );
		}

		/*
		 * IDEAL SOLUTION:
		 * 1. Ping the operation to start
		 * 2. Check in a few seconds to make sure the operation has landed
		 * 3-1. If it did land, ??? profit
		 * 3-2. If it didn't land, ping the operation again
		 */
		if( !$ping_operation = $this->ping_operation( $jump_post_data ) ) {

			$this->log( "Retrying..." );

			// Try again
			if( !$ping_operation = $this->ping_operation( $jump_post_data ) ) {

				$this->log( "Retrying..." );

				// Try again
				if( !$ping_operation = $this->ping_operation( $jump_post_data ) ) {

					$this->log( "Jumping has failed for OVCOP ID: " . $this->ID );
				}
			}
		}

		die();
	}

	public function ping_operation( $jump_post_data ) {
		
		$ch = curl_init();

		$curl_options = array(
			CURLOPT_URL 			=> admin_url( 'admin-ajax.php' ),
			CURLOPT_POST 			=> true,
			CURLOPT_POSTFIELDS		=> http_build_query( $jump_post_data ),
			CURLOPT_RETURNTRANSFER	=> true,
			CURLOPT_FRESH_CONNECT	=> true,
			CURLOPT_TIMEOUT		 	=> 3, // seconds
			CURLOPT_CONNECTTIMEOUT	=> 2,
			CURLOPT_SSL_VERIFYPEER	=> false,
			CURLOPT_SSL_VERIFYHOST	=> false
		);

		curl_setopt_array( $ch, $curl_options );
		
		$op_curl_exec = curl_exec( $ch );

		$op_curl_errno = curl_errno( $ch );
		$op_curl_error = curl_error( $ch );

		curl_close( $ch );

		if( 
			'jumping' == $this->status
			&& false === $op_curl_exec 
			// Make sure it's not a timeout as we're forcing timeouts
			&& 28 != $op_curl_errno
		) {
			$op_curl_info = curl_getinfo( $ch );

			$this->log( "Commencing operation has failed for OVCOP ID: " . $this->ID );
			$this->log( "Response from failed OP - Error Code: " . $op_curl_errno . " URL: " . $op_curl_info['url'] . " HTTP Code: " . $op_curl_info['http_code'] . " Total Time: " . $op_curl_info['total_time'] );

			return false;
		}

		return true;
	}

	public function reset_operation( $msg = "No reset message given.", $suicide = true ) {
		$this->use_log_prefix = false;
		$this->log( "OPERATION RESET: {$msg}" );
		$this->results->success = false;
		$this->results->main_msg = "OPERATION RESET";
		$this->results->error( $msg );

		$this->set_status( 'reset' );

		if( !$suicide ) {
			return false;
		} else {
			die();
		}
	}

	public function abort_operation( $msg = "No abort message given.", $suicide = true ) {
		$this->use_log_prefix = false;
		$this->log( "OPERATION ABORTED: {$msg}" );
		$this->results->success = false;
		$this->results->main_msg = "OPERATION ABORTED";
		$this->results->error( $msg );

		// Determine final status
		switch( $this->status ) {
			case 'reset':
				$this->status = 'aborted_after_reset';
			break;
			case 'user_abort_requested':
				$this->status = 'user_aborted';
			break;
			default:
				$this->status = 'aborted';
			break;
		}
		$this->set_status( $this->status );

		// If the request came directly from the user / frontend, send an ajax response, otherwise die()
		if( !$suicide ) {
			return false;
		} else {
			die();
		}
	}

	public function successful_finish() {
		$this->use_log_prefix = false;

		foreach( array( 'before_successful_finish', 'generate_final_report', 'after_successful_finish' ) as $method ) {
			if( method_exists( $this, $method ) ) {
				$this->$method();
			}
		}

		$this->op_data['data'] = [];
		$this->pack_op();

		$this->set_status( 'complete' );

	}

	// Status Handling
	protected function set_status( $new_status ) {
		$current_status = $this->status;
		
		if ( !in_array( $new_status, array_merge( OVCOP::$queued_statuses, OVCOP::$running_statuses, OVCOP::$finished_statuses ) ) ) {
			$new_status = 'abort';
			$this->log( "Invalid status sent to ::set_status ( {$status} ).  Aborting operation.", false );
		}

		$ovcop_data = array(
			'status' 	=> $new_status,
			'last_seen'	=> current_time( 'mysql', 1 )
		);

		// Set start time if we are commencing a queued operation
		if( 'queued' == $current_status && 'running' == $new_status ) {
			$this->log( "Commencing operation from queue." );
			$this->start_time = current_time( 'mysql', 1 );
			$ovcop_data['start_time'] = $this->start_time;
		}

		// Add end time
		if( in_array( $current_status, OVCOP::$running_statuses ) && in_array( $new_status, OVCOP::$finished_statuses ) ) {
			$ovcop_data['end_time'] = current_time( 'mysql', 1 );
		}
		
		global $wpdb;
		$wpdb->update( 'wp_ovcop_log', $ovcop_data, array( 'ID' => $this->ID ) );

		$this->status = $new_status;

		if( in_array( $new_status, OVCOP::$finished_statuses ) ) {
			$op_duration = date_diff( date_create( current_time( 'mysql', 1 ) ), date_create( $this->start_time ) )->format( '%H:%I:%S' );

			$this->log();
			$this->log( '<span class="op-log-finished status-'.$new_status.'">Operation Finished. Final Status: '.$new_status.' - Duration: ' . $op_duration . '</span>', false );

			$this->log_file()->complete_file();
			if( $this->op_file() ) {
				$this->op_file()->complete_file();
			}

			// Advance OVCOP Queue
			OVCOP_manager::auto_start_queued_op();
		}
	}

	// Logging
	public function format_log_msg( $msg = '', $prefix = null ) {

		// Determine whether to prepend log prefix to $msg
		if( 
			method_exists( $this, 'log_prefix' )
			&& (
				( isset( $prefix ) && $prefix )
				|| ( !isset( $prefix ) && $this->use_log_prefix )
			)
		) {
			$msg = $this->log_prefix() . $msg;
		}

		// Wrap the message
		$msg = date( 'Y-m-d H:i:s' ) . ' - ' . $msg;

		return $msg;
	}

	public function log( $msg = '', $prefix = null ) {

		$this->log_file()->write( $this->format_log_msg( $msg ) );
	}

	public function log_results_msgs( $results = false ) {
		OVC_Results::maybe_init( $results );

		$formatted_log_msgs = array_filter( array_map( array( $this, 'format_log_msg' ), $results->get_msgs() ) );

		if( $formatted_log_msgs ) {

			$log_str = implode( $this->log_file()->line_endings, $formatted_log_msgs );
			$this->log_file()->write( $log_str );
		}

		return true;
	}

	protected function generate_final_report() {
		$this->log();
		$this->log();
		$this->log( "{$this->type} OPERATION COMPLETE =======" );

		foreach( $this->stats() as $stat ) {
			$this->log( str_pad( $stat['name'] . ": ", 70 ) . $stat['value'] );
		}

		$this->log();
		$this->log();
	}

	// Packing / Unpacking Operation Data
	public function pack_op() {
		global $wpdb;

		// Save semi-magic operation-specific properties
		foreach ( $this->op_data['data'] as $key => $value ) {
			if ( property_exists( $this, $key ) ) {
				$this->op_data['data'][ $key ] = $this->$key;
			}
		}

		$op_data = serialize( $this->op_data );
		$wpdb->update( 'wp_ovcop_log', array( 'data' => $op_data, 'last_seen' => date( 'Y-m-d H:i:s') ), array( 'ID' => $this->ID ) );
	}

	public function unpack_op() {
		global $wpdb;
		$full_op_data = $wpdb->get_row( "SELECT * FROM wp_ovcop_log WHERE ID = {$this->ID}", ARRAY_A );

		// Unpack General OVCOP data
		foreach ( $full_op_data as $key => $value ) {
			if ( 'data' == $key ) {
				$this->op_data = unserialize( $value );
			}
			else if ( property_exists( $this, $key ) ) {
				$this->$key = $value;
			}
		}

		// Unpack optype-specific data
		foreach ( $this->op_data['data'] as $key => $value ) {
			if ( property_exists( $this, $key ) ) {
				$this->$key = $value;
			}
		}

		// Unpack related OVCOP files (log file, export file, etc.)
		$ovcop_files_meta = $wpdb->get_results( "SELECT ID, file_type FROM wp_ovcop_files WHERE op_id = {$this->ID}", ARRAY_A );

		foreach( $ovcop_files_meta as $file_meta ) {
			$this->files[ $file_meta['ID'] ] = $file_meta['file_type'];

			// Auto-initialize log file
			if( 'log' == $file_meta['file_type'] ) {
				$this->log_file_id = $file_meta['ID']; // Save the log file ID
				$this->files[ $file_meta['ID'] ] = new OVC_File( $file_meta['ID'] ); // Pre-initialize the log file object
			}
			else if( $file_meta['ID'] == $this->meta( 'op_file_id' ) ) {
				$this->files[ $file_meta['ID'] ] = new OVC_File( $file_meta['ID'] );
			}
		}
	}

	/*
	 * File Handling 
	 */

	function ftp_op_dir() {
		return 'ovc_' . $this->type;
	}

	// File Helper Functions

	// Get the log file object
	public function log_file() {
		return $this->files[ $this->log_file_id ];
	}

	public function op_file() {
		if( $this->meta( 'op_file_id' ) ) {
			return $this->files[ $this->meta( 'op_file_id' ) ];
		}
		return false;
	}

	public function new_file( $file_type = '', $file_ext = '', $set_op_file = false ) {

		$file_name = $this->generate_file_name( $file_type, $file_ext );

		$db_file_type = $this->type . '_' . $file_type;

		$new_file = OVC_File::init_file( $file_name, $file_type, $this->ID );

		$this->files[ $new_file->ID ] = $new_file;

		if( 'log' == $file_type ) {
			$this->log_file_id = $new_file->ID;
		}
		else if( $set_op_file ) {
			$this->meta( 'op_file_id', $new_file->ID );
		}

		return $new_file;
	}

	public function generate_file_name( $file_type = '', $file_ext = 'csv' ) {
		return $this->ID . '-' . strtoupper( $this->type ) . '-' . strtoupper( $file_type ) . '-' . date( 'Ymd_His') . '.' . $file_ext;
	}

	public function csv_wrap_value( $value = '', $escape = true, $force_quotes = false ) {
		
		$value = (string) $value;

		if( $escape ) {
			$value = str_replace( '"', "'", stripslashes( $value ) );
		}

		if( $force_quotes || false !== strpos( $value, ',' ) ) {
			$value = '"' . $value . '"';
		}

		return $value;
	}

	public function csv_unwrap_value( $value = '' ) {
		return str_replace( "'", '"', trim( $value, '"' ) );
	}

	protected function write_csv_line_from_array( $values = array() ) {
		$this->op_file()->write( implode( ",", $values ) );

	}

	public function get_php_excel( $template_file ) {
		return \PhpOffice\PhpSpreadsheet\IOFactory::load( $template_file );
	}

	/*
	 * OVCOP Stats functions
	 */
	// Initialize a stat
	public function init_stat( $key, $name ) {
		$this->op_data['stats'][ $key ] = array( 'name' => $name, 'value' => 0 );
	}

	public function set_stat( $key, $value ) {
		if( isset( $this->op_data['stats'][ $key ] ) ) {
			$this->op_data['stats'][ $key ]['value'] = $value;
		}
	}

	public function get_stat( $key ) {
		return $this->op_data['stats'][ $key ]['value'] ?? null;
	}

	public function stat( $key, $add = 1 ) {
		if( isset( $this->op_data['stats'][ $key ] ) ) {
			$this->op_data['stats'][ $key ]['value'] += $add;
		}
	}

	/*
	 * OVCOP Product_Meta functions
	 */

	// Update OVCOP products meta table from an array of OVC IDs
	//dev:improve
	public function mass_update_ovcop_products_meta( $ovcop_meta_field = '', $ovc_ids = array() ) {
		$this->log(); // Skip a line
		$this->log( "Beginning OVC product OVCOP meta update." );
		global $wpdb;

		$this->log( "# OVC IDs for OVCOP products meta update:    " . count( $ovc_ids ) );

		// Chunk through $ovc_ids to avoid long query errors
		$ovc_ids = array_chunk( $ovc_ids, 256 );
		$total_updated = 0;
		foreach( $ovc_ids as $chunk ) {
			
			$meta_update_result = $wpdb->query( $wpdb->prepare( "UPDATE wp_ovc_products SET {$ovcop_meta_field} = %d WHERE ID IN(" . implode( ',', $chunk ) . ')', $this->ID ) );
		
			
			if( false === $meta_update_result ) {
				$this->log( "ERROR updating OVCOP products meta entries: " . $wpdb->last_error );
				return false;
			}
			else {
				$total_updated += $meta_update_result;
			}
		}

		$this->log( "# OVCOP products meta rows actually updated: " . $total_updated );
		$this->log( "OVCOP products meta update complete." );
		return true;
	}
}