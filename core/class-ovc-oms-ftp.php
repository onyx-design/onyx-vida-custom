<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_OMS_FTP {
	// FTP Host / Login info
	private $ftp_host = '24.43.31.234';
	private $ftp_user = 'ovc';
	private $ftp_pass = '901Salameda';

	private $ftpc = false; // FTP Connection Object
	public $ftp_systype;

	public $last_connection_check = false;

	public $results;

	public function __construct( $autoconnect = true ) {
		OVC_Results::maybe_init( $this->results );

		$this->check_connection( $autoconnect );

		// Register PHP Shutdown function that will ensure the ftp connection is closed
		register_shutdown_function( array( $this, 'close_connection' ) );
	}

	public function check_connection( $autoconnect = true ) {
		if ( $this->is_connected() ) {
			return true;
		}
		else if ( $autoconnect ) {
			return $this->connect();
		}
		return false;
	}

	public function is_connected() {
		return ( !$this->ftpc || !is_array( ftp_nlist( $this->ftpc, "." ) ) ? false : true );
	}

	private function connect() {
		// Try to connect up to 3 times
		for( $t = 3; $t > 0; $t-- ) {
			if( $this->ftpc = ftp_connect( $this->ftp_host, 21, 10 ) ) {
				break;
			}
		}

		if( false === $this->ftpc ) {
			return $this->results->error( "Could not connect to VIDA OVC OMS FTP." );
		}
		else if ( ! @ftp_login( $this->ftpc, $this->ftp_user, $this->ftp_pass ) ) {
			return $this->results->error( "VIDA OVC OMS FTP login failed." );
		}
		else {
			ftp_pasv( $this->ftpc, true );
			$this->ftp_systype = ftp_systype( $this->ftpc );
			$this->results->notice( "Succesfully connected and logged into VIDA OVC OMS FTP." );
			return true;
		}
	}

	public function close_connection() {
		if( $this->check_connection() ) {
			ftp_close( $this->ftpc );
			$this->ftpc = false;
			$this->results->notice( "FTP connection closed." );
		}
	}

	public function pwd() {
		return ftp_pwd( $this->ftpc );
	}

	public function chdir( $directory = '.' ) {
		if ( !$this->check_connection() || !ftp_chdir( $this->ftpc, $directory ) ) {
			return $this->results->error( "Unable to change to directory: {$directory}" );
		}
		return true;
	}

	public function nlist_dir( $directory = '.' ) {
		return ftp_nlist( $this->ftpc, $directory );
	}

	public function rawlist_dir( $directory = '.' ) {
		if ( !$this->check_connection() ) {
			return false;
		}

		if ( is_array( $children = @ftp_rawlist( $this->ftpc, $directory ) ) ) {
			return $children;
		}

		return false;
	}

	public function list_dir( $directory = '.', $files_only = false, $sort_key = false ) {
		if ( is_array( $children = $this->rawlist_dir( $directory ) ) ) { 
			$items = array(); 

			switch ( $this->ftp_systype ) {
				case 'UNIX':
					foreach ( $children as $i => $child ) { 
						$chunks = preg_split( "/\s+/", $child ); 
						
						list( $item['rights'], $item['number'], $item['user'], $item['group'], $item['size'], $item['month'], $item['day'], $item['time'] ) = $chunks; 
						
						$item['is_dir'] = $chunks[0]{0} === 'd' ? true : false; 
						
						array_splice( $chunks, 0, 8 ); 
						
						$items[ implode( " ", $chunks ) ] = $item;
						//$items[ $item[''] ] = $item;
					}
				break;
				case 'Windows_NT':
					foreach ( $children as $i => $child ) {
						preg_match( "/([0-9]{2})-([0-9]{2})-([0-9]{2}) +([0-9]{2}):([0-9]{2})(AM|PM) +([0-9]+|<DIR>) +(.+)/", $child , $split );

						if ( is_array( $split ) ) {
							// 4digit year fix
							if ( $split[3]<70) {
								$split[3]+=2000;
							}
							else {
								$split[3]+=1900;
							}

							$item = array(
								'name' 		=> $split[8],
								'is_dir'    => ( $split[7] == "<DIR>" ? true : false ),
								'size'  	=> ( $split[7] == "<DIR>" ? 0 : $split[7] ),
								'month' 	=> $split[1],
								'day'   	=> $split[2],
								'year'  	=> $split[3],
								'hour'  	=> $split[4],
								'min'		=> $split[5],
								'ampm'		=> $split[6],
								'mdtm'		=> false
							);

							if( !$item['is_dir'] ) {
								$item['mdtm'] = strtotime( $item['year'].'-'.$item['month'].'-'.$item['day'].' '.$item['hour'].':'.$item['min'].$item['ampm'] );
							}
							
							// Add item to array (unless it's a dir and we are only including files)
							if( !$item['is_dir'] || !$files_only ) {
								$items[ $item['name'] ] = $item;	
							}
						}
					}
				break;
				default:
					return $this->results->error( "Dir List Failed - Unknown FTP Systype: {$this->ftp_systype}" );
				break;
			}            

			// Maybe sort list (only works if we're in files only mode )
			if( count( $items ) && $files_only && $sort_key && in_array( $sort_key, array( 'name', 'size', 'mdtm' ) ) ) {

				$items = array_column( $items, null, $sort_key );

				krsort( $items );

				$items = array_column( $items, null, 'name' );
			}

			return $items; 
		}
		else {

			return false;
		}

		// Throw exception or return false < up to you 
	} 


	public function get_newest_file( $directory = '.', $name_only = true ) {
		
		$list_dir_mdtm = $this->list_dir( $directory, true, 'mdtm' );

		if( $list_dir_mdtm ) {

			$newest_file = array_shift( $list_dir_mdtm );
		
			if( isset( $newest_file['name'] ) && $name_only ) {
				return $newest_file['name'];
			}	

			return $newest_file;
		}
	}

	public function list_dir_mdtm( $directory = '.' ) {

		if( !$this->check_connection() ) {
			return $this->results->error( "list_dir_mdtm failed: FTP connection failed." );
		}




		foreach( $this->nlist_dir( $directory ) as $file ) {

			if( $mdtm = $this->mdtm( $file ) ) {
				$files_mdtm[ $file ] = $this->mdtm( $file );    
			}
			
		}

		arsort( $files_mdtm, SORT_NUMERIC );

		return $files_mdtm;
	}

	public function mdtm( $file_name ) {

		$mdtm = ftp_mdtm( $this->ftpc, $file_name );

		if ( -1 === $mdtm ) {
			return $this->results->error( "Unable to retrieve last modified time of {$file_name}" );
		}
		else {
			return $mdtm;
		}
	}



	public function file_size( $file_name ) {
		if( !$file_name || !$this->check_connection() ) {
			return $this->results->error( "Unable to retrieve filesize. Invalid filename or FTP connection failed." );
		}

		$file_size = ftp_size( $this->ftpc, $file_name );

		if ( -1 === $file_size ) {
			return $this->results->error( "Unable to retrieve filesize of {$file_name}" );
		}
		else {
			return $file_size;
		}
	}



	public function rename( $oldname, $newname ) {
		
		if( !$this->check_connection() ) {
			return $this->results->error( "Unable to rename file: FTP connection failed." );
		}

		return ftp_rename( $this->ftpc, $oldname, $newname );     
	}

	public function put( $remote_file, $local_file ) {
		if( !$remote_file || !$local_file || !$this->check_connection() ) {
			return $this->results->error( "FTP File Upload failed. Invalid file names supplied or connection problem." );
		}

		if ( ! ftp_put( $this->ftpc, $remote_file, $local_file, FTP_ASCII ) ) {
			return $this->results->error( "Failed to upload file." );
		}
		else {
			$this->results->notice( "File uploaded successfully." );
			return true;
		}
	}

	public function get( $remote_file, $local_file ) {
		if( !$remote_file || !$local_file || !$this->check_connection() ) {
			return $this->results->error( "FTP File Download failed. Invalid file names supplied or connection problem." );
		}

		if ( ! ftp_get( $this->ftpc, $local_file, $remote_file, FTP_ASCII ) ) {
			return $this->results->error( "Failed to download file." );
		}
		else {
			$this->results->notice( "File downloaded successfully." );
			return true;
		}
	}
}