<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_HTML_Tag_Builder {
	public $type;

	protected $attributes = array();

	protected $inner_contents = '';

	
	/**
	 * Initialize the table join object
	 * 
	 * @param mixed 		$args 	if array, $args is parsed into different parts of the element; if string, it is used as the element's type
	 * 
	 * @return bool 
	 **/
	public function __construct( $args, $attrs = null ) {
		
		if( is_array( $args ) ) {

			foreach( $args as $arg_name => $value ) {

				if( 'type' == $arg_name ) {
					$this->type = $value;
				} 
				else if( 'attrs' == $arg_name && is_array( $value ) ) {
					foreach ( $value as $attr_name => $attr_value ) {
						$this->attr( $attr_name, $attr_value );
					}
				}
				else if( 'contents' == $arg_name ) {
					$this->contents( $value );
				}
			}
		}
		else if( is_string( $args ) ) {
			$this->type = $args;

			if( is_array( $attrs ) ) {
				foreach ( $attrs as $attr_name => $attr_value ) {
					$this->attr( $attr_name, $attr_value );
				}
			}
		}
	}

	/**
	 * Get / Set an attribute
	 * 
	 * @param string 		$name 		the name of the attribute
	 * @param mixed 		$value		if int / string, set the value; if null, return the value; if false, remove the attribute
	 * 
	 * @return bool
	 **/
	public function attr( $name, $value = null ) {
		
		if( !isset( $value ) ) {

			if( array_key_exists( $name, $this->attributes ) ) {
				return $this->attributes[ $name ];
			}
			else return null;
		}
		else if( false === $value ) {

			if( array_key_exists( $name, $this->attributes ) ) {
				unset( $this->attributes[ $name ] );
			}
			return true;
		}
		else {
			
			if( ( is_string( $value ) || is_numeric( $value ) )
				|| ( is_array( $value ) && in_array( $name, array( 'class', 'css', 'style' ) ) )
			) {

				// Special handling for 'class' & 'css' attributes
				if( 'class' == $name ) {
					$value = is_array( $value ) ? $value : explode( ' ', $value );
				}
				else if( 'css' == $name || 'style' == $name ) {
					$name = 'style';
					
					if( !is_array( $value ) ) {
						
						$value = explode( ';', $value );
						
						$css_props = array();
						foreach( $value as $css_prop ) {
							$css_prop = explode( ':', $css_prop );

							$css_props[ trim( $css_prop[0] ) ] = trim( $css_prop[1] );
						}

						$value = $css_props;
					}
					else {

						foreach( $value as $property => $css_value ) {

						}
					}
				}

				$this->attributes[ $name ] = $value;
				return true;	
			}
			else return null;
		}
	}

	/**
	 * Update multiple attribute values with an associative array
	 * 
	 * @param array 			$attrs an associative array of attribute => value pairs
	 **/
	public function multi_attr( $attrs ) {
		if( is_array( $attrs ) ) {
			foreach ( $attrs as $attr_name => $attr_value ) {
				$this->attr( $attr_name, $attr_value );
			}
		}
	}

	/**
	 * Add a class to the element
	 * 
	 * @param string 			$class
	 **/
	public function add_class( $class = '' ) {

		if( strlen( $class ) ) {

			// Maybe initialize the class attribute
			if( !isset( $this->attributes['class'] ) 
				|| !is_array( $this->attributes['class'] )
			) {
				$this->attributes['class'] = array();
			}

			// Add the class if it does 
			if( !in_array( $class, $this->attributes['class'] ) ) {
				$this->attributes['class'][] = trim( $class );
			}
		}
	}

	/**
	 * Remove a class from the element
	 * 
	 * @param string 			$class
	 **/
	public function remove_class( $class = '' ) {

		// Avoid PHP Notice by checking if class attribute is set
		if( isset( $this->attributes['class'] )
		 && is_array( $this->attributes['class'] )
		 && strlen( $class )
		) { 

			// Remove the class if it exists
			$new_classes = array();
			foreach( $this->attributes['class'] as $existing_class ) {

				if( $class != $existing_class ) {
					$new_classes[] = $existing_class;
				}
			}

			// Set new classes or remove class attribute if there are no classes
			if( $new_classes ) {
				$this->attributes['class'] = $new_classes;	
			}
			else {
				unset( $this->attributes['class'] );
			}
		}
	}

	/**
	 * Get / Set inline CSS properties
	 * 
	 * @param string 		$property 		The CSS property to get/set
	 * @param string 		$value 			[optional] The value to use. Sending false will remove this property
	 * 
	 * @return mixed 		
	 **/
	public function css( $property, $value = null ) {
		// Maybe initialize css properties
		if( !isset( $this->attributes['style'] ) ) {
			$this->attributes['style'] = array();
		}

		// Determine if getting or setting css value
		if( isset( $value ) ) {

			if( false === $value ) {

				unset( $this->attributes['style'][ $property ] );
			}
			else {

				$this->attributes['style'][ $property ] = $value;
			}
		}

		return isset( $this->attributes['style'][ $property ] ) ? $this->attributes['style'][ $property ] : null;
	}

	/**
	 * Get / Replace the contents
	 * 
	 * @param string 		$contents		the content to
	 * 
	 * @return string
	 **/
	public function contents( $contents = null, $append = false ) {

		if( false === $contents ) {
			$this->inner_contents = '';
		}
		else if ( isset( $contents ) ) {
			$this->inner_contents = $append ? $this->inner_contents . $contents : $contents;
		}

		return $this->inner_contents;
	}

	/**
	 * Generate the element as an HTML string
	 * 
	 * @return string
	 **/
	public function get_html() {

		$html = "<{$this->type} ";

		// Build attributes
		foreach( $this->attributes as $name => $value ) {

			// Special preparation of attribute value for 'class' and 'style' attributes
			if( 'class' == $name ) {
				$value = implode( ' ', $value );
			}
			else if( 'style' == $name ) {

				$style_value = '';
				foreach( $value as $css_prop => $css_value ) {
					$style_value .= "{$css_prop}:{$css_value};";
				}
				$value = $style_value;
			}

			$html .= $name . '="' . $value . '" ';
		}

		if( in_array( $this->type, array( 'input', 'br' ) ) ) {
			$html .= '/>';
		}
		else {
			$html .= '>' . $this->inner_contents . "</{$this->type}>";
		}

		return $html;
	}

	/**
	 * Export an associative array that can be used as the $args for a new instance
	 * 
	 * @return array
	 **/
	public function export() {
		$args = array(
			'type' 		=> $this->type,
			'attrs'		=> $this->attributes,
			'contents'	=> $this->inner_contents
		);

		return $args;
	}

	/**
	 * Convert an associative array into html
	 *
	 * @return string
	 **/
	public static function array_to_html( $data ) {

		$html = '';

		if( is_array( $data ) ) {

			foreach( $data as $key => $value ) {

				$element = new OVC_HTML_Tag_Builder( $key );

				if( is_string( $value ) ) {

					$element->contents( $value, true );
				}
				else if( is_array( $value ) ) {

					$element->contents( OVC_HTML_Tag_Builder::array_to_html( $value ), true );
					
				}

				$html .= $element->get_html() . "\r\n";
			}
		}

		return $html;
	}
}