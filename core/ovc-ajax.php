<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

error_reporting(E_ALL); 
ini_set("log_errors", 1); 
ini_set("max_execution_time", 300 );

function ovc_ajax_handler( $command = false ) {
	$command = ( !$command ? $_POST['command'] : $command );

	// Determine AJAX call type base on $_POST['command']
	if ( 'ovcdt' == $command ) {
		if( isset( $_POST['ovcdt']['type'], $_POST['ovcdt']['request'] ) ) {

			$ovcdt_args = isset( $_POST['ovcdt']['args'] ) && is_array( $_POST['ovcdt']['args'] ) ? $_POST['ovcdt']['args'] : array();

			if( $dt_meta = OVCSC::get_field( $_POST['ovcdt']['type'], 'ovcdt' ) ) {

				$classname = $dt_meta->classname ? $dt_meta->classname : 'OVC_Table';

				if( class_exists( $classname ) ) {

					$ovcdt = new $classname( $_POST['ovcdt']['request'], $ovcdt_args, $dt_meta );

					// Send JSON - do not use wp_send_json because it does not use the JSON_NUMERIC_CHECK flag, which sends numeric values as numeric variables instead of strings
					@header( 'Content-Type: application/json; charset=' . get_option( 'blog_charset' ) );

					echo json_encode( $ovcdt->ajax_response() );

					//echo json_encode( $ovcdt->ajax_response(), JSON_NUMERIC_CHECK );
					wp_die();
					//wp_send_json( $ovcdt->ajax_response() );
				}
				else die( "Invalid OVCDT Class: {$classname}" );
			
			} 
			else die( "Invalid OVCDT Type requested: {$_POST['ovcdt']['type']}" );
		}
		else {

			die( "Invalid OVCDT request format." );
		}
	}
	else if ( 'ovcop' == $command ) {

		if( empty( $_POST['ovcop']['request'] ) ) {
			die( "Missing OVCOP request." );
		}

		$request = $_POST['ovcop']['request'];

		$args = isset( $_POST['ovcop']['args'] ) && is_array( $_POST['ovcop']['args'] ) ? $_POST['ovcop']['args'] : array();

		if( 'commence_operation' == $request ) {

			ob_start();
					
			@header( 'Content-Type: application/json; charset=' . get_option( 'blog_charset' ) );

			echo wp_json_encode( array( 'success' => true ) );

			ob_end_flush();
			ob_flush();
			flush();

			$ovcop = OVCOP::constructify( $request, $args );
		}
		else {
			$ovcop_manager = new OVCOP_manager( $request, $args );	
		}

	}
	else if ( 'oxmd' == $command ) {
		$oxmd = new OXMD( $_POST['oxmd'] );
		wp_send_json( $oxmd->ajax_response() );
	}
	else if( 'ovc_screen' == $command ) {

		if( 'file_upload' == $_POST['ovc_screen'] ) {

			if( isset( $_FILES, $_FILES['uploaded_file'], $_POST['ovc_id'] ) ) {	

				$args = array(
					'check_only'	=> $_POST['check_only'],
					'ovcdt_version'	=> $_POST['ovcdt_version'],
					'data'			=> array(
						'ovc_id' 		=> $_POST['ovc_id'],
						'files'			=> $_FILES
					)
				);

				$ovcdt = new OVC_Table_ovc_images( 'upload_override_image', $args, OVCSC::get_field( 'ovc_images', 'ovcdt' ) );

				wp_send_json( $ovcdt->ajax_response() );
			}

		} else {

			$ovc_screen = new OVC_Screen( $_POST['ovc_screen'] );
			wp_send_json( $ovc_screen->ajax_response() );
		}
	}
	else die( "INVALID OVC AJAX COMMAND: {$_POST['command']}" );
}
add_action( 'wp_ajax_ovc_ajax', 'ovc_ajax_handler');

function ovc_ajax_nopriv_handler() {
	ovc_ajax_handler( $_POST['command'] );
}
add_action( 'wp_ajax_nopriv_ovc_ajax_nopriv', 'ovc_ajax_nopriv_handler');


