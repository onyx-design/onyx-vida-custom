<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

//ini_set("auto_detect_line_endings", true); // Ensure proper reading of csv files
//ini_set("memory_limit", '256M' );
//ini_set("max_execution_time", 300 );

//session_write_close();  // Allows simultaneous asynchronous script execution and database calls

class OVCOP_manager {

	public $request;

	public static $template_requests = array( 'new_operation', 'view_operation' );

	public static $user_action_requests = array( 'reset_operation', 'abort_operation', 'reset_all_operations', 'auto_start_queued_op' );

	public static $system_requests = array( 'check_queue' );

	protected static $max_running_ops = 4;

	//public $result_sets = array();
	public $results;
	public $response_data = array();

	public $skip_ajax_response = false;

	public function __construct( $request, $args = array(), $skip_ajax_response = false ) {

		$this->results = new OVC_Results();

		$this->request = $request;

		$this->skip_ajax_response = $skip_ajax_response;


		if( in_array( $request, self::$template_requests ) ) {

			$this->$request( $args );
		}
		else if( 'auto_start_queued_op' == $request ) {
			self::auto_start_queued_op();
		}
		else if( in_array( $request, self::$user_action_requests ) ) {

			$this->$request( $args );
		}
		else {
			$this->results->success = false;
			$this->results->main_msg = "Invalid OVCOP Request";
		}

		$this->ajax_response();
	}

	protected function new_operation( $args ) {

		// Load operation select table
		if( !empty( $args['load_op_selection'] ) ) {

			$this->response_data['template_type'] = 'select_operation';
			$this->response_data['html'] = $this->load_template( 'operation-select' );
			$this->response_data['refresh_ovcop_table'] = true;

			$this->results->success = true;
			$this->results->main_msg = "OVC Operation Manager Ready.";
		}
		// Operation type selected -> determine whether to load configuration screen or to create operation
		else if( !empty( $args['type'] ) && in_array( $args['type'], OVCOP::$valid_op_types ) ) {

			$op_type = $args['type'];
			$this->response_data['refresh_ovcop_table'] = true;

			if( empty( $args['config'] ) ) {
				$this->response_data['template_type'] 	= 'config_operation';
				$this->response_data['html'] 			= $this->load_template( $op_type . '-options' );

				
				$this->results->success = true;
				$this->results->main_msg = "Configure {$op_type} operation.";
			}
			else {

				$this->init_operation( $args );
			}
		}
		else {
			$this->results->success = false;
			$this->results->main_msg = "New Operation Request Failed.";
		}
	}

	public function view_operation( $args ) {

		if( $args instanceof OVCOP ) {
			$ovcop = $args;
		}
		else if( isset( $args['opID'] ) ) {
			$ovcop = new OVCOP( $args['opID'] );	
		}
		else {
			$this->results->success = false;
			$this->results->main_msg = "View Operation failed.";
			$this->results->notice( "Invalid arguments sent to OVCOP_manager view_operation method" );
			return false;
		}

		$this->response_data['opData'] = array(
			'ID' 			=> $ovcop->ID,
			'opType'		=> $ovcop->type,
			'status'		=> $ovcop->status
		);

		$this->response_data['log_url'] 		= $ovcop->log_file()->url();
		$this->response_data['template_type'] 	= 'view_operation';
		$this->response_data['html'] 			= $this->load_template_generalized( 'view', $ovcop );

		// dev:improve
		if( in_array( $ovcop->status, OVCOP::$finished_statuses ) ) {
			$this->response_data['refresh_ovcop_table'] = true;
		}

		$this->results->success = true;
		$this->results->main_msg = "Viewing Operation Details: Op ID {$ovcop->ID}";
	}

	protected function init_operation( $args ) {

		if( !self::optype_queueable( $args['type'] ) ) {
			$this->results->success = false;
			$this->results->main_msg = "Cannot initialize operation. Operation of type {$args['type']} is already queued.";
			return false;
		}

		// Initialize semi-magic 'op_data' array
		$op_data = array(
			'config'	=> array_map( 'ox_maybe_str_to_bool', $args['config'] ),
			'meta'		=> array(
				'verification'	=> sha1( wp_generate_password( 20 ) . uniqid( time(), true ) )
			),
			'data'		=> array(),
			'stats'		=> array()
		);

		// Initialize basic operation info
		$ovcop_meta = array(
			'type'			=> $args['type'],
			'last_seen'		=> current_time( 'mysql', 1 ),
			'status' 		=> 'queued',
			'user_id'		=> get_current_user_id(),
			'data' 			=> serialize( $op_data )
		);

		// Determine Operation-specific Child Class and maybe run operation-specific extra_init method
		$child_class = OVCSC::get_meta_value( 'ovcop', $args['type'], 'classname' );

		// Hook to allow operations to modify their meta/op_data or to cancel themselves before init
		if( method_exists( $child_class, 'pre_init_check' ) ) {

			$start_op = $child_class::pre_init_check( $ovcop_meta, $op_data, $this->results );

			if( empty( $start_op ) ) {
				return false;
			}
		}

		global $wpdb;

		if( $wpdb->insert( 'wp_ovcop_log', $ovcop_meta ) ) {
			
			$op_ID = $wpdb->insert_id;
		} 
		else {
			$this->results->success = false;
			$this->results->main_msg = "Failed to create operation reference in database.";
			return false;
		}

		// Instanstiate OVCOP object
		$ovcop = new OVCOP( $op_ID );

		// Initialize log file
		$ovcop->new_file( 'log', 'txt' );

		$log_header = 'OVCOP OPERATION LOG - {Operation ID:' . $ovcop->ID . ',Type:' . $ovcop->type . ',init_time: ' . current_time( 'mysql', 1 ) . '}';

		$ovcop->log_file()->write( $log_header );

		

		$auto_commence_operation = true;
		if( method_exists( $child_class, 'extra_init' ) ) {
			$auto_commence_operation = call_user_func( array( $child_class, 'extra_init' ), $ovcop );
		}
		else {
			$ovcop->results->main_msg = "{$ovcop->type} initialized successfully.";
			$ovcop->results->success = true;
		}

		// Save any initialization changes
		$ovcop->pack_op();
		

		// Send view_operation response back to Admin page
		if( !$this->skip_ajax_response ) {
			$this->view_operation( $ovcop );	
		}

		if( $auto_commence_operation ) {
			self::auto_start_queued_op();
		}
	

		if( $ovcop->results->success ) {
			$this->results->notice( "OVCOP {$ovcop->type}, ID {$ovcop->ID} initialized successfully" );
		}
		/*
		else {
			$this->results->notice( "Operation not yet started.  Waiting for further input to complete setup." );
		}
		*/
		//$this->
	}

	public function reset_operation( $args ) {
		global $wpdb;

		if( false !== $wpdb->query( "UPDATE wp_ovcop_log SET status = 'reset' WHERE ID = " . $args['opID'] ) ) {
			$this->response_data['refresh_ovcop_table'] = true;
			$this->results->main_msg = "Operation ID " . $args['opID'] . " reset.";

			self::auto_start_queued_op();
		}

		$this->results->success = true;
	}

	public function abort_operation( $args ) {

		$ovcop = new OVCOP( $args['opID'] ); 

		//dev:improve - validate opID / OVCOP

		$ovcop->user_abort();

		$this->response_data['refresh_ovcop_table'] = true;

		$this->results = $ovcop->results;
	}

	public function reset_all_operations( $args ) {
		global $wpdb;

		// DEV IMPROVE STATUS HANDLING!! 
		// Jumping operations should check their status when they land
		// Running operations should be set to user_cancel and reset themselves
		// Init operations should recheck their status before they start
		if( false !== $wpdb->query( "UPDATE wp_ovcop_log SET status = 'reset' WHERE status IN('" . implode( "','", array_merge( OVCOP::$running_statuses, OVCOP::$queued_statuses ) ) . "')" ) ) {
			$this->response_data['refresh_ovcop_table'] = true;
			$this->results->main_msg = "All OVCOP Operations reset.";
		}
		$this->results->success = true;
	}

	public static function queue_auto_sync_ops() {
		//return false;
		// Do not run on dev sites or if auto sync is turned off
		if( !on_vida_live() || !get_option( 'ovc_auto_sync_ops' ) ) {
			return false;
		}

		// Check for operation with a running status that was last seen greater than 5 minutes ago
		if( $stuck_op = self::get_stuck_ovcop() ) {

			$ovcop = new OVCOP( $stuck_op->ID );

			$ovcop->reset_operation( 'Resetting stuck operation', false );
		}

		$auto_sync_ops = array( 
			'ovc_data_scan' => array(
				'type' 			=> 'ovc_data_scan',
				'config'		=> array(
					'cron_queued'			=> true,
					'scan_mode'				=> 'auto_scan',
					'data_errors'			=> true
				)
			),
			'oms_inventory' => array(
				'type'			=> 'oms_inventory',
				'config'		=> array(
					'file_source'			=> 'ftp',
					'import_mode' 			=> 'standard',
					'cron_queued'			=> true
				)
			),
			'wc_sync' 		=> array(
				'type' 			=> 'wc_sync',
				'config'		=> array(
					'cron_queued'			=> true,
					'sync_mode'				=> 'auto_sync'
				)
			),
			'walmart_sync' 	=> array(
				'type' 			=> 'walmart_sync',
				'config'		=> array(
					'cron_queued'			=> true,
					'sync_mode'				=> 'auto_sync',
					'inventory_only'		=> false,
					'sync_all_inventory'	=> false
				)
			),
			'shopify_sync' => array(
				'type'			=> 'shopify_sync',
				'config'		=> array(
					'cron_queued'			=> true,
					'sync_mode'				=> 'auto_sync',
					'shopify_back_sync'		=> false,
				)
			),
			'analyze_images'=> array(
				'type' 			=> 'analyze_images',
				'config'		=> array(
					'cron_queued'			=> true,
					'analysis_type'			=> 'cropability',
					'copy_to_walmart_ftp'	=> true
				)
			),
		);

		foreach ( $auto_sync_ops as $optype => $args ) {
			
			if( self::time_since_optype( $optype ) >= 3600 
				&& OVC_Sync_Manager::sync_needed( $optype ) ) {
				// Queue the operation - this will fail smoothly if the optype can't be queued and will also auto-start the op_queue
				new OVCOP_manager( 'new_operation', $args, true );
			}
		}
	}

	public static function time_since_optype( $optype ) {

		global $wpdb;

		$last_run = $wpdb->get_var( "SELECT MAX( start_time ) FROM wp_ovcop_log WHERE type = '{$optype}'" );

		if( $last_run ) {

			return current_time( 'timestamp', 1 ) - strtotime( $last_run );
		} 

		return false;
	}

	public static function get_stuck_ovcop() {
		global $wpdb;

		$query = "SELECT * FROM wp_ovcop_log WHERE status IN ('" . implode( "','", OVCOP::$running_statuses ) . "') AND last_seen <= '" . date( 'Y-m-d H:i:s', strtotime( '-5 minutes' ) ) . "' ORDER BY ID DESC LIMIT 1";

		return $wpdb->get_row( $query );
	}

	public static function get_incomplete_ovcops( $statuses = 'all', $fields = '*' ) {

		global $wpdb;

		$fields = is_array( $fields ) ? implode( ', ', $fields ) : $fields;

		switch( $statuses ) {
			case 'queued':
				$statuses = OVCOP::$queued_statuses;
			break;
			case 'active':
				$statuses = OVCOP::$running_statuses;
			break;
			default:
				$statuses = array_merge( OVCOP::$running_statuses, OVCOP::$queued_statuses );
			break;
		}

		$query = "SELECT {$fields} FROM wp_ovcop_log WHERE status IN('" . implode( "','", $statuses ) . "')";

		return $wpdb->get_results( $query, ARRAY_A );
	}

	public static function optype_queueable( $op_type ) {

		global $wpdb;

		return !( (bool) $wpdb->get_var( "SELECT COUNT(*) FROM wp_ovcop_log WHERE type = '{$op_type}' AND status IN('" . implode( "','", array_merge( OVCOP::$queued_statuses, OVCOP::$running_statuses ) ) . "')" ) );

		/*
		$query = "SELECT * FROM wp_ovcop_log WHERE status IN('" . implode( "','", array_merge( OVCOP::$queued_statuses, OVCOP::$running_statuses ) ) . "')";
		ovc_dev_log( $query );
		$queued_ops = $wpdb->get_results( $query, ARRAY_A );

		ovc_dev_log( $queued_ops );
		ovc_dev_log( $op_type );
		return !in_array( $op_type, array_column( $queued_ops, 'type' ) );
		*/
	}

	public static function auto_start_queued_op() {

		global $wpdb;

		// First check if there are any active operations
		$active_ops = $wpdb->get_results( "SELECT * FROM wp_ovcop_log WHERE status IN('" . implode( "','", OVCOP::$running_statuses ) . "')", ARRAY_A );

		$running_ops_count = count( $active_ops );
		if( $running_ops_count >= self::$max_running_ops ) {
			return false;
		}

		// Check if a single queue operation is running
		$single_queue_op_running = false;
		foreach( $active_ops as $active_op ) {
			if( !in_array( $active_op['type'], OVCOP::$multi_queue_ops ) ) {
				$single_queue_op_running = true;
				break;
			}
		}


 
		$queued_ops = $wpdb->get_results( "SELECT * FROM wp_ovcop_log WHERE status = 'queued' ORDER BY ID ASC", ARRAY_A );
		$commence_op_ids = array();

		foreach( $queued_ops as $queued_op ) {

			if( $running_ops_count < self::$max_running_ops ) {

				// Check if queued op is multie_queue allowed or not
				if( in_array( $queued_op['type'], OVCOP::$multi_queue_ops ) ) {
					$commence_op_ids[] = $queued_op['ID'];
					$running_ops_count++;
				}
				// Can only queueue a single_queue opType if there is not already one running
				else if( !$single_queue_op_running ) {
					$commence_op_ids[] = $queued_op['ID'];
					$running_ops_count++;
					$single_queue_op_running = true;
				}
			}
			else {
				break;
			}

		}

		// Check for active ops
		foreach( $commence_op_ids as $commence_op_id ) {

			$commence_op = new OVCOP( $commence_op_id );

			register_shutdown_function( array( $commence_op, 'commence_operation' ) );	
		}

	}

	// OVCOP Page Template / HTML Helper Functions
	public function load_template( $ovcop_template, $ovcop = null ) {
		$filename = OVC_PATH . "/templates/ovcop/ovcop-{$ovcop_template}.php";

		if ( is_file( $filename ) ) {
			ob_start();
			include $filename;
			return ob_get_clean();
		}
		else {
			$this->results->main_msg = "Error loading template.";
			return $this->results->error( "Unable to load OVCOP template: {$ovcop_template}" );
		}
	}

	// Maybe load a general template instead of an operation-specific template
	public function load_template_generalized( $step_name, $ovcop = null ) {
		$template_file_stem = OVC_PATH . "/templates/ovcop/ovcop-";

		// DEV:IMPROVE!!!!
		$optype_template = "--{$step_name}";
		if( !empty( $ovcop->type ) ) {
			$optype_template = "{$ovcop->type}-{$step_name}";
		}
		
		$general_template = "operation-{$step_name}";

		if( is_file( $template_file_stem . $optype_template . ".php" ) ) {
			return $this->load_template( $optype_template, $ovcop );
		}
		else {
			return $this->load_template( $general_template, $ovcop );
		}
	}


	// Build AJAX Response
	public function ajax_response() {

		if( $this->skip_ajax_response ) {
			return false;
		}
		
		$response = array(
			'request'		=> $this->request,
			'status'		=> ( $this->results->success ? 'success' : 'fail' ),
			'result_sets'	=> array( get_object_vars( $this->results ) ),
			'response_data' => $this->response_data
		);

		@header( 'Content-Type: application/json; charset=' . get_option( 'blog_charset' ) );
		ob_start();
		echo json_encode( $response );
		ob_end_flush();
		flush();
		die();
	}
}