<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Expression {

	public $raw;

	public $row;

	protected $valid = null;

	protected $output;

	public $fields = null;

	//public $extract_fields = true;

	public function __construct( $raw_expression, $row = null, $results = false, $extract_fields = true ) {
		OVC_Results::maybe_init( $results );

		$this->raw = $raw_expression;
		$this->row = $row;

		/*
		if( !empty( $options['extract_fields'] ) ) {
			$this->extract_fields = $options['extract_fields'];
		}
		*/

		$this->evaluate( true, $extract_fields, $results );
	}

	public function is_valid() {

		if( !isset( $valid ) ) {

			$this->evaluate();
		}

		return $this->valid;
	}

	public function is_equation() {

		return (bool) ( is_string( $this->raw ) && 0 === strpos( $this->raw, '::=' ) );
	}



	
	

	public function evaluate( $force_refresh = false, $extract_fields = false, $results = false ) {
		OVC_Results::maybe_init( $results );

		if( !( $force_refresh || !isset( $this->valid ) ) ) {

			return $this->output;
		}

		// Maybe initialize $fields property
		if( !isset( $this->fields ) ) {
			$this->fields = array();
		}

		// Get expression to evaluate (maybe remove ::= equation prefix)
		$expression = $this->is_equation() ? substr( $this->raw, 3 ) : $this->raw;

		// Check for and evaluate nested expressions
		$nested_expression_regex = "/:{2}\{[a-zA-Z0-9_\[\]\:.]+\}/";

		while( preg_match( $nested_expression_regex, $expression, $match ) ) {

			// Get the nested expression (de-tokenize)
			$nested_expression = substr( $match[0], 3, strlen( $match[0] ) - 4 );

			$nested_expression = new OVC_Expression( $nested_expression, $this->row );

			// Evaluate the nested expression
			$nested_value = $nested_expression->evaluate( $force_refresh, $extract_fields );


			if( $extract_fields ) {
				$this->fields = array_merge( $this->fields, $nested_expression->get_fields() );	
			}
			

			if( $nested_expression->is_valid() ) {

				$expression = str_replace( $match[0], $nested_value, $expression );
			}
			else {

				$this->valid = false;
				return null;
			}
		}

		// Check for and replace any variable tokens
		$var_token_regex = "/:{2}\[[a-zA-Z0-9_.]+\]/";
		
		while( preg_match( $var_token_regex, $expression, $match ) ) {

			// Get the nested expression (de-tokenize)
			$var_name = substr( $match[0], 3, strlen( $match[0] ) - 4 );

			// Maybe get field
			if( $extract_fields && OVCSC::get_field( $var_name ) ) {

				$this->fields[] = $var_name;
			}


			if( isset( $this->row ) ) {

				// Determine value to use in expression based on type of $row supplied (array or OVC_Row)
				if( is_array( $this->row ) ) {
					$var_value = $this->row[ $var_name ];
				}
				else {
					$var_value = $this->row->new_value( $var_name );
				}
				
				// Fix issue with blank values getting put into equations //dev:improve
				if( $this->is_equation() && empty( $var_value ) ) {
					$var_value = 0;
				} 

				$expression = str_replace( $match[0], $var_value, $expression );
			}
			// extract fields only mode // dev:improve
			else if( !isset( $this->row ) && $extract_fields ) {
				$expression = str_replace( $match[0], $var_name, $expression );
			}
			else {

				$this->valid = false;
				return null;
			}
		}

		// Finalize field list
		$this->fields = array_unique( $this->fields );

		// Maybe evaluate as an equation
		if( $this->is_equation() ) {

				// ovc_debug()->set_active()->log('EVAL EQUATION');
				// $row_data = $this->row ? "\r\nRow ID: {$this->row->ID} FS: {$this->row->field_set} \r\n": '';
				// $error_msg =  "Error evaluating expression: [  {$expression}  ]{$row_data}Raw Expression: {$this->raw} \r\n Error message: ".print_r( error_get_last(), true);
				// ovc_debug()->set_active()->log( $error_msg );
			
				@eval( '$expression = ' . $expression . ';' );
			
				

		}

		$this->valid = true;

		$this->output = $expression;

		return $this->output;
	}

	public function get_fields( $force_refresh = false, $results = false ) {
		OVC_Results::maybe_init( $results );

		$this->evaluate( $force_refresh, true, $results );

		return $this->fields;
	}


}