<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Sync_Manager {

	private static $version = 38;

	public static $sync_manager_views = array(
		'wc_sync',
		'wc_delete_variations',

		'ovc_data_scan',
		'ovc_orphaned_image_sets',
		'ovc_unused_images',
		 
		'walmart_init_items',
		'walmart_cancel_items',
		'walmart_retire_items',
		'walmart_images_requiring_sync',

		'walmart_bulk_update_skus',
		'walmart_bulk_create_items',
		'walmart_bulk_update_items',
		'walmart_bulk_update_items_all',
		'walmart_bulk_update_manual',
		'walmart_bulk_update_prices',
		'walmart_bulk_update_inventory',
		'walmart_bulk_update_inventory_all',

		'shopify_sync',
		'shopify_sync_all',
	
		'shopify_create_parents',
		'shopify_update_parents',
		'shopify_delete_parents',
		'shopify_delete_variants',
		'shopify_update_variants',
		'shopify_variant_images',

		'oms_global_change_skus',

		'case_sku_children_count',
	);

	protected static $query_results = array(
		// WooCommerce
		'wc_sync' 								=> null,
		'wc_delete_variations'					=> null,

		// OVC
		'ovc_data_scan'							=> null,
		'ovc_orphaned_image_sets'				=> null,
		'ovc_unused_images'						=> null,

		// Walmart
		'walmart_feeds'							=> null,
		'walmart_init_items'					=> null,
		'walmart_cancel_items'					=> null,
		'walmart_retire_items'					=> null,
		'walmart_images_requiring_sync'			=> null,

		'walmart_bulk_update_skus'				=> null,
		'walmart_bulk_create_items'				=> null,
		'walmart_bulk_update_items'				=> null,
		'walmart_bulk_update_items_all'			=> null,
		'walmart_bulk_update_manual'			=> null,
		'walmart_bulk_update_prices'			=> null,
		'walmart_bulk_update_inventory'			=> null,
		'walmart_bulk_update_inventory_all'		=> null,

		// Shopify
		'shopify_sync'							=> null,
		'shopify_sync_all'						=> null,
	
		'shopify_create_parents'				=> null,
		'shopify_update_parents'				=> null,
		'shopify_delete_parents'				=> null,
		'shopify_delete_variants'				=> null,
		'shopify_update_variants'				=> null,
		'shopify_delete_images'					=> null,
		'shopify_variant_images'				=> null,

		// OMS
		'oms_global_change_skus'				=> null,
		'oms_product_data'						=> null,
		'oms_product_data_import'				=> null,

		// Other
		'case_sku_children_count'				=> null,
	);

	public static function sync_needed( $sync_type = null ) { //dev:improve

		$sync_dependencies = array(
			'walmart_sync' 	=> array(
				'walmart_bulk_create_items',
				'walmart_bulk_update_items',
				'walmart_bulk_update_items_all',
				'walmart_bulk_update_prices',
				'walmart_bulk_update_inventory',
				//'walmart_bulk_update_inventory_all',
				'walmart_cancel_items',
				'walmart_retire_items',
				'walmart_feeds'
			),
			'wc_sync' 		=> array(
				'wc_sync',
				'wc_delete_variations'
			),
			'shopify_sync' 	=> array(
				'shopify_create_parents',
				'shopify_update_parents',
				'shopify_delete_parents',
				'shopify_delete_variants',
				'shopify_update_variants',
				'shopify_delete_images',
				'shopify_variant_images'
			)
		);

		if( 'oms_inventory' == $sync_type ) {
			$ftp = new OVC_OMS_FTP;

			if( !$ftp->check_connection() ) {
				return false;
			}

			$remote_file_name = $ftp->get_newest_file( '/oms_inventory_update' );

			if( !$remote_file_name ) {
				return false;
			}


			$newest_file = '/oms_inventory_update/' . $remote_file_name;

			// Check that this file is newer than the last one used from FTP
			$this_mdtm = intval( $ftp->mdtm( $newest_file ) );
			$last_mdtm = intval( get_option( 'ovc_oms_inventory_last_mdtm', 0 ) );

			if( $this_mdtm < $last_mdtm ) {

				return false;	
			}

			return true;
		} else if( 'ovc_data_scan' == $sync_type ) {
			$last_ovc_data_scan = get_option( 'ovcop__last_ovc_data_scan' );

			if( !$last_ovc_data_scan 
				|| ( time() - $last_ovc_data_scan ) > ( 24 * 60 * 60 )
			) {

				update_option( 'ovcop__last_ovc_data_scan', time() );
				return true;
			}

			return false;
		} else if( 'analyze_images' == $sync_type ) {

			return true;
		} else if( isset( $sync_dependencies[ $sync_type ] ) ) {
			foreach( $sync_dependencies[ $sync_type ] as $sync_dependency ) {
				if( self::get_auto_sync_count( $sync_dependency ) ) {
					return true;
				}
			}
		} else {
			return (bool) self::get_auto_sync_count( $sync_type );
		}
	}

	public static function get_results( $query = null ) {
		global $wpdb;

		switch( $query ) {

			case 'oms_product_data_import' :

				$query_results = $wpdb->get_col( "
					SELECT pr.ID
					FROM wp_ovc_products pr
					WHERE pr.sync_oms = 1
						AND pr.oms_sku_confirmed = ''
				" );
			break;
			case 'oms_product_data' :

				$query_results = $wpdb->get_col( "
					SELECT pr.ID
					FROM wp_ovc_products pr
					LEFT JOIN wp_ovc_parent_data pa
						ON pr.parent_sku = pa.parent_sku
					LEFT JOIN wp_ovc_style_data st
						ON pr.sku_style = st.style
					LEFT JOIN wp_ovcop_log ol
						ON pr.op_oms_export_unconfirmed = ol.ID
					WHERE pr.sync_oms = 1
						AND ( 
							pr.op_oms_export_unconfirmed = 0 
							OR GREATEST( pr._data_updated, pr._price_updated, pa._meta_updated, st._meta_updated ) > ol.start_time 
						)
				" );


			break;
			case 'walmart_feeds':

				$query_results = $wpdb->get_col( "
					SELECT feed.ID
					FROM wp_ovc_api_feeds feed 
					WHERE feed.status IN( 'SUBMITTED', 'RECEIVED', 'INPROGRESS' )
						OR (
							feed.status = 'INIT'
							AND feed._meta_created < DATE_SUB( NOW(), INTERVAL 2 HOUR )
						)

				" );
			break;

			/*
			 * WC Flow
			 */
			case 'wc_sync':

				// @return product ID
				$query_results = $wpdb->get_col( "
					SELECT pr.ID
					FROM wp_ovc_products pr
					LEFT JOIN wp_ovc_parent_data pa
						ON pr.parent_sku = pa.parent_sku
					LEFT JOIN wp_ovc_style_data st
						ON pr.sku_style = st.style
					LEFT JOIN wp_ovc_image_sets img
						ON pr.image_set = img.ID
					LEFT JOIN wp_ovc_data_fixes fix
						ON pr.ID = fix.ovc_id
					WHERE fix.ovc_id IS NULL 
						AND (
							(
						 		( pr.post_id != 0 OR pr.sync_wc = 1 )
						 		AND GREATEST( pr._data_updated, pr._stock_updated, pr._price_updated, pa._meta_updated, st._meta_updated, img._meta_updated ) > pr.last_wc_sync
						 	) OR (
						 		( pr.sync_wc = 1 AND ( pr.sync_oms = 0 OR pr.oms_sku_confirmed = '' ) )
						 	)
						)
				" );
			break;
			case 'wc_delete_variations':

				// @return wp_post ID
				$query_results = $wpdb->get_col( "
					SELECT ps.ID
					FROM wp_posts ps
					LEFT JOIN wp_postmeta pm
						ON ps.ID = pm.post_id
					LEFT JOIN wp_ovc_products pr
						ON pm.meta_value = pr.ID 
					WHERE ps.post_type = 'product_variation'
						AND pm.meta_key = '_ovc_id'
						AND pr.ID IS NULL
				" );
			break;

			/*
			 * OVC Flow
			 */
			case 'ovc_data_scan':

				// @return product ID
				$query_results = $wpdb->get_col( "
					SELECT pr.ID
					FROM wp_ovc_products pr
					WHERE pr._meta_updated > (
						SELECT MAX( ol.start_time ) 
					    FROM wp_ovcop_log ol 
					    WHERE ol.status = 'complete' 
					    ORDER BY ol.ID DESC 
					    LIMIT 0,1
					)
					ORDER BY pr.ID ASC
				" );
			break;
			case 'ovc_orphaned_image_sets':

				// @return image_set ID
				$query_results = $wpdb->get_col( "
					SELECT img.ID
					FROM wp_ovc_image_sets img
					LEFT JOIN wp_ovc_products pr
						ON pr.image_set = img.ID
					WHERE pr.ID IS NULL
				" );
			break;
			case 'ovc_unused_images':

				$query_results = $wpdb->get_col( "
					SELECT unimgs.ID
					FROM wp_ovc_unattached_img_post_ids unimgs
					WHERE unimgs.in_use = 0
				" );
			break;
 
			/*
			 * Walmart Flow
			 */
			case 'walmart_init_items':

				// @return product ID
				$query_results = $wpdb->get_col( "
					SELECT pr.ID
					FROM wp_ovc_products pr 
					LEFT JOIN wp_ovc_walmart_products wa 
						ON pr.ID = wa.ovc_id 
					WHERE wa.ovc_id IS NULL
						AND pr.sync_walmart = 1
				" );
			break;
			case 'walmart_cancel_items':

				// @return walmart_product ID
				// NOTE!
				// RETURNS wp_ovc_walmart_products ID (wa.ID) instead of OVC ID (pr.ID)!
				$query_results = $wpdb->get_col( "
					SELECT wa.ID
					FROM wp_ovc_walmart_products wa 
					LEFT JOIN wp_ovc_products pr 
						ON wa.ovc_id = pr.ID 
					WHERE wa.sync_status IN('PENDING','CREATE_FAIL') 
						AND (
							pr.sync_walmart = 0
							OR pr.ID IS NULL
						)
				" );
			break;
			case 'walmart_retire_items':

				// @return walmart_product ID
				// NOTE!
				// RETURNS wp_ovc_walmart_products ID (wa.ID) instead of OVC ID (pr.ID)!
				$query_results = $wpdb->get_col( "
					SELECT wa.ovc_id
					FROM wp_ovc_walmart_products wa 
					LEFT JOIN wp_ovc_products pr 
						ON wa.ovc_id = pr.ID 
					WHERE (
						wa.manual_action = '' 
						OR wa.manual_action IS NULL
					)
					AND (
						wa.sync_status = 'DUPLICATE_RETIRE' 
						OR (
							wa.sync_status IN('ACTIVE','CREATE_FAIL','UPDATE_FAIL') 
							AND ( pr.ID IS NULL OR pr.sync_walmart = 0 )
						)
					)
				" );
			break;
			case 'walmart_images_requiring_sync':

				// @return image ID
				$query_results = $wpdb->get_col( "
					SELECT pic.ID
					FROM wp_ovc_images pic
					WHERE ( 
						( pic.last_wa_ftp_sync < pic._meta_updated )
						OR 
						( pic.last_wa_ftp_sync IS NULL )
					)
					AND ( pic.status = 'Approved' )
				" );
			break;

			/*
			 * Walmart Sync Flow
			 */
			case 'walmart_bulk_update_skus':

				// @return walmart_product ID
				// NOTE!
				// RETURNS wp_ovc_walmart_products ID (wa.ID) instead of OVC ID (pr.ID)!
				$query_results = $wpdb->get_col( "
					SELECT wa.ovc_id
					FROM wp_ovc_walmart_products wa
					LEFT JOIN wp_ovc_products pr
						ON pr.ID = wa.ovc_id
					WHERE (
						wa.ovc_id != 0
						AND wa.sku != pr.sku
						AND wa.sync_status IN('ACTIVE','UPDATE_FAIL')
						AND ( 
							wa.manual_action = '' 
							OR wa.manual_action IS NULL
						)
					)
					OR (
						wa.manual_action IN ('UPDATE_SKU')
						AND wa.override_sku IS NOT NULL
						AND wa.override_sku != ''
					)
				" );
			break;
			case 'walmart_bulk_create_items':

				// @return walmart_product OVC ID
				// NOTE!
				// RETURNS wp_ovc_walmart_products OVC ID (wa.ovc_id) instead of OVC ID (pr.ID)!
				$query_results = $wpdb->get_col( "
					SELECT wa.ovc_id
					FROM wp_ovc_walmart_products wa 
					WHERE wa.ovc_id != 0 
						AND wa.sync_status IN('PENDING','CREATE_FAIL','RETIRED') 
						AND ( 
							wa.manual_action = '' 
							OR wa.manual_action IS NULL 
						)
				" );
			break;
			case 'walmart_bulk_update_items':

				// @return walmart_product OVC ID
				// NOTE!
				// RETURNS wp_ovc_walmart_products OVC ID (wa.ovc_id) instead of OVC ID (pr.ID)!
				$query_results = $wpdb->get_col( "
					SELECT wa.ovc_id
					FROM wp_ovc_walmart_products wa
					LEFT JOIN wp_ovc_products pr 
						ON wa.ovc_id = pr.ID 
					LEFT JOIN wp_ovc_api_feeds feed 
						ON wa.last_item_feed_id = feed.ID 
					LEFT JOIN wp_ovc_parent_data pa 
						ON pr.parent_sku = pa.parent_sku 
					LEFT JOIN wp_ovc_style_data st 
						ON pr.sku_style = st.style 
					LEFT JOIN wp_ovc_image_sets img 
						ON pr.image_set = img.ID 
					WHERE wa.ovc_id != 0
						AND ( 
					 		wa.manual_action = '' 
					 		OR wa.manual_action IS NULL 
					 		OR wa.manual_action IN ('UPDATE_SKU') 
					 	) AND ( 
					 		wa.sync_status = 'UPDATE_FAIL' 
					 		OR pr.sku != wa.sku
							OR GREATEST( pr._data_updated, pa._meta_updated, st._meta_updated, img._meta_updated ) > feed._meta_submitted 
							OR (
								wa.sync_status IN('UPDATING', 'UPDATE_CONFIRM')
								AND feed.status IN('ERROR', 'FEED_ERROR')
							)
					 	)
				" );
			break;
			case 'walmart_bulk_update_items_all':

				// @return walmart_product OVC ID
				// NOTE!
				// RETURNS wp_ovc_walmart_products OVC ID (wa.ovc_id) instead of OVC ID (pr.ID)!
				$query_results = $wpdb->get_col( "
					SELECT wa.ovc_id
					FROM wp_ovc_walmart_products wa
					LEFT JOIN wp_ovc_products pr 
						ON wa.ovc_id = pr.ID 
					WHERE wa.ovc_id != 0
						AND ( 
					 		wa.manual_action = '' 
					 		OR wa.manual_action IS NULL 
					 		OR wa.manual_action IN ('UPDATE_SKU') 
					 	) AND ( 
					 		wa.sync_status IN('ACTIVE','UPDATING','UPDATE_FAIL','UPDATE_CONFIRM')
					 		OR pr.sku != wa.sku
					 	)
				" );
			break;
			case 'walmart_bulk_update_manual':

				// @return walmart_product OVC ID
				// NOTE!
				// RETURNS wp_ovc_walmart_products OVC ID (wa.ovc_id) instead of OVC ID (pr.ID)!
				$query_results = $wpdb->get_col( "
					SELECT wa.ovc_id
					FROM wp_ovc_walmart_products wa
					WHERE wa.manual_action NOT IN('','PAUSE_SYNC','UPDATE_SKU_CONFIRM','UPDATE_UPC_CONFIRM') 
						AND wa.manual_action IS NOT NULL
				" );
			break;
			case 'walmart_bulk_update_prices':

				// @return walmart_product ID
				// NOTE!
				// RETURNS wp_ovc_walmart_products ID (wa.ID) instead of OVC ID (pr.ID)!
				$query_results = $wpdb->get_col(
					"
					SELECT wa.ovc_id
					FROM wp_ovc_walmart_products wa
					LEFT JOIN wp_ovc_products pr
						ON wa.ovc_id = pr.ID
					LEFT JOIN wp_ovc_api_feeds feed 
						ON wa.last_price_feed_id = feed.ID 
					WHERE pr.price_walmart != wa.price 
						AND ( 
							wa.manual_action = '' 
							OR wa.manual_action IS NULL 
						)
						AND wa.product_status = 'PUBLISHED' 
						AND wa.sync_status IN('ACTIVE','UPDATE_FAIL','UPDATE_CONFIRM') 
						AND ( 
							wa.last_price_feed_id = 0 
							OR feed.status IN('PROCESSED','ERROR', 'FEED_ERROR','INIT','OUTDATED') 
						)
				" );
			break;
			case 'walmart_bulk_update_inventory':

				// @return walmart_product ID
				// NOTE!
				// RETURNS wp_ovc_walmart_products ID (wa.ID) instead of OVC ID (pr.ID)!
				$query_results = $wpdb->get_col( "
					SELECT wa.ovc_id
					FROM wp_ovc_walmart_products wa
					LEFT JOIN wp_ovc_products pr
						ON wa.ovc_id = pr.ID 
					LEFT JOIN wp_ovc_api_feeds feed
						ON wa.last_inventory_feed_id = feed.ID
					WHERE wa.product_status = 'PUBLISHED'
						AND ( wa.manual_action = '' OR wa.manual_action IS NULL )
						AND wa.sync_status IN('ACTIVE','UPDATING','UPDATE_FAIL') 
						AND (
							pr._stock_updated > feed._meta_submitted
							OR wa.last_inventory_feed_id = 0
						)
				" );
			break;
			case 'walmart_bulk_update_inventory_all':

				// @return walmart_product ID
				// NOTE!
				// RETURNS wp_ovc_walmart_products ID (wa.ID) instead of OVC ID (pr.ID)!
				$query_results = $wpdb->get_col( "
					SELECT wa.ovc_id
					FROM wp_ovc_walmart_products wa
					WHERE wa.product_status = 'PUBLISHED'
						AND ( 
							wa.manual_action = '' 
							OR wa.manual_action IS NULL 
						)
						AND wa.sync_status IN('ACTIVE','UPDATING','UPDATE_FAIL') 
				" );
			break;

			/*
			 * Shopify Flow
			 */
			case 'shopify_sync':

				// @return product ID
				$query_results = $wpdb->get_col( "
					SELECT pr.ID
					FROM wp_ovc_products pr
					LEFT JOIN wp_ovc_image_sets img
					ON pr.image_set = img.ID
					LEFT JOIN wp_ovc_style_data st
					ON pr.sku_style = st.style
					LEFT JOIN wp_ovc_shopify_products sypr
					ON pr.ID = sypr.ovc_id
					LEFT JOIN wp_ovc_parent_data pa
					ON pr.parent_sku = pa.parent_sku
					LEFT JOIN wp_ovc_shopify_parents sypas
					ON pa.ID = sypas.shopify_parent_id
					WHERE ( 
							pr.sync_shopify = 1 
							AND (
						 		sypr.ovc_id IS NULL
						 		OR GREATEST( pr._data_updated, pr._stock_updated, pr._price_updated, pa._meta_updated, st._meta_updated, img._meta_updated ) > sypr.last_sync
						 	)
						) OR (
							pr.sync_shopify = 0 
							AND ( 
								sypr.ID IS NOT NULL 
								OR sypas.ID IS NOT NULL 
							)
						)
				" );
			break;
			case 'shopify_sync_all':

				// @return shopify_parents ID
				$query_results = $wpdb->get_col( "
					SELECT sypas.ID
					FROM wp_ovc_shopify_parents sypas
				" );
			break;

			case 'shopify_create_parents':

				// @return parent_data ID
				$query_results = $wpdb->get_col( "
					SELECT DISTINCT( pa.ID )
					FROM wp_ovc_parent_data pa 
					LEFT JOIN wp_ovc_products pr 
						ON pa.parent_sku = pr.parent_sku 
					LEFT JOIN wp_ovc_shopify_parents sypas 
						ON pa.ID = sypas.ovc_parent_id
					WHERE pr.sync_shopify = 1 
						AND (
					 		sypas.ID IS NULL
					 		OR ( 
					 			sypas.shopify_parent_id = '' 
					 			AND sypas.errors = ''
					 		)
					 	)
				" );	
			break;
			case 'shopify_delete_parents':

				// @return shopify_parents ID
				$query_results = $wpdb->get_col( "
					SELECT DISTINCT( sypa.ID )
					FROM wp_ovc_shopify_parents sypa
					LEFT JOIN wp_ovc_parent_data pa
						ON pa.ID = sypa.ovc_parent_id
					LEFT JOIN wp_ovc_products pr
						ON pr.parent_sku = pa.parent_sku
						AND pr.sync_shopify = 1
					GROUP BY pa.ID
					HAVING COUNT( pr.ID ) = 0
				" );
			break;
			case 'shopify_update_parents':
 
				// @return shopify_parents ID
				$query_results = $wpdb->get_col( "
					SELECT DISTINCT( sypas.ID )
					FROM wp_ovc_shopify_parents sypas 
					LEFT JOIN wp_ovc_parent_data pa 
						ON sypas.ovc_parent_id = pa.ID 
					LEFT JOIN wp_ovc_products pr 
						ON pa.parent_sku = pr.parent_sku 
					LEFT JOIN wp_ovc_shopify_products sypr 
						ON pr.ID = sypr.ovc_id
					LEFT JOIN wp_ovc_style_data st 
						ON pr.sku_style = st.style
					LEFT JOIN wp_ovc_image_sets img 
						ON pr.image_set = img.ID
					LEFT JOIN wp_ovc_shopify_images syimgs 
						ON syimgs.shopify_parent_id = sypas.ID 
					WHERE pr.sync_shopify = 1
						AND sypas.errors = ''
						AND ( 
							sypr.ID IS NULL 
					 		OR GREATEST( st._meta_updated, pa._meta_updated, img._meta_updated, syimgs._meta_updated, pr._stock_updated, pr._price_updated ) > sypas.last_sync
					 		OR sypr.shopify_image_id = ''
					 	)
				" );
			break;
			case 'shopify_delete_variants':

				// @return shopify_products ID
				$query_results = $wpdb->get_col( "
					SELECT DISTINCT( sypr.ID )
					FROM wp_ovc_shopify_products sypr 
					LEFT JOIN wp_ovc_products pr
						ON sypr.ovc_id = pr.ID
					WHERE pr.ID IS NULL
						OR (
							pr.sync_shopify = 0 
							AND sypr.ID IS NOT NULL 
						)
				" );
			break;
			case 'shopify_update_variants':

				// @return shopify_products ID
				$query_results = $wpdb->get_col( "
					SELECT DISTINCT( sypr.ID )
					FROM wp_ovc_shopify_products sypr 
					LEFT JOIN wp_ovc_products pr
						ON sypr.ovc_id = pr.ID
					LEFT JOIN wp_ovc_shopify_images syimgs
						ON sypr.shopify_image_id = syimgs.shopify_image_id
					LEFT JOIN wp_ovc_shopify_parents sypas 
						ON sypr.shopify_parent_id = sypas.shopify_parent_id
					WHERE pr.sync_shopify = 1 
						AND (
							GREATEST( pr._data_updated, pr._stock_updated, pr._price_updated ) > sypr.last_sync
							OR (
						 		sypr.shopify_image_id = '' 
						 		AND sypas.last_sync > sypr.last_sync
						 	)
						)
				" );			
			break;
			case 'shopify_delete_images':
				$query_results = $wpdb->get_col( "
					SELECT DISTINCT( syimg.ID )
					FROM wp_ovc_shopify_images syimg
					LEFT JOIN wp_ovc_shopify_parents sypa
						ON sypa.ID = syimg.shopify_parent_id
					WHERE sypa.ID IS NULL
				" );
			break;
			case 'shopify_variant_images':

				// @return shopify_products ID
				$query_results = $wpdb->get_col( "
					SELECT DISTINCT( sypr.ID )
					FROM wp_ovc_shopify_products sypr 
					LEFT JOIN wp_ovc_products pr 
						ON sypr.ovc_id = pr.ID
					LEFT JOIN wp_ovc_image_sets img
						ON pr.image_set = img.ID
					WHERE pr.sync_shopify = 1
						AND sypr.variant_id != ''
						AND img._meta_updated > sypr.last_image_sync
				" );
			break;

			/*
			 * OMS Flow
			 */
			case 'oms_global_change_skus':

				// @return products ID
				$query_results = $wpdb->get_col( "
					SELECT pr.ID
					FROM wp_ovc_products pr
					WHERE pr.sku != pr.oms_sku_confirmed
						AND pr.oms_sku_confirmed != ''
				" );
			break;

			/*
			 * Various
			 */
			case 'case_sku_children_count':

				// @return case SKU, children count
				$query_results = $wpdb->get_results( "
					SELECT case_sku, COUNT(ID) as children_count
					FROM wp_ovc_products
					WHERE case_sku != ''
						AND case_sku NOT LIKE '%_CS'
						AND sku != case_sku
					GROUP BY case_sku
				" );
			break;

			default:
				return false;
			break;
		}

		static::$query_results[ $query ] = $query_results;
	}

	public static function get_ids( $query = null, $force_refresh = false ) {

		if( $query && array_key_exists( $query, static::$query_results ) ) {

			if( !static::$query_results[ $query ] || $force_refresh ) {

				static::get_results( $query, true );
			}

			return static::$query_results[ $query ] ?: false;
		}

		return false;
	}

	public static function get_count( $query = null, $force_refresh = false ) {

		if( $query && array_key_exists( $query, static::$query_results ) ) {

			if( !static::$query_results[ $query ] || $force_refresh ) {

				static::get_results( $query, false );
			}

			return static::$query_results[ $query ] !== false ? count( static::$query_results[ $query ] ) : false;
		}

		return false;
	}

	public static function get_auto_sync_count( $sync_type = null, $return_ids = false ) {

		if( $return_ids ) {
			return static::get_ids( $sync_type );
		}

		return static::get_count( $sync_type );
	}

	public static function maybe_create_view( $view_suffix, $force_create = false ) {

		global $wpdb;

		$view_name = 'wp_ovc_view_' . $view_suffix;

		if( !$force_create && $wpdb->get_var( "SELECT COUNT(*) FROM INFORMATION_SCHEMA.VIEWS WHERE table_schema = '{$wpdb->dbname}' AND table_name = '{$view_name}'" ) ) {
			return true;
		}

		$create_view_query = "CREATE OR REPLACE ALGORITHM = UNDEFINED VIEW `{$view_name}` ";

		switch( $view_suffix ) {

			/**
			 * Syncs
			 */
			case 'wc_sync':
			// @return product ID
$create_view_query .= 
"(ID)
 AS SELECT pr.ID
 FROM wp_ovc_products pr
 LEFT JOIN wp_ovc_parent_data pa
 ON pr.parent_sku = pa.parent_sku
 LEFT JOIN wp_ovc_style_data st
 ON pr.sku_style = st.style
 LEFT JOIN wp_ovc_image_sets img
 ON pr.image_set = img.ID
 LEFT JOIN wp_ovc_data_fixes fix
 ON pr.ID = fix.ovc_id
 WHERE fix.ovc_id IS NULL 
 AND (
	 (
	 	( pr.post_id != 0 OR pr.sync_wc = 1 )
	 	AND GREATEST( pr._data_updated, pr._stock_updated, pr._price_updated, pa._meta_updated, st._meta_updated, img._meta_updated ) > pr.last_wc_sync
	 ) OR (
	 	( pr.sync_wc = 1 AND ( pr.sync_oms = 0 OR pr.oms_sku_confirmed = '' ) )
	 )
 )";
			break;
			case 'shopify_sync':
			// @return product ID
$create_view_query .= 
"(ID)
 AS SELECT pr.ID
 FROM wp_ovc_products pr
 LEFT JOIN wp_ovc_image_sets img
 ON pr.image_set = img.ID
 LEFT JOIN wp_ovc_style_data st
 ON pr.sku_style = st.style
 LEFT JOIN wp_ovc_shopify_products sypr
 ON pr.ID = sypr.ovc_id
 LEFT JOIN wp_ovc_parent_data pa
 ON pr.parent_sku = pa.parent_sku
 LEFT JOIN wp_ovc_shopify_parents sypas
 ON pa.ID = sypas.shopify_parent_id
 WHERE 
 ( pr.sync_shopify = 1 
 	AND (
 		sypr.ovc_id IS NULL
 		OR GREATEST( pr._data_updated, pr._stock_updated, pr._price_updated, pa._meta_updated, st._meta_updated, img._meta_updated ) > sypr.last_sync
 	)
 )
 OR (
 	pr.sync_shopify = 0 
 	AND ( sypr.ID IS NOT NULL OR sypas.ID IS NOT NULL )
 )";
			break;
			case 'shopify_create_parents':
			// @return parent_data ID
$create_view_query .= 
"(ID)
AS SELECT DISTINCT( pa.ID )
 FROM wp_ovc_parent_data pa 
 LEFT JOIN wp_ovc_products pr 
 ON pa.parent_sku = pr.parent_sku 
 LEFT JOIN wp_ovc_shopify_parents sypas 
 ON pa.ID = sypas.ovc_parent_id
 WHERE pr.sync_shopify = 1 
 AND (
 	sypas.ID IS NULL
 	OR ( 
 		sypas.shopify_parent_id = '' 
 		AND sypas.errors = ''
 	)
 )";	
			break;
			case 'shopify_update_parents':
			// @return shopify_parents ID
$create_view_query .= 
"(ID)
AS SELECT DISTINCT( sypas.ID )
 FROM wp_ovc_shopify_parents sypas 
 LEFT JOIN wp_ovc_parent_data pa 
 ON sypas.ovc_parent_id = pa.ID 
 LEFT JOIN wp_ovc_products pr 
 ON pa.parent_sku = pr.parent_sku 
 LEFT JOIN wp_ovc_shopify_products sypr 
 ON pr.ID = sypr.ovc_id
 LEFT JOIN wp_ovc_style_data st 
 ON pr.sku_style = st.style
 LEFT JOIN wp_ovc_image_sets img 
 ON pr.image_set = img.ID
 LEFT JOIN wp_ovc_shopify_images syimgs 
 ON syimgs.shopify_parent_id = sypas.ID 
 WHERE pr.sync_shopify = 1
 AND sypas.errors = ''
 AND ( 
	sypr.ID IS NULL 
 	OR GREATEST( st._meta_updated, pa._meta_updated, img._meta_updated, syimgs._meta_updated, pr._stock_updated, pr._price_updated ) > sypas.last_sync
 	OR sypr.shopify_image_id = ''
 )";	
			break;
			case 'ovc_data_scan':
			// @return product ID
$create_view_query .= 
"(ID)
 AS SELECT pr.ID
  FROM wp_ovc_products pr
  WHERE pr._meta_updated > (
    SELECT MAX( ol.start_time ) FROM wp_ovcop_log ol WHERE ol.status = 'complete' ORDER BY ol.ID DESC LIMIT 0,1
  )
  ORDER BY pr.ID ASC";	
			break;
			case 'oms_global_change_skus':
			// @return products ID
$create_view_query .=
"(ID)
 AS SELECT pr.ID
 FROM wp_ovc_products pr
 WHERE pr.sku != pr.oms_sku_confirmed
 AND pr.oms_sku_confirmed != ''
 ";
			break;

			default:
				return false;
			break;
		}

		return $wpdb->query( $create_view_query );
	}
}