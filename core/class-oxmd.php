<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OXMD {
	public $args = false;
	public $template_html = "";

	public function __construct( $args ) {
		$this->args = $args;
		$this->load_template( $args['template'] );
	}

	public function load_template( $template ) {
		$template_html = $this->get_include_contents( OVC_PATH . "/templates/oxmd/oxmd-{$template}.php" );

		$this->template_html = ( $template_html !== false ? $template_html : "Failed to load OXMD template: {$template}" );
	}

	private function get_include_contents( $filename ) {
	    if (is_file($filename)) {
	        ob_start();
	        include $filename;
	        return ob_get_clean();
	    }
	    return false;
	}

	public function ajax_response() {
		$response = $this->args;
		$response['template_html'] = $this->template_html;

		global $oxmd_extra_response_data;
		if( isset( $oxmd_extra_response_data ) ) {
			$response['extra_data'] = $oxmd_extra_response_data;
		}

		return $response;
	}
}