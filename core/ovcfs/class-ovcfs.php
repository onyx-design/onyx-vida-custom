<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVCFS {

	protected $name;

	protected $field_set_meta;
	
	protected $field_meta;

	protected $field_objects = null;

	protected $primary_key = null;

	protected $joins = array(
		'child'	=> array(),
		'parent'	=> array()
	);


	/*
	public $field_set 	= 'ovc_products';
	public $nice_name 	= 'OVC Products';

	protected $field_objects 	= array();
	
	public $alias 		= null;
	public $p_key		= null;

	public $invalid_msg = false;
	
	protected $raw_fm 	= null; // Full Raw Field Set Meta, including metameta
	protected $fs_meta 	= null; // Field Set 'metameta' (Field Set Meta with field __meta)
	protected $fm 		= null; // Full Field Set Meta without metameta
	*/


	public function __construct( $field_set_name, $field_meta, $field_set_meta ) {
		
		$this->name = $field_set_name;

		$this->field_meta = $field_meta;

		$this->field_set_meta = $field_set_meta;
		
	}



	public function __get( $var_name ) {

		if( 'name' == $var_name ) {
			return $this->name;
		}
		else if( 'nice_name' == $var_name ) {
			return oxd( $this->fs_meta( 'nice_name' ), $this->name );
		}

		// Check if the requested property is a field_set_meta key
		$fs_meta_value = $this->fs_meta( $var_name );
		if( isset( $fs_meta_value ) ) {

			return $fs_meta_value;
		}
		else if( 'alias' == $var_name ) {
		
			return $this->fs_meta( 'sql_alias' );
		}
		else if( 'p_key' == $var_name || 'pKey' == $var_name || 'pkey' == $var_name ) { // dev:improve

			return $this->fs_meta('primary_key') ?? 'ID';
		}
	}

	public function __toString() {
		return $this->name;
	}

	public function fs_meta( $meta_key = null ) {

		if( empty( $meta_key ) ) {

			return $this->field_set_meta;
		}
		else if ( isset( $this->field_set_meta[ $meta_key ] ) ) {

			return $this->field_set_meta[ $meta_key ];
		}
		else if ( 'type' == $meta_key ) {
			// if we got here it means field_set_meta['type'] is not set, so send field_set_name
			return $this->name;
		}
		else if ( 
			'ovc_join' == $this->type 
			&& array_key_exists( $meta_key, self::$join_meta_defaults ) 
		) {

			return self::$join_meta_defaults[ $meta_key ];
		}
		else return null;
	}

	public function has_field( $field_local_name ) {

		return isset( $this->field_meta[ $field_local_name ] );

	}

	public function is_local( $reference ) {

		if( is_string( $reference ) ) {

			if( $reference == $this->name
				|| $reference == $this->alias
				|| $this->has_field( $reference ) 
			) {
				return true;
			}
			else if( $field = OVCSC::get_field( $reference, $this ) ) {
				return $this->is_local( $field );
			}
		}
		else if( 
			( $reference instanceof OVCFS && $reference->name == $this->name )
			|| ( $reference instanceof OVC_Field && $reference->fs_alias == $this->alias ) 
		) {
			return true;
		} 

		return false;
	}

	public function get_field_object( $field ) {

		$field_local_name = $field instanceof OVC_Field ? $field->local_name : $field;

		if( $this->has_field( $field_local_name ) && $this->get_fields() ) {

			$field = $this->field_objects[ $field_local_name ];

			return $field;
		}
			
		return false;
	}

	public function get_fields( $format = 'object' ) {

		if( 'local_name' == $format || 'name' == $format ) {

			$field_names_local = array_keys( $this->field_meta );

			if( 'local_name' == $format ) {

				return $field_names_local;
			}
			else { // $format = 'name'

				$field_names_global = array();
				$fs_alias 			= $field_set->alias;

				foreach( $field_names_local as $field_name_local ) {

					$field_names_global[] = $fs_alias . '.' . $field_name_local;
				}

				return $field_names_global;
			}
		}
		else if( 'object' == $format ) {

			// Maybe initialize field set field_objects
			if( empty( $this->field_objects ) ) {

				$this->field_objects = array();

				foreach( $this->field_meta as $field => $meta ) {

					$this->field_objects[ $field ] = new OVC_Field( $this, $field, $meta );
				}
			}

			return $this->field_objects;
		}
	}
	


	public function field_meta( $field = null, $meta_key = null ) {
		
		if( !isset( $field ) && !isset( $meta_key ) ) {

			return $this->field_meta;
		}
		else if( $field = $this->get_field_object( $field ) ) {

			return $field->meta( $meta_key );
		}
	}


	/**
	 * Get an array of values for a single meta_key, optionally excluding fields that don't have the key
	 * 
	 * @param string 			$meta_key
	 * @param bool 				$full_meta
	 * @param bool 				$keep_blank_values
	 * 
	 * @return array
	 **/
	public function get_single_meta_key_values( $meta_key = '', $full_meta = false, $keep_blank_values = false, $use_full_field_name = false ) {

		$field_set_meta_values = array();

		foreach( $this->get_fields() as $field ) {

			$field_name = $use_full_field_name ? $field->global_name : $field->local_name;

			//$field = $use_full_field_name ? "{$field_set->alias}.{$field}" : $field;
			
			$meta_value = $field->meta( $meta_key );

			if( isset( $meta_value ) || $keep_blank_values ) {

				$field_set_meta_values[ $field_name ] = $full_meta ? $field->meta() : $meta_value ;
			}
		}

		return $field_set_meta_values;

	}

	// Get row class for this field set
	public function get_row_class() {
		return class_exists( "OVC_Row_{$this}" ) ? "OVC_Row_{$this}" : "OVC_Row";
	}


	  /////
	 // FIELD SET TYPE: DATABASE
	//

	/**
	 * Get the name of this field set's database table
	 * 
	 * @return mixed
	 **/
	public function table_name() {
		return $this->fs_meta( 'table_name' );
	}

	
	

	// public function get_primary_key( $format = 'field' ) {

	// 	//dev:remove
	// 	if( 'ovc_img_orphans' == $this->name ) {
	// 		return OVCSC::get_field( 'ior.ID' );
	// 	}
		
	// 	if( 'database' == $this->type 
	// 		&& !$this->primary_key
	// 	) {
			
	// 		$this->primary_key = OVCSC::get_field( OVCDB()->get_table_primary_key( $this->table_name() ), $this );	

	// 		if( $this->primary_key 
	// 			&& 'field' != $format 
	// 			&& $this->primary_key->$format 
	// 		) {
	// 			return $this->primary_key->$format;
	// 		}
	// 	}

	// 	return $this->primary_key;
	// }
	
	// /**
	//  * Get
	//  * 
	//  * 
	//  **/
	// public function get_ref_relation( OVCFS $ref_fs ) {

	// 	$tbl_refs_children 	= $this->fs_meta( 'tbl_refs_children' );
	// 	$tbl_refs_parents 	= $this->fs_meta( 'tbl_refs_parents' );

	// 	if( $tbl_refs_children && in_array( $ref_fs->name, $tbl_refs_children ) ) {
	// 		return 'child';
	// 	}
	// 	else if( $tbl_refs_parents && in_array( $ref_fs->name, $tbl_refs_parents ) ) {
	// 		return 'parent';
	// 	}

	// 	return false;
	// }

	// public function get_join( $ref_field_set = '', $child_field = false ) {

	// 	$join_obj = false;
		
	// 	if( !OVCSC::init_field_set( $ref_field_set ) ) {
	// 		return false;
	// 	}

	// 	$ref_relation = $this->get_ref_relation( $ref_field_set );

	// 	if( !$ref_relation ) {
	// 		return false;
	// 	}
	// 	else if( 'child' == $ref_relation ) {

	// 		$parent_fs = $this;
	// 		$child_fs = $ref_field_set;

			
	// 	}
	// 	else if( 'parent' == $ref_relation ) {
			
	// 		$parent_fs = $ref_field_set;
	// 		$child_fs = $this;
	// 	}




	// 	// Check to see if join has already been created
	// 	if( isset( $this->joins[ $ref_relation ][ $ref_field_set->name ] ) ) {

	// 		// If child field wasn't supplied, then this is probably the only join to the foreign table
	// 		if( !$child_field 
	// 			&& 1 == count( $this->joins[ $ref_relation ][ $ref_field_set->name ] ) 
	// 		) {
	// 			$join = array_values( $this->joins[ $ref_relation ][ $ref_field_set->name ] )[0];

	// 			if( $join instanceof OVC_Join ) {

	// 				$join_obj = $join;
	// 			}
	// 		}
	// 		// If $child_field was supplied and is valid, then let's try to make the join id
	// 		else if( 
	// 			$child_field_obj = OVCSC::get_field( $child_field ) 
	// 			&& $parent_field = OVCSC::get_field( $child_field_obj->meta( 'foreign_key' ), $parent_fs )
	// 		) {

	// 			$join_id = $child_fs->alias . "-" . $child_field_obj->local_name . "--" . $parent_fs->alias . "-" . $parent_field->local_name;

	// 			if( isset( $this->joins[ $ref_relation ][ $ref_field_set->name ][ $join_id ] ) 
	// 				&& $this->joins[ $ref_relation ][ $ref_field_set->name ][ $join_id ] instanceof OVC_Join
	// 			) {

	// 				$join_obj = $this->joins[ $ref_relation ][ $ref_field_set->name ][ $join_id ];
	// 			} 
	// 		}


	// 	}
	// 	else {

	// 		$this->joins[ $ref_relation ] = array( $ref_field_set->name => array() );
	// 	}

	// 	// Need to initialize the join
	// 	if( !$join_obj ) {

	// 		$results = new OVC_Results();

	// 		$join = new OVC_Join( $parent_fs, $child_fs, $child_field, $results );


	// 		if( $join->is_valid ) {

	// 			$join_obj = $join;

	// 			$this->joins[ $ref_relation ][ $ref_field_set->name ][ $join->get_join_id() ] = $join_obj;
	// 		}
	// 	}

	// 	return $join_obj;
		

	// 	//if( $field_set->name == $this->name ) 
	// } 

}