<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVCFS_Join extends OVCFS {

	protected $name;

	protected $field_set_meta;
	
	protected $field_meta;

	protected $field_objects = null;

	protected $primary_key = null;

	protected $joins = array(
		'child'	=> array(),
		'parent'	=> array()
	);


	/*
	public $field_set 	= 'ovc_products';
	public $nice_name 	= 'OVC Products';

	protected $field_objects 	= array();
	
	public $alias 		= null;
	public $p_key		= null;

	public $invalid_msg = false;
	
	protected $raw_fm 	= null; // Full Raw Field Set Meta, including metameta
	protected $fs_meta 	= null; // Field Set 'metameta' (Field Set Meta with field __meta)
	protected $fm 		= null; // Full Field Set Meta without metameta
	*/


	public function __construct( $field_set_name, $field_meta, $field_set_meta ) {
		
		$this->name = $field_set_name;

		$this->field_meta = $field_meta;

		$this->field_set_meta = $field_set_meta;
		
	}



	
	/**
	 * Get
	 * 
	 * 
	 **/
	public function get_ref_relation( OVCFS $ref_fs ) {

		$tbl_refs_children 	= $this->fs_meta( 'tbl_refs_children' );
		$tbl_refs_parents 	= $this->fs_meta( 'tbl_refs_parents' );

		if( $tbl_refs_children && in_array( $ref_fs->name, $tbl_refs_children ) ) {
			return 'child';
		}
		else if( $tbl_refs_parents && in_array( $ref_fs->name, $tbl_refs_parents ) ) {
			return 'parent';
		}

		return false;
	}

	public function get_join( $ref_field_set = '', $child_field = false ) {

		$join_obj = false;
		
		if( !OVCSC::init_field_set( $ref_field_set ) ) {
			return false;
		}

		$ref_relation = $this->get_ref_relation( $ref_field_set );

		if( !$ref_relation ) {
			return false;
		}
		else if( 'child' == $ref_relation ) {

			$parent_fs = $this;
			$child_fs = $ref_field_set;

			
		}
		else if( 'parent' == $ref_relation ) {
			
			$parent_fs = $ref_field_set;
			$child_fs = $this;
		}




		// Check to see if join has already been created
		if( isset( $this->joins[ $ref_relation ][ $ref_field_set->name ] ) ) {

			// If child field wasn't supplied, then this is probably the only join to the foreign table
			if( !$child_field 
				&& 1 == count( $this->joins[ $ref_relation ][ $ref_field_set->name ] ) 
			) {
				$join = array_values( $this->joins[ $ref_relation ][ $ref_field_set->name ] )[0];

				if( $join instanceof OVC_Join ) {

					$join_obj = $join;
				}
			}
			// If $child_field was supplied and is valid, then let's try to make the join id
			else if( 
				$child_field_obj = OVCSC::get_field( $child_field ) 
				&& $parent_field = OVCSC::get_field( $child_field_obj->meta( 'foreign_key' ), $parent_fs )
			) {

				$join_id = $child_fs->alias . "-" . $child_field_obj->local_name . "--" . $parent_fs->alias . "-" . $parent_field->local_name;

				if( isset( $this->joins[ $ref_relation ][ $ref_field_set->name ][ $join_id ] ) 
					&& $this->joins[ $ref_relation ][ $ref_field_set->name ][ $join_id ] instanceof OVC_Join
				) {

					$join_obj = $this->joins[ $ref_relation ][ $ref_field_set->name ][ $join_id ];
				} 
			}


		}
		else {

			$this->joins[ $ref_relation ] = array( $ref_field_set->name => array() );
		}

		// Need to initialize the join
		if( !$join_obj ) {

			$results = new OVC_Results();

			$join = new OVC_Join( $parent_fs, $child_fs, $child_field, $results );


			if( $join->is_valid ) {

				$join_obj = $join;

				$this->joins[ $ref_relation ][ $ref_field_set->name ][ $join->get_join_id() ] = $join_obj;
			}
		}

		return $join_obj;
		

		//if( $field_set->name == $this->name ) 
	} 

}