<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * OVC SCHEMA 
 * Handles Field Sets and Field Meta
 */
final class OVCSC {

	//protected static $_instance = null;

	protected static $fieldmeta = null;

	protected static $fs_objects = array();

	protected static $fs_aliases = array();

	protected static $fs_tables = array();

	/*
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	*/

	// public static function migrate_old_schema() {

	// 	global $wpdb;

	// 	$wpdb->query( "TRUNCATE TABLE wp_ovc_entities" );
	// 	$wpdb->query( "ALTER TABLE wp_ovc_entities AUTO_INCREMENT = 1" );

	// 	$wpdb->query( "TRUNCATE TABLE wp_ovc_entitymeta" );
	// 	$wpdb->query( "ALTER TABLE wp_ovc_entitymeta AUTO_INCREMENT = 1" );

	// 	self::init();


	// 	// Add Field Sets to entities
	// 	$ignore_field_sets = array( 'field_set_meta', 'ovcdt', 'ovc_logic', 'ovc_list', 'ovcop' );

	// 	$fs_id_name = array();
	// 	$fs_name_id = array();

	// 	foreach( self::$fieldmeta as $field_set_name => $fs_meta ) {

	// 		if( in_array( $field_set_name, $ignore_field_sets ) ) {
	// 			continue;
	// 		}

	// 		$field_set = self::get_field_set( $field_set_name );
				

	// 		$insert_data = array(
	// 			'parent_id' => 0,
	// 			'name'		=> $field_set->name,
	// 			'code'		=> ( $field_set->alias ? $field_set->alias : $field_set->name ),
	// 			'title'		=> $field_set->nice_name,
	// 			'type'		=> $field_set->type,
	// 			'ordered'	=> ( $field_set->ordered ? 1 : 0 ),
	// 			'entity_order'=> 0
	// 		);

	// 		$wpdb->insert( 'wp_ovc_entities', $insert_data );

	// 		$fs_id = $wpdb->insert_id;
			
	// 		$fs_id_name[ $fs_id ] = $field_set_name;
	// 		$fs_name_id[ $field_set_name ] = intval( $fs_id );

	// 	}

	// 	// Add Field Set Meta to entitymeta
	// 	$ignore_meta = array( 'ordered', 'sql_alias', 'nice_name', 'type' );

	// 	$fs_meta = self::get_field_set( 'field_set_meta' );

	// 	foreach( $fs_meta->get_fields() as $field_set_name => $fs_meta_field ) {

	// 		foreach( $fs_meta_field->meta() as $meta_key => $meta_value ) {

	// 			if( in_array( $meta_key, $ignore_meta ) || !isset( $fs_name_id[ $field_set_name ] ) ) {
	// 				continue;
	// 			}

	// 			$insert_data = array(
	// 				'entity_id'		=> intval( $fs_name_id[ $field_set_name ] ),
	// 				'meta_key'		=> $meta_key,
	// 				'meta_value'	=> maybe_serialize( $meta_value )
	// 			);

	// 			$wpdb->insert( 'wp_ovc_entitymeta', $insert_data );
	// 		}

	// 	}


	// 	// Add fields and their meta

	// 	$ignore_meta = array( 'nice_name', 'order' );

	// 	foreach( self::$fieldmeta as $field_set_name => $fs_meta ) {

	// 		if( 'field_set_meta' == $field_set_name ) {
	// 			continue;
	// 		}

	// 		$field_set = self::get_field_set( $field_set_name );


	// 		// Determine field type
	// 		$field_set_type = $field_set->type;

	// 		$field_type = '';

	// 		switch( $field_set_type ) {
	// 			case 'database':
	// 				$field_type = 'db_field';
	// 			break;
	// 			case 'external':
	// 				$field_type = 'ext_field';
	// 			break;
	// 			case 'ovc_list':
	// 				$field_type = 'list';
	// 			break;
	// 			case 'ovc_logic':
	// 				$field_type = 'logic';
	// 			break;
	// 			default:
	// 				$field_type = $field_set_type;
	// 			break;
	// 		}

	// 		// Determine parent id
	// 		$parent_id = 0;

	// 		if( isset( $fs_name_id[ $field_set_name ] ) ) {
	// 			$parent_id = $fs_name_id[ $field_set_name ];
	// 		}



	// 		foreach( $field_set->get_fields() as $field_name => $field ) {



	// 			$insert_data = array(
	// 				'parent_id'	=> $parent_id,
	// 				'name'		=> $field->local_name,
	// 				'code'		=> $field->local_name,
	// 				'title'		=> $field->nice_name(),
	// 				'type'		=> $field_type,
	// 				'ordered'	=> 0,
	// 				'entity_order'	=> intval( $field->meta( 'order' ) )
	// 			);

	// 			if( !$wpdb->insert( 'wp_ovc_entities', $insert_data ) ) {
	// 				ovc_dev_log( $field->global_name );
	// 			}

	// 			$field_id = intval( $wpdb->insert_id );

	// 			foreach( $field->meta() as $meta_key => $meta_value ) {

	// 				if( in_array( $meta_key, $ignore_meta ) ) {
	// 					continue;
	// 				}

	// 				$insert_data = array(
	// 					'entity_id'		=> $field_id,
	// 					'meta_key'		=> $meta_key,
	// 					'meta_value'	=> maybe_serialize( $meta_value )
	// 				);

	// 				$wpdb->insert( 'wp_ovc_entitymeta', $insert_data );
	// 			}
	// 		}
	// 	}


	// }

	/**
	 * Prevent instantiation
	 */
	private function __construct() {

	}

	public static function init( $force_refresh = false ) {

		self::get_schema( $force_refresh );
	}

	

	protected static function get_schema( $force_refresh = false ) {

		if( $force_refresh || 
			( empty( self::$fieldmeta ) 
				&& !( self::$fieldmeta = wp_cache_get( 'schema', 'ovc' ) ) 
			) 
		) {
			// Re-initialize all properties
			$properties = array( 'fieldmeta', 'fs_objects', 'fs_aliases', 'fs_tables' );
			foreach( $properties as $property ) {
				self::$$property = array();
			}

			global $wpdb;

			// Get full OVC Schema table from database
			$db_fieldmeta = $wpdb->get_results( "SELECT * FROM wp_ovc_schema", ARRAY_A );

			// Convert raw schema table data into usable fieldmeta associative array format
			foreach( $db_fieldmeta as $meta ) {

				$field_set 	= $meta['field_set'];
				$field 		= $meta['field'];

				// Maybe initialize field set index
				if( !isset( self::$fieldmeta[ $field_set ] ) ) {

					self::$fieldmeta[ $field_set ] = array();
					/*
					self::$fieldmeta[ $field_set ] = array(
						//'__meta'		=> array( 'field_set' => $field_set ),	
						'field_ meta'	=> array(),
						'field_objects' => array(),
						'field_set_obj'	=> null
					);
					*/
				}

				self::$fieldmeta[ $field_set ][ $field ][ $meta['meta_key'] ] = maybe_unserialize( $meta['meta_value'] );

				/*
				self::$fieldmeta[ $field_set ]['field_ meta'][ $field ][ $meta['meta_key'] ] = maybe_unserialize( $meta['meta_value'] );

				/*
				if( '__meta' == $field ) {

					self::$fieldmeta[ $field_set ]['__meta'][ $meta['meta_key'] ] = maybe_unserialize( $meta['meta_value'] );
				}
				else {

					self::$fieldmeta[ $field_set ]['field_ meta'][ $field ][ $meta['meta_key'] ] = maybe_unserialize( $meta['meta_value'] );	
				}
				*/
			}


			wp_cache_set( 'schema', self::$fieldmeta, 'ovc' );
	 	}

	 	//ovc_dev_log( self::$fieldmeta, true );
	 	
	 	// Build Field Set Alias and Table Name lookup arrays
	 	foreach( self::$fieldmeta['field_set_meta'] as $field_set_name => $fs_meta ) {

	 		if( isset( $fs_meta['sql_alias'] ) ) {

	 			self::$fs_aliases[ $fs_meta['sql_alias'] ] = $field_set_name;
	 		}

	 		if( isset( $fs_meta['table_name'] ) ) {

	 			self::$fs_tables[ $fs_meta['table_name'] ] = $field_set_name;
	 		}
	 	}

	 	// Always make sure field_set_meta is initialized
	 	self::get_field_set( 'field_set_meta' );

	}


	public static function init_field_set( &$field_set, $force_refresh = false ) {

		// ovc_debug( memory_get_usage() . ' ' . debug_backtrace_str() . ' START - Field Set: ' . $field_set );

		if( $force_refresh || !( $field_set instanceof OVCFS ) ) {
			
			if( $field_set && $field_set_name = self::determine_field_set_name( $field_set ) ) {

				if( $force_refresh 
					|| empty( self::$fs_objects[ $field_set_name ] )
					|| !( self::$fs_objects[ $field_set_name ] instanceof OVCFS )
				) {

					// Initialize Field Set class/subclass name (might be different for different types of field sets)
					$fs_classname = 'OVCFS';

					// Get field_set_meta - Do not use get_field_set_meta method to avoid infinite loop
					$field_set_meta = array();
					if( !empty( self::$fieldmeta['field_set_meta'][ $field_set_name ] ) ) {

						$field_set_meta = self::$fieldmeta['field_set_meta'][ $field_set_name ];

						// Maybe order field set meta
						if( !empty( $field_set_meta['ordered'] ) ) {

							self::order_field_set( $field_set_name );
						}

						// Maybe override field set classname
						if( !empty( $field_set_meta['type'] )
							&& 'database' == $field_set_meta['type']
						) {
							$fs_classname = 'OVCFS_database';
						}
					}

					self::$fs_objects[ $field_set_name ] = new $fs_classname( $field_set_name, self::$fieldmeta[ $field_set_name ], $field_set_meta );
				}

				$field_set = self::$fs_objects[ $field_set_name ]; 
			}
			else {		
				$field_set = null;
			}
		}
		
		

		return $field_set;
	}

	public static function get_field_set( $field_set, $fs_type = null, $force_refresh = false ) {
		self::init_field_set( $field_set, $force_refresh );
		
		// Maybe validate field set type if $fs_type is supplied
		if( empty( $fs_type ) || $field_set->type == $fs_type ) {
			return $field_set;
		}

		return null;
	}

	//Unless you want to get all OVC Schema field_set_meta, it's probably better to get fs_meta from OVCFS
	public static function get_field_set_meta( $field_set = null, $meta_key = null ) {

		// if no parameters sent, return full field_set_meta
		if( !isset( $field_set, $meta_key ) ) {

			return self::$fieldmeta['field_set_meta'];
		}

		// Validate $field_set
		if( self::init_field_set( $field_set ) ) {

			return $field_set->fs_meta( $meta_key );
		}
	}


	public static function init_field( &$field, $field_set = null ) {

		if( !( $field instanceof OVC_Field ) ) {

			// If $field is a global field name, parse it into field and field set
			if( self::field_name_is_global( $field ) ) {

				// Parse ovc_field into pieces
				$field_name_pieces = explode( '.', $field );

				$field_set 	= $field_name_pieces[0]; // actually field_set alias at this point
				$field 		= $field_name_pieces[1];

			} // If field is not global, assume it's local

			// Verify field set & field
			if( $field_set = self::get_field_set( $field_set ) ) {
				
				$field = $field_set->get_field_object( $field );
			}
		}

		if( $field instanceof OVC_Field ) {

			// ovc_debug( memory_get_usage() . ' ' . debug_backtrace_str() . ' - Field: ' . $field->global_name );
			return $field;
		}
		else {
			return false;
		}
	}

	public static function get_field( $field, $field_set = null ) {

		return self::init_field( $field, $field_set );
	}

	

	

	protected static function order_field_set( $field_set_name ) {		

		$field_meta = self::$fieldmeta[ $field_set_name ];

		$ordered_fields = array_combine( array_keys( $field_meta ), array_column( $field_meta, 'order' ) );

		if( !$ordered_fields || count( $ordered_fields ) != count( $field_meta ) ) {
			// dev:error_handling
			ovc_dev_log( "ORDERED FIELDS COUNT MISMATCH - {$field_set_name}" );
		}
		else if( count( $ordered_fields ) != count( array_unique( $ordered_fields ) ) ) {
			// dev:error_handling
			ovc_dev_log( "ORDERED FIELDS DUPLICATE ORDER VALUE ERROR - {$field_set_name}" );
		}
		else {

			asort( $ordered_fields );
			self::$fieldmeta[ $field_set_name ] = array_merge( $ordered_fields, $field_meta );

			/*/ Order object fields if they have been initialized // dev:deprecated
			if( !empty( self::$fieldmeta[ $field_set_name ]['field_objects'] ) ) {

				self::$fieldmeta[ $field_set_name ]['field_objects'] = array_merge( $ordered_fields, self::$fieldmeta[ $field_set_name ]['field_objects'] );
			}
			*/
		}
			
		
	}

	
	

	////////
	// HELPER FUNCTIONS
	////////

	public static function determine_field_set_name( $field_set_id ) {

		// Try the easiest method first...
		if( $field_set_id instanceof OVCFS ) {
			return $field_set_id->name;
		}
		// First check if $field_set_id is already the name of a field set
		if( isset( self::$fieldmeta[ $field_set_id ] ) ) {
			return $field_set_id;
		}
		// Check field set alias lookup array
		else if( isset( self::$fs_aliases[ $field_set_id ] ) ) {
			return self::$fs_aliases[ $field_set_id ];
		}
		// Check table name lookup array
		else if( isset( self::$fs_tables[ $field_set_id ] ) ) {
			return self::$fs_tables[ $field_set_id ];
		}
		// If this is a field or a global field name, get the field set from the name
		else if( $field_set_id instanceof OVC_Field ) {
			return $field_set_id->field_set->name;
		}
		// If this is a global field name, get the field set from the name
		else if( self::field_name_is_global( $field_set_id ) ) {
			$global_field_pieces = explode( '.', $field_set_id );
			return self::determine_field_set_name( $global_field_pieces[0] );
		}
		else {
			return null;
		}
	}


	

	public static function determine_field_name_format( $field_name ) {
		return false === strpos( $field_name, '.' ) ? 'local' : 'global';
	}

	public static function field_name_is_local( $field_name ) {
		return 'local' == self::determine_field_name_format( $field_name ) ? true : false;
	}

	public static function field_name_is_global( $field_name ) {
		return 'global' == self::determine_field_name_format( $field_name ) ? true : false;
	}



	///////
	// Basic field meta retrieval
	/////


	public static function get_field_meta( $field_set = null, $field_name = null, $meta_key = null ) {

		if( self::init_field_set( $field_set ) ) {

			return $field_set->field_meta( $field_name, $meta_key );
		}
	}

	public static function get_meta_value( $field_set, $field_name = null, $meta_key = null ) {
		// dev:improve //figure out how arguments should be passed, i.e. how field should be referenced
		if( $field_name instanceof OVC_Field ) {
			$field_name = $field_name->local_name;
		}

		if( self::init_field_set( $field_set ) 
			&& isset( $field_name, $meta_key ) 
			&& isset( self::$fieldmeta[ $field_set->name ][ $field_name ][ $meta_key ] )
		) {

			return self::$fieldmeta[ $field_set->name ][ $field_name ][ $meta_key ];
		}
	}

	/*
	public static function get_field_set_single_meta_key_values( $field_set, $meta_key = '', $full_meta = false, $keep_blank_values = false, $use_full_field_name = false ) {
		if( !self::init_field_set( $field_set ) ) {return null;}

		$field_set_meta_values = array();

		foreach( self::$fieldmeta[ $field_set->name ]['field_meta'] as $field => $meta ) {
			$field = $use_full_field_name ? "{$field_set->alias}.{$field}" : $field;
			
			if( array_key_exists( $meta_key, $meta ) ) {
				$field_set_meta_values[ $field ] = $full_meta ? $meta : $meta[ $meta_key ] ;			
			}
			else if( $keep_blank_values ) {
				$field_set_meta_values[ $field ] = null;
			}
		}

		return $field_set_meta_values;
	}
	*/

	//////
	// Editing Fieldmeta
	////////

	// dev: should use OVCDB!
	public static function update_field_meta( $field_set_name = null, $field = null, $meta_key = null, $meta_value = '' ) {
		if( !$field || !$meta_key || !$field_set_name ) {
			return false;
		}

		global $wpdb;

		$where = array(
			'field'		=> $field,
			'meta_key'	=> $meta_key
		);

		// Get field set / Check if $field_set_name exists or is a new field set
		$field_set = self::get_field_set( $field_set_name );
		$where['field_set'] = $field_set ? $field_set->name : $field_set_name;
		
		$data = array( 
			'meta_value'=> maybe_serialize( $meta_value )
		);

		// Determine whether to insert or update fieldmeta
		if( is_string( $field_set ) || null === self::get_meta_value( $field_set, $field, $meta_key ) ) {
			$update_result = $wpdb->insert( 'wp_ovc_schema', array_merge( $where, $data ), '%s' );
		}
		else {
			$update_result = $wpdb->update( 'wp_ovc_schema', $data, $where, '%s', '%s' );
		}

		if( $update_result ) { // Refresh the OVC fieldmeta cache if it changed
			self::get_schema( true );
		}

		return $update_result;
	}

	public static function delete_field_meta( $field_set = null, $field = null, $meta_key = null ) {
		if( !$field || !$meta_key || !OVCSC::init_field_set( $field_set ) ) {
			return false;
		}

		global $wpdb;

		$where = array(
			'field_set'	=> $field_set->name,
			'field'		=> $field,
			'meta_key'	=> $meta_key
		);

		$delete_result = $wpdb->delete( 'wp_ovc_schema', $where, '%s' );

		// Refresh the OVC fieldmeta cache if it changed
		if( $delete_result ) {
			self::get_schema( true );
		}

		return $delete_result;
	}

	public static function multi_update_field_meta( $ovc_schema_array = array() ) {

		foreach( $ovc_schema_array as $field_set => $fields ) {

			foreach( $fields as $field => $field_meta ) {

				foreach( $field_meta as $meta_key => $meta_value ) {

					self::update_field_meta( $field_set, $field, $meta_key, $meta_value );
				}
			}
		}
	} 

	/**
	 * Add an item to a meta_value's array (only applies when meta_value is an array)
	 * 
	 * @param string 		$meta_key the meta key to add an array item to
	 * @param mixed 		$add_value the value to add to the array
	 * @param mixed 		$after [optional] 
	 * 							false : $add_value is added to the beginning of the array;
	 * 							$after matches an element in the array : $add_value is appended after the matched item;
	 * 							null or if $add_value does not match element in the array: $add_value is appended to the end of the array
	 * 
	 * @return bool 		true on success, false on failure
	 **/
	public static function meta_add_array_item( $field, $meta_key, $add_value = null, $after = null, $no_duplicates = true ) {
		if( !$field || !self::init_field( $field ) ) {
			return false;
		}

		$meta_value = $field->meta( $meta_key );

		// Validate arguments and verify that $meta_value for $meta_key is an array
		if( is_string( $meta_key )
			&& isset( $add_value )
			&& is_array( $meta_value )
		) {

			if( $no_duplicates && in_array( $add_value, $meta_value ) ) {

				self::meta_remove_array_item( $field, $meta_key, $add_value );
			}
			
			// Prepend item to beginning of array
			if( false === $after ) {
				
				array_unshift( $meta_value, $add_value );
			}
			// Add item after $after value if it exists
			else if( $after && in_array( $after, $meta_value ) ) {

				$new_meta_value = array();

				foreach( $meta_value as $array_item ) {

					$new_meta_value[] = $array_item;

					if( $after == $array_item ) {
						$new_meta_value[] = $add_value;
					}
				}

				$meta_value = $new_meta_value;
			}
			// Add item to end of array
			else {
				array_push( $meta_value, $add_value );
			}

			self::update_field_meta( $field->field_set, $field, $meta_key, $meta_value );
		}

		return false;
	}


	public static function meta_remove_array_item( $field, $meta_key, $remove_value ) {
		if( !$field || !self::init_field( $field ) ) {
			return false;
		}

		if( is_string( $meta_key )
			&& strlen( $meta_key )
			&& is_array( $field->meta( $meta_key ) )
			&& in_array( $remove_value, $field->meta( $meta_key ) ) 
		) {
			$updated_array = array_diff( $field->meta( $meta_key ), array( $remove_value ) );

			return self::update_field_meta( $field->field_set, $field, $meta_key, $updated_array );
		}
	}


}