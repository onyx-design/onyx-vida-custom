<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVCDT_Session {
	public $is_valid = false;

	public $user;

	public $sesh_id	= '';

	public $user_id;
	public $display_name;
	

	public $field_set = false;

	public $qry_head = false;
	public $qry_where = false;
	public $qry_tail = false;
	public $qry_count = false;

	public $viewing_ids = false;
	public $last_seen = false;

	public $session_data_template = array(
		'sesh_id'		=> '',
		'user_id'		=> '',
		'display_name'	=> '',
		'field_set'		=> '',
		'qry_head'		=> '',
		'qry_where'		=> '',
		'qry_tail'		=> '',
		'qry_count'		=> '',
		'viewing_ids'	=> '',
		'last_seen' 	=> ''
	);


	public function __construct( $session_id = false, $field_set = false ) {
		global $current_user;
		$this->user = $current_user;

		if( !$session_id && $field_set ) {
			$this->field_set = $field_set;
			$this->initialize();
			$this->is_valid = true;
		}
		else if ( $this->check_session_id( $session_id ) ) {
			$this->is_valid = true;
			$this->sesh_id = $session_id;
		}
	}

	private function initialize( $field_set = false ) {
		$all_session_ids = $this->get_all_session_ids();

		while ( $this_session_id = $this->generate_session_id() ) {
			if ( !in_array( $this_session_id, $all_session_ids ) ) {
				$this->sesh_id = $this_session_id;
				$all_session_ids[] = $this->sesh_id;
				break;
			}
		}

		$session_data = array(
			'sesh_id'		=> $this->sesh_id,
			'user_id'		=> $this->user->ID,
			'display_name'	=> $this->user->display_name,
			'field_set'		=> $this->field_set,
			'qry_head'		=> $this->qry_head,
			'qry_where'		=> $this->qry_where,
			'qry_tail'		=> $this->qry_tail,
			'qry_count'		=> $this->qry_count,
			'viewing_ids'	=> '',
			'last_seen' 	=> date( 'Y-m-d H:i:s')	
		);


		set_transient( "ovcdt_session_{$this->sesh_id}", $session_data, 600 );
		set_transient( "ovcdt_sessions", $all_session_ids );
		

	}

	private function generate_session_id() {
		return md5( uniqid( $this->user->ID . microtime( true ) ) );
	}

	private function check_session_id( $session_id = '' ) {
		$valid_session = false;

		$ovcdt_session_ids = self::get_all_session_ids();

		if ( in_array( $session_id, $ovcdt_session_ids ) ) {
			$valid_session = true;
		}

		$session_data = get_transient( "ovcdt_session_{$session_id}" );

		if( is_array( $session_data ) ) {
			if ( array_keys( $session_data ) == array_keys( $this->session_data_template ) ) {
				$valid_session = true;

				foreach( $session_data as $key => $value ) {
					$this->$key = $value;
				}
			}
		}

		return true; // dev:improve
	}

	public function update() {
		$session_data = array(
			'sesh_id'		=> $this->sesh_id,
			'user_id'		=> $this->user->ID,
			'display_name'	=> $this->user->display_name,
			'field_set'		=> $this->field_set,
			'qry_head'		=> $this->qry_head,
			'qry_where'		=> $this->qry_where,
			'qry_tail'		=> $this->qry_tail,
			'qry_count'		=> $this->qry_count,
			'viewing_ids'	=> '',
			'last_seen' 	=> date( 'Y-m-d H:i:s'),
		);

		set_transient( "ovcdt_session_{$this->sesh_id}", $session_data, 600 );
	}

	public function get_updated_data( $force_check_ids = array() ) {
		global $wpdb;

		// Build qry to get data that has been updated
		$qry_where = '';
		$qry_where .= $this->qry_where ? " AND" : " WHERE";
		$qry_where .= " pr._meta_updated > '{$this->last_seen}'";

		$qry = $this->qry_head . $qry_where . $this->qry_tail;

		//ovc_dev_log( $qry, true ); // dev:indev

		$updated_data = array();

		$updated_ids = $wpdb->get_col( $qry );

		if( $updated_ids ) {
			$qry = "SELECT * FROM wp_ovc_products pr WHERE pr.ID IN(" . implode( ',', $updated_ids ) . ")";
			$updated_rows = $wpdb->get_results( $qry, ARRAY_A );

			foreach( $updated_rows as $row ) {

				//$updated_data[ $row['ID'] ] = OVCDB()->make_fields_array_ovcdt_format( $row, $this->field_set, true ); //dev:deprecated
			}
		}

		$this->update();

		return $updated_data;
	}



	static public function clear_all_sessions() {
		global $wpdb;
		$wpdb->query( "DELETE FROM {$wpdb->options} WHERE option_name LIKE 'ovcdt_session_%'" );
		set_transient( 'ovcdt_sessions', array() );
	}

	static public function get_all_session_ids() {
		$session_ids = get_transient( 'ovcdt_sessions' );
		return $session_ids && is_array( $session_ids ) ? $session_ids : array();
	}

	static public function get_all_sessions_data() {
		$session_ids = self::get_all_session_ids();
		$sessions_data = array();
		foreach( $session_ids as $session_id ) {
			if( $session_data = get_transient( "ovcdt_session_{$session_id}" ) ) {
				$sessions_data[ $session_id ] = $session_data;
			}
		}
		return $sessions_data;
	}
}