<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;

class OVC_FTP {

	/**
	 * Current FTP Credentials, based on environment
	 * 
	 * @var 	array|null
	 */
	protected $ftp_credentials = null;

	/**
	 * Array of all FTP Environments
	 * 
	 * @var 	array
	 */
	protected $ftp_environments = array(
		'oms'	=> array(
			'name'	=> 'VIDA OVC OMS FTP',
			'host'	=> '24.43.31.234',
			'port'	=> 21,	// ftp
			'user'	=> 'ovc',
			'pass'	=> '901Salameda',
		),
	);

	/**
	 * FTP Connection Stream
	 * 
	 * @var 	FTP Stream
	 */
	protected $ftp_instance = false;

	/**
	 * FTP System Type
	 * 
	 * @var 	string
	 */
	protected $ftp_sys_type;

	/**
	 * @var 	OVC_Results
	 */
	public $results = false;

	public function __construct( $environment, $autoconnect = true ) {
		OVC_Results::maybe_init( $this->results );

		$this->set_environment( $environment );
		$this->check_connection( $autoconnect );

		// Register PHP Shutdown function that will ensure the FTP connection is closed
		register_shutdown_function( array( $this, 'close_connection' ) );
	}

	/**
	 * Set the FTP Credentials based on the input $environment,
	 * or use the first $environment if the input is not found
	 * 
	 * @param 	string 	$environment
	 * 
	 * @return 	array
	 */
	public function set_environment( $environment ) {

		if( !isset( $environment ) && isset( $this->ftp_credentials ) ) {
			return $this->ftp_credentials;
		}

		// Set the FTP Credentials to the selected environment, or the first if that one is not found
		return $this->ftp_credentials = $this->ftp_environments[ $environment ] ?? current( $this->ftp_environments );
	}

	/**
	 * Check whether we're already connected to FTP or not, or autoconnect if true
	 * 
	 * @param 	bool $autoconnect 
	 * 
	 * @return 	type
	 */
	public function check_connection( $autoconnect = true ) {

		if( $this->is_connected() ) {
			return true;
		} else if( $autoconnect ) {
			return $this->connect();
		}

		return false;
	}

	/**
	 * Returns whether we have a connection or not
	 * 
	 * @return 	bool
	 */
	public function is_connected() {

		return ( !$this->ftp_instance || !is_array( $this->nlist_dir() ) ? false : true );
	}

	/**
	 * Connect to our FTP
	 * 
	 * @return 	OVC_Results|bool
	 */
	public function connect() {
		OVC_Results::maybe_init( $this->results );

		// Try to connect up to 3 times
		for( $try = 3; $try > 0; $try-- ) {

			if( $this->ftp_instance = ftp_connect( $this->ftp_credentials['host'], $this->ftp_credentials['port'], 5 ) ) {
				break;
			}
		}

		if( false === $this->ftp_instance ) {
			return $this->results->error( "Could not connect to {$this->ftp_credentials['name']}" );
		} else if( !@ftp_login( $this->ftp_instance, $this->ftp_credentials['user'], $this->ftp_credentials['pass'] ) ) {
			return $this->results->error( "{$this->ftp_credentials['name']} login failed." );
		} else {

			ftp_pasv( $this->ftp_instance, true );
			$this->ftp_sys_type = ftp_systype( $this->ftp_instance );
			$this->results->notice( "Successfully connected and logged into {$this->ftp_credentials['name']}." );
			return true;
		}
	}

	/**
	 * Close the connection to our FTP
	 * 
	 * @param 	OVC_Results|bool 	&$results 
	 */
	public function close_connection() {

		if( $this->check_connection() ) {

			ftp_close( $this->ftp_instance );
			$this->ftp_instance = false;
			$this->results->notice( "{$this->ftp_credentials['name']} connection closed." );
		}
	}

	/**
	 * Return the FTP Connections current directory
	 * 
	 * @return 	string
	 */
	public function pwd() {
		return ftp_pwd( $this->ftp_instance );
	}

	/**
	 * Change the FTP Connection to a different directory
	 * 
	 * @param 	string 	$directory
	 * 
	 * @return 	OVC_Results|bool
	 */
	public function chdir( $directory = '.' ) {

		if( !$this->check_connection() || !ftp_chdir( $this->ftp_instance, $directory ) ) {
			return $this->results->error( "Unable to change to directory: {$directory}" );
		}

		return true;
	}

	/**
	 * Get the list of files in the $directory
	 * 
	 * @param 	string 	$directory
	 * 
	 * @return 	array|bool
	 */
	public function nlist_dir( $directory = '.' ) {
		return ftp_nlist( $this->ftp_instance, $directory );
	}

	/**
	 * Get the detailed list of files in the $directory
	 * 
	 * @param 	string	$directory
	 * 
	 * @return 	array|bool
	 */
	public function rawlist_dir( $directory = '.' ) {

		if( !$this->check_connection() ) {
			return false;
		}

		if( is_array( $children = @ftp_rawlist( $this->ftp_instance, $directory ) ) ) {
			return $children;
		}

		return false;
	}

	/**
	 * List the files in the specified $directory
	 * 
	 * @param 	string 	$directory 
	 * @param 	bool 	$files_only 
	 * @param 	bool 	$sort_key 
	 * @return type
	 */
	public function list_dir( $directory = '.', $files_only = false, $sort_key = false ) {

		if( is_array( $children = $this->rawlist_dir( $directory ) ) ) {

			$items = array();

			switch( $this->ftp_sys_type ) {
				case 'UNIT':
					foreach( $children as $i => $child ) {

						$chunks = preg_split( "/\s+/", $child ); 
						
						list( $item['rights'], $item['number'], $item['user'], $item['group'], $item['size'], $item['month'], $item['day'], $item['time'] ) = $chunks; 
						
						$item['is_dir'] = $chunks[0]{0} === 'd' ? true : false; 
						
						array_splice( $chunks, 0, 8 ); 
						
						$items[ implode( " ", $chunks ) ] = $item;
						//$items[ $item[''] ] = $item;
					}
					break;
				case 'Windows_NT':

					foreach ( $children as $i => $child ) {

						preg_match( "/([0-9]{2})-([0-9]{2})-([0-9]{2}) +([0-9]{2}):([0-9]{2})(AM|PM) +([0-9]+|<DIR>) +(.+)/", $child , $split );

						if ( is_array( $split ) ) {

							// 4digit year fix
							if ( $split[3]<70) {
								$split[3]+=2000;
							}
							else {
								$split[3]+=1900;
							}

							$item = array(
								'name' 		=> $split[8],
								'is_dir'    => ( $split[7] == "<DIR>" ? true : false ),
								'size'  	=> ( $split[7] == "<DIR>" ? 0 : $split[7] ),
								'month' 	=> $split[1],
								'day'   	=> $split[2],
								'year'  	=> $split[3],
								'hour'  	=> $split[4],
								'min'		=> $split[5],
								'ampm'		=> $split[6],
								'mdtm'		=> false
							);

							if( !$item['is_dir'] ) {
								$item['mdtm'] = strtotime( $item['year'].'-'.$item['month'].'-'.$item['day'].' '.$item['hour'].':'.$item['min'].$item['ampm'] );
							}
							
							// Add item to array (unless it's a dir and we are only including files)
							if( !$item['is_dir'] || !$files_only ) {
								$items[ $item['name'] ] = $item;	
							}
						}
					}
					break;
				default:
					return $this->results->error( "Dir List Failed - Unknown FTP Systype: {$this->ftp_sys_type}" );
					break;
			}

			// Maybe sort list (only works if we're in files only mode )
			if( count( $items ) && $files_only && $sort_key && in_array( $sort_key, array( 'name', 'size', 'mdtm' ) ) ) {

				$items = array_column( $items, null, $sort_key );

				krsort( $items );

				$items = array_column( $items, null, 'name' );
			}

			return $items; 
		} else {
			return false;
		}
	}

	/**
	 * Get the newest file in the specified $directory
	 * 
	 * @param 	string 	$directory
	 * @param 	bool 	$name_only
	 * 
	 * @return 	string
	 */
	public function get_newest_file( $directory = '.', $name_only = true ) {
		
		$list_dir_mdtm = $this->list_dir( $directory, true, 'mdtm' );

		if( $list_dir_mdtm ) {

			$newest_file = array_shift( $list_dir_mdtm );
		
			if( isset( $newest_file['name'] ) && $name_only ) {
				return $newest_file['name'];
			}	

			return $newest_file;
		}
	}

	/**
	 * Get the list of files in the specified $directory sorted by last updated
	 * 
	 * @param 	string 	$directory
	 * 
	 * @return 	array
	 */
	public function list_dir_mdtm( $directory = '.' ) {

		if( !$this->check_connection() ) {
			return $this->results->error( "list_dir_mdtm failed: FTP connection failed." );
		}

		foreach( $this->nlist_dir( $directory ) as $file ) {

			if( $mdtm = $this->mdtm( $file ) ) {
				$files_mdtm[ $file ] = $this->mdtm( $file );    
			}
		}

		arsort( $files_mdtm, SORT_NUMERIC );

		return $files_mdtm;
	}

	/**
	 * Get the last modified time of the specified $file_name
	 * 
	 * @param 	string	$file_name
	 * 
	 * @return 	OVC_Results|string
	 */
	public function mdtm( $file_name ) {

		$mdtm = ftp_mdtm( $this->ftp_instance, $file_name );

		if ( -1 === $mdtm ) {
			return $this->results->error( "Unable to retrieve last modified time of {$file_name}" );
		}
		else {
			return $mdtm;
		}
	}

	/**
	 * Get the file size of the specified $file_name
	 * 
	 * @param 	string	$file_name
	 * 
	 * @return 	OVC_Results|int
	 */
	public function file_size( $file_name ) {

		if( !$file_name || !$this->check_connection() ) {
			return $this->results->error( "Unable to retrieve filesize. Invalid filename or FTP connection failed." );
		}

		$file_size = ftp_size( $this->ftp_instance, $file_name );

		if ( -1 === $file_size ) {
			return $this->results->error( "Unable to retrieve filesize of {$file_name}" );
		}
		else {
			return $file_size;
		}
	}

	/**
	 * Rename the file from $old_name to $new_name
	 * 
	 * @param 	string	$old_name 
	 * @param 	string	$new_name
	 * 
	 * @return 	bool
	 */
	public function rename( $old_name, $new_name ) {
		
		if( !$this->check_connection() ) {
			return $this->results->error( "Unable to rename file: FTP connection failed." );
		}

		return ftp_rename( $this->ftp_instance, $old_name, $new_name );     
	}

	/**
	 * Upload the specified $local_file to the $remote_file location
	 * 
	 * @param 	string	$remote_file
	 * @param 	string	$local_file
	 * 
	 * @return 	OVC_Results|bool
	 */
	public function put( $remote_file, $local_file ) {

		if( !$remote_file || !$local_file || !$this->check_connection() ) {
			return $this->results->error( "FTP File Upload failed. Invalid file names supplied or connection problem." );
		}

		if ( ! ftp_put( $this->ftp_instance, $remote_file, $local_file, FTP_ASCII ) ) {
			return $this->results->error( "Failed to upload file." );
		}
		else {

			$this->results->notice( "File uploaded successfully." );
			return true;
		}
	}

	/**
	 * Download the specified $remote_file to the $local_file location
	 * 
	 * @param 	string	$remote_file 
	 * @param 	string	$local_file
	 * 
	 * @return 	OVC_Results|bool
	 */
	public function get( $remote_file, $local_file ) {

		if( !$remote_file || !$local_file || !$this->check_connection() ) {
			return $this->results->error( "FTP File Download failed. Invalid file names supplied or connection problem." );
		}

		if ( !ftp_get( $this->ftp_instance, $local_file, $remote_file, FTP_ASCII ) ) {
			return $this->results->error( "Failed to download file." );
		} else {

			$this->results->notice( "File downloaded successfully." );
			return true;
		}
	}
}