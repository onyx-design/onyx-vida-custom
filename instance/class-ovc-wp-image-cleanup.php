<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_WP_Image_Cleanup {

	public function get_all_ovc_img_ids() {
		global $wpdb;

		$single_cols = array( 
			'wp_ovc_image_sets' 	=> array(
				'img_main',
				'img_oms1',
				'img_oms2',
				'img_case',
				'img_amazon',
				'img_sizeguide',
				'img_swatch'
			),
			'wp_ovc_parent_data' 	=> array(
				'img_main'
			)
		);

		$multi_cols = array( 
			'wp_ovc_image_sets' 	=> array(
				'img_gallery_ids'
			),
			'wp_ovc_parent_data' 	=> array(
				'img_gallery_ids'
			)
		);

		$img_ids = array();

		foreach( $single_cols as $table_name => $col_names ) {

			foreach( $col_names as $col_name ) {

				$col_img_ids = $wpdb->get_col( "SELECT {$col_name} FROM {$table_name} WHERE {$col_name} != 0" );

				$img_ids = array_merge( $img_ids, $col_img_ids );
			}
		}

		foreach( $multi_cols as $table_name => $col_names ) {

			foreach( $col_names as $col_name ) {

				$col_gallery_imgs = $wpdb->get_col( "SELECT {$col_name} FROM {$table_name} WHERE {$col_name} != ''" );

				foreach( $col_gallery_imgs as $gallery ) {
					$gallery = explode( ',', $gallery );
					$gallery = array_map( 'intval', $gallery );
					$img_ids = array_merge( $img_ids, $gallery );
				}

				
			}
		}

		//$img_ids = array_map( 'intval', $img_ids );

		sort( $img_ids, SORT_NUMERIC );

		$img_ids = array_unique( $img_ids );

		return $img_ids;
	}

	public function update_ovc_img_ids_table() {
		global $wpdb;

		$wpdb->query( "CREATE TABLE IF NOT EXISTS wp_ovc_image_ids ( ID INT )" );

		$wpdb->query( "TRUNCATE TABLE wp_ovc_image_ids" );

		$img_ids = $this->get_all_ovc_img_ids();

		$img_id_sql_vals = array_map( array( $this, 'wrap_val_for_multi_insert' ), $img_ids );

		$sql = "INSERT INTO wp_ovc_image_ids (ID) VALUES " . implode( ',', $img_id_sql_vals );

		//ovc_dev_log( strlen( $sql ) );

		$wpdb->query( $sql );

	}

	public function wrap_val_for_multi_insert( $value ) {
		return "({$value})";
	}

	public function get_img_ids_maybe_delete( $update_ovc_img_ids = true ) {
		global $wpdb;

		if( $update_ovc_img_ids ) {
			$this->update_ovc_img_ids_table();	
		}
		
		$img_ids_maybe_delete = $wpdb->get_( "SELECT ID FROM wp_posts WHERE post_type = 'attachment' AND post_mime_type = 'image/jpeg' AND ID NOT IN ( SELECT ID FROM wp_ovc_image_ids )" );

		return $img_ids_maybe_delete;
	}

	public function get_image_sizes() {
		global $_wp_additional_image_sizes;

		$sizes = array();

		foreach ( get_intermediate_image_sizes() as $_size ) {
			if ( in_array( $_size, array('thumbnail', 'medium', 'medium_large', 'large') ) ) {
				$sizes[ $_size ]['width']  = get_option( "{$_size}_size_w" );
				$sizes[ $_size ]['height'] = get_option( "{$_size}_size_h" );
				$sizes[ $_size ]['crop']   = (bool) get_option( "{$_size}_crop" );
			} elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {
				$sizes[ $_size ] = array(
					'width'  => $_wp_additional_image_sizes[ $_size ]['width'],
					'height' => $_wp_additional_image_sizes[ $_size ]['height'],
					'crop'   => $_wp_additional_image_sizes[ $_size ]['crop'],
				);
			}
		}

		return $sizes;
	}
}