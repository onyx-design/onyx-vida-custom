<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Image {

	protected $imagick;
	protected $original_imagick = null;

	protected $new_name = '';
	protected $original_path = '';

	/**
	 * Cropping Status
	 * 
	 * Needs Review
	 * Needs Override
	 * Approved
	 * 
	 * @var 	string
	 */
	protected $status = 'Needs Review';

	/**
	 * Horizontal Anchor / Focus
	 * 
	 * @var 	string
	 */
	protected $hanchor = 'Center';

	/**
	 * Vertical Anchor / Focus
	 * 
	 * @var 	string
	 */
	protected $vanchor = 'Middle';

	public function __construct( $img_path ) {

		// Get img path if WP ID is supplied
		if( is_numeric( $img_path ) ) {
			$img_path = get_attached_file( $img_path );
		}

		if( file_exists( $img_path ) ) {

			$this->imagick = new imagick( $img_path );
			$this->original_imagick = clone $this->imagick;

			$this->original_path = $img_path;

			$this->raw_width = $this->imagick->getImageWidth();
			$this->raw_height = $this->imagick->getImageHeight();
		} else {

			$this->imagick = new imagick();
			$this->imagick->newImage( 0, 0, new ImagickPixel( 'black' ) );
		}
	}

	/**
	 * Set the Cropping Status
	 * 
	 * @param 	string $status 
	 * 
	 * @return 	string
	 */
	public function set_status( $status ) {
		$this->status = $status;

		return $this->status;
	}

	/**
	 * Get the Cropping Status
	 * 
	 * @return 	string
	 */
	public function get_status() {
		return $this->status;
	}

	/**
	 * Get the width of the image
	 * 
	 * @return 	int
	 */
	public function get_width() {
		return $this->imagick->getImageWidth();
	}

	/**
	 * Get the height of the image
	 * 
	 * @return 	int
	 */
	public function get_height() {
		return $this->imagick->getImageHeight();
	}

	/**
	 * Get the original width of the image (before any editing)
	 * 
	 * @return 	int
	 */
	public function get_original_width() {
		return $this->original_imagick->getImageWidth();
	}

	/**
	 * Get the original height of the image (before any editing)
	 * 
	 * @return 	int
	 */
	public function get_original_height() {
		return $this->original_imagick->getImageHeight();
	}

	/**
	 * Returns the number of edges from the image that are white(>=2)
	 * 
	 * @return 	int
	 */
	public function get_white_edge_count( $type = 'uncropped' ) {

		$raw_edges = $this->get_raw_edges( true );

		$white_edges = 0;
		foreach( $raw_edges as $raw_edge ) {

			if( 2 <= $raw_edge ) {
				
				$white_edges++;
			}
		}

		return $white_edges;
	}

	/**
	 * Returns the number of edges from the original image that are white(>=2)
	 * 
	 * @return 	int
	 */
	public function get_original_white_edge_count() {

		$raw_edges = $this->get_original_raw_edges( true );

		$white_edges = 0;
		foreach( $raw_edges as $raw_edge ) {

			if( 2 <= $raw_edge ) {

				$white_edges++;
			}
		}

		return $white_edges;
	}

	/**
	 * Return the values of the raw edges
	 * 
	 * If $array is true, this will return an array,
	 * otherwise it returns a string
	 * ex: '5 5 0 5'
	 * 
	 * Wrapper for get_raw_trbl( 'imagick' )
	 * 
	 * @see 	get_raw_trbl()
	 * 
	 * @param 	bool $array
	 * 
	 * @return 	string|array
	 */
	public function get_raw_edges( $array = false ) {

		if( $array ) {
			return $this->get_raw_trbl( 'imagick', true );
		}

		return $this->get_raw_trbl();
	}

	/**
	 * Return the values of the original raw edges
	 * 
	 * If $array is true, this will return an array,
	 * otherwise it returns a string
	 * ex: '5 5 0 5'
	 * 
	 * Wrapper for get_raw_trbl( 'original_imagick' )
	 * 
	 * @see 	get_raw_trbl()
	 * 
	 * @param 	bool $array
	 * 
	 * @return 	string|array|bool
	 */
	public function get_original_raw_edges( $array = false ) {

		if( $array ) {
			return $this->get_raw_trbl( 'original_imagick', true );
		}

		return $this->get_raw_trbl( 'original_imagick' );
	}

	/**
	 * Top Right Bottom Left
	 * 
	 * Values
	 * 0: Less than 250
	 * 1: 250-251
	 * 2: 251-252
	 * 3: 252-253
	 * 4: 253-254
	 * 5: 254-255
	 * 
	 * @return string|array
	 */
	public function get_raw_trbl( $type = 'imagick', $array = false ) {

		$trbl = array_map( function( $value ) {

			$value = $value - 250;

			if( 0 >= $value ) {
				return 0;
			}

			// Round the value down to get the value
			return floor( $value );

		}, array( $this->get_edge_top( 'I', true, $type ), $this->get_edge_right( 'I', true, $type ), $this->get_edge_bottom( 'I', true, $type ), $this->get_edge_left( 'I', true, $type ) ) );

		if( $array ) {
			return $trbl;
		}

		return implode( ' ', $trbl );
	}

	/**
	 * Returns the average white/empty pixels on the top edge
	 * 
	 * @param 	string $map
	 * @param 	bool $average 
	 * @param 	string $type 
	 * 
	 * @return 	double
	 */
	public function get_edge_top( $map = "RGB", $average = true, $type = 'imagick' ) {

		$edge = $this->$type->exportImagePixels(0,0,$this->$type->getImageWidth(),1, $map,imagick::PIXEL_CHAR );

		return $average ? $this->average( $edge ) : $edge;
	}

	/**
	 * Returns the average white/empty pixels on the right edge
	 * 
	 * @param 	string $map
	 * @param 	bool $average 
	 * @param 	string $type 
	 * 
	 * @return 	double
	 */
	public function get_edge_right( $map = "RGB", $average = true, $type = 'imagick' ) {

		$edge = $this->$type->exportImagePixels($this->$type->getImageWidth() - 1,0,1,$this->$type->getImageHeight(), $map,imagick::PIXEL_CHAR );

		return $average ? $this->average( $edge ) : $edge;
	}

	/**
	 * Returns the average white/empty pixels on the bottom edge
	 * 
	 * @param 	string $map
	 * @param 	bool $average 
	 * @param 	string $type 
	 * 
	 * @return 	double
	 */
	public function get_edge_bottom( $map = "RGB", $average = true, $type = 'imagick' ) {

		$edge = $this->$type->exportImagePixels(0,$this->$type->getImageHeight() - 1,$this->$type->getImageWidth(),1, $map,imagick::PIXEL_CHAR );

		return $average ? $this->average( $edge ) : $edge;
	}

	/**
	 * Returns the average white/empty pixels on the left edge
	 * 
	 * @param 	string $map
	 * @param 	bool $average 
	 * @param 	string $type 
	 * 
	 * @return 	double
	 */
	public function get_edge_left( $map = "RGB", $average = true, $type = 'imagick' ) {

		$edge = $this->$type->exportImagePixels(1,0,1,$this->$type->getImageHeight(), $map,imagick::PIXEL_CHAR );

		return $average ? $this->average( $edge ) : $edge;
	}

	/**
	 * Returns the average of an array
	 * 
	 * @param 	array $array
	 * 
	 * @return 	double
	 */
	public function average( $array ) {
		return array_sum( $array ) / count( $array );
	}

	/**
	 * Returns whether the image is within 2% of 3:4 aspect ratio
	 * 
	 * @return bool
	 */
	public function is_3_4_aspect_ratio() {
		return ( $this->get_aspect_ratio() >= 0.73 && $this->get_aspect_ratio() <= 0.78 );
	}

	/**
	 * Returns the aspect ratio of the image
	 * 
	 * @return double
	 */
	public function get_aspect_ratio() {
		return $this->get_width() / $this->get_height();
	}

	/**
	 * Returns the aspect ratio of the original image
	 * 
	 * @return double
	 */
	public function get_original_aspect_ratio() {
		return $this->get_original_width() / $this->get_original_height();
	}

	/**
	 * Set the anchor point that will be used for cropping
	 * 
	 * Left Top
	 * Left Middle
	 * Left Bottom
	 * 
	 * Center Top
	 * Center Middle
	 * Center Bottom
	 * 
	 * Right Top
	 * Right Middle
	 * Right Bottom
	 * 
	 * @param type $x 
	 * @param type $y 
	 * @return type
	 */
	public function set_anchor( $anchor = 'Center Middle' ) {

		list( $this->hanchor, $this->vanchor ) = explode( ' ', trim( $anchor ) );

		return $this->get_anchor();
	}

	/**
	 * Returns the anchor / focus of the image
	 * as a string in the form "[Horizontal Point] [Vertical Point]"
	 * 
	 * @return string
	 */
	public function get_anchor() {
		return "{$this->get_horizontal_anchor()} {$this->get_vertical_anchor()}";
	}

	/**
	 * Returns the Horizontal Anchor / Focus Point
	 * 
	 * @return string
	 */
	public function get_horizontal_anchor() {
		return $this->hanchor;
	}

	/**
	 * Returns the Vertical Anchor / Focus Point
	 * 
	 * @return string
	 */
	public function get_vertical_anchor() {
		return $this->vanchor;
	}

	/*
	 * IMAGE FILE FUNCTIONS
	 */

	public function generate_new_name( OVC_Row $ovc_row, $extension = false, $override = false ) {

		if( !$ovc_row ) {
			return false;
		}

		$file_name = strtolower( $ovc_row->data( 'pic.ID' ) . '__' . ( $override ? 'OVERRIDE' : $ovc_row->data( 'pic.crop_mode' ) ) . '-' . str_replace( ' ', '_', $ovc_row->data( 'pic.crop_focus' ) ) . '-' . $this->get_width() . '-' . $this->get_height() . ( $extension ? '.jpg' : '' ) );

		return $this->set_new_name( $file_name );
	}

	public function set_new_name( $file_name ) {

		$this->new_name = $file_name;

		return $this->get_new_name();
	}

	public function get_new_name() {
		return $this->new_name;
	}

	/**
	 * Returns the name of the file for the original image
	 * 
	 * dev:todo Setup name format
	 * 
	 * @param 	bool $extension 
	 * 
	 * @return 	string
	 */
	public function name( $extension = false ) {

		$path_info = pathinfo( $this->original_imagick->getImageFilename() );

		if( $extension ) {

			return $path_info['basename'];
		}
		return $path_info['filename'];
	}

	public function override_path( OVC_Row $ovc_row = null ) {

		$file_name = $this->name();

		if( $ovc_row ) {
			$file_name = $this->generate_new_name( $ovc_row, true, true );
		}

		return $this->name() ? $file_name : false;
	}

	public function cropped_path( OVC_Row $ovc_row = null ) {

		$file_name = $this->name();

		if( $ovc_row ) {
			$file_name = $this->generate_new_name( $ovc_row, true );
		}

		return $this->name() ? $file_name : false;
	}

	/**
	 * Returns the path in the OVC Image Directory of the image
	 * 
	 * @return string
	 */
	public function path() {
		return $this->name() ? $this->name( true ) : false;
	}

	/**
	 * Returns whether the file exists in the OVC Image Directory or not
	 * 
	 * @return bool
	 */
	public function exists() {
		return $this->path() ? file_exists( $this->path() ) : false;
	}

	/**
	 * Returns the URL of the image in the OVC Image Directory
	 * 
	 * @return type
	 */
	public function url() {
		return $this->path() ? ovc_content_url_from_path( $this->path() ) : false;
	}

	/**
	 * Returns the extension of the image file
	 * 
	 * @return type
	 */
	public function extension() {
		return $this->name() ? substr( $this->name( true ), strrpos( $this->name( true ), '.' ) + 1 ) : false;
	}

	/*
	 * SAVE IMAGE FUNCTIONS
	 */

	/**
	 * Save the image to the $path
	 * 
	 * @param 	string $path
	 * 
	 * @return 	bool
	 */
	public function write_image( $path ) {

		$file_handle = fopen( $path, 'w' );

		return $this->imagick->writeImageFile( $file_handle );
	}

	public function delete_image( $path ) {
		return unlink( $path );
	}

	/*
	 * IMAGE MANIPULATION FUNCTIONS
	 */

	/**
	 * Resets the Image to the Original
	 */
	public function reset() {
		$this->imagick = clone $this->original_imagick;
	}

	public function repage() {
		return $this->imagick->setImagePage( $this->get_width(), $this->get_height(), 0, 0 );
	}

	/**
	 * Resize an image
	 * 
	 * @param 	int 	$width 
	 * @param 	int 	$height 
	 * @param 	bool 	$bestfit 
	 * 
	 * @return 	bool
	 */
	public function resize( $width, $height, $bestfit = false ) {
		return $this->imagick->scaleImage( $width, $height, $bestfit );
	}

	/**
	 * Add a border around the image
	 * 
	 * @param 	string 	$color 
	 * @param 	int 	$width 
	 * @param 	int 	$height 
	 * 
	 * @return 	bool
	 */
	public function border( $color, $width, $height ) {

		$bordered = $this->imagick->borderImage( $color, $width, $height );

		$this->repage();

		return $bordered;
	}

	/**
	 * Crop the image down
	 * 
	 * $x - x top left corner
	 * $y - y top left corner
	 * 
	 * @param 	int 	$width 
	 * @param 	int 	$height 
	 * @param 	int 	$x 
	 * @param 	int 	$y 
	 * 
	 * @return 	bool
	 */
	public function crop( $width, $height, $x, $y ) {

		$cropped = $this->imagick->cropImage( $width, $height, $x, $y );

		$this->repage();

		return $cropped;
	}

	public function splice( $width, $height, $x, $y ) {

		$this->imagick->setImageBackgroundColor( 'white' );

		$spliced = $this->imagick->spliceImage( $width, $height, $x, $y );

		$this->repage();

		return $spliced;
	}

	/**
	 * Analyze the cropping for this image and return the data
	 * 
	 * @return type
	 */
	public function analyze_cropping( $mode = 'Auto', $focus = 'Center Middle' ) {

		$mode = $mode ?: 'Auto';
		$focus = $focus ?: 'Center Middle';

		switch( $mode ) {

			case 'Auto':
				// How many white edges are there on the uncropped image
				$uncropped_edges = $this->get_original_white_edge_count();

				// Crop it
				$this->crop_3_4_center();

				$cropped_edges = $this->get_white_edge_count();

				// Make sure there are 3 or more white edges
				if( 4 === $uncropped_edges
					&& $cropped_edges < 3 ) {

					// Remove the cropping and resize instead
					$this->reset();
					$this->resize_3_4();

					$cropped_edges = $this->get_white_edge_count();
				}

				if( $cropped_edges <= 2 ) {
					$this->set_status( 'Needs Override' );
				}
			break;
			case 'Crop':
			case 'Scale':

				$this->set_anchor( $focus );
				$this->calculate_focus( $mode );
			break;
		}
	}

	/**
	 * Crop the image to 3:4 aspect ratio centered
	 * 
	 * @return bool
	 */
	public function crop_3_4_center() {

		$aspect_ratio = $this->get_aspect_ratio();

		if( $aspect_ratio > 0.75 ) {

			$crop_x = ( ( ( $aspect_ratio - 0.75 ) / 2 ) * $this->get_width() );

			$crop_width = $this->get_height() * 0.75;

			return $this->crop( $crop_width, $this->get_height(), $crop_x, 0 );
		}

		return false;
	}

	/**
	 * Resize and add a border to the image to bring it to 3:4 aspect ratio
	 * 
	 * @return bool
	 */
	public function resize_3_4() {

		// Shrinks the image by 25%
		$resized = $this->resize( ( $this->get_width() * 0.75 ), ( $this->get_height() * 0.75 ), true );

		// dev:improve return
		if( $resized ) {

			// Adds border to the top and bottom of the image 
			return $this->border( 'white', 0, ( $this->get_original_height() - $this->get_height() ) / 2 );
		}

		return false;
	}

	public function calculate_focus( $mode = 'Crop' ) {

		$hanchor = $this->hanchor;
		$vanchor = $this->vanchor;

		$image_width = $this->get_width();
		$image_height = $this->get_height();

		switch( $hanchor ) {

			case 'Left':
				$splice_w = $image_width / 3;
				$splice_x = 0;

				$crop_x = 0;
				$resize_x = $image_width / 3;
			break;

			case 'Right':
				$splice_w = $image_width / 3;
				$splice_x = $image_width;

				$crop_x = $image_width / 3;
				$resize_x = $image_width / 3;
			break;

			case 'Center':
			default:
				$splice_w = 0;
				$splice_x = $image_width / 2;

				$crop_x = 0;
				$resize_x = 0;
			break;
		}

		switch( $vanchor ) {

			case 'Top':
				$splice_h = $image_height / 3;
				$splice_y = 0;

				$crop_y = 0;
				$resize_y = $image_height / 3;
			break;

			case 'Bottom':
				$splice_h = $image_height / 3;
				$splice_y = $image_height;

				$crop_y = $image_height / 3;
				$resize_y = $image_height / 3;
			break;

			case 'Middle':
			default:
				$splice_h = 0;
				$splice_y = $image_height / 2;

				$crop_y = 0;
				$resize_y = 0;
			break;
		}

		if( 'Crop' === $mode ) {
			// Get the focus and crop it down
			$spliced = $this->splice( $splice_w, $splice_h, $splice_x, $splice_y );

			if( $spliced ) {

				$cropped = $this->crop( $this->get_original_width(), $this->get_original_height(), $crop_x, $crop_y );

				if( $cropped ) {

					return $this->crop_3_4_center();
				}
			}
		}
		elseif( 'Scale' === $mode ) {
			// Get the focus and scale it
			$scaled = $this->resize( ( $this->get_width() * 0.75), ( $this->get_height() * 0.75 ), true );

			if( $scaled ) {

				if( $splice_y === $image_height ) {
					$splice_y = $this->get_height();
				}

				if( 'Middle' == $vanchor ) {

					$this->splice( 0, $image_height / 8, 0, 0 );
					$this->splice( 0, $image_height / 8, 0, $this->get_height() );
				}
				else {
					$this->splice( 0, $image_height / 4, 0, $splice_y );
				}
			}
		}

	}

	public function old_calculate_focus( $mode = 'Crop' ) {

		$image_width = $this->get_width();
		$image_height = $this->get_height();

		switch( $this->get_anchor() ) {

			case 'Left Top':

				$splice_w = $image_width / 3;
				$splice_x = 0;

				$splice_h = $image_height / 3;
				$splice_y = 0;

				$crop_x = 0;
				$resize_x = $image_width / 3;

				$crop_y = 0;
				$resize_y = $image_height / 3;
				break;
			case 'Left Middle':

				$splice_w = $image_width / 3;
				$splice_x = 0;

				$splice_h = 0;
				$splice_y = $image_height / 2;

				$crop_x = 0;
				$resize_x = $image_width / 3;

				$crop_y = 0;
				$resize_y = 0;
				break;
			case 'Left Bottom':

				$splice_w = $image_width / 3;
				$splice_x = 0;

				$splice_h = $image_height / 3;
				$splice_y = $image_height;

				$crop_x = 0;
				$resize_x = $image_width / 3;

				$crop_y = $image_height / 3;
				$resize_y = $image_height / 3;
				break;
			case 'Center Top':

				$splice_w = 0;
				$splice_x = $image_width / 2;

				$splice_h = $image_height / 3;
				$splice_y = 0;

				$crop_x = 0;
				$resize_x = 0;

				$crop_y = 0;
				$resize_y = $image_height / 3;
				break;
			case 'Center Middle':

				$splice_w = 0;
				$splice_x = $image_width / 2;

				$splice_h = 0;
				$splice_y = $image_height / 2;

				$scale_top_height = $image_width / 8;
				$scale_bottom_height = $image_width / 8;

				$crop_x = 0;
				$resize_x = 0;

				$crop_y = 0;
				$resize_y = 0;
				break;
			case 'Center Bottom':

				$splice_w = 0;
				$splice_x = $image_width / 2;

				$splice_h = $image_height / 3;
				$splice_y = $image_height;

				$crop_x = 0;
				$resize_x = 0;

				$crop_y = $image_height / 3;
				$resize_y = $image_height / 3;
				break;
			case 'Right Top':

				$splice_w = $image_width / 3;
				$splice_x = $image_width;

				$splice_h = $image_height / 3;
				$splice_y = 0;

				$crop_x = $image_width / 3;
				$resize_x = $image_width / 3;

				$crop_y = 0;
				$resize_y = $image_height / 3;
				break;
			case 'Right Middle':

				$splice_w = $image_width / 3;
				$splice_x = $image_width;

				$splice_h = 0;
				$splice_y = $image_height / 2;

				$crop_x = $image_width / 3;
				$resize_x = $image_width / 3;

				$crop_y = 0;
				$resize_y = 0;
				break;
			case 'Right Bottom':

				$splice_w = $image_width / 3;
				$splice_x = $image_width;

				$splice_h = $image_height / 3;
				$splice_y = $image_height;

				$crop_x = $image_width / 3;
				$resize_x = $image_width / 3;

				$crop_y = $image_height / 3;
				$resize_y = $image_height / 3;
				break;

			default:

				break;
		}

		if( 'Crop' === $mode ) {

			// Get the focus and crop it down
			$spliced = $this->splice( $splice_w, $splice_h, $splice_x, $splice_y );

			if( $spliced ) {

				$cropped = $this->crop( $this->get_original_width(), $this->get_original_height(), $crop_x, $crop_y );

				if( $cropped ) {

					return $this->crop_3_4_center();
				}
			}
		}
		elseif( 'Scale' === $mode ) {

			// Get the focus and scale it
			$scaled = $this->resize( ( $this->get_width() * 0.75), ( $this->get_height() * 0.75 ), true );

			if( $scaled ) {

				// $spliced = $this->splice( 0, $image_height / 4, $splice_x, $splice_y );
				$this->splice( 0, $scale_top_height, 0, 0 );
				$this->splice( 0, $scale_bottom_height, $this->get_width(), $this->get_height() );
			}
		}
	}
}