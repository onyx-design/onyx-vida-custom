<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Product_WooCommerce extends OVC_Row_ovc_products {

	public static $attributes_template = null;


	// Fully sync $this OVC Product with WooCommerce
	public function wc_sync( &$results = false, $sync_parent = true ) {
		OVC_Results::maybe_init( $results );

		if( $this->should_be_wc() ) {

			if( !$this->wc_parent() ) {
				$this->create_wc_parent( $results );
			}
			
			if( !$this->wc_variation() ) {
				$this->create_wc_variation( $results );
			}
		}

		// If this variation has ever been synced, check wc parent matching
		if( $this->data( 'post_id' ) ) {
			
			if( $this->check_wc_parent_mismatch( $results ) ) {
				$sync_parent = true;
			}	
		}
		
		

		if( $this->wc_variation( true ) ) {

			//$this->match_ovc_wc_parent( $results );
			$this->sync_wc_variation_data( $results );
			
			//$this->ovc_set_wc_stock( $results );
		}

		if( $sync_parent ) {
			$this->wc_parent_internal_sync( $results );
		}

		$results->success = !$results->has_error;

		if( $results->success ) {
			
			$this->update_field( 'pr.last_wc_sync', current_time( 'mysql', 1 ), $results );
		}

		return $results->success;
	}

	// Create a WC Variable Parent based on $this OVC Product
	// This function was built based on WooCommerce file: /wp-content/plugins/woocommerce/includes/admin/meta-boxes/class-wc-meta-box-product-data.php
	private function create_wc_parent( &$results = false ) {
		OVC_Results::maybe_init( $results );

		$parent_data = $this->get_external_data_set( FS( 'wc_parent_props' ), $results );

		//$parent_data['attributes'] = array();

		$wc_parent = new WC_Product_Variable();

		$wc_parent->set_props( $parent_data );

		if( $wc_parent->save() ) {

			$wc_parent_id = $wc_parent->get_id();

			$this->get_parent()->update_field( 'pa.wc_parent_id', $wc_parent_id );

			$this->wc_parent( true );

			$results->notice( "Created new WC Variable Parent: Parent SKU: {$this->parent_sku}, WC Parent ID: {$wc_parent_id}" );

			return $wc_parent_id;
		}
		else {

			return $results->error( "ERROR! wp_insert_post failed: Could not create WC parent with SKU: {$this->parent_sku}" );
		}
	}

	// Create a new WC Variation by OVC ID
	private function create_wc_variation( &$results = false ) {
		OVC_Results::maybe_init( $results );

		$variation_props = $this->get_external_data_set( FS( 'wc_variation_props' ), $results );

		//$parent_data['attributes'] = array();

		$wc_variation = new WC_Product_Variation();

		$wc_variation->set_props( $variation_props );

		if( $wc_variation->save() ) {

			$wc_variation_id = $wc_variation->get_id();

			$this->update_field( 'pr.post_id', $wc_variation_id );

			$this->wc_variation( true );

			$results->notice( "Created new WC Variation from OVC ID: {$this->ID}, Parent SKU: {$this->parent_sku}, WC Variation ID: {$wc_variation_id}" );

			return $wc_variation_id;
		}
		else {

			return $results->error( "ERROR! wp_insert_post failed: Could not create variation for OVC ID: {$this->ID}, SKU: {$this->sku}" );
		}
	}

	// Check (and maybe fix) wc_parent mismatch
	public function check_wc_parent_mismatch( &$results = false ) {
		OVC_Results::maybe_init( $results );

		if( !$this->wc_parent() || $this->parent_sku != $this->wc_parent()->get_sku() ) {

			$old_wc_parent_id = $this->data( 'pa.wc_parent_id' );

			$results->notice( "OVC Parent -> WC Parent mismatch. OVC Parent Data: WC Parent ID: " . $this->data( 'pa.wc_parent_id' ) . "  Parent SKU: {$this->parent_sku}" );
			
			$wc_parent_id = wc_get_product_id_by_sku( $this->parent_sku );
			
			if( $wc_parent_id && $wc_parent = wc_get_product( $wc_parent_id ) ) {

				$results->notice( "Parent SKU {$wc_parent->get_sku()} found. Updating pr.wc_parent_id to {$wc_parent->get_id()}" );
				$this->update_field( 'pa.wc_parent_id', $wc_parent->get_id() );
			}
			else {
				$this->create_wc_parent();
			}

			return true;
		}

		return false;
	}

	// Sync OVC Product to its WC Variation
	public function sync_wc_variation_data( &$results = false ) {
		OVC_Results::maybe_init( $results );

		if( !( $this->wc_variation() instanceof WC_Product_Variation ) ) {
			return $results->error( "ERROR! Cannot sync WC variation data for OVC ID: {$this->ID}. Invalid WC variation association. OVC post_id: {$this->post_id}" );
		}

		// Set variation meta
		$raw_variation_meta = $this->get_external_data_set( FS( 'wc_variation_meta' ), $results );

		foreach( $raw_variation_meta as $meta_key => $meta_value ) {

			$meta_id = '';

			if( $existing_meta = $this->wc_variation()->get_meta( $meta_key, false, 'edit' ) ) {
				
				$existing_meta = array_pop( $existing_meta );
				$meta_id = $existing_meta->id;
			}

			$this->wc_variation()->update_meta_data( $meta_key, $meta_value, $meta_id );			
		}

		// Set variation props
		$variation_props = $this->get_external_data_set( FS( 'wc_variation_props' ), $results );
		
		$this->wc_variation()->set_props( $variation_props );

		return $this->wc_variation()->save();
	}

	// Check that the WC Variation has the correct WC Parent (by Parent SKU).  This function might create WC Parents and/or move WC Variations between WC Parents
	public function match_ovc_wc_parent( &$results = false ) {
		OVC_Results::maybe_init( $results );

		// Check that the OVC Parent SKU matches the WC Parent SKU
		if( $this->wc_variation() && ( !$this->wc_parent() || ( $this->parent_sku != $this->wc_parent()->get_sku() ) ) ) {

			//$results->notice( "OVC Parent SKU ( {$this->parent_sku} ) does not match associated WC Parent SKU ( {$this->wc_parent()->sku} )" );

			$results->notice( "OVC Parent -> WC Parent mismatch. OVC Parent Data: WC Parent ID: " . $this->data( 'pa.wc_parent_id' ) . "  Parent SKU: {$this->parent_sku}" );

			// Determine if there is already a WC Variable Parent with the correct Parent SKU
			$correct_wc_parent_id = wc_get_product_id_by_sku( $this->parent_sku );

			// Determine if there are any Approved OVC Products with the old parent sku
			$ovcps_old_parent_sku = OVCDB()->count_results( 'ovc_products', array( 'parent_sku' => $this->wc_parent()->get_sku(), 'sync_oms' => 1 ) );

			// If no WC Parent with correct Parent SKU exists, we either need to create a new WC Parent or change the SKU of the existing one
			if( !$correct_wc_parent_id ) {
				if( $ovcps_old_parent_sku ) {
					// Create new WC Variable Parent and set the $correct_wc_parent_id to match
					$correct_wc_parent_id = $this->create_wc_parent( $results );
					$results->notice( "Created new WC Parent ( {$correct_wc_parent_id} ): No existing WC Parent with matching SKU: {$this->parent_sku} exists but there are still OVC Products with currently-matched WC Parent SKU: {$this->wc_parent()->get_sku()}" );
				}
				else {
					update_post_meta( intval( $this->wc_parent()->get_id() ), '_sku', $this->parent_sku );
					$results->notice( "Changed WC Parent SKU: No existing WC Parent with matching SKU: {$this->parent_sku} exists and there zero OVC Products with currently-matched WC Parent SKU: {$this->wc_parent()->get_sku()}" );
				}
			}

			// If there is a WC Variable Parent with the current OVC Parent SKU, move the WC Variation to the new parent
			if( $correct_wc_parent_id ) {
				if( !wp_update_post( array( 'ID' => intval( $this->post_id ), 'post_parent' => intval( $correct_wc_parent_id ) ) ) ) { // Switch parent ID to corrent WC parent
					return $results->error( "Error trying to switch post parent of WC variation ( {$ovc_row['post_id']} ) to WC parent ID ( {$wc_parent_id_by_ovc_parent_sku} ). OVC ID: {$ovc_id}, sku: {$ovc_row['sku']}" );
				}
				else {
					$results->notice( "Moved WC Variation ( {$this->post_id} ) to WC Parent with matching parent sku ( {$correct_wc_parent_id} )" );
					// Update OVC Product's Shared Data
					$this->update_field( 'pa.wc_parent_id', $correct_wc_parent_id, $results );
				}

				// Refresh WC Parent Data
				OVC_Product_WooCommerce::static_wc_parent_internal_sync( $this->wc_parent()->get_id() );
				OVC_Product_WooCommerce::static_wc_parent_internal_sync( $correct_wc_parent_id );
			}

			// Refresh OVC Product's WC Data
			$this->wc_variation( true );
			$this->wc_parent( true );
		}
	}

	// Sync a WC Parent
	public function wc_parent_internal_sync( &$results = false ) {
		OVC_Results::maybe_init( $results );

		$wc_parent = $this->wc_parent();

		if( !$wc_parent ) {
			return false;
		}

		// Set fresh props
		$wc_parent->set_props( $this->get_external_data_set( FS( 'wc_parent_props' ), $results ) );
		$wc_parent->save();


		// Sync first because we need certain data prepared for the rest of the WC parent sync
		WC_Product_Variable::sync( $wc_parent );

		wc_delete_product_transients( $wc_parent->get_id() );

		// Initialize Variables
		global $wpdb;

		$wc_parent_props 	= $this->get_external_data_set( FS( 'wc_parent_props' ), $results );
		$image_ids 			= array();
		$categories 		= array(); // Initialize product categories term ids array
		$parent_enabled 	= false; // Determine if the parent should be hidden based on variation statuses

		$attributes 		= $wc_parent_props['attributes']; // Already set to fresh ones in $wc_parent->set_props() above
		$variation_ids 		= (array) $wc_parent->get_children();

		// Initialize the swatch meta template to be used when looping through attributes(for swatch images)
		$md5_pa_color		= md5( 'pa_color' );
		$md5_pa_wc_size		= md5( 'pa_wc-size' );

		$swatch_meta = array(
			$md5_pa_color 	=> array(
				'type'			=> 'product_custom',
				'layout' 		=> 'label_above',
				'size'			=> 'shop_thumbnail',
				'attributes'	=> array()
			),
			$md5_pa_wc_size 	=> array(
				'type'			=> 'product_custom',
				'layout' 		=> 'label_above',
				'size'			=> 'shop_thumbnail',
				'attributes'	=> array()
			)
		);

		// Reset any image / attachment ids that are set to this parent
		$wpdb->update( 
			$wpdb->posts,
			array( 
				'post_parent' => 0
			), 
			array(
				'post_parent' => $this->data( 'pa.wc_parent_id' ),
				'post_type' => 'attachment'
			)
		);

		// Record parent images
		if( !empty( $wc_parent_props['image_id'] ) ) {
			$image_ids[] = $wc_parent->get_image_id();
		}

		if( !empty( $wc_parent_props['gallery_image_ids'] ) ) {
			$images_ids = array_merge( $image_ids, explode( ',', $wc_parent_props['gallery_image_ids'] ) );
		}

		// Maybe add 'Featured' Product Category
		if( $this->data( 'pa.featured' ) ) {

			$categories[] = 'Featured';
		}
		// Maybe add 'Homepage' Product Category
		if( $this->data( 'pa.homepage' ) ) {

			$categories[] = 'Homepage Featured';
		}
		
		// Set Parent Brand
		//$attributes['pa_brand']->set_options( array() )

		foreach( $variation_ids as $variation_id ) {

			$wc_variation = wc_get_product( $variation_id );

			$variation_attributes = $wc_variation->get_attributes();

			// Maybe do default attributes for parent (if they haven't been done yet)
			if( empty( $wc_parent_props['default_attributes'] ) && !empty( $variation_attributes ) ) {

				$wc_parent_props['default_attributes'] = $variation_attributes;
			}

			$ovc_pr = null;
			$ovc_product = null;

			if( $ovc_id = $wc_variation->get_meta( '_ovc_id' ) ) {

				// Unfortunately because of OVCDB Row handling updates (Aug 2018), we have to get the actual OVC row so we can send the data to the special OVC_Product_WooCommerce row
				$ovc_pr = OVCDB()->get_row( FS('ovc_products'), $ovc_id );
				$ovc_product = new OVC_Product_WooCommerce( FS( 'ovc_products' ), $ovc_id, $ovc_pr->data_set() );

				// Enable parent if all variations are set to sync and in stock
				if( 'enabled' == $wc_variation->get_meta( '_ovc_wc_status' ) 
					&& $wc_variation->is_in_stock()
					&& $ovc_product->should_be_wc()
				) {
					$parent_enabled = true;
				}

				// Maybe add style number's product category
				if( !isset( $categories[ $ovc_product->data( 'pr.sku_style' ) ] ) ) {

					$categories[ $ovc_product->data( 'pr.sku_style' ) ] = $ovc_product->data( 'st.wc_product_category' );
				}

				// Maybe add department product category
				if( !in_array( $ovc_product->data( 'pr.department' ), $categories ) && ( $ovc_product->data( 'pr.department' ) ) ) {

					$categories[] = $ovc_product->data( 'pr.department' );

					/*
					$wc_department_product_cat = $ovc_product->get_wc_department_product_cat_slug();

					if( $wc_department_product_cat ) {

						$categories[] = $wc_department_product_cat;
					}
					*/
				}

				// Add images to attachment id array
				if( $ovc_product->img_main ) {
					$image_ids[] = $ovc_product->img_main;	
				}
				if( $ovc_product->img_gallery_ids ) {
					$image_ids = array_merge( $image_ids, explode( ',', $ovc_product->img_gallery_ids ) );
				}
			}

			foreach( $variation_attributes as $taxonomy => $slug ) {

				if( isset( $attributes[ $taxonomy ] ) 
					&& $attributes[ $taxonomy ]->is_taxonomy()
					&& $slug
				) {

					// Determine term_id from slug 
					// (might need to create it [again] if term was deleted for whatever reason)
					$attr_term = get_term_by( 'slug', $slug, $taxonomy );

					if( !$attr_term && $ovc_product ) {
						//If term not found, run the method that is used to initialize/create on wc variation sync
						$method_name = ( 'pa_wc-size' === $taxonomy ) ? 'format_attribute_pa_size' : 'format_attribute_pa_color';

						$ovc_product->$method_name();

						$attr_term = get_term_by( 'slug', $slug, $taxonomy );

						if( !$attr_term ) {
							$results->notice( "WARNING! Product ID: {$ovc_product->ID} - Invalid term slug: {$slug} for taxonomy: {$taxonomy}" );
						}
					}

					if( $attr_term ) {

						// Add child attribute value term ID to parent attributes
						$attribute = $attributes[ $taxonomy ];
						$options = $attribute->get_options();
						$options[] = $attr_term->term_id; // Will process this more later
						$options = $attribute->set_options( array_unique( $options ) );
					}

					

					if( $ovc_product ) {

						$md5_slug = md5( $slug );

						if( 'pa_color' == $taxonomy 
							&& !array_key_exists( $md5_slug, $swatch_meta[ $md5_pa_color ]['attributes'] )
							&& $ovc_product->get_best_swatch_id()
						) {
							$swatch_meta[ $md5_pa_color ]['attributes'][ $md5_slug ] = array(
								'type'	=> 'image',
								'color'	=> '#FFFFFF',
								'image'	=> strval( $ovc_product->get_best_swatch_id() )
							);

							// Add swatch to image_ids
							$image_ids[] = $ovc_product->get_best_swatch_id();
						}
						else if( 'pa_wc-size'
								 && !array_key_exists( $md5_slug, $swatch_meta[ $md5_pa_wc_size ]['attributes'] )
						) {
							$swatch_meta[ $md5_pa_wc_size ]['attributes'][ $md5_slug ] = array(
								'type'	=> 'color',
								'color'	=> '#FFFFFF',
								'image'	=> '0'
							);
						}
					}
				}
			}
		}

		// Finish processing swatch meta
		if( $existing_meta = $wc_parent->get_meta( '_swatch_type_options', false, 'edit' ) ) {
				
			$existing_meta = array_pop( $existing_meta );
			$wc_parent->update_meta_data( '_swatch_type_options', $swatch_meta, $existing_meta->id );
		}
		else {

			$wc_parent->add_meta_data( '_swatch_type_options', $swatch_meta, true );
		}
		
		// Convert attribute term slugs to term ids
		// foreach( $attributes as $taxonomy => &$attribute ) {
			
		// 	if( 'pa_brand' != $taxonomy ) {

		// 		$options_slugs = array_unique( $attribute->get_options() );

		// 		$options_ids = array();

		// 		foreach( $options_slugs as $slug ) {

		// 			$attr_term = get_term_by( 'slug', $slug, $taxonomy );

		// 			if( !$attr_term ) {
		// 				$msg = "Product ID: {$this->ID} - Invalid term slug: {$slug} for taxonomy: {$taxonomy}";
		// 				ovc_dev_log( $msg );
		// 				$results->notice( $msg );
		// 				//$results->error("Invalid term slug: {$slug} for taxonomy: {$taxonomy}" );
		// 			}
		// 			else {
		// 				$options_ids[] = $attr_term->term_id;
		// 			}
		// 		}

		// 		$attribute->set_options( $options_ids );
		// 	}
		// }

		// Finish processing image ids
		//$wpdb->query( "UPDATE {$wpdb->posts} SET post_parent = 0 WHERE post_parent = {$wc_parent->id} AND post_type = 'attachment'" ); // already did this earlier in this function

		if( $image_ids ) {
			$image_ids = array_unique( array_map( 'intval', $image_ids ), SORT_NUMERIC );
			$wpdb->query( "UPDATE {$wpdb->posts} SET post_parent = {$wc_parent->get_id()} WHERE ID IN(" . implode( ',', $image_ids ) . ")" );	
		}

		// Finish processing categories
		$categories = array_unique( $categories );
		$category_ids 	= array();

		// Include parent categories (this should also automatically add the 'department' parent category)
		foreach( $categories as $cat_name ) {

			$term = get_term_by( 'name', $cat_name, 'product_cat' );

			if( !is_object( $term ) ) {
				
				$results->notice( "Invalid product category term name: {$cat_name}" );
			} else {

				$category_ids[] = intval( $term->term_id );

				if( $term->parent ) {

					$parent_term = get_term( $term->parent, 'product_cat' );
					$category_ids[] = intval( $parent_term->term_id );
				}
			}
		}

		// Special Handling for Bra / Panty Sets //dev:generalize
		// if( in_array( 'bras', $category_slugs ) && in_array( 'panties', $category_slugs ) ) {

		// 	$category_ids[] = 1235; //bra-panty-lingerie-sets
		// }

		$wc_parent_props['category_ids'] = $category_ids;

		$wc_parent->set_props( $wc_parent_props );

		WC_Product_Variable::sync( $wc_parent );

		$wc_parent = $this->wc_parent( true );

		$wc_parent_visibility = $wc_parent->is_in_stock() ? 'visible' : 'hidden';
		$wc_parent->set_catalog_visibility = $wc_parent_visibility;

		// If the product shouldn't sync to WC, exclude it.
		$terms = array( 'exclude-from-catalog', 'exclude-from-search' );
		if( !$parent_enabled ) {

			wp_set_object_terms( $wc_parent->get_id(), $terms, 'product_visibility' );
		} else {

			wp_remove_object_terms( $wc_parent->get_id(), $terms, 'product_visibility' );
		}

		// If Yoast SEO is installed, set the Primary Category to our Style Data Product Category
		if( class_exists('WPSEO_Primary_Term') ) {

			$primary_term = get_term_by( 'name', $this->data( 'st.wc_product_category' ), 'product_cat' );

			if( $primary_term ) {
				
				$wpseo_primary_term = new WPSEO_Primary_Term( 'product_cat', $wc_parent->get_id() );
				$wpseo_primary_term->set_primary_term( $primary_term->term_id );
			}
		}

		/*
		 * Save the Primary Category to meta to be used for both the Related Products and the Breadcrumbs
		 */
		$ovc_product_cat = $this->get_parent()->get_ovc_product_category();
		$product_categories = wc_get_product_term_ids( $wc_parent->get_id(), 'product_cat' );

		if( $ovc_product_cat instanceof WP_Term ) {
			
			// If it's in the category list, remove it and add it to the front of the array
			if( in_array( $ovc_product_cat->term_id, $product_categories ) ) {

				array_diff( $product_categories, array( $ovc_product_cat->term_id ) );
			}
			array_unshift( $product_categories, $ovc_product_cat->term_id );
		}
		
		$wc_parent->update_meta_data( 'ovc_product_cats', $product_categories );

		// Delete Product Transients during every save
		wc_delete_product_transients( $wc_parent->get_id() );

		return $wc_parent->save();
	}

	public function get_fresh_attributes() {
		OVC_Results::maybe_init( $results );

		$for_variations	= array( 'pa_color', 'pa_wc-size' );

		if( !isset( self::$attributes_template ) ) {

			$attributes_template = array();

			$attribute_names = array( 'pa_color', 'pa_wc-size', 'pa_brand' );

			foreach( $attribute_names as $position => $attribute_name ) {
				
				$attribute = new WC_Product_Attribute();
		        $attribute->set_id( wc_attribute_taxonomy_id_by_name( $attribute_name ) );
		        $attribute->set_name( $attribute_name );
		        $attribute->set_position( $position );
		        $attribute->set_visible( true );
		        $attribute->set_variation( in_array( $attribute_name, $for_variations ) );

		        $attributes_template[ $attribute_name ] = $attribute;
			}

			self::$attributes_template = $attributes_template;
		}

		$attributes = array();

		foreach( self::$attributes_template as $attributes_name => $attribute ) {
			$attributes[ $attributes_name ] = clone $attribute;
		}
		
		foreach( $attributes as $attribute ) {

			$options = array();

			$taxonomy = $attribute->get_name();

			if( !in_array( $taxonomy, $for_variations ) ) {

				if( 'pa_brand' == $taxonomy ) { //dev:generalize //dev:improve
					$options[] = $this->data( 'pa.brand' );
				} 
			}

			/*
			if( 'pa_brand' == $attribute->get_name() ) {

				$brand_term = get_term_by( 'name', $this->data( 'pa.brand' ), 'pa_brand' );
				$options[] = $brand_term->term_id;
			}*/
			 
			$attribute->set_options( $options );
		}

		return $attributes;
	}

	// Format WC Parent Description
	public function format_wc_parent_description() {
		global $wpdb;

		$wc_parent_description = '';

		// First, get parent description from product's shared data
		if( $this->data( 'pa.description' ) ) {
			$wc_parent_description .= '<p class="ovc-descrip ovc-parent-descrip">' . stripslashes( $this->data( 'pa.description' ) ) . '</p>';
		}

		// Second, get shared descriptions by style #
		$sql = 
			"SELECT 
				st.description AS 'style_description',
				st.product_title AS 'style_product_title', 
				GROUP_CONCAT( DISTINCT pr.sku_style SEPARATOR ',' ) AS 'sku_styles'
			FROM 
				wp_ovc_products pr 
				LEFT JOIN wp_ovc_style_data st
					ON pr.sku_style = st.style 
			WHERE 
				pr.parent_sku = '{$this->parent_sku}' 
			GROUP BY 
				pr.parent_sku";

		$style_descriptions = $wpdb->get_results( $sql, ARRAY_A );

		foreach( $style_descriptions as $descr ) {

			if( !$descr['style_description'] ) {
				continue;
			}

			$wc_parent_description .= '<h3 class="ovc-descrip-subh">' . stripslashes( $descr['style_product_title'] ) . '<span> Applies to style numbers: ' . $descr['sku_styles'] . '</span></h3>';
			$wc_parent_description .= '<p class="ovc-descrip">' . stripslashes( $descr['style_description'] ) . '</p>';
		}

		return $wc_parent_description;
	}

	// TITLE FORMATTING FUNCTIONS

	// Custom formatting for WC Variation Title
	public function format_wc_variation_title() {

		$pk_txt = ( 1 == $this->sku_pkqty ? '' : "({$this->sku_pkqty} {$this->pk_str})" );
		$variation_title = $this->brand . " " . trim( $this->data( 'st.product_title' ) ) . " " . $pk_txt . " #" . $this->sku;

		return stripslashes( $variation_title );
	}

	// Custom formatting for WC Parent Title
	public function format_wc_parent_title() {
		global $wpdb;

		// Get variation min/max sku_pkqty and possible differences in pk_str
		$pk_qty_vars = $wpdb->get_row( 
			$wpdb->prepare( 
				"
				SELECT MIN( sku_pkqty ) AS min_sku_pkqty, MAX( sku_pkqty ) AS max_sku_pkqty, MIN( pk_str ) AS min_pk_str, MAX( pk_str ) AS max_pk_str, parent_sku 
				FROM wp_ovc_products 
				WHERE parent_sku = %s 
				AND sync_oms = %d 
				AND post_id <> 0 
				AND size <> 'CS'
				", 
				$this->parent_sku,
				1 
			), 
			ARRAY_A 
		);

		// Use 'Pairs' in parent title only if all variations use 'Pairs'
		$parent_pk_str = ( 'Pairs' == $pk_qty_vars['min_pk_str'] && 'Pairs' == $pk_qty_vars['max_pk_str'] ? 'Pairs' : 'Pack' );

		// Build range of sku_pkqty across variations (not displayed as a range if they are all the same )
		$parent_pk_qty = ( $pk_qty_vars['min_sku_pkqty'] != $pk_qty_vars['max_sku_pkqty'] ? "{$pk_qty_vars['min_sku_pkqty']}-{$pk_qty_vars['max_sku_pkqty']}" : "{$pk_qty_vars['min_sku_pkqty']}" );

		$parent_pk_txt = ( "1" == $parent_pk_qty ? '' : " ({$parent_pk_qty} {$parent_pk_str})" );

		$parent_title = $this->brand . " " . trim( $this->get_best_parent_title_text() ) . $parent_pk_txt . " #" . $this->parent_sku;

		// Strip slashes that get added on OVC fields by wpdb sanitization
		return stripslashes( $parent_title );
	}

	// OVC->WC FIELD FILTER FUNCTIONS
	public function filter_wc_variation_attributes() {

		return array(
			'pa_color'		=> $this->format_attribute_pa_color(),
			'pa_wc-size'	=> $this->format_attribute_pa_size()
		);
	}

	// Get correct slug for pa_color
	public function format_attribute_pa_color() {

		// Get all of the sibling products to $this not including the case product
		$siblings = OVCDB()->get_ovc_data_by_case_sku( $this->case_sku, 'ID, sku, size, sku_color, sync_wc, color_long_name, color_full_name, sku_pkqty', false );

		// $color_term_name defaults to color_full_name
		$color_term_name = $this->color_full_name;

		/*
		 * This will only happen if there is only a case product
		 *
		 * If there aren't any siblings, return the best color name (color_long_name or sku_color)
		 */
		if( !is_array( $siblings ) || ( 0 === count( $siblings ) ) ) {
			$color_term_name = $this->get_best_color_name();
		}
		/*
		 * This is the most common instance
		 *
		 * If there are siblings, and they have the same pack quantity and color full name, return that color full name
		 */
		elseif( 
			( 1 === count( array_unique( array_column( $siblings, 'sku_pkqty' ) ) ) ) 
			&& ( 1 === count( array_unique( array_column( $siblings, 'color_full_name' ) ) ) ) 
		) {

			$color_term_name = current( array_unique( array_column( $siblings, 'color_full_name' ) ) );
		}
		/*
		 * This is the instance where there are multiple pack quantities synced to WC
		 *
		 * If there are siblings, but they do not have the same color full name, 
		 * let's return the highest pack quantity color full name
		 */
		elseif( 1 !== count( array_unique( array_column( $siblings, 'color_full_name' ) ) ) ) {

			$sibling_pack_qties = array_column( $siblings, 'sku_pkqty' );

			$color_term_name = $siblings[ array_search( max( $sibling_pack_qties ), $sibling_pack_qties ) ]['color_full_name'];
		}

		$color_term = get_term_by( 'name', $color_term_name, 'pa_color' );

		// Add color term if it doesn't exist
		if( false === $color_term && $this->wc_parent() ) {

			$color_term_taxonomy_id = wp_set_object_terms( $this->wc_parent()->get_id(), $color_term_name, 'pa_color', true );
			if( is_wp_error( $color_term_taxonomy_id ) || !is_array( $color_term_taxonomy_id ) || empty( $color_term_taxonomy_id ) ) {
				return false;
			}

			$color_term = get_term_by( 'term_taxonomy_id', $color_term_taxonomy_id[0], 'pa_color' );
		}
		
		// Maybe fix color term name
		if ( 0 !== strcmp( $color_term->name, $color_term_name ) ) {
			wp_update_term( $color_term->term_id, 'pa_color', array( 'name' => $color_term_name ) );
		}

		return $color_term->slug;
	}

	// Get correct slug for pa_size
	public function format_attribute_pa_size() {
		// return OVCDB()->get_size_code_slug( $this->size ); // dev:generalize

		// $size_term_name = $this->size;

		// // Get normal slug
		// $size_term_slug = OVC_Lists()->get_size_code_slug( $size_term_name );
		// $size_term = get_term_by( 'slug', $size_term_slug, 'pa_size' );

		// if( 'CS' == $size_term_name ) {

		// 	$size_term_name = 'Case ' . $this->sku_style;
		// 	$wc_size_term = get_term_by( 'name', $size_term_name, 'pa_wc-size' );
		// } else {

		// 	$wc_size_term = get_term_by( 'name', $size_term->name, 'pa_wc-size' );
		// }

		// if( false === $wc_size_term && $this->wc_parent() ) {

		// 	// Make sure to create the Case Term if it doesn't exist
		// 	if( 'CS' !== $size_term_name ) {
		// 		$wc_size_term = get_term_by( 'slug', 'wc-' . $size_term_slug, 'pa_wc-size' );
		// 	}

		// 	if( false === $wc_size_term ) {

		// 		$wc_size_term_args = array(
		// 			'slug'	=> 'wc-size-' . strtolower( $size_term_name )
		// 		);
		// 		$wc_size_term_id = wp_insert_term( $size_term_name, 'pa_wc-size', $wc_size_term_args );

		// 		$wc_size_term_taxonomy_id = wp_set_object_terms( $this->wc_parent()->get_id(), $wc_size_term_id, 'pa_wc-size', true );
		// 		$wc_size_term = get_term_by( 'term_taxonomy_id', $wc_size_term_taxonomy_id[0], 'pa_wc-size' );
		// 	// Make sure the names match between wc-size and size
		// 	} elseif( ( $wc_size_term->name != $size_term->name )
		// 		&& ( 'CS' !== $size_term_name )
		// 	) {

		// 		$wc_size_term_args = array(
		// 			'name'	=> $size_term->name
		// 		);
		// 		wp_update_term( $wc_size_term->term_id, 'pa_wc-size', $wc_size_term_args );
		// 	}
		// }

		$size_term_name = OVC_Lists()->get_list_item_name( 'size', $this->size );

		if( 'Case' == $size_term_name ) {
			$size_term_name = 'Case ' . $this->sku_style;
		}

		$wc_size_term = get_term_by( 'name', $size_term_name, 'pa_wc-size' );
		
		// Add size term if it doesn't exist
		if( false === $wc_size_term && $this->wc_parent() ) {

			$size_term_slug = 'wc-size-' . strtolower( $this->size );
			
			if( 'CS' == $this->size ) {
				$size_term_slug .= '-' . strtolower( $this->sku_style );
			}

			$wc_size_term_args = array(
				'slug'	=> $size_term_slug
			);

			$wc_size_term_array = wp_insert_term( $size_term_name, 'pa_wc-size', $wc_size_term_args );
			// If error, just return false
			if( is_wp_error( $wc_size_term_array ) ) {
				return false;
			}

			$wc_size_term_taxonomy_id = wp_set_object_terms( $this->wc_parent()->get_id(), $wc_size_term_array['term_id'], 'pa_wc-size', true );
			// If error, just return false
			if( is_wp_error( $wc_size_term_taxonomy_id ) || !is_array( $wc_size_term_taxonomy_id ) || empty( $wc_size_term_taxonomy_id ) ) {
				return false;
			}
			$wc_size_term = get_term_by( 'term_taxonomy_id', $wc_size_term_taxonomy_id[0], 'pa_wc-size' );

		}

		if( 0 !== strcmp( $wc_size_term->name, $size_term_name ) ) {
			wp_update_term( $wc_size_term->term_id, 'pa_wc-size', array( 'name' => $size_term_name ) );
		}

		return $wc_size_term->slug;
	}

	// Build the Unit Qty Details string(s) that are displayed on the single product frontend
	public function format_unit_qty_details() {

		$unit_qty = '';

		if( 'CS' != $this->size ) {
			$unit_qty = '<span class="unit-qty-details">'; // dev:improve!! This shouldn't have to add the html wrapper

			// Determine item is a multi-item pack
			$plural = ( $this->sku_pkqty > 1 ? true : false );

			$unit_qty .= ( $plural ? '1 Pack of ' : '' );
			$unit_qty .= $this->sku_pkqty;
			$unit_qty .= ( 'Pairs' == $this->pk_str ? ' Pair' : ' Piece' );
			$unit_qty .= ( $plural ? 's' : '' );
			$unit_qty .= '</span>';
		}
		else {

			// Get Case Children info
			$case_children = OVCDB()->get_ovc_data_by_case_sku( $this->sku, 'ID, sku, size, sku_pkqty, pk_str, case_sku, unit_per_box' );

			if( $case_children ) {
				$unit_qty = '<span class="unit-qty-details">';

				$children_pkqty = intval( $case_children[0]['sku_pkqty'] );
				$children_plural = ( $children_pkqty > 1 ? true : false );
				$children_pk_str = ( 'Pairs' == $case_children[0]['pk_str'] ? 'Pair' : 'Piece' );

				$unit_qty .= "1 Case of {$this->sku_pkqty} Packs of {$children_pkqty} {$children_pk_str}";
				$unit_qty .= ( $children_plural ? 's' : '' ) . ' each';
				$unit_qty .= ' (' . strval( $children_pkqty * intval( $this->sku_pkqty ) ) . " {$children_pk_str}s total)";
				$unit_qty .= '</span>';

				foreach ( $case_children as $case_child ) {
					$plural = ( $case_child['sku_pkqty'] > 1 ? true : false );
					$size_term = get_term_by( 'slug', ( 'size-' . strtolower( $case_child['size'] ) ), 'pa_size' );

					// Leave the actual case off the list
					if( 'size-cs' == $size_term->slug ) {
						continue;
					}

					$unit_qty .= '<span class="case-child-details">';
					$unit_qty .= "{$case_child['unit_per_box']} x {$size_term->name} ({$case_child['sku_pkqty']}-{$children_pk_str} Pack" . ( $plural ? 's' : '' ) . ")";
					$unit_qty .= '</span>';
				}
			}
		}

		return $unit_qty;
	}

	// Determine the value for the _ovc_wc_status meta_key
	public function get_ovc_wc_status( &$results = false ) {
		return $this->should_be_wc() ? 'enabled' : 'disabled';
	}

	// Get WC department product_cat slug from pr.department
	public function get_wc_department_product_cat_slug() { // dev:generalize! dev:lists
		$department = $this->data( 'pr.department' );

		if( $department ) {
			$department .= in_array( $department, array( 'Men', 'Women' ) ) ? 's' : '';
			return sanitize_title( $department );
		}	
	}
}