<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_update_ovc_images_for_walmart {

	public function up() {

		$ovc_schema = array(
			'ovc_images' => array(
				'last_wa_ftp_sync'	=> array(
					'readonly'			=> 'true',
					'nice_name'			=> 'Last Walmart FTP Sync'
				)
			)
		);

		OVCSC::multi_update_field_meta( $ovc_schema );

		OVCSC::meta_add_array_item( 'dt.ovc_images', 'fg.meta_info', 'pic.last_wa_ftp_sync', 'pic.last_check' );

		OVC_Migrations::add_col_if_not_exists( 'ovc_images', 'last_wa_ftp_sync', 'DATETIME', 'last_check' );

		self::update_ovc_image_filenames();
	}

	/**
	 * Update the paths in OVC Images to use filenames instead
	 */
	public static function update_ovc_image_filenames() {
		global $wpdb;

		$ovc_images = $wpdb->get_results(
			"
			SELECT *
			FROM `wp_ovc_images`
			"
		);

		foreach( $ovc_images as $ovc_image ) {

			// Update the paths to filenames
			// self::update_path_to_filename( $ovc_image->path, $ovc_image, 'path' );
			self::update_path_to_filename( $ovc_image->image_3_4_path, $ovc_image, 'image_3_4_path' );
			self::update_path_to_filename( $ovc_image->image_override_path, $ovc_image, 'image_override_path' );

			if( 50 < ox_exec_time() ) {

				update_option( 'ovc_migration_version', '20181003' );
				wp_redirect( admin_url() );
				exit;
			}
		}
	}

	/**
	 * Returns whether a path has been updated to use filenames or not
	 * 
	 * @param 	string 			$path
	 * @param 	bool|OVC_Image 	$ovc_image 
	 * 
	 * @return 	bool
	 */
	public static function is_path_updated( $path = '', $ovc_image = false ) {

		if( !$path || !$ovc_image ) {
			return true;
		}

		if( 
			false !== strpos( $path, '/' ) 
			|| 0 !== strpos( $path, $ovc_image->ID . '__' )
		) {
			return false;
		}

		return true;
	}

	/**
	 * Update a path to use filename
	 * 
	 * @param 	string 			$path 
	 * @param 	bool|OVC_Image 	$ovc_image 
	 * 
	 * @return 	bool
	 */
	public static function update_path_to_filename( $path = '', $ovc_image = false, $path_name = 'path' ) {

		if( !$path || !$ovc_image ) {
			return false;
		}

		if( !self::is_path_updated( $path, $ovc_image ) ) {
			global $wpdb;

			$old_filename = basename( $path );
			$new_filename = "{$ovc_image->ID}__{$old_filename}";

			$path_updated = $wpdb->update( 'wp_ovc_images', array( $path_name => $new_filename ), array( 'ID' => $ovc_image->ID ) );

			if( false !== $path_updated ) {

				// If the file exists, change the file's name as well as the path
				if( file_exists( $path ) ) {

					$file_renamed = rename( get_ovc_image_path( $old_filename ), OVC_IMAGE_DIR . '/' . $new_filename );

					if( !$file_renamed ) {
						error_log('We couldn\'t rename the file from ' . $old_filename . ' to ' . $new_filename);
					}
				}
			} else {
				return false;
			}
		}

		return true;
	}
}