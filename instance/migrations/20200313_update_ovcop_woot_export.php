

<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_update_ovcop_woot_export {

	public function up() {

		$schema = array(
            'field_set_meta'	=> array(
                'woot_export'	=> array(
                    'excel_worksheet'	=> 'Sheet1',
                    'start_row' => 1
                )
            )
        );
        
        OVCSC::multi_update_field_meta( $schema );
    }
}
