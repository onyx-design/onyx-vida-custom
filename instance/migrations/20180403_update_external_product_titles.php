<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_update_external_product_titles {

	public function up() {

		$schema = array(
			'amazon' => array(
				'item_name' => array(
					'filter_func'	=> 'filter_external_product_name_with_style_size'
				),
			),
			'groupon' => array(
				'Unique Product Name' => array(
					'filter_func'	=> 'filter_external_product_name'
				)
			),
			'dropship' => array(
				'Unit Weight' => array(
					'order'		=> '13',
					'ovc_field'	=> 'pr.unit_weight'
				),
				'Country of Origin' => array(
					'order'	=> '14'
				),
				'Fast Facts 1' => array(
					'order'	=> '15'
				),
				'Fast Facts 2' => array(
					'order'	=> '16'
				),
				'Material Content' => array(
					'order'	=> '17'
				),
				'Sizing Guide' => array(
					'order'	=> '18'
				),
				'Care Instructions' => array(
					'order'	=> '19'
				),
				'Product Description' => array(
					'order'	=> '20'
				),
			),
			'wc_parent_props' => array(
				'name'	=> array(
					'filter_func'	=> 'filter_external_product_name'
				)
			)
		);

		OVCSC::multi_update_field_meta( $schema );
	}
}