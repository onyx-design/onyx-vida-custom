<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_alter_data_errors__add_resolved_by_user {

	public function up() {

		$err_fs = OVCSC::get_field_set( 'err' );

		$schema = array(
			'data_errors'	=> array(
				'resolved_by_user'	=> array(
					'nice_name'		=> 'Resolved By',
					'readonly'		=> 'true'
				),
			),
			'ovcdt'	=> array(
				'data_errors'	=> array(
					'fields'	=> array(
						'err.ID', 
						'errcd.error_code',
						'err.ovc_id',
						'err.active',
						'err.error_data',
						'errcd.severe',
						'errcd.actions_needed',
						'errcd.actions_prevented',
						'err.notes',
						'errcd.name',
						'errcd.description',
						'err.resolved_by_user',
						'err.date_resolved',
						'err._meta_updated',
						'err._meta_created'
					),
				)
			)
		);

		OVCSC::multi_update_field_meta( $schema );

		OVC_Migrations::add_col_if_not_exists( $err_fs, 'resolved_by_user', "INT NOT NULL DEFAULT 0", 'notes' );
	}
}