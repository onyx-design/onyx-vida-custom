<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_alter_ovcop_log_add_meta_created {

	public function up() {
		global $wpdb;

		$ovc_schema = array(
			'ovcop_log'	=> array(
				'archived'		=> array(
					'nice_name'		=> 'Archived',
					'var_type'		=> 'bool',
					'readonly'		=> 'true'
				),
				'_meta_created'	=> array(
					'nice_name'		=> 'Date Created',
					'var_type'		=> 'datetime',
					'readonly'		=> 'true'
				)
			),
			'ovcdt'		=> array(
				'ovcop_log'		=> array(
					'fields'		=> array(
						'ol.ID',
						'ol.type',
						'wu.display_name',
						'ol.status',
						'ol.archived',
						'ol.start_time',
						'ol.end_time',
						'ol.last_seen',
						'ol._meta_created'
					)
				)
			)
		);

		OVCSC::multi_update_field_meta( $ovc_schema );

		OVC_Migrations::add_col_if_not_exists( 'ovcop_log', 'archived', 'TINYINT(1) NOT NULL DEFAULT 0', 'user_id' );
		OVC_Migrations::add_col_if_not_exists( 'ovcop_log', '_meta_created', 'DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP', 'data' );

		// Set _meta_created equal to start_time when start_time isn't blank, or the last rows start_time + 1 second
		$wpdb->query( 
			"
				UPDATE {$wpdb->prefix}ovcop_log
				SET _meta_created = (
					@created := IF( 
						UNIX_TIMESTAMP( start_time ) != 0,
						start_time, 
						DATE_ADD( @created, INTERVAL 1 SECOND )
					)
				)
				ORDER BY ID ASC;
			"
		);
	}
}