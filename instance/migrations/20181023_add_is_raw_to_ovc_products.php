<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_add_is_raw_to_ovc_products {

	public function up() {
		global $wpdb;

		$ovc_schema = array(
			'ovcdt'			=> array(
				'ovc_products'	=> array(
					'fg.inventory'	=> array(
						'pr.case_sku',
						'pr.is_case',
						'pr.is_raw',
						'pr.stock',
						'pr.so_qty',
						'pr.avail_qty',
						'pr.potential_qty',
						'pr.reserve_qty',
						'pr.size_ratio',
						'pr.comp_level',
						'pr.dozens',
						'pr.unit_per_case',
						'pr.unit_per_box',
					)
				)
			),
			'ovc_products'	=> array(
				'is_raw'		=> array(
					'nice_name'				=> 'Is Raw',
					'var_type'				=> 'bool',
					'on_duplicate_value'	=> '0'
				),
				'potential_qty'	=> array(
					'nice_name'				=> 'Potential Qty',
					'readonly'				=> 'true',
					'custom_meta_updated'	=> 'stock',
					'var_type'				=> 'stock',
					'on_duplicate_value'	=> '0'
				),
				'comp_level'	=> array(
					'readonly'				=> 'true'
				)
			)
		);

		OVCSC::multi_update_field_meta( $ovc_schema );

		OVC_Migrations::add_col_if_not_exists( 'ovc_products', 'is_raw', 'TINYINT(1) NOT NULL DEFAULT 0', 'is_case' );
		OVC_Migrations::add_col_if_not_exists( 'ovc_products', 'potential_qty', 'MEDIUMINT(9) NOT NULL DEFAULT 0' );

		$wpdb->query( "UPDATE {$wpdb->prefix}ovc_products SET is_raw = 1 WHERE comp_level = 'R';" );
	}
}