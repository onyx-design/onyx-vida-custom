<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_update_fast_facts_fields_length {

	public function up() {

		$schema = array(
			'parent_data'	=> array(
				'fast_facts1'	=> array(
					'nice_name'		=> 'Fast Facts 1',
					'maxlength'		=> '500'
				),
				'fast_facts2'	=> array(
					'nice_name'		=> 'Fast Facts 2',
					'maxlength'		=> '500'
				),
				'fast_facts3'	=> array(
					'nice_name'		=> 'Fast Facts 3',
					'maxlength'		=> '500'
				),
				'fast_facts4'	=> array(
					'nice_name'		=> 'Fast Facts 4',
					'maxlength'		=> '500'
				),
			),
			'style_data'	=> array(
				'fast_facts1'	=> array(
					'nice_name'		=> 'Fast Facts 1',
					'maxlength'		=> '500'
				),
				'fast_facts2'	=> array(
					'nice_name'		=> 'Fast Facts 2',
					'maxlength'		=> '500'
				),
				'fast_facts3'	=> array(
					'nice_name'		=> 'Fast Facts 3',
					'maxlength'		=> '500'
				),
				'fast_facts4'	=> array(
					'nice_name'		=> 'Fast Facts 4',
					'maxlength'		=> '500'
				),
			),
			'ovcdt'			=> array(
				'parent_data'	=> array(
					'fields'		=> array(
						'pa.ID',
						'pa.parent_sku',
						'pa.brand',
						'pa.product_title',
						'pa.featured',
						'pa.homepage',
						'pa.description',
						'pa.fast_facts1',
						'pa.fast_facts2',
						'pa.fast_facts3',
						'pa.fast_facts4',
						'pa.sizing',
						'pa.care',
						'pa.care_old',
						'pa.menu_order',
						'pa.img_main',
						'pa.img_gallery_ids',
						'pa.wc_parent_id',
						'pa._meta_updated',
					)
				)
			)
		);

		OVCSC::multi_update_field_meta( $schema );

		$st_fs = OVCSC::get_field_set( 'style_data' );
		OVC_Migrations::update_col( $st_fs, 'fast_facts1', "VARCHAR(500) NOT NULL DEFAULT ''" );
		OVC_Migrations::update_col( $st_fs, 'fast_facts2', "VARCHAR(500) NOT NULL DEFAULT ''" );
		OVC_Migrations::update_col( $st_fs, 'fast_facts3', "VARCHAR(500) NOT NULL DEFAULT ''" );
		OVC_Migrations::add_col_if_not_exists( $st_fs, 'fast_facts4', "VARCHAR(500) NOT NULL DEFAULT ''", 'fast_facts3' );

		$pa_fs = OVCSC::get_field_set( 'parent_data' );
		OVC_Migrations::update_col( $pa_fs, 'fast_facts1', "VARCHAR(500) NOT NULL DEFAULT ''" );
		OVC_Migrations::update_col( $pa_fs, 'fast_facts2', "VARCHAR(500) NOT NULL DEFAULT ''" );
		OVC_Migrations::update_col( $pa_fs, 'fast_facts3', "VARCHAR(500) NOT NULL DEFAULT ''" );
		OVC_Migrations::add_col_if_not_exists( $pa_fs, 'fast_facts4', "VARCHAR(500) NOT NULL DEFAULT ''", 'fast_facts3' );
	}
}