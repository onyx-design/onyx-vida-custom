<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_update_style_data__amazon_export_fields {

	public function up() {

		$schema = array(
			// Add to Database
			'style_data'	=> array(
				'amazon_color_map'	=> array(
					'var_type'		=> 'list:amazon_color_map',
					'nice_name'		=> 'Amazon Color Map',
					'product_type'	=> 'General'
				),
				'amazon_fabric_wash'	=> array(
					'var_type'		=> 'list:amazon_fabric_wash',
					'nice_name'		=> 'Amazon Fabric Wash',
					'product_type'	=> 'General'
				),
				'amazon_pattern_type'	=> array(
					'var_type'		=> 'list:amazon_pattern_type',
					'nice_name'		=> 'Amazon Fabric Wash',
					'product_type'	=> 'General'
				),
				'amazon_size_name'	=> array(
					'nice_name'		=> 'Amazon Size Name',
					'product_type'	=> 'General',
					'maxlength'		=> '20'
				),
				// Use a filter function for this?
				'amazon_size_map'	=> array(
					'var_type'		=> 'list:amazon_size_map',
					'nice_name'		=> 'Amazon Size Map',
					'product_type'	=> 'General'
				),
				'amazon_strap_type'	=> array(
					'var_type'		=> 'list:amazon_strap_type',
					'nice_name'		=> 'Amazon Strap Type',
					'product_type'	=> 'Bras'
				),
				'amazon_style_name'	=> array(
					'var_type'		=> 'list:amazon_style_name',
					'nice_name'		=> 'Amazon Style Name',
					'product_type'	=> 'Bras'
				),
				'amazon_top_style'	=> array(
					'var_type'		=> 'list:amazon_top_style',
					'nice_name'		=> 'Amazon Top Style',
					'product_type'	=> 'Bras'
				),
				'amazon_underwire_type'	=> array(
					'var_type'		=> 'list:amazon_underwire_type',
					'nice_name'		=> 'Amazon Underwire Type',
					'product_type'	=> 'Bras'
				)
			),
			/*
			 * Amazon OVC Lists
			 */
			'ovc_list'		=> array(
				'amazon_color_map'	=> array(
					'type'			=> 'simple',
					'valid_values'	=> array(
						'Beige',
						'Black',
						'Blue',
						'Bronze',
						'Brown',
						'Gold',
						'Green',
						'Grey',
						'Metallic',
						'Multicoloured',
						'Off-White',
						'Orange',
						'Pink',
						'Purple',
						'Red',
						'Silver',
						'Transparent',
						'Turquoise',
						'White',
						'Yellow',
						'gray',
						'multicolored'
					),
					'allow_blank'	=> 'true'
				),
				'amazon_fabric_wash'		=> array(
					'type'			=> 'simple',
					'valid_values'	=> array(
						'Dark',
						'Light',
						'Medium'
					),
					'allow_blank'	=> 'true'
				),
				'amazon_pattern_type'	=> array(
					'type'			=> 'simple',
					'valid_values'	=> array(
						'Checkered',
						'Paisley',
						'Patterned',
						'Plaid',
						'Polka Dots',
						'Solid',
						'String',
						'Stripes'
					),
					'allow_blank'	=> 'true'
				),
				'amazon_size_map'	=> array(
					'type'			=> 'simple',
					'valid_values'	=> array(
						'XXXXX-Large',
						'XXXX-Large',
						'XXX-Large',
						'XX-Large',
						'X-Large',
						'Large',
						'Medium',
						'Small',
						'X-Small',
						'XX-Small',
						'XXX-Small',
						'XXXX-Small',
						'XXXXX-Small'
					),
					'allow_blank'	=> 'true'
				),
				'amazon_strap_type'	=> array(
					'type'			=> 'simple',
					'valid_values'	=> array(
						'Adjustable',
						'Backless',
						'Basic',
						'Convertible',
						'Halter',
						'Invisible',
						'Racerback',
						'Strapless',
						'compression',
						'flannel-lined',
						'heavyweight',
						'lightweight',
						'lined',
						'midweight',
						'non-breathable',
						'seam-sealed',
						'stretch',
						'tearaway',
						'water-resistant',
						'waterproof',
						'windproof',
						'wrinkle-resistant'
					),
					'allow_blank'	=> 'true'
				),
				'amazon_style_name'	=> array(
					'type'			=> 'simple',
					'valid_values'	=> array(
						'Balconette',
						'Classic',
						'Demi',
						'Double-Breasted',
						'Full Coverage',
						'Modern/Fitted',
						'Molded',
						'Padded',
						'Plunge',
						'Push-Up',
						'Seamless',
						'Soft'
					),
					'allow_blank'	=> 'true'
				),
				'amazon_top_style'	=> array(
					'type'			=> 'simple',
					'valid_values'	=> array(
						'Bandeaux',
						'Button Down',
						'Cami',
						'Halter',
						'One Shoulder',
						'Racerback',
						'Strapless/Tube',
						'Tank',
						'Triangle Tops'
					),
					'allow_blank'	=> 'true'
				),
				'amazon_underwire_type'	=> array(
					'type'			=> 'simple',
					'valid_values'	=> array(
						'Underwire',
						'Wire Free'
					),
					'allow_blank'	=> 'true'
				)
			)
		);

		OVCSC::multi_update_field_meta( $schema );
	}
}