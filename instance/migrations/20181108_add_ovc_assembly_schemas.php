<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_add_ovc_assembly_schemas {

	public function up() {

		$this->add_ovc_assemblies_schema();

		$this->add_ovc_assemblies_db();

		$this->add_ovc_assembly_items_schema();

		$this->add_ovc_assembly_items_db();

		$this->add_ovc_assembly_ref_schema();
	}

	/**
	 * Setup the OVC Assemblies OVC Schema
	 */
	private function add_ovc_assemblies_schema() {

		$ovc_assemblies_schema = array(
			'field_set_meta'	=> array(
				'ovc_assemblies'	=> array(
					'nice_name'				=> 'OVC Assemblies',
					'sql_alias'				=> 'asm',
					'table_name'			=> 'wp_ovc_assemblies',
					'type'					=> 'database',
					'delete_access'			=> array( 'administrator' ),
					'tbl_refs_parents'		=> array( 'ovc_products' ),
					'tbl_refs_children'		=> array( 'ovc_assembly_items' )
				)
			),
			'ovc_assemblies'	=> array(
				'ID'				=> array(
					'nice_name'			=> 'ID',
					'var_type'			=> 'int'
				),
				'ovc_id'			=> array(
					'nice_name'			=> 'OVC ID',
					'foreign_key'		=> 'pr.ID',
					'var_type'			=> 'int'
				),
				'total_quantity'	=> array(
					'nice_name'			=> 'Total Quantity',
					'var_type'			=> 'int'
				),
				'_meta_updated'		=> array(
					'nice_name'			=> 'Date Updated',
					'var_type'		=> 'datetime',
				),
				'_meta_created'		=> array(
					'nice_name'			=> 'Date Created',
					'var_type'		=> 'datetime',
				)
			),
			'ovcdt'				=> array(
				'ovc_assemblies'	=> array(
					'force_readonly_fields'	=> 'true'
				)
			)
		);

		OVCSC::multi_update_field_meta( $ovc_assemblies_schema );
	}

	/**
	 * Setup the OVC Assemblies Database Table
	 */
	private function add_ovc_assemblies_db() {

		$asm_fs = OVCSC::get_field_set( 'asm' );

		$ovc_assemblies_cols = array(
			'ovc_id'			=> 'INT UNSIGNED NOT NULL DEFAULT 0',
			'total_quantity'	=> 'INT UNSIGNED NOT NULL DEFAULT 0',
			'_meta_updated'		=> 'DATETIME ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
			'_meta_created'		=> 'DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP'
		);

		$after = 'ID';

		OVC_Migrations::add_table_if_not_exists( $asm_fs );

		foreach( $ovc_assemblies_cols as $col_name => $col_def ) {

			OVC_Migrations::add_col_if_not_exists( $asm_fs, $col_name, $col_def, $after );

			$after = $col_name;
		}
	}

	/**
	 * Setup the OVC Assembly Items OVC Schema
	 */
	private function add_ovc_assembly_items_schema() {

		$ovc_assembly_items_schema = array(
			'field_set_meta'	=> array(
				'ovc_assembly_items'=> array(
					'nice_name'			=> 'OVC Assembly Items',
					'sql_alias'			=> 'asmi',
					'table_name'		=> 'wp_ovc_assembly_items',
					'type'				=> 'database',
					'delete_access'		=> array( 'administrator' ),
					'tbl_refs_parents'	=> array( 'ovc_assemblies', 'ovc_products' ),
				)
			),
			'ovc_assembly_items' => array(
				'ID'				=> array(
					'nice_name'			=> 'ID',
					'var_type'			=> 'int',
				),
				'assembly_id'		=> array(
					'nice_name'			=> 'Assembly ID',
					'foreign_key'		=> 'asm.ID',
					'var_type'			=> 'int'
				),
				'ovc_id'			=> array(
					'nice_name'			=> 'OVC ID',
					'foreign_key'		=> 'pr.ID',
					'var_type'			=> 'int'
				),
				'quantity'			=> array(
					'nice_name'			=> 'Total Quantity',
					'var_type'			=> 'int'
				),
				'_meta_updated'		=> array(
					'nice_name'			=> 'Date Updated',
					'var_type'		=> 'datetime',
				),
				'_meta_created'		=> array(
					'nice_name'			=> 'Date Created',
					'var_type'		=> 'datetime',
				)
			),
			'ovcdt'				=> array(
				'ovc_assembly_items'	=> array(
					'force_readonly_fields'	=> 'true'
				)
			)
		);

		OVCSC::multi_update_field_meta( $ovc_assembly_items_schema );
	}

	/**
	 * Setup the OVC Assembly Items Database Table
	 */
	private function add_ovc_assembly_items_db() {

		$asmi_fs = OVCSC::get_field_set( 'asmi' );

		$ovc_assembly_item_cols = array(
			'assembly_id'	=> 'INT UNSIGNED NOT NULL DEFAULT 0',
			'ovc_id'		=> 'INT UNSIGNED NOT NULL DEFAULT 0',
			'quantity'		=> 'INT UNSIGNED NOT NULL DEFAULT 0',
			'_meta_updated'	=> 'DATETIME ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
			'_meta_created'	=> 'DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP'
		);

		$after = 'ID';

		OVC_Migrations::add_table_if_not_exists( $asmi_fs );

		foreach( $ovc_assembly_item_cols as $col_name => $col_def ) {

			OVC_Migrations::add_col_if_not_exists( $asmi_fs, $col_name, $col_def, $after );

			$after = $col_name;
		}
	}

	/**
	 * Setup the Reference Data in OVC Schema
	 */
	private function add_ovc_assembly_ref_schema() {

		$schema = array(
			'field_set_meta'	=> array(
				'ovc_products'		=> array(
					'tbl_refs_children'	=> array(
						'shopify_products',
						'walmart_product',
						'data_errors',
						'ovc_assemblies',
						'ovc_assembly_items'
					)
				)
			)
		);

		OVCSC::multi_update_field_meta( $schema );
	}
}