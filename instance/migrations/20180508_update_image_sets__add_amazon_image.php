<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_update_image_sets__add_amazon_image {

	public function up() {

		$schema = array(
			'image_sets'	=> array(
				// Add to Database
				'img_amazon'	=> array(
					'var_type'		=> 'int',
					'nice_name'		=> 'Amazon Image'
				)
			)
		);

		OVCSC::multi_update_field_meta( $schema );
	}
}