<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_update_amazon_new__update_defaults {

	public function up() {

		// Remove Size Map Field
		OVCSC::delete_field_meta( 'amazon_new', 'size_map', 'ovc_field' );
		OVCSC::delete_field_meta( 'amazon_new', 'fabric_type', 'filter_func' );

		$schema = array(
			'amazon_new'	=> array(
				'standard_price'	=> array(
					'ovc_field'		=> 'pr.price_amazon||decimal:2|strval'
				),
				'variation_theme'	=> array(
					'default_value'	=> 'sizecolor'
				),
				'size_map'			=> array(
					'filter_func'	=> 'filter_size_map_amazon'
				),
				'fabric_type'		=> array(
					'default'		=> ''
				)
			)
		);

		OVCSC::multi_update_field_meta( $schema );
	}
}