<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_update_ovc_images__add_ovc_permissions {

	public function up() {

		$schema = array(
			'ovcdt'			=> array(
				'ovc_images'	=> array(
					'edit_access'	=> array( 'vida_sr_data_tech' ),
				),
			),
			'ovc_images'	=> array(
				'status'		=> array(
					'edit_access'	=> array( 'vida_sr_data_tech' ),
				),
				'crop_mode'		=> array(
					'edit_access'	=> array( 'administrator' ),
				),
				'crop_focus'	=> array(
					'edit_access'	=> array( 'administrator' ),
				),
			)
		);

		OVCSC::multi_update_field_meta( $schema );
	}
}