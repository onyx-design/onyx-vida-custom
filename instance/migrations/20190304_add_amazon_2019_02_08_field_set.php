<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_add_amazon_2019_02_08_field_set {

	public function up() {

		$this->add_ovcop_schema();

		$this->add_amazon_2019_02_08_fs_schema();
	}

	private function add_ovcop_schema() {

		$ovcop_schema = array(
			'ovcop'	=> array(
				'amazon_export'	=> array(
					'field_set'		=> 'amazon_2019_02_08'
				)
			),
		);

		OVCSC::multi_update_field_meta( $ovcop_schema );
	}

	private function add_amazon_2019_02_08_fs_schema() {

		$amazon_export_fs_schema = array(
			'field_set_meta'	=> array(
				'amazon_2019_02_08'			=> array(
					'nice_name'				=> 'Amazon (2019.0208)',
					'ordered'				=> '1',
					'type'					=> 'external',
					'excel_worksheet'		=> 'Template',
					'start_row'				=> '4'
				)
			),
			'amazon_2019_02_08'		=> array()
		);

		$amazon_2019_02_08_export_fields = array(
			'feed_product_type'														=> array(
				// Leave Blank
			),
			'item_sku'																=> array(
				'ovc_field'																=> 'pr.sku',
			),
			'brand_name'															=> array(
				'ovc_field'																=> 'pa.brand',
			),
			'item_name'																=> array(
				'filter_func'															=> 'filter_external_product_name_with_style_size',
			),
			'external_product_id'													=> array(
				'ovc_field'																=> 'pr.upc_code',
			),
			'external_product_id_type'												=> array(
				'default_value'															=> 'UPC',
			),
			'item_type'																=> array(
				// Blank on the excel doc
				// 'filter_func'														=> 'filter_external_product_type'
			),
			'outer_material_type1'													=> array(
				// Leave Blank
			),
			'outer_material_type2'													=> array(
				// Leave Blank
			),
			'outer_material_type3'													=> array(
				// Leave Blank
			),
			'outer_material_type4'													=> array(
				// Leave Blank
			),
			'outer_material_type5'													=> array(
				// Leave Blank
			),
			'band_size_num'															=> array(
				// Leave Blank
			),
			'band_size_num_unit_of_measure'											=> array(
				// Leave Blank
			),
			'color_name'															=> array(
				'filter_func'															=> 'filter_color_name_amazon',
			),
			'color_map'																=> array(
				// Blank on the excel doc
				// 'filter_func'														=> 'filter_color_map_amazon',
			),
			'size_name'																=> array(
				// Blank on the excel doc
				// 'filter_func'														=> 'filter_size_name_amazon',
			),
			'special_features1'														=> array(
				// Leave Blank
			),
			'special_features2'														=> array(
				// Leave Blank
			),
			'special_features3'														=> array(
				// Leave Blank
			),
			'special_features4'														=> array(
				// Leave Blank
			),
			'special_features5'														=> array(
				// Leave Blank
			),
			'material_composition1'													=> array(
				// Leave Blank
			),
			'material_composition2'													=> array(
				// Leave Blank
			),
			'material_composition3'													=> array(
				// Leave Blank
			),
			'material_composition4'													=> array(
				// Leave Blank
			),
			'material_composition5'													=> array(
				// Leave Blank
			),
			'material_composition6'													=> array(
				// Leave Blank
			),
			'material_composition7'													=> array(
				// Leave Blank
			),
			'material_composition8'													=> array(
				// Leave Blank
			),
			'material_composition9'													=> array(
				// Leave Blank
			),
			'material_composition10'												=> array(
				// Leave Blank
			),
			'department_name'														=> array(
				'filter_func'															=> 'filter_department_name_amazon',
			),
			'size_map'																=> array(
				// Blank on the excel doc
				// 'filter_func'														=> 'filter_size_map_amazon'
			),
			'is_adult_product'														=> array(
				// Leave Blank
			),
			'standard_price'														=> array(
				'ovc_field'																=> 'pr.price_amazon||decimal:2|strval'
			),
			'quantity'																=> array(
				'default_value'															=> '0',
			),
			'main_image_url'														=> array(
				'filter_func'															=> 'filter_main_image_amazon',
			),
			'other_image_url1'														=> array(
				'filter_func'															=> 'filter_other_image_1_amazon',
			),
			'other_image_url2'														=> array(
				'filter_func'															=> 'filter_other_image_2_amazon',
			),
			'other_image_url3'														=> array(
				'filter_func'															=> 'filter_other_image_3_amazon',
			),
			'other_image_url4'														=> array(
				'filter_func'															=> 'filter_other_image_4_amazon',
			),
			'other_image_url5'														=> array(
				'filter_func'															=> 'filter_other_image_5_amazon',
			),
			'other_image_url6'														=> array(
				'filter_func'															=> 'filter_other_image_6_amazon',
			),
			'other_image_url7'														=> array(
				'filter_func'															=> 'filter_other_image_7_amazon',
			),
			'other_image_url8'														=> array(
				'filter_func'															=> 'filter_other_image_8_amazon',
			),
			'swatch_image_url'														=> array(
				'filter_func'															=> 'filter_swatch_image_amazon',
			),
			'parent_child'															=> array(
				'default_value'															=> 'Child',
			),
			'parent_sku'															=> array(
				'default_value'															=> '',
			),
			'relationship_type'														=> array(
				'default_value'															=> 'Variation',
			),
			'variation_theme'														=> array(
				'default_value'															=> 'sizecolor',
			),
			'update_delete'															=> array(
				'default_value'															=> 'Update'
			),
			'product_description'													=> array(
				'filter_func'															=> 'filter_amazon_product_description'
			),
			'closure_type'															=> array(
				// Leave Blank
			),
			'edition'																=> array(
				// Leave Blank
			),
			'sole_material'															=> array(
				// Leave Blank
			),
			'heel_type'																=> array(
				// Leave Blank
			),
			'model_year'															=> array(
				// Leave Blank
			),
			'heel_height'															=> array(
				// Leave Blank
			),
			'model'																	=> array(
				'ovc_field'																=> 'pr.sku',
			),
			'inner_material_type1'													=> array(
				// Leave Blank
			),
			'inner_material_type2'													=> array(
				// Leave Blank
			),
			'inner_material_type3'													=> array(
				// Leave Blank
			),
			'inner_material_type4'													=> array(
				// Leave Blank
			),
			'inner_material_type5'													=> array(
				// Leave Blank
			),
			'part_number'															=> array(
				// Leave Blank
			),
			'manufacturer'															=> array(
				// Leave Blank
			),
			'heel_height_unit_of_measure'											=> array(
				// Leave Blank
			),
			'bullet_point1'															=> array(
				'filter_func'															=> 'filter_amazon_bullet_point_1'
			),
			'bullet_point2'															=> array(
				'filter_func'															=> 'filter_amazon_bullet_point_2'
			),
			'bullet_point3'															=> array(
				'filter_func'															=> 'filter_amazon_bullet_point_3'
			),
			'bullet_point4'															=> array(
				'filter_func'															=> 'filter_amazon_bullet_point_4'
			),
			'bullet_point5'															=> array(
				// Blank on the excel doc
				// 'filter_func'														=> 'filter_amazon_bullet_point_5'
			),
			'generic_keywords'														=> array(
				'filter_func'															=> 'filter_keywords_amazon',
			),
			'style_keywords'														=> array(
				// Leave Blank
			),
			'fit_type'																=> array(
				// Leave Blank
			),
			'team_name'																=> array(
				// Leave Blank
			),
			'country_as_labeled'													=> array(
				// Leave Blank
			),
			'sport_type'															=> array(
				// Leave Blank
			),
			'theme'																	=> array(
				// Leave Blank
			),
			'toe_style'																=> array(
				// Leave Blank
			),
			'top_style'																=> array(
				// Leave Blank
			),
			'water_resistance_level'												=> array(
				// Leave Blank
			),
			'lifestyle'																=> array(
				// Leave Blank
			),
			'platinum_keywords1'													=> array(
				// Leave Blank
			),
			'platinum_keywords2'													=> array(
				// Leave Blank
			),
			'platinum_keywords3'													=> array(
				// Leave Blank
			),
			'platinum_keywords4'													=> array(
				// Leave Blank
			),
			'platinum_keywords5'													=> array(
				// Leave Blank
			),
			'seasons'																=> array(
				// Leave Blank
			),
			'target_audience_keywords1'												=> array(
				// Leave Blank
			),
			'target_audience_keywords2'												=> array(
				// Leave Blank
			),
			'target_audience_keywords3'												=> array(
				// Leave Blank
			),
			'target_audience_keywords4'												=> array(
				// Leave Blank
			),
			'target_audience_keywords5'												=> array(
				// Leave Blank
			),
			'catalog_number'														=> array(
				// Leave Blank
			),
			'belt_style'															=> array(
				// Leave Blank
			),
			'bottom_style'															=> array(
				// Leave Blank
			),
			'subject_character'														=> array(
				// Leave Blank
			),
			'chest_size'															=> array(
				// Leave Blank
			),
			'chest_size_unit_of_measure'											=> array(
				// Leave Blank
			),
			'collar_style'															=> array(
				// Leave Blank
			),
			'control_type'															=> array(
				// Leave Blank
			),
			'cup_size'																=> array(
				'filter_func'															=> 'filter_cup_size',
			),
			'fabric_wash'															=> array(
				// Leave Blank
			),
			'arch_type'																=> array(
				// Leave Blank
			),
			'front_style'															=> array(
				// Leave Blank
			),
			'inseam_length'															=> array(
				// Leave Blank
			),
			'inseam_length_unit_of_measure'											=> array(
				// Leave Blank
			),
			'cleat_description'														=> array(
				// Leave Blank
			),
			'cleat_material_type'													=> array(
				// Leave Blank
			),
			'rise_height'															=> array(
				// Leave Blank
			),
			'rise_height_unit_of_measure'											=> array(
				// Leave Blank
			),
			'leg_diameter'															=> array(
				// Leave Blank
			),
			'shoe_dimension_unit_of_measure'										=> array(
				// Leave Blank
			),
			'leg_diameter_unit_of_measure'											=> array(
				// Leave Blank
			),
			'shaft_height'															=> array(
				// Leave Blank
			),
			'leg_style'																=> array(
				// Leave Blank
			),
			'opacity'																=> array(
				// Leave Blank
			),
			'neck_size'																=> array(
				// Leave Blank
			),
			'neck_size_unit_of_measure'												=> array(
				// Leave Blank
			),
			'neck_style'															=> array(
				// Leave Blank
			),
			'pattern_type'															=> array(
				// Blank on the excel doc
				// 'ovc_field'															=> 'st.amazon_pattern_type',
			),
			'pocket_description'													=> array(
				// Leave Blank
			),
			'rise_style'															=> array(
				// Leave Blank
			),
			'special_size_type'														=> array(
				// Leave Blank
			),
			'sleeve_length'															=> array(
				// Leave Blank
			),
			'sleeve_length_unit_of_measure'											=> array(
				// Leave Blank
			),
			'sleeve_type'															=> array(
				// Leave Blank
			),
			'style_name'															=> array(
				// Leave Blank
			),
			'underwire_type'														=> array(
				// Blank on the excel doc
				// 'ovc_field'															=> 'st.amazon_underwire_type',
			),
			'waist_size'															=> array(
				// Leave Blank
			),
			'waist_size_unit_of_measure'											=> array(
				// Leave Blank
			),
			'strap_type'															=> array(
				// Blank on the excel doc
				// 'ovc_field'															=> 'st.amazon_strap_type',
			),
			'fur_description'														=> array(
				// Leave Blank
			),
			'material_type'															=> array(
				// Leave Blank
			),
			'website_shipping_weight'												=> array(
				// Leave Blank
			),
			'website_shipping_weight_unit_of_measure'								=> array(
				// Leave Blank
			),
			'height_map'															=> array(
				// Leave Blank
			),
			'platform_height'														=> array(
				// Leave Blank
			),
			'shoe_width'															=> array(
				// Leave Blank
			),
			'item_length_unit_of_measure'											=> array(
				// Leave Blank
			),
			'item_length'															=> array(
				'ovc_field'																=> 'pr.unit_length',
			),
			'item_width'															=> array(
				'ovc_field'																=> 'pr.unit_width',
			),
			'item_height'															=> array(
				'ovc_field'																=> 'pr.unit_height',
			),
			'item_dimensions_unit_of_measure'										=> array(
				'default_value'															=> 'IN'
			),
			'item_display_width'													=> array(
				// Leave Blank
			),
			'item_display_height_unit_of_measure'									=> array(
				// Leave Blank
			),
			'item_display_width_unit_of_measure'									=> array(
				// Leave Blank
			),
			'item_display_height'													=> array(
				// Leave Blank
			),
			'maximum_circumference'													=> array(
				// Leave Blank
			),
			'item_display_length'													=> array(
				// Leave Blank
			),
			'item_display_length_unit_of_measure'									=> array(
				// Leave Blank
			),
			'fulfillment_center_id'													=> array(
				// Leave Blank
			),
			'package_height'														=> array(
				// Leave Blank
			),
			'package_width'															=> array(
				// Leave Blank
			),
			'package_length'														=> array(
				// Leave Blank
			),
			'package_length_unit_of_measure'										=> array(
				// Leave Blank
			),
			'package_weight'														=> array(
				// Leave Blank
			),
			'package_weight_unit_of_measure'										=> array(
				// Leave Blank
			),
			'package_dimensions_unit_of_measure'									=> array(
				// Leave Blank
			),
			'cpsia_cautionary_statement'											=> array(
				// Leave Blank
			),
			'cpsia_cautionary_description'											=> array(
				// Leave Blank
			),
			'fabric_type'															=> array(
				'filter_func'															=> 'filter_amazon_fabric_type'
			),
			'import_designation'													=> array(
				// Leave Blank
			),
			'item_weight_unit_of_measure'											=> array(
				// Leave Blank
			),
			'item_weight'															=> array(
				// Leave Blank
			),
			'country_of_origin'														=> array(
				// Leave Blank
			),
			'batteries_required'													=> array(
				// Leave Blank
			),
			'are_batteries_included'												=> array(
				// Leave Blank
			),
			'battery_cell_composition'												=> array(
				// Leave Blank
			),
			'battery_type1'															=> array(
				// Leave Blank
			),
			'battery_type2'															=> array(
				// Leave Blank
			),
			'battery_type3'															=> array(
				// Leave Blank
			),
			'number_of_batteries1'													=> array(
				// Leave Blank
			),
			'number_of_batteries2'													=> array(
				// Leave Blank
			),
			'number_of_batteries3'													=> array(
				// Leave Blank
			),
			'battery_weight'														=> array(
				// Leave Blank
			),
			'battery_weight_unit_of_measure'										=> array(
				// Leave Blank
			),
			'number_of_lithium_metal_cells'											=> array(
				// Leave Blank
			),
			'number_of_lithium_ion_cells'											=> array(
				// Leave Blank
			),
			'lithium_battery_packaging'												=> array(
				// Leave Blank
			),
			'lithium_battery_energy_content'										=> array(
				// Leave Blank
			),
			'lithium_battery_energy_content_unit_of_measure'						=> array(
				// Leave Blank
			),
			'lithium_battery_weight'												=> array(
				// Leave Blank
			),
			'lithium_battery_weight_unit_of_measure'								=> array(
				// Leave Blank
			),
			'supplier_declared_dg_hz_regulation1'									=> array(
				// Leave Blank
			),
			'supplier_declared_dg_hz_regulation2'									=> array(
				// Leave Blank
			),
			'supplier_declared_dg_hz_regulation3'									=> array(
				// Leave Blank
			),
			'supplier_declared_dg_hz_regulation4'									=> array(
				// Leave Blank
			),
			'supplier_declared_dg_hz_regulation5'									=> array(
				// Leave Blank
			),
			'hazmat_united_nations_regulatory_id'									=> array(
				// Leave Blank
			),
			'safety_data_sheet_url'													=> array(
				// Leave Blank
			),
			'item_volume'															=> array(
				// Leave Blank
			),
			'item_volume_unit_of_measure'											=> array(
				// Leave Blank
			),
			'flash_point'															=> array(
				// Leave Blank
			),
			'legal_disclaimer_description'											=> array(
				// Leave Blank
			),
			'safety_warning'														=> array(
				// Leave Blank
			),
			'ghs_classification_class1'												=> array(
				// Leave Blank
			),
			'ghs_classification_class2'												=> array(
				// Leave Blank
			),
			'ghs_classification_class3'												=> array(
				// Leave Blank
			),
			'california_proposition_65_compliance_type'								=> array(
				// Leave Blank
			),
			'california_proposition_65_chemical_names1'								=> array(
				// Leave Blank
			),
			'california_proposition_65_chemical_names2'								=> array(
				// Leave Blank
			),
			'california_proposition_65_chemical_names3'								=> array(
				// Leave Blank
			),
			'california_proposition_65_chemical_names4'								=> array(
				// Leave Blank
			),
			'california_proposition_65_chemical_names5'								=> array(
				// Leave Blank
			),
			'list_price'															=> array(
				// Leave Blank
			),
			'condition_type'														=> array(
				// Leave Blank
			),
			'product_tax_code'														=> array(
				// Leave Blank
			),
			'condition_note'														=> array(
				// Leave Blank
			),
			'currency'																=> array(
				// Leave Blank
			),
			'fulfillment_latency'													=> array(
				// Leave Blank
			),
			'product_site_launch_date'												=> array(
				// Leave Blank
			),
			'merchant_release_date'													=> array(
				// Leave Blank
			),
			'restock_date'															=> array(
				// Leave Blank
			),
			'sale_price'															=> array(
				// Leave Blank
			),
			'sale_from_date'														=> array(
				// Leave Blank
			),
			'sale_end_date'															=> array(
				// Leave Blank
			),
			'offering_end_date'														=> array(
				// Leave Blank
			),
			'max_aggregate_ship_quantity'											=> array(
				// Leave Blank
			),
			'item_package_quantity'													=> array(
				// Leave Blank
			),
			'number_of_items'														=> array(
				// Leave Blank
			),
			'offering_can_be_gift_messaged'											=> array(
				'default_value'															=> 'FALSE',
			),
			'offering_can_be_giftwrapped'											=> array(
				'default_value'															=> 'FALSE',
			),
			'is_discontinued_by_manufacturer'										=> array(
				// Leave Blank
			),
			'max_order_quantity'													=> array(
				// Leave Blank
			),
			'merchant_shipping_group_name'											=> array(
				// Leave Blank
			),
			'offering_start_date'													=> array(
				// Leave Blank
			),
			'business_price'														=> array(
				// Leave Blank
			),
			'quantity_price_type'													=> array(
				// Leave Blank
			),
			'quantity_lower_bound1'													=> array(
				// Leave Blank
			),
			'quantity_price1'														=> array(
				// Leave Blank
			),
			'quantity_lower_bound2'													=> array(
				// Leave Blank
			),
			'quantity_price2'														=> array(
				// Leave Blank
			),
			'quantity_lower_bound3'													=> array(
				// Leave Blank
			),
			'quantity_price3'														=> array(
				// Leave Blank
			),
			'quantity_lower_bound4'													=> array(
				// Leave Blank
			),
			'quantity_price4'														=> array(
				// Leave Blank
			),
			'quantity_lower_bound5'													=> array(
				// Leave Blank
			),
			'quantity_price5'														=> array(
				// Leave Blank
			),
			'national_stock_number'													=> array(
				// Leave Blank
			),
			'unspsc_code'															=> array(
				// Leave Blank
			),
			'pricing_action'														=> array(
				// Leave Blank
			),
		);

		$amazon_export_order = 1;
		foreach( $amazon_2019_02_08_export_fields as $amazon_field => $amazon_field_values ) {

			// Generate order so we can be lazy if we need to add more fields in the middle
			$amazon_field_values['order'] = $amazon_export_order;

			$amazon_export_fs_schema['amazon_2019_02_08'][ $amazon_field ] = $amazon_field_values;

			$amazon_export_order++;
		}

		OVCSC::multi_update_field_meta( $amazon_export_fs_schema );
	}
}