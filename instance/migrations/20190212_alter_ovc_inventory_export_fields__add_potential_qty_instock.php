<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_alter_ovc_inventory_export_fields__add_potential_qty_instock {

	public function up() {

		$ovc_inventory_export_fields = array(
			'ovc_inventory_export'		=> array(
				'potential_qty_instock'		=> array(
					'filter_func'				=> 'filter_potential_qty_instock'
				)
			)
		);

		OVCSC::multi_update_field_meta( $ovc_inventory_export_fields );
	}
}