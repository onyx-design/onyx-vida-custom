<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_remove_tag_barcode_from_row_duplication {

	public function up() {

		$ovc_schema = array(
			'ovc_products'	=> array(
				'tag_barcode'	=> array(
					'on_duplicate_value'	=> ''
				)
			)
		);

		OVCSC::multi_update_field_meta( $ovc_schema );
	}
}