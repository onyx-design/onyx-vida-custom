<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_change_product_name_filters {

	public function up() {

		$ovc_schema = array(
			'amazon' 			=> array(
				'item_name'			=> array(
					'filter_func'		=> 'filter_amazon_product_name',
				),
			),
			'amazon_new' 		=> array(
				'item_name'			=> array(
					'filter_func'		=> 'filter_amazon_product_name',
				),
			),
			'wc_parent_props'	=> array(
				'name'				=> array(
					'filter_func'		=> 'filter_wc_parent_product_name',
				)
			),
			'shopify_variant'	=> array(
				'title'				=> array(
					'filter_func'		=> 'filter_shopify_variant_product_name'
				)
			),
			'zulily_intimates'	=> array(
				'Product Title'		=> array(
					'filter_func'		=> 'filter_zulily_product_name'
				)
			),
			'zulily_sexy'		=> array(
				'Style Name, Silhouette & Product Type'		=> array(
					'filter_func'		=> 'filter_zulily_product_name'
				)
			),
			'groupon'			=> array(
				'Unique Product Name'	=> array(
					'filter_func'		=> 'filter_groupon_product_name'
				)
			)
		);

		OVCSC::multi_update_field_meta( $ovc_schema );
	}
}