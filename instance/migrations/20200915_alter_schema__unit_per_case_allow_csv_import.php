

<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_alter_schema__unit_per_case_allow_csv_import {

	public function up() {
		// Enable allow_csv_import for pr.unit_per_case
		OVCSC::update_field_meta( 'ovc_products', 'unit_per_case', 'allow_csv_import', 'true' );
    }
}
