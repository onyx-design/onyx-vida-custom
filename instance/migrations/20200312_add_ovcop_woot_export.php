<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_add_ovcop_woot_export {

	public function up() {

		$schema = array( 
			'ovcop'	=> array(
				'woot_export'	=> array(
					'nice_name'	=> 'Woot Export',
					'descrip'	=> 'Export OVC Data in Woot format',
					'order'		=> 19,
					'classname'	=> 'OVCOP_woot_export',
					'field_set'	=> 'woot_export',
					'allowed_roles'	=> array(
						'administrator',
						'vida_sr_data_tech',
						'vida_data_tech'
					)
				)
			), 
			'woot_export'	=> array(
				'UPC or EAN' => array(
					'order' => 1,
					'ovc_field'	=> 'pr.upc_code'
				),
				'ASIN # (if in Amazon Catalog)' => array(
					'order' => 2,
					'ovc_field' => 'pr.asin'
				),
				'WIN (if repeat)' => array(
					'order' => 3,
					'default_value'	=> '',
				),
				'Brand Name' => array(
					'order' => 4,
					'ovc_field'	=> 'pa.brand'
				),
				'Short Product Title (40 character limit for PO)' => array(
					'order' => 5,
					'ovc_field' => 'pa.product_title'
				),
				'Long Product Title (if needed)' => array(
					'order' => 6,
					'filter_func'	=> 'filter_external_product_name'
				),
				'Link to Exact Item - Preferably Manufacturers Website' => array(
					'order' => 7,
					'filter_func'	=> 'filter_retail_link_groupon'
				),
				'Color (if applicable)' => array(
					'order' => 8,
					'ovc_field'	=> 'pr.color_full_name'
				),
				'Gender' => array(
					'order' => 9,
					'ovc_field' => 'pr.department'
				),
				'Size (if applicable)' => array(
					'order' => 10,
					'filter_func'	=> 'get_full_size_name'
				),
				'Model #' => array(
					'order' => 11,
					'ovc_field'	=> 'pr.sku'
				),
				'Manuf Part # (Unique Indentifier)' => array(
					'order' => 12,
					'ovc_field' => 'pr.sku'
				),
				'Manu. Warranty to End User (# of Days ONLY)' => array(
					'order' => 13,
					'default_value' => '0'
				),
				'Link to Warranty (if applicable)' => array(
					'order' => 14,
					'default_value' => ''
				),
				'Country of Origin' => array(
					'order' => 15,
					'ovc_field'	=> 'st.origin_country'
				),
				'product_condition' => array(
					'order' => 16,
					'default_value' => 'New'
				),
				'Package Type' => array(
					'order' => 17,
					'default_value' => 'Poly Bag'
				),
				'Item pacakge quanity' => array(
					'order' => 18,
					'default_value' => '1'
				),
				'count_value' => array(
					'order' => 19,
					'default_value' => ''
				),
				'MSRP (List Price)' => array(
					'order' => 20,
					'ovc_field'	=> 'pr.price_msrp'
				),
				'product_weight' => array(
					'order' => 21,
					'ovc_field'	=> 'pr.unit_weight'
				),
				'item_shipping_length' => array(
					'order' => 22,
					'ovc_field'	=> 'pr.unit_length'
				),
				'item_shipping_height' => array(
					'order' => 23,
					'ovc_field'	=> 'pr.unit_height'
				),
				'item_shipping_width' => array(
					'order' => 24,
					'ovc_field'	=> 'pr.unit_width'
				),
				'Purchase QTY' => array(
					'order' => 25,
					'ovc_field'	=> 'Purchase Quantity'
				),
				'Unit Cost' => array(
					'order' => 26,
					'ovc_field'	=> 'pr.qtyprice1'
				),
				'Negotiated Flip Cost (only applies to IOG TRANS)' => array(
					'order' => 27,
					'default_value' => ''
				),
				'GL (Item Category Family)' => array(
					'order' => 28,
					'default_value' => 'Apparel'
				),
				'Category (Item Category Code)' => array(
					'order' => 29,
					'default_value' => 'Category'
				),
				'Subcategory (Product Group Code)' => array(
					'order' => 30,
					'default_value' => 'Sub Category'
				),
				'UPC or EAN_1' => array(
					'order' => 31,
					'default_value' => 'Auto'
				),
				'Product title' => array(
					'order' => 32,
					'default_value' => 'Auto'
				),
				'Long Product Title (If needed).1' => array(
					'order' => 33,
					'default_value' => 'Auto'
				),
				'Bullet#1' => array(
					'order' => 34,
					'ovc_field'	=> 'st.fast_facts1'
				),
				'Bullet#2' => array(
					'order' => 35,
					'ovc_field'	=> 'st.fast_facts2'
				),
				'Bullet#3' => array(
					'order' => 36,
					'ovc_field'	=> 'st.fast_facts3'
				),
				'Bullet#4' => array(
					'order' => 37,
					'filter_func'	=> 'get_material_string'
				),
				'Bullet#5' => array(
					'order' => 38,
					'ovc_field'	=> 'st.care'
				),
				'Bullet#6' => array(
					'order' => 39,
					'default_value' => ''
				),
				'Bullet#7' => array(
					'order' => 40,
					'default_value' => ''
				),
				'Bullet#8' => array(
					'order' => 41,
					'default_value' => ''
				),
				'Bullet#9' => array(
					'order' => 42,
					'default_value' => ''
				),
				'Description(sales blurb)' => array(
					'order' => 43,
					'ovc_field'	=> 'pa.description'
				),
				'Flavour' => array(
					'order' => 44,
					'default_value' => ''
				),
				'Scent_value' => array(
					'order' => 45,
					'default_value' => ''
				),
				'Sun_protection_factor' => array(
					'order' => 46,
					'default_value' => ''
				),
				'Ingredients' => array(
					'order' => 47,
					'default_value' => ''
				),
				'Material_type' => array(
					'order' => 48,
					'default_value' => ''
				),
				'Product_type' => array(
					'order' => 49,
					'default_value' => ''
				),
				'Safety warnings' => array(
					'order' => 50,
					'default_value' => ''
				)
			),
			'field_set_meta'	=> array(
				'woot_export'	=> array(
					'nice_name'	=> 'Woot Export',
					'type'		=> 'external',
					'ordered'	=> 1
				)
			)
		);


		OVCSC::multi_update_field_meta( $schema );
	
	}
}