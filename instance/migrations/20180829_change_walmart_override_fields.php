<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_change_walmart_override_fields {

	public function up() {

		$ovc_schema = array(
			'walmart_product'	=> array(
				'override_sku'	=> array(
					'unique'		=> '1',
				),
				'override_upc'	=> array(
					'unique'		=> '1',
				),
			)
		);

		OVCSC::multi_update_field_meta( $ovc_schema );
	}
}