<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_update_shopify_variant__change_available_quantity {

	public function up() {

		// Remove Available Quantity
		OVCSC::delete_field_meta( 'shopify_variant', 'inventory_quantity', 'ovc_field' );
		OVCSC::delete_field_meta( 'shopify_variant', 'inventory_quantity', 'custom_meta_updated' );
		OVCSC::delete_field_meta( 'shopify_variant', 'inventory_quantity', 'calc_formula' );

		$schema = array(
			'shopify_variant'	=> array(
				'inventory_quantity'	=> array(
					'filter_func'		=> 'filter_shopify_avail_qty',
				)
			),
		);

		OVCSC::multi_update_field_meta( $schema );
	}
}