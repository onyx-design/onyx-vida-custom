

<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_alter_walmart_products_field_length {

	public function up() {
        // Increase the size of walmart_product productName field
        $wa_fs = OVCSC::get_field_set( 'walmart_product' );
        OVC_Migrations::update_col( $wa_fs, 'productName', 'varchar(256)' );
    }
}
