<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_update_ovc_images_schema_add_field_groups {

	public function up() {

		$schema = array(
			'ovc_images' => array(
				'ID' => array(
					'nice_name'	=> 'ID',
					'var_type'	=> 'int',
					'readonly'	=> 'true'
				),
				'post_id'	=> array(
					'nice_name'	=> 'Post ID',
					'var_type'	=> 'int',
					'unique'	=> '1',
					'valid_id'	=> '1',
					'readonly'	=> 'true'
				),
				'path'		=> array(
					'nice_name'	=> 'Path',
					'readonly'	=> 'true'
				),
				// Added to Database
				'image_3_4_path'		=> array(
					'nice_name'	=> '3:4 Path',
					'readonly'	=> 'true'
				),
				// Added to Database
				'image_override_path'	=> array(
					'nice_name'	=> 'Override Path',
					'readonly'	=> 'true'
				),
				'width'					=> array(
					'nice_name'	=> 'Original Width',
					'var_type'	=> 'int',
					'readonly'	=> 'true'
				),
				'height' 				=> array(
					'nice_name'	=> 'Original Height',
					'var_type'	=> 'int',
					'readonly'	=> 'true'
				),
				// Added to Database
				'raw_edges_uncropped' 	=> array(
					'nice_name'	=> 'Raw Edges Uncropped',
					'readonly'	=> 'true'
				),
				'raw_top_white'			=> array(
					'nice_name'	=> 'Raw Top White',
					'readonly'	=> 'true'
				),
				'raw_bottom_white'		=> array(
					'nice_name'	=> 'Raw Bottom White',
					'readonly'	=> 'true'
				),
				'raw_left_white'		=> array(
					'nice_name'	=> 'Raw Left White',
					'readonly'	=> 'true'
				),
				'raw_right_white'		=> array(
					'nice_name'	=> 'Raw Right White',
					'readonly'	=> 'true'
				),
				// Added to Database
				'raw_edges_cropped'		=> array(
					'nice_name'	=> 'Raw Edges Cropped',
					'readonly'	=> 'true'
				),
				'crop_left_white' 		=> array(
					'nice_name'	=> 'Crop Left White',
					'readonly'	=> 'true'
				),
				'crop_right_white' 		=> array(
					'nice_name'	=> 'Crop Right White',
					'readonly'	=> 'true'
				),
				// Added to Database
				'status'				=> array(
					'nice_name'	=> 'Status',
					'var_type'	=> 'list:image_status'
				),
				'last_check'			=> array(
					'nice_name'	=> 'Last Analyzed',
					'var_type'	=> 'datetime',
					'readonly'	=> 'true'
				),
				'_meta_updated'			=> array(
					'nice_name'	=> 'Updated',
					'readonly'	=> 'true'
				),
				'_meta_created' 		=> array(
					'nice_name'	=> 'Created',
					'readonly'	=> 'true'
				)
			),
			'ovcdt'	=> array(
				'ovc_images'	=> array(
					// Added Field Groups
					'field_groups'			=> array(
						'essential' 		=> 'Essential',
						'images' 			=> 'Images',
						'status'			=> 'Status',
						'details'			=> 'Analysis Details',
						'image_paths'		=> 'Image Paths',
						'meta_info'			=> 'Meta Info'
					),
					'default_field_groups'	=> array( 'essential', 'images' ),
					'required_field_groups'	=> array( 'essential' ),
					'fg.images'				=> array(),
					'fg.essential'			=> array( 'pic.ID', 'pic.post_id', 'pic.status' ),
					'fg.status'				=> array(),
					'fg.details'	=> array( 'pic.width', 'pic.height', 'pic.raw_edges_uncropped', 'pic.raw_edges_cropped', 'pic.raw_top_white', 'pic.raw_right_white', 'pic.raw_bottom_white', 'pic.raw_left_white', 'pic.crop_left_white', 'pic.crop_right_white' ),
					'fg.image_paths'		=> array( 'pic.path', 'pic.image_3_4_path', 'pic.image_override_path' ),
					'fg.meta_info'			=> array( 'pic.last_check', 'pic._meta_updated', 'pic._meta_created' ),
					// 'fields'	=> array( 'pic.ID', 'pic.post_id', 'pic.raw_edges_uncropped', 'pic.raw_edges_cropped', 'pic.width', 'pic.height', 'pic.path', 'pic.image_3_4_path', 'pic.status', 'pic.last_check', 'pic._meta_updated', 'pic._meta_created' ),
					'classname'	=> 'OVC_Table_ovc_images'
				)
			),
			'ovc_list' => array(
				'image_cropping'	=> array(
					'type'			=> 'simple',
					'valid_values'	=> array()
				),
				'image_status'		=> array(
					'type'			=> 'simple',
					'valid_values'	=> array( 'Needs Review', 'Needs Override', 'Approved' )
				)
			)
		);

		OVCSC::multi_update_field_meta( $schema );

		OVCSC::delete_field_meta( 'ovcdt', 'ovc_images', 'force_readonly_fields' );
		OVCSC::delete_field_meta( 'ovcdt', 'ovc_images', 'fields' );
	}
}