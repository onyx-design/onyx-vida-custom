<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_add_tag_barcode_field_to_ovc_product {

	public function up() {

		$ovc_schema = array(
			'ovc_products'	=> array(
				'tag_barcode'	=> array(
					'maxlength'		=> '20',
					'nice_name'		=> 'Tag Barcode',
					'pattern'		=> '^[0-9]+$',
				)
			)
		);

		OVCSC::multi_update_field_meta( $ovc_schema );

		OVCSC::meta_add_array_item( 'dt.ovc_products', 'fg.alternate_ids', 'pr.tag_barcode', 'pr.upc_code' );

		OVC_Migrations::add_col_if_not_exists( 'ovc_products', 'tag_barcode', 'VARCHAR(20)', 'upc_code' );
	}
}