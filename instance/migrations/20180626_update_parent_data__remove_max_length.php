<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_update_parent_data__remove_max_length {

	public function up() {

		// Remove pa.care maxlength
		OVCSC::delete_field_meta( 'parent_data', 'care', 'maxlength' );
	}
}