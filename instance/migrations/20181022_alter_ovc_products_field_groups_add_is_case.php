<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_alter_ovc_products_field_groups_add_is_case {

	public function up() {
		global $wpdb;

		$ovc_schema = array(
			'ovcdt'		=> array(
				'ovc_products'	=> array(
					'fg.essential'	=> array(
						'pr.ID',
						'pr.sku_style',
						'pr.sku_color',
						'pr.sku_pkqty',
						'pr.size',
						'pr.sku',
						'pr.pk_str',
						'pr.parent_sku',
						'pr.ovc_status',
						'pr.status_receiving',
						'pr.status_wholesale_pricing',
						'pr.status_online_pricing',
						'pr.status_description',
						'pr.status_upc',
						'pr.oms_status',
						'pr.sync_oms',
						'pr.sync_whs3',
						'pr.sync_whs5',
						'pr.sync_wc',
						'pr.sync_walmart',
						'pr.sync_shopify',
						'pr.sync_zulily',
						'pr.sync_groupon',
						'pr.sync_amazon',
						'st.product_title',
						'pa.product_title',
					),
					'fg.details'	=> array(
						'pr.department',
						'pr.class',
						'pr.oms_img_1',
						'pr.oms_img_2',
					),
					'fg.inventory'	=> array(
						'pr.case_sku',
						'pr.is_case',
						'pr.stock',
						'pr.so_qty',
						'pr.avail_qty',
						'pr.reserve_qty',
						'pr.size_ratio',
						'pr.comp_level',
						'pr.dozens',
						'pr.unit_per_case',
						'pr.unit_per_box',
					),
					'fg.measurements'	=> array(
						'pr.unit_length',
						'pr.unit_width',
						'pr.unit_height',
						'pr.unit_vol',
						'pr.unit_weight',
						'pr.case_length',
						'pr.case_width',
						'pr.case_height',
						'pr.case_vol',
						'pr.case_weight',
					),
				)
			),
			'ovc_products'	=> array(
				'is_case'		=> array(
					'nice_name'				=> 'Is Case',
					'var_type'				=> 'bool',
					'on_duplicate_value'	=> '0'
				)
			)
		);

		OVCSC::multi_update_field_meta( $ovc_schema );

		OVC_Migrations::add_col_if_not_exists( 'ovc_products', 'is_case', 'TINYINT(1) NOT NULL DEFAULT 0', 'case_sku' );

		$wpdb->query( "UPDATE {$wpdb->prefix}ovc_products SET is_case = 1 WHERE size = 'CS' OR case_sku = sku;" );
	}
}