<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_update_ovc_images__change_permissions {

	public function up() {

		$schema = array(
			'ovc_images'	=> array(
				'crop_mode'		=> array(
					'edit_access'	=> array( 'vida_sr_data_tech' ),
				),
				'crop_focus'	=> array(
					'edit_access'	=> array( 'vida_sr_data_tech' ),
				),
			)
		);

		OVCSC::multi_update_field_meta( $schema );
	}
}