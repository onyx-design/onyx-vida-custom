<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_alter_amazon_new__add_fabric_type {

	public function up() {

		$schema = array(
			'amazon_new'	=> array(
				'fabric_type'	=> array(
					'filter_func'		=> 'filter_amazon_fabric_type'
				),
				'bullet_point5'	=> array(
					'filter_func'		=> 'filter_amazon_bullet_point_5'
				)
			),
			'style_data'	=> array(
				'amazon_fabric_type'			=> array(
					'nice_name'			=> 'Amazon Fabric Type',
					'product_type'		=> 'General'
				),
				// 'amazon_bullet_point_5'	=> array(
				// 	'nice_name'			=> 'Amazon Bullet Point 5',
				// 	'maxlength'			=> '200',
				// 	'product_type'		=> 'Bullet Points / Description'
				// )
			)
		);

		OVCSC::multi_update_field_meta( $schema );
		OVCSC::delete_field_meta( 'amazon_new', 'bullet_point5', 'ovc_field' );
	}
}