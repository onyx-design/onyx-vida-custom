<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_update_ovc_products__add_wholesale_pricing_online_pricing_upc {

	public function up() {

		$schema = array(
			'ovc_products'	=> array(
				/*
				 * Added to database
				 * After Status Description
				 *
				 * Copied data from status_pricing to status_wholesale_pricing
				 * UPDATE `wp_ovc_products` SET `status_wholesale_pricing` = 1 WHERE `status_pricing` = 1
				 */
				'status_wholesale_pricing'	=> array(
					'nice_name'				=> 'Wholesale Pricing',
					'var_type'				=> 'bool',
					'on_duplicate_value'	=> '0'
				),
				/*
				 * Added to database
				 * After Status Wholesale Pricing
				 */
				'status_online_pricing'	=> array(
					'nice_name'				=> 'Online Pricing',
					'var_type'				=> 'bool',
					'on_duplicate_value'	=> '0'
				),
				/*
				 * Added to database
				 * After Status Online Pricing
				 */
				'status_upc'	=> array(
					'nice_name'				=> 'UPC',
					'var_type'				=> 'bool',
					'on_duplicate_value'	=> '0'
				),
			),
			/*
			 * Removed Status Pricing from OVCDT
			 * Added Fields:
			 *		Status Wholesale Pricing
			 *		Status Online Pricing
			 *		Status UPC
			 */
			'ovcdt'			=> array(
				'ovc_products'	=> array(
					'fg.essential'	=> array(
						'pr.ID',
						'pr.sku_style',
						'pr.sku_color',
						'pr.sku_pkqty',
						'pr.size',
						'pr.sku',
						'pr.pk_str',
						'pr.parent_sku',
						'pr.ovc_status',
						'pr.status_receiving',
						'pr.status_wholesale_pricing',
						'pr.status_online_pricing',
						'pr.status_description',
						'pr.status_upc',
						'pr.oms_status',
						'pr.sync_oms',
						'pr.sync_whs3',
						'pr.sync_whs5',
						'pr.sync_wc',
						'pr.sync_walmart',
						'pr.sync_shopify',
						'pr.sync_zulily',
						'pr.sync_groupon',
						'pr.sync_amazon',
						'pr.sync_zulily',
						'pr.sync_groupon',
						'pr.sync_amazon',
						'st.product_title',
						'pa.product_title',
						'pr.case_sku'
					)
				)
			)
		);

		OVCSC::delete_field_meta( 'ovc_products', 'status_pricing', 'nice_name' );
		OVCSC::delete_field_meta( 'ovc_products', 'status_pricing', 'var_type' );
		OVCSC::delete_field_meta( 'ovc_products', 'status_pricing', 'on_duplicate_value' );

		// OVCSC::meta_add_array_item( 'dt.ovc_products', 'fg.essential', 'pr.status_wholesale_pricing', 'pr.status_description' );
		// OVCSC::meta_add_array_item( 'dt.ovc_products', 'fg.essential', 'pr.status_online_pricing', 'pr.status_wholesale_pricing' );
		// OVCSC::meta_add_array_item( 'dt.ovc_products', 'fg.essential', 'pr.status_upc', 'pr.status_online_pricing' );

		OVCSC::multi_update_field_meta( $schema );
	}
}