<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_update_api_feed_schema {

	public function up() {

		OVCSC::update_field_meta( 'api_feeds', 'remote_feed_id', 'valid_id', '1' );
	}
}