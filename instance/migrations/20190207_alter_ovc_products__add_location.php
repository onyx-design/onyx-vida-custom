<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_alter_ovc_products__add_location {

	public function up() {

		$schema = array(
			'ovc_products'		=> array(
				'location'			=> array(
					'nice_name'			=> 'Location',
					'maxlength'			=> 8,
				)
			),
			'oms_product_data'	=> array(
				'Location'			=> array(
					'ovc_field'			=> 'pr.location||maxlength:8'
				)
			),
			'ovcdt'				=> array(
				'ovc_products'		=> array(
					'fg.inventory'		=> array(
    					'pr.case_sku',
    					'pr.is_case',
    					'pr.is_raw',
    					'pr.use_potential_inventory',
    					'pr.location',
    					'pr.stock',
    					'pr.so_qty',
    					'pr.avail_qty',
    					'pr.potential_qty',
    					'pr.reserve_qty',
    					'pr.size_ratio',
    					'pr.comp_level',
    					'pr.dozens',
    					'pr.unit_per_case',
    					'pr.unit_per_box',
					)
				)
			)
		);

		OVCSC::multi_update_field_meta( $schema );

		$pr_fs = OVCSC::get_field_set( 'ovc_products' );
		OVC_Migrations::add_col_if_not_exists( $pr_fs, 'location', "VARCHAR(8) NOT NULL DEFAULT ''", 'use_potential_inventory' );
	}
}