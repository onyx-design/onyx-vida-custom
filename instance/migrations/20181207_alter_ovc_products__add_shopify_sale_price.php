<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_alter_ovc_products__add_shopify_sale_price {

	public function up() {

		$schema = array(
			'ovc_products'	=> array(
				'shopify_sale_price'	=> array(
					'nice_name'				=> 'Shopify Sale Price',
					'edit_access'			=> array( 'vida_sr_data_tech' ),
					'var_type'				=> 'price',
					'custom_meta_updated'	=> 'price'
				)
			),
			'shopify_products'	=> array(
				'shopify_compare_at_price'	=> array(
					'nice_name'				=> 'Compare At Price[Shopify]',
					'var_type'				=> 'price'
				)
			),
			'shopify_variant'	=> array(
				'price'				=> array(
					'filter_func'		=> 'filer_ovc_shopify_price'
				),
				'compare_at_price'	=> array(
					'filter_func'		=> 'filter_ovc_shopify_compare_at_price'
				),
			),
			'ovcdt'			=> array(
				'ovc_products'			=> array(
					'fg.price_cost'	=> array(
						'pr.base_cost',
						'pr.duty',
						'pr.shipping_fee',
						'pr.handling_fee',
						'pr.qtyprice1',
						'pr.qtyprice2',
						'pr.qtyprice3',
						'pr.qtyprice4',
						'pr.qtyprice5',
						'pr.qtyprice6',
						'pr.sales_cost',
						'pr.range2',
						'pr.range3',
						'pr.range4',
						'pr.range5',
						'pr.range6',
						'pr.shopify_sale_price',
						'pr.price_retail',
						'pr.price_amazon',
						'pr.price_walmart',
						'pr.price_groupon',
						'pr.price_zulily',
						'pr.price_wc',
						'pr.dropship_fee',
						'pr.price_msrp',
						'pr.price_case',
					)
				),
				'shopify_products'		=> array(
					'fields'		=> array(
						'sypr.ID',
						'sypr.variant_id',
						'sypr.shopify_parent_id',
						'sypr.ovc_id',
						'sypr.sku',
						'sypr.upc',
						'pr.sku',
						'sypr.shopify_image_id',
						'sypr.shopify_inventory_qty',
						'sypr.price_shopify',
						'sypr.shopify_compare_at_price',
						'sypr.shopify_created_at',
						'sypr.shopify_updated_at',
						'sypr.last_sync',
						'sypr.last_inventory_check',
						'sypr.last_image_sync',
						'sypr.errors',
					)
				)
			)
		);

		OVCSC::multi_update_field_meta( $schema );

		$pr_fs = OVCSC::get_field_set( 'ovc_products' );
		OVC_Migrations::add_col_if_not_exists( $pr_fs, 'shopify_sale_price', 'DECIMAL(8,2) NOT NULL', 'range6' );

		$sypr_fs = OVCSC::get_field_set( 'shopify_products' );
		OVC_Migrations::add_col_if_not_exists( $sypr_fs, 'shopify_compare_at_price', 'DECIMAL(8,2) NOT NULL', 'price_shopify' );

		OVCSC::delete_field_meta( 'shopify_variant', 'price', 'ovc_field' );
	}
}