<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_add_data_fixes_schema {

	public function up() {

		// Unrelated fix of walmart_feed_item_errors joins
		OVCSC::update_field_meta( 'field_set_meta', 'walmart_feed_item_errors', 'tbl_refs_parents', array( 'walmart_error_codes', 'walmart_product', 'api_feeds' ) );


		// Setup Error Codes
		$this->add_ovc_error_codes_schema();

		$this->add_ovc_error_codes_db();

		$this->add_initial_error_codes();


		// Setup Data Errors
		$this->add_ovc_data_errors_schema();

		$this->add_ovc_data_errors_db();


	}



	public function add_ovc_error_codes_schema() {

		$schema = array(
			'field_set_meta'	=> array(
				'error_codes'		=> array(
					'nice_name'		=> 'OVC Error Codes',
					'sql_alias'		=> 'errcd',
					'table_name'	=> 'wp_ovc_error_codes',
					'type'			=> 'database',
					'delete_access'	=> array('administrator'),
					'tbl_refs_children'	=> array('data_errors')
				)
			),
			'error_codes'	=> array(
				'ID'	=> array(
					'var_type'	=> 'int'
				),
				'error_code'	=> array(
					'nice_name'	=> 'Error Code',
					'maxlength'	=> '48',
					'unique'	=> '1',
					'valid_id'	=> '1',
					'required'	=> 'required'
				),
				'group'			=> array(
					'nice_name'	=> 'Error Group'
				),
				'name'			=> array(
					'nice_name'	=> 'Error Name'
				),
				'single_resource'			=> array(
					'nice_name'	=> 'Single Resource?',
					'var_type'	=> 'bool'
				),
				'severe'			=> array(
					'nice_name'	=> 'Severe',
					'var_type'	=> 'bool'
				),
				'actions_needed'			=> array(
					'nice_name'	=> 'Actions Needed'
				),
				'actions_prevented'			=> array(
					'nice_name'	=> 'Actions Prevented'
				),
				'description'	=> array(
					'nice_name'	=> 'Description'
				),
				'meta_created'	=> array(
					'nice_name'	=> 'Date Created',
					'var_type'	=> 'datetime'
				)
			),
			'ovcdt'	=> array(
				'error_codes'	=> array(
					'fields'	=> array(
						'errcd.ID', 
						'errcd.error_code',
						'errcd.description',
						'errcd.name',
						'errcd.group',
						'errcd.single_resource',
						'errcd.severe',
						'errcd.actions_needed',
						'errcd.actions_prevented',
						'errcd.meta_created'
					),
					'force_readonly_fields'	=> 'true'
				)
			)
		);

		OVCSC::multi_update_field_meta( $schema );

		
	}

	public function add_ovc_error_codes_db() {

		$errcd_fs = OVCSC::get_field_set( 'errcd' );

		OVC_Migrations::add_table_if_not_exists( $errcd_fs );

		$cols = array(
			'error_code' 		=> 'VARCHAR(48)',
			'group' 			=> 'VARCHAR(24)',
			'name' 				=> 'VARCHAR(48)',
			'single_resource' 	=> "TINYINT(1) NOT NULL DEFAULT '0'",
			'severe' 			=> "TINYINT(1) NOT NULL DEFAULT '0'",
			'actions_needed' 	=> 'VARCHAR(128)',
			'actions_prevented' => 'VARCHAR(128)',
			'description' 		=> 'VARCHAR(256)',
			'meta_created' 		=> 'DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP'
		);

		$after = 'ID';

		foreach ($cols as $col_name => $col_def ) {

			OVC_Migrations::add_col_if_not_exists( $errcd_fs, $col_name, $col_def, $after );

			$after = $col_name;
		}
	}

	public function add_initial_error_codes() {

		$error_codes_fields = array(
			'error_code',
			'group',
			'name',
			'single_resource',
			'severe',
			'actions_needed',
			'actions_prevented',
			'description'
		);

		$error_codes = array(
			array(
				'oms__missing_ovc_ids',
				'oms',
				'OMS SKUs Missing OVC IDs',
				'0',
				'1',
				'manual',
				'',
				'The following SKUs exist in OVC (pr.sku or pr.oms_sku_confirmed) but do not have OVC IDs in the Inventory CSV! '
			),
			array(
				'oms__invalid_ovc_ids',
				'oms',
				'OMS Invalid OVC IDs',
				'0',
				'1',
				'manual',
				'',
				'The following OVC IDs were found in the OMS Inventory import file but do not exist in OVC!',
			),
			array(
				'oms__duplicate_ovc_ids',
				'oms',
				'OMS Duplicate OVC IDs',
				'0',
				'1',
				'manual',
				'',
				'Duplicate OVC IDs in OMS! Please review carefully and delete or fix SKUs in OMS.'
			),
			array(
				'oms__global_change_needed',
				'oms',
				'OMS Global Change Needed',
				'1',
				'1',
				'global_change',
				'',
				'Global change was not processed correctly'
			),
			array(
				'oms__ovc_sku_mismatch',
				'oms',
				'OMS OVC SKU Mismatch',
				'1',
				'1',
				'global_change',
				'',
				'OMS SKU does not match OVC SKU'
			),
			array(
				'oms__export_id_mismatch',
				'oms',
				'OMS Export ID Mismatch',
				'1',
				'0',
				'oms_export',
				'',
				'OMS data last export ID does not match OVC OMS Export Unconfirmed ID'
			),
			array(
				'sync_oms_required_fields',
				'ovc_products',
				'Sync OMS required fields invalid',
				'1',
				'0',
				'manual',
				'sync_oms',
				'OMS sync invalid required fields: pr.price_retail, pr.qtyprice1, pr.price_case, pr.price_msrp, pa.product_title, st.product_title, pa.brand, st.origin_country'
			),
			array(
				'sync_walmart_required_fields',
				'ovc_products',
				'Sync Walmart required fields invalid',
				'1',
				'1',
				'manual',
				'sync_walmart',
				'Walmart sync invalid required fields: pr.op_oms_export_confirmed, pr.price_walmart, pr.department, pr.upc_code, img.img_main, st.fast_facts1, st.fast_facts2, st.description'
			),
			array(
				'sync_wc_required_fields',
				'ovc_products',
				'Sync WC required fields invalid',
				'1',
				'0',
				'manual',
				'sync_wc',
				'Sync WC invalid required fields: pr.op_oms_export_confirmed, pa.img_main, img.img_main, st.wc_product_category, pr.price_wc'
			),
			array(
				'sync_shopify_required_fields',
				'ovc_products',
				'Sync Shopify required fields invalid',
				'1',
				'0',
				'manual',
				'sync_shopify',
				'Sync Shopify invalid required fields: pr.op_oms_export_confirmed, pa.img_main, img.img_main, pa.product_title, st.wc_product_category'
			),
			array(
				'invalid_upc_code',
				'ovc_products',
				'Invalid UPC Code',
				'1',
				'0',
				'manual',
				'',
				'UPC Code does not pass checksum test'
			),
			array(
				'duplicate_upc_code',
				'ovc_products',
				'Duplicate UPC Code',
				'1',
				'0',
				'manual',
				'',
				'Duplicate UPC code'
			)
		);

		$errcd_fs = OVCSC::get_field_set( 'errcd' );

		foreach( $error_codes as $error_code ) {

			$error_code_data = array_combine( $error_codes_fields, $error_code );

			OVCDB()->new_row( $errcd_fs, $error_code_data )->save();
		}
	}

	public function add_ovc_data_errors_schema() {
		$schema = array(
			'field_set_meta'	=> array(
				'data_errors'		=> array(
					'nice_name'		=> 'OVC Data Errors',
					'sql_alias'		=> 'err',
					'table_name'	=> 'wp_ovc_data_errors',
					'type'			=> 'database',
					'delete_access'	=> array('administrator'),
					'tbl_refs_parents'	=> array('error_codes','ovc_products'),
					'default_get_children_filter'	=> '::[err.active]'
				),
				'ovc_products'	=> array(
					'tbl_refs_children'	=> array(
						'shopify_products',
						'walmart_product',
						'data_errors'
					)
				)
			),
			'data_errors' 	=> array(
				'ID'	=> array(
					'nice_name'	=> 'ID',
					'var_type'	=> 'int'
				),
				'errcd_id'	=> array(
					'nice_name'		=> 'Error Code ID',
					'foreign_key'	=> 'errcd.ID',
					'var_type'		=> 'int'
				),
				'ovc_id'	=> array(
					'nice_name'		=> 'OVC ID',
					'foreign_key'	=> 'pr.ID',
					'var_type'		=> 'int'
				),
				'error_data'	=> array(
					'nice_name'		=> 'Error Data'
				),
				'active'		=> array(
					'nice_name'		=> 'Active',
					'var_type'		=> 'bool',
					'readonly'		=> 'true'
				),
				'date_resolved'	=> array(
					'nice_name'		=> 'Date Resolved',
					'var_type'		=> 'datetime',
					'readonly'		=> 'true'
				),
				'_meta_updated'	=> array(
					'nice_name'		=> 'Date Updated',
					'var_type'		=> 'datetime',
					'readonly'		=> 'true'
				),
				'_meta_created'	=> array(
					'nice_name'		=> 'Date Created',
					'var_type'		=> 'datetime',
					'readonly'		=> 'true'
				)
			),
			'ovcdt'	=> array(
				'data_errors'	=> array(
					'fields'	=> array(
						'err.ID', 
						'errcd.error_code',
						'err.ovc_id',
						'err.active',
						'err.error_data',
						'errcd.severe',
						'errcd.actions_needed',
						'errcd.actions_prevented',
						'errcd.name',
						'errcd.description',
						'err.date_resolved',
						'err._meta_updated',
						'err._meta_created'
					),
					'classname'	=> 'OVC_Table_data_errors',
					'default_sorting'	=> 'err.ID DESC',
					'force_readonly_fields'	=> 'true'
				)
			)
		);

		OVCSC::multi_update_field_meta( $schema );

		

	}

	public function add_ovc_data_errors_db() {

		$err_fs = OVCSC::get_field_set( 'err' );

		OVC_Migrations::add_table_if_not_exists( $err_fs );

		$cols = array(
			'errcd_id'		=> 'INT UNSIGNED NOT NULL',
			'ovc_id'		=> 'INT UNSIGNED NOT NULL',
			'error_data'	=> 'VARCHAR(4096)',
			'active'		=> "TINYINT(1) NOT NULL DEFAULT '1'",
			'date_resolved'	=> 'DATETIME NOT NULL',
			'_meta_updated'	=> 'DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
			'_meta_created'	=> 'DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP'
		);

		$after = 'ID';

		foreach ($cols as $col_name => $col_def ) {

			OVC_Migrations::add_col_if_not_exists( $err_fs, $col_name, $col_def, $after );

			$after = $col_name;
		}
	}
}