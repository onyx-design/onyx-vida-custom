<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_alter_style_data__remove_panties_technical_values {

	public function up() {
		global $wpdb;

		$schema = array(
			'ovc_list'	=> array(
				'zulily_care_instructions'	=> array(
					'valid_values'		=> array(
						'Machine wash',
						'Machine wash; tumble dry',
						'Machine wash; hang dry',
						'Machine wash; dry flat',
						'Hand wash',
						'Hand wash; hang dry',
						'Hand wash; dry flat',
						'Hand wash; tumble dry',
						'Spot clean',
					)
				)
			),
		);

		OVCSC::multi_update_field_meta( $schema );

		$removed_technical_features = array(
			'Dry clean',
			'Dishwasher-safe',
			'Wipe clean',
			'Rinse clean',
			'Shake clean',
			'Vacuum clean',
			'Professionally clean'
		);
		$wpdb->query( "UPDATE {$wpdb->prefix}ovc_style_data SET panties_technicalfeatures = '' WHERE panties_technicalfeatures IN (" . '"' . implode( '", "', $removed_technical_features ) . '"' . ")" );
	}
}