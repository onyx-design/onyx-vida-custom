<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_alter_amazon_style_data__change_nice_names {

	public function up() {

		$schema = array(
			'style_data'	=> array(
				'amazon_fabric_wash'			=> array(
					'nice_name'			=> 'Fabric Wash (Override)',
				),
				'amazon_pattern_type'			=> array(
					'nice_name'			=> 'Pattern Type (Override)',
				),
				'amazon_color_map'				=> array(
					'nice_name'			=> 'Color Map (Override)',
				),
				'amazon_size_map'				=> array(
					'nice_name'			=> 'Size Map (Override)',
				),
				'amazon_size_name'				=> array(
					'nice_name'			=> 'Size Name (Override)',
				),

				'amazon_style_name'				=> array(
					'nice_name'			=> 'Style Name (Override)',
				),
				'amazon_strap_type'				=> array(
					'nice_name'			=> 'Strap Type (Override)',
				),
				'amazon_top_style'				=> array(
					'nice_name'			=> 'Top Style (Override)',
				),
				'amazon_underwire_type'			=> array(
					'nice_name'			=> 'Underwire Type (Override)',
				),

				'amazon_bullet_point_1'			=> array(
					'nice_name'			=> 'Bullet Point 1 (Override)',
				),
				'amazon_bullet_point_2'			=> array(
					'nice_name'			=> 'Bullet Point 2 (Override)',
				),
				'amazon_bullet_point_3'			=> array(
					'nice_name'			=> 'Bullet Point 3 (Override)',
				),
				'amazon_bullet_point_4'			=> array(
					'nice_name'			=> 'Bullet Point 4 (Override)',
				),
				'amazon_product_description'	=> array(
					'nice_name'			=> 'Product Description (Override)',
				),
			)
		);

		OVCSC::multi_update_field_meta( $schema );
		OVCSC::delete_field_meta( 'style_data', 'amazon_fabric_type', 'nice_name' );
		OVCSC::delete_field_meta( 'style_data', 'amazon_fabric_type', 'product_type' );
	}
}