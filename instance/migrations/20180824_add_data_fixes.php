<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_add_data_fixes {

	public function up() {

		// Remove Old Data Fix Fields
		// OVCSC::delete_field_meta( 'data_fixes', 'fields_to_fix', 'nice_name' );
		// OVCSC::delete_field_meta( 'data_fixes', 'fields_to_fix', 'readonly' );
		// OVCSC::delete_field_meta( 'data_fixes', 'unsynced', 'nice_name' );
		// OVCSC::delete_field_meta( 'data_fixes', 'unsynced', 'readonly' );
		// OVC_Migrations::drop_col_if_exists( 'data_fixes', 'fields_to_fix' );
		// OVC_Migrations::drop_col_if_exists( 'data_fixes', 'unsynced' );

		// Add new Data Fix Fields
		// OVC_Migrations::add_col_if_not_exists( 'data_fixes', 'error_id', 'INT', 'ovc_id' );
		// OVC_Migrations::add_col_if_not_exists( 'data_fixes', 'error_data', 'TEXT', 'error_id' );
		// OVC_Migrations::add_col_if_not_exists( 'data_fixes', '_meta_checked', 'DATETIME', 'error_data' );
		// OVC_Migrations::add_col_if_not_exists( 'data_fixes', '_meta_updated', 'DATETIME', '_meta_checked' );
		// OVC_Migrations::add_col_if_not_exists( 'data_fixes', '_meta_created', 'DATETIME', '_meta_updated' );

		$ovc_schema = array(
			'data_fixes' => array(
			// 	'ID' 			=> array(
			// 		'nice_name'					=> 'ID',
			// 		'var_type'					=> 'int',
			// 		'readonly'					=> 'true'
			// 	),
			// 	'ovc_id'		=> array(
			// 		'nice_name'					=> 'OVC ID',
			// 		'var_type'					=> 'int',
			// 		'readonly'					=> 'true',
			// 		'unique'					=> '1',
			// 		'valid_id'					=> '1',
			// 		'foreign_key'				=> 'pr.ID',
			// 		'foreign_key_cardinality'	=> '11'
			// 	),
				'error_id'		=> array(
					'nice_name'		=> 'Error ID',
					'var_type'		=> 'int',
					'readonly'		=> 'true'
				),
				'error_data'	=> array(
					'nice_name'		=> 'Error Data'
				),
				'_meta_checked'	=> array(
					'var_type'		=> 'datetime',
					'nice_name'		=> 'Last Checked',
					'readonly'		=> 'true'
				),
				'_meta_updated'	=> array(
					'var_type'		=> 'datetime',
					'nice_name'		=> 'Last Updated',
					'readonly'		=> 'true'
				),
				'_meta_created'	=> array(
					'var_type'		=> 'datetime',
					'nice_name'		=> 'Created',
					'readonly'		=> 'true'
				)
			),
			'error_codes' => array(
				'ID' 				=> array(
					'var_type'			=> 'int',
					'nice_name'			=> 'Error ID'
				),
				'code'				=> array(
					'unique'			=> '1',
					'nice_name'			=> 'Error Code',
					'required'			=> 'required'
				),
				'name'				=> array(
					'nice_name'			=> 'Error Name',
					'maxlength'			=> '60'
				),
				'description'		=> array(
					'nice_name'			=> 'Error Description',
					'maxlength'			=> '2000'
				),
				'action_check'		=> array(
					'nice_name'			=> 'Error Check'
				),
				'action_resolved'	=> array(
					'nice_name'			=> 'Error Resolved'
				),
				'_meta_updated'		=> array(
					'var_type'		=> 'datetime',
					'nice_name'		=> 'Last Updated',
					'readonly'		=> 'true'
				),
				'_meta_created'		=> array(
					'var_type'		=> 'datetime',
					'nice_name'		=> 'Created',
					'readonly'		=> 'true'
				)
			),
			'ovcdt'		=> array(
				'data_fixes'	=> array(
					'fields'				=> array(
						'fix.ID',
						'fix.ovc_id',
						'pr.sku',
						'pr.sku_style',
						'pr.parent_sku',
						'fix._meta_checked',
						'fix._meta_updated',
						'fix._meta_created'
					)
				)
			),
			'field_set_meta'	=> array(
				'data_fixes'		=> array(
					'join.wp_ovc_error_codes'	=> array(
						'fix.error_id'				=> 'err.ID'
					),
					'tbl_refs_parents'	=> array(
						'ovc_products',
						'error_codes'
					)
				),
				'error_codes'	=> array(
					'type'				=> 'database',
					'sql_alias'			=> 'err',
					'table_name'		=> 'wp_ovc_error_codes',
					'nice_name'			=> 'OVC Error Codes',
					'tbl_refs_children'	=> array(
						'data_fixes'
					)
				)
			)
		);

		// OVCSC::multi_update_field_meta( $ovc_schema );

		// Run after we add the field_set
		// OVC_Migrations::add_table_if_not_exists( 'error_codes' );
		// OVC_Migrations::add_col_if_not_exists( 'error_codes', 'code', 'VARCHAR(128)', 'ID' );
		// OVC_Migrations::add_col_if_not_exists( 'error_codes', 'name', 'VARCHAR(64)', 'code' );
		// OVC_Migrations::add_col_if_not_exists( 'error_codes', 'description', 'VARCHAR(2000)', 'name' );
		// OVC_Migrations::add_col_if_not_exists( 'error_codes', 'action_check', 'VARCHAR(256)', 'description' );
		// OVC_Migrations::add_col_if_not_exists( 'error_codes', 'action_resolved', 'VARCHAR(256)', 'action_check' );
		// OVC_Migrations::add_col_if_not_exists( 'error_codes', '_meta_updated', 'DATETIME', 'action_resolved' );
		// OVC_Migrations::add_col_if_not_exists( 'error_codes', '_meta_created', 'DATETIME', '_meta_updated' );
	}
}