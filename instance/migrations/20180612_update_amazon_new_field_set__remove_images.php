<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_update_amazon_new_field_set__remove_images {

	public function up() {

		// Remove Image Fields
		OVCSC::delete_field_meta( 'amazon_new', 'main_image_url', 'filter_func' );
		OVCSC::delete_field_meta( 'amazon_new', 'other_image_url1', 'filter_func' );
		OVCSC::delete_field_meta( 'amazon_new', 'other_image_url2', 'filter_func' );
		OVCSC::delete_field_meta( 'amazon_new', 'other_image_url3', 'filter_func' );
		OVCSC::delete_field_meta( 'amazon_new', 'other_image_url4', 'filter_func' );
		OVCSC::delete_field_meta( 'amazon_new', 'other_image_url5', 'filter_func' );
		OVCSC::delete_field_meta( 'amazon_new', 'other_image_url6', 'filter_func' );
		OVCSC::delete_field_meta( 'amazon_new', 'swatch_image_url', 'filter_func' );

		// Remove Size Map and use Filter Func
		OVCSC::delete_field_meta( 'amazon_new', 'size_name', 'ovc_field' );

		// Remove Color Map and use Filter Func
		OVCSC::delete_field_meta( 'amazon_new', 'color_map', 'ovc_field' );

		// Remove Generic Keywords and use Filter Func
		OVCSC::delete_field_meta( 'amazon_new', 'generic_keywords', 'ovc_field' );

		$schema = array(
			'style_data'	=> array(
				'amazon_size_name'	=> array(
					'nice_name'		=> 'Amazon Size Name',
					'product_type'	=> 'General',
					'maxlength'		=> '20'
				),
				'amazon_pattern_type'	=> array(
					'var_type'		=> 'list:amazon_pattern_type',
					'nice_name'		=> 'Amazon Pattern Type',
					'product_type'	=> 'General'
				),
			),
			'amazon_new'	=> array(
				'department_name'	=> array(
					'filter_func'	=> 'filter_department_name_amazon',
				),
				'generic_keywords'	=> array(
					'filter_func'	=> 'filter_keywords_amazon',
				),
				'size_name'	=> array(
					'filter_func'	=> 'filter_size_name_amazon',
				),
				'color_map'	=> array(
					'filter_func'	=> 'filter_color_map_amazon',
				),
			)
		);

		OVCSC::multi_update_field_meta( $schema );
	}
}