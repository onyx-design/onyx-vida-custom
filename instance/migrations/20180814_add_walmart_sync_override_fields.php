<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_add_walmart_sync_override_fields {

	public function up() {

		OVC_Migrations::add_col_if_not_exists( 'walmart_product', 'manual_action', 'VARCHAR(42)', 'sync_status' );
		OVC_Migrations::add_col_if_not_exists( 'walmart_product', 'override_sku', 'VARCHAR(42)', 'manual_action' );
		OVC_Migrations::add_col_if_not_exists( 'walmart_product', 'override_upc', 'VARCHAR(42)', 'override_sku' );
		OVC_Migrations::add_col_if_not_exists( 'walmart_product', 'ovc_sync_notes', 'VARCHAR(128)', 'override_upc' );

		$ovc_schema = array(
			'ovc_list'			=> array(
				'walmart_manual_syncs'	=> array(
					'type'			=> 'simple',
					'valid_values'	=> array('PAUSE_SYNC','UPDATE_SKU','UPDATE_SKU_CONFIRM','UPDATE_UPC','UPDATE_UPC_CONFIRM','REPLACE_ALL_DATA'),
					'allow_blank'	=> 'true'
				)
			),
			'walmart_product'	=> array(
				'manual_action'	=> array(
					'nice_name'		=> 'Manual Sync Action',
					'var_type'		=> 'list:walmart_manual_syncs',
					'edit_access'	=> array('vida_sr_data_tech'),
					'override_readonly' => 'true'
				),
				'override_sku'	=> array(
					'nice_name'		=> 'Override SKU',
					'edit_access'	=> array('vida_sr_data_tech'),
					'override_readonly' => 'true'
				),
				'override_upc'	=> array(
					'nice_name'		=> 'Override UPC',
					'edit_access'	=> array('vida_sr_data_tech'),
					'override_readonly' => 'true'
				),
				'ovc_sync_notes'	=> array(
					'nice_name'		=> 'OVC Sync Notes',
					'edit_access'	=> array('vida_sr_data_tech'),
					'override_readonly' => 'true'
				)
			)
		);

		OVCSC::multi_update_field_meta( $ovc_schema );

		$wa_fg_sync = array(
			'wa.sync_status',
			'wa.manual_action',
			'wa.feed_item_status',
			'wa.product_status',
			'wa.lifecycle_status',
			'wa.status_change_reason',
			'wa.override_sku',
			'wa.override_upc',
			'wa.ovc_sync_notes',
			'wa.last_item_check',
			'wa.last_item_feed_id',
			'wa.last_price_feed_id',
			'wa.last_inventory_feed_id'
		);

		OVCSC::update_field_meta( 'ovcdt', 'walmart_product', 'fg.sync', $wa_fg_sync );

	}
}