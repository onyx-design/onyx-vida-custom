<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_ovc_schema_cleanup {

	public function up() {

		global $wpdb;

		$wpdb->query( "DELETE FROM {$wpdb->prefix}ovc_schema WHERE field_set = 'productmeta'" );

		OVCSC::delete_field_meta( 'style_data', 'oms_release_code', 'after_update' );

		OVCSC::delete_field_meta( 'field_set_meta', 'ovc_products', 'validation_override_func' );

					
	}
}