<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_alter_style_data__add_additional_sizing_guides {

	public function up() {

		$sd_fs = OVCSC::get_field_set( 'style_data' );

		$schema = array(
			'style_data'	=> array(
				'additional_sizing_guides'	=> array(
					'nice_name'		=> 'Additional Sizing Guides'
				)
			),
		);

		OVCSC::multi_update_field_meta( $schema );

		OVC_Migrations::add_col_if_not_exists( $sd_fs, 'additional_sizing_guides', "TEXT DEFAULT ''", 'sizing' );
	}
}