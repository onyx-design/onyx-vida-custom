<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_alter_ovc_products__add_use_potential_qty {

	public function up() {

		$pr_fs = OVCSC::get_field_set( 'ovc_products' );

		$schema = array(
			'ovc_products'	=> array(
				'use_potential_inventory'	=> array(
					'nice_name'		=> 'Use Potential Inventory',
					'var_type'		=> 'bool',
					'edit_access'	=> array(
						'vida_sr_data_tech'
					)
				)
			),
			'ovcdt'			=> array(
				'ovc_products'				=> array(
					'fg.inventory'	=> array(
    					'pr.case_sku',
    					'pr.is_case',
    					'pr.is_raw',
    					'pr.use_potential_inventory',
    					'pr.stock',
    					'pr.so_qty',
    					'pr.avail_qty',
    					'pr.potential_qty',
    					'pr.reserve_qty',
    					'pr.size_ratio',
    					'pr.comp_level',
    					'pr.dozens',
    					'pr.unit_per_case',
    					'pr.unit_per_box',
					)
				)
			)
		);

		OVCSC::multi_update_field_meta( $schema );

		OVC_Migrations::add_col_if_not_exists( $pr_fs, 'use_potential_inventory', 'TINYINT(1) NOT NULL DEFAULT 0', 'is_raw' );
	}
}