<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_advanced_inventory__potential_qty__primary_case {

	public function up() {

		$this->change_potential_quantity();

		$this->add_primary_case();
	}

	/**
	 * Change to make Potential Quantity nullable and default to null
	 * 
	 * This allows us to check if a Potential Quantity is null and use the old inventory system,
	 * and if a validation fails in the new inventory system, we can set Potential quantity
	 * to null to fallback to the old inventory system
	 */
	private function change_potential_quantity() {

		$pr_fs = OVCSC::get_field_set( 'ovc_products' );

		$schema = array(
			'ovc_products'	=> array(
				'potential_qty'		=> array(
					'on_duplicate_value'	=> null
				)
			)
		);

		OVCSC::multi_update_field_meta( $schema );

		// Change Potential Quantity to allow null with a default of null, and move it to be after Reserve Quantity
		OVC_Migrations::update_col( $pr_fs, 'potential_qty', 'MEDIUMINT(9) DEFAULT NULL', 'reserve_qty' );
	}

	private function add_primary_case() {

		$asm_fs = OVCSC::get_field_set( 'ovc_assemblies' );

		$schema = array(
			'ovc_assemblies'	=> array(
				'primary_case'		=> array(
					'nice_name'			=> 'Primary Case',
					'var_type'			=> 'bool'
				)
			)
		);

		OVCSC::multi_update_field_meta( $schema );

		OVC_Migrations::add_col_if_not_exists( $asm_fs, 'primary_case', 'TINYINT(1) NOT NULL DEFAULT 0', 'total_quantity' );
	}
}