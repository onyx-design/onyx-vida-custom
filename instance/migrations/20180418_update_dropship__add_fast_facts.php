<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_update_dropship_add_fast_facts {

	public function up() {

		$schema = array(
			'dropship'	=> array(
				'Fast Facts 3'	=> array(
					'order'		=> '17',
					'ovc_field'	=> 'st.fast_facts3'
				),
				'Material Content'	=> array(
					'order'		=> '18'
				),
				'Sizing Guide'		=> array(
					'order'		=> '19'
				),
				'Sizing Guide'		=> array(
					'order'		=> '19'
				),
				'Care Instructions'	=> array(
					'order'		=> '20'
				),
				'Product Description'	=> array(
					'order'		=> '21'
				),
			),
		);

		OVCSC::multi_update_field_meta( $schema );
	}
}