<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_alter_ovcop_files_add_archived {

	public function up() {
		global $wpdb;

		$ovc_schema = array(
			'ovcop_files'	=> array(
				'archived'		=> array(
					'nice_name'		=> 'Archived',
					'var_type'		=> 'bool',
					'readonly'		=> 'true'
				)
			),
			'ovcdt'		=> array(
				'ovcop_files'		=> array(
					'fields'		=> array(
						'file.ID',
						'file.file_type',
						'file.op_id',
						'file.file_name',
						'file.file_size',
						'file.archived',
						'file._meta_completed',
						'file._meta_created',
					)
				)
			)
		);

		OVCSC::multi_update_field_meta( $ovc_schema );

		OVC_Migrations::add_col_if_not_exists( 'ovcop_files', 'archived', 'TINYINT(1) NOT NULL DEFAULT 0', 'file_size' );
	}
}