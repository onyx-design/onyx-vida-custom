<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_alter_shopify_variant__add_inventory_quantity {

	public function up() {

		$schema = array(
			'shopify_variant'	=> array(
				'inventory_quantity'	=> array(
					'filter_func'		=> 'filter_shopify_avail_qty',
				)
			),
		);

		OVCSC::multi_update_field_meta( $schema );
	}
}