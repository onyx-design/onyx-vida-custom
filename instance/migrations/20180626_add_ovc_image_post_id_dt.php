<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_add_ovc_image_post_id_dt {

	public function up() {

		$schema = array(
			'field_set_meta'	=> array(
				'ovc_image_set_post_ids'	=> array(
					'table_name'	=> 'wp_ovc_image_set_post_ids',
					'nice_name'		=> 'OVC Image Set Post IDs',
					'sql_alias'		=> 'imgs',
					'type'			=> 'database',
					'delete_access'	=> array()
				)
			),
			'ovc_image_set_post_ids'	=> array(
				'ID'			=> array(
					'var_type'	=> 'int',
					'nice_name'	=> 'ID'
				),
				'object_id'		=> array(
					'var_type'	=> 'int',
					'nice_name'	=> 'Object ID'
				),
				'object_type'	=> array(
					'nice_name'	=> 'Object Type'
				),
				'post_id'		=> array(
					'var_type'	=> 'int',
					'nice_name'	=> 'Post ID'
				)
			),
			'ovcdt'	=> array(
				'ovc_image_set_post_ids'	=> array(
					'force_readonly_fields'	=> 'true',
					'fields'	=> array( 
						'imgs.ID',
						'imgs.object_id',
						'imgs.object_type',
						'imgs.post_id'
					),
					'classname'	=> 'OVC_Table_ovc_image_set_post_ids'
				)
			),
		);

		OVCSC::multi_update_field_meta( $schema );
	}
}