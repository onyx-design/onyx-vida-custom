<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_add_walmart_sku_required {

	public function up() {

		$ovc_schema = array(
			'walmart_product'	=> array(
				'sku'	=> array(
					'required'	=> 'required'
				)
			)
		);

		OVCSC::multi_update_field_meta( $ovc_schema );
	}
}