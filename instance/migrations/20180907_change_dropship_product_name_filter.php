<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_change_dropship_product_name_filter {

	public function up() {

		$ovc_schema = array(
			'dropship' 			=> array(
				'Product Title'			=> array(
					'filter_func'		=> 'filter_dropship_product_name',
				),
			)
		);

		OVCSC::multi_update_field_meta( $ovc_schema );
	}
}