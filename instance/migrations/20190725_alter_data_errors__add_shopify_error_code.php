<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_alter_data_errors__add_shopify_error_code {

	public function up() {

		self::add_shopify_max_variant_error_code();

		self::add_data_errors_reference();
	}

	public function add_shopify_max_variant_error_code() {

		$error_codes_fields = array(
			'error_code',
			'group',
			'name',
			'single_resource',
			'severe',
			'actions_needed',
			'actions_prevented',
			'description'
		);

		$error_codes = array(
			array(
				'sync_shopify_too_many_variants',
				'ovc_products',
				'Sync Shopify too many variants',
				'1',
				'0',
				'manual',
				'',
				'There is a limit of 100 variants per Shopify product.'
			)
		);

		$errcd_fs = OVCSC::get_field_set( 'errcd' );

		foreach( $error_codes as $error_code ) {

			$error_code_data = array_combine( $error_codes_fields, $error_code );

			OVCDB()->new_row( $errcd_fs, $error_code_data )->save();
		}
	}

	public function add_data_errors_reference() {

		$data_errors_reference = array(
			'field_set_meta'	=> array(
				'data_errors'		=> array(
					'tbl_refs_parents'	=> array(
						'error_codes',
						'ovc_products',
						'parent_data',
					)
				),
				'parent_data'	=> array(
					'tbl_refs_children'	=> array(
						'ovc_products',
						'shopify_parents',
						'data_errors'
					)
				)
			)
		);
		
		OVCSC::multi_update_field_meta( $data_errors_reference );
	}
}