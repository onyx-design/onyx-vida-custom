<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_update_shopify__add_inventory_variables {

	public function up() {

		$schema = array(
			'shopify_products'	=> array(
				'shopify_inventory_item_id'	=> array(
					'nice_name'	=> 'Shopify Inventory Item ID'
				)
			),
			// 'shopify_variant'	=> array(
			// 	'inventory_item_id'		=> array(
			// 		'ovc_field'	=> 'sypr.inventory_item_id'
			// 	),
			// )
		);

		OVCSC::multi_update_field_meta( $schema );
	}
}