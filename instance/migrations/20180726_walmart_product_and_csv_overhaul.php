<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_walmart_product_and_csv_overhaul {

	public function up() {

		$add_cols_walmart_product = array(
			'status_change_reason'	=> array(
				'def'	=> 'VARCHAR(512)',
				'after'	=> 'feed_item_status'
			),
			'lifecycle_status'	=> array(
				'def'	=> 'VARCHAR(12)',
				'after'	=> 'product_status'
			),
			'inventory_count'	=> array(
				'def'	=> 'INT(11)',
				'after'	=> 'price'
			),
			'item_id'	=> array(
				'def'	=> 'VARCHAR(12)',
				'after'	=> 'walmart_id'
			),
			'gtin'	=> array(
				'def'	=> 'VARCHAR(20)',
				'after'	=> 'upc'
			),
			'wa_creation_date'	=> array(
				'def'	=> 'DATE',
				'after'	=> 'last_inventory_feed_id'
			),
			'wa_last_updated'	=> array(
				'def'	=> 'DATE',
				'after'	=> 'last_inventory_feed_id'
			),
			'item_page_url'	=> array(
				'def'	=> 'VARCHAR(512)',
				'after'	=> 'last_inventory_feed_id'
			),
			'reviews_count'	=> array(
				'def'	=> 'INT(11)',
				'after'	=> 'item_page_url'
			),
			'average_rating'	=> array(
				'def'	=> 'VARCHAR(8)',
				'after'	=> 'item_page_url'
			),
			'searchable'	=> array(
				'def'	=> 'VARCHAR(4)',
				'after'	=> 'average_rating'
			)
		);

		foreach( $add_cols_walmart_product as $col_name => $col_details ) {

			OVC_Migrations::add_col_if_not_exists( 'walmart_product', $col_name, $col_details['def'], $col_details['after'] );
		}

		$schema = array(
			'walmart_product'	=> array(
				'walmart_id'	=> array(
					'unique'		=> '1',
					'valid_id'		=> '1'
				),
				'status_change_reason'	=> array(
					'nice_name'		=> 'Status Change Reason',
				),
				'lifecycle_status'	=> array(
					'nice_name'		=> 'Lifecycle Status'
				),
				'inventory_count'	=> array(
					'nice_name'		=> 'Inventory [WA]',
					'var_type'		=> 'int'
				),
				'item_id'		=> array(
					'nice_name'		=> 'Item ID'
				),
				'gtin'			=> array(
					'nice_name'	=> 'GTIN'

				),
				'wa_creation_date' => array(
					'nice_name'	=> 'Creation Date [WA]',
					'var_type'	=> 'date'
				),
				'wa_last_updated'	=> array(
					'nice_name'	=> 'Last Update [WA]',
					'var_type'	=> 'date'
				),
				'item_page_url'	=> array(
					'nice_name'	=> 'Walmart URL'
				),
				'reviews_count'	=> array(
					'nice_name'	=> 'Reviews Count',
					'var_type'	=> 'int'
				),
				'average_rating'=> array(
					'nice_name'	=> 'Average Rating'
				),
				'searchable'	=> array(
					'nice_name'	=> 'Searchable?'
				)
			),
			'field_set_meta'	=> array(
				'walmart_items_csv'	=> array(
					'nice_name'	=> 'Walmart Items Report CSV',
					'sql_alias'	=> 'waprcsv',
					'ordered'	=> '1',
					'type'		=> 'external'
				)
			),
			'walmart_items_csv'	=> array(
				'PARTNER ID' => array(
					'order'			=> '1',
					'skip_import'	=> '1'
				),
				'SKU' => array(
					'order'			=> '2',
					'ovc_field' 	=> 'wa.sku'
				),
				'PRODUCT NAME' => array(
					'order'			=> '3',
					'ovc_field' 	=> 'wa.productName'
				),
				'PRODUCT CATEGORY' => array(
					'order'			=> '4',
					'skip_import'	=> '1'
				),
				'PRICE' => array(
					'order'			=> '5',
					'ovc_field' 	=> 'wa.price'
				),
				'CURRENCY' => array(
					'order'			=> '6',
					'skip_import' 	=> '1'
				),
				'PUBLISH STATUS' => array(
					'order'			=> '7',
					'ovc_field' 	=> 'wa.product_status'
				),
				'STATUS CHANGE REASON' => array(
					'order'			=> '8',
					'ovc_field' 	=> 'wa.status_change_reason'
				),
				'LIFECYCLE STATUS' => array(
					'order'			=> '9',
					'ovc_field' 	=> 'wa.lifecycle_status'
				), //add
				'INVENTORY COUNT' => array(
					'order'			=> '10',
					'ovc_field' 	=> 'wa.inventory_count'
				), //add
				'SHIP METHODS' => array(
					'order'			=> '11',
					'skip_import' 	=> '1'
				),
				'WPID' => array(
					'order'			=> '12',
					'ovc_field' 	=> 'wa.walmart_id'
				),
				'ITEM ID' => array(
					'order'			=> '13',
					'ovc_field' 	=> 'wa.item_id'
				),//add
				'GTIN' => array(
					'order'			=> '14',
					'ovc_field' 	=> 'wa.gtin'
				),//add
				'UPC' => array(
					'order'			=> '15',
					'ovc_field' 	=> 'wa.upc'
				),
				'PRIMARY IMAGE URL' => array(
					'order'			=> '16',
					'skip_import' 	=> '1'
				),
				'SHELF NAME' => array(
					'order'			=> '17',
					'ovc_field' 	=> 'wa.productType'
				),
				'PRIMARY CAT PATH' => array(
					'order'			=> '18',
					'skip_import' 	=> '1'
				),
				'OFFER START DATE' => array(
					'order'			=> '19',
					'skip_import' 	=> '1'
				),
				'OFFER END DATE' => array(
					'order'			=> '20',
					'skip_import' 	=> '1'
				),
				'ITEM CREATION DATE' => array(
					'order'			=> '21',
					'ovc_field' 	=> 'wa.wa_creation_date'
				),//add
				'ITEM LAST UPDATED' => array(
					'order'			=> '22',
					'ovc_field' 	=> 'wa.wa_last_updated'
				),//
				'ITEM PAGE URL' => array(
					'order'			=> '23',
					'ovc_field'		=> 'wa.item_page_url'
				),//add
				'REVIEWS COUNT' => array(
					'order'			=> '24',
					'ovc_field'		=> 'wa.reviews_count'
				),
				'AVERAGE RATING' => array(
					'order'			=> '25',
					'ovc_field'		=> 'wa.average_rating'
				),
				'SEARCHABLE?' => array(
					'order'			=> '26',
					'ovc_field'		=> 'wa.searchable'
				)
			),
			'ovcdt'	=> array(
				'walmart_product'	=> array(
					'classname'		=> 'OVC_Table_walmart_products',
					'field_groups'	=> array(
						'essential'	=> 'Essential',
						'sync'		=> 'Sync Status',
						'details'=> 'Product Details',
						'ovc_data'	=> 'OVC Data',
						'marketplace' => 'Marketplace Data',
						'meta'		=> 'Meta Info'
					),
					'default_field_groups'	=> array( 'essential', 'sync' ),
					'required_field_groups'	=> array( 'essential' ),
					'fg._always'	=> array(
						'wa.item_page_url',
						'wa.sku',
						'wa.upc'
					),
					'fg.essential'	=> array(
						'wa.ID',
						'wa.ovc_id',
						'wa.sku',
						'wa.walmart_id',
						'wa.upc'
					),
					'fg.ovc_data'	=> array(
						'pr.ID',
						'pr.sku',
						'pr.upc_code',
						'pr.avail_qty',
						'pr.sync_walmart'
					),
					'fg.details'	=> array(
						'wa.productName',
						'wa.price',
						'wa.inventory_count'
					),
					'fg.sync'		=> array(
						'wa.sync_status',
						'wa.feed_item_status',
						'wa.product_status',
						'wa.lifecycle_status',
						'wa.status_change_reason',
						'wa.last_item_check',
						'wa.last_item_feed_id',
						'wa.last_price_feed_id',
						'wa.last_inventory_feed_id'
					),
					'fg.marketplace'	=> array(
						'wa.gtin',
						'wa.item_id',
						'wa.productType',
						'wa.reviews_count',
						'wa.average_rating',
						'wa.searchable',
						'wa.item_page_url'
					),
					'fg.meta'		=> array(
						'wa.wa_last_updated',
						'wa.wa_creation_date',
						'wa._meta_updated',
						'wa._meta_created'
					)
				)
			)
			
		);

	
		OVCSC::multi_update_field_meta( $schema );

		OVCSC::delete_field_meta( 'ovcdt', 'walmart_product', 'fields' );
	}
}