<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_update_shopify_parent_title__add_pack_quantity {

	public function up() {

		$schema = array(
			'shopify_parent'	=> array(
				'title'		=> array(
					'filter_func'		=> 'filter_shopify_parent_product_name',
				),
			),
		);

		OVCSC::multi_update_field_meta( $schema );
	}
}