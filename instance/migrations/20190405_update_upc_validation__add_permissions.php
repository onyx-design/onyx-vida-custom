<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_update_upc_validation__add_permissions {

	public function up() {

		$schema = array(
			'ovc_products'		=> array(
				'allow_invalid_upc'			=> array(
					'edit_access'				=> array(
						'administrator',
						'vida_sr_data_tech'
					)
				)
			),
		);

		OVCSC::multi_update_field_meta( $schema );
	}
}