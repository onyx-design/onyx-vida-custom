<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_update_upc_validation {

	public function up() {

		$schema = array(
			'ovc_products'		=> array(
				'upc_code'					=> array(
					'var_type'					=> 'string'
				),
				'allow_invalid_upc'			=> array(
					'nice_name'					=> 'Allow Invalid UPC',
					'var_type'					=> 'bool',
					'on_duplicate_value'		=> 0
				)
			),
			'ovcdt'				=> array(
				'ovc_products'				=> array(
					'fg.alternate_ids'			=> array(
						'pr.post_id',
						'pa.wc_parent_id',
						'pr.allow_invalid_upc',
						'pr.upc_code',
						'pr.tag_barcode',
						'pr.asin',
						'pr.parent_asin',
						'pr.walmart_id',
						'pr.old_sku'
					)
				)
			),
			'ovc_logic'			=> array(
				'check_allow_invalid_upc'	=> array(
					'nice_name'					=> 'Check Allow Invalid UPC',
					'pre_conditions'			=> array(
						'relation'					=> 'AND',
						'conditions'				=> array(
							array(
								'var'					=> '::[pr.allow_invalid_upc]',
								'op'					=> '=',
								'value'					=> '0'
							),
							array(
								'var'					=> '::[pr.upc_code]',
								'op'					=> '!=',
								'value'					=> ''
							),
						)
					),
					'conditions'				=> array(
						'var'					=> 'upc',
						'op'					=> 'is_valid',
						'value'					=> '::[pr.upc_code]'
					),
					// // Resolve data error if upc is valid
					// 'if_true'					=> array(
					// 	'resolve_data_error'		=> array(
					// 		'error_code'				=> 'invalid_upc_code',
					// 	)
					// ),
					// Invalidate row if condition is false
					'if_false'					=> array(
						'row_valid'					=> array(
							'value'						=> false
						),
					),
					'always_check'				=> true
				)
			),
			'field_set_meta'	=> array(
				'ovc_products'				=> array(
					'after_validate'			=> array(
						'update_sku_color_check_color_full_name',
						'cross_check_sku_color_color_long_name',
						'update_color_full_name',
						'sync_oms_required_fields',
						'sync_walmart_required_fields',
						'sync_wc_required_fields',
						'sync_shopify_required_fields',
						'update_ovc_color_name',
						'check_allow_invalid_upc',
					)
				)
			)
		);

		OVCSC::multi_update_field_meta( $schema );

		$pr_fs = OVCSC::get_field_set( 'ovc_products' );
		OVC_Migrations::add_col_if_not_exists( $pr_fs, 'allow_invalid_upc', "TINYINT NOT NULL DEFAULT 0", 'potential_qty' );
	}
}