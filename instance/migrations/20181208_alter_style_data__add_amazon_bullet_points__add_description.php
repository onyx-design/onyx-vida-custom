<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_alter_style_data__add_amazon_bullet_points__add_description {

	public function up() {

		$st_fs = OVCSC::get_field_set( 'style_data' );

		$schema = array(
			'style_data'	=> array(
				'amazon_bullet_point_1'			=> array(
					'nice_name'		=> 'Amazon Bullet Point 1',
					'maxlength'		=> '200',
					'product_type'	=> 'Bullet Points / Description'
				),
				'amazon_bullet_point_2' 		=> array(
					'nice_name'		=> 'Amazon Bullet Point 2',
					'maxlength'		=> '200',
					'product_type'	=> 'Bullet Points / Description'
				),
				'amazon_bullet_point_3'			=> array(
					'nice_name'		=> 'Amazon Bullet Point 3',
					'maxlength'		=> '200',
					'product_type'	=> 'Bullet Points / Description'
				),
				'amazon_bullet_point_4'			=> array(
					'nice_name'		=> 'Amazon Bullet Point 4',
					'maxlength'		=> '200',
					'product_type'	=> 'Bullet Points / Description'
				),
				'amazon_product_description'	=> array(
					'nice_name'		=> 'Amazon Product Description',
					'maxlength'		=> '2000',
					'product_type'	=> 'Bullet Points / Description'
				)
			),
			'amazon_new'	=> array(
				'bullet_point1'	=> array(
					'filter_func'	=> 'filter_amazon_bullet_point_1'
				),
				'bullet_point2'	=> array(
					'filter_func'	=> 'filter_amazon_bullet_point_2'
				),
				'bullet_point3'	=> array(
					'filter_func'	=> 'filter_amazon_bullet_point_3'
				),
				'bullet_point4'	=> array(
					'filter_func'	=> 'filter_amazon_bullet_point_4'
				),
				'product_description'	=> array(
					'filter_func'	=> 'filter_amazon_product_description'
				),
			)
		);

		OVCSC::multi_update_field_meta( $schema );

		$ovc_style_data_item_cols = array(
			'amazon_bullet_point_1'			=> "VARCHAR(200) NOT NULL DEFAULT ''",
			'amazon_bullet_point_2'			=> "VARCHAR(200) NOT NULL DEFAULT ''",
			'amazon_bullet_point_3'			=> "VARCHAR(200) NOT NULL DEFAULT ''",
			'amazon_bullet_point_4'			=> "VARCHAR(200) NOT NULL DEFAULT ''",
			'amazon_product_description'	=> "VARCHAR(2000) NOT NULL DEFAULT ''"
		);

		$after = 'amazon_underwire_type';

		foreach( $ovc_style_data_item_cols as $col_name => $col_def ) {

			OVC_Migrations::add_col_if_not_exists( $st_fs, $col_name, $col_def, $after );

			$after = $col_name;
		}

		OVCSC::delete_field_meta( 'amazon_new', 'bullet_point1', 'ovc_field' );
		OVCSC::delete_field_meta( 'amazon_new', 'bullet_point2', 'ovc_field' );
		OVCSC::delete_field_meta( 'amazon_new', 'bullet_point3', 'ovc_field' );
		OVCSC::delete_field_meta( 'amazon_new', 'bullet_point4', 'ovc_field' );
		OVCSC::delete_field_meta( 'amazon_new', 'product_description', 'ovc_field' );
	}
}