<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_alter_ovc_products__update_use_potential_qty_stock_update {

	public function up() {

		$pr_fs = OVCSC::get_field_set( 'ovc_products' );

		$schema = array(
			'ovc_products'	=> array(
				'use_potential_inventory'	=> array(
					'custom_meta_updated'	=> 'stock'
				)
			),
		);
		OVCSC::multi_update_field_meta( $schema );
	}
}