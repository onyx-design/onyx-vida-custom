<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_add_amazon_new_field_set {

	public function up() {

		$schema = array(
			'ovcop'	=> array(
				'amazon_export'	=> array(
					'field_set'	=> 'amazon_new'
				)
			),
			'field_set_meta'	=> array(
				'amazon_new'				=> array(
					'nice_name'			=> 'Amazon (New)',
					'ordered'			=> '1',
					'type'				=> 'external',
					'excel_worksheet'	=> 'Template',
					'start_row'			=> '4'
				)
			),
			'amazon_new'	=> array(
				'item_sku'	=> array(
					'ovc_field'	=> 'pr.sku',
				),
				'item_name'	=> array(
					'filter_func'	=> 'filter_external_product_name_with_style_size',
				),
				'external_product_id'	=> array(
					'ovc_field'	=> 'pr.upc_code',
				),
				'external_product_id_type'	=> array(
					'default_value'	=> 'UPC',
				),
				'brand_name'	=> array(
					'ovc_field'	=> 'pa.brand',
				),
				'product_description'	=> array(
					'ovc_field'	=> 'pa.description',
				),
				'item_type'	=> array(
					'filter_func'	=> 'filter_external_product_type'
				),
				'model'	=> array(
					'ovc_field'	=> 'pr.sku',
				),
				'update_delete'	=> array(
					'default_value'	=> 'Update'
				),
				'standard_price'	=> array(
					'ovc_field'	=> 'pr.price_amazon||price',
				),
				'list_price'	=> array(
				),
				'product_tax_code'	=> array(
				),
				'fulfillment_latency'	=> array(
				),
				'product_site_launch_date'	=> array(
				),
				'merchant_release_date'	=> array(
				),
				'restock_date'	=> array(
				),
				'quantity'	=> array(
				),
				'sale_price'	=> array(
				),
				'sale_from_date'	=> array(
				),
				'sale_end_date'	=> array(
				),
				'max_aggregate_ship_quantity'	=> array(
				),
				// dev:todo Something?
				'item_package_quantity'	=> array(
					// 'ovc_field'	=> '',
				),
				'number_of_items'	=> array(
				),
				'offering_can_be_gift_messaged'	=> array(
					'default_value'	=> 'False',
				),
				'offering_can_be_giftwrapped'	=> array(
					'default_value'	=> 'False',
				),
				'is_discontinued_by_manufacturer'	=> array(
					'default_value'	=> 'False',
				),
				'missing_keyset_reason'	=> array(
				),
				'merchant_shipping_group_name'	=> array(
				),
				'website_shipping_weight'	=> array(
				),
				'website_shipping_weight_unit_of_measure'	=> array(
				),
				'item_weight_unit_of_measure'	=> array(
					'default_value'		=> 'LB'
				),
				'item_weight'	=> array(
					'ovc_field'	=> 'pr.unit_weight',
				),
				'item_length_unit_of_measure'	=> array(
					'default_value'		=> 'IN'
				),
				'item_length'	=> array(
					'ovc_field'	=> 'pr.unit_length',
				),
				'item_width'	=> array(
					'ovc_field'	=> 'pr.unit_width',
				),
				'item_height'	=> array(
					'ovc_field'	=> 'pr.unit_height',
				),
				'bullet_point1'	=> array(
					'ovc_field'	=> 'st.fast_facts1',
				),
				'bullet_point2'	=> array(
					'ovc_field'	=> 'st.fast_facts2',
				),
				'bullet_point3'	=> array(
					'ovc_field'	=> 'st.fast_facts3',
				),
				'bullet_point4'	=> array(
					'filter_func'	=> 'get_material_string',
				),
				'bullet_point5'	=> array(
					'ovc_field'	=> 'st.care',
				),
				'generic_keywords'	=> array(
					'ovc_field'	=> 'st.keywords',
				),
				'main_image_url'	=> array(
					'filter_func'	=> 'filter_main_image_amazon',
				),
				'other_image_url1'	=> array(
					'filter_func'	=> 'filter_other_image_1_amazon',
				),
				'other_image_url2'	=> array(
					'filter_func'	=> 'filter_other_image_2_amazon',
				),
				'other_image_url3'	=> array(
					'filter_func'	=> 'filter_other_image_3_amazon',
				),
				'other_image_url4'	=> array(
					'filter_func'	=> 'filter_other_image_4_amazon',
				),
				'other_image_url5'	=> array(
					'filter_func'	=> 'filter_other_image_5_amazon',
				),
				'other_image_url6'	=> array(
					'filter_func'	=> 'filter_other_image_6_amazon',
				),
				'swatch_image_url'	=> array(
					'filter_func'	=> 'filter_swatch_image_amazon',
				),
				'fulfillment_center_id'	=> array(
				),
				'package_height'	=> array(
				),
				'package_width'	=> array(
				),
				'package_length'	=> array(
				),
				'package_length_unit_of_measure'	=> array(
				),
				'package_weight'	=> array(
				),
				'package_weight_unit_of_measure'	=> array(
				),
				'parent_child'	=> array(
					'default_value'	=> 'Child',
				),
				'parent_sku'	=> array(
				),
				'relationship_type'	=> array(
					'default_value'	=> 'Variation',
				),
				'variation_theme'	=> array(
					'default_value'	=> '',
				),
				'cpsia_cautionary_statement'	=> array(
				),
				'cpsia_cautionary_description'	=> array(
				),
				'fabric_type'	=> array(
					'filter_func'	=> 'filter_material_string_amazon',
				),
				'import_designation'	=> array(
				),
				'closure_type'	=> array(
				),
				'belt_style'	=> array(
				),
				'bottom_style'	=> array(
				),
				'subject_character'	=> array(
				),
				'chest_size'	=> array(
				),
				'chest_size_unit_of_measure'	=> array(
				),
				'band_size_num'	=> array(
					'filter_func'	=> 'filter_band_size',
				),
				'band_size_num_unit_of_measure'	=> array(
					'default_value'	=> 'IN',
				),
				'collar_style'	=> array(
				),
				'color_name'	=> array(
					'filter_func'	=> 'filter_color_name_amazon',
				),
				'color_map'	=> array(
					'ovc_field'	=> 'st.amazon_color_map',
				),
				'control_type'	=> array(
				),
				'cup_size'	=> array(
					'filter_func'	=> 'filter_cup_size',
				),
				// dev:todo Ask client
				'department_name'	=> array(
					// 'ovc_field'	=> 'pr.department',
				),
				'fabric_wash'	=> array(
					'ovc_field'	=> 'st.amazon_fabric_wash',
				),
				'fit_type'	=> array(
				),
				'front_style'	=> array(
				),
				'inseam_length'	=> array(
				),
				'inseam_length_unit_of_measure'	=> array(
				),
				'rise_height'	=> array(
				),
				'rise_height_unit_of_measure'	=> array(
				),
				'leg_diameter'	=> array(
				),
				'leg_diameter_unit_of_measure'	=> array(
				),
				'leg_style'	=> array(
				),
				'country_as_labeled'	=> array(
				),
				'fur_description'	=> array(
				),
				'opacity'	=> array(
				),
				'neck_size'	=> array(
				),
				'neck_size_unit_of_measure'	=> array(
				),
				'neck_style'	=> array(
				),
				'pattern_type'	=> array(
					'ovc_field'	=> 'st.amazon_pattern_type',
				),
				'pocket_description'	=> array(
				),
				'rise_style'	=> array(
				),
				'shoe_width'	=> array(
				),
				// dev:todo New Field
				'size_name'	=> array(
					'ovc_field'	=> 'pr.size',
				),
				// dev:todo Can be defined for each size, but follows a specific table, provided separately
				'size_map'	=> array(
					'ovc_field'	=> 'st.amazon_size_map',
				),
				'special_size_type'	=> array(
				),
				'sleeve_length'	=> array(
				),
				'sleeve_length_unit_of_measure'	=> array(
				),
				'sleeve_type'	=> array(
				),
				'special_features'	=> array(
				),
				'strap_type'	=> array(
					'ovc_field'	=> 'st.amazon_strap_type',
				),
				'style_name'	=> array(
					'ovc_field'	=> 'st.amazon_style_name',
				),
				'theme'	=> array(
				),
				'toe_style'	=> array(
				),
				'top_style'	=> array(
					'ovc_field'	=> 'st.amazon_top_style',
				),
				'underwire_type'	=> array(
					'ovc_field'	=> 'st.amazon_underwire_type',
				),
				'waist_size'	=> array(
				),
				'waist_size_unit_of_measure'	=> array(
				),
				'water_resistance_level'	=> array(
				),
				'sport_type'	=> array(
				),
				'wheel_type'	=> array(
				),
			)
		);

		$order = 1;
		foreach( $schema['amazon_new'] as &$amazon_field ) {

			$amazon_field['order'] = $order;
			$order++;
		}

		OVCSC::multi_update_field_meta( $schema );
	}
}