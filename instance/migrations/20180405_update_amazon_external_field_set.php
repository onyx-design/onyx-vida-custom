<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_update_amazon_external_field_set {

	public function up() {

		$schema = array(
			'field_set_meta'	=> array(
				'amazon'		=> array(
					'excel_worksheet' 	=> 'Template',
					'start_row'			=> '4',
				)
			),
			'amazon'			=> array(
				'product_tax_code'	=> array(
					'order'	=> '12'
				),
				'fulfillment_latency' => array(
					'order'	=> '13'
				),
				'product_site_launch_date'	=> array(
					'order'	=> '14'
				),
				'merchant_release_date'	=> array(
					'order'	=> '15'
				),
				'restock_date'	=> array(
					'order'	=> '16'
				),
				'quantity'	=> array(
					'order'	=> '17'
				),
				'sale_price'	=> array(
					'order'	=> '18'
				),
				'sale_from_date'	=> array(
					'order'	=> '19'
				),
				'sale_end_date'	=> array(
					'order'	=> '20'
				),
				'max_aggregate_ship_quantity'	=> array(
					'order'	=> '21'
				),
				'item_package_quantity'	=> array(
					'order'	=> '22'
				),
				'number_of_items'	=> array(
					'order'	=> '23'
				),
				'offering_can_be_gift_messaged'	=> array(
					'order'	=> '24'
				),
				'offering_can_be_giftwrapped'	=> array(
					'order'	=> '25'
				),
				'is_discontinued_by_manufacturer'	=> array(
					'order'	=> '26'
				),
				'missing_keyset_reason'	=> array(
					'order'	=> '27'
				),
				'merchant_shipping_group_name' => array(
					'order'	=> '28'
				),
				'website_shipping_weight'	=> array(
					'order'	=> '29'
				),
				'website_shipping_weight_unit_of_measure'	=> array(
					'order'	=> '30'
				),
				'item_weight_unit_of_measure'	=> array(
					'order'	=> '31'
				),
				'item_weight'	=> array(
					'order'	=> '32'
				),
				'item_length_unit_of_measure'	=> array(
					'order'	=> '33'
				),
				'item_length'	=> array(
					'order'	=> '34'
				),
				'item_width'	=> array(
					'order'	=> '35'
				),
				'item_height'	=> array(
					'order'	=> '36'
				),
				'bullet_point1'	=> array(
					'order'	=> '37'
				),
				'bullet_point2'	=> array(
					'order'	=> '38'
				),
				'bullet_point3'	=> array(
					'order'	=> '39'
				),
				'bullet_point4'	=> array(
					'order'	=> '40'
				),
				'bullet_point5'	=> array(
					'order'	=> '41'
				),
				'generic_keywords' => array(
					'order'	=> '42'
				),
				'main_image_url'	=> array(
					'order'	=> '43'
				),
				'other_image_url1'	=> array(
					'order'	=> '44'
				),
				'other_image_url2'	=> array(
					'order'	=> '45'
				),
				'other_image_url3'	=> array(
					'order'	=> '46'
				),
				'other_image_url4'	=> array(
					'order'	=> '47'
				),
				'other_image_url5'	=> array(
					'order'	=> '48'
				),
				'other_image_url6'	=> array(
					'order'	=> '49'
				),
				'swatch_image_url'	=> array(
					'order'	=> '50'
				),
				'fulfillment_center_id'	=> array(
					'order'	=> '51'
				),
				'package_height'	=> array(
					'order'	=> '52'
				),
				'package_width'	=> array(
					'order'	=> '53'
				),
				'package_length'	=> array(
					'order'	=> '54'
				),
				'package_length_unit_of_measure'	=> array(
					'order'	=> '55'
				),
				'package_weight'	=> array(
					'order'	=> '56'
				),
				'package_weight_unit_of_measure'	=> array(
					'order'	=> '57'
				),
				'parent_child'	=> array(
					'order'	=> '58'
				),
				'parent_sku'	=> array(
					'order'	=> '59'
				),
				'relationship_type'	=> array(
					'order'	=> '60'
				),
				'variation_theme'	=> array(
					'order'	=> '61'
				),
				'cpsia_cautionary_statement1' => array(
					'order'	=> '62'
				),
				'cpsia_cautionary_description'	=> array(
					'order'	=> '63'
				),
				'fabric_type' => array(
					'order'	=> '64'
				),
				'import_designation' => array(
					'order'	=> '65'
				),
				'closure_type'	=> array(
					'order'	=> '66'
				),
				'belt_style'	=> array(
					'order'	=> '67'
				),
				'bottom_style'	=> array(
					'order'	=> '68'
				),
				'subject_character'	=> array(
					'order'	=> '69'
				),
				'chest_size'	=> array(
					'order'	=> '70'
				),
				'chest_size_unit_of_measure'	=> array(
					'order'	=> '71'
				),
				'band_size_num'	=> array(
					'order'	=> '72'
				),
				'band_size_num_unit_of_measure'	=> array(
					'order'	=> '73'
				),
				'collar_style'	=> array(
					'order'	=> '74'
				),
				'color_name'	=> array(
					'order'	=> '75'
				),
				'color_map'	=> array(
					'order'	=> '76'
				),
				'control_type'	=> array(
					'order'	=> '77'
				),
				'cup_size'	=> array(
					'order'	=> '78'
				),
				'department_name'	=> array(
					'order'	=> '79'
				),
				'fabric_wash'	=> array(
					'order'	=> '80'
				),
				'fit_type'	=> array(
					'order'	=> '81'
				),
				'front_style'	=> array(
					'order'	=> '82'
				),
				'inseam_length'	=> array(
					'order'	=> '83'
				),
				'inseam_length_unit_of_measure'	=> array(
					'order'	=> '84'
				),
				'rise_height'	=> array(
					'order'	=> '85'
				),
				'rise_height_unit_of_measure'	=> array(
					'order'	=> '86'
				),
				'leg_diameter'	=> array(
					'order'	=> '87'
				),
				'leg_diameter_unit_of_measure'	=> array(
					'order'	=> '88'
				),
				'leg_style'	=> array(
					'order'	=> '89'
				),
				'country_as_labeled'	=> array(
					'order'	=> '90'
				),
				'fur_description'	=> array(
					'order'	=> '91'
				),
				'opacity'	=> array(
					'order'	=> '92'
				),
				'neck_size'	=> array(
					'order'	=> '93'
				),
				'neck_size_unit_of_measure'	=> array(
					'order'	=> '94'
				),
				'neck_style'	=> array(
					'order'	=> '95'
				),
				'pattern_type'	=> array(
					'order'	=> '96'
				),
				'pocket_description'	=> array(
					'order'	=> '97'
				),
				'rise_style'	=> array(
					'order'	=> '98'
				),
				'shoe_width'	=> array(
					'order'	=> '99'
				),
				'size_name'		=> array(
					'order'	=> '100'
				),
				'size_map'	=> array(
					'order'	=> '101'
				),
				'special_size_type'	=> array(
					'order'	=> '102'
				),
				'sleeve_length'	=> array(
					'order'	=> '103'
				),
				'sleeve_length_unit_of_measure'	=> array(
					'order'	=> '104'
				),
				'sleeve_type'	=> array(
					'order'	=> '105'
				),
				'special_features'	=> array(
					'order'	=> '106'
				),
				'strap_type'	=> array(
					'order'	=> '107'
				),
				'style_name'	=> array(
					'order'	=> '108'
				),
				'theme'	=> array(
					'order'	=> '109'
				),
				'toe_style'	=> array(
					'order'	=> '110'
				),
				'top_style'	=> array(
					'order'	=> '111'
				),
				'underwire_type'	=> array(
					'order'	=> '112'
				),
				'waist_size'	=> array(
					'order'	=> '113'
				),
				'waist_size_unit_of_measure'	=> array(
					'order'	=> '114'
				),
				'water_resistance_level'	=> array(
					'order'	=> '115'
				),
				'sport_type'	=> array(
					'order'	=> '116'
				),
				'wheel_type'	=> array(
					'order'	=> '117'
				)
			)
		);

		OVCSC::multi_update_field_meta( $schema );

		OVCSC::delete_field_meta( 'amazon', 'currency', 'default_value' );
		OVCSC::delete_field_meta( 'amazon', 'currency', 'order' );
		OVCSC::delete_field_meta( 'amazon', 'delivery_schedule_group_id', 'order' );
		OVCSC::delete_field_meta( 'amazon', 'generic_keywords1', 'order' );
		OVCSC::delete_field_meta( 'amazon', 'generic_keywords2', 'order' );
		OVCSC::delete_field_meta( 'amazon', 'generic_keywords3', 'order' );
		OVCSC::delete_field_meta( 'amazon', 'generic_keywords4', 'order' );
		OVCSC::delete_field_meta( 'amazon', 'generic_keywords5', 'order' );
		OVCSC::delete_field_meta( 'amazon', 'platinum_keywords1', 'order' );
		OVCSC::delete_field_meta( 'amazon', 'platinum_keywords2', 'order' );
		OVCSC::delete_field_meta( 'amazon', 'platinum_keywords3', 'order' );
		OVCSC::delete_field_meta( 'amazon', 'platinum_keywords4', 'order' );
		OVCSC::delete_field_meta( 'amazon', 'platinum_keywords5', 'order' );
		OVCSC::delete_field_meta( 'amazon', 'other_image_url7', 'order' );
		OVCSC::delete_field_meta( 'amazon', 'other_image_url8', 'order' );
		OVCSC::delete_field_meta( 'amazon', 'button_quantity', 'order' );
		OVCSC::delete_field_meta( 'amazon', 'cuff_type', 'order' );
		OVCSC::delete_field_meta( 'amazon', 'is_stain_resistant', 'order' );
		OVCSC::delete_field_meta( 'amazon', 'fabric_type1', 'order' );
		OVCSC::delete_field_meta( 'amazon', 'fabric_type2', 'order' );
		OVCSC::delete_field_meta( 'amazon', 'fabric_type3', 'order' );
		OVCSC::delete_field_meta( 'amazon', 'cpsia_cautionary_statement2', 'order' );
		OVCSC::delete_field_meta( 'amazon', 'cpsia_cautionary_statement3', 'order' );
		OVCSC::delete_field_meta( 'amazon', 'cpsia_cautionary_statement4', 'order' );
		OVCSC::delete_field_meta( 'amazon', 'number_of_pieces', 'order' );
		OVCSC::delete_field_meta( 'amazon', 'capacity_name', 'order' );
	}
}