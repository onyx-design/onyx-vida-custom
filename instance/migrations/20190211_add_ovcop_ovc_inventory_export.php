<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_add_ovcop_ovc_inventory_export {

	public function up() {

		$this->add_ovcop_schema();

		$this->add_ovc_inventory_export_fs_schema();
	}

	/**
	 * Setup the OVCOP for the Inventory Export
	 */
	private function add_ovcop_schema() {

		$ovcop_schema = array(
			'ovcop'		=> array(
				'ovc_inventory_export'	=> array(
					'nice_name'			=> 'OVC Inventory Export',
					'descrip'			=> 'Export OVC Inventory Data',
					'field_set'			=> 'ovc_inventory_export',
					'order'				=> '17',
					'classname'			=> 'OVCOP_ovc_inventory_export',
					'allowed_roles'		=> array( 'administrator', 'vida_sr_data_tech' ),
				)
			)
		);

		OVCSC::multi_update_field_meta( $ovcop_schema );
	}

	private function add_ovc_inventory_export_fs_schema() {

		$ovc_inventory_export_fs_schema = array(
			'field_set_meta'				=> array(
				'ovc_inventory_export'			=> array(
					'nice_name'						=> 'OVC Inventory Export',
					'ordered'						=> '1',
					'type'							=> 'external'
				)
			),
			'ovc_inventory_export'			=> array(),
			'ovc_assembly_parents_export'	=> array(),
			'ovc_assembly_items_export'		=> array()
		);

		$ovc_inventory_export_fields = array(
			'ovc_id'				=> array(
				'ovc_field'				=> 'pr.ID',
			),
			'style'					=> array(
				'ovc_field'				=> 'pr.sku_style',
			),
			'sku_color'				=> array(
				'ovc_field'				=> 'pr.sku_color',
			),
			'sku_pkqty'				=> array(
				'ovc_field'				=> 'pr.sku_pkqty',
			),
			'size'					=> array(
				'ovc_field'				=> 'pr.size',
			),
			'sku'					=> array(
				'ovc_field'				=> 'pr.sku',
			),
			'case_sku'				=> array(
				'ovc_field'				=> 'pr.case_sku',
			),
			'is_case'				=> array(
				'ovc_field'				=> 'pr.is_case',
			),
			'is_raw'				=> array(
				'ovc_field'				=> 'pr.is_raw',
			),
			'use_potential_qty'		=> array(
				'ovc_field'				=> 'pr.use_potential_qty',
			),
			'location'				=> array(
				'ovc_field'				=> 'pr.location',
			),
			'instock'				=> array(
				'ovc_field'				=> 'pr.stock',
			),
			'so_qty'				=> array(
				'ovc_field'				=> 'pr.so_qty',
			),
			'avail_qty'				=> array(
				'ovc_field'				=> 'pr.avail_qty',
			),
			'potential_qty'			=> array(
				'ovc_field'				=> 'pr.potential_qty',
			),
			'potential_qty_instock'	=> array(

			),
			'dozens'				=> array(
				'ovc_field'				=> 'pr.dozens',
			),
			'unit_per_case'			=> array(
				'ovc_field'				=> 'pr.unit_per_case',
			),
			'unit_per_box'			=> array(
				'ovc_field'				=> 'pr.unit_per_box',
			),
			'oms_sku_confirmed'		=> array(
				'ovc_field'				=> 'pr.oms_sku_confirmed',
			),
		);

		$inventory_order = 1;
		foreach( $ovc_inventory_export_fields as $ovc_inventory_export_field => $ovc_inventory_export_field_options ) {

			// Generate order so we can be lazy if we need to add more fields in the middle
			$ovc_inventory_export_field_options['order'] = $inventory_order;

			$ovc_inventory_export_fs_schema['ovc_inventory_export'][ $ovc_inventory_export_field ] = $ovc_inventory_export_field_options;

			$inventory_order++;
		}

		$ovc_inventory_assembly_parents_export_fields = array(
			'assembly_id'		=> array(
				'ovc_field'				=> 'asm.ID',
			),
			'ovc_id'				=> array(
				'ovc_field'				=> 'asm.ovc_id',
			),
			'total_quantity'		=> array(
				'ovc_field'				=> 'asm.total_quantity',
			),
			'primary_case'			=> array(
				'ovc_field'				=> 'asm.primary_case',
			),
			'_meta_updated'			=> array(
				'ovc_field'				=> 'asm._meta_updated',
			),
			'_meta_created'			=> array(
				'ovc_field'				=> 'asm._meta_created',
			),
		);

		$assembly_parents_order = 1;
		foreach( $ovc_inventory_assembly_parents_export_fields as $ovc_inventory_assembly_parents_export_field => $ovc_inventory_assembly_parents_export_field_options ) {

			// Generate order so we can be lazy if we need to add more fields in the middle
			$ovc_inventory_assembly_parents_export_field_options['order'] = $assembly_parents_order;

			$ovc_inventory_export_fs_schema['ovc_assembly_parents_export'][ $ovc_inventory_assembly_parents_export_field ] = $ovc_inventory_assembly_parents_export_field_options;

			$assembly_parents_order++;
		}

		$ovc_inventory_assembly_items_export_fields = array(
			'assembly_item_id'	=> array(
				'ovc_field'				=> 'asmi.ID',
			),
			'assembly_id'			=> array(
				'ovc_field'				=> 'asmi.assembly_id',
			),
			'ovc_id'				=> array(
				'ovc_field'				=> 'asmi.ovc_id',
			),
			'quantity'				=> array(
				'ovc_field'				=> 'asmi.quantity',
			),
			'_meta_updated'			=> array(
				'ovc_field'				=> 'asmi._meta_updated',
			),
			'_meta_created'			=> array(
				'ovc_field'				=> 'asmi._meta_created',
			),
		);

		$assembly_items_order = 1;
		foreach( $ovc_inventory_assembly_items_export_fields as $ovc_inventory_assembly_items_export_field => $ovc_inventory_assembly_items_export_field_options ) {

			// Generate order so we can be lazy if we need to add more fields in the middle
			$ovc_inventory_assembly_items_export_field_options['order'] = $assembly_items_order;

			$ovc_inventory_export_fs_schema['ovc_assembly_items_export'][ $ovc_inventory_assembly_items_export_field ] = $ovc_inventory_assembly_items_export_field_options;

			$assembly_items_order++;
		}

		OVCSC::multi_update_field_meta( $ovc_inventory_export_fs_schema );
	}
}