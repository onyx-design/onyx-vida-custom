<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_change_walmart_ovcdt_headers {

	public function up() {

		$ovc_schema = array(
			'walmart_product'	=> array(
				'sku'						=> array(
					'ovcdt_header'				=> 'SKU [Walmart]',
				),
				'upc'						=> array(
					'ovcdt_header'				=> 'UPC [Walmart]',
				),
			)
		);

		OVCSC::multi_update_field_meta( $ovc_schema );
	}
}