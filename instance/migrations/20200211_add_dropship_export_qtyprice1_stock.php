<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_add_dropship_export_qtyprice1_stock {

	public function up() {

		$schema = array( 
			'dropship'	=> array(
				'Price 1'	=> array(
					'order'	=> 22,
					'ovc_field'	=> 'pr.qtyprice1'
				),
				'In Stock'	=> array(
					'order'	=> 23,
					'ovc_field'	=> 'pr.stock'
				)
			)
		);

		OVCSC::multi_update_field_meta( $schema );
	
	}
}