<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_alter_data_errors__add_notes {

	public function up() {

		$err_fs = OVCSC::get_field_set( 'err' );

		$schema = array(
			'data_errors'	=> array(
				'ovc_id'	=> array(
					'readonly'		=> 'true'
				),
				'error_data'	=> array(
					'readonly'		=> 'true'
				),
				'notes'		=> array(
					'nice_name'	=> 'Notes',
					'edit_access'	=> array( 'administrator', 'vida_sr_data_tech', 'vida_data_tech' ),
				)
			),
			'ovcdt'	=> array(
				'data_errors'	=> array(
					'fields'	=> array(
						'err.ID', 
						'errcd.error_code',
						'err.ovc_id',
						'err.active',
						'err.error_data',
						'errcd.severe',
						'errcd.actions_needed',
						'errcd.actions_prevented',
						'err.notes',
						'errcd.name',
						'errcd.description',
						'err.date_resolved',
						'err._meta_updated',
						'err._meta_created'
					),
				)
			)
		);

		OVCSC::multi_update_field_meta( $schema );
		OVCSC::delete_field_meta( 'ovcdt', 'data_errors', 'force_readonly_fields' );

		OVC_Migrations::add_col_if_not_exists( $err_fs, 'notes', "VARCHAR(512) NOT NULL DEFAULT ''", 'active' );
	}
}