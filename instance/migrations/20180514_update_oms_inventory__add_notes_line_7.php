<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_update_oms_inventory__add_notes_line_7 {

	public function up() {

		$schema = array(
			'oms_inventory'	=> array(
				// Add to Database
				'op_oms_export_unconfirmed'	=> array(
					'order'			=> '5',
					'csv_header'	=> 'Notes Line 7',
					'ovc_field'		=> 'pr.op_oms_export_unconfirmed',
					'var_type'		=> 'int'
				)
			)
		);

		OVCSC::multi_update_field_meta( $schema );
	}
}