<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_update_shopify_variant__remove_inventory {

	public function up() {

		// Remove Inventory Fields
		OVCSC::delete_field_meta( 'shopify_variant', 'inventory_quantity', 'filter_func' );
		OVCSC::delete_field_meta( 'shopify_variant', 'old_inventory_quantity', 'ignore_if_false' );
		OVCSC::delete_field_meta( 'shopify_variant', 'old_inventory_quantity', 'ovc_field' );
	}
}