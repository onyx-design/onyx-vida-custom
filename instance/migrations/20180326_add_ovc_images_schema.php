<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_add_ovc_images_schema {

	public function up() {

		$schema = array(
			'field_set_meta' => array(
				'ovc_images'	=> array(
					'table_name'	=> 'wp_ovc_images',
					'nice_name'		=> 'OVC Images',
					'sql_alias'		=> 'pic',
					'type'			=> 'database',
					'delete_access' => array('administrator')			
				)
			),
			'ovc_images' => array(
				'ID' => array(
					'var_type'	=> 'int'
				),
				'post_id'	=> array(
					'var_type'	=> 'int',
					'unique'	=> '1',
					'valid_id'	=> '1'
				),
				'path'		=> array(
					'nice_name'	=> 'Path'
				),
				'width'		=> array(
					'var_type'	=> 'int'
				),
				'height' 	=> array(
					'var_type'	=> 'int'
				),
				'raw_top_white'	=> array(
					'nice_name'	=> 'Raw Top White'
				),
				'raw_bottom_white'	=> array(
					'nice_name'	=> 'Raw Bottom White'
				),
				'raw_left_white'	=> array(
					'nice_name'	=> 'Raw Left White'
				),
				'raw_right_white'	=> array(
					'nice_name'	=> 'Raw Right White'
				),
				'crop_left_white' => array(
					'nice_name'	=> 'Crop Left White'
				),
				'crop_right_white' => array(
					'nice_name'	=> 'Crop Right White'
				),
				'last_check'	=> array(
					'var_type'	=> 'datetime'
				),
				'_meta_updated'	=> array(
					'nice_name'	=> 'Updated'
				),
				'_meta_created' => array(
					'nice_name'	=> 'Created'
				)
			),
			'ovcdt'	=> array(
				'ovc_images'	=> array(
					'force_readonly_fields'	=> 'true',
					'fields'	=> array( 'pic.ID', 'pic.post_id', 'pic.raw_top_white', 'pic.raw_bottom_white', 'pic.raw_left_white', 'pic.raw_right_white', 'pic.crop_left_white', 'pic.crop_right_white', 'pic.width', 'pic.height', 'pic.path','pic.last_check', 'pic._meta_updated', 'pic._meta_created' ),
					'classname'	=> 'OVC_Table_ovc_images'
				)
			),
			'ovcop'	=> array(
				'analyze_images'	=> array(
					'nice_name'	=> 'Analyze Images',
					'descrip'	=> 'Analyze all OVC Images (Cropability)',
					'field_set'	=> 'ovc_images',
					'classname' => 'OVCOP_analyze_images',
					'order'		=> 15,
					'allowed_roles'	=> array( 'administrator', 'vida_sr_data_tech' )
				)
			),
			'ovc_list' => array(
				'image_cropping'	=> array(
					'type'			=> 'simple',
					'valid_values'	=> array()
				)
			)
		);

		OVCSC::multi_update_field_meta( $schema );
	}
}