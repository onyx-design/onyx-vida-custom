<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_add_spanish_description_fields {

	public function up() {

		OVC_Migrations::add_col_if_not_exists( 'parent_data', 'description_spanish', 'VARCHAR(2000)', 'description' );
		OVC_Migrations::add_col_if_not_exists( 'style_data', 'description_spanish', 'VARCHAR(2000)', 'description' );

		$ovc_schema = array(
			'parent_data'	=> array(
				'description_spanish'	=> array(
					'nice_name'		=> 'Description [Spanish]',
					'maxlength'		=> '2000',
				)
			),
			'style_data'	=> array(
				'description_spanish'	=> array(
					'nice_name'		=> 'Description [Spanish]',
					'maxlength'		=> '2000',
				)
			)
		);

		OVCSC::multi_update_field_meta( $ovc_schema );
	}
}