<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_update_parent_data__add_featured_homepage {

	public function up() {

		$schema = array(
			'parent_data'	=> array(
				// Add to database
				'featured'	=> array(
					'nice_name'	=> 'Featured',
					'var_type'	=> 'bool'
				),
				// Add to database
				'homepage'	=> array(
					'nice_name'	=> 'Homepage',
					'var_type'	=> 'bool'
				)
			),

			'ovcdt'			=> array(
				'parent_data'	=> array(
					'fields'	=> array(
						'pa.ID',
						'pa.parent_sku',
						'pa.brand',
						'pa.product_title',
						'pa.featured',
						'pa.homepage',
						'pa.description',
						'pa.fast_facts1',
						'pa.fast_facts2',
						'pa.fast_facts3',
						'pa.sizing',
						'pa.care',
						'pa.menu_order',
						'pa.img_main',
						'pa.img_gallery_ids',
						'pa.wc_parent_id',
						'pa._meta_updated'
					)
				)
			)
		);

		OVCSC::multi_update_field_meta( $schema );
	}
}