<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_update_api_shopify_image_fs {

	public function up() {

		global $wpdb;

		$wpdb->query( "UPDATE {$wpdb->prefix}ovc_schema SET field_set = 'api_shopify_image' WHERE field_set = 'shopify_image';" );

		OVCSC::init( true );

		$schema = array(
			'field_set_meta' => array(
				'api_shopify_image' => array(
					'type'	=> 'external',
					'sql_alias'	=> 'apisyimg'
				)
			)
		);

		OVCSC::multi_update_field_meta( $schema );
	}
}