<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_update_manual_api_feed_schema {

	public function up() {

		OVCSC::meta_add_array_item( 'ovc_list.walmart_manual_syncs', 'valid_values', 'CREATE' );
	
	}
}