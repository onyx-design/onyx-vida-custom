<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_alter_ovc_assembly__update_primary {

	public function up() {
		global $wpdb;

		$asm_fs = OVCSC::get_field_set( 'ovc_assemblies' );

		OVCSC::delete_field_meta( 'ovc_assemblies', 'primary_case', 'nice_name' );
		OVCSC::delete_field_meta( 'ovc_assemblies', 'primary_case', 'var_type' );

		$schema = array(
			'ovc_assemblies'	=> array(
				'is_primary'	=> array(
					'nice_name'		=> 'Is Primary',
					'var_type'		=> 'bool',
					'readonly'		=> 'true'
				),
			)
		);


		OVCSC::multi_update_field_meta( $schema );

		OVC_Migrations::drop_col_if_exists( $asm_fs, 'primary_case' );
		OVC_Migrations::add_col_if_not_exists( $asm_fs, 'is_primary', 'TINYINT(1) NOT NULL DEFAULT 0', 'total_quantity' );

		
		$wpdb->query( "UPDATE {$wpdb->prefix}ovc_assemblies SET is_primary = 1" );

	}
}