<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_alter_ovc_parent_data_ovc_image_sets__add_img_marketplace {

	public function up() {

		$schema = array(
			'parent_data'	=> array(
				'img_marketplace_featured'	=> array(
					'nice_name'		=> 'Marketplaced Featured Image Override',
					'readonly'		=> 'true',
				),
			),
			'image_sets'	=> array(
				'img_marketplace_featured'	=> array(
					'nice_name'		=> 'Marketplaced Featured Image Override',
					'var_type'		=> 'int',
				),
			),
		);

		$pa_fs = OVCSC::get_field_set( 'parent_data' );
		$img_fs = OVCSC::get_field_set( 'image_sets' );

		OVCSC::multi_update_field_meta( $schema );

		OVC_Migrations::add_col_if_not_exists( $pa_fs, 'img_marketplace_featured', 'INT(10) UNSIGNED NOT NULL', 'img_gallery_ids' );
		OVC_Migrations::add_col_if_not_exists( $img_fs, 'img_marketplace_featured', 'INT(10) UNSIGNED NOT NULL', 'img_gallery_ids' );
	}
}