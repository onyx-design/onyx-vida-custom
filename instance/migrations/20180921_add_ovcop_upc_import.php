<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_add_ovcop_upc_import {

	public function up() {

		$ovc_schema = array(
			'ovcop'	=> array(
				'upc_import'	=> array(
					'order'			=> '16',
					'nice_name'		=> 'UPC Import',
					'descrip'		=> 'Import UPC data by SKU',
					'classname'		=> 'OVCOP_upc_import',
					'allowed_roles'	=> array(
						'administrator',
						'vida_sr_data_tech'
					)
				)
			)
		);

		OVCSC::multi_update_field_meta( $ovc_schema );
	}
}