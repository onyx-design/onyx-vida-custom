<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_update_ovc_images_schema {

	public function up() {

		$schema = array(
			'ovc_images' => array(
				// Add to Database
				'crop_mode'		=> array(
					'nice_name'	=> 'Crop Mode',
					'var_type'	=> 'list:image_crop_mode'
				),
				// Add to Database
				'crop_focus'	=> array(
					'nice_name'	=> 'Crop Focus',
					'var_type'	=> 'list:image_crop_focus'
				)
			),
			'ovcdt'	=> array(
				'ovc_images'	=> array(
					// Added Field Groups
					'field_groups'			=> array(
						'essential' 		=> 'Essential',
						'images' 			=> 'Images',
						'details'			=> 'Analysis Details',
						'image_paths'		=> 'Image Paths',
						'meta_info'			=> 'Meta Info'
					),
					'fg.essential'			=> array( 'pic.ID', 'pic.post_id', 'pic.status', 'pic.crop_mode', 'pic.crop_focus' ),
				)
			),
			'ovc_list' => array(
				'image_crop_focus'	=> array(
					'type'			=> 'simple',
					'valid_values'	=> array(
						'Left Top',
						'Left Middle',
						'Left Bottom',
						'Center Top',
						'Center Middle',
						'Center Bottom',
						'Right Top',
						'Right Middle',
						'Right Bottom'
					)
				),
				'image_crop_mode' 	=> array(
					'type'			=> 'simple',
					'valid_values'	=> array(
						'Auto',
						'Scale',
						'Crop'
					)
				)
			)
		);

		OVCSC::multi_update_field_meta( $schema );

		OVCSC::delete_field_meta( 'ovc_list', 'image_cropping', 'type' );
		OVCSC::delete_field_meta( 'ovc_list', 'image_cropping', 'valid_values' );
		OVCSC::delete_field_meta( 'ovcdt', 'ovc_images', 'fg.status' );
	}
}