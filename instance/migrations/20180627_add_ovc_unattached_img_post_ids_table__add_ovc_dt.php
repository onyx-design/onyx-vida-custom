<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_add_ovc_unattached_img_post_ids_table__add_ovc_dt {

	public function up() {

		// Remove Size Map Field
		OVCSC::delete_field_meta( 'ovcdt', 'ovc_unattached_img_post_ids', 'force_readonly_fields' );

		$schema = array(
			'field_set_meta'	=> array(
				'ovc_unattached_img_post_ids'	=> array(
					'table_name'	=> 'wp_ovc_unattached_img_post_ids',
					'nice_name'		=> 'OVC Unattached Image Post IDs',
					'sql_alias'		=> 'unimgs',
					'type'			=> 'database',
					'delete_access'	=> array( 'administrator', 'vida_sr_data_tech' )
				)
			),
			'ovc_unattached_img_post_ids'	=> array(
				'ID'			=> array(
					'var_type'	=> 'int',
					'nice_name'	=> 'ID'
				),
				'post_id'		=> array(
					'var_type'	=> 'int',
					'nice_name'	=> 'Post ID',
					'valid_id'	=> '1',
					'readonly'	=> 'true'
				),
				'in_use'	=> array(
					'var_type'	=> 'bool',
					'nice_name'	=> 'In Use'
				),
				'post_parent'		=> array(
					'var_type'	=> 'int',
					'nice_name'	=> 'Post Parent ID',
					'readonly'	=> 'true'
				),
				'post_parent_type'		=> array(
					'nice_name'	=> 'Parent Type',
					'readonly'	=> 'true'
				),
				'post_created'		=> array(
					'nice_name'	=> 'Post Created',
					'var_type'	=> 'datetime',
					'readonly'	=> 'true'
				)
			),
			'ovcdt'	=> array(
				'ovc_unattached_img_post_ids'	=> array(
					'default_sorting'	=> 'unimgs.ID ASC',
					'fields'	=> array( 
						'unimgs.ID',
						'unimgs.post_id',
						'unimgs.in_use',
						'unimgs.post_parent',
						'unimgs.post_parent_type',
						'unimgs.post_created'
					),
					'classname'	=> 'OVC_Table_ovc_unattached_img_post_ids',
					'edit_access'	=> array( 'administrator', 'vida_sr_data_tech' )
				)
			),
		);

		OVCSC::multi_update_field_meta( $schema );
	}
}