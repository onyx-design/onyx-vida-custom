<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_update_shopify_variant__change_price_field {

	public function up() {

		$schema = array(
			'shopify_variant'	=> array(
				'price'	=> array(
					'ovc_field'	=> 'pr.price_retail||decimal:2|strval',
				),
			),
		);

		OVCSC::multi_update_field_meta( $schema );
	}
}