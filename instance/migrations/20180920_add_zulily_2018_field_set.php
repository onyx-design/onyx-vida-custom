<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_add_zulily_2018_field_set {

	public function up() {

		$ovc_schema = array(
			'field_set_meta'						=> array(
				'zulily_2018_ia'		=> array(
					'excel_worksheet'	=> 'Template',
					'nice_name'			=> 'Zulily 2018 IA',
					'ordered'			=> '1',
					'start_row'			=> '4',
					'type'				=> 'external'
				),
				'zulily_2018_kids_apparel'		=> array(
					'excel_worksheet'	=> 'Template',
					'nice_name'			=> 'Zulily 2018 Kids Apparel',
					'ordered'			=> '1',
					'start_row'			=> '4',
					'type'				=> 'external'
				),
				'zulily_2018_kids_personal_accessories'		=> array(
					'excel_worksheet'	=> 'Template',
					'nice_name'			=> 'Zulily 2018 Kids Personal/Accessories',
					'ordered'			=> '1',
					'start_row'			=> '4',
					'type'				=> 'external'
				),
				'zulily_2018_kids_sleep_undergarments'		=> array(
					'excel_worksheet'	=> 'Template',
					'nice_name'			=> 'Zulily 2018 Kids Sleep/Undergarments',
					'ordered'			=> '1',
					'start_row'			=> '4',
					'type'				=> 'external'
				),
			),
			'zulily_2018_ia' 						=> array(
				'Vendor Style Name'		=> array(
					'order'			=> '1',
					'filter_func'	=> 'filter_zulily_product_name',
				),
				'Vendor SKU'		=> array(
					'order'			=> '2',
					'ovc_field'		=> 'pr.sku',
				),
				'Vendor Size'		=> array(
					'order'			=> '3',
					'filter_func'	=> 'get_full_size_name'
				),
				'Vendor Product Color'		=> array(
					'order'			=> '4',
					'filter_func'	=> 'get_best_color_long_name'
				),
				'Vendor Product Description'		=> array(
					'order'			=> '5',
					'ovc_field'		=> 'st.description'
				),
				'UPC'		=> array(
					'order'			=> '6',
					'ovc_field'		=> 'pr.upc_code'
				),
				'Gender'		=> array(
					'order'			=> '7'
				),
				'Material Content'		=> array(
					'order'			=> '8',
					'filter_func'	=> 'get_material_string'
				),
				'Vendor Inventory'		=> array(
					'order'			=> '9',
				),
				'Cost'		=> array(
					'order'			=> '10',
					'ovc_field'		=> 'pr.price_zulily||price'
				),
				'Wholesale Cost'		=> array(
					'order'			=> '11',
					'ovc_field'		=> 'pr.price_zulily||price'
				),
				'MSRP'		=> array(
					'order'			=> '12',
					'ovc_field'		=> 'pr.price_msrp||price'
				),
				'Vendor Prepack ID'		=> array(
					'order'			=> '13'
				),
				'Prepack Quantity'		=> array(
					'order'			=> '14',
					'filter_func'	=> 'get_zulily_pack_string'
				),
				'HPS Measurement'		=> array(
					'order'			=> '15'
				),
				'Inseam'		=> array(
					'order'			=> '16'
				),
				'Country of Origin'		=> array(
					'order'			=> '17',
					'ovc_field'		=> 'st.origin_country'
				),
				'Care Instruction'		=> array(
					'order'			=> '18',
					'ovc_field'		=> 'st.care'
				),
				'Maternity'		=> array(
					'order'			=> '19'
				),
				'Nursing'		=> array(
					'order'			=> '20'
				),
				'Inseam Garment Size'		=> array(
					'order'			=> '21'
				),
				'Lined'		=> array(
					'order'			=> '22'
				),
				'Size Note'		=> array(
					'order'			=> '23'
				),
			),
			'zulily_2018_kids_apparel' 				=> array(
				'Vendor Style Name'				=> array(
					'order'			=> '1',
					'filter_func'	=> 'filter_zulily_product_name',
				),
				'Vendor SKU'					=> array(
					'order'			=> '2',
					'ovc_field'		=> 'pr.sku',
				),
				'Vendor Size'					=> array(
					'order'			=> '3',
					'filter_func'	=> 'get_full_size_name'
				),
				'Product zuSKU'					=> array(
					'order'			=> '4'
				),
				'Vendor Product Color'			=> array(
					'order'			=> '5',
					'filter_func'	=> 'get_best_color_long_name'
				),
				'Vendor Product Description'	=> array(
					'order'			=> '6',
					'ovc_field'		=> 'st.description'
				),
				'UPC'							=> array(
					'order'			=> '7',
					'ovc_field'		=> 'pr.upc_code'
				),
				'Vendor Barcode'				=> array(
					'order'			=> '8'
				),
				'Vendor Barcode Type'			=> array(
					'order'			=> '9'
				),
				'Gender'						=> array(
					'order'			=> '10'
				),
				'Vendor Name'					=> array(
					'order'			=> '11'
				),
				'Vendor Brand Name'				=> array(
					'order'			=> '12'
				),
				'Material Content'				=> array(
					'order'			=> '13',
					'filter_func'	=> 'get_material_string'
				),
				'Vendor Prepack ID'				=> array(
					'order'			=> '14'
				),
				'Vendor Inventory'				=> array(
					'order'			=> '15'
				),
				'Max Sale Quantity'				=> array(
					'order'			=> '16'
				),
				'Cost'							=> array(
					'order'			=> '17',
					'ovc_field'		=> 'pr.price_zulily||price'
				),
				'Wholesale Cost'				=> array(
					'order'			=> '18',
					'ovc_field'		=> 'pr.price_zulily||price'
				),
				'MSRP'							=> array(
					'order'			=> '19',
					'ovc_field'		=> 'pr.price_msrp||price'
				),
				'Prepack Quantity'				=> array(
					'order'			=> '20',
					'filter_func'	=> 'get_zulily_pack_string'
				),
				'Min. Advertised Price'			=> array(
					'order'			=> '21'
				),
				'Product Dimensions Units'		=> array(
					'order'			=> '22'
				),
				'Product Width'					=> array(
					'order'			=> '23'
				),
				'Product Depth'					=> array(
					'order'			=> '24'
				),
				'Product Height'				=> array(
					'order'			=> '25'
				),
				'Ship Height'					=> array(
					'order'			=> '26'
				),
				'Ship Width'					=> array(
					'order'			=> '27'
				),
				'Ship Length'					=> array(
					'order'			=> '28'
				),
				'Ship Weight Units'				=> array(
					'order'			=> '29'
				),
				'Ship Weight'					=> array(
					'order'			=> '30'
				),
				'Product Weight'				=> array(
					'order'			=> '31'
				),
				'Dimensions Units'				=> array(
					'order'			=> '32'
				),
				'Assembled Width'				=> array(
					'order'			=> '33'
				),
				'Assembled Height'				=> array(
					'order'			=> '34'
				),
				'Assembled Depth'				=> array(
					'order'			=> '35'
				),
				'Assembled Diameter'			=> array(
					'order'			=> '36'
				),
				'Product Circumference'			=> array(
					'order'			=> '37'
				),
				'Product Diameter'				=> array(
					'order'			=> '38'
				),
				'Inseam'						=> array(
					'order'			=> '39'
				),
				'Country of Origin'				=> array(
					'order'			=> '40',
					'ovc_field'		=> 'st.origin_country'
				),
				'Care Instructions'				=> array(
					'order'			=> '41',
					'ovc_field'		=> 'st.care'
				),
				'Pant Style'					=> array(
					'order'			=> '42'
				),
				'Lining'						=> array(
					'order'			=> '43'
				),
				'Product Includes'				=> array(
					'order'			=> '44'
				),
				'Product Does Not Include'		=> array(
					'order'			=> '45'
				),
				'Is Electric'					=> array(
					'order'			=> '46'
				),
				'Warranty'						=> array(
					'order'			=> '47'
				),
				'Features'						=> array(
					'order'			=> '48'
				),
				'Rise'							=> array(
					'order'			=> '49'
				),
				'Inseam Garment Size'			=> array(
					'order'			=> '50'
				),
				'Lined'							=> array(
					'order'			=> '51'
				),
				'Apparel Closure'				=> array(
					'order'			=> '52'
				),
				'Includes Batteries'			=> array(
					'order'			=> '53'
				),
				'Battery Size'					=> array(
					'order'			=> '54'
				),
				'Cord Type'						=> array(
					'order'			=> '55'
				),
				'Cord Length Units'				=> array(
					'order'			=> '56'
				),
				'Cord Length'					=> array(
					'order'			=> '57'
				),
				'Dual Voltage'					=> array(
					'order'			=> '58'
				),
				'Charge Time Units'				=> array(
					'order'			=> '59'
				),
				'Charge Time'					=> array(
					'order'			=> '60'
				),
				'Use Time per Charge Units'		=> array(
					'order'			=> '61'
				),
				'Use Time per Charge'			=> array(
					'order'			=> '62'
				),
				'UPF Rating'					=> array(
					'order'			=> '63'
				),
				'Choking Hazard Type'			=> array(
					'order'			=> '64'
				),
				'Prop 65'						=> array(
					'order'			=> '65'
				),
				'State'							=> array(
					'order'			=> '66'
				),
				'Manufacturer Address'			=> array(
					'order'			=> '67'
				),
				'Product Net Weight in kg'		=> array(
					'order'			=> '68'
				),
				'Fabric Construction'			=> array(
					'order'			=> '69',
					'ovc_field'		=> 'fabric',
				),
				'Stitch Count'					=> array(
					'order'			=> '70'
				),
				'3rd Party Test'				=> array(
					'order'			=> '71'
				),
			),
			'zulily_2018_kids_personal_accessories' => array(
				'Vendor Style Name'							=> array(
					'order'			=> '1',
					'filter_func'	=> 'filter_zulily_product_name',
				),
				'Vendor SKU'								=> array(
					'order'			=> '2',
					'ovc_field'		=> 'pr.sku',
				),
				'Vendor Size'								=> array(
					'order'			=> '3',
					'filter_func'	=> 'get_full_size_name'
				),
				'Product zuSKU'								=> array(
					'order'			=> '4'
				),
				'Vendor Product Color'						=> array(
					'order'			=> '5',
					'filter_func'	=> 'get_best_color_long_name'
				),
				'Vendor Product Description'				=> array(
					'order'			=> '6',
					'ovc_field'		=> 'st.description'
				),
				'UPC'										=> array(
					'order'			=> '7',
					'ovc_field'		=> 'pr.upc_code'
				),
				'Vendor Barcode'							=> array(
					'order'			=> '8'
				),
				'Vendor Barcode Type'						=> array(
					'order'			=> '9'
				),
				'Gender'									=> array(
					'order'			=> '10'
				),
				'Vendor Name'								=> array(
					'order'			=> '11'
				),
				'Vendor Brand Name'							=> array(
					'order'			=> '12'
				),
				'Material Content'							=> array(
					'order'			=> '13',
					'filter_func'	=> 'get_material_string'
				),
				'Vendor Prepack ID'							=> array(
					'order'			=> '14'
				),
				'Vendor Inventory'							=> array(
					'order'			=> '15'
				),
				'Max Sale Quantity'							=> array(
					'order'			=> '16'
				),
				'Cost'										=> array(
					'order'			=> '17',
					'ovc_field'		=> 'pr.price_zulily||price'
				),
				'Wholesale Cost'							=> array(
					'order'			=> '18',
					'ovc_field'		=> 'pr.price_zulily||price'
				),
				'MSRP'										=> array(
					'order'			=> '19',
					'ovc_field'		=> 'pr.price_msrp||price'
				),
				'Prepack Quantity'							=> array(
					'order'			=> '20',
					'filter_func'	=> 'get_zulily_pack_string'
				),
				'Min. Advertised Price'						=> array(
					'order'			=> '21'
				),
				'Product Dimensions Units'					=> array(
					'order'			=> '22'
				),
				'Product Width'								=> array(
					'order'			=> '23'
				),
				'Product Depth'								=> array(
					'order'			=> '24'
				),
				'Product Height'							=> array(
					'order'			=> '25'
				),
				'Ship Height'								=> array(
					'order'			=> '26'
				),
				'Ship Width'								=> array(
					'order'			=> '27'
				),
				'Ship Length'								=> array(
					'order'			=> '28'
				),
				'Ship Weight Units'							=> array(
					'order'			=> '29'
				),
				'Ship Weight'								=> array(
					'order'			=> '30'
				),
				'Product Weight'							=> array(
					'order'			=> '31'
				),

				'Weight Units'								=> array(
					'order'			=> '32'
				),
				'Weight Capacity'							=> array(
					'order'			=> '33'
				),

				'Dimensions Units'							=> array(
					'order'			=> '34'
				),
				'Assembled Width'							=> array(
					'order'			=> '35'
				),
				'Assembled Height'							=> array(
					'order'			=> '36'
				),
				'Assembled Depth'							=> array(
					'order'			=> '37'
				),
				'Assembled Diameter'						=> array(
					'order'			=> '38'
				),
				'Product Circumference'						=> array(
					'order'			=> '39'
				),
				'Product Diameter'							=> array(
					'order'			=> '40'
				),
				'Diagonal Screen Size in Inches'			=> array(
					'order'			=> '41'
				),
				'Inseam'									=> array(
					'order'			=> '42'
				),
				'Eyewear Lens Width in Millimeters'			=> array(
					'order'			=> '43'
				),
				'Eyewear Bridge Distance in Millimeters'	=> array(
					'order'			=> '44'
				),
				'Eyewear Arm Length in Millimeters'			=> array(
					'order'			=> '45'
				),
				'Country of Origin'							=> array(
					'order'			=> '46',
					'ovc_field'		=> 'st.origin_country'
				),
				'Care Instructions'							=> array(
					'order'			=> '47',
					'ovc_field'		=> 'st.care'
				),
				'Jewelry Measurements'						=> array(
					'order'			=> '48'
				),
				'Stone Cut'									=> array(
					'order'			=> '49'
				),
				'Stone Setting'								=> array(
					'order'			=> '50'
				),
				'Lining'									=> array(
					'order'			=> '51'
				),
				'Product Includes'							=> array(
					'order'			=> '52'
				),
				'Product Does Not Include'					=> array(
					'order'			=> '53'
				),
				'Is Electric'								=> array(
					'order'			=> '54'
				),
				'Warranty'									=> array(
					'order'			=> '55'
				),
				'Features'									=> array(
					'order'			=> '56'
				),
				'Compatibility'								=> array(
					'order'			=> '57'
				),
				'Inseam Garment Size'						=> array(
					'order'			=> '58'
				),
				'Lined'										=> array(
					'order'			=> '59'
				),
				'Jewelry Closure Type'						=> array(
					'order'			=> '60'
				),
				'Watch Closure Type'						=> array(
					'order'			=> '61'
				),
				'Watch Crown Type'							=> array(
					'order'			=> '62'
				),
				'Watch Display Type'						=> array(
					'order'			=> '63'
				),
				'Band Type'									=> array(
					'order'			=> '64'
				),
				'Number of Exterior Compartments'			=> array(
					'order'			=> '65'
				),
				'Number of Interior Compartments'			=> array(
					'order'			=> '66'
				),
				'Age Recommendation Units'					=> array(
					'order'			=> '67'
				),
				'Lower Age Recommendation'					=> array(
					'order'			=> '68'
				),
				'Upper Age Recommendation'					=> array(
					'order'			=> '69'
				),
				'Includes Batteries'						=> array(
					'order'			=> '70'
				),
				'Battery Size'								=> array(
					'order'			=> '71'
				),
				'Cord Type'									=> array(
					'order'			=> '72'
				),
				'Cord Length Units'							=> array(
					'order'			=> '73'
				),
				'Cord Length'								=> array(
					'order'			=> '74'
				),
				'Dual Voltage'								=> array(
					'order'			=> '75'
				),
				'Charge Time Units'							=> array(
					'order'			=> '76'
				),
				'Charge Time'								=> array(
					'order'			=> '77'
				),
				'Use Time per Charge Units'					=> array(
					'order'			=> '78'
				),
				'Use Time per Charge'						=> array(
					'order'			=> '79'
				),
				'Number of Pieces'							=> array(
					'order'			=> '80'
				),
				'UPF Rating'								=> array(
					'order'			=> '81'
				),
				'Choking Hazard Type'						=> array(
					'order'			=> '82'
				),
				'Prop 65'									=> array(
					'order'			=> '83'
				),
				'State'										=> array(
					'order'			=> '84'
				),
				'Manufacturer Address'						=> array(
					'order'			=> '85'
				),
				'Product Net Weight in kg'					=> array(
					'order'			=> '86'
				),
				'Fabric Construction'						=> array(
					'order'			=> '87',
					'ovc_field'		=> 'fabric',
				),
				'Stitch Count'								=> array(
					'order'			=> '88'
				),
				'Primary Jewelry Content'					=> array(
					'order'			=> '89'
				),
				'3rd Party Test'							=> array(
					'order'			=> '90'
				),
			),
			'zulily_2018_kids_sleep_undergarments' 	=> array(
				'Vendor Style Name'				=> array(
					'order'			=> '1',
					'filter_func'	=> 'filter_zulily_product_name',
				),
				'Vendor SKU'					=> array(
					'order'			=> '2',
					'ovc_field'		=> 'pr.sku',
				),
				'Vendor Size'					=> array(
					'order'			=> '3',
					'filter_func'	=> 'get_full_size_name'
				),
				'Product zuSKU'					=> array(
					'order'			=> '4'
				),
				'Vendor Product Color'			=> array(
					'order'			=> '5',
					'filter_func'	=> 'get_best_color_long_name'
				),
				'Vendor Product Description'	=> array(
					'order'			=> '6',
					'ovc_field'		=> 'st.description'
				),
				'UPC'							=> array(
					'order'			=> '7',
					'ovc_field'		=> 'pr.upc_code'
				),
				'Vendor Barcode'				=> array(
					'order'			=> '8'
				),
				'Vendor Barcode Type'			=> array(
					'order'			=> '9'
				),
				'Gender'						=> array(
					'order'			=> '10'
				),
				'Vendor Name'					=> array(
					'order'			=> '11'
				),
				'Vendor Brand Name'				=> array(
					'order'			=> '12'
				),
				'Material Content'				=> array(
					'order'			=> '13',
					'filter_func'	=> 'get_material_string'
				),
				'Vendor Prepack ID'				=> array(
					'order'			=> '14'
				),
				'Vendor Inventory'				=> array(
					'order'			=> '15'
				),
				'Max Sale Quantity'				=> array(
					'order'			=> '16'
				),
				'Cost'							=> array(
					'order'			=> '17',
					'ovc_field'		=> 'pr.price_zulily||price'
				),
				'Wholesale Cost'				=> array(
					'order'			=> '18',
					'ovc_field'		=> 'pr.price_zulily||price'
				),
				'MSRP'							=> array(
					'order'			=> '19',
					'ovc_field'		=> 'pr.price_msrp||price'
				),
				'Prepack Quantity'				=> array(
					'order'			=> '20',
					'filter_func'	=> 'get_zulily_pack_string'
				),
				'Min. Advertised Price'			=> array(
					'order'			=> '21'
				),
				'Product Dimensions Units'		=> array(
					'order'			=> '22'
				),
				'Product Width'					=> array(
					'order'			=> '23'
				),
				'Product Depth'					=> array(
					'order'			=> '24'
				),
				'Product Height'				=> array(
					'order'			=> '25'
				),
				'Ship Height'					=> array(
					'order'			=> '26'
				),
				'Ship Width'					=> array(
					'order'			=> '27'
				),
				'Ship Length'					=> array(
					'order'			=> '28'
				),
				'Ship Weight Units'				=> array(
					'order'			=> '29'
				),
				'Ship Weight'					=> array(
					'order'			=> '30'
				),
				'Product Weight'				=> array(
					'order'			=> '31'
				),
				'Dimensions Units'				=> array(
					'order'			=> '32'
				),
				'Assembled Width'				=> array(
					'order'			=> '33'
				),
				'Assembled Height'				=> array(
					'order'			=> '34'
				),
				'Assembled Depth'				=> array(
					'order'			=> '35'
				),
				'Assembled Diameter'			=> array(
					'order'			=> '36'
				),
				'Product Circumference'			=> array(
					'order'			=> '37'
				),
				'Product Diameter'				=> array(
					'order'			=> '38'
				),
				'Inseam'						=> array(
					'order'			=> '39'
				),
				'Country of Origin'				=> array(
					'order'			=> '40',
					'ovc_field'		=> 'st.origin_country'
				),
				'Care Instructions'				=> array(
					'order'			=> '41',
					'ovc_field'		=> 'st.care'
				),
				'Panty Style'					=> array(
					'order'			=> '42'
				),
				'Lining'						=> array(
					'order'			=> '43'
				),
				'Product Includes'				=> array(
					'order'			=> '44'
				),
				'Product Does Not Include'		=> array(
					'order'			=> '45'
				),
				'Is Electric'					=> array(
					'order'			=> '46'
				),
				'Warranty'						=> array(
					'order'			=> '47'
				),
				'Features'						=> array(
					'order'			=> '48'
				),
				'Inseam Garment Size'			=> array(
					'order'			=> '49'
				),
				'Lined'							=> array(
					'order'			=> '50'
				),
				'Apparel Closure'				=> array(
					'order'			=> '51'
				),
				'Age Recommendation Units'		=> array(
					'order'			=> '52'
				),
				'Lower Age Recommendation'		=> array(
					'order'			=> '53'
				),
				'Upper Age Recommendation'		=> array(
					'order'			=> '54'
				),
				'Includes Batteries'			=> array(
					'order'			=> '55'
				),
				'Battery Size'					=> array(
					'order'			=> '56'
				),
				'Cord Type'						=> array(
					'order'			=> '57'
				),
				'Cord Length Units'				=> array(
					'order'			=> '58'
				),
				'Cord Length'					=> array(
					'order'			=> '59'
				),
				'Dual Voltage'					=> array(
					'order'			=> '60'
				),
				'Charge Time Units'				=> array(
					'order'			=> '61'
				),
				'Charge Time'					=> array(
					'order'			=> '62'
				),
				'Use Time per Charge Units'		=> array(
					'order'			=> '63'
				),
				'Use Time per Charge'			=> array(
					'order'			=> '64'
				),
				'Number of Pieces'				=> array(
					'order'			=> '65'
				),
				'UPF Rating'					=> array(
					'order'			=> '66'
				),
				'Flame Resistant Sleepwear'		=> array(
					'order'			=> '67'
				),
				'Tight-Fitting Sleepwear'		=> array(
					'order'			=> '68'
				),
				'Choking Hazard Type'			=> array(
					'order'			=> '69'
				),
				'Prop 65'						=> array(
					'order'			=> '70'
				),
				'State'							=> array(
					'order'			=> '71'
				),
				'Manufacturer Address'			=> array(
					'order'			=> '72'
				),
				'Product Net Weight in kg'		=> array(
					'order'			=> '73'
				),
				'Fabric Construction'			=> array(
					'order'			=> '74',
					'ovc_field'		=> 'fabric',
				),
				'Stitch Count'					=> array(
					'order'			=> '75'
				),
				'3rd Party Test'				=> array(
					'order'			=> '76'
				),
			),
		);

		OVCSC::multi_update_field_meta( $ovc_schema );
	}
}