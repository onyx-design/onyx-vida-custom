<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_update_parent_care_instructions_field {

	public function up() {
		global $wpdb;

		$ovc_schema = array(
			'parent_data'	=> array(
				'care'			=> array(
					'var_type'		=> 'list:care',
					'maxlength'		=> '80'
				),
				'care_old'		=> array(
					'nice_name'		=> 'Care Instructions (OLD VALUE)',
					'readonly'		=> 'true'
				)
			),
			'ovc_list'		=> array(
				'care'			=> array(
					'allow_blank'	=> 'false'
				)
			),
			'ovcdt'			=> array(
				'parent_data'	=> array(
					'fields'		=> array(
						'pa.ID',
						'pa.parent_sku',
						'pa.brand',
						'pa.product_title',
						'pa.featured',
						'pa.homepage',
						'pa.description',
						'pa.fast_facts1',
						'pa.fast_facts2',
						'pa.fast_facts3',
						'pa.sizing',
						'pa.care',
						'pa.care_old',
						'pa.menu_order',
						'pa.img_main',
						'pa.img_gallery_ids',
						'pa.wc_parent_id',
						'pa._meta_updated',
					)
				)
			)
		);

		OVCSC::multi_update_field_meta( $ovc_schema );
		
		OVC_Migrations::add_col_if_not_exists( 'parent_data', 'care_old', "VARCHAR(80) NOT NULL DEFAULT ''", 'care' );

		$wpdb->query( "UPDATE {$wpdb->prefix}ovc_parent_data SET care_old = care WHERE care != ''" );
	
		$care_list = OVC_Lists()->get_list( 'care' );
		$wpdb->query( "UPDATE {$wpdb->prefix}ovc_parent_data SET care = '' WHERE care NOT IN (" . '"' . implode( '", "', $care_list ) . '"' . ")" );
	}
}