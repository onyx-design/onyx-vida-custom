<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OVC_Migration_add_ovcop_ovc_import {

	public function up() {

		$this->ovcop_schema();
		$this->ovcop_permissions_schema();
	}

	private function ovcop_schema() {

		$schema = array(
			'ovcop'	=> array(
				'ovc_import'	=> array(
					'order'			=> '18',
					'nice_name'		=> 'OVC Import',
					'descrip'		=> 'Mass update OVC data.',
					'classname'		=> 'OVCOP_ovc_import',
					'allowed_roles'	=> array(
						'administrator',
						'vida_sr_data_tech'
					)
				)
			)
		);

		OVCSC::multi_update_field_meta( $schema );
	}

	private function ovcop_permissions_schema() {

		$schema = array(
			'ovc_products'		=> array(
				'ovc_status'				=> array(
					'allow_csv_import'	=> 'true'
				),
				'oms_status'				=> array(
					'allow_csv_import'	=> 'true'
				),
				'price_retail'				=> array(
					'allow_csv_import'	=> 'true'
				),
				'price_msrp'				=> array(
					'allow_csv_import'	=> 'true'
				),
				'price_case'				=> array(
					'allow_csv_import'	=> 'true'
				),
				'price_amazon'				=> array(
					'allow_csv_import'	=> 'true'
				),
				'price_groupon'				=> array(
					'allow_csv_import'	=> 'true'
				),
				'price_zulily'				=> array(
					'allow_csv_import'	=> 'true'
				),
				'price_wc'					=> array(
					'allow_csv_import'	=> 'true'
				),
				'price_walmart'				=> array(
					'allow_csv_import'	=> 'true'
				),
				'shopify_sale_price'		=> array(
					'allow_csv_import'	=> 'true'
				),
				'qtyprice1'					=> array(
					'allow_csv_import'	=> 'true'
				),
				'qtyprice2'					=> array(
					'allow_csv_import'	=> 'true'
				),
				'qtyprice3'					=> array(
					'allow_csv_import'	=> 'true'
				),
				'qtyprice4'					=> array(
					'allow_csv_import'	=> 'true'
				),
				'qtyprice5'					=> array(
					'allow_csv_import'	=> 'true'
				),
				'qtyprice6'					=> array(
					'allow_csv_import'	=> 'true'
				),
				'base_cost'					=> array(
					'allow_csv_import'	=> 'true'
				),
				'handling_fee'				=> array(
					'allow_csv_import'	=> 'true'
				),
				'shipping_fee'				=> array(
					'allow_csv_import'	=> 'true'
				),
				'dropship_fee'				=> array(
					'allow_csv_import'	=> 'true'
				),
				'sales_cost'				=> array(
					'allow_csv_import'	=> 'true'
				),
				'sync_wc'					=> array(
					'allow_csv_import'	=> 'true'
				),
				'sync_walmart'				=> array(
					'allow_csv_import'	=> 'true'
				),
				'sync_zulily'				=> array(
					'allow_csv_import'	=> 'true'
				),
				'sync_amazon'				=> array(
					'allow_csv_import'	=> 'true'
				),
				'sync_groupon'				=> array(
					'allow_csv_import'	=> 'true'
				),
				'sync_shopify'				=> array(
					'allow_csv_import'	=> 'true'
				),
				'pk_str'					=> array(
					'allow_csv_import'	=> 'true'
				),
				'case_sku'					=> array(
					'allow_csv_import'	=> 'true'
				),
				'is_case'					=> array(
					'allow_csv_import'	=> 'true'
				),
				'is_raw'					=> array(
					'allow_csv_import'	=> 'true'
				),
				'color_long_name'			=> array(
					'allow_csv_import'	=> 'true'
				),
				'asin'						=> array(
					'allow_csv_import'	=> 'true'
				),
				'parent_asin'				=> array(
					'allow_csv_import'	=> 'true'
				),
				'class'						=> array(
					'allow_csv_import'	=> 'true'
				),
				'department'				=> array(
					'allow_csv_import'	=> 'true'
				),
				'unit_length'				=> array(
					'allow_csv_import'	=> 'true'
				),
				'unit_height'				=> array(
					'allow_csv_import'	=> 'true'
				),
				'unit_width'				=> array(
					'allow_csv_import'	=> 'true'
				),
				'unit_weight'				=> array(
					'allow_csv_import'	=> 'true'
				),
				'case_weight'				=> array(
					'allow_csv_import'	=> 'true'
				),
				'case_length'				=> array(
					'allow_csv_import'	=> 'true'
				),
				'case_width'				=> array(
					'allow_csv_import'	=> 'true'
				),
				'case_height'				=> array(
					'allow_csv_import'	=> 'true'
				),
				'reserve_qty'				=> array(
					'allow_csv_import'	=> 'true'
				),
				'range2'					=> array(
					'allow_csv_import'	=> 'true'
				),
				'range3'					=> array(
					'allow_csv_import'	=> 'true'
				),
				'range4'					=> array(
					'allow_csv_import'	=> 'true'
				),
				'range5'					=> array(
					'allow_csv_import'	=> 'true'
				),
				'range6'					=> array(
					'allow_csv_import'	=> 'true'
				),
				'duty'						=> array(
					'allow_csv_import'	=> 'true'
				),
				'oms_img_1'					=> array(
					'allow_csv_import'	=> 'true'
				),
				'oms_img_2'					=> array(
					'allow_csv_import'	=> 'true'
				),
				'location'					=> array(
					'allow_csv_import'	=> 'true'
				),
				'status_receiving'			=> array(
					'allow_csv_import'	=> 'true'
				),
				'status_description'		=> array(
					'allow_csv_import'	=> 'true'
				),
				'status_wholesale_pricing'	=> array(
					'allow_csv_import'	=> 'true'
				),
				'status_online_pricing'		=> array(
					'allow_csv_import'	=> 'true'
				),
				'status_upc'				=> array(
					'allow_csv_import'	=> 'true'
				),
				'use_potential_inventory'	=> array(
					'allow_csv_import'	=> 'true'
				),
				'allow_invalid_upc'			=> array(
					'allow_csv_import'	=> 'true'
				),
				'tag_barcode'				=> array(
					'allow_csv_import'	=> 'true'
				),
			)
		);

		OVCSC::multi_update_field_meta( $schema );
	}
}