<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Row_ovc_assemblies extends OVC_Row {

	protected $assembly_items = array();

	/**
	 * Initialize the Assembly Items for this Assembly
	 * 
	 * @param 	OVC_Results|bool 	$results
	 * 
	 * @return 	array
	 */
	public function init_assembly_items( $results = false ) {
		OVC_Results::maybe_init( $results );

		$ovc_pr = $this->get_product( $results );

		if( $ovc_pr ) {

			$where = array(
				'case_sku'	=> $ovc_pr->data( 'pr.sku' ),
				array(
					'var'	=> 'ID',
					'op'	=> '!=',
					'value'	=> $this->data( 'asm.ovc_id' )
				)
			);
			$case_products = OVCDB()->load_rows_where( FS( 'ovc_products' ), $where );

			foreach( $case_products as $case_product ) {

				if( 'CS' == $ovc_pr->data( 'pr.size' ) ) {

					$assembly_item_data = array(
						'assembly_id'		=> $this->ID,
						'ovc_id'			=> $case_product->data( 'pr.ID' ),
						'quantity'			=> $case_product->data( 'pr.unit_per_box' )
					);
				} elseif( count( $case_products ) == $ovc_pr->data( 'pr.sku_pkqty' ) / $case_product->data( 'pr.sku_pkqty' ) ) {

					$assembly_item_data = array(
						'assembly_id'		=> $this->ID,
						'ovc_id'			=> $case_product->data( 'pr.ID' ),
						'quantity'			=> 1
					);
				} elseif( 1 == count( $case_products ) ) {

					$assembly_item_data = array(
						'assembly_id'		=> $this->ID,
						'ovc_id'			=> $case_product->data( 'pr.ID' ),
						'quantity'			=> $ovc_pr->data( 'pr.sku_pkqty' ) / $case_product->data( 'pr.sku_pkqty' )
					);
				} else {

					$results->error( "Product OVC ID: {$case_product->data( 'pr.ID' )} for Case SKU: {$ovc_pr->data( 'pr.sku' )} failed validation - unequal SKU pack quantity per Case pack quantity compared to sibling count (Sibling Count: " . count( $case_products ) . " Case Pack Quantity: {$ovc_pr->data( 'pr.sku_pkqty' )} Product Pack Quantity: {$case_product->data( 'pr.sku_pkqty' )})" );
				}
				
				if( isset( $assembly_item_data ) ) {

					$assembly_item = OVCDB()->new_row( FS( 'ovc_assembly_items' ), $assembly_item_data );

					if( $assembly_item->save( $results ) ) {
						$this->assembly_items[ $assembly_item->ID ] = $assembly_item;
					}
				}
			}
		}

		return $this->assembly_items;
	}

	public function validate_total_quantity( $results = false ) {

		$assembly_items = $this->get_assembly_items();
		$asmi_total_qty = 0;
		
		foreach( $assembly_items as $assembly_item ) {

			$asmi_total_qty += $assembly_item->data( 'asmi.quantity' );
		}

		return $asmi_total_qty == $this->data( 'asm.total_quantity' );
	}

	public function get_product( $results = false ) {
		OVC_Results::maybe_init( $results );

		return $this->ovc_id ? $this->get_joined_data( FS( 'pr' ), $results ) : null;
	}

	public function get_assembly_items( $results = false ) {
		OVC_Results::maybe_init( $results );

		return $this->get_joined_data( FS( 'asmi' ), $results );
	}

	public function get_max_depth() {

		return $this->get_product()->get_max_depth();
	}

	/**
	 * Calculate the maximum theoretical quantity that can be made with children
	 * $based_on field uses that field for calculating the quantity
	 * 
	 * @param 	string 				$based_on
	 * @param 	OVC_Results|bool 	$results
	 * 
	 * @return 	int|bool
	 */
	public function get_potential_stock( $results = false ) {

		$potential_qty = null;

		foreach( $this->get_assembly_items() as $assembly_item ) {

			$assembly_item_product = $assembly_item->get_product( $results );

			$assembly_item_potential_stock = $assembly_item_product->get_potential_stock( $assembly_item, false, $results );

			$potential_assembly_qty = $assembly_item->data( 'asmi.quantity' ) ? floor( $assembly_item_potential_stock / $assembly_item->data( 'asmi.quantity' ) ) : 0;

			$potential_qty = isset( $potential_qty ) 
				? min( $potential_qty, $potential_assembly_qty ) 
				: $potential_assembly_qty;
		}

		return $potential_qty ?? 0;

		// // $children_quantities = array();

		// if( $based_on_field = OVCSC::get_field( $based_on, FS( 'ovc_products' ) ) ) {

		// 	foreach( $this->get_assembly_items() as $assembly_item ) {

		// 		$ovc_pr = $assembly_item->get_product( $results );

		// 		$children_quantities[ $assembly_item->ID ] = floor( $ovc_pr->data( $based_on_field->global_name ) / $assembly_item->data( 'asmi.quantity' ) );
		// 	}
		// } else {

		// 	return false;
		// }

		// return min( $children_quantities );
	}
}