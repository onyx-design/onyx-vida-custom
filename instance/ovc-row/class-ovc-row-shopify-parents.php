<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Row_shopify_parents extends OVC_Row {

	protected $shopify_variants;

	protected $resync = false;

	protected $sync_time;

	// SHOPIFY API FUNCTIONS

	public function api_refresh( &$results = false ) {
		OVC_Results::maybe_init( $results );

		$api_request_data = array(
			'url_params'	=> array( 
				'shopify_product_id'	=> $this->data( 'sypas.shopify_parent_id' )
			)
		);

		return $this->handle_api_response( 
			OVC_API_Shopify::request( 'get_product', $api_request_data, $results ),
			$results
		);
	}

	public function delete_parent( &$results = false ) {
		OVC_Results::maybe_init( $results );

		if( $this->exists()
			&& $this->shopify_parent_id
		) {

			$request_data = array(
				'url_params'		=> array(
					'shopify_product_id'	=> $this->shopify_parent_id
				)
			);

			$api_response = OVC_API_Shopify::request( 'delete_product', $request_data, $results );

			return ( $results->success = $this->handle_api_response( $api_response, $results, true ) );
		} else {
			return $results->error( "Cannot delete parent without Shopify Parent ID" );
		}
	}

	public function delete_shopify_images( &$results = false ) {
		OVC_Results::maybe_init( $results );

		foreach( $this->get_children( FS( 'shopify_images' ) ) as $ovc_shopify_image ) {
			$ovc_shopify_image->delete( $results );
		}
	}

	public function reset_shopify_images( &$results = false ) {
		OVC_Results::maybe_init( $results );

		$this->delete_shopify_images( $results );

		foreach( $this->get_children( FS( 'shopify_products' ) ) as $ovc_shopify_variant ) {

			$shopify_variant_data = array(
				'sypr.shopify_image_id'	=> ''
			);

			$ovc_shopify_variant->update_data( $shopify_variant_data, $results );
		}

		$parent_data = array(
			'sypas.updated'	=> current_time( 'mysql', 1 )
		);

		$this->update_data( $parent_data, $results );

		$results->main_msg = $results->success ? "Shopify Images Reset Successfully." : "Shopify Image Reset Failed.";
	}

	/**
	 * dev:improve These handle_api_response functions should be split out as they are being used even when not a direct api response
	 * 
	 * @param type $response 
	 * @param type|bool &$results 
	 * @return type
	 */
	public function handle_api_response( $response, &$results = false ) {
		OVC_Results::maybe_init( $results );


		if( $response instanceof OVC_API_Response && !$response->succeeded() ) {

			// Check if this is an image error, if it is we want to remove the image and resync up a new image
			// If we're resyncing, don't get stuck in a loop
			if( !$this->resync 
				&& isset( $response->errors->image )
			) {

				$this->reset_shopify_images( $results );
				// Force sync (true) and resync (true)
				$this->shopify_sync( $results, true, true );
				$results->notice( "Image ID incorrect for Shopify Parent ID: {$this->ID}, reset images and resynced.." );
			} else {

				// ovc_debug( $response );

				// dev:improve dev:data_errors
				$parent_errors = array(
					'sypas.errors' 	=> maybe_serialize( $response->errors )
				);

				$this->update_data( $parent_errors, $results );

				return $results->error( print_r( $response->errors, true ) );
			}
		}

		if( $response instanceof OVC_API_Response ) {

			switch( $response->request_type('type') ) {

				case 'get_product':
				case 'create_product':
				case 'update_product':
					// Update the parent from the response
					$parent_data = array(
						'sypas.shopify_parent_id' 	=> $response->id,
						'sypas.title'				=> $response->title,
						'sypas.handle'				=> $response->handle,
						'sypas.published'			=> date("Y-m-d H:i:s",strtotime( $response->published_at ) ),
						'sypas.updated'				=> date("Y-m-d H:i:s",strtotime( $response->updated_at ) ),
						'sypas.last_sync'			=> $this->sync_time ?? current_time( 'mysql', 1 ),
						'sypas.errors'				=> ''
					);

					$this->update_data( $parent_data, $results );

					// Update Images
					if( $response->images ) {
						foreach( $response->images as $image_response ) {
							$image_row = OVC_Row_shopify_images::get_from_api_response( $image_response, $this, $results );

							if( $image_row->exists() ) {

								$image_row->handle_api_response( $image_response, $results );	
							}
						}
					}

					// Update Variants
					if( $response->variants && is_array( $response->variants ) ) {
						foreach( $response->variants as $variant_response ) {

							$variant_row = OVC_Row_shopify_products::get_from_api_response( $variant_response, $results );

							$variant_row->handle_api_response( $variant_response, $results, false );
						}
					}
					
					return !$results->has_error;
				break;

				case 'delete_product':

					$this->delete_shopify_images( $results );

					return parent::delete( $results, true );
				break;
			}
		} else {

			// Update the parent from the response
			$parent_data = array(
				'sypas.shopify_parent_id' 	=> $response->id,
				'sypas.title'				=> $response->title,
				'sypas.handle'				=> $response->handle,
				'sypas.published'			=> date("Y-m-d H:i:s",strtotime( $response->published_at ) ),
				'sypas.updated'				=> date("Y-m-d H:i:s",strtotime( $response->updated_at ) ),
				'sypas.last_sync'			=> $this->sync_time ?? current_time( 'mysql', 1 ),
				'sypas.errors'				=> ''
			);

			$this->update_data( $parent_data, $results );

			// Update Images
			if( $response->images ) {
				foreach( $response->images as $image_response ) {
					$image_row = OVC_Row_shopify_images::get_from_api_response( $image_response, $this, $results );

					if( $image_row->exists() ) {

						$image_row->handle_api_response( $image_response, $results );	
					}
				}
			}

			// Update Variants
			if( $response->variants && is_array( $response->variants ) ) {
				foreach( $response->variants as $variant_response ) {

					$variant_row = OVC_Row_shopify_products::get_from_api_response( $variant_response, $results );

					$variant_row->handle_api_response( $variant_response, $results, false );
				}
			}
			
			return !$results->has_error;
		}
	}

	// SYNC FUNCTIONS

	/**
	 * Sync this parent up to Shopify
	 * 
	 * @param type|bool &$results 
	 * @param type|bool $force_sync 
	 * @param type|bool $resync 
	 * @return type
	 */
	public function shopify_sync( &$results = false, $force_sync = false, $resync = false ) {
		OVC_Results::maybe_init( $results );
		gc_enable();

		// If we're resyncing, save this option so we don't get stuck in a loop
		$this->resync = $resync;

		// Init row if it doesn't exist
		if( ( !$this->exists() || !$this->shopify_parent_id )
			&& ( $force_sync || $this->needs_sync( 'create' ) ) 
		) {
			// Insert the row into OVC
			$row_init = $this->update_data(
				array( 'sypas.ovc_parent_id' => $this->ovc_parent()->ID ),
				$results
			);

			if( !$row_init ) {
				return $results->error( "Error initializing shopify_parents row for Parent SKU: " . $this->ovc_parent()->parent_sku );
			}
			
			$shopify_parent_external_ds = $this->get_external_data_set( FS( 'shopify_parent' ) );

			// If this parent has more than 100 variants, we need to display an error
			$failed_check = OVC_Row_error_codes::check_ovc_product_error_code( 'sync_shopify_too_many_variants', $this->ovc_parent(), $results );
			if( !is_null( $failed_check ) && $failed_check !== true ) {
				return $results->error( 'Maximum amount of variants exceeded. Max allowed: 100, Amount submitted: '. count( $shopify_parent_external_ds['variants'] ) );
			}

			// Initialize images
			$this->set_ovc_shopify_images( $results );

			$this->sync_time = current_time( 'mysql', 1 );

			$request_data = array(
				'request_body'	=> array( 'product' => $shopify_parent_external_ds )
			);

			$api_response = OVC_API_Shopify::request( 'create_product', $request_data, $results );

			if( !( $results->success = $this->handle_api_response( $api_response, $results ) ) ) {
				return false;
			}
		}

		// Sync if needed
		if( $this->exists() 
			&& $this->shopify_parent_id
			&& ( $force_sync || $this->needs_sync( 'update' ) )
		) {

			$shopify_parent_external_ds = $this->get_external_data_set( FS( 'shopify_parent' ) );

			// If this parent has more than 100 variants, we need to display an error
			$failed_check = OVC_Row_error_codes::check_ovc_product_error_code( 'sync_shopify_too_many_variants', $this->ovc_parent(), $results );
			if( !is_null( $failed_check ) && $failed_check !== true ) {
				return $results->error( 'Maximum amount of variants exceeded. Max allowed: 100, Amount submitted: '. count( $shopify_parent_external_ds['variants'] ) );
			}

			// Refresh images
			$this->set_ovc_shopify_images( $results );

			$this->sync_time = current_time( 'mysql', 1 );

			$request_data = array(
				'url_params'	=> array(
					'shopify_product_id'	=> $this->shopify_parent_id
				),
				'request_body'	=> array( 'product' => $shopify_parent_external_ds )
			);

			$api_response = OVC_API_Shopify::request( 'update_product', $request_data, $results );

			return ( $results->success = $this->handle_api_response( $api_response, $results ) );
		}
	}

	public function force_shopify_sync( OVC_Results $results ) {
		
		return $this->shopify_sync( $results, true );
	}

	public function set_ovc_shopify_images( OVC_Results $results ) {

		// ovc_debug( memory_get_usage() . ' ' . __FUNCTION__ . ' Row ID: ' . $this->ID );

		if( !$this->exists() ) {
			return $results->error( 'Cannot execute set_ovc_shopify_images from an ovc_shopify_parents row that does not exist.' );
		}

		// Reset shopify_images children syimgs.positions to 0
		foreach( $this->get_children( FS( 'shopify_images' ) ) as $ovc_shopify_image ) { //dev:improve //dev:update_multi

			$ovc_shopify_image->update_data( 
				array( 'syimgs.position' => 0 )
			);
		}

		/// Determine image post_ids that should be synced to Shopify
		$image_ids = $this->get_shopify_parent_img_ids();

		// Get image sets of children that are set to sync to shopify
		$image_set_ids = $this->ovc_products( 'image_set', $results );

		foreach( $image_set_ids as $image_set_id ) {

			$image_set = OVCDB()->get_row( FS( 'image_sets' ), $image_set_id );

			$image_set_image_ids = array();
			// Override main image if override exists
			if( !( $main_img_id = $image_set->get_image_ids( array( 'marketplace_featured' ) ) ) ) {
				$main_img_id = $image_set->get_image_ids( array( 'main' ) );
			}
			$image_set_image_ids = array_merge( $main_img_id, $image_set->get_image_ids( 
				array( 
					'main',
					'swatch', 
					'gallery_ids', 
					'sizeguide' 
				), 
				false 
			) );

			$image_ids = array_merge( 
				$image_ids, 
				$image_set_image_ids
			);
		}

		$image_ids = array_values( array_unique( array_filter( $image_ids ) ) );

		// Create or Update shopify_images
		$position = 1;
		foreach( $image_ids as $image_id ) {

			// See if child exists
			$shopify_images_filter = array( 
				'var' 	=> '::[post_id]',
				'value' => $image_id
			);

			$shopify_image = $this->get_children( FS( 'shopify_images' ), 'object', $results, $shopify_images_filter );

			if( $shopify_image ) {
				$shopify_image = $shopify_image[0];
			}
			else {
				$shopify_image = OVCDB()->get_row( FS( 'shopify_images' ) );
			}

			// ovc_debug( __FUNCTION__ . ' - BEFORE update image ' . $image_id );

			$shopify_image->update_data(
				array(
					'post_id' => $image_id, 
					'shopify_parent_id' => $this->ID,
					'position' => $position
				),
				$results
			);

			// ovc_debug( __FUNCTION__ . ' - AFTER update image ' . $image_id );

			$position++;
		}

		// Delete any child shopify_images that still have a position of 0
		$delete_images_filter = array( 
			'var' 	=> '::[position]',
			'op'	=> '==',
			'value' => '0'
		);

		$delete_children = $this->get_children( FS( 'shopify_images' ), 'object', $results, $delete_images_filter );

		$child_deleted = false;
		foreach( $delete_children as $delete_child ) {
			if( $delete_child->delete( $results ) ) {
				$child_deleted = true;
			}
		}

		// Force refresh child_images
		$this->get_joined_data( FS( 'shopify_images' ), $results, true );
	}

	public function needs_sync( $sync_type ) {

		$id_to_check = 'create' == $sync_type ? $this->ovc_parent()->ID : $this->ID;

		if( !$id_to_check ) {
			return false;
		}

		$sync_type = "shopify_{$sync_type}_parents";

		$shopify_parents = OVC_Sync_Manager::get_auto_sync_count( $sync_type, true );

		return $shopify_parents ? in_array( $id_to_check, $shopify_parents ) : false;
	}

	/// FILTER FUNCTIONS
	public function filter_shopify_variants( $force_refresh = false ) {

		if( $force_refresh || !$this->shopify_variants ) {
			$ovc_products_shopify = $this->ovc_products();

			$shopify_variants = array();

			foreach( $ovc_products_shopify as $ovc_product ) {
				$shopify_variants[] = $ovc_product->get_external_data_set( FS( 'shopify_variant' ) );
			}

			$this->shopify_variants = $shopify_variants;
		}

		return (array) $this->shopify_variants;
	}

	public function filter_ovc_shopify_product_options() {

		$shopify_variants = $this->filter_shopify_variants();

		$options = array(
			array(
				'name' 		=> 'Color',
				'values'	=> array()
			),
			array(
				'name' 		=> 'Size',
				'values'	=> array()
			)
		);

		foreach( $this->filter_shopify_variants() as $variant ) {

			$options[0]['values'][] = $variant['option1'];
			$options[1]['values'][] = $variant['option2'];
		}

		$options[0]['values'] = array_values( array_unique( $options[0]['values'] ) );
		$options[1]['values'] = array_values( array_unique( $options[1]['values'] ) );

		return $options;
	} 
	
	public function filter_ovc_shopify_images( &$results = false ) {
		OVC_Results::maybe_init( $results );

		$ovc_shopify_images = $this->get_children( FS( 'shopify_images' ) );

		$api_shopify_images = array();

		foreach( $ovc_shopify_images as $ovc_shopify_image ) {
			$external_shopify_image_data = $ovc_shopify_image->get_external_data_set( FS( 'api_shopify_image' ), $results );

			// Remove product_id key from image data when sending with parent data
			if( isset( $external_shopify_image_data['product_id'] ) ) {
				unset( $external_shopify_image_data['product_id'] );
			}

			$api_shopify_images[] = $external_shopify_image_data;
		}

		return $api_shopify_images;
	}

	public function filter_ovc_shopify_descrip() {

		$description = htmlspecialchars( stripslashes( $this->data( 'pa.description' ) ) );
		//$sizing = '<div data-tab="Sizing">' . htmlspecialchars( stripslashes( $this->data( 'st.sizing' ) ) ) . '</div>';
		//$care = '<div data-tab="Care">' . htmlspecialchars( stripslashes( $this->data( 'st.care' ) ) ) . '</div>';

		return $description;
	}

	public function filter_ovc_shopify_title() {
		
		return stripslashes( $this->ovc_parent()->data( 'pa.product_title' ) );
	}

	public function filter_ovc_shopify_meta_title() {

		return $this->ovc_parent()->data( 'pa.brand' ) . " " . $this->filter_ovc_shopify_title();
	}
	public function filter_ovc_shopify_product_type() {

		$ovc_parent = $this->ovc_parent();

		$primary_category = '';

		$style_row = OVCDB()->get_row_by_valid_id( FS( 'style_data' ), 'st.style', $ovc_parent->data( 'pa.parent_sku' ) );

		/*
		 * If there's only one style, use that product category
		 */
		if( $style_row->exists() ) {

			$primary_category = $style_row->data( 'st.wc_product_category' );
		
		// Else we need to check the children
		} else {

			$primary_category = $ovc_parent->get_ovc_product_category();

			if( $primary_category ) {

				$primary_category = $primary_category->name;
			}
		}

		return $primary_category;
	}

	public function filter_ovc_shopify_tags() {

		$tags = array();
		$styles = array();

		foreach( $this->ovc_products() as $ovc_product ) {

			if( !in_array( $ovc_product->data('pr.sku_style' ), $styles ) ) {

				$styles[] = $ovc_product->data('pr.sku_style');

				// Keep track of these separately for now because they may have commas
				$tags[] = str_replace( ',', '', $ovc_product->data('st.wc_product_category') );

				$keywords = array_map( 'ucwords', array_map( 'trim', explode( ',', $ovc_product->data( 'st.keywords' ) ) ) );

				$parent_cat_tags = $this->get_shopify_parent_category_tags( $ovc_product->data( 'st.wc_product_category' ) );

				$tags = array_merge( $tags, $keywords, $parent_cat_tags );
			}

			$tags[] = "size_" . $ovc_product->data('pr.size');

			// Include Department
			$tags[] = $ovc_product->data( 'pr.department' );
			$tags[] = "department_" . $ovc_product->data( 'pr.department' );
			
		}
		
		return implode( ', ', array_unique( $tags ) );
	}

	/// HELPER FUNCTIONS
	public function get_shopify_parent_img_ids() {

		$img_ids = (array) $this->ovc_parent()->get_img_gallery_ids();

		$img_main_id = $this->ovc_parent()->img_marketplace_featured ?: $this->ovc_parent()->img_main;

		array_unshift( $img_ids, $img_main_id );

		$img_ids = array_unique( $img_ids );

		return $img_ids;
	}

	public function get_shopify_parent_category_tags( $primary_cat ) {
		
		$parent_category_tags = array();

		$primary_cat_obj = get_term_by( 'name', $primary_cat, 'product_cat' );

		if( $primary_cat_obj ) {

			$prefix = 'parent-category_';

			if( $primary_cat_obj->parent ) {

				$primary_cat_obj = get_term( $primary_cat_obj->parent );
			}

			$parent_category_tags[] = $primary_cat_obj->name;
			$parent_category_tags[] = $prefix . $primary_cat_obj->slug;

			if( 'Bras' == $primary_cat_obj->name ) {

				$parent_category_tags[] = 'Bra';
			} elseif( 'Tops' == $primary_cat_obj->name ) {

				$parent_category_tags[] = 'Top';
			}
		}

		return $parent_category_tags;
	}

	public function get_parent() {
		return $this->ovc_parent();
	}

	public function ovc_parent() {

		return $this->get_joined_data( FS( 'parent_data' ) );
	}

	public function get_variants() {

		$results = new OVC_Results;

		return $this->get_joined_data( FS( 'parent_data' ) )->get_children( 'ovc_products', 'raw', $results, array( 'conditions' => array( array( 'var' => '::[sync_shopify]', 'value' => '1' ), array( 'var' => '::[size]', 'op' => '!=', 'value' => 'CS' ) ), 'relation' => 'AND' ) );
	}

	public function ovc_products( $return_type = 'object', &$results = false, $shopify_children_filter = true ) {
		OVC_Results::maybe_init( $results );

		if( $shopify_children_filter ) {

			$shopify_children_filter = array(
				'var' => '::[sync_shopify]',
				'value' => 1
			);
		}

		return $this->ovc_parent()->get_children( FS( 'ovc_products' ), $return_type, $results, $shopify_children_filter );
	}
}