<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Row_ovc_products extends OVC_Row {

	protected $wc_variation = null;
	protected $wc_parent = null;

	protected $walmart_product_name = null;

	public function __get( $name ) {

		$value = parent::__get( $name );

		if( isset( $value ) ) {

			return $value;
		}
		else {

			// Magic Parent Product Data Properties
			if( in_array( $name, array( 'brand', 'wc_parent_id' ) ) ) {
				return $this->data( "pa.{$name}" );
			}
			// Magic Image ID properties (from image_set)
			else if( 0 === strpos( $name, 'img_' ) ) {
				$field = OVCSC::get_field( "img.{$name}" );
				if( $field ) {
					return $this->data( $field );
				}
			}
		}
	}

	// ACTIONS FUNCTIONS

	// HELPER FUNCTIONS

	public function get_parent() {
		//$pafs = OVCSC::get_field_set( 'parent_data' );
		return $this->get_joined_data( FS( 'parent_data' ) );
	}

	public function shopify_parent() {
		return $this->get_parent()->get_joined_data( 'shopify_parents' );
	}

	public function get_active_data_errors( $results = false, $force_refresh = false ) {

		// Maybe load active errors from preloaded cache via OVC_Row_data_errors
		if( !$force_refresh
			&& !isset( $this->joined_data[ 'data_errors' ] ) 
			&& OVC_Row_data_errors::active_errors_loaded()
		) {

			$where = array(
				'err.ovc_id'	=> $this->ID,
				'err.active'	=> 1
			);

			return $this->joined_data['data_errors'] = OVCDB()->get_loaded_rows_where( FS('data_errors'), $where );
		}
		
		return $this->get_children( FS('data_errors'), 'object', $results, true, $force_refresh );
	}

	public function get_active_data_error( $error_code, $results = false, $force_refresh = false ) {

		
		foreach( $this->get_active_data_errors( $results, $force_refresh ) as $data_error ) {

			if( $error_code == $data_error->data( 'errcd.error_code' ) ) {

				return $data_error;
			}
		}

		return null;
	}

	public function resolve_data_error( $error_code, $results = false, $force = false ) {

		if( !OVC_Row_data_errors::is_enabled() && !$force ) {
			return false;
		}

		OVC_Results::maybe_init( $results );

		$active_error = $this->get_active_data_error( $error_code, $results );

		return $active_error 
			? $active_error->resolve_error( $results )
			: false;
	}

	public function activate_data_error( $error_code, $error_data = null, $results = false, $force = false ) {
		
		if( !OVC_Row_data_errors::is_enabled() && !$force ) {
			return false;
		}

		$error_code = OVC_Row_error_codes::get_error_code( $error_code );

		if( !$error_code ) {
			return $results->error( 'Invalid error_code!' );
		}

		OVC_Results::maybe_init( $results );

		$error_action = 'updating';
		$active_error = $this->get_active_data_error( $error_code->error_code, $results );
		// Create new error
		if( !$active_error ) {

			$error_action = 'creating';
			$active_error = OVCDB()->new_row( FS('err'), 
				array(
					'err.errcd_id'	=> $error_code->ID,
					'err.ovc_id'	=> $this->ID,
					'err.active'	=> true,
					'err.error_data'=> maybe_serialize( $error_data )
				)
			);

			$result = $active_error->save( $results );

			if( $result ) {

				$this->joined_data[ 'data_errors' ][ $active_error->ID ] = $active_error;
			}
		}
		else if( isset( $error_data ) 
			&& $active_error->data( 'error_data' ) != maybe_serialize( $error_data ) 
		) {

			$updated_data['error_data'] = maybe_serialize( $error_data );
			$result = $active_error->update_data( $updated_data, $results );
		}

		if( !empty( $result ) ) {
			$results->notice( "ERROR CODE {$error_code->error_code} - OVC ID: {$this->ID} - " . $error_code->description );
		}
		return $result ?? true;
	}

	/*
	 * Assembly Functions
	 */
	
	/**
	 * Get the OVC Assemblies attached to this product
	 * 
	 * @param 	OVC_Results|bool 	$results
	 * 
	 * @return 	array
	 */
	public function get_assemblies( $results = false ) {

		return $this->get_joined_data( FS( 'asm' ), $results );
	}

	/**
	 * Get the OVC Assembly items attached to this product
	 * 
	 * @param 	OVC_Results|bool 	$results
	 * 
	 * @return 	array
	 */
	public function get_assembly_items( $results = false ) {

		return $this->get_joined_data( FS( 'asmi' ), $results );
	}

	/**
	 * Get the Parent Products Assemblies and Assembly Items
	 * 
	 * @param 	string 				$return_type 
	 * @param 	OVC_Results|bool 	$results
	 * 
	 * @return 	array
	 */
	public function get_parent_children_and_assemblies( $return_type = 'raw', $results = false ) {
		return $this->get_parent()->get_children_and_assemblies( $return_type = 'raw', $results );
	}

	public function get_potential_stock( $called_by = false, $use_in_stock_quantity = false, $results = false ) {

		$potential_stock = 0;
		
		foreach( $this->get_assembly_items() as $assembly_item ) {

			if( !$called_by || $called_by->ID != $assembly_item->ID ) {
				$potential_stock += $assembly_item->get_potential_stock( $results );
			}
			
		}

		foreach( $this->get_assemblies() as $assembly ) {

			if( !$called_by || $called_by->data( 'asmi.assembly_id' ) != $assembly->ID ) {
				$potential_stock += $assembly->get_potential_stock( $results );
			}
		}

		if( $use_in_stock_quantity ) {
			$potential_stock += $this->data( 'pr.stock' );
		} else {
			$potential_stock += $this->data( 'pr.avail_qty' );
		}

		return $potential_stock;
	} 

	/**
	 * Return whether this product is unattached from assemblies or assembly items
	 * 
	 * @return 	bool
	 */
	public function is_assembly_member() {
	// public function is_unattached() {

		return !empty( $this->get_assembly_items() ) || !empty( $this->get_assemblies() );
	}

	/**
	 * Return whether this product is a top level assembly
	 * 
	 * @return 	bool
	 */
	public function is_top_level_assembly() {

		return !empty( $this->get_assemblies() ) && empty( $this->get_assembly_items() );
	}

	/**
	 * Return the Max Depth of assemblies and assembly items attached to this product
	 * 
	 * @return 	int|null
	 */
	public function get_max_depth() {

		if( !$this->is_assembly_member() ) {
			return null;
		} elseif( $this->is_top_level_assembly() ) {
			return 0;
		} else {

			return max( array_map( function( $assembly_item ) {

				return $assembly_item->get_max_depth();
			}, $this->get_assembly_items() ) );
		}
	}


	/**
	 * 	EVERYTHING BELOW HERE NEEDS TO BE ADAPTED TO NEW OVC_Row CLASS
	 * 
	 **/

	

	// GENERAL SYNC-RELATED FUNCTIONS //


	public function needs_sync( $type = 'oms' ) {

		if( !$this->sync_oms ) {
			return false;
		}

		$query_results = OVC_Sync_Manager::get_auto_sync_count( "{$type}_sync" );

		return $query_results ? in_array( $this->ID, $query_results ) : false;
	}

	/**
	 * WooCommerce-related methods
	 */

	// Get the associated WC Variation as a WC_Product_Variation object
	public function wc_variation( $force_refresh = false ) {
		if ( ( !$this->wc_variation || $force_refresh  ) && $this->post_id ) {
			$this->wc_variation = wc_get_product( $this->data( 'pr.post_id' ) );
		}
		return $this->wc_variation;
	}

	// Get the assocaited WC Variable Parent as a WC_Product_Variable object
	public function wc_parent( $force_refresh = false ) {
		if ( !$this->wc_parent || $force_refresh ) {
			$this->wc_parent = wc_get_product( $this->data( 'pa.wc_parent_id' ) );
		}
		return $this->wc_parent;
	}

	// Check if this product should be in WooCommerce
	public function should_be_wc() {
		return ( $this->data( 'pr.sync_oms' ) && $this->data( 'pr.sync_wc' ) ? true : false );
	}

	// OVC Custom Stock settings for WC
	public function ovc_set_wc_stock( $results = false ) {
		OVC_Results::maybe_init( $results );

		if( $this->post_id && $this->wc_variation() instanceof WC_Product_Variation ) { // DEV IMPROVE
			$current_stock = $this->wc_variation()->get_stock_quantity();

			if( $current_stock && !$this->should_be_wc() ) {
				$results->notice( "OVC ID: {$this->ID}, SKU: {$this->sku} has WC stock but should not be available.  Forcing stock to zero." );
			}

			$stock = $this->should_be_wc() ? $this->get_online_stock() : 0 ;

			$this->wc_variation()->set_stock( $stock );

			WC_Product_Variable::sync_stock_status( $this->wc_parent_id );

			return true;
		}
		return false;
	}

	// Validation Functions

	/**
	 * Validate that Dozens is equal to:
	 * 	Case: Pack Quantity
	 * 	Else: Pack Quantity / 12
	 * 
	 * @param 	OVC_Results|bool 	$results
	 * 
	 * @return 	bool
	 */
	public function validate_dozens( $results = false ) {
		OVC_Results::maybe_init( $results );

		$dozens = $this->data( 'pr.dozens' );

		if( !is_numeric( $dozens ) ) {
			return $results->error( "Invalid Dozens: Dozens must be numeric." );
		}

		if( 'CS' === $this->data( 'pr.size' ) ) {

			if( $this->data( 'pr.sku_pkqty' ) != $dozens ) {
				return $results->error( "Invalid Dozens: Case Product Dozens must be equal to the pack quantity." );
			}
		}
		elseif( ( $this->data( 'pr.sku_pkqty' ) / 12 ) == $dozens ) {
			return $results->error( "Invalid Dozens: Dozens must be equal to the pack quantity divided by 12." );
		}

		return true;
	}

	/**
	 * Validate that Units per Case is equal to
	 * 	Case: 1
	 * 	Else: Case.dozens / This.dozens
	 * 
	 * @param 	OVC_Results|bool 	$results
	 * 
	 * @return 	bool
	 */
	public function validate_units_per_case( $results = false ) {
		OVC_Results::maybe_init( $results );

		$loaded_rows = OVCDB()->load_rows_where( FS( 'ovc_products' ), array( 'parent_sku' => $this->data( 'pr.parent_sku' ) ), $results );

		$units_per_case = $this->data( 'pr.unit_per_case' );

		if( !is_int( $units_per_case ) ) {
			return $results->error( "Invalid Units/Case: Units/Case must be an integer." );
		}

		// If we're a case, we need to be 1
		if( 'CS' === $this->data( 'pr.size' ) ) {

			if( 1 != $units_per_case ) {
				return $results->error( "Invalid Units/Case: Case Product Units/Case must be 1" );
			}
		}
		else {

			$cases = OVCDB()->get_loaded_rows_where( FS( 'ovc_products' ), array( 'case_sku' => $this->data( 'pr.case_sku' ), 'size' => 'CS' ) );

			foreach( $cases as $case ) {

				if( ( $case->data( 'pr.dozens' ) / $this->data( 'pr.dozens' ) ) != $units_per_case ) {
					return $results->error( "Invalid Units/Case: Units/Case must be equal to ( Case Dozens / Product Dozens )." );
				}
			}
		}

		return true;
	}

	/**
	 * Validate that Units per Box adds up to all children Units per Case
	 * 	Case: 1
	 * 	Else: Case - Siblings.units/box
	 * 
	 * @param 	OVC_Results|bool 	$results
	 * 
	 * @return 	bool
	 */
	public function validate_units_per_box( $results = false ) {
		OVC_Results::maybe_init( $results );

		$loaded_rows = OVCDB()->load_rows_where( FS( 'ovc_products' ), array( 'parent_sku' => $this->data( 'pr.parent_sku' ) ), $results );

		$units_per_box = $this->data( 'pr.unit_per_box' );

		if( !is_int( $units_per_box ) || !( is_numeric( $units_per_box ) && intval( $units_per_box ) == $units_per_box ) ) {
			return $results->error( "Invalid Units/Box: Units/Box must be an integer." );
		}

		// If we're a case, we need to be 1
		if( 'CS' === $this->data( 'pr.size' ) ) {

			if( 1 != $units_per_box ) {
				return $results->error( "Invalid Units/Box: Case Product Units/Box must be 1" );
			}
		}
		// Else we need to equal ( Unit/Case - All other children Units/Box )
		else {

			$siblings = OVCDB()->get_loaded_rows_where( FS( 'ovc_products' ), array( 'case_sku' => $this->data( 'pr.case_sku' ) ) );

			$case_dozens = 0;
			$sibling_units_per_box = 0;

			foreach( $siblings as $sibling ) {

				if( 'CS' === $sibling->data( 'pr.size' ) ) {

					$case_dozens = $sibling->data( 'pr.dozens' );
				} elseif( $sibling->ID == $this->ID ) {
					continue;
				} else {

					$sibling_units_per_box += $sibling->data( 'pr.unit_per_box' );
				}


			}
		}

		return true;
	}

	// Other Functions

	public function get_online_stock( $zero_if_negative = true ) {

		$quantity = $this->data( 'pr.avail_qty' );
		if( $this->data( 'pr.use_potential_inventory' ) && !is_null( $this->data( 'pr.potential_qty' ) ) ) {
			$quantity = $this->data( 'pr.potential_qty' );
		}
		$stock = intval( $quantity - $this->data( 'pr.reserve_qty' ) );

		if( $zero_if_negative && $stock < 0 ) {
			$stock = 0;
		}

		return $stock;
	}

	public function get_pack_string( $return_pieces = false, $force_build = false, $parentheses = true ) {

		$pk_str_pieces = array();

		if( $force_build
			|| $this->sku_pkqty > 1
			|| 'CS' == $this->size 
			|| 'Set' == $this->pk_str 
		) {
			$pk_qty = 'CS' == $this->size || 'Set' == $this->pk_str ? 1 : $this->sku_pkqty; 

			// Maybe Pluralize Pack String (if it's not 'Pack')
			$pk_str = rtrim( $this->pk_str, 's' );
			$pk_str = $pk_qty > 1 && 'Pack' != $pk_str ? $pk_str . 's' : $pk_str;

			

			$pk_str_pieces = array(
				'pk_qty'	=> $pk_qty,
				'pk_str'	=> $pk_str
			);

		}

		if( $return_pieces ) {
			return $pk_str_pieces;
		}
		else {

			$pack_string = count( $pk_str_pieces ) ? "{$pk_str_pieces['pk_qty']} {$pk_str_pieces['pk_str']}" : '';

			$pack_string = ( $pack_string && $parentheses  ) ? "({$pack_string})" : $pack_string;

			return $pack_string;
		}
	}

	public function get_zulily_pack_string() {
		$pack_string = $this->get_pack_string( false, true, false );

		$pack_string = str_replace( ' ', '-', strtolower( $pack_string ) );

		return $pack_string;
	}

	public function get_best_parent_title_text() {
		$parent_title = htmlspecialchars( stripslashes( $this->data( 'pa.product_title' ) ) );

		if( !$parent_title ) {
			$parent_title = htmlspecialchars( stripslashes( $this->data( 'st.product_title' ) ) );
		}

		return $parent_title ? $parent_title : false;
	}

	/**
	 * Color-related methods
	 */

	public function on_update_sku_color_get_color_long_name( $results = false ) {
		OVC_Results::maybe_init( $results );

		$where_data = array(
			'pr.parent_sku' => $this->new_value( 'pr.parent_sku' ),
			'pr.sku_color'	=> $this->new_value( 'pr.sku_color' )
		);

		$table_data = OVCDB()->get_table_data( $this->field_set, $where_data, $results );

		$new_color_long_name = $table_data[0]['color_long_name'];

		$this->set_new_data( array( 'color_long_name' => $new_color_long_name ) );

		$results->notice( "Updated color_long_name to: {$new_color_long_name}" );
	}

	public function update_siblings_color_long_name( $results = false ) {
		OVC_Results::maybe_init( $results );

		global $wpdb;

		$sku_color_siblings = $wpdb->get_col( "SELECT ID FROM wp_ovc_products WHERE parent_sku = '{$this->data( 'pr.parent_sku')}' AND sku_color = '{$this->new_value('pr.sku_color')}' AND ID != {$this->ID}" );

		if( $sku_color_siblings ) {

			$this->set_new_data( array( 'color_long_name' => $this->new_value('pr.color_long_name') ) );

			$update_data = array( 
				'color_long_name' => $this->new_value('color_long_name')
			);

			$where_data = array(
				'parent_sku' 	=> $this->parent_sku,
				'sku_color'		=> $this->new_value('sku_color')
			);

			if( false !== $wpdb->update( 'wp_ovc_products', $update_data, $where_data, '%s', '%s' ) ) {

				$results->updated_ids = $sku_color_siblings;

				$results->notice( "Auto-updating color_long_name of sibling SKUs with matching sku_color of {$this->new_value('sku_color')}" );
			}
			else {
				$results->error( "Error updating color_long_name of sibling SKUs with matching sku_color of {$this->new_value('sku_color')}" );
			}
		}
	}

	public function update_siblings_color_full_names( $results = false ) {
		$this->get_parent()->update_children_color_full_names( $results );
	}

	public function set_ovc_color_name( $results = false ) {
		OVC_Results::maybe_init( $results );

		$this->set_new_data( array( 'color' => $this->get_new_ovc_color_name() ) );
	}

	public function get_new_ovc_color_name() {
		return $this->new_value('sku_color') . '-' . $this->new_value('sku_pkqty') . $this->data( 'st.oms_release_code' );
	}


	public function get_best_color_name() {
		return ( strlen( $this->color_long_name ) ? $this->color_long_name : $this->sku_color );
	}

	public function get_best_color_long_name() {
		return $this->new_value( 'color_long_name' ) ? $this->new_value( 'color_long_name' ) : ucwords( strtolower( str_replace( '-', ' ', $this->sku_color ) ) );
	}

	public function get_best_swatch_id( $results = false ) {

		if( $this->data( 'img.img_swatch' ) ) {
			return $this->data( 'img.img_swatch' );
		}
		else if( $this->data( 'img.img_main' ) ) {
			return $this->data( 'img.img_main' );
		}
		return 0;
	}

	public function get_full_size_name() {
		$size_slug = 'size-' . strtolower( $this->data( 'pr.size' ) );
		$size_term = get_term_by( 'slug', $size_slug, 'pa_size' );
		return ( $size_term === false ) ? false : $size_term->name;
	}

	public function get_material_content_array( $auto_concat_limit = false ) {
		$material_content = array();

		for( $i = 1; $i <= 5; $i++ ) {

			$material_field_prefix = "st.material{$i}";

			if( strlen( $this->data( "{$material_field_prefix}_name" ) )
				 && intval( $this->data( "{$material_field_prefix}_percent" ) )
			) {

				$material_content[ $this->data( "{$material_field_prefix}_name" ) ] = intval( $this->data( "{$material_field_prefix}_percent" ) );
			}
		}

		// Order materials from highest to lowest
		arsort( $material_content );

		// Maybe combine some materials if $auto_concat_limit is set and is less than the number of materials
		if( is_int( $auto_concat_limit )
			&& count( $material_content ) > $auto_concat_limit
		) {
			$auto_concat_material_names = array();
			$auto_concat_material_percent = 0;

			// The shortened array of material content
			$concat_material_content = array();

			$material_count = 0;
			foreach( $material_content as $material_name => $material_percent ) {
				$material_count++;

				if( $material_count < $auto_concat_limit ) {
					$concat_material_content[ $material_name ] = $material_percent;
				}
				else {
					$auto_concat_material_names[] = $material_name;
					$auto_concat_material_percent += $material_percent;
				}
			}

			$concat_material_content[ implode( ', ', $auto_concat_material_names ) ] = $auto_concat_material_percent;
			$material_content = $concat_material_content;
		}

		return $material_content;
	}

	public function get_material_string() {

		$material_content_array = $this->get_material_content_array();
		$material_content_strings = array();

		foreach( $material_content_array as $material_name => $material_percent ) {
			
			$material_content_strings[] = "{$material_percent}% {$material_name}";
		}

		return implode( ', ', $material_content_strings );
	}

	/*
	 * Filter Functions
	 */

	public function filter_walmart_img_alt_text( $img_type_str = '' ) {

		$img_alt_text = $this->filter_walmart_product_name() . ', ' . $this->get_best_color_name();

		if( $img_type_str ) {
			$img_alt_text .= " - {$img_type_str}";
		} 

		return $img_alt_text;
	}

	// Get Walmart Shelf Description Bullet points
	public function filter_walmart_shelf_descrip() {

		$shelf_descrip = '';
		$shelf_descrip_fields = array( 'st.fast_facts1', 'st.fast_facts2', 'st.sizing' );
		$shelf_descrip_bullets = array();

		foreach( $shelf_descrip_fields as $field_name ) {
			if( $this->data( $field_name ) ) {
				$shelf_descrip_bullets[] = '<li>' . htmlspecialchars( stripslashes( $this->data( $field_name ) ) ) . '</li>';
			}
		}

		if( count( $shelf_descrip_fields ) ) {
			$shelf_descrip = '<ul>' . implode( '', $shelf_descrip_bullets ) . '</ul>';
		}

		return $shelf_descrip;
	}

	public function filter_walmart_short_descrip() {

		$short_descrip = htmlspecialchars( stripslashes( strval( $this->data( 'st.description' ) ) ) );

		return $short_descrip;
	}

	public function filter_walmart_long_descrip() {

		$long_descrip = '';

		if( $this->img_swatch ) {
			$long_descrip .= '<h3>SIZE GUIDE: <br />Please refer to the additional product images for the ' . $this->brand . ' ' . $this->data( 'st.clothing_type' ) . ' Size Guide</h3>';
		}

		$long_descrip .= $this->filter_walmart_shelf_descrip();

		return $long_descrip;
	}

	public function filter_walmart_tax_code() {
		// Two options for tax code based on whether product is meant for adult or kids

		$tax_code = '2038356'; // Adult tax code

		if( in_array( $this->department, array( 'Boys', 'Girls', 'Unisex-Kids' ) ) ) {
			$tax_code = '2040259';
		}

		return $tax_code;
	}

	public function filter_walmart_attribute_size_name() {
		$attribute_size_name = 'clothingSize';

		if( 'Bras' == $this->data( 'st.clothing_type' ) ) {
			$attribute_size_name = 'braSize';
		}
		else if( 'Socks' == $this->data( 'st.clothing_type' ) ) {
			$attribute_size_name = 'sockSize';
		}
		else if( 'Panties' == $this->data( 'st.clothing_type' ) ) {
			$attribute_size_name = 'pantySize';
		}

		return $attribute_size_name;
	}

	public function filter_walmart_bra_size_part( $bra_size_part ) {

		$size_code = $this->size;

		$bra_band_size = substr( $size_code, 0, 2 );

		if( !is_numeric( $bra_band_size )
			|| !in_array( $bra_size_part, array( 'cup', 'band' ) )
		) {
			return $this->get_full_size_name();
		}
		else if ( 'band' == $bra_size_part ) {
			return $bra_band_size;
		}
		else if( 'cup' == $bra_size_part ) {

			$bra_cup_size = substr( $size_code, 2 );

			// Get rid of panty size on bra/panty set sizes
			$bra_cup_size = explode( '-', $bra_cup_size );
			return $bra_cup_size[0];
		}
		else {
			return $this->get_full_size_name();
		}
	}

	public function filter_walmart_gender() {
        $gender = $this->data( 'pr.department' );
        $filtered_gender = '';

        if( $gender == 'Baby' || $gender == 'Unisex-Kids' || $gender == 'Unisex-Adult' ){
            $filtered_gender = 'Unisex';
        }

        if( $gender == 'Women' || $gender == 'Girls' ){
            $filtered_gender = 'Female';
        }

        if( $gender == 'Men' || $gender == 'Boys' ){
            $filtered_gender = 'Male';
        }

        return $filtered_gender;
		//return 0 === strpos( $this->data( 'pr.department' ), 'Unisex' ) ? 'Unisex' : $this->data( 'pr.department' );
	}

	public function filter_ovc_shopify_image_id() {

		$shopify_image_id = $this->data('sypr.shopify_image_id');

		if( !$shopify_image_id ) {
			global $wpdb;

			$img_post_id = $this->data('img.img_main');

			$shopify_parent = $this->shopify_parent();

			if( $img_post_id && $shopify_parent->ID ) {
				$ovc_shopify_image_id = $wpdb->get_var( "SELECT ID FROM {$wpdb->prefix}ovc_shopify_images WHERE post_id = {$img_post_id} AND shopify_parent_id = {$shopify_parent->ID}" );
				
				if( $ovc_shopify_image_id ) {
					$shopify_image_row = OVCDB()->get_row( FS( 'shopify_images' ), $ovc_shopify_image_id ); 

					if( $shopify_image_row->exists() ) {
						$shopify_image_id = $shopify_image_row->shopify_image_id;
					}
				}	
			}
		}

		return $shopify_image_id;
	}

	public function filter_main_ovc_image_url() {

		$img_post_id = $this->data( 'img.img_main' );

		if( $img_post_id ) {

			$main_img = OVCDB()->get_row_by_valid_id( FS( 'ovc_images' ), 'post_id', $img_post_id );

			if( $main_img ) {
				
				return $main_img->filter_ovc_image_url();
			}
		}

		return wp_get_attachment_url( $img_post_id );
	}


}