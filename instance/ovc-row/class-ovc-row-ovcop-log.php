<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Row_ovcop_log extends OVC_Row {

	public function get_op_files() {
		global $wpdb;

		return $wpdb->get_results(
			$wpdb->prepare(
				"
					SELECT opfile.*
					FROM {$wpdb->prefix}ovcop_files opfile
					WHERE opfile.op_id = %d
				",
				$this->ID
			)
		);
	}

	public function get_log_file() {

		$op_files = $this->get_op_files();

		foreach( $op_files as $op_file ) {

			if( 'log' === $op_file->file_type ) {
				return new OVC_File( $op_file->ID );
			}
		}

		return false;
	}

	public function is_stuck() {

		if( $this->data( 'ol.last_seen' ) <= date( 'Y-m-d H:i:s', strtotime( '-5 minutes' ) ) ) {
			return true;
		}

		return false;
	}

	public function archive_ovcop( $save_files = true, $force = false ) {
		global $wpdb;

		if( $this->data( 'ol.archived' ) && !$force ) {
			return true;
		}

		$ovcop_files = $this->get_op_files();

		foreach( $ovcop_files as $ovcop_file ) {

			$ovc_file = new OVC_File( $ovcop_file->ID );
			$ovc_file->archive_file( $save_files, $force );
		}

		$ovcop_meta = array(
			'archived'		=> '1'
		);

		return $wpdb->update( 'wp_ovcop_log', $ovcop_meta, array( 'ID' => $this->ID ) );
	}
}