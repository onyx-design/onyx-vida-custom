<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Row_data_errors extends OVC_Row {

	protected static $enabled = false;
	protected static $active_errors = null;

	public static function enable( $enable = true ) {
		static::$enabled = (bool) $enable;
	}

	public static function is_enabled() {
		return static::$enabled;
	}

	public static function load_active_errors( $force_refresh = false ) {

		if( $force_refresh || !static::active_errors_loaded() ) {

			static::$active_errors = OVCDB()->load_rows_where( FS('err'), array( 'active' => 1 ) );

			update_option( 'ovc_active_data_errors', count( static::$active_errors ) );
		}

		return static::$active_errors;
	}

	public static function active_errors_loaded() {

		return isset( static::$active_errors );
	}

	public static function get_active_general_data_error( $error_code, $results = false, $force_refresh = false ) {
		OVC_Results::maybe_init( $results );

		if( $error_code = OVC_Row_error_codes::get_general_error_code( $error_code, $results ) ) {

			$where = array(
				'err.errcd_id'	=> $error_code->ID,
				'err.active'	=> 1
			);

			if( !$force_refresh && static::active_errors_loaded() ) {

				$active_errors = OVCDB()->get_loaded_rows_where( FS( 'err' ), $where );
			}
			else {

				$active_errors = OVCDB()->load_rows_where( FS( 'err' ), $where, $results );
			}

			if( $active_errors ) {

				if( count( $active_errors ) > 1 ) {

					$results->notice( "WARNING! There are multiple active general data_errors for Error Code {$error_code->error_code}" );
				}

				return current( $active_errors );
			}
		}
		
		return null;
	}

	public static function activate_general_error( $error_code, $error_data = null, OVC_Results $results, $force = false ) {

		if( !OVC_Row_data_errors::is_enabled() && !$force ) {
			return false;
		}

		if( $error_code = OVC_Row_error_codes::get_general_error_code( $error_code, $results ) ) {

			$active_error = static::get_active_general_data_error( $error_code, $results, $force );

			if( !$active_error ) {

				$active_error = OVCDB()->new_row( FS('err'), 
					array(
						'err.errcd_id'	=> $error_code->ID,
						'err.ovc_id'	=> 0,
						'err.active'	=> true,
						'err.error_data'=> maybe_serialize( $error_data )
					)
				);

				$result = $active_error->save( $results );
			}
			else if( isset( $error_data )
				&& $active_error->error_data != maybe_serialize( $error_data ) 
			) {

				$updated_data['error_data'] = maybe_serialize( $error_data );
				$result = $active_error->update_data( $updated_data, $results );
			} 

			if( !empty( $result ) ) {
				$results->notice( "Activated general Error Code {$error_code->error_code} - " . $error_code->description );
			}
		}

		return $result ?? true;
	}

	public function resolve_error( $results = false, $resolved_by = false ) {

	 	if( $this->data( 'active' ) ) {

	 		$updated_data = array( 
	 			'active'	=> 0, 
	 			'date_resolved' => current_time( 'mysql', 1 ) 
	 		);

 			$updated_data['resolved_by_user'] = $resolved_by ? ( true === $resolved_by ) ? get_current_user_id() : $resolved_by : 0;

	 		if( $this->update_data( $updated_data, $results ) ) {
	 			$results->notice( "Error Code {$this->data('error_code')} resolved for OVC ID {$this->data('ovc_id')}" );
	 			return true;
	 		}
	 	}

		return false;
	}

	public function get_error_code( $results = false ) {
		return $this->get_joined_data( FS( 'errcd' ) );
	}

	public function get_product( $results = false ) {
		return $this->ovc_id ? $this->get_joined_data( FS( 'pr' ) ) : null;
	}

	public function check_error( $results = false ) {
		

		if( !$this->data( 'active' ) ) {

			OVC_Results::maybe_init( $results );
			return $results->error( "Cannot check error that is already resolved" ); // dev:improve - should this maybe work somehow? Maybe auto-create a new error of same type?
		}
		else {

			$error_code = $this->get_error_code();

			if( !$error_code->single_resource ) {

				OVC_Row_error_codes::check_general_error_code( $error_code, $results );
			}
			else if( $ovc_product = $this->get_product( $results ) ) {

				$error_code->check_ovc_product( $ovc_product, $results );
			}
		}
	}
}
