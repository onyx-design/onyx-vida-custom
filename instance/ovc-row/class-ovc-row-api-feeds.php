<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Row_api_feeds extends OVC_Row {

	protected $feed_file;

	protected static $walmart_error_codes = false;

	public static function init_feed( $feed_type = '', $file_id = 0, $op_id = 0, &$results = false ) {
		OVC_Results::maybe_init( $results );

		// Auto-init feed file if not supplied
		if( !$file_id ) {

			$file_name = 'USER' . get_current_user_id() . '-' . strtoupper( $feed_type ) . '-' . date( 'Ymd_His') . '.xml';
			$feed_file = OVC_File::init_file( $file_name, $feed_type );
			$file_id  = $feed_file->ID;
		}

		$api_feed_data = array(
			'feed_type'	=> $feed_type,
			'status'	=> 'INIT',
			'op_id'		=> (int) $op_id,
			'file_id'	=> (int) $file_id
		);

		$api_feed = OVCDB()->new_row( FS( 'api_feeds' ), $api_feed_data );

		if( $api_feed->save( $results ) ) {
			return $api_feed;
		}
	}

	public function get_ingestion_errors() {

		return maybe_unserialize( $this->ingestion_errors );
	}

	// FEED FILE HANDLING FUNCTIONS
	public function get_feed_file( $force_refresh = false ) {

		if( $this->file_id && ( $force_refresh || !$this->feed_file instanceof OVC_File ) ) {
			$this->feed_file = new OVC_File( $this->file_id );
		}

		return $this->feed_file;
	}

	public function write_feed_to_file( $ovc_ids = null, &$results = false ) {
		OVC_Results::maybe_init( $results );
		$this->data_set();

		if( !$this->exists() ) {
			return false;
		}
		// If no file associated with feed, create one
		else if( !$this->get_feed_file() ) {

			$file_name = $this->feed_type . '-' . date( 'Ymd_His') . '.xml';
			$this->feed_file = OVC_File::init_file( $file_name, $this->feed_type, $this->op_id );
			$this->file_id = $this->feed_file->ID;

			$this->update_data( array( 'file_id' => $this->file_id ), $results );
		}

		// Maybe get $ovc_ids
		if( !is_array( $ovc_ids ) ) {
			$ovc_ids = OVC_Sync_Manager::get_auto_sync_count( $this->feed_type, true );
		}

		$this->feed_file->erase_file();

		$template_feed_file = 'walmart_bulk_update_manual' === $this->feed_type ? 'walmart_bulk_update_items' : $this->feed_type;

		$feed_template_file = OVC_PATH . "/templates/walmart-api/{$this->feed_type}.php";

		if( !file_exists( $feed_template_file ) ) {
			return $results->error( "Cannot locate feed template file for feed type: {$this->feed_type}" );
		}

		ob_start();

		// REQUIRES $ovc_ids
		include $feed_template_file;

		$xml_contents = ob_get_clean();

		// Create XML
		$xmlDoc = new DOMDocument();
		$xmlDoc->preserveWhiteSpace = false;
		$xmlDoc->formatOutput = true; // Format the XML nicely

		if( $xmlDoc->loadXML( $xml_contents ) ) {
			
			$xmlDoc->save( $this->feed_file->path() );
		}
		else {

			$results->error( "ERROR! API FEED {$this->ID} - There was an error trying to write the XML to the feed file (File ID {$this->feed_file->ID})" );
			// If error trying to write the xml, save the xml contents for review
			$this->feed_file->write( $xml_contents );
		}
		

		$this->feed_file->complete_file();

		return !$results->has_error;
	}

	// WALMART API FUNCTIONS
	public function api_refresh( &$results = false ) {

		return $this->refresh_walmart_feed_items_status( $results );
	}

	public function submit_feed( &$results = false, $force_refresh = false ) {
		OVC_Results::maybe_init( $results );
		$this->data_set();

		if( 'INIT' != $this->status && !$force_refresh ) {

			return $results->error( "Cannot submit feed ID {$this->ID}. OVC Feed status is {$this->status}" );
		}
		else if( $feed_file = $this->get_feed_file() ) {

			$feed_type = 'walmart_bulk_update_manual' === $this->feed_type ? 'walmart_bulk_update_items' : $this->feed_type;

			$response = OVC_API_Walmart::request( $feed_type, array( 'file' => $feed_file ), $results );

			$response_data = $response->format();

			$updated_feed_data = array(
				'_meta_submitted'	=> current_time( 'mysql', 1 )
			);

			ovc_debug( $response_data );

			if( $response->succeeded() ) {

				
				$remote_feed_id = $response_data['feedId'];

				$updated_feed_data['status'] = 'SUBMITTED';
				$updated_feed_data['remote_feed_id'] = $remote_feed_id;

				$results->notice( "Feed [{$this->feed_type}] submitted successfully. ID: {$this->ID}, Remote ID: {$response_data['feedId']}" );	
			}
			else {

				$updated_feed_data['status'] = 'SUBMITTED';	
				$results->error( "ERROR submitting [{$this->feed_type}] feed: " . print_r( $response, true ) );
			}

			$this->update_data( $updated_feed_data, $results );

			return $response;

			// $feed_status = $response->succeeded() ? 'SUBMITTED' : 'SUBMIT_FAILED';

			// if( $response->succeeded() ) {

			// 	$updated_feed_data = array(
			// 		'remote_feed_id'	=> $response['feedId'],
			// 		'status'			=> 'SUBMITTED',
			// 		'_meta_submitted'	=> current_time( 'mysql', 1 )
			// 	);
			// 	$this
			// }

			// if( $ ) {

			// 	$results->notice( "Feed [{$this->feed_type}] submitted successfully. ID: {$this->ID}, Remote ID: {$response['feedId']}" );

			// 	$updated_feed_meta = array(
			// 		'remote_feed_id'	=> $response['feedId'],
			// 		'status'			=> 'SUBMITTED',
			// 		'_meta_submitted'	=> current_time( 'mysql', 1 )
			// 	);

			// 	if( !$this->update_feed_meta( $updated_feed_meta ) ) {
			// 		return $results->error( "ERROR: Unable to update API feed meta information." );
			// 	}
			// 	return true;
			// }
			// else {
			// 	return $results->error( "ERROR submitting [{$this->feed_type}] feed: " . print_r( $response, true ) );
			// }
		}
		else {
			return $results->error( "Invalid feed meta or feed file is missing." );
		}
	}

	public function refresh_walmart_feed_items_status( &$results = false ) {
		OVC_Results::maybe_init( $results );
		$this->data_set();

		$limit = 1000;

		$request_data = array(
			'url_params'	=> array(
				'remote_feed_id'	=> $this->remote_feed_id
			),
			'query_params'	=> array(
				'includeDetails'	=> 'true',
				'limit'				=> $limit,
				'offset'			=> 0
			)
		);

		$looping = true;
		$feed_items_updated = 0;

		while( $looping === true ) {

			$response = OVC_API_Walmart::request( 'get_feed_items_status', $request_data, $results );

			if( !$response->succeeded() ) {

				// Don't mark the feed as feed error if it was submitted less than 5 minutes ago
				if( $this->data( 'feed._meta_submitted' ) >= date( 'Y-m-d H:i:s', strtotime( '-5 minutes' ) ) ) {

					$results->notice( "Error retrieving feed items status for API Feed (ID: {$this->ID}) – will retry becasue this feed was submitted less than 5 minutes ago" );
				}
				else {
					$results->notice( "Error retrieving feed items status. API Feed (ID: {$this->ID}) may be outdated. \r\n" . print_r( $response->get_errors(), true ) );

					$this->update_data( array( 'status' => 'FEED_ERROR' ), $results );	
				}

				$looping = false;
				break;
			}
			else {

				$response_data = $response->format();
				$this->handle_api_response( $response_data, $results );

				$feed_items_received = intval( $response_data['itemsReceived'] );

				if( isset( $response_data['itemDetails'] )
					&& !empty( $response_data['itemDetails']['itemIngestionStatus'] )
				) {

					// Check to make sure single hanging itemIngestionStatus have the numerically-indexed array layer //dev:improve
					if( !isset( $response_data['itemDetails']['itemIngestionStatus'][0] ) ) {

						$response_data['itemDetails']['itemIngestionStatus'] = array(
							$response_data['itemDetails']['itemIngestionStatus']
						);
					}

					// Update feed_items details
					foreach( $response_data['itemDetails']['itemIngestionStatus'] as $response_feed_item ) {

						$item_results = new OVC_Results();

						// Check for / import any ingestion errors
						if( isset( $response_feed_item['ingestionErrors'] )
							&& is_array( $response_feed_item['ingestionErrors'] )
							&& is_array( $response_feed_item['ingestionErrors']['ingestionError'] )
							&& count( $response_feed_item['ingestionErrors']['ingestionError'] )
						) {

							// Auto-correct xml numerical array with only one value //dev:improve
							$item_ingestion_errors = 
								isset( $response_feed_item['ingestionErrors']['ingestionError']['type'] ) 
								? array( $response_feed_item['ingestionErrors']['ingestionError'] ) 
								: $response_feed_item['ingestionErrors']['ingestionError'];

							foreach( $item_ingestion_errors as $error_info ) {

								// Make sure the error code is in the reference table
								self::maybe_add_error_code( $error_info['type'], $error_info['code'], $error_info['description'] );

								// Record the feed item error
								$error_id = (int) self::get_error_id_by_code( $error_info['code'] );
								self::save_feed_item_error( $response_feed_item['sku'], $error_id, $this );
							}

						}

						// DEV:TOdo //dev:improve MOVE THIS TO OVC_Row_walmart_product
						if( $ovc_walmart_product = OVC_Row_walmart_product::get_from_api_response( $response_feed_item, $item_results ) ) {

							// Only update the product's feed_item_status if this is the most recent item feed for the product
							if( $ovc_walmart_product->data( 'wa.last_item_feed_id' ) == $this->ID ) {

								$ingestion_status = $response_feed_item['ingestionStatus'];

								$updated_product_data = array(
									'wa.feed_item_status' => $ingestion_status
								);

								$feed_is_complete = in_array( $response_data['feedStatus'], array( 'PROCESSED', 'ERROR', 'FEED_ERROR' ) );

								$current_sync_status = $new_sync_status = $ovc_walmart_product->data( 'wa.sync_status' );
								
								// Determine if we need to update wa.sync_status, (which shouldn't change unless the feed is complete/done processing)
								if( $feed_is_complete ) {

									if( 'PROCESSED' == $response_data['feedStatus'] 
										&& 'SUCCESS' == $ingestion_status
									) {

										if( empty( $ovc_walmart_product->data( 'wa.sku' ) ) ) {

											$updated_product_data['wa.sku']			= $response_feed_item['sku'];
										}
										
										if( empty( $ovc_walmart_product->data( 'wa.walmart_id' ) ) ) {
											
											$updated_product_data['wa.walmart_id']	= $response_feed_item['wpid'];
										}
									}

									if( 'CREATING' == $ovc_walmart_product->data( 'wa.sync_status' ) ) {

										if( 'PROCESSED' == $response_data['feedStatus'] 
											&& 'SUCCESS' == $ingestion_status
										) {

											$updated_product_data['wa.sync_status'] = 'CREATE_CONFIRM';
										}
										else {
											$updated_product_data['wa.sync_status'] = 'CREATE_FAIL';
										}
									}
									else if( 'UPDATING' == $ovc_walmart_product->data( 'wa.sync_status' ) ) {

										if( 'PROCESSED' == $response_data['feedStatus'] 
											&& 'SUCCESS' == $ingestion_status
										) {
											
											$updated_product_data['wa.sync_status'] = 'UPDATE_CONFIRM';
										}
										else {
											$updated_product_data['wa.sync_status'] = 'UPDATE_FAIL';
										}
									}
								}

								$ovc_walmart_product->update_data( $updated_product_data, $item_results );
							}			
						}

						$feed_items_updated++;
					}

					if( $feed_items_updated >= $feed_items_received ) {
						$looping = false;
						break;
					}
				}
				else {
					$looping = false;
					break;
				}
			}

			$request_data['query_params']['offset'] += $limit;
		}
	}

	public function handle_api_response( $response, &$results = false ) {
		OVC_Results::maybe_init( $results );
		$this->data_set();

		$response_data = $response;

		if( $response instanceof OVC_API_Response ) {
			$response_data = $response->format();
		}

		//dev:feedId?

		$updated_data = array(
			'status'			=> (string) $response_data['feedStatus'],
			'submitted'			=> (int) $response_data['itemsReceived'],
			'processed'			=> (int) $response_data['itemsSucceeded'],
			'pending'			=> (int) $response_data['itemsProcessing'],
			'errors'			=> (int) $response_data['itemsFailed'],
			'ingestion_errors'	=> ''
		);

		if( isset( $response_data['ingestionErrors'] )
			&& isset( $response_data['ingestionErrors']['ingestionError'] )
			&& is_array( $response_data['ingestionErrors']['ingestionError'] )
		) {

			// If there is only one error,  // dev:improve  xml parsing
			$ingestion_errors = 
				isset( $response_data['ingestionErrors']['ingestionError']['type'] ) 
				? array( $response_data['ingestionErrors']['ingestionError'] ) 
				: $response_data['ingestionErrors']['ingestionError'];

			$updated_data['ingestion_errors'] = serialize( $ingestion_errors );
		}

		return $this->update_data( $updated_data, $results );
	}

	/**
	 * WALMART FEED ITEM ERROR CODE HANDLING
	 */

	public static function init_error_codes( $force_refresh = false ) {


		if( !self::$walmart_error_codes || $force_refresh ) {

			global $wpdb;

			$walmart_error_codes = $wpdb->get_results( "SELECT * FROM wp_ovc_walmart_error_codes", ARRAY_A );

			// Set the keys to the Error code for faster lookups
			foreach( $walmart_error_codes as $error_code_meta ) {

				self::$walmart_error_codes[ $error_code_meta['code'] ] = $error_code_meta;
			}
		}

		return self::$walmart_error_codes;
	}

	public static function maybe_add_error_code( $type, $code, $description ) {
		self::init_error_codes();

		// Check if error code exists
		if( !isset( self::$walmart_error_codes[ $code ] ) ) {
			global $wpdb;

			$error_code_data = array(
				'type'			=> $type,
				'code'			=> $code,
				'description'	=> $description
			);

			$wpdb->insert( 'wp_ovc_walmart_error_codes', $error_code_data, '%s' );

			self::init_error_codes( true );
		}
	}

	public static function get_error_id_by_code( $error_code ) {
		self::init_error_codes();

		foreach( self::$walmart_error_codes as $error_code_info ) {

			if( $error_code == $error_code_info['code'] ) {
				return $error_code_info['ID'];
			}
		}

		return false;
	}

	public static function save_feed_item_error( $sku, $error_id = 0, $api_feed ) {
		global $wpdb;

		//
		if( !$wpdb->get_var( "SELECT COUNT(*) FROM wp_ovc_walmart_feed_item_errors WHERE feed_id = {$api_feed->ID} AND sku = '{$sku}' AND error_id = $error_id" ) ) {

			$feed_item_error_data = array(
				'feed_id'	=> $api_feed->ID,
				'sku'		=> $sku,
				'error_id'	=> $error_id
			);

			$formats = array( '%d', '%s', '%d' );

			$wpdb->insert( 'wp_ovc_walmart_feed_item_errors', $feed_item_error_data, $formats );
		}
	}
}