<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Row_walmart_product extends OVC_Row {

	public function api_refresh( &$results = false ) {
		OVC_Results::maybe_init( $results );

		if( $this->data( 'wa.sku' ) ) {

			$request_data = array(
				'url_params' => array(
					'sku' => $this->data( 'wa.sku' )
				)
			);

			$response = OVC_API_Walmart::request( 'get_item', $request_data, $results );


			//return $response;

			return $this->handle_api_response( $response, $results );
		}
		//OVC_Walmart_API::refresh_local_walmart_product( $this->data( 'wa.sku' ), null, $results );
	}

	public function get_best_walmart_sku() {

		$walmart_sku = OVC_External::filter_walmart_sku( $this );

		if( !$walmart_sku ) {
			$walmart_sku = $this->data( 'wa.sku' );
		}

		return $walmart_sku;
	}

	public function get_ovc_product() {

		// If 'wa.ovc_id' is null, this will return an empty OVC Product Object that can be checked using $ovc_pr->exists()
		$ovc_pr = OVCDB()->get_row( FS('ovc_products'), $this->data( 'wa.ovc_id' ) );

		return $ovc_pr;
	}

	public function get_online_stock() {

		$ovc_pr = $this->get_ovc_product();

		// If the OVC Product exists, return the stock otherwise return a stock of 0
		$walmart_stock = $ovc_pr->exists() ? $ovc_pr->get_online_stock() : 0;

		return $walmart_stock;
	}

	public static function get_from_api_response( $response, &$results = false ) {
		OVC_Results::maybe_init( $results );
		$ovc_walmart_product = null;
		$walmart_fs = FS( 'walmart_product' );
		$results->walmart_product_match_type = 'match_failed'; 

		if( $response instanceof OVC_API_Response ) {
			$response = $response->format();
		}

		if( !empty( $response['error'] ) ) {
			return $results->error( 'Cannot get OVC Walmart Product from API Error Response.' );
		}

		// Extract relevant values from different types of responses
		$response_walmart_id 	= $response['wpid'] ?? $response['wa.walmart_id'] ?? $response['walmart_id'] ?? null;
		$response_sku 			= $response['sku'] ?? $response['wa.sku'] ?? null;
		$response_upc 			= $response['upc'] ?? $response['wa.upc'] ?? '';

		if( !empty( $response_walmart_id ) ) {

			$ovc_walmart_product = OVCDB()->get_row_by_valid_id( $walmart_fs, 'walmart_id', $response_walmart_id, $results );

			if( $ovc_walmart_product->exists() ) {
				$results->walmart_product_match_type = 'match_walmart_id';
			}
		}

		// If not found by walmart_id, try by SKU
		if( ( !$ovc_walmart_product || !$ovc_walmart_product->exists() ) && $response_sku ) {

			global $wpdb;

			/* This could be a newly created walmart product:
			 * Check if there's only one match by SKU where 
			 * wa.ovc_id != 0 AND wa.walmart_id = '' AND wa.sync_status = 'CREATE_CONFIRM'
			 */
			$new_ovc_walmart_products = $wpdb->get_results( "SELECT ID FROM {$wpdb->prefix}ovc_walmart_products WHERE sku = '{$response_sku}' AND ovc_id = 0 AND walmart_id = '' AND sync_status = 'CREATE_CONFIRM'", ARRAY_A );

			if( 1 == count( $new_ovc_walmart_products ) ) {

				$ovc_walmart_product = OVCDB()->get_row( $walmart_fs, $new_ovc_walmart_products[0]['ID'], $results );
				$results->walmart_product_match_type = 'match_sku_creating';
			}
			else if( 1 < count( $new_ovc_walmart_products ) ) {
				// Multiple matches... this shouldn't be able to happen
				$results->walmart_product_match_type = 'match_fail_multiple_creating';
				return $results->error( "ERROR! Multiple local walmart products found with status of CREATE_CONFIRM for SKU {$response_sku}" );
			}
			else {
				// This must be an unknown remote walmart product
				$ovc_walmart_products_by_sku = $wpdb->get_results( "SELECT ID FROM {$wpdb->prefix}ovc_walmart_products WHERE sku = '{$response_sku}'", ARRAY_A );

				if( !count( $ovc_walmart_products_by_sku ) ) {
					// Remote Walmart product not found in local data
					$results->notice( "Creating local entry for unknown remote Walmart Product. Walmart ID: {$response_walmart_id}, SKU: {$response_sku}, UPC: {$response_upc}" );
					$results->walmart_product_match_type = 'match_none_create_local';

					// Return new empty row
					$ovc_walmart_product = OVCDB()->new_row( $walmart_fs );
				}
				else if( 1 == count( $ovc_walmart_products_by_sku ) ) {
					// Single local Walmart product found by SKU
					$results->walmart_product_match_type = 'match_sku_single';
					$ovc_walmart_product = OVCDB()->get_row( $walmart_fs, $ovc_walmart_products_by_sku[0]['ID'], $results, false );
				}
				else {
					// More than one ovc_walmart_product has this SKU
					foreach( $ovc_walmart_products_by_sku as $walmart_product_data ) {

						$duplicate_error_walmart_product = OVCDB()->get_row( $walmart_fs, $walmart_product_data['ID'], $results, false );

						$updated_data = array(
							'wa.sync_status' 	=> 'DUPLICATE_ERROR'
						);

						$duplicate_error_walmart_product->update_data( $updated_data, $results );

						$results->walmart_product_match_type = 'match_fail_duplicate_sku';
						$results->error( "DUPLICATE_ERROR for SKU {$response_sku}, wa.ID: {$walmart_product_data['ID']} " );
					}

					return $results->error( 'ERROR! Unable to identify local walmart product from response. ' . count( $ovc_walmart_products_by_sku ) . " local walmart products found for SKU {$response_sku}. " );
				}
			}
		}

		if( !$ovc_walmart_product ) {

			$results->notice( "No OVC Walmart Product found from API Response. Walmart ID: {$response_walmart_id}, SKU: {$response_sku}, UPC: {$response_upc}" );
		}

		return $ovc_walmart_product;
	}

	public static function localize_response_data( $response, &$results = false ) {

		$response_data = $response;

		if( $response instanceof OVC_API_Response ) {
			$response_data = $response->format();
		}


		$response_key_map = array(
			'wpid'				=> 'wa.walmart_id', // dev:walmart_v3
			'sku'				=> 'wa.sku',
			'upc'				=> 'wa.upc',
			'productName'		=> 'wa.productName',
			'productType'		=> 'wa.productType',
			'price'				=> false, // special handling
			'publishedStatus'	=> 'wa.product_status'
		);

		$ovc_walmart_data = array();

		foreach( $response_key_map as $walmart_key => $field ) {

			if( isset( $response_data[ $walmart_key ] ) ) {
				
				if( is_string( $field ) ) {

					$ovc_walmart_data[ $field ] = $response_data[ $walmart_key ] ;
				}
				else if( 'price' == $walmart_key 
						&& isset( $response_data['price']['amount'] )
				) {

					$ovc_walmart_data['wa.price'] = $response_data['price']['amount'];
				}
			}
			// Fix for prices not being sent when SKUs are CREATE_FAIL
			else if( 'price' == $walmart_key ) {
				$ovc_walmart_data['wa.price'] = '0.00';
			}
			else {
				$results->notice( "Walmart Product: ItemResponse response_data is missing key: {$walmart_key} \r\n " . print_r( $response, true ) );
			}
		}

		return $ovc_walmart_data;
	}

	public function handle_api_response( $response, &$results = false ) {
		OVC_Results::maybe_init( $results );

		$response_data = $response;

		if( $response instanceof OVC_API_Response ) {
			$response_data = $response->format();
		}

		// Detect if the response_data is still has the raw response keys
		if( isset( $response_data['wpid'] ) ) {
			// Convert API response object to OVC data
			$updated_product_data = $this->localize_response_data( $response_data, $results );	
		}
		else {
			// This probably came from the Items Report CSV
			$updated_product_data = $response_data;
		}		

		// We're adding a new remote walmart product (does not happen under healthy operating circumstances)
		if( !$this->exists() ) {

			$sku = $updated_product_data['wa.sku'];
			$upc = $updated_product_data['wa.upc'];

			//
			if( !OVCDB()->is_value_unique( 'wa.sku', $sku ) 
				|| !OVCDB()->is_value_unique( 'wa.upc', $upc )
			) {
				$updated_product_data['wa.sync_status'] = 'DUPLICATE_FIX';
			}
			else if( 'UNPUBLISHED' == $updated_product_data['wa.product_status'] ) {
				$updated_product_data['wa.sync_status'] = 'RETIRED';
			}
			else if( 'PUBLISHED' == $updated_product_data['wa.product_status'] ) {
				$updated_product_data['wa.sync_status'] = 'ACTIVE';
			}
		}
		else {
			// Get latest API Feed - needed to determine wa.sync_status
			$api_feed_id = $this->data( 'wa.last_item_feed_id' );

			$api_feed = OVCDB()->get_row( FS('api_feeds'), $api_feed_id );

			$sync_status = $this->data( 'wa.sync_status' );

			$feed_is_complete = in_array( $api_feed->status, array( 'PROCESSED', 'ERROR', 'FEED_ERROR' ) );

			$item_is_processed = isset( $updated_product_data['wa.product_status'] ) ? in_array( $updated_product_data['wa.product_status'], array( 'PUBLISHED', 'UNPUBLISHED', 'SYSTEM_ERROR' ) ) : false;
			if( empty( $api_feed_id ) 
				&& empty( $sync_status )
				&& 'PUBLISHED' == $updated_product_data['wa.product_status']
				&& 'ACTIVE' == $this->data( 'wa.lifecycle_status' )
			) {

				$updated_product_data['wa.sync_status'] = 'ACTIVE';
			}
			else if( $feed_is_complete 
				&& $item_is_processed 
				&& in_array( $sync_status, array( 'CREATING', 'CREATE_CONFIRM', 'UPDATING', 'UPDATE_CONFIRM' ) ) 
			) {

				if( 'UPDATE_SKU' == $this->data( 'wa.manual_action' ) 
					|| 'UPDATE_UPC' == $this->data( 'wa.manual_action' )
				) {

					$updated_product_data['wa.manual_action'] = $this->data( 'wa.manual_action' ) . '_CONFIRM';
				}

				if( 'PROCESSED' == $api_feed->status 
					&& 'SUCCESS' == $this->data( 'wa.feed_item_status' ) 
				) {
					
					$updated_product_data['wa.sync_status'] = 'ACTIVE';					
				}
				else if( 'CREATE_CONFIRM' == $this->data( 'wa.sync_status' ) ) {
					$updated_product_data['wa.sync_status'] = 'CREATE_FAIL';
					//$updated_product_data['wa.walmart_id']	= '';
				}
				else if( 'UPDATE_CONFIRM' == $this->data( 'wa.sync_status' ) ) {
					$updated_product_data['wa.sync_status'] = 'UPDATE_FAIL';
				}
			}
			else if( $feed_is_complete 
				&& $item_is_processed 
				&& 'SENT' == $this->data( 'wa.feed_item_status')
			) {

				// If we're updating the SKU or UPC, make sure to update the sync status
				if( 'UPDATE_SKU' == $this->data( 'wa.manual_action' ) 
					|| 'UPDATE_UPC' == $this->data( 'wa.manual_action' )
				) {

					$updated_product_data['wa.manual_action'] = $this->data( 'wa.manual_action' ) . '_CONFIRM';

					if( 'UPDATING' == $this->data( 'wa.sync_status' ) ) {
						$updated_product_data['wa.sync_status'] = 'UPDATE_CONFIRM';
					}
					else if( 'CREATING' == $this->data( 'wa.sync_status' ) ) {
						$updated_product_data['wa.sync_status'] = 'CREATE_CONFIRM';
					}
				} else {
					$updated_product_data['wa.feed_item_status'] = 'OTHER_ERROR';

					if( 'UPDATING' == $this->data( 'wa.sync_status' ) ) {
						$updated_product_data['wa.sync_status'] = 'UPDATE_FAIL';
					}
					else if( 'CREATING' == $this->data( 'wa.sync_status' ) ) {
						$updated_product_data['wa.sync_status'] = 'CREATE_FAIL';
					}
				}
			}
			else if( 'RETIRE_CONFIRM' == $this->data( 'wa.sync_status' ) 
				&& 'UNPUBLISHED' == $updated_product_data['wa.product_status']
			) {
				$updated_product_data['wa.sync_status'] = 'RETIRED';
			}
			else if( $feed_is_complete 
				&& $item_is_processed 
				&& 'DUPLICATE_CONFIRM' == $this->data( 'wa.sync_status' )
			) {

				$sku = $updated_product_data['wa.sku'];
				$upc = $updated_product_data['wa.upc'];

				if( !OVCDB()->is_value_unique( 'wa.sku', $sku, $this->ID ) 
					|| !OVCDB()->is_value_unique( 'wa.upc', $upc, $this->ID )
				) {
					$updated_product_data['wa.sync_status'] = 'DUPLICATE_FIX';
				}
				else if( 'UNPUBLISHED' == $updated_product_data['wa.product_status'] ) {
					$updated_product_data['wa.sync_status'] = 'RETIRED';
				}
				else if( 'PUBLISHED' == $updated_product_data['wa.product_status'] ) {
					$updated_product_data['wa.sync_status'] = 'DUPLICATE_RETIRE';
				}
			}
			// If there was an error, be sure to set the sync status to fail so that it can rerun
			else if( 'DATA_ERROR' == $this->data( 'wa.feed_item_status' ) ) {

				if( 'UPDATING' == $this->data( 'wa.sync_status' ) ) {
					$updated_product_data['wa.sync_status'] = 'UPDATE_FAIL';
				}
				else if( 'CREATING' == $this->data( 'wa.sync_status' ) ) {
					$updated_product_data['wa.sync_status'] = 'CREATE_FAIL';
				}
			}

			

		}

		if( count( $updated_product_data ) ) {
			$updated_product_data['wa.last_item_check'] = current_time( 'mysql', 1 );
		}

		// Updated product data
		if( false === $this->update_data( $updated_product_data, $results ) ) {
			
			return $results->error( "ERROR updating local walmart product, SKU: {$updated_product_data['wa.sku']}" );
		}

		return true;

	}

	// dev:move
	// public static function refresh_local_walmart_product( $walmart_sku, $wapi = null, &$results = false ) {
	// 	OVC_Walmart_API::auto_init( $wapi );
	// 	OVC_Results::maybe_init( $results );

	// 	$request_data = array(
	// 		'query_params' => array(
	// 			'sku' => $walmart_sku
	// 		)
	// 	);

	// 	$item = $wapi->call_api( 'get_items', $request_data );

	// 	if( isset( $item['ItemResponse'] ) ) {

	// 		OVC_Walmart_API::update_local_walmart_product_from_item_response( $item['ItemResponse'], $results );
	// 	} else if( isset( $item['error'] ) ) {

	// 		OVC_Walmart_API::update_local_walmart_product_from_error_response( $walmart_sku, $item['error'], $results );
	// 	} else {
	// 		$results->error( "Invalid item response: \n\r" . print_r( $item, true ) );
	// 	}
	// }

	// dev:move
	public static function get_ovc_product_from_walmart_response( $response, &$results = false ) {
		OVC_Results::maybe_init( $results );
		global $wpdb; // dev:OVCDB dev:OVCSC

		$wa_response_keys = array(
			'wpid'	=> 'walmart_id',
			'upc'	=> 'upc',
			'sku'	=> 'sku'
		);

		// Loop through possible ways of determining walmart_product
		foreach( $wa_response_keys as $response_key => $field_name ) {

			if( isset( $response[ $response_key ] ) && $response[ $response_key ] ) {

				$ovc_id = $wpdb->get_col( "SELECT ovc_id FROM wp_ovc_walmart_products WHERE {$field_name} = '{$response[ $response_key ]}'" );

				// Make sure we get exactly one result
				if( 1 == count( $ovc_id ) && $ovc_id[0] ) {

					$ovc_product = OVCDB()->get_row( FS('ovc_products'), $ovc_id[0], $results, false );

					if( $ovc_product->exists() ) {

						return $ovc_product;
					}
				} else if( count( $ovc_id ) > 1 ) {

					$results->notice( "WARNING! DUPLICATE values in field wa.{$field_name} : " . $response[ $response_key ] );
				} else {

					// Check if this is a sku change
					if( 'sku' == $response_key ) {

						//ovc_dev_log( 'here' );

						$ovc_product = OVCDB()->get_row_by_valid_id( FS('ovc_products'), 'sku', $response[ $response_key ], $results, false );
						
						if( $ovc_product->exists() 
							&& $response['sku'] != $ovc_product->data( 'wa.sku' )
						) {
							return $ovc_product;
						}
					}
				}
			}
		}

		$results->notice( "Unable to identify OVC product from walmart response: \r\n" . print_r( $response, true ) );

		return false;
	}

	// dev:move
	public static function get_ovc_product_from_walmart_sku( $walmart_sku, &$results = false ) {
		OVC_Results::maybe_init( $results );

		$response = array( 'sku' => $walmart_sku );

		return self::get_ovc_product_from_walmart_response( $response, $results );
	}

	// dev:move
	public static function update_local_walmart_product_from_error_response( $walmart_sku, $error = array(), &$results = false ) {
		OVC_Results::maybe_init( $results );

		$results->notice( "Walmart Item update response ERROR. CODE: ". $error['code'] . " - {$error['description']} . {$error['info']} " );

		$error_code = explode( '.', $error['code'] );
		$error_code = $error_code[0];

		$product = self::get_ovc_product_from_walmart_sku( $walmart_sku, $results );

		if( !$product ) {
			return $results->error( "COULD NOT IDENTIFY WALMART PRODUCT, {$walmart_sku} " );
		}

		$sync_status = $product->data( 'wa.sync_status' );

		if( $product->exists() ) {
			$api_feed = self::get_api_feed( $product->data( 'wa.last_item_feed_id' ) );
		}

		$updated_product_data = array();

		switch( $error_code ) {

			case 'CONTENT_NOT_FOUND':
				
				if( 'CREATE_CONFIRM' == $sync_status ) {
					$updated_product_data['wa.sync_status'] = 'CREATE_FAIL';
				}
			break;
			case 'ERR_PDI_0030':
				$results->notice( "Item not found for update... Resubmitting SKU {$walmart_sku} for CREATE" );
				$updated_product_data['wa.sync_status'] = 'CREATE_FAIL';
			break;
			default:
				return $results->error( "ERROR! No handler for error code: {$error['code']}" );
			break;

		}

		// Updated product data
		if( false === $product->update_data( $updated_product_data, $results ) ) {
			return $results->error( "ERROR updating local walmart product, SKU: {$walmart_sku}" );
		}

		return true;
	}

	// dev:move
	// public static function update_local_walmart_products( $wapi = null, &$results = false ) {
	// 	OVC_Walmart_API::auto_init( $wapi );
	// 	OVC_Results::maybe_init( $results );

	// 	$offset = 0;
	// 	$chunk = 20;
	// 	$looping = true;

	// 	$updated_count = 0;
		
	// 	$request_data = array(
	// 		'query_params' => array(
	// 			'includeDetails' => 'true',
	// 			'offset' 		 => $offset
	// 		)
	// 	);

	// 	while( $looping ){

	// 		//Update Offset
	// 		$request_data['query_params']['offset'] = $offset;

	// 		$items = $wapi->call_api( 'get_items', $request_data );

	// 		if( isset( $items['ItemResponse'] )
	// 			&& count( $items['ItemResponse'] )
	// 		) {

	// 			// Check to make sure single hanging ItemResponse have the numerically-indexed array layer //dev:improve //xml parsing issues
	// 			if( !isset( $items['ItemResponse'][0] ) ) {

	// 				$items['ItemResponse'] = array(
	// 					$items['ItemResponse']
	// 				);
	// 			}

	// 			foreach( $items['ItemResponse'] as $item ) {

	// 				if( is_array( $item )
	// 					&& isset( $item['sku'] )
	// 				) {
	// 					OVC_Walmart_API::update_local_walmart_product_from_item_response( $item, $results );
	// 					$updated_count++;
	// 				}
	// 			}
	// 		} else {
	// 			$looping = false;
	// 			break;
	// 		}

	// 		$offset += $chunk;
	// 	}
	// }

	// dev:move
	// public static function update_local_walmart_product_from_item_response( $response_data, &$results = false ) {
	// 	OVC_Results::maybe_init( $results );

	// 	$walmart_sku = $response_data['sku'];

	// 	$response_key_map = array(
	// 		'wpid'				=> 'wa.walmart_id', // dev:walmart_v3
	// 		'sku'				=> 'wa.sku',
	// 		'upc'				=> 'wa.upc',
	// 		'productName'		=> 'wa.productName',
	// 		'productType'		=> 'wa.productType',
	// 		'price'				=> false, // special handling
	// 		'publishedStatus'	=> 'wa.product_status'
	// 	);

	// 	$updated_product_data = array();

	// 	foreach( $response_key_map as $walmart_key => $field ) {

	// 		if( isset( $response_data[ $walmart_key ] ) ) {
				
	// 			if( is_string( $field ) ) {
	// 				$updated_product_data[ $field ] = $response_data[ $walmart_key ] ;
	// 			} else if( 'price' == $walmart_key 
	// 					&& isset( $response_data['price']['amount'] )
	// 			) {
	// 				$updated_product_data['wa.price'] = $response_data['price']['amount'];
	// 			}

	// 		// Fix for prices not being sent when SKUs are CREATE_FAIL
	// 		} else if( 'price' == $walmart_key ) {
	// 			$updated_product_data['wa.price'] = '0.00';
	// 		} else {
	// 			$results->notice( "Error updating local Walmart SKU {$walmart_sku} - ItemResponse response_data is missing key: {$walmart_key}" );
	// 		}
	// 	}

	// 	if( count( $updated_product_data ) ) {
	// 		$updated_product_data['wa.last_item_check'] = current_time( 'mysql', 1 );
	// 	}

	// 	// Check if product exists
	// 	$product = self::get_ovc_product_from_walmart_response( $response_data, $results );

	// 	if( !$product ) {


	// 		return $results->error( "UNKNOWN WALMART SKU {$walmart_sku}" );
	// 	} else {

	// 		$api_feed = self::get_api_feed( $product->data( 'wa.last_item_feed_id' ) );

	// 		$feed_is_complete = in_array( $api_feed->status, array( 'PROCESSED', 'ERROR', 'FEED_ERROR' ) );

	// 		$item_is_processed = in_array( $updated_product_data['wa.product_status'], array( 'PUBLISHED', 'UNPUBLISHED', 'SYSTEM_ERROR' ) );

	// 		if( $feed_is_complete 
	// 			&& $item_is_processed 
	// 			&& in_array( $product->data( 'wa.sync_status' ), array( 'CREATE_CONFIRM', 'UPDATE_CONFIRM' ) ) 
	// 		) {

	// 			if( 'PROCESSED' == $api_feed->status 
	// 				&& 'SUCCESS' == $product->data( 'wa.feed_item_status' ) 
	// 			) {
					
	// 				$updated_product_data['wa.sync_status'] = 'ACTIVE';					
	// 			} else if( 'CREATE_CONFIRM' == $product->data( 'wa.sync_status' ) ) {
	// 				$updated_product_data['wa.sync_status'] = 'CREATE_FAIL';
	// 				//$updated_product_data['wa.walmart_id']	= '';
	// 			} else if( 'UPDATE_CONFIRM' == $product->data( 'wa.sync_status' ) ) {
	// 				$updated_product_data['wa.sync_status'] = 'UPDATE_FAIL';
	// 			}
	// 		} else if( $feed_is_complete 
	// 			&& $item_is_processed 
	// 			&& 'SENT' == $product->data( 'wa.feed_item_status')
	// 		) {

	// 			// If we're updating the SKU or UPC, make sure to update the sync status
	// 			if( 'UPDATE_SKU' == $product->data( 'wa.manual_action' ) 
	// 				|| 'UPDATE_UPC' == $product->data( 'wa.manual_action' )
	// 			) {

	// 				$updated_product_data['wa.manual_action'] = $product->data( 'wa.manual_action' ) . '_CONFIRM';

	// 				if( 'UPDATING' == $product->data( 'wa.sync_status' ) ) {
	// 					$updated_product_data['wa.sync_status'] = 'UPDATE_CONFIRM';
	// 				}
	// 				else if( 'CREATING' == $product->data( 'wa.sync_status' ) ) {
	// 					$updated_product_data['wa.sync_status'] = 'CREATE_CONFIRM';
	// 				}
	// 			} else {
	// 				$updated_product_data['wa.feed_item_status'] = 'OTHER_ERROR';

	// 				if( 'UPDATING' == $product->data( 'wa.sync_status' ) ) {
	// 					$updated_product_data['wa.sync_status'] = 'UPDATE_FAIL';
	// 				} else if( 'CREATING' == $product->data( 'wa.sync_status' ) ) {
	// 					$updated_product_data['wa.sync_status'] = 'CREATE_FAIL';
	// 				}
	// 			}
	// 		} else if( 'RETIRE_CONFIRM' == $product->data( 'wa.sync_status' ) 
	// 			&& 'UNPUBLISHED' == $updated_product_data['wa.product_status']
	// 		) {
	// 			$updated_product_data['wa.sync_status'] = 'RETIRED';
	// 		}

	// 		// Updated product data
	// 		if( false === $product->update_data( $updated_product_data, $results ) ) {
	// 			return $results->error( "ERROR updating local walmart product, SKU: {$walmart_sku}" );
	// 		}

	// 		return true;
	// 	}
	// }

	// dev:move
	// unused?
	// public static function update_local_walmart_product_from_feed_response( $response_data, &$results = false, $api_feed ) {
	// 	OVC_Results::maybe_init( $results );

	// 	// Validate $response_data
	// 	if( !is_array( $response_data )
	// 		|| !isset( $response_data['sku'], $response_data['ingestionStatus'] )
	// 	) {
	// 		return $results->error( "Error updating local Walmart SKU {$response_data['sku']} - FeedItem invalid response_data \n\r" . print_r( $response_data, true ) );
	// 	}

	// 	$walmart_sku = $response_data['sku'];
	// 	$feed_item_errors = false;

	// 	$product = self::get_ovc_product_from_walmart_response( $response_data, $results );

	// 	// Special handling and exit if product doesn't exist // dev:improve
	// 	if( !$product ) {

	// 		$updated_product_data = array(
	// 			//'wa.walmart_id' 	=> $response_data[ 'wpid' ],
	// 			'wa.feed_item_status' => $response_data['ingestionStatus']
	// 		);

	// 		return $results->error( "UNKNOWN WALMART SKU: {$walmart_sku}" );
	// 	}

	// 	// We only need to do anything if there are feed errors or if this is a feed item response on a CREATE or UPDATE feed 
	// 	if( isset( $response_data['ingestionErrors'] )
	// 		&& is_array( $response_data['ingestionErrors'] )
	// 		&& is_array( $response_data['ingestionErrors']['ingestionError'] )
	// 		&& count( $response_data['ingestionErrors']['ingestionError'] )
	// 	) {

	// 		$item_ingestion_errors = isset( $response_data['ingestionErrors']['ingestionError']['type'] ) ? array( $response_data['ingestionErrors']['ingestionError'] ) : $response_data['ingestionErrors']['ingestionError'];

	// 		foreach( $item_ingestion_errors as $error_info ) {

	// 			// Make sure the error code is in the reference table
	// 			self::maybe_add_error_code( $error_info['type'], $error_info['code'], $error_info['description'] );

	// 			// Record the feed item error
	// 			$error_id = (int) self::get_error_id_by_code( $error_info['code'] );
	// 			self::save_feed_item_error( $api_feed->ID, $walmart_sku, $error_id );
	// 		}

	// 		$feed_item_errors = true;
	// 	}

	// 	// Determine various statuses on walmart product (only matters if this is the product's latest CREATE or UPDATE feed)
	// 	if( in_array( $api_feed->feed_type, array( 'walmart_bulk_create_items', 'walmart_bulk_update_items' ) ) 
	// 		&& $api_feed->ID == $product->data( 'wa.last_item_feed_id' )
	// 	) {

	// 		$feed_is_complete = in_array( $api_feed->status, array( 'PROCESSED', 'ERROR', 'FEED_ERROR' ) );

	// 		$current_sync_status = $new_sync_status = $product->data( 'wa.sync_status' );

	// 		$updated_product_data = array(
	// 			//'wa.walmart_id' 		=> $response_data[ 'wpid' ],
	// 			'wa.feed_item_status'	=> $response_data['ingestionStatus']
	// 		);

	// 		// Only update walmart_id if it's blank
	// 		if( isset( $response_data['wpid'] ) && !$product->data( 'wa.walmart_id') ) {

	// 			$updated_product_data = array(
	// 				'wa.walmart_id' 		=> $response_data[ 'wpid' ]
	// 			);
	// 		}
			
	// 		// Determine if we need to update wa.sync_status, (which shouldn't change unless the feed is complete/done processing)
	// 		if( $feed_is_complete ) {

	// 			if( 'PROCESSED' == $api_feed->status 
	// 				&& 'SUCCESS' == $response_data['ingestionStatus']
	// 			) {
	// 				$updated_product_data['wa.sku']			= $response_data['sku'];
	// 			}

	// 			if( 'CREATING' == $product->data( 'wa.sync_status' ) ) {

	// 				if( 'PROCESSED' == $api_feed->status 
	// 					&& 'SUCCESS' == $response_data['ingestionStatus']
	// 				) {

	// 					$updated_product_data['wa.sync_status'] = 'CREATE_CONFIRM';
	// 				} else {
	// 					$updated_product_data['wa.sync_status'] = 'CREATE_FAIL';
	// 				}
	// 			} else if( 'UPDATING' == $product->data( 'wa.sync_status' ) ) {

	// 				if( 'PROCESSED' == $api_feed->status 
	// 					&& 'SUCCESS' == $response_data['ingestionStatus'] 
	// 				) {
						
	// 					$updated_product_data['wa.sync_status'] = 'UPDATE_CONFIRM';
	// 				} else {
	// 					$updated_product_data['wa.sync_status'] = 'UPDATE_FAIL';
	// 				}
	// 			}

	// 		}

	// 		// Updated product data
	// 		if( false === $product->update_data( $updated_product_data, $results ) ) {
	// 			return $results->error( "ERROR updating local walmart product, SKU: {$walmart_sku}" );
	// 		}
	// 	}

	// 	return true;
	// }

	

	// dev:move
	// public static function refresh_local_walmart_feed( $api_feed, $wapi = null, &$results = false, $update_feed_items = true ) {
	// 	OVC_API_Feed::auto_init( $api_feed );
	// 	OVC_Walmart_API::auto_init( $wapi );
	// 	OVC_Results::maybe_init( $results );

	// 	if( $update_feed_items ) {

	// 		OVC_Walmart_API::refresh_walmart_feed_items_status( $api_feed, $wapi, $results );
	// 	} else {

	// 		$request_data = array(
	// 			'query_params'	=> array(
	// 				'feedId' 	=> $api_feed->remote_feed_id
	// 			)
	// 		);

	// 		$response = $wapi->call_api( 'get_feeds_status', $request_data );

	// 		if( is_array( $response ) 
	// 			&& isset( $response['feed'] )
	// 		) {
	// 			OVC_Walmart_API::update_walmart_feed_meta_from_response( $response['feed']['feedId'], $response['feed'] );
	// 		}
	// 	}
	// }

	// dev:move
	// public static function update_local_walmart_feeds( $wapi = null, &$results = false, $update_feed_items = true, $force_update_all = false ) {
	// 	OVC_Walmart_API::auto_init( $wapi );
	// 	OVC_Results::maybe_init( $results );

	// 	$updated_count = 0;

	// 	if( !$force_update_all ) {

	// 		$feed_ids = OVC_Sync_Manager::get_auto_sync_count( 'walmart_feeds', true );

	// 		$results->notice( "Auto-updating Walmart feeds: " . count( $feed_ids ) . " feeds to update." );

	// 		foreach( $feed_ids as $feed_id ) {

	// 			$api_feed = new OVC_API_Feed( $feed_id );

	// 			if( $api_feed->remote_feed_id ) {

	// 				if( $update_feed_items ) {

	// 					OVC_Walmart_API::refresh_walmart_feed_items_status( $api_feed, $wapi, $results );
	// 					$updated_count++;

	// 				}
	// 				else {

	// 					$request_data = array(
	// 						'query_params'	=> array(
	// 							'feedId' 	=> $api_feed->remote_feed_id
	// 						)
	// 					);

	// 					$response = $wapi->call_api( 'get_feeds_status', $request_data );

	// 					if( is_array( $response ) 
	// 						&& isset( $response['results'] )
	// 						&& isset( $response['results']['feed'] )
	// 						&& is_array( $response['results']['feed'] )
	// 						&& count( $response['results']['feed'] )
	// 					) {

	// 						$feed_responses = $response['results']['feed'];

	// 						foreach( $feed_responses as $feed_response ) {

	// 							if( isset( $feed_response['feedId'] ) 
	// 								&& OVC_Walmart_API::update_walmart_feed_meta_from_response( $feed_response['feedId'], $feed_response )
	// 							) {

	// 								$updated_count++;
	// 							}
	// 						}
	// 					}
	// 				}
	// 			}
	// 			else {
	// 				// $results->error()
	// 			}
	// 		}

	// 	// Force update all local feeds
	// 	} else {
	// 		$results->notice( "Updating ALL Walmart Feed statuses." );

	// 		$offset = 0;
	// 		$chunk = 50;
	// 		$looping = true;

	// 		$request_data = array(
	// 			'query_params' => array(
	// 				'limit'		=> $chunk,
	// 				'offset' 	=> $offset
	// 			)
	// 		);

	// 		while( $looping ){

	// 			$feed_responses = array();
	// 			$request_data['query_params']['offset'] = $offset; //Update Offset

	// 			$response = $wapi->call_api( 'get_feeds_status', $request_data );

	// 			if( is_array( $response ) 
	// 				&& isset( $response['results'] )
	// 				&& isset( $response['results']['feed'] )
	// 				&& is_array( $response['results']['feed'] )
	// 				&& count( $response['results']['feed'] )
	// 			) {

	// 				$feed_responses = $response['results']['feed'];
	// 			}
	// 			else {
					
	// 				$looping = false;
	// 				break;
	// 			}

	// 			foreach( $feed_responses as $feed_response ) {

	// 				if( isset( $feed_response['feedId'] ) 
	// 					&& OVC_Walmart_API::update_walmart_feed_meta_from_response( $feed_response['feedId'], $feed_response )
	// 				) {

	// 					$updated_count++;
	// 				}
	// 			}

	// 			$offset += $chunk;
	// 		}
	// 	}

	// 	$results->notice( "Walmart feeds update completed: {$updated_count} feeds updated." );
	// }

	// dev:move
	// public static function refresh_walmart_feed_items_status( $api_feed, $wapi = null, &$results = false ) {
	// 	OVC_API_Feed::auto_init( $api_feed );
	// 	OVC_Walmart_API::auto_init( $wapi );
	// 	OVC_Results::maybe_init( $results );

	// 	$request_data = array(
	// 		'url_params'	=> array(
	// 			'remote_feed_id'	=> $api_feed->remote_feed_id
	// 		),
	// 		'query_params'	=> array(
	// 			'includeDetails'	=> 'true',
	// 			'limit'				=> 1000,
	// 			'offset'			=> 0
	// 		)
	// 	);

	// 	$looping = true;
	// 	$feedItemsUpdated = 0;

	// 	while( $looping === true ) {

	// 		$response = $wapi->call_api( 'get_feed_items_status', $request_data );

	// 		// Check feed response for error
	// 		if( is_array( $response ) && isset( $response['error'] ) ) {
	// 			$results->notice( print_r( $response, true ) );
	// 			$results->notice( "Error retrieving feed items status. API Feed (ID: {$api_feed->ID}) may be outdated." );
	// 			$api_feed->update_feed_meta( array( 'status' => 'FEED_ERROR' ) ); //dev: should be standard update row
	// 			$looping = false;
	// 			break;
	// 		}			

	// 		if( is_array( $response ) && isset( $response['feedId'], $response['feedStatus'] ) ) {

			
	// 			// Update Feed Meta
	// 			OVC_Walmart_API::update_walmart_feed_meta_from_response( $api_feed, $response );

	// 			$feedItemsReceived = intval( $response['itemsReceived'] );

	// 			if( isset( $response['itemDetails'] )
	// 				&& isset( $response['itemDetails']['itemIngestionStatus'] )
	// 				&& count( $response['itemDetails']['itemIngestionStatus'] )
	// 			) {

	// 				// Check to make sure single hanging itemIngestionStatus have the numerically-indexed array layer
	// 				// dev:improve
	// 				if( !isset( $response['itemDetails']['itemIngestionStatus'][0] ) ) {

	// 					$response['itemDetails']['itemIngestionStatus'] = array(
	// 						$response['itemDetails']['itemIngestionStatus']
	// 					);
	// 				}

	// 				// Update feed_items details
	// 				foreach( $response['itemDetails']['itemIngestionStatus'] as $feed_item ) {

	// 					$item_results = false;

	// 					OVC_Walmart_API::update_local_walmart_product_from_feed_response( $feed_item, $item_results, $api_feed );

	// 					if( $item_results->has_error ) {
	// 						$results->error( $item_results->get_msgs() );
	// 					}

	// 					$feedItemsUpdated++;
	// 				}

	// 				if( $feedItemsUpdated >= $feedItemsReceived ) {
	// 					$looping = false;
	// 					break;
	// 				}

	// 			}
	// 			else {
	// 				$looping = false;
	// 				break;
	// 			}
	// 		}

	// 		$request_data['query_params']['offset'] += 1000;
	// 	}
	// }

	// dev:move
	// public static function update_walmart_feed_meta_from_response( $api_feed, $response_data ) {
	// 	OVC_API_Feed::auto_init( $api_feed );

	// 	$updated_feed_data = array(
	// 		'status'			=> (string) $response_data['feedStatus'],
	// 		'submitted'			=> (int) $response_data['itemsReceived'],
	// 		'processed'			=> (int) $response_data['itemsSucceeded'],
	// 		'pending'			=> (int) $response_data['itemsProcessing'],
	// 		'errors'			=> (int) $response_data['itemsFailed'],
	// 		'ingestion_errors'	=> ''
	// 	);

	// 	if( isset( $response_data['ingestionErrors'] )
	// 		&& isset( $response_data['ingestionErrors']['ingestionError'] )
	// 		&& is_array( $response_data['ingestionErrors']['ingestionError'] )
	// 	) {

	// 		// If there is only one error,  // dev:improve  xml parsing
	// 		$ingestion_errors = isset( $response_data['ingestionErrors']['ingestionError']['type'] ) ? array( $response_data['ingestionErrors']['ingestionError'] ) : $response_data['ingestionErrors']['ingestionError'];

	// 		$updated_feed_data['ingestion_errors'] = serialize( $ingestion_errors );
	// 	}

	// 	return $api_feed->update_feed_meta( $updated_feed_data ); // dev:should be standard update row
	// }
	
	// public static function retire_walmart_sku( $sku, $wapi = null, &$results = false ) {
	// 	OVC_Results::maybe_init( $results );
	// 	OVC_Walmart_API::auto_init( $wapi );

	// 	$request_data = array(
	// 		'url_params'	=> array(
	// 			'sku'			=> "{$sku}"
	// 		)
	// 	);

	// 	$response = $wapi->call_api( 'retire_item', $request_data );

	// 	if( !is_array( $response )
	// 		|| isset( $response['error'] ) 
	// 		|| !isset( $response['sku'] )
	// 	) {
	// 		$results->error( "Failed to retire Walmart SKU: {$sku}" );
	// 		return $results->error( print_r( $response, true ) );
	// 	}
	// 	else {
	// 		global $wpdb;

	// 		$wpdb->update( 'wp_ovc_walmart_products', array( 'sync_status' => 'RETIRE_CONFIRM' ), array( 'sku' => $sku ), '%s', '%s' );

	// 		$results->notice( "Retire Walmart item request for SKU {$sku} was successful." );

	// 		OVC_Walmart_API::refresh_local_walmart_product( $sku, $wapi, $results );
	// 	}

	// 	return $response;
	// }

	public static function save_feed_item_error( $feed_id, $sku, $error_id = 0 ) {
		global $wpdb;

		// Make sure error exists in reference table
		//$this->maybe_add_error_code( $error_code, $error_msg );

		if( !$wpdb->get_var( "SELECT COUNT(*) FROM wp_ovc_walmart_feed_item_errors WHERE feed_id = $feed_id AND sku = '{$sku}' AND error_id = $error_id" ) ) {

			$feed_item_error_data = array(
				'feed_id'	=> $feed_id,
				'sku'		=> $sku,
				'error_id'	=> $error_id
			);

			$formats = array( '%d', '%s', '%d' );

			$wpdb->insert( 'wp_ovc_walmart_feed_item_errors', $feed_item_error_data, $formats );
		}
	}
}