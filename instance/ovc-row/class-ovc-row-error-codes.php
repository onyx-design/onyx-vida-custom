<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Row_error_codes extends OVC_Row {



	protected static $error_codes = null;
	
	// Static Method: Preload data if 

	public static function get_all_error_codes( $force_refresh = false ) {

		if( $force_refresh || !isset( static::$error_codes ) ) {

			$error_codes_rows = OVCDB()->load_rows_where( FS('errcd') );
			$error_codes = array();

			// Re-key error codes with error_code as index
			foreach( $error_codes_rows as $index => $error_code ) {

				$error_codes[] = $error_code->data( 'error_code' );
			}

			static::$error_codes = array_combine( $error_codes, $error_codes_rows );
		}

		return static::$error_codes;
	}

	public static function get_error_code( $error_code = '' ) {

		return ( $error_code instanceof OVC_Row_error_codes )
			? $error_code 
			: ( static::get_all_error_codes()[ $error_code ] ?? null );
	}

	public static function init_error_code( &$error_code = null ) {
		return $error_code = static::get_error_code( $error_code );
	}

	public static function check_ovc_product_error_code( $error_code, $ovc_product, $results = false ) {
		OVC_Results::maybe_init( $results );

		$error_code = static::get_error_code( $error_code );

		if( !$error_code ) {

			$results->error( "Invalid error code {$error_code}" );
		}
		else {

			return $error_code->check_ovc_product( $ovc_product, $results );
		}

	}

	public static function get_general_error_code( $error_code = '', $results = false ) {
		OVC_Results::maybe_init( $results );

		$error_code = static::get_error_code( $error_code );

		if( $error_code ) {

			if( $error_code->single_resource ) {

				return $results->error( "Error Code {$error_code->error_code} is not a general error code" );
			}
			else {

				return $error_code;
			}
		}
		else {
			return $results->error( "Invalid Error Code" ); // dev:improve - should probably go somewhere else
		}
	}

	public static function check_general_error_code( $error_code, $results = false, $force = false ) {

		if( $error_code = static::get_general_error_code( $error_code, $results ) ) {

			global $wpdb;

			switch( $error_code->error_code ) {

				case 'oms__missing_ovc_ids' :

					$error_code_data_check = $wpdb->get_col( "SELECT inv.sku FROM wp_ovc_oms_inventory inv WHERE inv.ovc_id = 0 AND ( inv.sku IN ( SELECT pr1.sku FROM wp_ovc_products pr1 ) OR inv.sku IN ( SELECT pr2.oms_sku_confirmed FROM wp_ovc_products pr2 ) )" );

				break;
				case 'oms__invalid_ovc_ids':

					$error_code_data_check = $wpdb->get_col( "SELECT oi.ovc_id FROM wp_ovc_oms_inventory oi LEFT JOIN wp_ovc_products pr ON oi.ovc_id = pr.ID WHERE pr.ID IS NULL AND oi.ovc_id != 0" );

				break;
				case 'oms__duplicate_ovc_ids':

					$error_code_data_check = $wpdb->get_results( "SELECT *, COUNT(*), GROUP_CONCAT(sku SEPARATOR ', ') AS 'mismatch_skus' FROM `wp_ovc_oms_inventory` WHERE ovc_id != 0 GROUP BY ovc_id HAVING COUNT(*) > 1", ARRAY_A );
				break;
				default:
				break;
			}

			if( count( $error_code_data_check ) ) {

				OVC_Row_data_errors::activate_general_error( $error_code, $error_code_data_check, $results, $force );
			}
			else if( $active_error = OVC_Row_data_errors::get_active_general_data_error( $error_code, $results, $force ) ){

				$active_error->resolve_error( $results );
			}
		}
	}

	


	public function check_ovc_product( $ovc_product, $results ) {

		if( !$this->single_resource ) {

			return $results->error( "Cannot check ovc_product for error code {$this->error_code} - not a single resource error code" );
		}

		$error_data = null;

		switch( $this->error_code ) {

			// Validation logic error_codes
			case 'sync_oms_required_fields':
			case 'sync_walmart_required_fields':
			case 'sync_wc_required_fields':
			case 'sync_shopify_required_fields':

				$passed_check = OVCDB()->check_validation_logic( $ovc_product, $this->error_code, $results );

			break;
			case 'invalid_upc_code':

				$upc_code = $ovc_product->upc_code;

				if( !empty( $upc_code ) ) {

					$passed_check = OVCDB()->validate_upc( $upc_code, $results );
				}

			break;
			case 'duplicate_upc_code':

				$passed_check = OVCDB()->is_value_unique( 'pr.upc_code', $ovc_product->upc_code, $ovc_product->ID );

			break;
			case 'sync_shopify_too_many_variants':

				$shopify_parent = $ovc_product->shopify_parent();
				$shopify_parent_external_ds = $shopify_parent->get_external_data_set( FS( 'shopify_parent' ) );

				if( isset( $shopify_parent_external_ds['variants'] ) && count( $shopify_parent_external_ds['variants'] ) > 100 ) {
					$passed_check = false;

					$error_data = array(
						'parent_id'			=> $ovc_product->data( 'pa.ID' ),
						'shopify_parent_id'	=> $shopify_parent->ID,
						'variant_skus'		=> array_column( $shopify_parent_external_ds['variants'], 'sku' )
					);
				} else {
					$passed_check = true;
				}

			break;
			default:

				$results->error( "No error code check set for error code {$this->error_code}" );

			break;
		}

		if( isset( $passed_check ) ) {

			// Error check passed and active data error was resolved
			if( ( true === $passed_check ) && $ovc_product->resolve_data_error( $this->error_code, $results ) ) {

				$results->notice( "Error code {$this->error_code} resolved for OVC ID {$ovc_product->ID}" );
				return false;
			}
			// Error check failed and data error was set (not necessarily created)
			else if( ( false === $passed_check ) && $ovc_product->activate_data_error( $this->error_code, $error_data, $results ) ) {

				$results->notice( "Error code {$this->error_code} found for OVC ID {$ovc_product->ID}" );
				return true;
			}
		}
		
		return null;
	}
}
