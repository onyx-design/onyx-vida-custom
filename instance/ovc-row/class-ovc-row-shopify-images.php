<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Row_shopify_images extends OVC_Row {

	// ACTIONS AND API METHODS
	public function delete( $results = false, $force_delete = false ) {
		OVC_Results::maybe_init( $results );

		// Maybe delete shopify image
		if( $this->shopify_image_id ) {

			$request_data = array( 
				'url_params' => array( 
					'shopify_product_id' => $this->data( 'sypas.shopify_parent_id' ),
					'shopify_image_id'	=> $this->shopify_image_id
				)
			);

			$this->handle_api_response(
				OVC_API_Shopify::request( 'delete_image', $request_data, $results ), //dev:api_response_handling //dev:error_handling
				$results
			);
		}

		return parent::delete( $results, $force_delete );
	}

	public function api_refresh( &$results = false ) {
		OVC_Results::maybe_init( $results );

		// Maybe delete shopify image
		if( $this->shopify_image_id ) {

			$request_data = array( 
				'url_params' => array( 
					'shopify_product_id' => $this->data( 'sypas.shopify_parent_id' ),
					'shopify_image_id'	=> $this->shopify_image_id
				)
			);

			$response = OVC_API_Shopify::request( 'get_image', $request_data, $results );

			return $this->handle_api_response( $response, $results );
		}
		else {
			$results->main_msg = "Unable to perform API Refresh";
			$results->notice( "Unable to perform API Refresh on shopify_image (ID {$this->ID}) because it does not have a remote shopify_image_id" );
		}

		return !$results->has_error;
	}
	/**
	 * dev:improve These handle_api_response functions should be split out as they are being used even when not a direct api response
	 * 
	 * @param type $response 
	 * @param type|bool &$results 
	 * @return type
	 */
	public function handle_api_response( $response, &$results = false ) {
		OVC_Results::maybe_init( $results );

		if( $response instanceof OVC_API_Response ) {

			switch( $response->request_type( 'type' ) ) {

				case 'get_image':
					$updated_data = array(
						'syimgs.shopify_image_id' 	=> $response->id,
						'syimgs.last_sync'			=> date("Y-m-d H:i:s",strtotime( $response->updated_at ) )
					);

					return $this->update_data( $updated_data, $results );
				break;
			}
		} else {
			$updated_data = array(
				'syimgs.shopify_image_id' 	=> $response->id,
				'syimgs.last_sync'			=> date("Y-m-d H:i:s",strtotime( $response->updated_at ) )
			);

			return $this->update_data( $updated_data, $results );
		}
	}

	public static function get_from_api_response( $response, $ovc_shopify_parent, &$results = false ) {
		OVC_Results::maybe_init( $results );
		$ovc_shopify_image = null;
		$mismatch = false;

		// Try to get by Shopify Image ID
		if( $response->id ) {
			
			$ovc_shopify_image = OVCDB()->get_row_by_valid_id( FS( 'shopify_images' ), 'shopify_image_id', $response->id, $results );
		}
		
		// Try to get by position
		if( !$ovc_shopify_image->exists() && $ovc_shopify_parent ) {
			global $wpdb;

			if( $ovc_shopify_image_id = $wpdb->get_var( "SELECT ID FROM {$wpdb->prefix}ovc_shopify_images WHERE shopify_parent_id = {$ovc_shopify_parent->ID} AND position = {$response->position}") ) {
				
				$ovc_shopify_image = OVCDB()->get_row( FS( 'shopify_images' ), $ovc_shopify_image_id, $results, $ovc_shopify_parent );	
			}			
		}

		// Verify if found
		if( $ovc_shopify_image && $ovc_shopify_image->exists() ) {

			// Verify position	
			if( $ovc_shopify_image->position != $response->position ) {
				$mismatch = true;
				$results->notice("Shopify Image position mismatch for Parent SKU {$ovc_shopify_parent->ovc_parent()->parent_sku} - Image: {$ovc_shopify_image->shopify_image_id} - Positions: Local: {$ovc_shopify_image->position} Response: {$response->position}" );
			}
			// Verify parent ID match
			else if( $ovc_shopify_image->ovc_shopify_parent()->shopify_parent_id != $response->product_id
					|| $ovc_shopify_image->shopify_parent_id != $ovc_shopify_parent->ID 
			) {
				$mismatch = true;
				$results->notice("Shopify Product ID mismatch for Parent SKU {$ovc_shopify_parent->ovc_parent()->parent_sku} - Image: {$ovc_shopify_image->shopify_image_id} - Product IDs: Local: {$ovc_shopify_image->ovc_parent()->shopify_parent_id} Response: {$response->product_id}" );
			}
		}

		// If mismatched, remove Shopify Image ID and update last_sync to force a sync
		if( $mismatch ) {

			$updated_data = array(
				'syimgs.shopify_image_id' 	=> ''
			);
		}

		return $ovc_shopify_image;
	}
	
	public function ovc_shopify_parent() {
		return $this->get_joined_data( FS( 'sypas' ) );
	}

	// FILTER FUNCTIONS
	public function filter_external_image_src() {

		if( $this->data( 'syimgs.shopify_image_id')  ) {
			return false;
		}

		$image_data = wp_get_attachment_image_src( $this->post_id, 'full' );

		return str_replace( site_url(), 'https://vida-us.com', $image_data[0] );		
	}

	public function filter_shopify_image_metafields() {

		$metafields = array(
			array(
				'namespace'	=> 'ovc',
				'key'		=> 'post_id',
				'value'		=> $this->data( 'syimgs.post_id' ),
				'value_type'=> 'integer'
			),
			array(
				'namespace'	=> 'ovc',
				'key'		=> 'ovc_shopify_image_id',
				'value'		=> $this->data( 'syimgs.ID' ),
				'value_type'=> 'integer'
			)
		);

		return $metafields;
	}
}