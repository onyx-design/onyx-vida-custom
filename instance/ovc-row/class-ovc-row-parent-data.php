<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Row_parent_data extends OVC_Row {

 
	public $image_loops = 0;

	public function get_img_gallery_ids() {
		return explode( ',', $this->img_gallery_ids );
	}

	public function remove_image( $image_id = 0, &$results = false ) {
		OVC_Results::maybe_init( $results );

		if( $image_id ) {

			$updated_data = array();

			// Remove Image from img_main
			if( $image_id == $this->data( 'pa.img_main' ) ) {

				$updated_data = array(
					'pa.img_main' => 0
				);
			// Remove Image from img_gallery_ids
			} else if( in_array( $image_id, $this->get_img_gallery_ids() ) ) {

				$img_gallery_ids = $this->get_img_gallery_ids();

				$updated_data = array(
					'pa.img_gallery_ids' => implode( ',', array_diff( $img_gallery_ids, array( $image_id ) ) )
				);
			}

			$this->update_data( $updated_data, $results );
		}
	}

	public function update_children_color_full_names( &$results = false ) {
		OVC_Results::maybe_init( $results );

		if( $children = $this->get_children( 'ovc_products', 'object', $results, false ) ) {

			$updated_ids = array();
			$color_groups = array();

			// Build list of color groups
			foreach( $children as $product ) {

				$sku_color = $product->sku_color;

				// Maybe initialize color group
				if( !isset( $color_groups[ $sku_color ] ) ) {

					$color_groups[ $sku_color ] = array(
						'long_name'	=> $product->get_best_color_long_name(),
						'rcodes'	=> array(),
						'pkqtys'	=> array()
					);
				}

				// Make sure we have the product's release code in the color group
				$oms_release_code = $product->data( 'st.oms_release_code' );
				if( !in_array( $oms_release_code, $color_groups[ $sku_color ]['rcodes'] ) ) {

					$color_groups[ $sku_color ]['rcodes'][] = $oms_release_code;
				}	

				// Make sure we have the product's pack quantity in the color group
				$pk_qty = $product->data( 'pr.sku_pkqty' );
				if( !in_array( $pk_qty, $color_groups[ $sku_color ]['pkqtys'] ) ) {

					$color_groups[ $sku_color ]['pkqtys'][] = $pk_qty;
				}

			}

			// Update color full names
			foreach( $children as $product ) {

				$color_group = $color_groups[ $product->sku_color ];

				$color_full_name = $color_group['long_name'];

				// If different product types (oms_release_code) in same sku_color for a given parent, differentiate color full names by oms_release_code
				if( count( $color_group['rcodes'] ) > 1 ) {

					$color_full_name .= " " . OVC_Lists()->get_list_item_name( 'oms_release_code', $product->data( 'st.oms_release_code' ) );
				}

				// If different pack quantities for same sku_color for a given parent, differentiate color full names with pack quantities
				if( count( $color_group['pkqtys'] ) > 1 ) {

					$color_full_name .= " " . $product->get_pack_string( false, true );
				}

				if( $color_full_name != $product->new_value( 'pr.color_full_name' ) ) {

					$updated_data = array(
						'pr.color_full_name' => $color_full_name
					);

					$row_results = new OVC_Results();
					
					$product->update_data( $updated_data, $row_results );

					if( 'updated' == $row_results->outcome ) {

						$updated_ids[] = $product->ID;
					}	
				} 
				

				
			}

			$already_updated = is_array( $results->updated_ids ) ? $results->updated_ids : array();

			$results->updated_ids = array_unique( array_merge( $already_updated, $updated_ids ) );
		}
	}

	public function get_child_product_by_sku( $sku, &$results = false ) {
		OVC_Results::maybe_init( $results );

		$child_array = $this->get_children( 'ovc_products', 'object', $results, array( 'var' => '::[sku]', 'value' => $sku ) );

		if( count( $child_array ) ) {
			return $child_array[0];
		}
		else {
			return false;
		}
	}
	
	/**
	 * Get all of the Product Categories for the children of this parent
	 * 
	 * @return 	array
	 */
	public function get_children_primary_categories() {

		$children_primary_categories = array();

		if( $ovc_children = $this->get_children( 'ovc_products' ) ) {

			foreach( $ovc_children as $ovc_child ) {

				$product_category_name = $ovc_child->data( 'st.wc_product_category' );

				if( $product_term = get_term_by( 'name', $product_category_name, 'product_cat' ) ) {

					$children_primary_categories[ $product_term->slug ] = $product_term;
				}
			}
		}

		return $children_primary_categories;
	}

	/**
	 * Get all of the Primary Categories for the children of this parent
	 * (the parent terms of the product categories)
	 * 
	 * @return 	array
	 */
	public function get_children_parent_categories() {

		$children_parent_categories = array();

		if( $children_primary_categories = $this->get_children_primary_categories() ) {

			foreach( $children_primary_categories as $children_product_category ) {

				$primary_term = get_term( $children_product_category->parent, 'product_cat' );

				if( $primary_term && !is_wp_error( $primary_term ) ) {

					$children_parent_categories[ $primary_term->slug ] = $primary_term;
				}
			}
		}

		return $children_parent_categories;
	}

	/**
	 * Get the Primary Category based on:
	 * 1. Products with children in more than one primary_category should use just the parent_category for breadcrumbs
	 * 2. If there's more than one parent_category, use the first one, but 'Bras' should always take precedence
	 * 
	 * @return 	WP_Term
	 */
	public function get_ovc_product_category() {

		$ovc_product_cat = '';

		$children_primary_categories = $this->get_children_primary_categories();

		/*
		 * If there are more than one primary categories,
		 * Use the parent category
		 */
		if( count( $children_primary_categories ) > 1 ) {

			$children_parent_categories = $this->get_children_parent_categories();

			/*
			 * If there are more than one primary categories,
			 * Use Bras if it exists, else use the first one
			 */
			if( isset( $children_parent_categories['bras'] ) ) {

				$ovc_product_cat = $children_parent_categories['bras'];

			// Else use the first primary category
			} else {

				$ovc_product_cat = array_shift( $children_parent_categories );
			}

		// Else use the product category
		} else {

			$ovc_product_cat = array_shift( $children_primary_categories );
		}

		return $ovc_product_cat;
	}

	/**
	 * Get all children (ovc_products) and their attached assemblies and assembly_items 
	 */
	public function get_children_and_assemblies( $return_type = 'raw', $results = false ) {
		OVC_Results::maybe_init( $results );

		if( !$this->exists() ) {
			return $results->error( "Cannot get inventory assemblies. Parent does not exist!" );
		}
		else {

			// This uses the field's local_name
			$pr_data_keys = array(
				'ID',
				'sku',
				'sku_color',
				'sku_pkqty',
				'size',
				'case_sku',
				'is_case',
				'is_raw',
				'use_potential_inventory',
				'stock',
				'so_qty',
				'avail_qty',
				'potential_qty',
				'reserve_qty',
				'dozens',
				'unit_per_case',
				'unit_per_box',
			);

			$children_assemblies = array(
				'pr'	=> array(),
				'asm'	=> array(),
				'asmi'	=> array()
			);

			$pr_children = $this->get_children( FS( 'ovc_products' ) );

			foreach( $pr_children as $pr_child ) {

				$filtered_child_data = array_intersect_key( $pr_child->data_set(), array_flip( $pr_data_keys ) );

				$children_assemblies['pr'][ $pr_child->ID ] = $filtered_child_data;

				// Get ovc_product's assemblies and assembly_items (if any)
				foreach( $pr_child->get_assemblies() as $asm ) {

					$children_assemblies['asm'][ $asm->ID ] = $asm->data_set();
				}

				foreach( $pr_child->get_assembly_items() as $asmi ) {

					$children_assemblies['asmi'][ $asmi->ID ] = $asmi->data_set();
				}

			}

			return $children_assemblies;
		}
	}

	public function get_active_data_errors( $results = false, $force_refresh = false ) {

		if( !$force_refresh
			&& !isset( $this->joined_data[ 'data_errors' ] )
			&& OVC_Row_data_errors::active_errors_loaded()
		) {

			$where = array(
				'err.ovc_id'	=> $this->ID,
				'err.active'	=> 1
			);

			return $this->joined_data['data_errors'] = OVCDB()->get_loaded_rows_where( FS('data_errors'), $where );
		}

		return $this->get_children( FS('data_errors'), 'object', $results, true, $force_refresh );
	}

	public function get_active_data_error( $error_code, $results = false, $force_refresh = false ) {

		foreach( $this->get_active_data_errors( $results, $force_refresh ) as $data_error ) {

			if( $error_code == $data_error->data( 'errcd.error_code' ) ) {
				return $data_error;
			}
		}

		return null;
	}

	public function resolve_data_error( $error_code, $results = false, $force = false ) {

		if( !OVC_Row_data_errors::is_enabled() && !$force ) {
			return false;
		}

		OVC_Results::maybe_init( $results );

		$active_error = $this->get_active_data_error( $error_code, $results );

		return $active_error
			? $active_error->resolve_error( $results )
			: false;
	}

	public function activate_data_error( $error_code, $error_data = null, $results = false, $force = false ) {

		if( !OVC_Row_data_errors::is_enabled() && !$force ) {
			return false;
		}

		$error_code = OVC_Row_error_codes::get_error_code( $error_code );

		if( !$error_code ) {
			return $results->error( 'Invalid error_code!' );
		}

		OVC_Results::maybe_init( $results );

		$active_error = $this->get_active_data_error( $error_code->error_code, $results );

		// Create new error
		if( !$active_error ) {

			$active_error = OVCDB()->new_row( FS('err'),
				array(
					'err.errcd_id'	=> $error_code->ID,
					'err.ovc_id'	=> $this->ID,
					'err.active'	=> true,
					'err.error_data'=> maybe_serialize( $error_data )
				)
			);

			$result = $active_error->save( $results );

			if( $result ) {

				$this->joined_data[ 'data_errors' ][ $active_error->ID ] = $active_error;
			}
		}
		else if( isset( $error_data )
			&& $active_error->data( 'error_data' ) != maybe_serialize( $error_data )
		) {

			$updated_data['error_data'] = maybe_serialize( $error_data );
			$result = $active_error->update_data( $updated_data, $results );
		}

		if( !empty( $result ) ) {
			$results->notice( "ERROR CODE {$error_code->error_code} - OVC ID: {$this->ID} - " . $error_code->description );
		}

		return $result ?? true;
	}

	public function shopify_parent() {
		return $this->get_joined_data( FS('shopify_parents') );
	}
}
