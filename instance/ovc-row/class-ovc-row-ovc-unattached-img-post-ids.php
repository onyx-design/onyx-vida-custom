<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Row_ovc_unattached_img_post_ids extends OVC_Row {
	
	public function before_delete( &$results = false ) {
		OVC_Results::maybe_init( $results );

		if( false !== wp_delete_attachment( $this->post_id, true ) ) {
			$results->notice( 'Successfully delete WP attachment ' . $this->post_id );
		}
		else {
			$results->error( 'There may have been a problem deleting WP attachment ' . $this->post_id );
		}
	}
}