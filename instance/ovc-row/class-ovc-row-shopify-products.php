<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Row_shopify_products extends OVC_Row {

	public function api_refresh( &$results = false ) {
		OVC_Results::maybe_init( $results );

		if( $this->exists() 
			&& $this->variant_id
			&& $this->ovc_id //dev:improve
		) {
			$api_request_data = array(
				'url_params'	=> array( 
					'shopify_variant_id'	=> $this->data( 'sypr.variant_id' )
				)
			);

			$this->handle_api_response(
				OVC_API_Shopify::request( 'get_variant', $api_request_data, $results ), 
				$results,
				false
			);
		}
		else {
			return $results->error( "Cannot refresh without Shopify Variant ID" );
		}

		return !$results->has_error;
	}

	public function sync_variant( &$results = false, $force_sync = false ) {
		OVC_Results::maybe_init( $results );

		if( $this->exists() 
			&& $this->variant_id
			&& $this->ovc_id
		) {

			$ovc_product = $this->ovc_product( $results );

			// If shopify_inventory_item_id isn't set, back sync the variant
			if( !$shopify_inventory_item_id = $this->shopify_inventory_item_id ) {

				if( $updated_variant_data = $this->update_local_shopify_variant_data_from_remote() ) {

					$this->shopify_inventory_item_id = $shopify_inventory_item_id = $updated_variant_data->inventory_item_id;
				} else {

					return $results->error( 'Variant does not exist on Shopify, was it manually deleted?' );
				}
			}

			$variant_inventory_level = $this->get_remote_variant_inventory_level( $shopify_inventory_item_id, $results );

			if( false !== $variant_inventory_level ) {
				// Calculate inventory adjustment
				$inventory_adjustment = ( $this->get_online_stock() - $variant_inventory_level );

				if( $inventory_adjustment ) {
					
					$inventory_level_adjust_data = array( 
						'request_body'	=> array(
							'location_id'			=> get_option( 'shopify_location_id', false ),
							'inventory_item_id'		=> $shopify_inventory_item_id,
							'available_adjustment'	=> $inventory_adjustment
						)
					);
					$inventory_level_adjust = OVC_API_Shopify::request( 'update_inventory_level', $inventory_level_adjust_data, $results );
				}
			}

			$request_data = array(
				'url_params'	=> array(
					'shopify_variant_id'	=> $this->variant_id
				),
				'request_body'	=> array( 'variant' => $ovc_product->get_external_data_set( 'shopify_variant' ) )
			);

			$api_response = OVC_API_Shopify::request( 'update_variant', $request_data, $results );

			// Set last parameter to true to update last sync time so that we don't get stuck in a loop of syncing variants
			return ( $results->success = $this->handle_api_response( $api_response, $results, true ) );
		} else {

			return $results->error( "Cannot sync variant without Shopify Variant ID and OVC ID" );
		}
	}

	public function delete_variant( &$results ) {
		OVC_Results::maybe_init( $results );

		if( $this->exists() 
			&& $this->variant_id
			&& $this->ovc_id
		) {

			$request_data = array(
				'url_params'		=> array(
					'shopify_product_id'	=> $this->shopify_parent_id,
					'shopify_variant_id'	=> $this->variant_id
				)
			);

			$api_response = OVC_API_Shopify::request( 'delete_variant', $request_data, $results );

			return ( $results->success = $this->handle_api_response( $api_response, $results, true ) );
		} else {
			return $results->error( "Cannot delete variant without Shopify Variant ID and OVC ID" );
		}
	}

	public function get_remote_variant_inventory_level( $shopify_inventory_item_id = null, &$results = false ) {
		OVC_Results::maybe_init( $results );

		$shopify_inventory_item_id = $shopify_inventory_item_id ?? $this->shopify_inventory_item_id;

		if( $this->exists()
			&& $this->variant_id
			&& $this->ovc_id
			&& $shopify_inventory_item_id
		) {

			// Get Inventory Quantity from Inventory Level
			$shopify_inventory_level_data = array(
				'query_params'	=> array(
					'inventory_item_ids' 	=> $shopify_inventory_item_id,
					'location_ids'			=> get_option( 'shopify_location_id', false )
				)
			);
			$shopify_inventory_level_response = OVC_API_Shopify::request( 'get_inventory_levels', $shopify_inventory_level_data, $results );

			// Get inventory levels returns an array of objects but we only care about one inventory location
			$shopify_inventory_level_response_data = current( $shopify_inventory_level_response->data() );

			if( $shopify_inventory_level_response->succeeded() && !isset( $shopify_inventory_level_response_data->errors ) ) {

				if( is_object( $shopify_inventory_level_response_data ) 
					&& isset( $shopify_inventory_level_response_data->available )
				) {
					return $shopify_inventory_level_response_data->available;
				}

				return 0;
			}
		}

		return false;
	}
	
	/**
	 * dev:improve These handle_api_response functions should be split out as they are being used even when not a direct api response
	 * 
	 * @param type $response 
	 * @param type|bool &$results 
	 * @param type|bool $update_last_sync 
	 * @return type
	 */
	public function handle_api_response( $response, &$results = false, $update_last_sync = true ) {
		OVC_Results::maybe_init( $results );

		if( $response instanceof OVC_API_Response && !$response->succeeded() ) {

			// dev:improve dev:data_errors
			$variant_errors = array(
				'sypr.errors' 	=> maybe_serialize( $response->errors )
			);

			$this->update_data( $variant_errors, $results );

			return $results->error( print_r( $response->get_errors(), true ) );
		}

		if( !$this->data('sypr.ovc_id') ) {
			$ovc_product = OVCDB()->get_row_by_valid_id( FS( 'ovc_products' ), 'sku', $response->sku, $results, $this );
		}
		else {
			$ovc_product = $this->ovc_product( $results );
		}

		if( $response instanceof OVC_API_Response ) {

			switch( $response->request_type('type') ) {

				case 'get_variant':
				case 'update_variant':
					// Maybe get image_id from ovc_shopify_images if it's not present
					$shopify_image_id = $response->image_id ? : $ovc_product->filter_ovc_shopify_image_id();

					// Get Inventory Quantity from Inventory Level
					// $shopify_inventory_level_data = array(
					// 	'query_params'	=> array(
					// 		'inventory_item_ids' 	=> $response->inventory_item_id,
					// 		'location_ids'			=> get_option( 'shopify_location_id', false )
					// 	)
					// );
					// $shopify_inventory_level_response = OVC_API_Shopify::request( 'get_inventory_levels', $inventory_level_data, $results );
					// $shopify_inventory_level_quantity = $shopify_inventory_level_response->data()[0]->available;
					$shopify_inventory_level_quantity = isset( $response->inventory_quantity ) ? $response->inventory_quantity : $this->get_remote_variant_inventory_level( $response->inventory_item_id, $results );

					$updated_data = array(
						'sypr.ovc_id'						=> $ovc_product->ID,
						'sypr.sku'							=> $response->sku,
						'sypr.shopify_image_id'				=> $shopify_image_id,
						'sypr.shopify_inventory_qty'		=> $shopify_inventory_level_quantity,
						'sypr.variant_id'					=> $response->id,
						'sypr.shopify_parent_id'			=> $response->product_id,
						'sypr.shopify_inventory_item_id'	=> $response->inventory_item_id,
						'sypr.upc'							=> $response->barcode,
						'sypr.price_shopify'				=> $response->price,
						'sypr.shopify_compare_at_price'		=> $response->compare_at_price,
						'sypr.last_inventory_check'			=> current_time( 'mysql', 1 ),
						'sypr.shopify_updated_at'			=> date("Y-m-d H:i:s",strtotime( $response->updated_at ) ),
						'sypr.shopify_created_at'			=> date("Y-m-d H:i:s",strtotime( $response->created_at ) )
					);

					if( $update_last_sync ) {
						$updated_data['sypr.last_sync'] = current_time( 'mysql', 1 );
					}

					return $this->update_data( $updated_data, $results );
				break;

				case 'delete_variant':
					return parent::delete( $results, true );
				break;
			}
		} else {

			// Maybe get image_id from ovc_shopify_images if it's not present
			$shopify_image_id = $response->image_id ? : $ovc_product->filter_ovc_shopify_image_id();

			// Get Inventory Quantity from Inventory Level
			// $shopify_inventory_level_data = array(
			// 	'query_params'	=> array(
			// 		'inventory_item_ids' 	=> $response->inventory_item_id,
			// 		'location_ids'			=> get_option( 'shopify_location_id', false )
			// 	)
			// );
			// $shopify_inventory_level_response = OVC_API_Shopify::request( 'get_inventory_levels', $inventory_level_data, $results );
			// $shopify_inventory_level_quantity = $shopify_inventory_level_response->data()[0]->available;
			$shopify_inventory_level_quantity = isset( $response->inventory_quantity ) ? $response->inventory_quantity : $this->get_remote_variant_inventory_level( $response->inventory_item_id, $results );

			$updated_data = array(
				'sypr.ovc_id'						=> $ovc_product->ID,
				'sypr.sku'							=> $response->sku,
				'sypr.shopify_image_id'				=> $shopify_image_id,
				'sypr.shopify_inventory_qty'		=> $shopify_inventory_level_quantity,
				'sypr.variant_id'					=> $response->id,
				'sypr.shopify_parent_id'			=> $response->product_id,
				'sypr.shopify_inventory_item_id'	=> $response->inventory_item_id,
				'sypr.upc'							=> $response->barcode,
				'sypr.price_shopify'				=> $response->price,
				'sypr.shopify_compare_at_price'		=> $response->compare_at_price,
				'sypr.last_inventory_check'			=> current_time( 'mysql', 1 ),
				'sypr.shopify_updated_at'			=> date("Y-m-d H:i:s",strtotime( $response->updated_at ) ),
				'sypr.shopify_created_at'			=> date("Y-m-d H:i:s",strtotime( $response->created_at ) )
			);

			if( $update_last_sync ) {
				$updated_data['sypr.last_sync'] = current_time( 'mysql', 1 );
			}

			return $this->update_data( $updated_data, $results );
		}
	}

	public static function get_from_api_response( $response, &$results = false ) {
		OVC_Results::maybe_init( $results );

		// Try to get product row by Shopify Variant ID and then SKU
		$shopify_product_row = OVCDB()->get_row_by_valid_id( FS( 'shopify_products' ), 'variant_id', $response->id, $results );

		// If that didn't work, try by SKU
		if( !$shopify_product_row->exists() ) {

			$shopify_product_row = OVCDB()->get_row_by_valid_id( FS( 'shopify_products' ), 'sku', $response->sku, $results );

			// If that didn't work, try by pr.SKU
			if( !$shopify_product_row->exists() ) {

				$ovc_product = OVCDB()->get_row_by_valid_id( FS( 'ovc_products' ), 'sku', $response->sku, $results );

				if( $ovc_product->exists() && $ovc_id = $ovc_product->ID ) {

					$shopify_product_row = OVCDB()->get_row_by_valid_id( FS( 'shopify_products' ), 'ovc_id', $ovc_id, $results );
				}
				// If that didn't work, try to create the row
				else {

					$variant_data = array(
						'sypr.ovc_id'					=> $ovc_product->ID,
						'sypr.sku'						=> $response->sku,
						'sypr.variant_id'				=> $response->id,
						'sypr.shopify_parent_id'		=> $response->product_id
					);

					$shopify_product_row->update_data( $variant_data, $results );
				}
			}
		}

		return $shopify_product_row;
	}

	public function sync_img_metafield( &$results = false ) {

		OVC_Results::maybe_init( $results );

		if( $this->exists() 
			&& $this->variant_id
			&& $this->ovc_id
		) {

			$ovc_product = $this->ovc_product( $results );

			$request_data = array(
				'url_params'	=> array(
					'shopify_product_id'	=> $this->data( 'sypr.shopify_parent_id' ),
					'shopify_variant_id'	=> $this->variant_id
				),
				'request_body'	=> array( 'metafield' => $this->filter_shopify_variant_metafields( $results ) )
			);

			$api_response = OVC_API_Shopify::request( 'update_variant_metafields', $request_data, $results );

			if( !$api_response->succeeded() ) {
				$results->error( "Metafield sync failed for {$this->data('pr.sku')}, Shopify Product {$this->variant_id} \r\n" );
			}
			
			$updated_data = array(
				'sypr.last_image_sync'			=> current_time( 'mysql', 1 ),
			);

			return ( $results->success = $this->update_data( $updated_data, $results ) );
		}
		else {
			return $results->error( "Cannot sync variant images without Shopify Variant ID and OVC ID" );
		}
	}

	public function update_local_shopify_variant_data_from_remote() {

		$request_data['url_params']['shopify_variant_id'] = $this->variant_id;

		$product_response = OVC_API_Shopify::request( 'get_variant', $request_data, $results );

		if( $product_response->succeeded() ) {

			if( $variant_data = $product_response->data() ) {

				$shopify_variant = OVC_Row_Shopify_Products::get_from_api_response( $variant_data, $results );

				if( !$shopify_variant->exists() ) {

					return false;
				} else {
					$shopify_variant->handle_api_response( $product_response, $results, false );

					return $variant_data;
				}
			}
		}
	}

	public function filter_shopify_variant_metafields( &$results = false ) {
		OVC_Results::maybe_init( $results );

		$row = $this->ovc_product( $results );

		// Determine img_set shopify_image_ids
		$shopify_parent = $row->get_parent()->get_joined_data( FS( 'shopify_parents' ) );

		$image_set = $row->get_joined_data( FS( 'image_sets' ) );

		$shopify_images_filter = array( 
			'var' 	=> '::[post_id]',
			'op'	=> '!=',
			'value' => ''
		);

		$shopify_images = $shopify_parent->get_children( FS( 'shopify_images' ), 'raw_data', $results, $shopify_images_filter );

		$metafield_images = array();

		if( $shopify_parent->exists() && $image_set->exists() && count( $shopify_images ) ) {

			$img_keys = array( 'img_main', 'img_swatch', 'img_case', 'img_sizeguide', 'img_gallery_ids' );

			foreach( $img_keys as $img_key ) {

				if( $img_key === 'img_main' && $marketplace_featured = $image_set->data( 'img.marketplace_featured') ) {
					$img_post_ids = explode( ',', $marketplace_featured );
				} else {
					$img_post_ids = explode( ',', $image_set->data( "img.{$img_key}" ) );
				}

				$img_key_shopify_ids = array();

				foreach( $img_post_ids as $img_post_id ) {

					foreach( $shopify_images as $shopify_image ) {

						if( $shopify_image['syimgs.post_id'] == $img_post_id ) {

							$img_key_shopify_ids[] = $shopify_image['syimgs.shopify_image_id'];
							break;
						}
					}
				}

				if( !empty( $img_key_shopify_ids ) ) {

					$metafield_images[ $img_key ] = count( $img_key_shopify_ids ) > 1 ? $img_key_shopify_ids : $img_key_shopify_ids[0];
				}

			}


		}

		$metafield = array(
			'key'		=> 'img_data',
			'value'		=> json_encode( $metafield_images ),
			'value_type'=> 'string',
			'namespace'	=> 'variant_images'
		);
		
		return $metafield;
	}

	public function get_online_stock() {

		$ovc_pr = $this->ovc_product();

		// If the OVC Product exists, return the stock otherwise return a stock of 0
		$shopify_stock = $ovc_pr->exists() ? $ovc_pr->get_online_stock() : 0;

		return $shopify_stock;
	}

	public function ovc_product( &$results = false ) {
		
		return $this->get_joined_data( FS( 'ovc_products' ), $results );
	}

}
