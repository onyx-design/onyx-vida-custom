<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Row_ovc_images extends OVC_Row {

	public function before_delete( &$results = false ) {
		OVC_Results::maybe_init( $results );

		if( file_exists( $this->data( 'pic.path' ) ) && unlink( $this->data( 'pic.path' ) ) ) {
			$results->notice( 'Removed original image file.' );
		}

		if( file_exists( $this->data( 'pic.image_3_4_path' ) ) && unlink( $this->data( 'pic.image_3_4_path' ) ) ) {
			$results->notice( 'Removed 3:4 image file.' );
		}

		if( file_exists( $this->data( 'pic.image_override_path' ) ) && unlink( $this->data( 'pic.image_override_path' ) ) ) {
			$results->notice( 'Removed override image file.' );
		}
	}

	public function remove_image( $image_id = 0, $results = false ) {
		OVC_Results::maybe_init( $results );

		if( $image_id && ( $image_id == $this->data( 'pic.post_id' ) ) ) {

			$this->delete( $results, true );
		}
	}

	public function regenerate_image( $results = false ) {
		OVC_Results::maybe_init( $results );

		$img_path = $this->data( 'pic.path' );

		if( !file_exists( $img_path ) ) {

			$this->delete( $results );

			$results->error( 'Image has been removed or no longer exists. OVC Image has been deleted.' );

			return false;
		} else {

			$ovc_img = new OVC_Image( $img_path );

			$updated_data = array();

			$this->analyze_image( $ovc_img, $updated_data, $results );

			$this->update_data( $updated_data, $results );

			$results->main_msg = $results->success ? "Image Regenerated Successfully." : "Image Regeneration Failed.";
		}
	}

	public function analyze_image( $ovc_img, &$updated_data = array(), $results = false ) {
		OVC_Results::maybe_init( $results );

		$crop_mode = $this->data( 'pic.crop_mode' ) ? : 'Auto';
		$crop_focus = $this->data( 'pic.crop_focus' ) ? : 'Center Middle';

		$updated_data['crop_mode']			= $crop_mode;
		$updated_data['crop_focus']			= $crop_focus;
			
		$updated_data['width']				= $ovc_img->get_width();
		$updated_data['height']				= $ovc_img->get_height();
		$updated_data['raw_edges_uncropped']= $ovc_img->get_original_raw_edges();
		$updated_data['raw_top_white']		= number_format( $ovc_img->get_edge_top( 'I', true, 'original_imagick' ), 2 );
		$updated_data['raw_bottom_white']	= number_format( $ovc_img->get_edge_bottom( 'I', true, 'original_imagick' ), 2 );
		$updated_data['raw_left_white']		= number_format( $ovc_img->get_edge_left( 'I', true, 'original_imagick' ), 2 );
		$updated_data['raw_right_white']	= number_format( $ovc_img->get_edge_right( 'I', true, 'original_imagick' ), 2 );

		$ovc_img->analyze_cropping( $crop_mode, $crop_focus );

		$new_path = $ovc_img->cropped_path( $this );

		if( $old_path = $this->data( 'pic.image_3_4_path' ) ) {
			if( file_exists( $old_path ) ) {
				unlink( $old_path );
			}
		}

		$ovc_img->write_image( OVC_IMAGE_DIR . '/' . $new_path );

		$updated_data['image_3_4_path']			= $new_path;
		$updated_data['raw_edges_cropped']		= $ovc_img->get_raw_edges();
		$updated_data['crop_left_white']		= number_format( $ovc_img->get_edge_left( 'I' ), 2 );
		$updated_data['crop_right_white']		= number_format( $ovc_img->get_edge_right( 'I' ), 2 );

		if( 'Approved' !== $this->data( 'pic.status' ) ) {
			$updated_data['status'] = $ovc_img->get_status();
		}

		return $updated_data;
	}

	public function upload_override( $file, $results = false ) {
		OVC_Results::maybe_init( $results );

		if( !$this->exists() || !$file ) {
			return $results->error( 'Upload Override Image Failed! Row does not exist or uploaded file is invalid' );
		}

		$tmp_name = $file['uploaded_file']['tmp_name'];
		$file_name = $file['uploaded_file']['name'];
		$file_type = pathinfo( $file_name, PATHINFO_EXTENSION );

		if( in_array( $file_type, array( 'jpeg', 'jpg' ) ) ) {

			// dev:todo Check that size is correct
			$ovc_img = new OVC_Image( $tmp_name );

			if( !$ovc_img->is_3_4_aspect_ratio() ) {

				return $results->error( 'Upload Override Image Failed! Image is not 3:4 Aspect Ratio' );
			}

			$uploaded = $ovc_img->write_image( OVC_IMAGE_DIR . '/' . $ovc_img->override_path( $this ) );
			// $uploaded = move_uploaded_file( $tmp_name, $destination );

			if( !$uploaded ) {

				return $results->error( 'Upload Override Image Failed! There was an error uploading the file from TEMP' );
			}

			$updated_data = array(
				'image_override_path' 	=> $ovc_img->override_path( $this ),
				'status'				=> 'Needs Review'
			);

			$this->update_data( $updated_data, $results );
		}
		else {
			// Display error for only JPEG and JPG allowed
			return $results->error( 'Upload Override Image Failed! File must be of type JPEG' );
		}

		return !$results->has_error;
	}

	public function delete_override( $results = false ) {
		OVC_Results::maybe_init( $results );

		// If row doesn't exist, return 0 ( zero images deleted )
		if( !$this->exists() ) {
			return 0;
		}

		$updated_data = array(
			'image_override_path'	=> ''
		);

		unlink( $this->data( 'pic.image_override_path' ) );

		return $this->update_data( $updated_data, $results );
	}

	public function get_image_name() {

		$img_path = $this->data( 'pic.path' );

		if( $img_path && file_exists( $img_path ) ) {

			$ovc_img = new OVC_Image( $img_path );

			return $ovc_img->name( true );
		}

		return false;
	}

	public function get_walmart_image_url() {

		if( !$this->is_image_approved() ) {
			return false;
		}

		$filename = $this->data( 'pic.image_override_path' ) ?: $this->data( 'pic.image_3_4_path' );
		$ovc_url = get_ovc_image_url( $filename ) ?: wp_get_attachment_url( $this->data( 'pic.post_id' ) );

		$walmart_url = '10000000364/' . $filename;

		// If the image hasn't been synced to Walmart, return the OVC Url, else return the Walmart Path
		return $this->image_needs_wa_ftp_sync() 
			&& ( 
				!$this->data( 'pic.last_wa_ftp_sync' ) 
				|| ( '0000-00-00 00:00:00' == $this->data( 'pic.last_wa_ftp_sync' ) )
			)
			? $ovc_url : '10000000364/' . $filename;
	}

	public function image_needs_wa_ftp_sync() {

		return strtotime( $this->data( 'pic.last_wa_ftp_sync' ) ) < strtotime( $this->data( 'pic._meta_updated' ) );
	}

	public function is_image_approved() {

		if( $this->exists() && 'Approved' === $this->data( 'pic.status' ) ) {
			return true;
		}

		return false;
	}

	public function filter_ovc_image_url() {

		if( !$this->is_image_approved() ) {
			return wp_get_attachment_url( $this->data( 'pic.post_id' ) );
		}

		$ovc_img = $this->data( 'pic.image_override_path' ) ? : $this->data( 'pic.image_3_4_path' );

		if( $ovc_img ) {
			return ovc_content_url_from_path( $ovc_img );
		}

		// Return the WP Image if there isn't an override or generated image
		return wp_get_attachment_url( $this->data( 'pic.post_id' ) );
	}
}