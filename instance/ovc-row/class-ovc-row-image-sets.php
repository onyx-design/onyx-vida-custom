<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Row_image_sets extends OVC_Row {

	protected $image_id_fields = array( 'main', 'oms1', 'oms2', 'case', 'amazon', 'sizeguide', 'swatch', 'gallery_ids', 'marketplace_featured' );

	public function get_image_ids( $fields = 'all', $unique = true ) {

		$fields = is_array( $fields ) ? $fields : $this->image_id_fields;

		$image_ids = array();
			
		foreach( $fields as $field_name ) {

			$field_name = 
				( 0 === strpos( $field_name, 'img_' ) ) ?
				$field_name :
				'img_' . $field_name;

			$field_image = $this->$field_name;

			if( !$field_image ) {
				continue;
			}

			if( 'img_gallery_ids' == $field_name ) {
				$image_ids = array_merge( $image_ids, explode( ',', $field_image ) );
			}
			else {
				$image_ids[] = $field_image;
			}

		}

		$image_ids = $unique ? array_unique( $image_ids ) : $image_ids;

		return $image_ids;
		
	}

	/**
	 * Remove Image from Row if it exists
	 * 
	 * @param 	int 				$image_id 
	 * @param 	OVC_Results|bool 	&$results 
	 */
	public function remove_image( $image_id = 0, &$results = false ) {
		OVC_Results::maybe_init( $results );

		if( $image_id ) {

			$row_image_ids = $this->get_image_ids();

			if( in_array( $image_id, $row_image_ids ) ) {

				foreach( $this->image_id_fields as $image_id_field ) {

					$field_name = 'img_' . $image_id_field;

					if( $this->data( 'img.' . $field_name ) ) {

						if( 'img_gallery_ids' == $field_name ) {

							$image_ids = $this->get_image_ids( 'gallery_ids', false );

							$updated_data = array(
								'img.' . $field_name => implode( ',', array_diff( $image_ids, array( $image_id ) ) )
							);
						} else {

							$updated_data = array(
								'img.' . $field_name => 0
							);
						}
						break;
					}
				}

				$this->update_data( $updated_data, $results );
			}
		}
	}

	public function get_parent_sku( &$results = false ) {
		OVC_Results::maybe_init( $results );

		$children = $this->get_children( FS( 'pr' ), 'object', $results );

		foreach( $children as $child ) {

			return $child->parent_sku;
		}

		return false;
	}
}