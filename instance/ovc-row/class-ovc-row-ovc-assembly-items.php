<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Row_ovc_assembly_items extends OVC_Row {

	public function get_assembly( $results = false ) {
		OVC_Results::maybe_init( $results );

		return $this->data( 'asmi.assembly_id' ) ? $this->get_joined_data( FS( 'asm' ), $results ) : null;
	}

	public function get_product( $results = false ) {
		OVC_Results::maybe_init( $results );

		return $this->data( 'asmi.ovc_id' ) ? $this->get_joined_data( FS( 'ovc_products' ), $results ) : null;
	}

	public function get_assembly_product() {

		return $this->get_assembly()->get_product();
	}

	public function get_max_depth() {

		return $this->get_assembly_product()->get_max_depth() + 1;
	}

	public function get_potential_stock( $results = false ) {

		// Get maximum (whole) amount of this assembly item if we disassembled the potential stock of this assembly parent's product
		return floor( $this->get_assembly_product()->get_potential_stock( $this ) * $this->data( 'asmi.quantity' ) );
	}
}