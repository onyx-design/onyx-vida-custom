<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVCOP_amazon_export extends OVCOP {
	public $type = 'amazon_export';

	// Simple operation-specific properties (re-initialized for each commence_operation request)
	public $export_field_set = array(); // Amazon Field Map (with fieldmeta information)
	public $excel = false;

	// Semi-magic properties via $op_data['data']
	public $export_fs_name = 'amazon_2019_02';
	public $template_file;
	public $parent_skus;
	public $exported_ids;

	// Operation Type Specific Initialization
	public static function extra_init( $ovcop ) {
		global $wpdb;

		// Initialize operation data based on configuration options
		$ovcop->parent_skus = array_map( 'sanitize_text_field', array_map( 'trim', explode( ",", $ovcop->config( 'filter_parent_skus' ) ) ) );

		if( !is_array( $ovcop->parent_skus ) || 0 == count( $ovcop->parent_skus ) ) {
			$ovcop->abort_operation( "Invalid entry in Parent SKUs field." );
		}

		// Initialize Output File
		if( 'excel' == $ovcop->config( 'format' ) ) {

			if( 'excel_2019_02_08' == $ovcop->config( 'excel_template_file' ) ) {
				$template_filename = 'amazon-2019-02-08.xls';
				$template_filetype = 'xls';

				$ovcop->export_fs_name = 'amazon_2019_02_08';

			} elseif( 'excel_2016_07_21' == $ovcop->config( 'excel_template_file' ) ) {
				$template_filename = 'amazon-2016-07-21.xls';
				$template_filetype = 'xls';

				$ovcop->export_fs_name = 'amazon_new';
			} else {
				$ovcop->results->success = false;
				$ovcop->results->main_msg = "Initialization failed. Invalid Excel template.";
				return false;
			}
			
			$ovcop->new_file( $template_filetype, $template_filetype, true );
			$ovcop->template_file = OVCOP_TEMPLATE_DIR . '/' . $template_filename;

			if( !file_exists( $ovcop->template_file )
				//|| !copy( $ovcop->template_file, OVCOP_CONTENT_DIR . '/' . $ovcop->csv_file )
			) {
				$ovcop->results->success = false;
				$ovcop->results->main_msg = "Initialization failed. Excel template not found.";
				return false;
			}
		}
		else if( 'csv' == $ovcop->config( 'format' ) ) {
			// Create CSV and set it to be the current op_file
			$ovcop->new_file( 'csv', 'csv', true );
			
			// Update csv_file name in OVCOP table
			//$ovcop->csv_file = $ovcop->generate_file_name();
			// Send initialization response 
		}
		else {
			$ovcop->results->success = false;
			$ovcop->results->main_msg = "Initialization failed. Invalid output format type.";
			return false;
		}

		//$wpdb->update( 'wp_ovcop_log', array( 'csv_file' => $ovcop->csv_file ), array( 'ID' => $ovcop->ID ) );

		$ovcop->exported_ids = array();

		$ovcop->results->success = true;
		$ovcop->results->main_msg = "Amazon Export operation initialized successfully";

		// Tell OVCOP to automatically commence the operation
		return true;
	}


	public function __construct( $request, $args ) {
		$this->child_construct( $request, $args );

		// Initialize Amazon Field Set
		$this->export_field_set = OVCSC::get_field_set( $this->export_fs_name );

		// Maybe initialize amazon Excel Template PHPExcel Object
		if( 'excel' == $this->config( 'format' ) ) {

			$this->excel = $this->get_php_excel( $this->template_file );

			$this->excel->setActiveSheetIndexByName( $this->export_field_set->fs_meta( 'excel_worksheet' ) );
		}

		$this->log( count( $this->parent_skus ) . " Parent SKUs remaining." );

		$this->export();
	}

	public function export() {

		if ( !$this->jumped && !$this->excel ) {
			// Write headers line to CSV
			$this->write_csv_line_from_array( array_map( array( $this, 'csv_wrap_value' ), $this->export_field_set->get_fields() ) );
		}

		global $wpdb;

		$qry = "SELECT ID FROM wp_ovc_products WHERE ";

		if( 'parent_skus' == $this->config( 'sku_filter' ) ) {
			$qry .= "parent_sku IN( \"" . implode( "\",\"", $this->parent_skus ) . "\") AND ";
		} else if( 'custom_selection' == $this->config( 'sku_filter' ) ) {
			$qry .= "ID IN(" . implode( ',', get_ovc_user_custom_selection( $this->user_id, 'ovc_products' ) ) . ") AND ";
		} else {
			$this->abort_operation( 'Invalid SKU Filtering Configuration' );
		}

		$qry .= "size != 'CS' AND sync_oms = 1 ORDER BY parent_sku ASC";

		$ovc_ids = $wpdb->get_col( $qry );

		$this->log( 'Beginning Export. ' . count( $ovc_ids ) . ' OVC IDs remaining to be exported.' );

		$prfs = OVCSC::get_field_set( 'ovc_products' );

		// xlsx only
		$excel_row_index = $this->export_field_set->fs_meta( 'start_row' );

		if( isset( $ovc_ids[0] ) ) {
			$results = new OVC_Results;

			$ovcp = OVCDB()->get_row( $prfs, $ovc_ids[0], $results );

			$parent_sku = $ovcp->data( 'pr.parent_sku' );
		} else {

			$parent_sku = '';
		}

		foreach( $ovc_ids as $ovc_id ) {
			$results = new OVC_Results;

			$ovcp = OVCDB()->get_row( $prfs, $ovc_id, $results );
			$csv_row_values = array();

			// Ignore cases, this is done in the query but we want to make sure here
			if( $ovcp->data( 'pr.size' ) == 'CS' ) {
				continue;
			}

			foreach( $this->export_field_set->get_fields() as $field ) {

				$am_value = $ovcp->get_external_value( $field, $results );

				if( false === $am_value ) {
					$results->error( "ERROR! OVC ID: {$ovc_id} - SKU: {$ovcp->data( 'pr.sku' )} - amazon_value is false for field {$field}" );
				}
				// CSV Only
				else if( !$this->excel ) {
					$escape_csv_value = ! (bool) $field->meta( 'csv_no_escape' );
	
					$am_value = $this->csv_wrap_value( $am_value, $escape_csv_value );
				}

				$csv_row_values[] = strval( $am_value );
			}

			foreach( $results->get_msgs() as $msg ) {
				$this->log( $msg );
			}

			// CSV Only
			if( !$this->excel ) {
				//$this->write_csv_line_from_array( $export_row_values );
				$this->write_csv_line_from_array( $csv_row_values );
			}
			// XLS/X Only
			else {

				// This is to add spaces between parent SKUs in the excel worksheet for easy viewing
				if( ( $new_parent_sku = $ovcp->data( 'pr.parent_sku' ) ) != $parent_sku ) {

					$parent_sku = $new_parent_sku;
					$this->excel->getActiveSheet()->fromArray( array(), '', "A{$excel_row_index}" );
					$excel_row_index++;
				}

				// Loop over every field and add it to the document
				$this->excel->getActiveSheet()->fromArray( $csv_row_values, '', "A{$excel_row_index}" );
			}

			$this->exported_ids[] = $ovcp->ID;
			$excel_row_index++; // xlsx only
		}

		if( $this->excel ) { // xlsx only
			
			// $product_row_count = intval( $this->export_field_set->fs_meta( 'start_row' ) ) + count( $this->exported_ids );
			$product_row_count = intval( $this->export_field_set->fs_meta( 'start_row' ) ) + $excel_row_index;
			// Loop over Amazon External Fields to get the number
			foreach( $this->export_field_set->get_fields() as $field ) {

				$column_string = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex( $field->meta( 'order' ) );
				$format_code = $field->meta( 'excel_format' ) ?: '###0';

				// Get Var Type and set style based on that var type
				$this->excel->getActiveSheet()->getStyle( $column_string . $this->export_field_set->fs_meta( 'start_row' ) . ':' . $column_string . $product_row_count )
					->getNumberFormat()
					->setFormatCode( $format_code );

				$this->excel->getActiveSheet()->getStyle( $column_string . $this->export_field_set->fs_meta( 'start_row' ) . ':' . $column_string . $product_row_count )
					->getAlignment()
					->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT );
			}

			$file_type = \PhpOffice\PhpSpreadsheet\IOFactory::identify( $this->template_file );
			$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter( $this->excel, $file_type );
			//$writer->save( OVCOP_CONTENT_DIR . '/' . $this->csv_file );
			$writer->save( $this->op_file()->path() );
		}

		$this->successful_finish();
	}

	public function before_successful_finish() {
		// Set stat for final report
		$this->init_stat( 'exported_products', '# of Products Exported' );
		$this->stat( 'exported_products', count( $this->exported_ids ) );
	}

	public function after_successful_finish() {
		// Output link to download CSV
		$this->log();
		//$this->log( "Download exported CSV here: " . ovc_content_url_from_path( OVCOP_CONTENT_DIR . '/' . $this->csv_file ) );
		$this->log( "Download exported file here: " . $this->op_file()->url() );
		$this->log();
	}
}	