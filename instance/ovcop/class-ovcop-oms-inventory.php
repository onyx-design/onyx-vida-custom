<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

error_reporting(E_ALL); 
ini_set("log_errors", 1); 
ini_set("memory_limit", '256M' );
ini_set("auto_detect_line_endings", true);

class OVCOP_oms_inventory extends OVCOP {
	public $type = 'oms_inventory';
	public $op_step = 3;

	// Simple operation-specific properties (re-initialized for each commence_operation request)
	public $oms_fm = array();
	public $pr_fs  = array();
	public $row;

	// Semi-magic properties via $op_data['data']
	//public $ovc_ids = array();
	public $skip_past_ovc_id = 0;
	public $remote_file_name = '';
	public $remaining_ovc_product_ids;

	// DEV TEMP
	public $oms_keys = array();


	public static function extra_init( $ovcop ) {
		global $wpdb;

		/*/ Update csv_file name in OVCOP table
		$ovcop->csv_file = $ovcop->generate_file_name();
		$wpdb->update( 'wp_ovcop_log', array( 'csv_file' => $ovcop->csv_file ), array( 'ID' => $ovcop->ID ) );
		*/

		// Initialize CSV
		$ovcop->new_file( 'csv', 'csv', true );

		// Set the op_step back to 2 because we'll need to stay on the second step on the OVCOP frontend
		//$ovcop->op_step = 2;


		// INITIALIZE CONFIGURATION VARIABLES
		// Determine Import CSV Source (VIDA FTP or Manual Upload)
		if( 'ftp' == $ovcop->config('file_source') ) {
			//$ovcop->meta( 'vida_ftp_csv_path', "O:\\ovc\\{$ovcop->ftp_op_dir()}\\{$ovcop->op_file()->name()}" );


			$ftp = new OVC_OMS_FTP;

			if( !$ftp->check_connection() ) {
				$ovcop->abort_operation( "FTP File Transfer failed: Unable to establish FTP connection." );
			}

			$ovcop->remote_file_name = $ftp->get_newest_file( '/oms_inventory_update' );

			if( !$ovcop->remote_file_name ) {
				$ovcop->abort_operation( "No OMS Inventory Update CSV found on VIDA OMS FTP" );
			}


			$newest_file = '/oms_inventory_update/' . $ovcop->remote_file_name;

			// Check that this file is newer than the last one used from FTP
			$this_mdtm = intval( $ftp->mdtm( $newest_file ) );
			$last_mdtm = intval( get_option( 'ovc_oms_inventory_last_mdtm', 0 ) );

			if( $this_mdtm < $last_mdtm ) {

				$ovcop->abort_operation( "Newest CSV on remote FTP is older than the last CSV imported via FTP. Aborting operation to avoid updating inventory with old data." );
			}
			else {

				$ovcop->log( "FTP OMS Inventory file modified time check passed: newest file mdtm: {$this_mdtm} - last oms_inventory FTP mdtm: {$last_mdtm}" );
				update_option( 'ovc_oms_inventory_last_mdtm', $this_mdtm );
			}

			$ovcop->log( "FTP file: " . $newest_file );
			$ovcop->log( $ovcop->op_file()->path() );

			if( $ftp->get( $newest_file, $ovcop->op_file()->path() ) ) {
				$ovcop->log( "CSV FTP transfer completed successfully. Remote file: " . $ovcop->remote_file_name );
				$ovcop->results->main_msg = "OMS Inventory Update initialized. Confirm FTP CSV.";	
			}
			else {
				foreach( $ftp->results->get_msgs( 'error' ) as $ftp_error_msg ) {
					$ovcop->log( $ftp_error_msg );
				}
				$ovcop->abort_operation( "CSV transfer via FTP failed" );
			}
			
			

			//$ovcop->response_data['html'] = $ovcop->load_template( $ovcop->type . '-ftp_upload' );
		}
		else if ( 'db' == $ovcop->config('file_source') ) {
			$ovcop->results->main_msg = "OMS Import initialized. Reading existing database information...";
		}
		else if ( 'upload' == $ovcop->config('file_source') ) {
			//$ovcop->response_data['do_file_upload'] = true;

			//$ovcop->response_data['csv_upload_name'] = $ovcop->op_file()->name();

			$ovcop->op_file()->write( stripslashes( $ovcop->config( 'import_csv_upload' ) ) );

			// Delete uploaded CSV data from op_data so we don't save huge text files to the database
			//ovcop->config( 'import_csv_upload', null ); //dev:improve - maybe adjust OVCOP so we don't ever have to save this to the DB in the first place?


			$ovcop->results->main_msg = "OMS Import initialized. Uploading CSV File...";
		}

		// Log import_mode
		$ovcop->log( "IMPORT MODE: " . $ovcop->config( 'import_mode' ) );

		// Determine if we are going to be doing data checks
		if( $ovcop->config( 'cron_queued' ) ) {

			$last_auto_error_check = get_option( 'ovcop__oms_inventory__check_data_errors' );

			if( !$last_auto_error_check 
				|| ( time() - $last_auto_error_check ) > ( 24 * 60 * 60 )
			) {

				update_option( 'ovcop__oms_inventory__check_data_errors', time() );
				$ovcop->config( 'check_data_errors', true );
			}
		}

		$check_errors_status = 
			$ovcop->config( 'check_data_errors' )
			? 'ENABLED'
			: 'DISABLED';

		$ovcop->log( 'OVC Data Error checks are ' . $check_errors_status );
	

		
		


		// INITIALIZE SEMI-MAGIC DATA VARIABLES
		$ovcop->skip_past_ovc_id = 0;
		//$ovcop->ovc_ids = array();
		$ovcop->remaining_ovc_product_ids = null;


		/*/ INITIALIZE STATS VARIABLES
		$ovcop->init_stat( 'valid', 'Valid Rows' );
		$ovcop->init_stat( 'invalid', 'Invalid Rows' );
		$ovcop->init_stat( 'ignored', 'Ignored Rows' );
		$ovcop->init_stat( 'warning', 'Total Warnings' );
		$ovcop->init_stat( 'updated_stock', 'Stock Updates' );
		*/

		// Send initialization response
		//$ovcop->response_data['log_once'] = true;
		return $ovcop->results->success = true;
		
		// Do not auto-commence the operation
		//return false;
	}

	public function __construct( $request, $args ) {
		$this->child_construct( $request, $args );

		// BUILD AND VALIDATE OMS / OVC FIELD MAP (built before $request switch because it's needed in init_source_file as well as commence_operation)
		$this->oms_fm = OVCSC::get_field_set( 'oms_inventory' ); //dev:improve
		$this->pr_fs = OVCSC::get_field_set( 'ovc_products' );

		$this->run_operation();
	}

	protected function run_operation() {

		// Maybe preload active data errors
		if( $this->config( 'check_data_errors' ) ) {
			OVC_Row_data_errors::enable();
			OVC_Row_data_errors::load_active_errors();
		}
		

		// Skip CSV -> DB import and high-level verification if we have already done it (if we have jumped)
		if( !$this->jumped ) {


			if( 'db' == $this->config('file_source') ) {

				$this->log( "Skipping CSV import. Reading existing OMS import data from database." );
			}
			else {

				$this->initialize_csv_file();

				$this->import_csv_db();	
			}
			
			$this->verify_csv_data();

		}

		$this->import_csv_rows();
		/* */

		// Update Potential Quantity
		$this->update_potential_quantities();

		$this->successful_finish();
	}

	// INITIALIZATION METHODS

	protected function initialize_csv_file() {
		$csv_is_valid = true;
		$csv_line_count = 0;
		$csv_max_chars = 0;

		if( !$this->op_file() ) {
			$this->abort_operation( "Invalid CSV file detected, aborting operation." );
		}
		
		// ANALYZE AND VALIDATE CSV
		$csv_stream = fopen( $this->op_file()->path(), 'r' );

		// COUNT # Lines in the CSV (Actual number of lines; Includes First row of column headers), also count max chars in any line
		while( ($line = fgets( $csv_stream )) !== false ) {
			$csv_line_count++;
			$csv_max_chars = ( (strlen($line) + 2) > $csv_max_chars ) ? strlen($line) + 2 : $csv_max_chars;
		}
		$this->meta( 'csv_max_chars', $csv_max_chars );

		if( $csv_line_count <= 1 ) {
			fclose($csv_stream);
			$this->abort_operation( "Invalid row count in CSV. Number of rows detected (including field headings): {$csv_line_count}" );
		} else {
			$this->log( "Number of CSV Rows Detected (Including Column Headers): {$csv_line_count}" );
			$this->log( "Maximum number of characters in any single line: {$csv_max_chars}" );
		}

		// MAP and Validate CSV Fields to OMS Import Field Map
		rewind( $csv_stream ); // Set file stream pointer back to beginning of file
		$csv_fields = fgetcsv( $csv_stream, $csv_max_chars ); // Read first line of CSV as an array

		if( $csv_fields !== array_values( $this->oms_fm->get_single_meta_key_values( 'csv_header' ) ) ) {
			fclose($csv_stream);
			$this->abort_operation( "Invalid CSV headers detected in import CSV." );
		}

		fclose($csv_stream);
		return $csv_is_valid;
	}

	protected function import_csv_db() {

		$this->check_user_abort( true );

		$this->log( "Importing CSV data into database table..." );

		// init
		$this->oms_keys = $this->oms_fm->get_fields('local_name');

		global $wpdb;

		// Empty & reset the OMS CSV data table
		$wpdb->query( "TRUNCATE TABLE wp_ovc_oms_inventory" );
		$wpdb->query( "ALTER TABLE wp_ovc_oms_inventory AUTO_INCREMENT = 2" );


		// fix stupid windows/OMS double quote escaping
		//file_put_contents( $this->op_file()->path(), str_replace('\\', '\\\\', file_get_contents( $this->op_file()->path() ) ) );

		//file_put_contents( $this->op_file()->path(), preg_replace('/((\.?0+)(?=,))|( )/', '', file_get_contents( $this->op_file()->path() ) ) );

		$csv_stream = fopen( $this->op_file()->path(), 'r' );

		fgets( $csv_stream, $this->meta('csv_max_chars') ); // Skip first line

		$line_index = 1;
		$chunk_size = 250;
		$chunk_data = array();

		// Import CSV Data into database in chunks
		while( ($row = fgetcsv( $csv_stream, $this->meta('csv_max_chars') ) ) !== FALSE ) {

			$line_index++;

			if( count( $row ) > 1 ) {
				$chunk_data[] = $row;
			}
			else {
				continue;
			}

			// Write to database in chunks of $chunk_size # of lines
			if( 0 == count( $chunk_data ) % $chunk_size ) {

				$this->check_user_abort();

				$this->write_csv_chunk_db( $chunk_data, $line_index );
			}
		}

		// Write final chunk into database
		$this->write_csv_chunk_db( $chunk_data, $line_index );

		fclose( $csv_stream );

		$this->log( "CSV data imported into database table successfully. line_index: {$line_index} - Current request elapsed exec time (seconds): " . ox_exec_time() );
	}


	private function write_csv_chunk_db( &$chunk_data, $line_index ) {

		if( !is_array( $chunk_data ) || !count( $chunk_data ) ) {
			return 0;
		}

		global $wpdb;

		$sql = "INSERT INTO wp_ovc_oms_inventory (" . implode( ',', $this->oms_keys ) . ") VALUES";

		foreach( $chunk_data as $row ) {
			$sql .= '("' . implode( '","', $row ) . '"),';
		}

		$sql = rtrim( $sql, ',' );
		$sql .= ';';

		$result = $wpdb->query( $sql );

		$chunk_data = array();

		if( false === $result ) {

			$this->abort_operation( "CSV to DB import failed at CSV line {$line_index}\r\nQuery that failed:\r\n{$sql}" );
		}
		else {
			return $result;
		}
	}

	public static function archive_file( $filename, $ftp = null ) {

		// Make sure FTP connection is initialized
		if( !( $ftp instanceof OVC_OMS_FTP ) ) {
			$ftp = new OVC_OMS_FTP;
		}

		// Make sure we are in the correct directory
		$ftp->chdir( '/oms_inventory_update' );

		return $ftp->rename( $filename, '/oms_inventory_update/archive/' . $filename );
	}

	private function verify_csv_data() {

		$this->check_user_abort();

		global $wpdb;

		$this->log();
		$this->log();
		$this->log();
		$this->log( "Beginning high-level data verification checks." );

		$general_data_results = new OVC_Results();

		// Determine total number of rows in 
		$count_oms_inventory_update = $wpdb->get_var( "SELECT COUNT(*) FROM wp_ovc_oms_inventory" );
		$general_data_results->notice( "Total # of rows in oms inventory update table: " . $count_oms_inventory_update );

		// Determine number of rows without OVC IDs
		$count_oms_skus_no_ovc_ids = $wpdb->get_var( "SELECT COUNT(*) FROM wp_ovc_oms_inventory WHERE ovc_id = 0" );
		$general_data_results->notice( "Total # of rows without OVC IDs: " . $count_oms_skus_no_ovc_ids );

		// Determine if any OMS Inventory rows without OVC IDs have SKUs that are in pr.sku or pr.oms_sku_confirmed
		OVC_Row_error_codes::check_general_error_code( 'oms__missing_ovc_ids', $general_data_results, true );
		

		// Determine if any OVC IDs in OMS Inventory do not exist in OVC
		OVC_Row_error_codes::check_general_error_code( 'oms__invalid_ovc_ids', $general_data_results, true );
		// $count_null_import_ovc_ids = $wpdb->get_col( "SELECT oi.ovc_id FROM wp_ovc_oms_inventory oi LEFT JOIN wp_ovc_products pr ON oi.ovc_id = pr.ID WHERE pr.ID IS NULL AND oi.ovc_id != 0" );
		// if( $count_null_import_ovc_ids ) {
		// 	$this->log( "WARNING! The following OVC IDs were found in the OMS Inventory import file but do not exist in OVC! \r\n" . implode( "\r\n", $count_null_import_ovc_ids ) );
		// }

		// Check if there are any duplicate OVC IDs
		OVC_Row_error_codes::check_general_error_code( 'oms__duplicate_ovc_ids', $general_data_results, true );
		// oms__duplicate_ovc_ids
		// $duplicate_ovc_ids = $wpdb->get_results( "SELECT *, COUNT(*), GROUP_CONCAT(sku SEPARATOR ', ') AS 'mismatch_skus' FROM `wp_ovc_oms_inventory` WHERE ovc_id != 0 GROUP BY ovc_id HAVING COUNT(*) > 1", ARRAY_A );
		// if( $duplicate_ovc_ids ) {
		// 	$this->log( "WARNING! Duplicate OVC IDs in OMS! Please review carefully and delete or fix SKUs in OMS." );
		// 	$this->log();
		// 	$this->log( str_pad( 'OVC ID', 12 ) . 'OMS SKUs' );

		// 	foreach( $duplicate_ovc_ids as $duplicate_row ) {
		// 		$this->log( str_pad( $duplicate_row['ovc_id'], 12 ) . $duplicate_row['mismatch_skus'] );
		// 	}

		// 	$this->log();
		// 	$this->log();
		// }

		$this->log_results_msgs( $general_data_results );
		$this->log( "CSV data-verification checks complete. Current request elapsed exec time (seconds): " . ox_exec_time() );

		return true;
	}

	protected function import_csv_rows() {

		global $wpdb;		

		if( !$this->jumped ) {

			$this->log();
			$this->log();
			$this->log( "Beginning individual row data checks." );
			$this->log();
		}

		$this->use_log_prefix = true;

		$oms_inv_data = $wpdb->get_results(
			"SELECT oi.sku AS 'oi.sku', pr.sku AS 'pr.sku', pr.oms_sku_confirmed AS 'pr.oms_sku_confirmed', pr.ID AS 'pr.ID', oi.ovc_id AS 'oi.ovc_id', oi.stock AS 'oi.stock', oi.so_qty AS 'oi.so_qty', oi.op_oms_export_unconfirmed AS 'oi.op_oms_export_unconfirmed', pr.op_oms_export_unconfirmed AS 'pr.op_oms_export_unconfirmed'
			 FROM `wp_ovc_oms_inventory` oi LEFT JOIN wp_ovc_products pr ON oi.ovc_id = pr.ID WHERE oi.ovc_id > {$this->skip_past_ovc_id} AND pr.ID IS NOT NULL ORDER BY oi.ovc_id ASC", ARRAY_A
		);

		foreach( $oms_inv_data as $row ) {

			$this->check_user_abort();

			$results = new OVC_Results;
			$ovc_product = OVCDB()->get_row( 'ovc_products', $row['pr.ID'], $results );

			$update_inventory 				= true;
			$update_oms_sku 				= false;
			$update_oms_export_confirmed	= true;

			// This needs to go before any logging in the loop because it is used for the log prefix
			$this->skip_past_ovc_id = $row['pr.ID'];

			if( $row['oi.sku'] == $row['pr.oms_sku_confirmed'] ) {

				// This case means that a SKU has been changed in OVC but the OMS Global change has not been completed
				if( $row['oi.sku'] != $row['pr.sku'] ) {
					$this->log( "WARNING! Global change needed for OVC ID {$row['pr.ID']} - OVC SKU: {$row['pr.sku']} - OMS SKU: {$row['oi.sku']}" );
					$ovc_product->activate_data_error( 'oms__global_change_needed', $row['oi.sku'], $results );
				} else {
					$ovc_product->resolve_data_error( 'oms__ovc_sku_mismatch', $results );
					$ovc_product->resolve_data_error( 'oms__global_change_needed', $results );
				}
			}
			else if( $row['oi.sku'] == $row['pr.sku'] ) {

				$update_oms_sku = true;

				// This means we are confirming a global change ( if oms_sku_confirmed is empty, that means we are importing for the first time)
				if( !empty( $row['pr.oms_sku_confirmed'] ) ) {

					$this->log( "Global Change confirmed for OVC ID {$row['pr.ID']} - Updating OMS SKU Confirmed from {$row['pr.oms_sku_confirmed']} to {$row['oi.sku']}" );
					$ovc_product->resolve_data_error( 'oms__global_change_needed', $results );
				}
			}
			else {

				$this->log( "ERROR! OMS Import data for OVC ID {$row['oi.ovc_id']} does not match OVC SKUs. OMS Import SKU {$row['oi.sku']} - OVC SKU {$row['pr.sku']} - OVC OMS SKU Confirmed {$row['pr.oms_sku_confirmed']}" );
				$ovc_product->activate_data_error( 'oms__ovc_sku_mismatch', $row['oi.sku'], $results );
				$update_inventory = false;
			}

			if( $row['oi.op_oms_export_unconfirmed'] != $row['pr.op_oms_export_unconfirmed'] ) {

				$this->log( "WARNING! OMS Import data for OVC ID {$row['oi.ovc_id']} does not match OMS Export Unconfirmed. OMS Data {$row['oi.op_oms_export_unconfirmed']} - OVC Data {$row['pr.op_oms_export_unconfirmed']}" );
				$ovc_product->activate_data_error( 'oms__export_id_mismatch', "OMS: {$row['oi.op_oms_export_unconfirmed']} - OVC: {$row['pr.op_oms_export_unconfirmed']}", $results );
				$update_oms_export_confirmed = false;
			}
			else {
				$ovc_product->resolve_data_error( 'oms__export_id_mismatch', $results );
			}

			if( $update_inventory ) {

				$updated_data = array(
					'pr.stock'				=> $row['oi.stock'],
					'pr.so_qty'				=> $row['oi.so_qty'],
					'pr.avail_qty'			=> $row['oi.stock'] - $row['oi.so_qty']
				);

				if( $update_oms_sku ) {

					$updated_data['pr.oms_sku_confirmed'] = $row['oi.sku'];
				}

				if( $update_oms_export_confirmed ) {

					$updated_data['pr.op_oms_export_confirmed'] = $row['oi.op_oms_export_unconfirmed'];
				}

				

				if( !$ovc_product->update_data( $updated_data, $results ) ) {

					$this->log( "ERROR updating stock for OVC ID {$row['pr.ID']}" );

					$this->log_results_msgs( $results );
				}
				else {

					
				}
			}

			//Check if need to jump operation
			if( 50 < ox_exec_time() ) {
				$this->log( "Preparing to Jump OMS -> OVC Import operation. Last OVC ID Processed: {$row['pr.ID']}", false );

				$this->skip_past_ovc_id = $row['pr.ID'];
				
				$this->jump_operation();
				return false;
			}
		}

		// $this->successful_finish();
		//return true;
	}

	protected function update_potential_quantities() {
		global $wpdb;

		if( !$this->jumped ) {
		}

		if( is_null( $this->remaining_ovc_product_ids ) ) {

			$this->log();
			$this->log( "Beginning indivividual potential quantity updates." );
			$this->log();

			$this->remaining_ovc_product_ids = $wpdb->get_col(
				"
					SELECT pr.ID
					FROM {$wpdb->prefix}ovc_products pr
					ORDER BY pr.ID ASC
				"
			);
		} else {

			$this->log();
			$this->log( "Continuing indivividual potential quantity updates." );
			$this->log();
		}

		while( $ovc_product_id = array_shift( $this->remaining_ovc_product_ids ) ) {

			$results = new OVC_Results;

			$ovc_product = OVCDB()->get_row( FS( 'ovc_products' ), $ovc_product_id );

			$ovc_product->get_potential_stock();

			$updated_data = array(
				'pr.potential_qty' => $ovc_product->get_potential_stock(),
			);

			if( !$ovc_product->update_data( $updated_data, $results ) ) {

				$this->log( "There was a problem updating potential quantity for OVC ID {$ovc_product->ID}" );

				$this->log_results_msgs( $results );
			}

			//Check if need to jump operation
			if( 50 < ox_exec_time() ) {
				
				$this->jump_operation();
				return false;
			}
		}
	}

	public function log_prefix() {
		return str_pad( "OVC ID: {$this->skip_past_ovc_id}", 18 ) . ' - ';
	}

	public function after_successful_finish() {

		if( 'ftp' == $this->config('file_source') ) {

			$this->log( "OMS Inventory Update cleanup: Archive remote CSV" );

			$ftp = new OVC_OMS_FTP;

			$ftp->rename( '/oms_inventory_update/'. $this->remote_file_name, '/oms_inventory_update/archive/'.$this->remote_file_name );

		}
	}
}	