<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVCOP_analyze_images extends OVCOP {
	public $type = 'analyze_images';

	// Request-level properties (not stored in $op_data['data'])

	// Semi-magic properties via $op_data['data']
	public $last_id;
	public $ovc_img_tables = array();

	public static function extra_init( $ovcop ) {

		$ovcop->last_id = 0;

		$ovcop->ovc_img_tables = array(
			'pa'		=> array(
				'pa.ID',
				'pa.img_main',
				'pa.img_gallery_ids'
			),
			'img'		=> array(
				'img.ID',
				'img.img_main',
				'img.img_oms1',
				'img.img_oms2',
				'img.img_case',
				'img.img_amazon',
				'img.img_sizeguide',
				'img.img_swatch',
				'img.img_gallery_ids'
			),
			'pic'		=> array(
				'pic.ID',
				'pic.post_id'
			)
		);

		if( in_array( $ovcop->config( 'analysis_type' ), array( 'cropability', 'generation' ) ) ) {

			// Set stat for final report
			foreach( array_keys( $ovcop->ovc_img_tables ) as $ovc_img_table ) {

				$field_set = OVCSC::get_field_set( $ovc_img_table );

				$ovcop->init_stat( $ovc_img_table, '# of Images removed from ' . $field_set->name );
			}

			$ovcop->init_stat( 'orphaned_image_sets', '# of Orphaned Image Sets removed' );
		}

		$ovcop->init_stat( 'deleted_unused_images', '# of Unused Images Deleted' );
		$ovcop->init_stat( 'walmart_sftp_images_synced', '# of Images Synced to Walmart' );
		$ovcop->init_stat( 'walmart_sftp_images_not_synced', '# of Images Not Synced to Walmart (errors)' );

		// Send initialization response 
		$ovcop->results->success = true;
		$ovcop->results->main_msg = "OVC Analyze Images operation initialized successfully";

		// Tell OVCOP to automatically commence the operation
		return true;
	}

	public static function pre_init_check( &$ovcop_meta, &$op_data, &$results ) {

		if( 0 == $ovcop_meta['user_id'] ) {

			$last_analyze_images_auto_sync = get_option( 'ovcop__last_analyze_images_auto_sync' );

			$pst = new DateTimeZone( 'America/Los_Angeles' );
			$date = new DateTime( '', $pst );
			// $friday_8pm = new DateTime( 'friday 8pm', $pst );
			// $sunday_8pm = new DateTime( 'sunday 8pm', $pst );

			if( 
				(
					// If it's not Friday and passed 8pm
					!( ( $date->format( 'N' ) == 5 ) && ( $date->format( 'H' ) >= 20 ) )
					// If it's not Saturday
					|| !( $date->format( 'N' ) == 6 )
					// If it's not Sunday and before 8pm
					|| !( ( $date->format( 'N' ) == 7 ) && ( $date->format( 'H' ) <= 20 ) )
				)
				
				// // Check if date is between Friday 8pm and Sunday 8pm
				// ( $date >= $friday_8pm ) && ( $date <= $sunday_8pm )
				// // Check that last_analyze_images_auto_sync is set or that it's been more than 48 hours
				// && ( !$last_analyze_images_auto_sync || ( time() - $last_analyze_images_auto_sync ) > ( 2 * 24 * 60 * 60 ) )

				// If not between Friday 8pm and Sunday 8pm
				// !( ( $date >= $friday_8pm ) && ( $date <= $sunday_8pm ) )

				// And $last_analyze_images_auto_sync is less than 48 hours
				&& ( $last_analyze_images_auto_sync && ( time() - $last_analyze_images_auto_sync ) < ( 2 * 24 * 60 * 60 ) )
			) {

				$results->main_msg = "Cancelling auto-sync of Analyze Images operation";
				// return true;
				return false;
			}

			// return false;
		}

		return true;
	}

	public function __construct( $request, $args ) {
		$this->child_construct( $request, $args );

		$this->analyze_images();	
	}

	/**
	 * Generate the OVC Image Set Post ID table
	 * 
	 * We can use this to delete any removed images
	 */
	function generate_ovc_image_set_post_ids_table() {

		global $wpdb;

		// Image Set Post ID table
		$ovc_img_id_table = $wpdb->prefix . 'ovc_image_set_post_ids';

		// Truncate table before refilling
		$wpdb->query( "TRUNCATE TABLE {$ovc_img_id_table}" );
		$wpdb->query( "ALTER TABLE {$ovc_img_id_table} AUTO_INCREMENT = 1" );

		// SQL Aliases to Table map
		$ovc_tables = array(
			'pa'	=> 'ovc_parent_data',
			'img'	=> 'ovc_image_sets',
			'pic'	=> 'ovc_images'
		);

		// Init the rows that will be inserted into our Image Set Post ID table
		$insert_rows = array();

		// Loop over each table alias to build the rows that will be inserted
		foreach( $this->ovc_img_tables as $ovc_img_table_alias => $ovc_img_table_fields ) {

			/*
			 * Change the name of the field columns that are returned so that they reflect the field name in $ovc_img_tables
			 * This can be removed as it's no longer being used, but it does make the returned arrays/objects easier to read 
			 */
			$ovc_img_select_fields = implode( ', ', array_map( function( $field ) {
				return "{$field} AS '{$field}'";
			}, $ovc_img_table_fields ) );

			// Get the name of the table that we're querying for results
			$ovc_img_table = $wpdb->prefix . $ovc_tables[ $ovc_img_table_alias ];

			// Init the select query aliasing the table to make it easier for us to select columns
			$img_query = "SELECT {$ovc_img_select_fields} FROM {$ovc_img_table} AS {$ovc_img_table_alias}";

			// Get the results of our query in an associative array
			$ovc_img_rows = $wpdb->get_results( $img_query, ARRAY_A );

			foreach( $ovc_img_rows as $ovc_img_row ) {

				// Pop ID off of the top
				$row_id = array_shift( $ovc_img_row );

				$ovc_img_row = array_filter( $ovc_img_row, function( $value ) {
					return $value != 0;
				} );

				foreach( $ovc_img_row as $ovc_img_field ) {

					/*
					 * Array Map over each image field in the row
					 * remove whitespace and explode into an array if it's a comma delimited string
					 * Add the row data to be inserted to our $insert_rows array for each image ID
					 */
					array_map( function( $id ) use ( $row_id, $ovc_img_table_alias, &$insert_rows ) {

						$insert_rows[] = "({$row_id},'{$ovc_img_table_alias}',{$id})";
					}, explode( ',', preg_replace( '/\s+/', '', $ovc_img_field ) ) );
				}
			}
		}

		// Chunk the inserts into 500 rows each and insert into the database
		foreach( array_chunk( $insert_rows, 500 ) as $chunk_inserts ) {

			$insert_query = "INSERT INTO {$ovc_img_id_table} (object_id, object_type, post_id) VALUES " . implode( ',', $chunk_inserts );

			$wpdb->query( $insert_query );
		}
	}

	/**
	 * Get an array of all images that have been deleted from the media library, 
	 * but still exist in OVC tables
	 * 
	 * @return 	array 	$deleted_images
	 */
	public function get_deleted_imgs() {

		global $wpdb;

		// Query Post IDs that exist in Image Fields, but no longer exist in the Posts Table
		$query = "SELECT imgs.* FROM {$wpdb->prefix}ovc_image_set_post_ids AS imgs LEFT JOIN {$wpdb->posts} AS posts ON imgs.post_id = posts.ID WHERE posts.ID IS NULL";

		$deleted_images = $wpdb->get_results( $query );

		return $deleted_images;
	}

	/**
	 * Get the image IDs from Parents and Image Sets
	 * 
	 * @return 	array
	 */
	public function get_image_ids() {

		global $wpdb;

		$query = "SELECT post_id FROM {$wpdb->prefix}ovc_image_set_post_ids WHERE object_type != 'pic' AND post_id > {$this->last_id} ORDER BY post_id";

		$img_ids = $wpdb->get_col( $query );

		return $img_ids;
	}

	public function generate_ovc_unattached_img_post_ids_table() {

		global $wpdb;

		// Image Set Post ID table
		$ovc_img_id_table = $wpdb->prefix . 'ovc_unattached_img_post_ids';

		// Delete table content before refilling
		$wpdb->query( "DELETE FROM wp_ovc_unattached_img_post_ids WHERE in_use != 1" );
		// $wpdb->query( "TRUNCATE TABLE {$ovc_img_id_table}" );
		// $wpdb->query( "ALTER TABLE {$ovc_img_id_table} AUTO_INCREMENT = 1" );

		$unattached_imgs = $this->get_unattached_images();

		// Init the rows that will be inserted into our Unattached Image Post ID table
		$insert_rows = array();
		foreach( $unattached_imgs as $unattached_img ) {

			$img_id = $unattached_img->ID;
			$post_parent = $unattached_img->post_parent;
			$post_parent_type = get_post_type( $unattached_img->post_parent );
			$post_created = $unattached_img->post_date;

			$insert_rows[] = "({$img_id},{$post_parent},'{$post_parent_type}', '{$post_created}')";
		}

		// Chunk the inserts into 500 rows each and insert into the database
		foreach( array_chunk( $insert_rows, 500 ) as $chunk_inserts ) {

			$insert_query = "INSERT IGNORE INTO {$ovc_img_id_table} (post_id, post_parent, post_parent_type, post_created) VALUES " . implode( ',', $chunk_inserts );

			$wpdb->query( $insert_query );
		}
	}

	public function get_unattached_images() {

		global $wpdb;

		$query = "SELECT posts.* FROM {$wpdb->posts} AS posts LEFT JOIN {$wpdb->prefix}ovc_image_set_post_ids AS imgs ON imgs.post_id = posts.ID WHERE posts.post_type = 'attachment' AND imgs.ID IS NULL";

		$img_ids = $wpdb->get_results( $query );

		return $img_ids;
	}

	public function analyze_images() {

		$this->check_user_abort( true );

		$results = new OVC_Results();

		// Maybe Initialize op_step
		if( empty( $this->meta( 'op_step' ) ) ) {
			$this->meta( 'op_step', 0 );
		}

		/*
		 * Step 1
		 * Build the Orphaned Image Set view
		 * Delete the Orphaned Image Sets
		 */
		if( 1 >= $this->meta( 'op_step' ) && in_array( $this->config( 'analysis_type' ), array( 'cropability', 'generation' ) ) ) {

			$this->meta( 'op_step', 1 );

			$orphaned_image_set_ids = OVC_Sync_manager::get_auto_sync_count( 'ovc_orphaned_image_sets', true );

			if( $orphaned_image_sets_count = count( $orphaned_image_set_ids ) ) {
				$this->log( 'Remaining orphaned Image Sets to remove: ' . $orphaned_image_sets_count );
			}

			foreach( $orphaned_image_set_ids as $orphaned_image_set_id ) {

				$results = new OVC_Results();

				$orphaned_image_set = OVCDB()->get_row( 'image_sets', $orphaned_image_set_id, $results );
				$orphaned_image_set->delete( $results, true );

				if( !$results->has_error ) {

					$this->stat( 'orphaned_image_sets' );

					$this->log( 'Successfully deleted Image Set - ID: ' . $orphaned_image_set->data( 'img.ID' ) );
				} else {

					$this->log( 'Error deleting orphaned Image Set - ID: ' . $orphaned_image_set->data( 'img.ID' ) );
					$this->log_results_msgs( $results );
				}

				$this->check_user_abort();

				if( 50 < ox_exec_time() ) {
					$this->jump_operation();
				}
			}
		}

		/*
		 * Step 2
		 * Regenerate Image ID table
		 */
		if( 2 >= $this->meta( 'op_step' ) && in_array( $this->config( 'analysis_type' ), array( 'cropability', 'generation' ) ) ) {

			$this->meta( 'op_step', 2 );

			$this->log( "Generating image set post ID table" );

			$this->generate_ovc_image_set_post_ids_table();
		}

		/*
		 * Step 3
		 * Remove deleted images from OVC
		 */
		if( 3 >= $this->meta( 'op_step' ) && in_array( $this->config( 'analysis_type' ), array( 'cropability', 'generation' ) ) ) {

			$this->meta( 'op_step', 3 );

			$deleted_imgs = $this->get_deleted_imgs();

			if( $deleted_imgs_count = count( $deleted_imgs ) ) {
				$this->log( 'Remaining deleted images to remove: ' . $deleted_imgs_count );
			}

			foreach( $deleted_imgs as $deleted_img ) {

				$results = new OVC_Results();

				$field_set = OVCSC::get_field_set( $deleted_img->object_type );

				$log_msg = "Post ID: {$deleted_img->post_id} removed from {$field_set->name}";

				switch( $deleted_img->object_type ) {

					case 'pa':

						$row = OVCDB()->get_row( FS('parent_data'), $deleted_img->object_id );

						$log_msg .= " - ID: {$row->ID} - parent_sku: {$row->data( 'pa.parent_sku' )}";
						break;
					case 'img':

						$row = OVCDB()->get_row( FS('image_sets'), $deleted_img->object_id );

						$log_msg .= " - ID: {$row->ID}";

						if( $parent_sku = $row->get_parent_sku( $results ) ) {
							$log_msg .= " - parent_sku: {$parent_sku}";
						} else {
							$this->log( "Image Set ID: {$row->ID} is not attached to any products." );
						}
						break;
					case 'pic':

						$row = OVCDB()->get_row( FS('ovc_images'), $deleted_img->object_id );

						$log_msg .= " - ID: {$row->ID}";

						if( $ovc_image_name = $row->get_image_name() ) {
							$log_msg .= " - image name: {$ovc_image_name}";
						}
						break;
				}

				if( $row ) {

					$row->remove_image( $deleted_img->post_id, $results );

					if( !$results->has_error ) {

						$this->log( $log_msg );
					} else {

						$this->log( "Error removing image Post ID: {$deleted_img->post_id} from {$field_set->name}" );
						$this->log_results_msgs( $results );
					}

					// Count up the stat for deleted images
					$this->stat( $deleted_img->object_type );
				} else {

					$this->log( "Invalid object_type found - '{$deleted_img->object_type}'" );
				}

				$this->check_user_abort();

				if( 50 < ox_exec_time() ) {
					$this->jump_operation();
				}
			}

			
		}

		/*
		 * Step 4
		 * Regenerate Image ID table since some have been deleted
		 */
		if( 4 >= $this->meta( 'op_step' ) && in_array( $this->config( 'analysis_type' ), array( 'cropability', 'generation' ) ) ) {

			$this->meta( 'op_step', 4 );

			$this->log( "Regenerating image set post ID table in case any image references were deleted." );

			$this->generate_ovc_image_set_post_ids_table();	
		}

		/*
		 * Step 5
		 * Analyze Images
		 */
		if( 5 >= $this->meta( 'op_step' ) && in_array( $this->config( 'analysis_type' ), array( 'cropability' ) ) ) {

			$this->meta( 'op_step', 5 );

			$img_ids = $this->get_image_ids();

			$this->log( "Commencing Analyze Image Operation. Remaining images to check: " . count( $img_ids ) );

			while( $post_id = array_shift( $img_ids ) ) {

				$results = new OVC_Results();

				$img_path = get_attached_file( $post_id, true );

				if( !$img_path ) {

					$this->log( "Post ID: {$post_id} not found, has it been deleted while operation was running?" );
				}

				$ovc_img = new OVC_Image( $img_path );

				if( !$ovc_img->get_width() ) {
					$this->log( "Error initializing OVC Image for Post ID: {$post_id}" );
					continue;
				}

				$img_row = OVCDB()->get_row_by_valid_id( FS('ovc_images'), 'post_id', $post_id, $results );

				$updated_data = array(
					'post_id'	=> $post_id,
					'path'		=> $img_path,
					'last_check'=> current_time( 'mysql', 1 )
				);

				if( !$img_row->exists() ) {

					$updated_data['status']		= 'Needs Review';
					$updated_data['crop_mode']	= 'Auto';
					$updated_data['crop_focus']	= 'Center Middle';
				}

				if( 'Approved' !== $img_row->data( 'pic.status' ) ) {

					$img_row->analyze_image( $ovc_img, $updated_data, $results );
				}

				$img_row->update_data( $updated_data, $results );

				if( !$results->has_error ) {

				} else {

					$this->log_results_msgs( $results );
				}

				if( 50 < ox_exec_time() ) {

					$this->last_id = $post_id;
					$this->log( "Preparing to jump. Last ID checked: {$this->last_id}" );
					$this->jump_operation();
				} else if( 0 == count( $img_ids ) % 20 ) {

					$this->check_user_abort();

					if( 0 == count( $img_ids ) % 100 ) {

						$this->log( 'Images remaining: ' . count( $img_ids ) );
					}
				}
			}
		}

		/*
		 * Step 6
		 * Generate Unattached Image Table
		 */
		if( 6 >= $this->meta( 'op_step' ) && in_array( $this->config( 'analysis_type' ), array( 'cropability', 'generation' ) ) ) {

			$this->meta( 'op_step', 6 );

			$this->log( "Generating unattached image post ID table" );

			$this->generate_ovc_unattached_img_post_ids_table();
		}

		/*
		 * Step 7
		 * [Maybe] Delete unattached images not marked as 'In Use'
		 */
		if( 7 >= $this->meta( 'op_step' ) &&  $this->config( 'delete_unused_images' ) ) {

			$this->meta( 'op_step', 7 );
			$this->check_user_abort();

			$unused_img_ids = OVC_Sync_Manager::get_auto_sync_count( 'ovc_unused_images', true );

			if( count( $unused_img_ids ) ) {

				$this->log( "Deleting unattached images that are not marked as In Use. " . count( $unused_img_ids ) . " remaining to be deleted." );

				while( $unused_img_id = array_pop( $unused_img_ids ) ) {
						
					$this->check_user_abort();

					$results = new OVC_Results();

					$ovc_row_unused_img = OVCDB()->get_row( FS('ovc_unattached_img_post_ids'), $unused_img_id, $results );

					if( false === $ovc_row_unused_img->delete( $results ) ) {

						$this->log( "Error deleting unused image ID {$ovc_row_unused_img->ID} (post ID {$ovc_row_unused_img->post_id})" );
						$this->log_results_msgs( $results );
					}
					else {

						$this->stat( 'deleted_unused_images' );
					}

					if( 0 == count( $unused_img_ids ) % 100 ) {

						$this->check_user_abort();
						$this->log( 'Images remaining to be deleted: ' . count( $unused_img_ids ) );
					}

					if( 50 < ox_exec_time() ) {

						$this->jump_operation();
					}
				}
			}
			else {

				$this->log( "No unattached images to delete." );
			}
		}

		/*
		 * Step 8
		 * Copy Approved Images to the Walmart FTP
		 */
		if( 8 >= $this->meta( 'op_step' ) && $this->config( 'copy_to_walmart_ftp' ) ) {

			$this->meta( 'op_step', 8 );
			$this->check_user_abort();

			$unsynced_images = OVC_Sync_Manager::get_auto_sync_count( 'walmart_images_requiring_sync', true );

			if( count( $unsynced_images ) ) {

				$this->log( 'Syncing images to Walmart sFTP. ' . count( $unsynced_images ) . ' remaining to be synced.' );

				$walmart_ftp = new OVC_SFTP( 'walmart', true );

				if( $walmart_ftp->check_connection() ) {

					while( $unsynced_image_id = array_pop( $unsynced_images ) ) {
						
						$this->check_user_abort();

						$results = new OVC_Results();

						$ovc_image_row = OVCDB()->get_row( FS( 'ovc_images' ), $unsynced_image_id, $results );

						$image_path = $ovc_image_row->data( 'pic.image_override_path' ) ?: $ovc_image_row->data( 'pic.image_3_4_path' );

						if( $image_path && file_exists( get_ovc_image_path( $image_path ) ) ) {

							if( !$walmart_ftp->put( $image_path, get_ovc_image_path( $image_path ) ) ) {
								
								$this->stat( 'walmart_sftp_images_not_synced' );
								// $this->log( 'There was a problem uploading image ID: ' . $unsynced_image_id . ' Filename: ' . $image_path );

								if( !$walmart_ftp->check_connection() ) {
									$this->log( 'sFTP Server Closed Connection Prematurely. Please try again.' );
									break;
								}
							} else {

								$this->stat( 'walmart_sftp_images_synced' );
								// Update Last Walmart FTP Sync Time
								$ovc_image_row->update_field( 'pic.last_wa_ftp_sync', current_time( 'mysql', 1 ), $results );
							}
						} else {

							$this->stat( 'walmart_sftp_images_not_synced' );
							// $this->log( "Image ID: {$unsynced_image_id} could not be synced with Walmart. Check that the file still exists and is analyzed (3:4)." );
						}

						if( 50 < ox_exec_time() ) {
							$walmart_ftp->close_connection();
							$this->jump_operation();
						}
					}
				} else {

					$this->log( 'Error syncing images to Walmart sFTP. Cannot establish connection with sFTP server.' );
				}
			}
		}
		
		if( 0 == $this->user_id ) {
			// Update last analyze images auto sync time
			update_option( 'ovcop__last_analyze_images_auto_sync', time() );
		}

		$this->successful_finish();
	}
}

