<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVCOP_oms_export extends OVCOP {
	public $type = 'oms_export';
	public $op_step = 3;

	// Simple operation-specific properties (re-initialized for each commence_operation request)
	public $export_field_set = array(); // OMS Field Map (with fieldmeta information)

	// Semi-magic properties via $op_data['data']
	public $last_id_exported = 0;
	public $parent_skus;
	public $exported_ids;
	

	// Operation Type Specific Initialization
	public static function extra_init( $ovcop ) {
		global $wpdb;

		// Initialize operation data based on configuration options
		$ovcop->parent_skus = false;
		if( 'parent_skus' == $ovcop->config( 'sku_filter' ) ) {
			$ovcop->parent_skus = array_map( 'sanitize_text_field', array_map( 'trim', explode( ",", $ovcop->config( 'filter_parent_skus' ) ) ) );
		}
		$ovcop->log( "SKU Filtering: {$ovcop->config( 'sku_filter' )}" );

		// Initialize semi-magic properties
		$ovcop->last_id_exported 	= 0;
		$ovcop->exported_ids 		= array();

		// Initialize CSV
		$ovcop->new_file( 'csv', 'csv', true );

		// Send initialization response 
		$ovcop->results->success = true;
		$ovcop->results->main_msg = "OMS Export operation initialized successfully";

		// Tell OVCOP to automatically commence the operation
		return true;
	}


	public function __construct( $request, $args ) {
		$this->child_construct( $request, $args );

		// Initialize OMS Field Set
		$this->export_field_set = OVCSC::get_field_set( $this->config( 'export_format' ) );

		// Set Line Endings
		//$this->op_file()->line_endings = $this->config('use_ftp') ? "\n" : "\r\n";
		$this->op_file()->line_endings = "\r\n";

		$this->export();		
	}

	public function export() {

		// Preload active data errors
		OVC_Row_data_errors::enable();
		OVC_Row_data_errors::load_active_errors();

		if ( !$this->jumped ) {
			// Write headers line to CSV
			$this->write_csv_line_from_array( array_map( array( $this, 'csv_wrap_value' ), $this->export_field_set->get_fields( 'local_name' ) ) );
		}
		
		// Build query and get OVC IDs for export
		global $wpdb;

		// dev:improve
		if( 'auto' == $this->config( 'sku_filter' ) ) {

			$qry = <<<EOT
				SELECT pr.ID
				 FROM wp_ovc_products pr
				 LEFT JOIN wp_ovc_parent_data pa
				 ON pr.parent_sku = pa.parent_sku
				 LEFT JOIN wp_ovc_style_data st
				 ON pr.sku_style = st.style
				 LEFT JOIN wp_ovcop_log ol
				 ON pr.op_oms_export_unconfirmed = ol.ID
				 WHERE 
				 pr.ID > {$this->last_id_exported}
				 AND pr.sync_oms = 1
				 AND ( ol.ID IS NOT NULL AND GREATEST( pr._data_updated, pr._price_updated, pa._meta_updated, st._meta_updated ) > ol.start_time OR pr.op_oms_export_unconfirmed = 0 )
EOT;
		}
		else {

			$qry = "SELECT ID FROM wp_ovc_products WHERE ID > {$this->last_id_exported} AND ";

			if( 'parent_skus' == $this->config( 'sku_filter' ) ) {
				$qry .= "parent_sku IN(\"" . implode( "\",\"", $this->parent_skus ) . "\") AND ";
			}
			else if ( 'custom_selection' == $this->config( 'sku_filter' ) ) {
				$qry .= "ID IN(" . implode( ',', get_ovc_user_custom_selection( $this->user_id, 'ovc_products' ) ) . ") AND ";
			}

			// dev:sync_revamp
			//$qry .= "ovc_status IN('Verified','Defective') ORDER BY parent_sku ASC";
			$qry .= "sync_oms = 1 ORDER BY ID ASC";
		}
		

		$ovc_ids = $wpdb->get_col( $qry );

		$this->log( 'Beginning Export. ' . count( $ovc_ids ) . ' OVC IDs remaining to be exported.' );

		$prfs = OVCSC::get_field_set( 'ovc_products' );

		$jump_lines_exported = 0;

		// Loop through and export IDs 
		foreach( $ovc_ids as $ovc_id ) {

			$results = new OVC_Results;
			$ovcp = OVCDB()->get_row( $prfs, $ovc_id, $results );
			$csv_row_values = array();

			// Validate the child and add to skip it if invalid
			$failed_check = OVC_Row_error_codes::check_ovc_product_error_code( 'sync_oms_required_fields', $ovcp, $results );
			if( !is_null( $failed_check ) && $failed_check !== true ) {
				$this->log_results_msgs( $results );
				continue;
			}
			
			foreach( $this->export_field_set->get_fields() as $field ) {
				$oms_value = $ovcp->get_external_value( $field, $results );

				if( false === $oms_value ) {
					$results->error( "ERROR! OVC ID: {$ovc_id} - oms_value is false for field {$field}" );
				}

				$escape_csv_value = ! (bool) $field->meta( 'csv_no_escape' );

				$csv_row_values[] = $this->csv_wrap_value( $oms_value, $escape_csv_value );
			}

			foreach( $results->get_msgs() as $msg ) {
				$this->log( $msg );
			}

			$this->write_csv_line_from_array( $csv_row_values );

			$jump_lines_exported++;

			$this->exported_ids[] = $ovcp->ID;

			if( !($jump_lines_exported % 50) ) {

				if( !($jump_lines_exported % 1000) ) {					

					if( !($jump_lines_exported % 2000) ) {

						$this->log( "Exported {$jump_lines_exported} this jump. Memory usage: " . memory_get_usage() .  " Last ID: {$ovcp->ID} - Remaining IDs: " . (count( $ovc_ids ) - $jump_lines_exported) );
					}

					OVCDB()->flush_db_data();
				}				

				$this->last_id_exported = $ovc_id;

				$this->check_user_abort();	
			}

			//Check if need to jump operation
			if( 55 < ox_exec_time() ) {
				$this->log( "Preparing to Jump OMS Export operation. Last ID Processed: {$ovc_id}", false );
				$this->log( "Exported {$jump_lines_exported} this jump. Last ID: {$ovcp->ID} - Remaining IDs: " . (count( $ovc_ids ) - $jump_lines_exported) );

				$this->last_id_exported = $ovc_id;
				
				$this->jump_operation();
				return false;
			}
		}

		$this->successful_finish();
	}

	public function before_successful_finish() {
		$this->log();

		// Maybe upload export file to FTP
		if( $this->config('use_ftp') ) {
			$this->log( "Attempting to save OMS Export CSV to VIDA FTP..." );
			
			$ovcftp = new OVC_OMS_FTP;
			
			if( !$ovcftp->is_connected() ) {
				$this->log( "File Transfer failed: Unable to establish FTP connection." );
			}
			else {
				$ftp_op_dir = $this->ftp_op_dir();

				$this->log( "FTP: Switching directory to {$ftp_op_dir}" );

				// Change to op directory
				if( !$ovcftp->chdir( 'ovc_' . $this->type ) ) {
					$this->log( "Failed to change directory. FTP Upload cancelled." );
				}
				// Upload file
				else if ( !$ovcftp->put( $this->op_file()->name(), $this->op_file()->path() ) ) {
					$this->log( "Failed to upload OMS Export CSV to {$ftp_op_dir} directory." );
				}
				// Verify file upload
				else {
					$dir_list = $ovcftp->list_dir();

					if( !$dir_list || !array_key_exists( $this->op_file()->name(), $dir_list ) ) {
						$this->log( "Failed to verify OMS Export CSV file upload.  It may or may not be in the proper directory on the VIDA server." );
					}
					else {
						$this->log( "OMS Export CSV uploaded successfully." );

						$this->meta( 'vida_ftp_csv_path', "O:\\ovc\\{$ftp_op_dir}\\{$this->op_file()->name()}" );
					}
				}
			}

			$ovcftp->close_connection();
		}
		else {
			$this->log( "Skipping automatic FTP Upload." );
		}

		$this->log();

		// Set stat for final report
		$this->init_stat( 'exported_products', '# of Products Exported' );
		$this->stat( 'exported_products', count( $this->exported_ids ) );

		// Update product OVCOP meta
		$this->mass_update_ovcop_products_meta( 'op_oms_export_unconfirmed', $this->exported_ids );
	}

	public function after_successful_finish() {
		$this->exported_ids = array(); //Reset so we don't save this in the DB after completion
		// Output link to download CSV
		$this->log();
		$this->log( "Download exported CSV here: " . $this->op_file()->url() );
		$this->log();
	}
}	