<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

ini_set("memory_limit", '256M' );
ini_set("max_execution_time", 300 );

class OVCOP_walmart_sync extends OVCOP {
	public $type = 'walmart_sync';

	// Semi-magic properties via $op_data['data']
	public $sync_steps = array();

	public $last_csv_line = 0;
	public $csv_file_path = '';

	public static function extra_init( $ovcop ) {

		$ovcop->last_csv_line = 0;
		$ovcop->csv_file_path = '';

		// Init stats
		// Items CSV Stats
		$ovcop->init_stat( 'items_csv_rows_total', 'Items CSV Total Data Rows' );
		$ovcop->init_stat( 'items_csv_rows_success', 'CSV Rows Processed' );
		$ovcop->init_stat( 'items_csv_rows_failed', 'CSV Rows Failed' );
		// Items CSV Matching stats
		$ovcop->init_stat( 'match_walmart_id', 'CSV Items Matched by Walmart ID' );
		$ovcop->init_stat( 'match_sku_creating', 'CSV Items Matched by SKU (CREATING, blank Walmart ID)' );
		$ovcop->init_stat( 'match_sku_single', 'CSV Items Matched by SKU (Walmart ID mismatch)' );
		$ovcop->init_stat( 'match_none_create_local', 'CSV Items not found, local created' );
		$ovcop->init_stat( 'match_failed', 'CSV Items Match Failed' );
		$ovcop->init_stat( 'match_fail_multiple_creating', 'CSV Items Match Failed (Duplicate SKUs CREATING)' );
		$ovcop->init_stat( 'match_fail_duplicate_sku', 'CSV Items Match Failed (Duplicate SKU)' );

		// dev:improve Change this to use the names of the step, always run the refresh steps
		$sync_steps = array(
			1	=> true,
			2	=> true,
			3	=> true,
			4	=> true,
			5	=> true,
			6	=> true,
			7	=> true,
			8	=> false,
			9	=> true,
			10 	=> true
		);
		
		switch( $ovcop->config( 'sync_mode' ) ) {

			case 'refresh_only':

				$sync_steps[3] = false;
				$sync_steps[4] = false;
				$sync_steps[5] = false;
				$sync_steps[6] = false;
				$sync_steps[7] = false;
				$sync_steps[8] = false;
				$sync_steps[9] = false;
				$sync_steps[10] = false;
				break;
			case 'custom_sync':

				$sync_steps[3] = $ovcop->config( 'sync_walmart_bulk_create_items' );
				$sync_steps[4] = $ovcop->config( 'sync_walmart_bulk_update_skus' );
				$sync_steps[5] = $ovcop->config( 'sync_walmart_bulk_update_items' );
				$sync_steps[6] = $ovcop->config( 'sync_walmart_bulk_update_prices' );
				$sync_steps[7] = $ovcop->config( 'sync_walmart_bulk_update_inventory' );
				$sync_steps[8] = $ovcop->config( 'sync_walmart_bulk_update_manual' );
				break;
			case 'auto_sync':
			default:

				$sync_steps[8] = false;
				break;
		}
		$ovcop->sync_steps = $sync_steps;
	
		// Send initialization response 
		$ovcop->results->success = true;
		$ovcop->results->main_msg = "Walmart Sync operation initialized successfully";

		// Tell OVCOP to automatically commence the operation
		return true;
	}

	public function __construct( $request, $args ) {
		$this->child_construct( $request, $args );

		$this->sync();
	}

	public function sync() {

		// Preload active data errors
		OVC_Row_data_errors::enable();
		OVC_Row_data_errors::load_active_errors();
		
		if( $this->jumped ) {
			$this->log( "Jump Landed. Continuing Walmart Sync at sync step {$this->meta( 'walmart_sync_step' )}." );
		}
		else {
			$this->meta( 'walmart_sync_step', 1 );
		}

		$this->check_user_abort( true );

		global $wpdb;

		/**
		 * SYNC STEPS
		 * 
		 * 1. update local feeds
		 * 2. update local products
		 * 3. submit new items 				(walmart_bulk_create_items)
		 * 4. submit data update skus 		(walmart_bulk_update_skus)
		 * 5. submit data update items 		(walmart_bulk_update_items)
		 * 6. submit bulk price update 		(walmart_bulk_update_prices)
		 * 7. submit bulk inventory update 	(walmart_bulk_update_inventory)
		 * 8. submit bulk manual update 	(walmart_bulk_update_manual)
		 * 9. update local feeds
		 * 10. update local products 		DISABLED
		 */
		// DEV:IMPROVE 
		$total_sync_steps = 'refresh_only' == $this->config( 'sync_mode' ) ? 2 : 9;

		// STEP 1: REFRESH LOCAL WALMART FEED DATA
			// dev:improve Remove this when we change to using the name of the step as it will always run
		if( 1 == $this->meta( 'walmart_sync_step' ) ) {
			if( $this->sync_steps[1] ) {
				$this->log( "Sync Step {$this->meta( 'walmart_sync_step' )} / {$total_sync_steps}: Refreshing Local Walmart Feed Data." );

				// Initialize new OVC Walmart SKUs
				$this->initialize_walmart_skus_for_sync();

				$this->refresh_local_walmart_feeds();

				$this->meta( 'walmart_sync_step', 2 );			
				if( !$this->maybe_jump_or_abort() ) {
					return false;
				}
			} else {
				$this->meta( 'walmart_sync_step', 2 );
			}
		}

		// STEP 2: REFRESH LOCAL WALMART PRODUCT DATA
		// dev:improve Remove this when we change to using the name of the step as it will always run
		if( 2 == $this->meta( 'walmart_sync_step' ) ) {
			
			if( $this->sync_steps[2] ) {
				$this->log( "Sync Step {$this->meta( 'walmart_sync_step' )} / {$total_sync_steps}: Refreshing Local Walmart Product Data." );

				$this->refresh_local_walmart_products();

				$results = new OVC_Results();

				$this->log_results_msgs( $results );

				if( 'refresh_only' == $this->config( 'sync_mode' ) ) {
					$this->successful_finish();
					return true;
				}

				$this->meta( 'walmart_sync_step', 3 );
				if( !$this->maybe_jump_or_abort() ) {
					return false;
				}
			} else {
				$this->meta( 'walmart_sync_step', 3 );
			}	
		}

		if( 'refresh_only' == $this->config( 'sync_mode' ) ) {
			$this->successful_finish();
			return true;
		}

		// STEP 3: BULK CREATE NEW ITEMS FEED
		if( 3 == $this->meta( 'walmart_sync_step' ) ) {
			
			if( $this->sync_steps[3] ) {
				OVCDB()->flush_db_data();
				
				$feed_type = 'walmart_bulk_create_items';

				$this->log( "Sync Step {$this->meta( 'walmart_sync_step' )} / {$total_sync_steps}: Bulk Create new items (Submitting new SKUs to Walmart)." );

				$ovc_ids = OVC_Sync_Manager::get_auto_sync_count( $feed_type, true );

				if( !$ovc_ids ) {
					$this->log( "No new SKUs to sync to Walmart" );
				}
				else if( !on_vida_live() ) {
					$this->log( "Skipping {$feed_type} feed - Remote API feeds should only be sent from live site." );
				}
				else {
					$this->log( count( $ovc_ids ) . " New SKUs to sync" );

					// Loop over OVC IDs and make sure all of the images are in the OVC Image table before writing them to the file
					// The function in the array_filter is also duplicated below in the Bulk Update
					$results = new OVC_Results();

					$ovc_ids = array_filter( $ovc_ids, function( $ovc_id ) use ( $results ) {

						$ovcpr = OVCDB()->get_row( FS( 'ovc_products' ), $ovc_id );
 
						// Validate the product and return false if invalid
						$failed_check = OVC_Row_error_codes::check_ovc_product_error_code( 'sync_walmart_required_fields', $ovcpr, $results );
						if( !is_null( $failed_check ) && $failed_check === true ) {
							return false;
						}

						$parent_product = $ovcpr->get_parent();
						$main_img_id = isset( $parent_product->img_marketplace_featured ) && $parent_product->img_marketplace_featured ? $parent_product->img_marketplace_featured : $parent_product->img_main;
						$main_img = OVCDB()->get_row_by_valid_id( FS( 'ovc_images' ), 'post_id', $main_img_id );
						$images_approved = true;

						if( !( $main_img->is_image_approved() ) ) {

							$images_approved = false;

							// $this->log( 'Unable to Create Walmart product for OVC ID: ' . $ovc_id . ' - main image is not approved in OVC Images.' );
							// return false;
						}
						else {
							foreach( explode( ',', $parent_product->img_gallery_ids ) as $img_id ) {

								$gallery_img = OVCDB()->get_row_by_valid_id( FS( 'ovc_images' ), 'post_id', $img_id );
	
								if( !( $gallery_img->is_image_approved() ) ) {
	
									$images_approved = false;
									break;
									
									// $this->log( 'OVC ID: ' . $ovc_id . ' does not have an approved analyzed image in OVC Images.' );
									// return false;
								}
							}
						}
						

						if( !$images_approved ) {
							$this->log( 'Unable to Create Walmart product for OVC ID: ' . $ovc_id . ' - all product images must be analyzed and approved in OVC Images.' );
							return false;
						}

						return true;
					});

					$this->log_results_msgs( $results );

					$ovc_ids_chunked = array_chunk( $ovc_ids, 500 );

					foreach( $ovc_ids_chunked as $ovc_ids_chunk ) {

						$feed_results = new OVC_Results();

						// Initialize Feed File
						$feed_file = $this->new_file( $feed_type, 'xml', true );

						// Initialize Feed Object (and in DB)
						$api_feed = OVC_Row_api_feeds::init_feed( $feed_type, $feed_file->ID, $this->ID, $feed_results );

						// Build & submit Feed
						$feed_submitted = false; 

						if(	$api_feed->write_feed_to_file( $ovc_ids_chunk, $feed_results ) ) {

							$feed_submitted = $api_feed->submit_feed( $feed_results );
						}

						$this->log_results_msgs( $feed_results );

						if( $feed_submitted ) {

							$pr_fs = FS('ovc_products');

							foreach( $ovc_ids_chunk as $ovc_id ) {
								$product = OVCDB()->get_row( $pr_fs, $ovc_id );

								$updated_walmart_data = array(
									'wa.last_item_feed_id'	=> $api_feed->ID,
									'wa.sync_status'		=> 'CREATING',
									'wa.feed_item_status'	=> 'SENT'		// dev:deprecated
								);

								$product->update_data( $updated_walmart_data );	
							}
						}
						
						if( !$this->maybe_jump_or_abort() ) {
							return false;	
						}
					}
				}

				$this->log( "Walmart Sync Step {$this->meta( 'walmart_sync_step' )} Complete: " . count( $ovc_ids ) . " new SKUs submitted" );

				$this->meta( 'walmart_sync_step', 4 );
				if( !$this->maybe_jump_or_abort( true ) ) {
					return false;
				}
			} else {
				$this->meta( 'walmart_sync_step', 4 );
			}
		}

		// STEP 4: BULK UPDATE SKUS
		if( 4 == $this->meta( 'walmart_sync_step' ) ) {

			if( $this->sync_steps[4] ) {
				OVCDB()->flush_db_data();

				$feed_type = 'walmart_bulk_update_skus';

				$this->log( "Sync Step {$this->meta( 'walmart_sync_step' )} / {$total_sync_steps}: Bulk Update SKUs." );

				$wa_ids = OVC_Sync_Manager::get_auto_sync_count( $feed_type, true );

				if( !$wa_ids ) {
					$this->log( "No Walmart Products need SKU update" );
				}
				else if( !on_vida_live() ) {
					$this->log( "Skipping {$feed_type} feed - Remote API feeds should only be sent from live site." );
				}
				else {
					$this->log( count( $wa_ids ) . " Walmart Products need SKU updates" );

					// Loop over WA IDs and make sure all of the images are in the OVC Image table before writing them to the file
					$results = new OVC_Results();
					// dev:improve dev:todo This array filter takes too long when doing a full sync, this needs to be improved or cut down using a query rather than array_filter
					$wa_ids = array_filter( $wa_ids, function( $wa_id ) use ( $results ) {

						$wapr = OVCDB()->get_row( FS( 'walmart_product' ), $wa_id );
						$ovcpr = $wapr->get_ovc_product();

						if( !$ovcpr->exists() ) {

							return $results->error( "Walmart Product ID: $wa_id - Missing OVC ID" );
						}

						$failed_check = OVC_Row_error_codes::check_ovc_product_error_code( 'sync_walmart_required_fields', $ovcpr, $results );
						if( !is_null( $failed_check ) && $failed_check === true ) {
							return false;
						}

						$parent_product = $ovcpr->get_parent();
						$main_img_id = isset( $parent_product->img_marketplace_featured ) && $parent_product->img_marketplace_featured ? $parent_product->img_marketplace_featured : $parent_product->img_main;
						$main_img = OVCDB()->get_row_by_valid_id( FS( 'ovc_images' ), 'post_id', $main_img_id );

						if( !$main_img->is_image_approved() ) {

							return false;
						}

						foreach( explode( ',', $parent_product->img_gallery_ids ) as $img_id ) {

							$gallery_img = OVCDB()->get_row_by_valid_id( FS( 'ovc_images' ), 'post_id', $img_id );

							if( !$gallery_img->is_image_approved() ) {

								return false;
							}
						}

						return true;
					});

					$this->log_results_msgs( $results );

					$wa_ids_chunked = array_chunk( $wa_ids, 500 );

					foreach( $wa_ids_chunked as $wa_ids_chunk ) {

						$feed_results = new OVC_Results();

						// Initialize Feed File
						$feed_file = $this->new_file( $feed_type, 'xml', true );

						// Initialize Feed Object (and in DB)
						$api_feed = OVC_Row_api_feeds::init_feed( $feed_type, $feed_file->ID, $this->ID, $feed_results );

						//Build and submit feed
						$feed_submitted = false;

						if(	$api_feed->write_feed_to_file( $wa_ids_chunk, $feed_results ) ) {

							$feed_submitted = $api_feed->submit_feed( $feed_results );
						}					

						$this->log_results_msgs( $feed_results );

						if( $feed_submitted ) {

							$wa_fs = FS( 'walmart_product' );
							$pr_fs = FS( 'ovc_products' );

							foreach( $wa_ids_chunk as $wa_id ) {
								$walmart_product = OVCDB()->get_row( $wa_fs, $wa_id );
								$product = $walmart_product->get_ovc_product();

								$updated_walmart_data = array(
									'wa.last_item_feed_id'	=> $api_feed->ID,
									'wa.sync_status'		=> 'UPDATING',
									'wa.feed_item_status'	=> 'SENT'		// dev:deprecated
								);

								$product->update_data( $updated_walmart_data );
							}
						}
						
						if( !$this->maybe_jump_or_abort() ) {
							return false;	
						}
					}
				}

				$feed_count = isset( $wa_ids_chunked ) ? count( $wa_ids_chunked ) : 0;

				$this->log( "Walmart Sync Step {$this->meta( 'walmart_sync_step' )} Complete: " . count( $wa_ids ) . " SKUs submitted in " . $feed_count . " feeds for data update" );

				$this->meta( 'walmart_sync_step', 5 );
				if( !$this->maybe_jump_or_abort() ) {
					return false;	
				}
			} else {
				$this->meta( 'walmart_sync_step', 5 );
			}
		}

		// STEP 5: BULK UPDATE ITEMS FEED
		if( 5 == $this->meta( 'walmart_sync_step' ) ) {

			if( $this->sync_steps[5] ) {
				OVCDB()->flush_db_data();

				$feed_type = 'walmart_bulk_update_items';
			
				if( $this->config( 'sync_all' ) ) {

					$this->log( "Force-syncing items for all active Walmart SKUs" );
					$feed_type = 'walmart_bulk_update_items_all';
				}

				$this->log( "Sync Step {$this->meta( 'walmart_sync_step' )} / {$total_sync_steps}: Bulk Update items (Auto-updating product data (not prices or inventory) of Walmart items)." );

				$ovc_ids = OVC_Sync_Manager::get_auto_sync_count( $feed_type, true );

				if( !$ovc_ids ) {
					$this->log( "No SKUs need walmart product data update" );
				}
				else if( !on_vida_live() ) {
					$this->log( "Skipping {$feed_type} feed - Remote API feeds should only be sent from live site." );
				}
				else {
					$this->log( count( $ovc_ids ) . " SKUs need walmart product data update" );

					// Loop over OVC IDs and make sure all of the images are in the OVC Image table before writing them to the file
					$results = new OVC_Results();
					// dev:improve dev:todo This array filter takes too long when doing a full sync, this needs to be improved or cut down using a query rather than array_filter
					$ovc_ids = array_filter( $ovc_ids, function( $ovc_id ) use ( $results ) {

						$ovcpr = OVCDB()->get_row( FS( 'ovc_products' ), $ovc_id );

						// Validate the product and return false if invalid
						$failed_check = OVC_Row_error_codes::check_ovc_product_error_code( 'sync_walmart_required_fields', $ovcpr, $results );
						if( !is_null( $failed_check ) && $failed_check === true ) {
							return false;
						}

						$parent_product = $ovcpr->get_parent();
						$main_img_id = isset( $parent_product->img_marketplace_featured ) && $parent_product->img_marketplace_featured ? $parent_product->img_marketplace_featured : $parent_product->img_main;
						$main_img = OVCDB()->get_row_by_valid_id( FS( 'ovc_images' ), 'post_id', $main_img_id );

						if( !$main_img->is_image_approved() ) {
							
							// $this->log( 'OVC ID: ' . $ovc_id . ' does not have an approved analyzed image in OVC Images.' );
							return false;
						}

						foreach( explode( ',', $parent_product->img_gallery_ids ) as $img_id ) {

							$gallery_img = OVCDB()->get_row_by_valid_id( FS( 'ovc_images' ), 'post_id', $img_id );

							if( !$gallery_img->is_image_approved() ) {
								
								// $this->log( 'OVC ID: ' . $ovc_id . ' does not have an approved analyzed image in OVC Images.' );
								return false;
							}
						}

						return true;
					});

					$this->log_results_msgs( $results );

					$ovc_ids_chunked = array_chunk( $ovc_ids, 500 );

					foreach( $ovc_ids_chunked as $ovc_ids_chunk ) {

						$feed_results = new OVC_Results();

						// Initialize Feed File
						$feed_file = $this->new_file( $feed_type, 'xml', true );

						// Initialize Feed Object (and in DB)
						$api_feed = OVC_Row_api_feeds::init_feed( $feed_type, $feed_file->ID, $this->ID, $feed_results );

						// Build and submit Feed
						$feed_submitted = false;

						if(	$api_feed->write_feed_to_file( $ovc_ids_chunk, $feed_results ) ) {

							$feed_submitted = $api_feed->submit_feed( $feed_results );
						}					

						$this->log_results_msgs( $feed_results );

						if( $feed_submitted ) {

							$pr_fs = FS( 'ovc_products' );

							foreach( $ovc_ids_chunk as $ovc_id ) {
								$product = OVCDB()->get_row( $pr_fs, $ovc_id );

								$updated_walmart_data = array(
									'wa.last_item_feed_id'	=> $api_feed->ID,
									'wa.sync_status'		=> 'UPDATING',
									'wa.feed_item_status'	=> 'SENT'		// dev:deprecated
								);

								$product->update_data( $updated_walmart_data );
							}
						}
						
						if( !$this->maybe_jump_or_abort() ) {
							return false;	
						}
					}
				}

				$feed_count = isset( $ovc_ids_chunked ) ? count( $ovc_ids_chunked ) : 0;

				$this->log( "Walmart Sync Step {$this->meta( 'walmart_sync_step' )} Complete: " . count( $ovc_ids ) . " SKUs submitted in " . $feed_count . " feeds for data update" );

				$this->meta( 'walmart_sync_step', 6 );
				if( !$this->maybe_jump_or_abort() ) {
					return false;	
				}
			} else {
				$this->meta( 'walmart_sync_step', 6 );
			}
		}

		// STEP 6: BULK UPDATE PRICES FEED
		if( 6 == $this->meta( 'walmart_sync_step' ) ) {

			if( $this->sync_steps[6] ) {

				OVCDB()->flush_db_data();
				
				$feed_type = 'walmart_bulk_update_prices';

				$this->log( "Sync Step {$this->meta( 'walmart_sync_step' )} / {$total_sync_steps}: Bulk Update Walmart Prices" );

				$wa_ids = OVC_Sync_Manager::get_auto_sync_count( $feed_type, true );

				if( !$wa_ids ) {
					$this->log( "No SKUs need walmart price update" );
				}
				else if( !on_vida_live() ) {
					$this->log( "Skipping {$feed_type} feed - Remote API feeds should only be sent from live site." );
				}
				else {
					
					$this->log( count( $wa_ids ) . " SKUs need walmart price update" );

					// Initialize Feed File
					$feed_file = $this->new_file( $feed_type, 'xml', true );

					// Initialize Feed Object (and in DB)
					$api_feed = OVC_Row_api_feeds::init_feed( $feed_type, $feed_file->ID, $this->ID, $feed_results );

					// Build and submit Feed
					$feed_results = new OVC_Results();
					$feed_submitted = false;
					
					if( $api_feed->write_feed_to_file( $wa_ids, $feed_results ) ) {

						$feed_submitted = $api_feed->submit_feed( $feed_results );	
					}

					$this->log_results_msgs( $feed_results );

					if( $feed_submitted ) {

						$pr_fs = FS( 'ovc_products' );

						foreach( $wa_ids as $wa_id ) {

							$walmart_pr = OVCDB()->get_row( $pr_fs, $wa_id );

							$walmart_pr->update_field( 'wa.last_price_feed_id', $api_feed->ID );
						}
					}
				}

				$this->log( "Walmart Sync Step {$this->meta( 'walmart_sync_step' )} Complete: " . count( $wa_ids ) . " SKUs submitted for price update" );

				$this->meta( 'walmart_sync_step', 7 );
				if( !$this->maybe_jump_or_abort() ) {
					return false;	
				}
			} else {
				$this->meta( 'walmart_sync_step', 7 );
			}
		}

		// STEP 7: BULK UPDATE INVENTORY FEED
		if( 7 == $this->meta( 'walmart_sync_step' ) ) {

			if( $this->sync_steps[7] ) {
			
				$feed_type = 'walmart_bulk_update_inventory';

				$this->log( "Sync Step {$this->meta( 'walmart_sync_step' )} / {$total_sync_steps}: Bulk Update Walmart Inventory" );

				// Special handling if we are force-syncing all inventory
				if( $this->config( 'sync_all' ) ) {

					$this->log( "Force-syncing inventory for all active Walmart SKUs" );
					$feed_type = 'walmart_bulk_update_inventory_all';
				}

				$wa_ids = OVC_Sync_Manager::get_auto_sync_count( $feed_type, true );

				if( !$wa_ids ) {
					$this->log( "No SKUs need walmart inventory update" );
				}
				else if( !on_vida_live() ) {
					$this->log( "Skipping {$feed_type} feed - Remote API feeds should only be sent from live site." );
				}
				else {
					$this->log( count( $wa_ids ) . " SKUs need walmart inventory update" );

					// Initialize Feed File
					$feed_file = $this->new_file( $feed_type, 'xml', true );

					// Initialize Feed Object (and in DB)
					$api_feed = OVC_Row_api_feeds::init_feed( $feed_type, $feed_file->ID, $this->ID, $feed_results );

					// Build and submit Feed
					$feed_results = new OVC_Results();
					$feed_submitted = false;
					
					if(	$api_feed->write_feed_to_file( $wa_ids, $feed_results ) ) {

						$feed_submitted = $api_feed->submit_feed( $feed_results );
					}

					if( $feed_results->out_of_stock_count ) {
						$this->log( "WARNING! {$feed_results->out_of_stock_count} SKUs were just synced to Walmart with 0 stock" );
					}		

					$this->log_results_msgs( $feed_results );

					if( $feed_submitted ) {

						$wa_fs = FS( 'ovc_products' );

						foreach( $wa_ids as $wa_id ) {
							$walmart_pr = OVCDB()->get_row( $wa_fs, $wa_id );

							$walmart_pr->update_field( 'wa.last_inventory_feed_id', $api_feed->ID );
						}
					}
				}

				$this->log( "Walmart Sync Step {$this->meta( 'walmart_sync_step' )} Complete: " . count( $wa_ids ) . " SKUs submitted for inventory update" );

				$this->meta( 'walmart_sync_step', 8 );
				if( !$this->maybe_jump_or_abort( true ) ) {
					return false;
				}
			} else {
				$this->meta( 'walmart_sync_step', 8 );
			}
		}

		// STEP 8: BULK UPDATE MANUAL FEED
		if( 8 == $this->meta( 'walmart_sync_step' ) ) {
			
			if( ( 'auto_sync' != $this->config( 'sync_mode' ) ) 
				&& $this->sync_steps[8]
			) {
				OVCDB()->flush_db_data();

				$feed_type = 'walmart_bulk_update_manual';

				$this->log( "Sync Step {$this->meta( 'walmart_sync_step' )} / {$total_sync_steps}: Bulk Manual Updates" );

				$ovc_ids = OVC_Sync_Manager::get_auto_sync_count( $feed_type, true );

				if( !$ovc_ids ) {
					$this->log( "No SKUs need manual updates" );
				}
				else if( !on_vida_live() ) {
					$this->log( "Skipping {$feed_type} feed - Remote API feeds should only be sent from live site." );
				}
				else {
					$this->log( count( $ovc_ids ) . " SKUs need manual updates" );

					$ovc_ids_chunked = array_chunk( $ovc_ids, 500 );

					foreach( $ovc_ids_chunked as $ovc_ids_chunk ) {

						// Initialize Feed File
						$feed_file = $this->new_file( $feed_type, 'xml', true );

						// Initialize Feed Obejct ( and in DB)
						$api_feed = OVC_Row_api_feeds::init_feed( $feed_type, $feed_file->ID, $this->ID, $feed_results );

						// Build and submit Feed
						$feed_results = new OVC_Results();
						$feed_submitted = false;

						if( $api_feed->write_feed_to_file( $ovc_ids, $feed_results ) ) {

							$feed_submitted = $api_feed->submit_feed( $feed_results );
						}				

						$this->log_results_msgs( $feed_results );

						if( $feed_submitted ) {

							$pr_fs = FS( 'ovc_products' );

							foreach( $ovc_ids_chunk as $ovc_id ) {
								$product = OVCDB()->get_row( $pr_fs, $ovc_id );

								$updated_walmart_data = array(
									'wa.last_item_feed_id'	=> $api_feed->ID,
									'wa.sync_status'		=> 'UPDATING',
									'wa.feed_item_status'	=> 'SENT'		// dev:deprecated
								);

								$product->update_data( $updated_walmart_data );
							}
						}
					}
				}

				$this->log( "Walmart Sync Step {$this->meta( 'walmart_sync_step' )} Complete: " . count( $ovc_ids ) . " SKUs submitted for inventory update" );

				$this->meta( 'walmart_sync_step', 9 );
				if( !$this->maybe_jump_or_abort( true ) ) {
					return false;
				}
			} else {
				$this->meta( 'walmart_sync_step', 9 );
			}
		}

		// STEP 9: REFRESH LOCAL WALMART FEED DATA
		// dev:improve Remove this when we change to using the name of the step as it will always run
		if( 9 == $this->meta( 'walmart_sync_step' ) ) {
			
			if( $this->sync_steps[9] ) {
				$this->log( "Sync Step {$this->meta( 'walmart_sync_step' )} / {$total_sync_steps}: Re-Refreshing Local Walmart Feed Data." );

				$this->refresh_local_walmart_feeds();
				
				$this->meta( 'walmart_sync_step', 10 );
				if( !$this->maybe_jump_or_abort( true ) ) {
					return false;
				}
			} else {
				$this->meta( 'walmart_sync_step', 10 );
			}
		}

		/*
		// STEP 10: REFRESH LOCAL WALMART PRODUCT DATA
		// dev:improve Remove this when we change to using the name of the step as it will always run
		if( 10 == $this->meta( 'walmart_sync_step' ) ) {
			
			if( $this->sync_steps[10] ) {
				$this->log( "Sync Step {$this->meta( 'walmart_sync_step' )} / {$total_sync_steps}: Re-Refreshing Local Walmart Product Data." );

				$this->refresh_local_walmart_products();

				$results = new OVC_Results();
				OVC_Walmart_API::check_walmart_ovc_id_sku_match( $results );
				$this->log_results_msgs( $results );

				$this->meta( 'walmart_sync_step', 9 );
				if( !$this->maybe_jump_or_abort() ) {
					return false;
				}
			}
		}
		*/
		
		$this->successful_finish();
	}

	public function maybe_jump_or_abort( $force_jump = false ) {

		$this->check_user_abort();

		if( $force_jump || 50 < ox_exec_time() ) {
			$this->log( "Preparing to jump Walmart Sync operation. Current Walmart Sync Step: {$this->meta( 'walmart_sync_step' )}" );
			$this->log( "Exec time: " . ox_exec_time() );

			$this->jump_operation();
			return false;
		}
		else {
			return true;
		}

	}

	protected function refresh_local_walmart_products( &$results = false ) {
		OVC_Results::maybe_init( $results );
 
		if( $this->jumped && ( $this->last_csv_line > 0 ) ) {
			$this->log( "Resuming Refresh of Local Walmart Product data at offset {$this->last_csv_line}" );
		}
		// NOT JUMPED / FIRST TIME THROUGH
		else {
			//$this->remove_cancelled_skus(); //dev:wasync

			$this->last_csv_line = 0;
		}

		// Retrieve and initialize Items report CSV if not done already
		if( !$this->csv_file_path ) {

			$this->log( 'Requesting Walmart Items Report CSV' );
			$items_response = OVC_API_Walmart::request( 'get_items_report' );
			$zip_file = OVCOP_CONTENT_DIR . '/walmart_items_report_test.zip';
			$unzip_dir = OVCOP_CONTENT_DIR . '/walmart-items-report/';

			$file_path = $unzip_dir;
			// Put response into zip file 
			file_put_contents( $zip_file, $items_response->format('raw') );

			$zip = new ZipArchive;
			$res = $zip->open($zip_file);
			if ($res === TRUE ) {
				$zip->extractTo($unzip_dir);
				$zip->close();
			}

			$dir_handle = opendir( $unzip_dir );

			// Get the most recent entry 
			$entry_name = "";
			$entry_time = 0;
			while( $entry = readdir( $dir_handle ) ) {
				if ($entry != "." 
					&& $entry != ".." 
					// Ensure file is CSV file
					&& (pathInfo($unzip_dir . $entry, PATHINFO_EXTENSION) == "csv")) {
					if ($entry_time <= filemtime( $unzip_dir . $entry)){
						$entry_time = filemtime( $unzip_dir . $entry);
						$entry_name = $entry;
					}
		        }
			}
			$file_path .= $entry_name;

			$this->csv_file_path = $file_path;
		}

		$this->log( 'Refreshing products from CSV: ' . $this->csv_file_path );

		$csv_file = fopen( $this->csv_file_path, 'r' );

		$wa_csv_fs = OVCSC::get_field_set( 'walmart_items_csv' );

		$import_fields = $wa_csv_fs->get_single_meta_key_values( 'ovc_field' );

		$csv_headers = fgetcsv( $csv_file );
		$csv_line = 1;

		if( $csv_headers !== $wa_csv_fs->get_fields( 'local_name' ) ){
			return $results->error( 'Unable to import Walmart Items CSV. Invalid CSV headers' );
		}
		
		while( false !== ( $csv_row = fgetcsv( $csv_file ) ) ) {
			$csv_line++;

			if( $csv_line <= $this->last_csv_line ) {
				continue;
			}

			// Turn raw CSV numerically indexed array into associative array with col headers
			$csv_row = array_combine( $csv_headers, $csv_row );

			// Remove cols that don't get imported into OVC
			$csv_import_data = array_intersect_key( $csv_row, $import_fields );

			// Replace CSV header keys with OVC field keys
			$csv_import_data = array_combine( $import_fields, $csv_import_data );

			// Retrieve local walmart product from response
			$update_walmart_item_results = new OVC_Results();
			$ovc_walmart_product = OVC_Row_walmart_product::get_from_api_response( $csv_import_data, $update_walmart_item_results );

			if( $ovc_walmart_product 
				&& $ovc_walmart_product->handle_api_response( $csv_import_data, $update_walmart_item_results )
			) {

				$this->stat( 'items_csv_rows_success' );
			}
			else {
				
				$this->stat( 'items_csv_rows_failed' );
				$this->log_results_msgs( $update_walmart_item_results );
				$this->log( "Parsed data from CSV Line {$csv_line} \r\n" .print_r( $csv_import_data, true ) );
			}

			$this->stat( $update_walmart_item_results->walmart_product_match_type );
			$this->stat( 'items_csv_rows_total' );
			$this->last_csv_line = $csv_line;

			if( 0 == $csv_line % 100 ) {

				if( 0 == $csv_line % 400 ) {
					$this->log( "Last CSV line processed: {$csv_line}" );	
				}

				$this->maybe_jump_or_abort();
			}

		}

		$this->log( "Refreshing Local Walmart Product data COMPLETED. Last CSV Line processed" );
	}

	protected function refresh_local_walmart_feeds() {

		$updated_count = 0;

		$feed_ids = OVC_Sync_Manager::get_auto_sync_count( 'walmart_feeds', true );

		$this->log( "Auto-updating Walmart feeds: " . count( $feed_ids ) . " feeds to update." );

		foreach( $feed_ids as $feed_id ) {

			$feed_update_results = new OVC_Results();
			$api_feed = OVCDB()->get_row( FS('api_feeds'), $feed_id, $feed_update_results, false );

			if( $api_feed->remote_feed_id ) {

				$this->log( "Updating Feed and Feed items for feed ID: {$api_feed->ID}, Remote ID: {$api_feed->remote_feed_id}" );

				$api_feed->refresh_walmart_feed_items_status( $feed_update_results );

				$this->log_results_msgs();

				$updated_count++;
			}
			else {
				$this->log( "ERROR Refreshing Local Walmart Feed: Invalid remote feed ID. Local feed id: {$api_feed->ID}" );

				$api_feed->update_data( array( 'status' => 'ERROR' ), $feed_update_results );
			}

			$this->maybe_jump_or_abort();
		}

		// Retire Walmart SKUs
		// $this->retire_walmart_skus();

		$this->log( "Walmart feeds update completed: {$updated_count} feeds updated." );
	}

	// Remove SKUs from wp_ovc_walmart_products that were never created and pr.sync_walmart = 0 (sync was cancelled before SKU was created in walmart)
	protected function remove_cancelled_skus() {
		
		// Get array of wa.ID to delete from wp_ovc_walmart_products
		$wa_ids = OVC_Sync_Manager::get_auto_sync_count( 'walmart_cancel_items', true );

		if( $wa_ids ) {

			global $wpdb;
			
			$delete_result = $wpdb->query("DELETE FROM wp_ovc_walmart_products WHERE ID IN(" . implode( ",", $wa_ids ) . ")");

			if( $delete_result ) {

				$this->log( "Walmart Sync cancelled SKUs: {$delete_result} SKUs were removed from local Walmart product data" );
			}
			else {
				$this->log( "Error cancelling SKUs for Walmart sync!" );
			}
		}
	}

	// Initialize Walmart SKUs that are to be synced and have not been created yet
	protected function initialize_walmart_skus_for_sync() {

		// Get array of OVC IDs that should be PENDING Walmart Sync
		$ovc_ids = OVC_Sync_Manager::get_auto_sync_count( 'walmart_init_items', true );

		$init_count = 0;
		if( $ovc_ids && is_array( $ovc_ids ) ) {

			foreach( $ovc_ids as $ovc_id ) {

				$results = new OVC_Results();
				$product = OVCDB()->get_row( 'ovc_products', $ovc_id, $results );

				$ovc_walmart_product_data = array(
					'wa.ovc_id'			=> $ovc_id,
					'wa.sku'			=> $product->sku,
					'wa.sync_status'	=> 'PENDING'
				);

				$walmart_row = OVCDB()->new_row( FS( 'walmart_product' ) );

				if( $walmart_row->update_data( $ovc_walmart_product_data, $results ) ) {
					$init_count++;
				}
				else {
					$this->log( "ERROR initializing walmart product data for OVC ID {$product->ID}, SKU {$product->sku}" );
					$this->log_results_msgs( $results );
				}
			}
		}

		$this->log( "{$init_count} new SKUs initialized for Walmart Sync." );	

		return $init_count;
	}

	// Retire Walmart SKUs that have been synced but have pr.sync_walmart = 0
	protected function retire_walmart_skus() {

		$wa_ids = OVC_Sync_Manager::get_auto_sync_count( 'walmart_retire_items', true );

		if( is_array( $wa_ids ) && count( $wa_ids ) ) {

			$this->log( "RETIRING WALMART SKUs where pr.sync_walmart = 0: " . count( $wa_ids ) . " SKUs to retire" );

			$wa_fs = FS('walmart_product');
			
			foreach( $wa_ids as $wa_id ) {

				$walmart_product = OVCDB()->get_row( $wa_fs, $wa_id );
				$retire_results = new OVC_Results();

				OVC_API_Walmart::retire_walmart_sku( $walmart_product->data( 'wa.sku' ), $this->wapi, $retire_results );

				$this->log_results_msgs( $retire_results );
			}			
		}
	}

	protected function check_duplicate_skus() {
		global $wpdb;

		// 1. Update 
	}
}
