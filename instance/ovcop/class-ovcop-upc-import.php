<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;

class OVCOP_upc_import extends OVCOP {

	public $type = 'upc_import';

	public static function extra_init( $ovcop ) {

		$ovcop->new_file( 'csv', 'csv', true );

		if( 'upload' == $ovcop->config( 'file_source' ) ) {

			$ovcop->op_file()->write( stripslashes( $ovcop->config( 'import_csv_upload' ) ) );

			// Delete uploaded CSV data from op_data so we don't save huge text files to the database
			$ovcop->config( 'import_csv_upload', null ); //dev:improve - maybe adjust OVCOP so we don't ever have to save this to the DB in the first place?

			$ovcop->results->main_msg = "UPC Import initialized. Uploading CSV File...";
		}

		// $ovcop->log( "IMPORT MODE: {$ovcop->config( 'import_mode' )}" );

		$ovcop->init_stat( 'total_rows', '# of rows' );
		$ovcop->init_stat( 'error_rows', '# of rows with errors' );
		$ovcop->init_stat( 'updated_upcs', '# of updated UPCs' );
		$ovcop->init_stat( 'updated_tag_upcs', '# of updated tag UPCs' );

		return $ovcop->results->success = true;
	}

	public function __construct( $request, $args ) {

		$this->child_construct( $request, $args );

		$this->import();	
	}

	protected function import() {

		if( $this->initialize_csv_file() ) {

			$this->log( '' );

			$current_line = 1;

			$csv_file_handle = fopen( $this->op_file()->path(), 'r' );

			while( $csv_row = fgetcsv( $csv_file_handle ) ) {

				$results = new OVC_Results();

				$ovc_sku 	= $csv_row[0];
				$upc 		= $csv_row[1];
				$tag_upc 	= $csv_row[2];

				if( !$ovc_sku ) {

					$this->log( "SKU missing in CSV on line {$current_line}" );
					$this->stat( 'error_rows' );
					continue;
				}

				$ovc_product = OVCDB()->get_row_by_valid_id( FS( 'ovc_products' ), 'sku', $ovc_sku );

				$updated_data = array();

				if( $ovc_product->exists() ) {

					if( $upc ) {

						$updated_data['pr.upc_code'] = $upc;
						$this->stat( 'updated_upcs' );
					}

					if( $tag_upc ) {

						$updated_data['pr.tag_barcode'] = $tag_upc;
						$this->stat( 'updated_tag_upcs' );
					}

					$ovc_product->update_data( $updated_data, $results );

					$this->log_results_msgs( $results );
				} else {

					$this->log( "SKU \"{$ovc_sku}\" doesn't exist" );
				}

				$current_line++;
			}

			fclose( $csv_file_handle );
		}

		$this->log( '' );
		$this->log( 'UPC Import completed successfully.' );

		$this->successful_finish();
	}

	protected function initialize_csv_file() {

		$csv_is_valid = true;
		$csv_line_count = 0;
		$csv_max_chars = 0;

		$csv_file = file( $this->op_file()->path() );

		$csv_line_count = count( $csv_file );

		if( $csv_line_count < 1 ) {

			$this->abort_operation( "Invalid row count in CSV. Number of rows detected (including field heading): {$csv_line_count}", false );
			$csv_is_valid = false;
		}

		$this->stat( 'total_rows', $csv_line_count );

		return $csv_is_valid;
	}
}
