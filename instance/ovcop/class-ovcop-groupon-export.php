<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVCOP_groupon_export extends OVCOP {
	public $type = 'groupon_export';

	// Simple operation-specific properties (re-initialized for each commence_operation request)
	public $export_field_set = array(); // Groupon Field Map (with fieldmeta information)

	// Semi-magic properties via $op_data['data']
	public $parent_skus;
	public $exported_ids;


	// Operation Type Specific Initialization
	public static function extra_init( $ovcop ) {
		global $wpdb;

		// Initialize operation data based on configuration options
		$ovcop->parent_skus = array_map( 'sanitize_text_field', array_map( 'trim', explode( ",", $ovcop->config( 'filter_parent_skus' ) ) ) );

		if( !is_array( $ovcop->parent_skus ) || 0 == count( $ovcop->parent_skus ) ) {
			$ovcop->abort_operation( "Invalid entry in Parent SKUs field." );
		}

		$ovcop->exported_ids = array();

		// Initialize Export CSV
		$ovcop->new_file( 'csv', 'csv', true );

		// Send initialization response 
		$ovcop->results->success = true;
		$ovcop->results->main_msg = "Groupon Export operation initialized successfully";

		// Tell OVCOP to automatically commence the operation
		return true;
	}


	public function __construct( $request, $args ) {
		$this->child_construct( $request, $args );

		// Initialize Groupon Field Set
		$this->export_field_set = OVCSC::get_field_set( 'groupon' );

		$this->log( count( $this->parent_skus ) . " Parent SKUs remaining." );

		$this->export();
	}

	public function export() {

		if ( !$this->jumped ) {
			// Write headers line to CSV
			$this->write_csv_line_from_array( array_map( array( $this, 'csv_wrap_value' ), $this->export_field_set->get_fields( 'local_name' ) ) );
		}

		global $wpdb;

		$prfs = OVCSC::get_field_set( 'ovc_products' );

		while( count( $this->parent_skus ) ) {
			// Shift [up to] 5 Parent SKUs off their array and build $qry['where2'] filter with them
			$temp_parent_skus = array();
			for( $s = 1; $s <= 5; $s++ ) {
				if( count( $this->parent_skus ) ) {
					$temp_parent_skus[] = array_shift( $this->parent_skus );
				}
				else break;
			}

			$ovc_ids = $wpdb->get_col( "SELECT ID FROM wp_ovc_products WHERE parent_sku IN(\"" . implode( "\",\"", $temp_parent_skus ) . "\") AND sync_oms = 1" );
	
			foreach( $ovc_ids as $ovc_id ) {
				$results = new OVC_Results;
				$ovcp = OVCDB()->get_row( $prfs, $ovc_id, $results );
				$csv_row_values = array();
				
				foreach( $this->export_field_set->get_fields() as $field ) {
					$gr_value = $ovcp->get_external_value( $field, $results );

					if( false === $gr_value ) {
						$results->error( "ERROR! OVC ID: {$ovc_id} - groupon_value is false for field {$field}" );
					}

					$escape_csv_value = ! (bool) $field->meta( 'csv_no_escape' );
	
					$csv_row_values[] = $this->csv_wrap_value( $gr_value, $escape_csv_value );
				}

				foreach( $results->get_msgs() as $msg ) {
					$this->log( $msg );
				}

				$this->write_csv_line_from_array( $csv_row_values );
				$this->exported_ids[] = $ovcp->ID;
			}

			$this->check_user_abort();
		}

		$this->successful_finish();
	}

	public function before_successful_finish() {
		// Set stat for final report
		$this->init_stat( 'exported_products', '# of Products Exported' );
		$this->stat( 'exported_products', count( $this->exported_ids ) );
	}

	public function after_successful_finish() {
		// Output link to download CSV
		$this->log();
		$this->log( "Download exported CSV here: " . $this->op_file()->url() );
		$this->log();
	}
}	