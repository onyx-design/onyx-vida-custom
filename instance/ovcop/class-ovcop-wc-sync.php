<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVCOP_wc_sync extends OVCOP {
	public $type = 'wc_sync';
	public $op_step = 3;

	// Semi-magic properties via $op_data['data']
	public $parent_skus;
	public $remaining_children;
	public $force_parent_jump;

	// Request-level properties (not stored in $op_data['data'])
	public $ovc_product;

	public static function extra_init( $ovcop ) {
		global $wpdb;

		// Initialize operation data based on configuration options
		if( 'sync_all' == $ovcop->config( 'sync_mode' ) ) {
			$ovcop->log( "SYNC MODE: sync_all - Syncing all OVC products with WC" );

			// dev:sync_revamp
			$ovcop->parent_skus = $wpdb->get_col( "SELECT DISTINCT( parent_sku ) FROM wp_ovc_products WHERE sync_oms = 1 AND op_oms_export_unconfirmed != 0" );
		}
		else if( 'auto_sync' == $ovcop->config( 'sync_mode' ) ) {
			$ovcop->log( "SYNC MODE: auto_sync - Only syncing relevant products that have been updated since their last WooCommerce sync. ( " . OVC_Sync_Manager::get_auto_sync_count( 'wc_sync' ) . " OVC IDs to sync )" );			

			$ovcop->parent_skus = $wpdb->get_col( "
				SELECT DISTINCT( pr.parent_sku )
				FROM (
				SELECT pr.ID
				FROM wp_ovc_products pr
				LEFT JOIN wp_ovc_parent_data pa
					ON pr.parent_sku = pa.parent_sku
				LEFT JOIN wp_ovc_style_data st
					ON pr.sku_style = st.style
				LEFT JOIN wp_ovc_image_sets img
					ON pr.image_set = img.ID
				LEFT JOIN wp_ovc_data_fixes fix
					ON pr.ID = fix.ovc_id
				WHERE fix.ovc_id IS NULL 
					AND (
						(
							( pr.post_id != 0 OR pr.sync_wc = 1 )
							AND GREATEST( pr._data_updated, pr._stock_updated, pr._price_updated, pa._meta_updated, st._meta_updated, img._meta_updated ) > pr.last_wc_sync
						) OR (
							( pr.sync_wc = 1 AND ( pr.sync_oms = 0 OR pr.oms_sku_confirmed = '' ) )
						)
					)
				) view_wc
				INNER JOIN wp_ovc_products pr
					ON view_wc.ID = pr.ID
			" );
		}
		else if( 'ovc_ids' == $ovcop->config( 'sync_mode' ) ) {
			$ovc_ids = preg_replace( '/\s+/', '', $ovcop->config( 'sync_mode_input' ) );

			$ovcop->parent_skus = $wpdb->get_col(
				"SELECT DISTINCT( pr.parent_sku )
					FROM wp_ovc_products pr
					WHERE pr.ID IN( {$ovc_ids} )"
			);

			$ovcop->log( "SYNC MODE: ovc_ids - Only syncing products from input IDs. ( " . count( $ovcop->parent_skus ) . " OVC IDs to sync )" );
		}
		else if( 'parent_skus' == $ovcop->config( 'sync_mode' ) ) {
			$ovcop->parent_skus = explode( ',', $ovcop->config( 'sync_mode_input' ) );

			$ovcop->log( "SYNC MODE: parent_skus - Only syncing products from input SKUs. ( " . count( $ovcop->parent_skus ) . " OVC IDs to sync )" );
		}
		else {
			$ovcop->abort_operation( "Invalid value for sync_mode: " . strval( $ovcop->config( 'sync_mode' ) ) );
		}

		$ovcop->remaining_children = array();

		$ovcop->force_parent_jump = false;

		/*/ INITIALIZE STATS VARIABLES
		$ovcop->init_stat( 'new_parents', 'New Parents' );
		$ovcop->init_stat( 'new_variations', 'New Variations' );
		$ovcop->init_stat( 'updated_variations', 'Updated Variations' );
		$ovcop->init_stat( 'skipped', 'Skipped OVC IDs' );
		$ovcop->init_stat( 'locked', 'Locked' );
		$ovcop->init_stat( 'errors', 'Errors' );
		$ovcop->init_stat( 'checked', 'Checked' );
		*/

		// Send initialization response 
		$ovcop->results->success = true;
		$ovcop->results->main_msg = "WC Sync operation initialized successfully";

		// Tell OVCOP to automatically commence the operation
		return true;
	}

	public function __construct( $request, $args ) {
		$this->child_construct( $request, $args );

		$this->sync();	
	}

	public function sync() {

		// Preload active data errors
		OVC_Row_data_errors::enable();
		OVC_Row_data_errors::load_active_errors();

		if( OVC_Sync_Manager::get_auto_sync_count( 'wc_delete_variations' ) ) {
			$this->delete_variations();
		}

		$this->log( count( $this->parent_skus ) . " Parent SKUs remaining" );
		$this->log( "" );

		while( count( $this->parent_skus ) ) {

			//Check if need to jump operation
			if( 50 < ox_exec_time() ) {				
				$this->jump_operation();
			}

			$this->check_user_abort();

			// Pop a parent_sku off the end of the $parent_skus array and use it to initialize a parent product
			$ovc_parent = OVCDB()->get_row_by_valid_id( FS( 'parent_data' ), 'parent_sku', array_pop( $this->parent_skus ) );

			$ovc_wc_parent = wc_get_product( $ovc_parent->data( 'pa.wc_parent_id' ) );

			// If we are landing a jump from mid-parent sync, there should be remaining_children so don't initialize children to sync
			if( !$this->remaining_children ) {

				// Get children to sync (maybe filtered)
				// dev:ovc_row
				//$children_filter = 'auto_sync' == $this->config('sync_mode') ? 'wc_sync' : false;
				$children_filter = false;
				$wc_sync_child_ids = array();
				if( 'auto_sync' == $this->config( 'sync_mode' ) ) {

					global $wpdb;
					
					$wc_sync_child_ids = $wpdb->get_col( "
						SELECT view_wc.ID
						FROM (
							SELECT pr.ID
							FROM wp_ovc_products pr
							LEFT JOIN wp_ovc_parent_data pa
								ON pr.parent_sku = pa.parent_sku
							LEFT JOIN wp_ovc_style_data st
								ON pr.sku_style = st.style
							LEFT JOIN wp_ovc_image_sets img
								ON pr.image_set = img.ID
							LEFT JOIN wp_ovc_data_fixes fix
								ON pr.ID = fix.ovc_id
							WHERE fix.ovc_id IS NULL 
							AND (
								(
							 		( pr.post_id != 0 OR pr.sync_wc = 1 )
							 		AND GREATEST( pr._data_updated, pr._stock_updated, pr._price_updated, pa._meta_updated, st._meta_updated, img._meta_updated ) > pr.last_wc_sync
							 	) OR (
									( pr.sync_wc = 1 AND ( pr.sync_oms = 0 OR pr.oms_sku_confirmed = '' ) )
								)
							)
						) view_wc
						INNER JOIN wp_ovc_products pr
							ON view_wc.ID = pr.ID
						WHERE pr.parent_sku = '{$ovc_parent->parent_sku}'
					" );

				} elseif( 'parent_skus' == $this->config( 'sync_mode' ) ) {

					global $wpdb;

					$wc_sync_child_ids = $wpdb->get_col(
						"
						SELECT pr.ID
						FROM wp_ovc_products pr
						WHERE pr.parent_sku = '{$ovc_parent->parent_sku}'
						"
					);
				}

				if( !empty( $wc_sync_child_ids ) ) {

					// If we jumped mid-parent sync, then filter
					$children_filter = array(
						'op'	=> 'in_array',
						'var'	=> $wc_sync_child_ids,
						'value'	=> '::[ID]'
					);
				}

				$sync_children_rows = $ovc_parent->get_children( FS( 'ovc_products' ), 'object', $this->results, $children_filter );

				$child_ids = array();

				foreach( $sync_children_rows as $sync_child ) {

					$failed_check = OVC_Row_error_codes::check_ovc_product_error_code( 'sync_wc_required_fields', $sync_child, $this->results );

					// Validate the child and add to $child_ids if valid
					if( !is_null( $failed_check ) && $failed_check !== true ) {
						continue;
					}
					
					$child_ids[] = $sync_child->ID;
				}

				$forced_parent = $child_ids ? '' : ' (forced)';
				$this->log( "Syncing Parent {$ovc_parent->parent_sku} - " . count( $child_ids ) . " SKUs to sync" . $forced_parent );

				// If auto_sync and no children, force the parent to sync (need to get one child) so that it deletes WC variants and parent
				if( !$child_ids 
					&& 'auto_sync' == $this->config( 'sync_mode' ) 
				) {
					$child_ids = $ovc_parent->get_children( FS('ovc_products'), 'ID' );

					if( $child_ids ) {

						//$child_product = new OVC_Product_WooCommerce( 'ovc_products', $child_ids[0] );
						//$children = array( $child_product );

						// We only need the first child_id just to be able to use the wc_sync function to force sync the parent
						$child_ids = array( $child_ids[0] );
					}	
				}

				$this->remaining_children = $child_ids;
			}
			else {

				$this->log( "Continuing sync of Parent {$ovc_parent->parent_sku} - " . count( $this->remaining_children ) . " SKUs remaining" );
			}

			$this->log_results_msgs( $this->results );
			
			$this->use_log_prefix = true;

			while( $ovc_product_id = array_pop( $this->remaining_children ) ) {

				// Unfortunately because of OVCDB Row handling updates (Aug 2018), we have to get the actual OVC row so we can send the data to the special OVC_Product_WooCommerce row
				$ovc_pr = OVCDB()->get_row( FS( 'ovc_products' ), $ovc_product_id );

				$this->ovc_product = new OVC_Product_WooCommerce( FS( 'ovc_products' ), $ovc_product_id, $ovc_pr->data_set() );
				$results = new OVC_Results;

				// If we have error checking on, validate the row and continue if invalid
				$failed_check = OVC_Row_error_codes::check_ovc_product_error_code( 'sync_wc_required_fields', $ovc_pr, $results );
				if( !is_null( $failed_check ) && $failed_check !== true ) {

					$this->log( 'Data Errors detected on OVC Product. Check the OVC Data Errors for more information.' );
					continue;
				}

				// If no more children left to sync, sync wc_parent
				$sync_parent = !boolval( count( $this->remaining_children ) );

				// If we need to sync the parent, and the parent has many variations in WC, jump first
				if( $sync_parent
					&& !$this->force_parent_jump
					&& ( $ovc_wc_parent && count( $ovc_wc_parent->get_children() ) > 50 )
				) {

					$this->force_parent_jump = true;

					$this->log( 'Beginning jump to sync Parent ' . $ovc_parent->parent_sku, false );

					// Readd the parent_sku to the parent_skus array
					$this->parent_skus[] = $ovc_parent->parent_sku;
					// Readd the remaining child as we need it to sync the parent
					$this->remaining_children[] = $ovc_product_id;
					$this->jump_operation();
				}

				$this->force_parent_jump = false;
				$mid_parent_jump = false;

				// Also sync_parent if we are going to need to jump the operation in the middle of a parent
				if( 45 < ox_exec_time() ) {
					
					$mid_parent_jump = true;

					/*
					 * August 20, 2018
					 * We have no need to sync the parent mid-jump anymore as we're keeping track of the remaining children
					 *
					 * This was causing the sync to sometimes hit the execution limit as syncing the parent added 20-40
					 * seconds to the sync
					 */
					// $sync_parent = true;

					$this->log( 'Reaching maximum execution time. Beginning mid-parent jump.', false );
				} 

				$this->ovc_product->wc_sync( $results, $sync_parent );

				$this->log_results_msgs( $results );

				if( $mid_parent_jump ) {

					// If we are doing a mid-parent jump, replace the parent_sku that just got popped off of the parent_skus array
					$this->parent_skus[] = $ovc_parent->parent_sku;
					$this->jump_operation();
				}
				// If this is the last child but we are close to exec time limit, jump
				elseif( 50 < ox_exec_time() ) {

					$this->jump_operation();
				}
			}

			$this->use_log_prefix = false;
		}

		/**
		 * Remove unused color/size terms
		 * @deprecated WooCommerce changed the REST API functions
		 */
		// $this->delete_unused_terms();

		/**
		 * Remove orphaned variations
		 * @deprecated WooCommerce changed the REST API functions
		 */
		// $this->delete_orphaned_variations();

		$this->successful_finish();
	}

	public function delete_variations() {

		$variation_ids = OVC_Sync_Manager::get_auto_sync_count( 'wc_delete_variations', true );

		$this->log( "Deleting WC Products before sync. " . count( $variation_ids ) . ' variations to delete.' );

		foreach( $variation_ids as $wc_variation_id ) {

			$wc_variation = wc_get_product( $wc_variation_id );

			if( $wc_variation->exists() ) {

				if( $wc_variation->delete( true ) ) {
					$this->log( 'Deleted WP Post / WC Variation, ID: ' . $wc_variation_id );
				}
				else {
					$this->log( 'Error deleting WP Post / WC Variation, ID: ' . $wc_variation_id );
				}

				wc_delete_product_transients( $wc_variation_id );

				$wc_parent_id = $wc_variation->get_parent_id();

				wc_delete_product_transients( $wc_parent_id );

				$wc_parent = wc_get_product( $wc_parent_id );

				if( $wc_parent->exists() ) {

					if( !$wc_parent->get_children() ) {

						if( $wc_parent->delete( true ) ) {
							$this->log( 'Deleted WP Post / WC Variable Parent, ID: ' . $wc_parent_id );
						}
						else {
							$this->log( 'Error deleting WP Post / WC Variable Parent, ID: ' . $wc_parent_id );
						}
					}
				}
			}
		}
	}

	public function delete_unused_terms() {

		$this->log();
		$this->log( 'Removing unused product attributes' );

		// WooCommerce Tools - Recount Terms
		$tools_controller = new WC_REST_System_Status_Tools_Controller();
		$recounted_terms = $tools_controller->execute_tool( 'recount_terms' );

		$this->log( 'Recounting product attribue terms' );
		if( $recounted_terms['success'] ) {
			$this->log( $recounted_terms['message'] );
		}

		$this->log( 'Checking color attributes for unused terms' );
		$color_terms = get_terms(
			array(
				'taxonomy'		=> 'pa_color',
				'hide_empty'	=> 0,
				'orderby'		=> 'count'
			)
		);

		foreach( $color_terms as $color_term ) {

			if( $color_term->count == 0 ) {

				if( wp_delete_term( $color_term->term_id, $color_term->taxonomy ) ) {

					$this->log( "Removed ID: {$color_term->term_id}, Color: {$color_term->name}, Slug: {$color_term->slug}" );
				}
			} else {
				// Terms are ordered by count, so break when we hit a term that has members
				break;
			}
		}

		$this->log( 'Checking size attributes for unused sizes' );
		$size_terms = get_terms(
			array(
				'taxonomy'		=> 'pa_wc-size',
				'hide_empty'	=> 0,
				'orderby'		=> 'count'
			)
		);

		foreach( $size_terms as $size_term ) {

			if( $size_term->count == 0 ) {

				if( wp_delete_term( $size_term->term_id, $size_term->taxonomy ) ) {

					$this->log( "Removed ID: {$size_term->term_id}, Color: {$size_term->name}, Slug: {$size_term->slug}" );
				}
			} else {
				// Terms are ordered by count, so break when we hit a term that has members
				break;
			}
		}
	}

	public function delete_orphaned_variations() {

		$this->log();
		$this->log( 'Removing orphaned variations' );

		// WooCommerce Tools - Delete Orphaned Variations
		$tools_controller = new WC_REST_System_Status_Tools_Controller();
		$deleted_orphaned_variations = $tools_controller->execute_tool( 'delete_orphaned_variations' );

		if( $deleted_orphaned_variations['success'] ) {
			$this->log( $deleted_orphaned_variations['message'] );
		}

		// $deleted_orphaned_variations = $wpdb->query(
		// 	"
		// 	SELECT products.ID
		// 	FROM {$wpdb->posts} products
		// 	LEFT JOIN {$wpdb->posts} wp 
		// 		ON wp.ID = products.post_parent
		// 	WHERE wp.ID IS NULL 
		// 		AND products.post_type = 'product_variation';
		// 	"
		// );

		// if( $deleted_orphaned_variations ) {
		// 	$this->log( "Deleted " . count( $deleted_orphaned_variations ) . " orphaned variations" );
		// } else {
		// 	$this->log( 'There was a problem deleting orphaned variations.' );
		// }



		// "DELETE products
		// FROM {$wpdb->posts} products
		// LEFT JOIN {$wpdb->posts} wp ON wp.ID = products.post_parent
		// WHERE wp.ID IS NULL AND products.post_type = 'product_variation';"
	}

	public function log_prefix() {
		return "ID: " . str_pad( $this->ovc_product->ID, 10 ) . "SKU: " . str_pad( $this->ovc_product->sku, 28 ) . " - ";
	}
}

