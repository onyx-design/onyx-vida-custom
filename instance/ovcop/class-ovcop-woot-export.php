<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVCOP_woot_export extends OVCOP {
	public $type = 'woot_export';
	public $op_step = 3;

	// Simple operation-specific properties (re-initialized for each commence_operation request)
	public $export_field_set = array(); // Export Field Map (with fieldmeta information)
	public $excel = false;

	// Semi-magic properties via $op_data['data']
	public $parent_skus;
	public $exported_ids;
	

	// Operation Type Specific Initialization
	public static function extra_init( $ovcop ) {
		global $wpdb;

		// Initialize operation data based on configuration options
		$ovcop->parent_skus = false;
		if( 'parent_skus' == $ovcop->config( 'sku_filter' ) ) {
			$ovcop->parent_skus = array_map( 'sanitize_text_field', array_map( 'trim', explode( ",", $ovcop->config( 'filter_parent_skus' ) ) ) );
		}
		$ovcop->log( "SKU Filtering: {$ovcop->config( 'sku_filter' )}" );

		$ovcop->exported_ids = array();

		// Initialize Output File
		if( 'excel' == $ovcop->config( 'format' ) ) {

			$template_filename = 'woot.xls';
			$template_filetype = 'xls';

			$ovcop->export_fs_name = 'woot_';

			
			$ovcop->new_file( $template_filetype, $template_filetype, true );
			$ovcop->template_file = OVCOP_TEMPLATE_DIR . '/' . $template_filename;

			if( !file_exists( $ovcop->template_file )
				//|| !copy( $ovcop->template_file, OVCOP_CONTENT_DIR . '/' . $ovcop->csv_file )
			) {
				$ovcop->results->success = false;
				$ovcop->results->main_msg = "Initialization failed. Excel template not found.";
				return false;
			}
		}
		else if( 'csv' == $ovcop->config( 'format' ) ) {
			// Create CSV and set it to be the current op_file
			$ovcop->new_file( 'csv', 'csv', true );
			
			// Update csv_file name in OVCOP table
			//$ovcop->csv_file = $ovcop->generate_file_name();
			// Send initialization response 
		}
		else {
			$ovcop->results->success = false;
			$ovcop->results->main_msg = "Initialization failed. Invalid output format type.";
			return false;
		}

		// Send initialization response 
		$ovcop->results->success = true;
		$ovcop->results->main_msg = "Woot Export operation initialized successfully";

		// Tell OVCOP to automatically commence the operation
		return true;
	}


	public function __construct( $request, $args ) {
		$this->child_construct( $request, $args );

		// Initialize Export Field Set
		$this->export_field_set = OVCSC::get_field_set( $this->optype_meta( 'field_set' ) );

		if( 'excel' == $this->config( 'format' ) ) {
			$this->excel = $this->get_php_excel( $this->template_file );

			$this->excel->setActiveSheetIndexByName( $this->export_field_set->fs_meta( 'excel_worksheet' ) );
		}

		$this->log( count( $this->parent_skus ) . " Parent SKUs remaining." );

		$this->export();	
	}

	public function export() {

		if ( !$this->jumped && !$this->excel ) {
			// Write headers line to CSV
			$this->write_csv_line_from_array( array_map( array( $this, 'csv_wrap_value' ), $this->export_field_set->get_fields( 'local_name' ) ) );
		}
		
		// Build query and get OVC IDs for export
		global $wpdb;
		
		$qry = "SELECT ID FROM wp_ovc_products WHERE ";

		if( 'parent_skus' == $this->config( 'sku_filter' ) ) {
			$qry .= "parent_sku IN(\"" . implode( "\",\"", $this->parent_skus ) . "\") AND ";
		}
		else if ( 'custom_selection' == $this->config( 'sku_filter' ) ) {
			$qry .= "ID IN(" . implode( ',', get_ovc_user_custom_selection( $this->user_id, 'ovc_products' ) ) . ") AND ";
		}
		else {
			$this->abort_operation( 'Invalid SKU Filtering Configuration' );
		}

		$qry .= "sync_oms = 1 ORDER BY parent_sku ASC";

		$ovc_ids = $wpdb->get_col( $qry );

		$this->log( 'Beginning Export. ' . count( $ovc_ids ) . ' OVC IDs remaining to be exported.' );

		$prfs = OVCSC::get_field_set( 'ovc_products' );

		// $excel_row_index = $this->export_field_set->fs_meta( 'start_row' );
		$excel_row_index = 1;

		// Loop through and export IDs 
		foreach( $ovc_ids as $ovc_id ) {
			$results = new OVC_Results;
			$ovcp = OVCDB()->get_row( $prfs, $ovc_id, $results );
			$csv_row_values = array();
			
			foreach( $this->export_field_set->get_fields() as $field ) {
				$export_value = $ovcp->get_external_value( $field, $results );

				if( false === $export_value ) {
					$results->error( "ERROR! OVC ID: {$ovc_id} - export value is false for field {$field}" );
				}

				if( !$this->excel ) {
					$escape_csv_value = ! (bool) $field->meta( 'csv_no_escape' );

					$export_value = $this->csv_wrap_value( $export_value, $escape_csv_value );
				}

				$csv_row_values[] = $export_value;
				
			}

			foreach( $results->get_msgs() as $msg ) {
				$this->log( $msg );
			}
			// CSV Only
			if( !$this->excel ) {
				$this->write_csv_line_from_array( $csv_row_values );
			}
			// XLS/X Only
			else {

				// This is to add spaces between parent SKUs in the excel worksheet for easy viewing
				if( ( $new_parent_sku = $ovcp->data( 'pr.parent_sku' ) ) != $parent_sku ) {

					$parent_sku = $new_parent_sku;
					$this->excel->getActiveSheet()->fromArray( array(), '', "A{$excel_row_index}" );
					$excel_row_index++;
				}

				// Loop over every field and add it to the document
				$this->excel->getActiveSheet()->fromArray( $csv_row_values, '', "A{$excel_row_index}" );
			}
			$this->exported_ids[] = $ovcp->ID;
			$excel_row_index++; // xlsx only
		}

		if( $this->excel ) { // xlsx only
			
			$product_row_count = intval( $this->export_field_set->fs_meta( 'start_row' ) ) + $excel_row_index;
			// Loop over Amazon External Fields to get the number
			foreach( $this->export_field_set->get_fields() as $field ) {

				$column_string = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex( $field->meta( 'order' ) );
				$format_code = $field->meta( 'excel_format' ) ?: '###0';

				// Get Var Type and set style based on that var type
				$this->excel->getActiveSheet()->getStyle( $column_string . $this->export_field_set->fs_meta( 'start_row' ) . ':' . $column_string . $product_row_count )
					->getNumberFormat()
					->setFormatCode( $format_code );

				$this->excel->getActiveSheet()->getStyle( $column_string . $this->export_field_set->fs_meta( 'start_row' ) . ':' . $column_string . $product_row_count )
					->getAlignment()
					->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT );
			}

			$file_type = \PhpOffice\PhpSpreadsheet\IOFactory::identify( $this->template_file );
			$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter( $this->excel, $file_type );
			//$writer->save( OVCOP_CONTENT_DIR . '/' . $this->csv_file );
			$writer->save( $this->op_file()->path() );
		}

		$this->successful_finish();
	}

	public function before_successful_finish() {
		// Set stat for final report
		$this->init_stat( 'exported_products', '# of Products Exported' );
		$this->stat( 'exported_products', count( $this->exported_ids ) );
	}

	public function after_successful_finish() {
		// Output link to download CSV
		$this->log();
		$this->log( "Download exported CSV here: " . $this->op_file()->url() );
		$this->log();
	}
}	