<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

error_reporting(E_ALL);
ini_set("log_errors", 1); 

class OVCOP_shopify_sync extends OVCOP {
	public $type = 'shopify_sync';

	// Semi-magic properties
	public $since_id;
	public $next_page;

	public $last_parent_id;
	public $last_variant_id;
	public $last_variant_img_id;
	public $shopify_collection_objs;

	public $create_shopify_parent_ids;
	public $update_shopify_parent_ids;
	public $delete_shopify_parent_ids;
	public $update_shopify_variant_ids;
	public $update_shopify_variant_image_ids;
	public $delete_shopify_variant_ids;

	/// Old
	public $skipped_parent_skus;
	public $skipped_parent_ids;

	public static function extra_init( $ovcop ) {

		$ovcop->create_shopify_parent_ids = null;
		$ovcop->update_shopify_parent_ids = null;
		$ovcop->delete_shopify_parent_ids = null;
		$ovcop->update_shopify_variant_ids = null;
		$ovcop->update_shopify_variant_image_ids = null;
		$ovcop->delete_shopify_variant_ids = null;
		
		$ovcop->skipped_parent_skus = array();
		$ovcop->skipped_parent_ids = array();

		$ovcop->since_id = null;
		$ovcop->next_page = false;

		$ovcop->last_parent_id = 0;
		$ovcop->last_variant_id = 0;
		$ovcop->last_variant_img_id = 0;
		$ovcop->shopify_collection_objs = null;

		// Send initialization response 
		$ovcop->results->success = true;
		$ovcop->results->main_msg = "Shopify Sync operation initialized successfully";

		// Tell OVCOP to automatically commence the operation
		return true;
	}

	public function __construct( $request, $args ) {
		$this->child_construct( $request, $args );

		$this->sync();	
	}

	public function sync() {

		// Preload active data errors
		OVC_Row_data_errors::enable();
		OVC_Row_data_errors::load_active_errors();

		$force_sync = false;

		/*
		 * Step 0
		 * OVCOP Options
		 *
		 * Update Shopify Location ID
		 * Back Sync from Shopify
		 *
		 * Force Shopify Sync
		 */
		if( $this->config( 'shopify_back_sync' ) 
			|| !get_option( 'shopify_location_id', false ) 
		) {

			if( !get_option( 'shopify_location_id', false ) ) {

				$this->update_local_shopify_location_id_from_remote();
			}

			if( false !== $this->since_id ) {

				$this->update_local_shopify_data_from_remote();	
			}
		}

		/*
		 * Step 1
		 * Maybe Delete Variants and Parents with Images
		 */

		/*
		 * Maybe Delete Variants
		 */
		if( is_null( $this->delete_shopify_variant_ids ) ) {
			$this->delete_shopify_variant_ids = OVC_Sync_Manager::get_auto_sync_count( 'shopify_delete_variants', true );
		}

		if( $this->delete_shopify_variant_ids ) {

			$this->log();
			$this->log( "Deleting unsynced shopify variants - " . count( $this->delete_shopify_variant_ids ) . " remaining" );

			while( $ovc_shopify_variant_id = array_shift( $this->delete_shopify_variant_ids ) ) {
				$results = new OVC_Results;

				if( $shopify_variant = OVCDB()->get_row( FS( 'shopify_products' ), $ovc_shopify_variant_id, $results ) ) {

					if( $shopify_variant->delete_variant( $results, $force_sync )
						&& $results->success ) {
						$this->log( 'Deleted shopify variant - SKU ' . $shopify_variant->data( 'pr.sku' ) );
					}
					else {
						$this->log( 'Error deleting shopify variant - SKU ' . $shopify_variant->data( 'pr.sku' ) );
						$this->log_results_msgs( $results );
						$this->log();
					}
				} else {
					$this->log( 'Shopify Product ID: ' . $ovc_shopify_variant_id . ' does not exist.' );
				}

				$this->check_user_abort();

				// Check if need to jump operation
				if( 40 < ox_exec_time() ) {

					$this->jump_operation();
				}
			}
		}

		/*
		 * Maybe Delete Parents
		 */
		if( is_null( $this->delete_shopify_parent_ids ) ) {
			$this->delete_shopify_parent_ids = OVC_Sync_Manager::get_auto_sync_count( 'shopify_delete_parents', true );
		}

		if( $this->delete_shopify_parent_ids ) {

			$this->log();
			$this->log( "Deleting unsynced shopify parents - " . count( $this->delete_shopify_parent_ids ) . " remaining" );

			while( $ovc_shopify_parent_id = array_shift( $this->delete_shopify_parent_ids ) ) {
				$results = new OVC_Results;

				if( $shopify_parent = OVCDB()->get_row( FS( 'shopify_parents' ), $ovc_shopify_parent_id, $results ) ) {

					if( $shopify_parent->delete_parent( $results, $force_sync )
						&& $results->success ) {
						$this->log( 'Deleted shopify parent - Parent SKU ' . $shopify_parent->data( 'pa.parent_sku' ) );
					}
					else {
						$this->log( 'Error deleting shopify parent - Parent SKU ' . $shopify_parent->data( 'pa.parent_sku' ) );
						$this->log_results_msgs( $results );
						$this->log();
					}
				} else {
					$this->log( 'Shopify Parent ID: ' . $ovc_shopify_parent_id . ' does not exist.' );
				}

				$this->check_user_abort();

				// Check if need to jump operation
				if( 40 < ox_exec_time() ) {
					$this->jump_operation();
				}
			}
		}

		/*
		 * Maybe Delete Images
		 */
		// if( is_null( $this->delete_shopify_image_ids ) ) {
		// 	$this->delete_shopify_image_ids = OVC_Sync_Manager::get_auto_sync_count( 'shopify_delete_images', true );
		// }

		// if( $this->delete_shopify_image_ids ) {

		// 	$this->log( "Deleting unsynced shopify images - " count( $this->delete_shopify_image_ids ) . " remaining" );

		// 	while( $ovc_shopify_image_id = array_shift( $this->delete_shopify_image_ids ) ) {
		// 		$results = new OVC_Results;

		// 		if( $shopify_image = OVCDB()->get_row( FS( 'shopify_images' ), $ovc_shopify_image_id, $results ) ) {

		// 			$shopify_image->delete_image( $results, $force_sync );
		// 		} else {
		// 			$this->log( 'Shopify Image ID: ' . $ovc_shopify_image_id . ' does not exist.' );
		// 		}

		// 		$this->check_user_abort();

		// 		if( 40 < ox_exec_time() ) {
		// 			$this->jump_operation();
		// 		}
		// 	}
		// }

		/*
		 * Step 2
		 * Create Parents
		 */
		if( is_null( $this->create_shopify_parent_ids ) ) {
			$this->create_shopify_parent_ids = OVC_Sync_Manager::get_auto_sync_count( 'shopify_create_parents', true );
		}

		if( $this->create_shopify_parent_ids ) {

			$this->log();
			$this->log( "Creating new shopify parent products - " . count( $this->create_shopify_parent_ids ) . " remaining" );

			while( $parent_id = array_shift( $this->create_shopify_parent_ids ) ) {

				$results = new OVC_Results;
				
				$ovc_parent = OVCDB()->get_row( FS( 'parent_data' ), $parent_id, $results );

				$shopify_parent = $ovc_parent->get_joined_data( FS( 'shopify_parents' ) );
				
				if( !$shopify_parent->exists() ) {

					$shopify_parent_data = array(
						'sypas.ovc_parent_id'	=> $ovc_parent->data( 'pa.ID' ),
						'pa.parent_sku'			=> $ovc_parent->data( 'pa.parent_sku' )
					);
					$shopify_parent = OVCDB()->new_row( FS( 'shopify_parents' ), $shopify_parent_data );

					if( !$shopify_parent->save( $results ) ) {
						$this->log( 'Error creating OVC Row Shopify Product - Parent SKU ' . $shopify_parent->data( 'pa.parent_sku' ) );
						$this->log_results_msgs( $results );
						continue;
					}
				}

				$shopify_parent->shopify_sync( $results );

				if( $results->success ) {
					$this->log( 'Created shopify product - Parent SKU ' . $shopify_parent->data('pa.parent_sku') );
				}
				else {
					$this->log( 'Error creating shopify product - Parent SKU ' . $shopify_parent->data('pa.parent_sku') );
					$this->log_results_msgs( $results );
				}
				
				$this->check_user_abort();

				//Check if need to jump operation
				if( 40 < ox_exec_time() ) {	
					$this->jump_operation();
				}
			}
		}

		// Set Force Sync here so that it gets set on jumps
		if( 'sync_all' == $this->config( 'sync_mode' ) 
			|| ( 'parent_skus' == $this->config( 'sync_mode' ) && $this->config( 'sync_mode_input' ) )
		) {
			$force_sync = true;
		}

		/*
		 * Step 3
		 * Update Shopify Parents
		 */
		if( is_null( $this->update_shopify_parent_ids ) ) {

			if( 'sync_all' == $this->config( 'sync_mode' ) ) {

				global $wpdb;
				$this->update_shopify_parent_ids = $wpdb->get_col( 
					"
						SELECT sypas.ID
						FROM {$wpdb->prefix}ovc_shopify_parents sypas
						WHERE sypas.ID > {$this->last_parent_id}
						ORDER BY sypas.ID
					"
				);

				$this->log( "Force syncing all shopify products..." );
			}
			elseif( 'parent_skus' == $this->config( 'sync_mode' ) 
				&& $this->config( 'sync_mode_input' )
			) {

				global $wpdb;

				$parent_skus = implode( ',', array_map( function( $value ) {

					return '\'' . $value . '\'';
				}, explode( ',', preg_replace( '/\s+/', '', $this->config( 'sync_mode_input' ) ) ) ) );

				$this->update_shopify_parent_ids = $wpdb->get_col( 
					"
						SELECT sypas.ID 
						FROM {$wpdb->prefix}ovc_shopify_parents sypas 
						WHERE sypas.ovc_parent_id IN ( 
							SELECT pa.ID 
							FROM {$wpdb->prefix}ovc_parent_data pa 
							WHERE pa.parent_sku IN ( {$parent_skus} ) 
						) 
							AND sypas.ID > {$this->last_parent_id}
						ORDER BY sypas.ID
					"
				);

				$this->log( "Syncing input parent SKUs..." );
			}
			else {
 
				$this->update_shopify_parent_ids = OVC_Sync_Manager::get_auto_sync_count( 'shopify_update_parents', true );

				$this->log( "Auto-syncing updated shopify products..." );
			}
		}

		if( $this->update_shopify_parent_ids ) {

			$this->log();
			$this->log( "Updating shopify parent products - " . count( $this->update_shopify_parent_ids ) . " remaining" );	

			while( $shopify_parent_id = array_shift( $this->update_shopify_parent_ids ) ) {

				$results = new OVC_Results;

				if( $shopify_parent = OVCDB()->get_row( FS( 'shopify_parents' ), $shopify_parent_id, $results ) ) {

					if( $shopify_parent->shopify_sync( $results, $force_sync ) 
						&& $results->success ) {
						$this->log( 'Updated shopify parent product - Parent SKU ' . $shopify_parent->data('pa.parent_sku') );
					}
					else {
						$this->log( 'Error updating shopify parent product - Parent SKU ' . $shopify_parent->data('pa.parent_sku') );
						$this->log_results_msgs( $results );
						$this->log();
					}
				} else {
					$this->log( 'Shopify Parent ID: ' . $shopify_parent_id . ' does not exist.' );
				}

				$this->check_user_abort();

				$this->last_parent_id = $shopify_parent_id;

				//Check if need to jump operation
				if( 40 < ox_exec_time() ) {

					$this->jump_operation();
				}
			}
		}

		/*
		 * Step 4
		 * Update Shopify Variants
		 */
		if( is_null( $this->update_shopify_variant_ids ) ) {

			if( 'sync_all' == $this->config( 'sync_mode' ) ) {

				global $wpdb;
				$this->update_shopify_variant_ids = $wpdb->get_col( 
					"
						SELECT ID
						FROM {$wpdb->prefix}ovc_shopify_products
						WHERE ID > {$this->last_variant_id}
						ORDER BY ID
					"
				);

				$this->og( "Force syncing all shopify variants..." );
			}
			elseif( 'parent_skus' == $this->config( 'sync_mode' ) 
				&& $this->config( 'sync_mode_input' )
			) {

				global $wpdb;

				$parent_skus = implode( ',', array_map( function( $value ) {

					return '\'' . $value . '\'';
				}, explode( ',', preg_replace( '/\s+/', '', $this->config( 'sync_mode_input' ) ) ) ) );

				$this->update_shopify_variant_ids = $wpdb->get_col( 
					"
						SELECT sypr.ID 
						FROM {$wpdb->prefix}ovc_shopify_products sypr
						WHERE sypr.ovc_id IN ( 
							SELECT pr.ID 
							FROM {$wpdb->prefix}ovc_products pr 
							WHERE pr.parent_sku IN ( {$parent_skus} ) 
						) 
							AND sypr.ID > {$this->last_variant_id}
						ORDER BY sypr.ID
					"
				);

				$this->log( "Syncing input parent SKU's variants..." );
			}
			else {

				$this->update_shopify_variant_ids = OVC_Sync_Manager::get_auto_sync_count( 'shopify_update_variants', true );
				$this->log( "Auto-syncing updated shopify variants..." );
			}
		}

		if( $this->update_shopify_variant_ids ) {

			$this->log();
			$this->log( "Updating shopify variants - " . count( $this->update_shopify_variant_ids ) . " remaining" );

			while( $ovc_shopify_variant_id = array_shift( $this->update_shopify_variant_ids ) ) {
				$results = new OVC_Results;

				if( $shopify_variant = OVCDB()->get_row( FS( 'shopify_products' ), $ovc_shopify_variant_id, $results ) ) {

					if( $shopify_variant->sync_variant( $results, $force_sync )
						&& $results->success ) {
						$this->log( 'Updated shopify variant - SKU ' . $shopify_variant->data( 'pr.sku' ) );
					}
					else {
						$this->log( 'Error updating shopify variant - SKU ' . $shopify_variant->data( 'pr.sku' ) );
						$this->log_results_msgs( $results );
						$this->log();
					}
				} else {
					$this->log( 'Shopify Product ID: ' . $ovc_shopify_variant_id . ' does not exist.' );
				}

				$this->check_user_abort();

				$this->last_variant_id = $ovc_shopify_variant_id;

				//Check if need to jump operation
				if( 40 < ox_exec_time() ) {

					$this->jump_operation();
				}
			}

		}

		/*
		 * Step 5
		 * Update Shopify Variant Images
		 */
		if( is_null( $this->update_shopify_variant_image_ids ) ) {

			if( 'sync_all' == $this->config( 'sync_mode' ) ) {

				global $wpdb;

				$this->update_shopify_variant_image_ids = $wpdb->get_col( 
					"
						SELECT sypr.ID 
						FROM {$wpdb->prefix}ovc_shopify_products sypr 
						LEFT JOIN {$wpdb->prefix}ovc_products pr 
							ON sypr.ovc_id = pr.ID 
						WHERE pr.sync_shopify = 1 
							AND sypr.variant_id != '' 
							AND sypr.ID > {$this->last_variant_img_id} 
						ORDER BY sypr.ID
					"
				);

				$this->log( "Force syncing all shopify variant image metafields..." );
			}
			elseif( 'parent_skus' == $this->config( 'sync_mode' )
				&& $this->config( 'sync_mode_input' )
			) {

				global $wpdb;

				$parent_skus = implode( ',', array_map( function( $value ) {

					return '\'' . $value . '\'';
				}, explode( ',', preg_replace( '/\s+/', '', $this->config( 'sync_mode_input' ) ) ) ) );

				$this->update_shopify_variant_image_ids = $wpdb->get_col(
					"
						SELECT sypr.ID
						FROM {$wpdb->prefix}ovc_shopify_products sypr
						LEFT JOIN {$wpdb->prefix}ovc_products pr 
							ON sypr.ovc_id = pr.ID 
						WHERE sypr.ovc_id IN (
							SELECT pr.ID
							FROM {$wpdb->prefix}ovc_products pr
							WHERE pr.parent_sku IN ( {$parent_skus} )
						)
							AND pr.sync_shopify = 1
							AND sypr.variant_id != '' 
							AND sypr.ID > {$this->last_variant_img_id}
						ORDER BY sypr.ID
					"
				);

				$this->log( "Syncing input parent SKU's variant image metafields..." );
			}
			else {

				$this->update_shopify_variant_image_ids = OVC_Sync_Manager::get_auto_sync_count( 'shopify_variant_images', true );
				$this->log( "Auto-syncing updated shopify variant image metafields..." );
			}
		}

		if( $this->update_shopify_variant_image_ids ) {

			$this->log();
			$this->log( "Updating shopify variant images - " . count( $this->update_shopify_variant_image_ids ) . " remaining" );

			while( $ovc_shopify_variant_image_id = array_shift( $this->update_shopify_variant_image_ids ) ) {
			// foreach( $ovc_shopify_variant_image_ids as $ovc_shopify_variant_image_id ) {

				$results = new OVC_Results;

				if( $shopify_variant = OVCDB()->get_row( FS( 'shopify_products' ), $ovc_shopify_variant_image_id, $results ) ) {

					$shopify_variant->sync_img_metafield( $results );

					if( $results->success ) {

						$this->log( 'Updated shopify variant image metafields - SKU ' . $shopify_variant->data( 'pr.sku' ) );
					} else {

						$this->log( 'Error updating shopify variant images - SKU ' . $shopify_variant->data( 'pr.sku' ) );
						$this->log_results_msgs( $results );
					}
				} else {
					$this->log( 'Shopify Product ID: ' . $ovc_shopify_variant_image_id . ' does not exist.' );
				}

				$this->check_user_abort();

				$this->last_variant_img_id = $shopify_variant->ID;

				if( 40 < ox_exec_time() ) {

					$this->jump_operation();
				}

			}
		}

		/*
		 * Step 6
		 * Update Shopify collection data cache
		 */
		$this->update_shopify_collections_cache();

		$this->successful_finish();
	}

	public function update_shopify_collections_cache() {

		$this->log( "Initiating Shopify collection data refresh." );

		$ch = curl_init();

		$curl_options = array(
			CURLOPT_URL 			=> 'https://onyxdesign.net/wp-json/collections/v1/refresh?collection=vida',
			CURLOPT_RETURNTRANSFER	=> true,
			CURLOPT_SSL_VERIFYPEER	=> false,
			CURLOPT_SSL_VERIFYHOST	=> false
		);

		curl_setopt_array( $ch, $curl_options );
		
		$op_curl_exec = curl_exec( $ch );

		$op_curl_errno = curl_errno( $ch );
		$op_curl_error = curl_error( $ch );

		curl_close( $ch );

		return true;
	}

	public function update_local_shopify_data_from_remote() {

		$log_msg = "Updating local shopify data from remote...";
		$log_msg .= $this->since_id ? "(Since ID {$this->since_id})" : '';
		$this->log( $log_msg );

		$next_page = 1;
		$last_id = null;

		while( $next_page ) {
			$results = new OVC_Results;

			$request_data = array(
				'query_params' => array(
					'page'		=> $next_page
				)
			);

			if( isset( $this->since_id ) ) {
				$request_data['query_params']['since_id'] = $this->since_id;
			}

			$products_response = OVC_API_Shopify::request( 'get_products', $request_data, $results );

			if( $products_response->succeeded() ) {

				if( $products_response->products ){

					$this->log( "Processing shopify products - page {$next_page}, " . count( $products_response->products ) . " products on this page" );
					$products_processed = 0;

					foreach( $products_response->products as $product_response ) {

						$shopify_parent = OVCDB()->get_row_by_valid_id( FS( 'shopify_parents' ), 'shopify_parent_id', $product_response->id, $results );

						if( !$shopify_parent->exists() ) {

							$this->log( "WARNING! Unknown Shopify Parent product found, Shopify ID {$product_response->id}, Title: {$product_response->title}" );
						} else {
							$shopify_parent->handle_api_response( $product_response, $results );
						}

						$products_processed++;

						if( 0 == $products_processed % 10 ) {
							$this->check_user_abort();
						}

						//Check if need to jump operation
						if( 40 < ox_exec_time() ) {	
							$this->since_id = $product_response->id;
							$this->jump_operation();
						}
					}

					$next_page++;
				}
				else {
					$next_page = false;
					$this->since_id = false;
				}
			}
		}
	}

	/**
	 * Makes sure that the local Shopify Location ID is the same as the remote Shopify Location ID
	 * 
	 * @return bool|null
	 */
	public function update_local_shopify_location_id_from_remote() {
			
		$this->log( "Updating shopify local location." );
			
		$results = new OVC_Results;

		$response = OVC_API_Shopify::request( 'get_locations', array(), $results );

		$response = $response->data();

		if( !is_array( $response ) ) {
			
			$this->log( "There was a problem updating the local shopify location." );
		} else {

			$single_location = $response[0];

			if( $single_location->id == get_option( 'shopify_location_id', false ) ) {

				return true;
			}

			if( !update_option( 'shopify_location_id', $single_location->id ) ) {

				$this->log( "There was a problem updating the local shopify location." );
			}
		}
	}

}