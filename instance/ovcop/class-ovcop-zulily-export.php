<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVCOP_zulily_export extends OVCOP {
	public $type = 'zulily_export';

	// Simple operation-specific properties (re-initialized for each commence_operation request)
	public $export_field_set = array();
	public $excel = false;

	// Semi-magic properties via $op_data['data']
	public $parent_skus;
	public $exported_ids;
	public $template_file;


	// Operation Type Specific Initialization
	public static function extra_init( $ovcop ) {
		global $wpdb;

		// Initialize operation data based on configuration options
		$ovcop->parent_skus = false;
		if( 'parent_skus' == $ovcop->config( 'sku_filter' ) ) {

			$ovcop->parent_skus = array_map( 'sanitize_text_field', array_map( 'trim', explode( ",", $ovcop->config( 'filter_parent_skus' ) ) ) );
		}
		$ovcop->log( "SKU Filtering: {$ovcop->config( 'sku_filter' )}" );

		// Determine the field_set
		if( '2018' == $ovcop->config( 'template_format' ) ) {

			if( in_array( $ovcop->config( 'template_type' ), array( 'ia', 'kids_apparel', 'kids_sleep_undergarments', 'kids_personal_accessories' ) ) ) {

				$ovcop->meta( 'field_set', 'zulily_2018_' . $ovcop->config( 'template_type' ) );
				$excel_template_name = "zulily_2018_{$ovcop->config( 'template_type' )}.xlsm";

			}
			else {

				$ovcop->results->success = false;
				$ovcop->results->main_msg = "Invalid Template Type: {$ovcop->config( 'template_type' )}";
				return false;
			}
		} 
		elseif( 'old' == $ovcop->config( 'template_format' ) ) {
		
			// Validate event_type
			if( in_array( $ovcop->config( 'event_type' ), array( 'intimates', 'sexy' ) ) ) {

				$ovcop->meta( 'field_set', 'zulily_' . $ovcop->config( 'event_type') );
				$excel_template_name = 'zulily-old.xlsx';
			}
			else {

				$ovcop->results->success = false;
				$ovcop->results->main_msg = "Invalid Event Type: {$ovcop->config( 'event_type' )}";
				return false;
			}
		}
		else {

			$ovcop->results->success = false;
			$ovcop->results->main_msg = "Invalid Template Format: {$ovcop->config( 'template_format' )}";
			return false;
		}

		// Initialize Output File
		if( 'xlsx' == $ovcop->config( 'format' ) ) {

			$ovcop->new_file( 'xlsx', 'xlsx', true );
			$ovcop->template_file = OVCOP_TEMPLATE_DIR . '/' . $excel_template_name;

			if( !file_exists( $ovcop->template_file ) ) {

				$ovcop->results->success = false;
				$ovcop->results->main_msg = "Initialization failed. Excel template not found.";
				return false;
			}
		}
		elseif( 'csv' == $ovcop->config( 'format' ) ) {

			$ovcop->new_file( 'csv', 'csv', true );
		}
		else {

			$ovcop->results->success = false;
			$ovcop->results->main_msg = "Initialization failed. Invalid output format type.";
			return false;
		}

		$ovcop->exported_ids = array();

		// Send initialization response 
		$ovcop->results->success = true;
		$ovcop->results->main_msg = "Zulily operation initialized successfully";

		// Tell OVCOP to automatically commence the operation
		return true;
	}


	public function __construct( $request, $args ) {
		$this->child_construct( $request, $args );

		// Initialize Zulily Field Set
		$this->export_field_set = OVCSC::get_field_set( $this->meta( 'field_set' ) );

		// Maybe initialize Zulily Excel Template PHPExcel Object
		if( 'xlsx' == $this->config( 'format' ) ) {

			$this->excel = $this->get_php_excel( $this->template_file );
			$this->excel->setActiveSheetIndexByName( $this->export_field_set->fs_meta( 'excel_worksheet' ) );
		}

		$this->export();
	}

	public function export() {

		if ( !$this->jumped && !$this->excel ) {
			
			// Write headers line to CSV
			$this->write_csv_line_from_array( array_map( array( $this, 'csv_wrap_value' ), $this->export_field_set->get_fields( 'local_name' ) ) );
		}

		// Build query and get OVC IDs for export
		global $wpdb;
		
		$qry = "SELECT ID FROM wp_ovc_products WHERE ";

		if( 'parent_skus' == $this->config( 'sku_filter' ) ) {
			$qry .= "parent_sku IN(\"" . implode( "\",\"", $this->parent_skus ) . "\") AND ";
		}
		else if ( 'custom_selection' == $this->config( 'sku_filter' ) ) {
			$qry .= "ID IN(" . implode( ',', get_ovc_user_custom_selection( $this->user_id, 'ovc_products' ) ) . ") AND ";
		}

		// dev:sync_revamp
		$qry .= "sync_oms = 1 ORDER BY parent_sku ASC";

		$ovc_ids = $wpdb->get_col( $qry );

		$this->log( 'Beginning Export. ' . count( $ovc_ids ) . ' OVC IDs remaining to be exported.' );

		// Set start row
		$excel_row_index = $this->export_field_set->fs_meta( 'start_row' ); // xlsx only

		$prfs = OVCSC::get_field_set( 'ovc_products' );

		foreach( $ovc_ids as $ovc_id ) {
			$results = new OVC_Results;
			$ovcp = OVCDB()->get_row( $prfs, $ovc_id, $results );
			
			$export_row_values = array(); // csv only
			
			foreach( $this->export_field_set->get_fields() as $field ) {
				$export_value = $ovcp->get_external_value( $field, $results );

				if( false === $export_value ) {
					$results->error( "ERROR! OVC ID: {$ovc_id} - zulily_value is false for field {$field}" );
				}
				else if( !$this->excel ) {
					$escape_value = ! (bool) $field->meta( 'csv_no_escape' );

					$export_value = $this->csv_wrap_value( $export_value, $escape_value );
				}
				/*
				else if( isset( $fmeta['cell_data_type'] ) ) { // xlsx only
					$col_index = intval( $fmeta['order'] ) - 1;

					$this->excel->getActiveSheet()->getCellByColumnAndRow( $col_index, $excel_row_index )->setDataType( $fmeta['cell_data_type'] );
				}
				*/

				$export_row_values[] = $export_value;

			}

			foreach( $results->get_msgs() as $msg ) {
				$this->log( $msg );
			}

			if( !$this->excel ) { // csv only
				$this->write_csv_line_from_array( $export_row_values );
			}
			else { // xlsx only
				$this->excel->getActiveSheet()->fromArray( $export_row_values, '', "A{$excel_row_index}" );
			}

			$this->exported_ids[] = $ovcp->ID;
			$excel_row_index++; // xlsx only
		}


		if( $this->excel ) { // xlsx only

			$product_row_count = intval( $this->export_field_set->fs_meta( 'start_row' ) ) + count( $this->exported_ids );
			foreach( $this->export_field_set->get_fields() as $field ) {

				$column_string = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex( $field->meta( 'order' ) );

				// Get Var Type and set style based on that var type
				$this->excel->getActiveSheet()->getStyle( $column_string . $this->export_field_set->fs_meta( 'start_row' ) . ':' . $column_string . $product_row_count )
					->getNumberFormat()
					->setFormatCode( '###0' );

				$this->excel->getActiveSheet()->getStyle( $column_string . $this->export_field_set->fs_meta( 'start_row' ) . ':' . $column_string . $product_row_count )
					->getAlignment()
					->setHorizontal( \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT );
			}

			$file_type = \PhpOffice\PhpSpreadsheet\IOFactory::identify( $this->template_file );
			$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter( $this->excel, $file_type );
			//$writer->save( OVCOP_CONTENT_DIR . '/' . $this->csv_file );
			$writer->save( $this->op_file()->path() );
		}

		$this->successful_finish();
	}

	public function before_successful_finish() {
		// Set stat for final report
		$this->init_stat( 'exported_products', '# of Products Exported' );
		$this->stat( 'exported_products', count( $this->exported_ids ) );
	}

	public function after_successful_finish() {
		// Output link to download CSV
		$this->log();
		//$this->log( "Download exported CSV here: " . ovc_content_url_from_path( OVCOP_CONTENT_DIR . '/' . $this->csv_file ) );
		$this->log( "Download exported file here: " . $this->op_file()->url() );
		$this->log();
	}
}	