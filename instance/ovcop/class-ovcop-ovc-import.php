<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;

ini_set("auto_detect_line_endings", true); // Ensure proper reading of csv files
ini_set("memory_limit", '512M' );

class OVCOP_ovc_import extends OVCOP {

	/**
	 * Operation Key
	 * 
	 * @var string
	 */
	public $type = 'ovc_import';

	// Semi-magic properties via $op_data['data']
	public $field_map;
	public $last_row_checked;
	public $import_by_id;
	public $import_by_sku;

	public static function extra_init( $ovcop ) {

		$ovcop->new_file( 'csv', 'csv', true );

		if( 'upload' == $ovcop->config( 'file_source' ) ) {

			$ovcop->op_file()->write( stripslashes( $ovcop->config( 'import_csv_upload' ) ) );

			// Delete uploaded CSV data from op_data so we don't save huge text files to the database
			$ovcop->config( 'import_csv_upload', null );

			$ovcop->results->main_msg = "OVC Import initialized. Uploading CSV File...";
		} else {

			$ovcop->results->success = false;
			$ovcop->results->main_msg = "Initialization failed. Invalid file source.";
			return false;	
		}

		$ovcop->init_stat( 'total_rows', '# of rows' );
		$ovcop->init_stat( 'error_rows', '# of rows with errors' );
		$ovcop->init_stat( 'not_found_rows', '# of rows not found' );
		$ovcop->init_stat( 'updated_rows', '# of updated products' );

		$ovcop->field_map = array();
		$ovcop->last_row_checked = 1;
		$ovcop->import_by_id = false;
		$ovcop->import_by_sku = false;

		return $ovcop->results->success = true;
	}

	public function __construct( $request, $args ) {

		$this->child_construct( $request, $args );

		$this->run_operation();
	}

	protected function run_operation() {

		if( !$this->jumped ) {

			$this->initialize_csv();
		}

		$this->import_data();

		$this->successful_finish();
	}

	public function initialize_csv() {

		$invalid_headers = array();
		$csv_line_count = 0;
		$csv_max_chars = 0;

		if( !$this->op_file() ) {
			$this->abort_operation( 'Invalid CSV file detected, aborting operation.' );
		}

		// Analyze and validate CSV
		$csv_stream = fopen( $this->op_file()->path(), 'r' );

		while( ( $line = fgets( $csv_stream ) ) !== false ) {

			$csv_line_count++;

			$csv_max_chars = ( ( strlen( $line ) + 2 ) > $csv_max_chars ) ? strlen( $line ) + 2 : $csv_max_chars;
		}
		$this->meta( 'csv_max_chars', $csv_max_chars );

		if( $csv_line_count <= 1 ) {
			fclose( $csv_stream );
			$this->abort_operation( "Invalid row count in CSV. Number of rows detected (including field headings): {$csv_line_count}" );
		} else {
			$this->log( "Number of CSV Rows Detected (Including Column Headers): {$csv_line_count}" );
			$this->log( "Maximum number of characters in any single line: {$csv_max_chars}" );
		}

		// Map and validate CSV Fields to ovc_product fields
		rewind( $csv_stream ); // Set file stream pointer back to beginning of file

		// Validate CSV headers
		$headers = fgetcsv( $csv_stream, $csv_max_chars );
		foreach( $headers as $header_key => $header ) {

			if( $header === 'ID' ) {
				$this->import_by_id = $header_key;
			} elseif( $header === 'sku' ) {
				$this->import_by_sku = $header_key;
			} else {

				// Check that other headers are valid fields
				$header_field = OVCSC::get_field( $header, FS( 'ovc_products' ) );
				if( !$header_field || !ox_maybe_str_to_bool( $header_field->meta( 'allow_csv_import' ) ) ) {
					$invalid_headers[] = $header;
				} else {
					$this->field_map[] = $header_field->local_name;
				}
			}
		}

		fclose( $csv_stream );

		$this->set_stat( 'total_rows', ( $csv_line_count - 1 ) );

		// If we have invalid data, we need to abort
		if( ( ( $this->import_by_id === false ) && ( $this->import_by_sku === false ) ) || ( !empty( $invalid_headers ) ) ) {

			$invalid_msg = "Invalid headers.";

			if( ( $this->import_by_id === false ) && ( $this->import_by_sku === false ) ) {
				$invalid_msg .= " Missing 'sku' or 'ID' column.";
			}

			if( !empty( $invalid_headers ) ) {
				$invalid_msg .= " The following headers are not valid fields: " . implode( ', ', $invalid_headers );
			}

			$this->abort_operation( $invalid_msg );
		}
		else {

			$this->log( 'CSV Initialized successfully' );
			$this->log( 'Columns detected ( ' . count( $headers ) . ' ) - ' . implode( ', ', $this->field_map ) );

		}

		$this->pack_op();
	}

	/**
	 * OVC Import function
	 * 
	 * @return boolean|void
	 */
	public function import_data() {

		$this->log( '' );

		$current_row = 0;

		$csv_stream = fopen( $this->op_file()->path(), 'r' );

		while( $line = fgetcsv( $csv_stream, $this->meta( 'csv_max_chars' ) ) ) {

			$current_row++;

			// If we've already imported the row, or we're looking at headers, skip them
			if( ( $current_row < $this->last_row_checked ) || $current_row === 1 ) {
				continue;
			}

			$results = new OVC_Results();

			if( $this->import_by_id !== false ) {
				
				if( $ovc_product = OVCDB()->get_row( FS( 'ovc_products' ), $line[ $this->import_by_id ], $results ) ) {

					if( ( $this->import_by_sku !== false ) && ( $line[ $this->import_by_sku ] != $ovc_product->data( 'pr.sku' ) ) ) {

						$this->stat( 'error_rows' );
						$this->log( "ERROR: Imported SKU ({$line[ $this->import_by_sku ]}) does not match product SKU ({$ovc_product->data( 'pr.sku' )}). Skipping import" );
						continue;
					}
				} else {

					$this->stat( 'error_rows' );
					$this->log( "ERROR: OVC Product ID {$line[ $this->import_by_id ]} not found. Skipping import" );
					continue;
				}
			} elseif( $this->import_by_sku !== false ) {
				
				$ovc_product = OVCDB()->get_row_by_valid_id( FS( 'ovc_products' ), 'sku', $line[ $this->import_by_sku ], $results );

				if( !$ovc_product ) {
					
					$this->stat( 'error_rows' );
					$this->log( "ERROR: OVC Product SKU {$line[ $this->import_by_sku ]} not found. Skipping import" );
					continue;
				}
				else if( !$ovc_product->exists() ) {

					$this->stat( 'not_found_rows' );
					$this->log( "ERROR: OVC Product SKU {$line[ $this->import_by_sku ]} not found. Skipping import" );
					continue;
				}
			}

			// Remove ID and/or SKU before combining
			$data_to_update = $this->validate_data_to_update( $line );

			if( count( $this->field_map ) !== count( $data_to_update ) ) {
				$this->stat( 'error_rows' );
				$this->log( "ERROR: Field count does not match header count. Skipping import." );
				continue;	
			}

			$update_data = array_combine( $this->field_map, $data_to_update );

			$ovc_product->update_data( $update_data, $results );

			if( $results->has_error ) {

				$this->stat( 'error_rows' );
				$this->log( "ERROR UPDATING OVC ID: {$ovc_product->ID}  SKU: {$ovc_product->sku}" );
				$this->log_results_msgs( $results );
				$this->log();
				$this->log();
			} else {
				$this->stat( 'updated_rows' );
			}
			
			$this->check_user_abort();

			if( 50 < ox_exec_time() ) {

				$this->last_row_checked = $current_row;
				$this->log( "Preparing to jump. Last row checked: {$current_row}" );
				$this->jump_operation();
			}
		}

		$this->log( '' );
		$this->log( 'OVC Import completed successfully.' );

		$this->successful_finish();
	}

	public function validate_data_to_update( $data_to_update ) {

		return array_filter( $data_to_update, function( $key ) {

			if( ( $this->import_by_id !== false ) && $key == $this->import_by_id ) {
				return false;
			}

			if( ( $this->import_by_sku !== false ) && $key == $this->import_by_sku ) {
				return false;
			}

			return true;
		}, ARRAY_FILTER_USE_KEY );
	}
}