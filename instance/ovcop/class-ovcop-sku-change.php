<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;

class OVCOP_sku_change extends OVCOP {

	/**
	 * Operation Key
	 * 
	 * @var string
	 */
	public $type = 'sku_change';

	// Semi-magic properties via $op_data['data']
	public $skus_to_change;
	
	//public $last_sku_changed = false;

	public static function extra_init( $ovcop ) {

		if( 'input_change' == $ovcop->config( 'change_mode' ) ) {
			$ovcop->log( "CHANGE MODE: input_change - Changing all SKUS in list" );

			$sku_pairs = explode( ',', $ovcop->config( 'change_mode_input' ) );

			$old_to_new = array();
			array_walk( $sku_pairs, function( $val ) use ( &$old_to_new ) {
				$values = explode(':',$val);
				$old_to_new[$values[0]] = $values[1];
			} );

			$ovcop->skus_to_change = $old_to_new;
		}

		// Send initialization response 
		$ovcop->results->success = true;
		$ovcop->results->main_msg = "SKU Change operation initialized successfully";

		// Tell OVCOP to automatically commence the operation
		return true;
	}

	public function __construct( $request, $args ) {
		$this->child_construct( $request, $args );

		$this->update();
	}

	/**
	 * SKU Update function
	 * 
	 * @return boolean|void
	 */
	public function update() {

		global $wpdb;

		$skus_updated = 0;

		foreach( $this->skus_to_change as $old_sku => $new_sku ) {

			/*
			if( $this->last_sku_changed && $this->last_sku_changed != $old_sku ) {
				continue;
			}
			/* */

			$results = new OVC_Results;
			$case_skus_updated = 0;

			if( !$this->is_valid( $new_sku, $results ) ) {

				$results->error( "Invalid SKU change for {$old_sku} - {$new_sku}" );

			} else if( $this->update_row_sku( $old_sku, $new_sku, $results ) ) {
				
				$case_skus_updated = $wpdb->update( 'wp_ovc_products', array( 'case_sku' => $new_sku ), array( 'case_sku' => $old_sku ), '%s', '%s' );
				
			}

			if( $results->has_error ) {
				
				$this->log( "SKU UPDATE FAILED - Old SKU {$old_sku}, New SKU {$new_sku}" );
				$this->log_results_msgs( $results );
			}
			else {
				
				$success_msg = "Old SKU {$old_sku} successfully updated to New SKU {$new_sku}";
				$success_msg .= $case_skus_updated ? " (Updated {$case_skus_updated} Case SKUs)" : '';
				$this->log( $success_msg );
			}

			array_shift( $this->skus_to_change );

			$skus_updated++;

			if( 55 < ox_exec_time() ) {
				$this->log( "Preparing to jump SKU Change operation. Last SKU Processed: {$old_sku} - {$new_sku}" );

				//$this->last_sku_changed = $old_sku;

				$this->jump_operation();
				return false;
			}
		}

		$this->log( "SKU Update completed successfully. Number of SKUs updated: {$skus_updated} - Current request elapsed exec time (seconds): " . ox_exec_time() );

		$this->successful_finish();

	}

	/**
	 * Check if $new_sku is valid and unique
	 * 
	 * @param type $new_sku 
	 * @param type &$results 
	 * @return type
	 */
	public function is_valid( $new_sku, &$results ) {

		if( !$this->validate_new_sku_field( $new_sku, $results ) ) {
			return false;
		}

		if( !$this->validate_new_sku_fields( $new_sku, $results ) ) {
			return false;
		}

		return true;
	}

	/**
	 * Validate $new_sku before changing it
	 * 
	 * Checks for validity and uniqueness *
	 * 
	 * @param type $new_sku 
	 * @return type
	 */
	public function validate_new_sku_field( $new_sku, &$results ) {

		// Validate the new sku here and make sure the sku doesn't exist in OMS (sku_confirmed)
		$sku_valid = OVCDB()->validate_value_by_field( OVCSC::get_field( 'pr.sku' ), $new_sku, $results );

		// If the sku isn't valid, or not unique, it's not valid
		if( 
			!$sku_valid 
			|| !$this->is_sku_unique( $new_sku ) 
		) {
			return false;
		}

		return true;

	}

	/**
	 * Explode $new_sku and check each field's validity - specifically the size
	 * 
	 * @param type $new_sku 
	 * @param type &$results 
	 * @return type
	 */
	public function validate_new_sku_fields( $new_sku, &$results ) {

		// Explode the new sku and validate the separate fields - especially the size field as this must be checked against a list of sizes that it CAN be
		$fields = explode( '_', $new_sku );

		$sku_parts = array(
			'pr.sku_style'	=> $fields[0],
			'pr.sku_color'	=> $fields[1],
			'pr.sku_pkqty'	=> $fields[2],
			'pr.size'		=> $fields[3]
		);

		foreach ($sku_parts as $field => $value) {
			
			if( !OVCDB()->validate_value_by_field( OVCSC::get_field( $field ), $value, $results ) ) {
				return false;
			}
		}

		return true;

	}

	/**
	 * Check if $new_sku is unique within pr.sku and pr.oms_sku_confirmed
	 * 
	 * @param type $new_sku 
	 * @return boolean
	 */
	public function is_sku_unique( $new_sku ) {

		// Make sure it's unique in OVC
		if( !OVCDB()->is_value_unique( 'pr.sku' ) ) {
			return false;
		// Make sure the new sku doesn't already exist in OMS
		} else if( !OVCDB()->is_value_unique( 'pr.oms_sku_confirmed' ) ) {
			return false;
		}

		return true;

	}

	/**
	 * Check to see that the row exists, then update the SKU and the SKU's associated fields
	 * 
	 * @param type $old_sku 
	 * @param type $new_sku 
	 * @param type &$results 
	 * @return type
	 */
	protected function update_row_sku( $old_sku, $new_sku, &$results ) {

		// Update the fields that make up the sku
		$fields = explode( '_', $new_sku );

		$updated_data = array(
			'sku_style'	=> $fields[0],
			'sku_color'	=> $fields[1],
			'sku_pkqty'	=> $fields[2],
			'size'		=> $fields[3],
			'sku'		=> $new_sku
		);

		// Retrieve Row Resource
		$ovc_row = OVCDB()->get_row_by_valid_id( 'ovc_products', 'sku', $old_sku, $results );

		if( !$ovc_row->exists() ) {
			return $results->error( "No OVC Product found for SKU {$old_sku}" );
		}
		else {
			return $ovc_row->update_data( $updated_data, $results );
		}

	}

}