<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

error_reporting(E_ALL); 
ini_set("log_errors", 1); 
ini_set("memory_limit", '256M' );
ini_set("auto_detect_line_endings", true);

class OVCOP_oms_import extends OVCOP {
	public $type = 'oms_import';
	public $op_step = 3;

	// Simple operation-specific properties (re-initialized for each commence_operation request)
	public $oms_fm = array();
	public $pr_fs  = array();
	public $row;

	// Semi-magic properties via $op_data['data']
	//public $ovc_ids = array();
	public $skip_past_line = 0;


	// DEV TEMP
	public $oms_keys = array();


	public static function extra_init( $ovcop ) {
		global $wpdb;

		/*/ Update csv_file name in OVCOP table
		$ovcop->csv_file = $ovcop->generate_file_name();
		$wpdb->update( 'wp_ovcop_log', array( 'csv_file' => $ovcop->csv_file ), array( 'ID' => $ovcop->ID ) );
		*/

		// Initialize CSV
		$ovcop->new_file( 'csv', 'csv', true );

		// Set the op_step back to 2 because we'll need to stay on the second step on the OVCOP frontend
		//$ovcop->op_step = 2;


		// INITIALIZE CONFIGURATION VARIABLES
		// Determine Import CSV Source (VIDA FTP or Manual Upload)
		if( 'ftp' == $ovcop->config('file_source') ) {
			$ovcop->meta( 'vida_ftp_csv_path', "O:\\ovc\\{$ovcop->ftp_op_dir()}\\{$ovcop->op_file()->name()}" );

			/*
			if( !self::get_csv_ftp() ) {
				$ovcop->abort_operation( "Failed to retrieve CSV via FTP." );
				return false;
			}
			*/
			$ovcop->results->main_msg = "OMS Import initialized. Confirm FTP CSV.";

			//$ovcop->response_data['html'] = $ovcop->load_template( $ovcop->type . '-ftp_upload' );
		}
		else if ( 'db' == $ovcop->config('file_source') ) {
			$ovcop->results->main_msg = "OMS Import initialized. Reading existing database information...";
		}
		else if ( 'upload' == $ovcop->config('file_source') ) {
			//$ovcop->response_data['do_file_upload'] = true;

			//$ovcop->response_data['csv_upload_name'] = $ovcop->op_file()->name();

			$ovcop->op_file()->write( stripslashes( $ovcop->config( 'import_csv_upload' ) ) );

			// Delete uploaded CSV data from op_data so we don't save huge text files to the database
			$ovcop->config( 'import_csv_upload', null ); //dev:improve - maybe adjust OVCOP so we don't ever have to save this to the DB in the first place?


			$ovcop->results->main_msg = "OMS Import initialized. Uploading CSV File...";
		}

		// Log import_mode
		$ovcop->log( "IMPORT MODE: " . $ovcop->config( 'import_mode' ) );


		
		


		// INITIALIZE SEMI-MAGIC DATA VARIABLES
		$ovcop->skip_past_line = 0;
		//$ovcop->ovc_ids = array();


		/*/ INITIALIZE STATS VARIABLES
		$ovcop->init_stat( 'valid', 'Valid Rows' );
		$ovcop->init_stat( 'invalid', 'Invalid Rows' );
		$ovcop->init_stat( 'ignored', 'Ignored Rows' );
		$ovcop->init_stat( 'warning', 'Total Warnings' );
		$ovcop->init_stat( 'updated_stock', 'Stock Updates' );
		*/

		// Send initialization response
		//$ovcop->response_data['log_once'] = true;
		return $ovcop->results->success = true;
		
		// Do not auto-commence the operation
		//return false;
	}

	public function __construct( $request, $args ) {
		$this->child_construct( $request, $args );

		// BUILD AND VALIDATE OMS / OVC FIELD MAP (built before $request switch because it's needed in init_source_file as well as commence_operation)
		$this->oms_fm = OVCSC::get_field_set( '_oms_field' );
		$this->pr_fs = OVCSC::get_field_set( 'ovc_products' );

		$this->run_operation();

		/*
		switch ( $request ) {
			case 'init_source_file':
				$source_file_success = false;
				if( 'ftp' == $this->config('file_source') ) {
					$source_file_success = $this->get_csv_ftp();
				}
				else if ( 'upload' == $this->config('file_source') ) {
					$source_file_success = $this->get_csv_upload();
				}

				$this->results->success = $source_file_success;

				if( !$source_file_success ) {
					// Operation has already been aborted
					$this->response_data['html'] = $this->load_template( $this->type . '-complete' );
				}
				else if ( $this->initialize_csv_file() ) {
					$this->op_step = 3;
					$this->response_data['html'] = $this->load_template_generalized( 'progress' );
					$this->pack_op();
					register_shutdown_function( array( $this, 'commence_operation' ) );
					
					$this->log( "CSV Initialized successfully. Commencing OMS Import." );
					$this->results->main_msg = "CSV Initialized successfully. Commencing OMS Import.";
				}

				$this->ajax_response();
			break;
			case 'commence_operation':

				$this->run_operation();
				


				/*
				if( $this->import() ) { // First time running the operation
					$this->successful_finish();
				}* /
			break;
		}
		*/
	}

	protected function run_operation() {

		// Skip CSV -> DB import and high-level verification if we have already done it (if we have jumped)
		if( !$this->jumped ) {

			if( 'ftp' == $this->config('file_source') && !$this->get_csv_ftp() ) {
				$this->abort_operation( "Failed to retrieve CSV via FTP." );
				return false;
			}

			if( 'db' == $this->config('file_source') ) {

				$this->log( "Skipping CSV import. Reading existing OMS import data from database." );
			}
			else {

				$this->initialize_csv_file();

				$this->import_csv_db();	
			}
			
			$this->verify_csv_data();

		}

		$this->verify_csv_rows();
	}

	



	// INITIALIZATION METHODS
	protected static function get_csv_ftp( $ovcop ) {
		$source_file_success = false;

		$ovcop->log();
		$ovcop->log( "Attempting to transfer OMS Import CSV from VIDA FTP...", false );
		
		$ovcftp = new OVC_OMS_FTP;
		
		if( !$ovcftp->is_connected() ) {
			return $ovcop->abort_operation( "FTP File Transfer failed: Unable to establish FTP connection.", false );
		}
		else {
			$ftp_op_dir = $ovcop->ftp_op_dir();

			$ovcop->log( "FTP: Switching FTP directory to {$ftp_op_dir}", false );

			// Change to op directory
			if( !$ovcftp->chdir( $ftp_op_dir ) ) {
				return $ovcop->abort_operation( "Failed to change FTP directory. FTP Download cancelled.", false );
			}
			else {
				$dir_list = $ovcftp->list_dir();

				if( !$dir_list || !array_key_exists( $ovcop->op_file()->name(), $dir_list ) ) {
					return $ovcop->abort_operation( "Failed to verify remote OMS Import CSV.  It may or may not be in the proper directory on the VIDA server.", false );
				}
				else if ( !$ovcftp->get( $ovcop->op_file()->name(), $ovcop->op_file()->path() ) ) {
					return $ovcop->abort_operation( "Failed to download OMS Import CSV from VIDA local server.", false );
				}
				else {
					$ovcop->log( "OMS Import CSV successfully downloaded from VIDA local server." );
					$ovcop->log();
					$source_file_success = true;
				}
			}
		}

		$ovcftp->close_connection();
		return $source_file_success;
	}

	/*
	protected function get_csv_upload() {
		$this->log();
		$this->log( "Uploading CSV file..." );

		if( !move_uploaded_file( $_FILES['uploaded_file']['tmp_name'], $this->op_file()->path() ) ) {
			return $this->abort_operation( "Failed to save uploaded CSV File.", false );
		}
		else {
			$this->log( "CSV Uploaded. Commencing OMS Import." );
			return true;
		}
	}
	*/

	protected function initialize_csv_file() {
		$csv_is_valid = true;
		$csv_line_count = 0;
		$csv_max_chars = 0;
		
		// ANALYZE AND VALIDATE CSV
		$csv_stream = fopen( $this->op_file()->path(), 'r' );

		// COUNT # Lines in the CSV (Actual number of lines; Includes First row of column headers), also count max chars in any line
		while( ($line = fgets( $csv_stream )) !== false ) {
			$csv_line_count++;
			$csv_max_chars = ( (strlen($line) + 2) > $csv_max_chars ) ? strlen($line) + 2 : $csv_max_chars;
		}
		$this->meta( 'csv_max_chars', $csv_max_chars );

		if( $csv_line_count <= 1 ) {
			$this->abort_operation( "Invalid row count in CSV. Number of rows detected (including field headings): {$csv_line_count}", false );
			$csv_is_valid = false;
		} else {
			$this->log( "Number of CSV Rows Detected (Including Column Headers): {$csv_line_count}" );
			$this->log( "Maximum number of characters in any single line: {$csv_max_chars}" );
		}

		// MAP and Validate CSV Fields to OMS Import Field Map
		rewind( $csv_stream ); // Set file stream pointer back to beginning of file
		$csv_fields = fgetcsv( $csv_stream, $csv_max_chars ); // Read first line of CSV as an array

		if( $csv_fields !== $this->oms_fm->get_fields( 'local_name' ) ) {
			$this->abort_operation( "Invalid CSV headers detected in import CSV.", false );
			$csv_is_valid = false;
		}

		fclose($csv_stream);
		return $csv_is_valid;
	}

	protected function import_csv_db() {

		$this->check_user_abort( true );

		$this->log( "Importing CSV data into database table..." );

		// init
		$this->oms_keys = $this->oms_fm->get_single_meta_key_values( 'ovc_oms_db_field' );

		global $wpdb;

		// Empty & reset the OMS CSV data table
		$wpdb->query( "TRUNCATE TABLE wp_ovc_oms_import" );
		$wpdb->query( "ALTER TABLE wp_ovc_oms_import AUTO_INCREMENT = 2" );


		// fix stupid windows/OMS double quote escaping
		//file_put_contents( $this->op_file()->path(), str_replace('\\', '\\\\', file_get_contents( $this->op_file()->path() ) ) );
		file_put_contents( $this->op_file()->path(), preg_replace('/\"{2}(?!\")/', '\\"', file_get_contents( $this->op_file()->path() ) ) );

		$csv_stream = fopen( $this->op_file()->path(), 'r' );

		fgets( $csv_stream, $this->meta('csv_max_chars') ); // Skip first line

		$line_index = 1;
		$chunk_size = 10;
		$chunk_data = array();

		// Import CSV Data into database in chunks
		while( ($row = fgetcsv( $csv_stream, $this->meta('csv_max_chars') ) ) !== FALSE ) {

			$line_index++;

			if( count( $row ) > 1 ) {
				$chunk_data[] = $row;
			}
			else {
				continue;
			}

			// Write to database in chunks of $chunk_size # of lines
			if( 0 == count( $chunk_data ) % $chunk_size ) {

				$this->check_user_abort();

				$this->write_csv_chunk_db( $chunk_data, $line_index );
			}
		}

		// Write final chunk into database
		$this->write_csv_chunk_db( $chunk_data, $line_index );

		fclose( $csv_stream );

		// Parse OVC Tracking Data (Processed in chunks)
		$offset = 0;
		$chunk_size = 500;

		while( $ovc_tracking = $wpdb->get_results( "SELECT ID, sku, whs, notes_line6, notes_line7 FROM wp_ovc_oms_import WHERE notes_line7 <> '' LIMIT {$offset},{$chunk_size}", ARRAY_A ) ) {

			foreach( $ovc_tracking as $row ) {

				$this->check_user_abort();

				$sanitized_data = false;

				// Test if using new OVC Tracking format
				if( is_numeric( $row['notes_line6'] ) && is_numeric( $row['notes_line7'] ) ) {

					$notes_line6 = intval( $row['notes_line6'] );
					$notes_line7 = intval( $row['notes_line7'] );

					if( $notes_line6 == $row['notes_line6']
						&& $notes_line7 == $row['notes_line7']
					) {

						$sanitized_data = array(
							'ovc_id'			=> $notes_line6,
							'last_oms_export'	=> $notes_line7
						);
					}
				}
				else {

					// DEV DEV DEV
					// DEV: PHASE THIS OUT AFTER DATA IS MORE CLEAN AND WE CAN BE MORE STRICT ABOUT OVC TRACKING DATA RULES!!! 

					$this->log( "WARNING! Invalid tracking data at row {$row['ID']}, sku {$row['sku']}, tracking data: notes_line6: {$row['notes_line6']}, notes_line7: {$row['notes_line7']}" );


					$ovc_data = maybe_unserialize( str_replace( "'", '"', trim( $row['notes_line7'], '"' ) ) );

					if( is_array( $ovc_data ) && array_key_exists( 'ovc_id', $ovc_data ) && array_key_exists( 'ovc_oms_export', $ovc_data ) ) {

						$sanitized_data = array(
							'ovc_id'			=> $ovc_data['ovc_id'],
							'last_oms_export'	=> $ovc_data['ovc_oms_export'],
							'import_status' 	=> 'invalid_ovc_data'
						);
					}
					else {
						$this->log( "Invalid tracking data at row {$row['ID']}, sku {$row['sku']}, tracking data: {$row['notes_line7']} - Attempting to match by SKU" );

						$ovc_product = OVCDB()->get_row( $this->pr_fs, $row['sku'] );

						if( !$ovc_product->exists() ) {
							$this->log( "There is no OVC product with SKU {$row['sku']} - Please manually check OMS and OVC Data" );
						}
						else {
							$sanitized_data = array(
								'ovc_id'			=> $ovc_product->ID,
								'import_status' 	=> 'invalid_ovc_data'
							);
						}
					}
				}

				if( is_array( $sanitized_data ) && !$wpdb->update( 'wp_ovc_oms_import', $sanitized_data, array( 'ID' => $row['ID'] ) ) ) {
					$this->log( "Unable to process and update tracking data at row {$row['ID']}, sku {$row['sku']}, tracking data: notes_line6: {$row['notes_line6']}, notes_line7: {$row['notes_line7']}" );
				}

			}

			$offset += $chunk_size;
		}

		$this->log( "CSV data imported into database table successfully. line_index: {$line_index} - Current request elapsed exec time (seconds): " . ox_exec_time() );
	}

	private function verify_csv_data() {

		$this->check_user_abort();

		global $wpdb;

		$this->log();
		$this->log();
		$this->log();
		$this->log( "Beginning high-level data verification checks." );

		// Check #1/4 : Check for duplicate OVC ID / WHS combinations

		$this->log();
		$this->log( "Check 1/4: Duplicate OVC ID / WHS combinations" );

		$results = $wpdb->get_results( "SELECT ID, ovc_id, whs FROM `wp_ovc_oms_import` WHERE ovc_id <> 0 GROUP BY ovc_id, whs HAVING COUNT( ovc_id ) > 1", ARRAY_A );

		if( !$results ) {

			$this->log( "Check 1/4 passed successfully. No duplicate OVC ID / WHS combinations detected." );
		}
		else {

			$this->log( "Check 1/4 FAILED! There are " . count( $results ) . " duplicate OVC ID / WHS combinations!" );

			foreach( $results as $row ) {

				$this->log( "Duplicate OVC ID: {$row['ovc_id']} in Warehouse: {$row['whs']}" );

				// Figure out which SKU is in OVC and which isn't
				$oms_skus = $wpdb->get_col( "SELECT sku FROM `wp_ovc_oms_import` WHERE ovc_id = {$row['ovc_id']} AND whs = '{$row['whs']}'", ARRAY_A );

				// Get the 
				$ovc_product = OVCDB()->get_row( $this->pr_fs, $row['ovc_id'] );

				if( $ovc_product->exists() && $ovc_product->sync_oms ) {

					$this->log( "One of the duplicate OMS SKUs with OVC ID of {$row['ovc_id']} matches the OVC SKU: {$ovc_product->sku}" );

					//$wpdb->query( "UPDATE wp_ovc_oms_import SET import_status = 'invalid_sku' WHERE ovc_id = {$row['ovc_id']} AND sku != '{$ovc_product->sku}" );
				}
				else {

					$this->log( "None of the duplicate OMS SKU / OVC ID combinations match correctly with OVC!" );
				}

				$this->log( "Incorrect OMS SKUs for OVC ID {$row['ovc_id']} in Warehouse {$row['whs']}: " . implode( ', ', array_diff( $oms_skus, array( $ovc_product->sku ) ) ) );
			}
		}


		// Check #2/4 : Check for mismatched OVC ID / SKU combinations

		$this->log();
		$this->log( "Check 2/4: Mismatched OVC ID / SKUs in OMS" );

		// Update OMS SKU CONFIRMED
		$wpdb->query(
			"UPDATE wp_ovc_products pr
			 LEFT JOIN wp_ovc_oms_import oms
			 ON pr.ID = oms.ovc_id AND pr.oms_sku_confirmed = '' AND oms.sku = pr.sku
			 SET pr.oms_sku_confirmed = oms.sku 
			 WHERE pr.oms_sku_confirmed = ''"
		);

		// UPDATE OMS IMPORT DATA WITH OVC ID MISMATCHES
		$mismatched_rows = $wpdb->query(
			"UPDATE wp_ovc_oms_import oms
			 LEFT JOIN wp_ovc_products pr
			 ON pr.ID = oms.ovc_id
			 SET oms.import_status = 'ovc_id_mismatch'
			 WHERE oms.ovc_id > 0
			 AND pr.oms_sku_confirmed != oms.sku"
		);

		if( 0 === $mismatched_rows ) {

			$this->log( "Check 2/4 passed successfully. No mismatched OVC ID / SKU combinations detected." );
		}
		else {

			$this->log( "Check 2/4 FAILED! Mismatched OVC ID / SKU combinations detected:" );

			$mismatched_data = $wpdb->get_results( 
				"SELECT pr.ID,
				 oms.ovc_id,
				 oms.whs,
				 pr.SKU AS 'OVC SKU',
				 pr.oms_sku_confirmed AS 'OVC OMS SKU (Confirmed)',
				 oms.sku AS 'OMS SKU',
				 pr.sync_oms,
				 pr.ovc_status
				 FROM
				 wp_ovc_oms_import oms
				 LEFT JOIN wp_ovc_products pr
				 ON pr.ID = oms.ovc_id
				 WHERE oms.import_status = 'ovc_id_mismatch'",
				 ARRAY_A
			);

			$log_table = "\r\n" . str_pad( "OVC ID", 10 ) . str_pad( "OVC ID (OMS)", 14 ) . str_pad( "OMS WHS", 9 ) . str_pad( "OVC SKU", 24 ) . str_pad( "OVC OMS SKU (Confirmed)", 24 ) . str_pad( "OMS SKU", 24 ) . str_pad( "OVC sync_oms?", 15 ) . str_pad( "OVC Status", 16 ) . "\r\n";

			foreach( $mismatched_data as $row ) {

				$log_table .= str_pad( $row['ID'], 10 ) . str_pad( $row['ovc_id'], 14 ) . str_pad( $row['whs'], 9 ) . str_pad( $row['OVC SKU'], 24 ) . str_pad( $row['OVC OMS SKU (Confirmed)'], 24 ) . str_pad( $row['OMS SKU'], 24 ) . str_pad( $row['sync_oms'], 15 ) . str_pad( $row['ovc_status'], 16 ) . "\r\n";
			}

			$log_table .="\r\n\r\n";

			$this->log( $log_table );
		}
		

		// Check #3/4 : Expose OVC SKUs that are not in the OMS Data (but should be), ignoring SKUs already flagged as ovc_id_mismatch

		$this->log();
		$this->log( "Check 3/4: OVC SKUs that should be in OMS that are not (excluding mismatched OVC ID / OMS SKU rows)" );

		// Query via a view ('wp_ovc_oms_import_missing_ovc_skus')
		$wpdb->query(
			"CREATE OR REPLACE VIEW wp_ovc_oms_import_missing_ovc_skus AS
			 SELECT 
			  pr.ID,
			  pr.sku,
			  pr.oms_sku_confirmed,
			  pr.op_oms_export_unconfirmed,
			  oms.ovc_id
			  FROM wp_ovc_products pr
			  LEFT JOIN wp_ovc_oms_import oms
			  ON pr.ID = oms.ovc_id AND oms.import_status != 'ovc_id_mismatch'
			  WHERE
			  pr.op_oms_export_unconfirmed != 0
			  AND oms.ID IS NULL"
		);

		$missing_ovc_skus = $wpdb->get_results( "SELECT ID, sku FROM wp_ovc_oms_import_missing_ovc_skus", ARRAY_A );

		if( !$missing_ovc_skus ) {

			$this->log( "Check 3/4 passed successfully. All OVC SKUs previously exported to OMS were found in the OMS data." );
		}
		else {

			$this->log( "Check 3/4 FAILED! There are " . count( $missing_ovc_skus ) . " previously exported OVC SKUs missing from OMS!" );

			/*

			$log_table = "\r\n" . str_pad( "OVC ID", 10 ) . str_pad( "OVC SKU", 24 );

			foreach( $missing_ovc_skus as $row ) {

				$log_table .= str_pad( $row['ID'], 10 ) . str_pad( $row['sku'], 24 ) . "\r\n";
			}

			$log_table .="\r\n\r\n";

			$this->log( $log_table );

			*/
		}


		// Check #4/4 : Expose OMS SKUs that are not in OVC that have 

		$this->log();
		$this->log( "Check 4/4: OMS SKUs that are not in OVC with remaining stock and/or unexpected OMS status" );

		$wpdb->query(
			"CREATE OR REPLACE VIEW wp_ovc_oms_import_incorrect_oms_skus AS
			 SELECT 
			 oms.ID AS 'OMS CSV Row',
			 oms.sku AS 'OMS SKU',
			 oms.ovc_id AS 'OMS OVC ID',
			 pr.sku AS 'OVC SKU',
			 oms.whs,
			 oms.status,
			 oms.in_stock,
			 oms.notes_line7
			 FROM wp_ovc_oms_import oms
			 LEFT JOIN wp_ovc_products pr
			 ON pr.oms_sku_confirmed = oms.sku
			 WHERE 
			 pr.sku IS NULL
			 AND 
			  ( NOT ( oms.status IN('Discontinu','Inactive') AND oms.in_stock = 0 AND oms.notes_line7 = '' )
			    OR
			    ( oms.status = 'Active' AND oms.ovc_id > 0 )
			  )"
		);

		$incorrect_oms_skus = $wpdb->get_var( "SELECT COUNT(*) FROM wp_ovc_oms_import_incorrect_oms_skus" );

		if( !$incorrect_oms_skus ) {

			$this->log( "Check 4/4 passed successfully. All OMS SKUs are in OVC or configured with proper OMS Status and stock amounts." );
		}
		else {

			$this->log( "Check 4/4 FAILED! There are {$incorrect_oms_skus} with improperly configured OMS Statuses and/or stock amounts" );
		}

		$this->log();
		$this->log( "High-level data-verification checks complete. Current request elapsed exec time (seconds): " . ox_exec_time() );

		return true;
	}

	protected function verify_csv_rows() {

		global $wpdb;

		if( !$this->jumped ) {

			$this->log();
			$this->log();
			$this->log( "Beginning individidual row data checks." );
			$this->log();
		}

		$offset = 0;
		$chunk_size = 500;
		$this->use_log_prefix = true;

		while ( $data = $wpdb->get_results( 
				"SELECT * FROM wp_ovc_oms_import
				 WHERE ID > {$this->skip_past_line}
				 AND ovc_id > 0
				 AND import_status != 'ovc_id_mismatch'
				 AND import_status != 'invalid_ovc_data'
				 AND whs = '01' 
				 ORDER BY ID ASC
				 LIMIT {$offset},{$chunk_size}",
				 ARRAY_A 
			 ) 
		) {

			foreach( $data as $row ) {

				// Initialize row, continue if: OVC ID is valid, OVC Product exists, sync_oms is 1 and last_oms_export != 0
				if( $this->init_row( $row ) ) {


					$ovc_import_data = array();

					$skip_ovc_update = false;

					// Check OVC OP Tracking ( Last OMS Export ID )
					if( $row['last_oms_export'] == $this->row->product->data( 'pr.op_oms_export_unconfirmed' ) ) {

						$ovc_import_data[ 'pr.op_oms_export_confirmed' ] = $row['last_oms_export'];
						//$ovc_import_data[ 'pr.oms_sku_confirmed' ] = $row['sku'];
					}
					else {

						$this->row->data_mismatch = 1;

						if( 'force' != $this->config( 'import_mode' ) ) {
							$skip_ovc_update = true;
							$this->row->import_status = 'ovc_tracking_error';
						}

						//$this->log( "WARNING! - Last OMS Export: {$row['last_oms_export']} does not match unconfirmed OMS Export: {$this->row->product->data( 'pr.op_oms_export_unconfirmed' )}" );
					}
					

					foreach( $this->oms_fm->get_fields( 'object' ) as $oms_field ) {

						if( !$oms_field->meta( 'skip_back_check' ) && !$oms_field->meta( 'import_oms_value' ) ) {

							$ovc_oms_value = trim( str_replace( '"', "'", stripslashes( $this->row->product->get_external_value( $oms_field, $this->row->results ) ) ) );

							$oms_csv_value = trim( $row[ $oms_field->meta( 'ovc_oms_db_field' ) ] );

							if( $ovc_oms_value != $oms_csv_value ) {

								$this->row->data_mismatch = 1;

								$this->row->mismatch_fields[ $oms_field->local_name ] = array(
									'ovc_oms_value'	=> $ovc_oms_value,
									'oms_csv_value'	=> $oms_csv_value
								);

							}
						}
						else if( $oms_field->meta( 'import_oms_value' ) ) {

							// Special handling for importing oms_sku_confirmed
							$field_name = 'Item#' == $oms_field->local_name ? 'pr.oms_sku_confirmed' : $oms_field->meta( 'ovc_field' );

							$ovc_import_data[ $field_name ] = $row[ $oms_field->meta( 'ovc_oms_db_field' ) ];
						}
					}

					$this->check_user_abort();

					if( ( 'standard' == $this->config( 'import_mode' ) && !$skip_ovc_update ) || 'force' == $this->config( 'import_mode' ) ) {

						$ovc_import_data[ 'pr.avail_qty' ] = $ovc_import_data[ 'pr.stock' ] - $ovc_import_data['pr.so_qty'];

						$ovc_import_data[ 'pr.op_oms_import'] = $this->ID;

						$ovc_import_data[ 'pr.oms_sku_confirmed' ] = $row['sku'];

						// Force update the stock (ensures that _stock_updated is updated even if the stock numbers are the same)
						$ovc_import_data[ 'pr._stock_updated' ] = current_time( 'mysql', 1 );;
						


						if( !$this->row->product->update_data( $ovc_import_data, $this->row->results ) ) {

							$this->row->import_status = 'ovc_update_failed';
						}
						else if( $this->config( 'update_wc_stock' ) ) {

							$this->row->product->ovc_set_wc_stock( $this->row->results );
						}
					}					
				}

				if( $this->row->mismatch_fields && $this->config( 'log_mismatched_fields' ) ) {

					$log_table = "\r\n\r\n" . str_pad( "OMS Field", 20 ) . str_pad( "OVC->OMS Value", 40 ) . str_pad( 'OMS CSV Value', 40 );

					foreach( $this->row->mismatch_fields as $mismatch_oms_field => $values ) {

						$log_table .= "\r\n" . str_pad( $mismatch_oms_field, 20 ) . str_pad( $values['ovc_oms_value'], 40 ) . str_pad( $values['oms_csv_value'], 40 );

					}

					$log_table .= "\r\n";
					$this->log( "Mismatched Fields: " . $log_table );
				}

				if( $this->row->results->has_error ) {

					foreach( $this->row->results->get_msgs( 'error' ) as $msg ) {
						$this->log( $msg );
					}
				}

				// UPDATE wp_ovc_oms_import table with row->import_status and row->data_mismatch boolean
				$wpdb->update( 'wp_ovc_oms_import', array( 'import_status' => $this->row->import_status, 'data_mismatch' => $this->row->data_mismatch ), array( 'ID' => $row['ID'] ), array( '%s', '%d' ), '%d' );

				//Check if need to jump operation
				if( 0 == $this->row->csv_line % 40 && 55 < ox_exec_time() ) {
					$this->log( "Preparing to Jump OMS -> OVC Import operation. Last CSV Line Processed: {$row['ID']}", false );

					$this->skip_past_line = $row['ID'];
					
					$this->jump_operation();
					return false;
				}
			}

			// Just checkin/notify in the log (Prefix with line number is included)
			$this->log( "CHECKPOINT - CSV Line {$row['ID']} Processed. Continuing OMS Import." );

			// Advance the offset to the next chunk
			$offset += $chunk_size;
		}




		$this->successful_finish();
		//return true;
	}

	private function write_csv_chunk_db( &$chunk_data, $line_index ) {

		if( !is_array( $chunk_data ) || !count( $chunk_data ) ) {
			return 0;
		}

		global $wpdb;

		$sql = "INSERT INTO wp_ovc_oms_import (" . implode( ',', $this->oms_keys ) . ") VALUES";

		foreach( $chunk_data as $row ) {
			$sql .= '("' . implode( '","', $row ) . '"),';
		}

		$sql = rtrim( $sql, ',' );
		$sql .= ';';

		

		$result = $wpdb->query( $sql );


		$chunk_data = array();

		if( false === $result ) {
			$this->abort_operation( "CSV to DB import failed at CSV line {$line_index}\r\nQuery that failed:\r\n{$sql}" );


		}
		else {
			return $result;
		}


	}

	// Row iteration object initialization
	private function init_row( $row_data = array() ) {
		// Initialize Row Object if needed
		if( !( $this->row instanceof stdClass ) ) {
			$this->row 				= new stdClass();
			$this->row->is_valid	= true;
			$this->row->line 		= 0;
		}

		// Reset the Row object
		$this->row->data 			= $row_data;
		$this->row->csv_line 		= $row_data['ID'];
		$this->row->ovc_id 			= $row_data['ovc_id'];
		$this->row->import_status 	= 'success';
		$this->row->data_mismatch 	= 0;
		$this->row->mismatch_fields = array();
		$this->row->results 		= new OVC_Results;
		$this->row->product 		= OVCDB()->get_row( 'ovc_products', $row_data['ovc_id'], $this->row->results );

		$error_msg = false;

		// Pre-import row validation of basic OVC data
		if( !$this->row->product->exists() ) {
			$error_msg = "OVC ID {$row_data['ovc_id']} does not exist.";
		}
		
		if( $error_msg ) {
			$this->log( $error_msg );
			$this->row->import_status = 'error';
		}


		return false === $error_msg ? true : false;
	}

	public function log_prefix() {
		return str_pad( "CSV LINE: {$this->row->csv_line}", 18 ) . str_pad( "OVC ID: {$this->row->ovc_id}", 18 ) . str_pad( "OMS SKU: {$this->row->data['sku']}", 32 ) . ' - ';
	}



	/**
	 * OLD FUNCTIONS!!!
	 * 
	 **/


	public function import() {

		// Open CSV stream and skip the first line of column headings
		$csv_stream = fopen( $this->op_file()->path(), 'r' );
		
		fgets( $csv_stream, $this->meta('csv_max_chars') ); // Skip first line
		$this->next_row( array(), true );
		$this->use_log_prefix = true;

		while( ($raw_row = fgetcsv( $csv_stream, $this->meta('csv_max_chars') ) ) !== FALSE ) {
			// Skip if post-jump
			if( $this->skip_past_line > $this->row->line ) {
				$this->next_row( array(), true );
				continue;
			}
			// Process the row
			else if( !$this->next_row( $raw_row ) ) {
				$this->row_error( "Invalid number of columns , expecting: ".count( $this->oms_fm->get_fields( 'local_name' ) ).", detected: ".count( $raw_row ) );
			}
			else if ( $this->match_row() && $this->check_row_data() ) {
				$this->update_ovcop_meta();
				$this->update_stock();
			}

			$valid_stat_increment = $this->row->is_valid ? 1 : 0;
			$this->stat( 'valid', $valid_stat_increment );

			// Output Results Msgs
			foreach( $this->row->results->get_msgs() as $msg ) {
				$this->log( $msg );
			}

			//Check if need to jump operation
			if( 55 < ox_exec_time() ) {
				$this->log( "Preparing to Jump OMS -> OVC Import operation. Last OVC ID Processed: {$this->ovc_ids[ count( $this->ovc_ids ) ]}", false );

				$this->skip_past_line = $this->row->line;

				fclose( $csv_stream );
				
				$this->jump_operation();
				return false;
			}
			
		}

		fclose( $csv_stream );
		return true;
	}



	// IMPORT LOOP METHODS / ROW OBJECT HANDLING
	private function next_row_OLD_VERSION( $raw_row = array(), $skipping = false ) {
		// Initialize Row Object if this
		if( !( $this->row instanceof stdClass ) ) {
			$this->row 				= new stdClass();
			$this->row->is_valid	= true;
			$this->row->line 		= 0;
		}

		// Reset the Row Object and increment the row index
		$this->row->line++;
		if( !$skipping ) {
			unset( $this->row->ovcp );
			$this->row->results 	= new OVC_Results;
			$this->row->data 		= array_combine( $this->oms_fm->fields(), $raw_row );
			$this->row->is_valid 	= (bool) $this->row->data;
			$this->row->quiet_skip 	= false;
		}
		
		return $this->row->is_valid;
	}

	private function row_error( $msg ) {
		$this->stat( 'invalid' );
		return $this->row->is_valid = $this->row->results->error( "ERROR! " . $msg );
	}

	private function row_warning( $msg, $return_val = true ) {
		$this->stat( 'warning' );
		$this->row->results->warning( "WARNING! " . $msg );
		return $return_val;
	}

	private function row_error_if_strict( $msg ) {
		return !$this->config( 'test_mode' ) ? $this->row_error( $msg ) : $this->row_warning( $msg );
	}

	private function match_row() {
		$omsovc_data = unserialize( $this->csv_unwrap_value( $this->row->data['Notes Line 7'] ) );

		// Validate OVC Tracking data structure
		if( !is_array( $omsovc_data ) || array_keys( $omsovc_data ) != array( 'ovc_id', 'ovc_oms_export' ) || !$omsovc_data['ovc_id'] ) {
			$this->row->is_valid = false;

			// Check if this is legacy OMS data that is allowed to not have OVC Tracking
			switch ( $this->row->data['Status'] ) {
				case 'Discontinu':
				case 'Inactive':
				case 'Closeout':
				case 'Liquidatn':
					$this->stat( 'ignored' );
					$this->row->quiet_skip = true;
					return false;
				break;
				default:
					return $this->row_error( "Invalid OVC Tracking data. Unrecognizable format." );
				break;
			}
		}
		// Check: Duplicate OVC IDs in CSV
		else if( in_array( $omsovc_data['ovc_id'], $this->ovc_ids ) ) {
			return $this->row_error( "Duplicate OVC IDs in CSV! OVC ID {$omsovc_data['ovc_id']} found previously on CSV line: " . array_search( $omsovc_data['ovc_id'], $this->ovc_ids ) . " - OMS Item#: " . $this->row->data['Item#'] );
		}
		// If still valid, add this OVC ID to the list
		else {
			$this->ovc_ids[ $this->row->line ] = $omsovc_data['ovc_id'];
		}

		// Validate OVC Tracking data content
		$this->row->ovcp = new OVC_Product_OMS( $omsovc_data['ovc_id'], $this );

		// Check: Does the OVC Product exist?
		if( !$this->row->ovcp->exists() ) { 
			return $this->row_error( "Invalid OVC Tracking data. OVC ID {$omsovc_data['ovc_id']} does not exist. PLEASE DELETE SKU {$this->row->data['Item#']} from OMS." );
		}
		// Check: Does the OVC SKU match the OMS SKU?
		else if( $this->row->ovcp->sku != $this->row->data['Item#'] ) {
			return $this->row_error( "OMS SKU does not match OVC SKU! OMS SKU: {$this->row->data['Item#']}, OVC SKU: {$this->row->ovcp->sku}, OVC ID: {$this->row->ovcp->ID}" );
		}
		// Check: Does OMS Export tracking match last unconfirmed OMS export?
		else if( $omsovc_data['ovc_oms_export'] != $this->row->ovcp->data( 'pr.op_oms_export_unconfirmed' ) ) {
			return $this->row_error_if_strict( "This product has been exported from OVC to OMS after this OMS > OVC CSV was generated. Last OMS Export (OVC): {$this->row->ovcp->data( 'pr.op_oms_export_unconfirmed' )}, last OMS Export (OMS): {$omsovc_data['ovc_oms_export']}" );
		}
		// Check: Has this product been updated since its last OMS export?
		else {

			$ovcop_log = OVCDB()->get_row_by_id( $this->row->ovcp->data( 'pr.oms_export_unconfirmed' ), 'ID', 'wp_ovcop_log' );
			$last_export = is_array( $ovcop_log ) ? $ovcop_log['start_time'] : 0;

			$last_updated = $this->row->ovcp->data( 'pr._meta_updated' );

			if( strtotime( $last_updated ) > strtotime( $last_export ) ) {
				return $this->row_error_if_strict( "This product was updated in OVC after its last OMS export. Last updated: {$last_updated}, Last exported: {$last_export}" );
			}
		}

		return true;
	}


	private function check_row_data() {
		foreach( $this->row->data as $oms_field => $oms_csv_value ) {
			// Check that the OMS value matches the OVC value (unless this field should be skipped)
			if( $this->oms_fm->field_needs_back_check( $oms_field ) ) {
				$ovc_csv_value = $this->row->ovcp->get_oms_value( $oms_field, $this->row->results );

				if( $oms_csv_value != $ovc_csv_value ) {
					return $this->row_error_if_strict( "Data mismatch at OMS field: {$oms_field}, CSV Value: {$oms_csv_value}, OVC Value: {$ovc_csv_value}" );	
				}
			}

			/*/ Maybe execute special import function
			if( isset( $this->oms_fm[ $oms_field ]['oms_import_func'] ) && method_exists( $this, $this->oms_fm[ $oms_field ]['oms_import_func'] ) ) {
				$this->{$this->oms_fm[ $oms_field ]['oms_import_func']}();
			}
			*/
		}

		return $this->row->is_valid;
	}

	// Maybe update OVCOP Operation Meta
	private function update_ovcop_meta() {
		if( $this->row->is_valid && !$this->test_mode ) {
			$this->row->ovcp->update_field( 'pr.op_oms_export_confirmed', $this->ID );
		}
	}

	// Maybe update OVC & WC stock
	private function update_stock() {
		if( $this->row->is_valid && $this->config('update_stock') && !$this->config('test_mode') ) {
			if( !is_numeric( $this->row->data['In Stock'] ) ) {
				$this->row_error( "Cannot update stock: Invalid value for stock amount: {$this->row->data['In Stock']}" );
			}
			else {
				$in_stock 	= $this->row->data['In Stock'];
				$so_qty 	= $this->row->data['So Qty'];
				$avail_qty 	= $in_stock - $so_qty;

				if( $in_stock != $this->row->ovcp->stock 
					|| $so_qty != $this->row->ovcp->stock
					|| $avail_qty != $this->row->ovcp->avail_qty
				) { 

					// Update stock amounts in OVC
					OVCDB()->save_ovc_field( $this->row->ovcp->ID, 'stock', $in_stock );
					OVCDB()->save_ovc_field( $this->row->ovcp->ID, 'so_qty', $so_qty );
					OVCDB()->save_ovc_field( $this->row->ovcp->ID, 'avail_qty', $avail_qty );
					
					$this->row->ovcp->init_data( 'ovc_products', true ); // Refresh product data
				}

				$this->row->ovcp->ovc_set_wc_stock( $this->row->results );
				
				$this->stat( 'updated_stock' );
			}
		}
	}


	
}	