<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;

class OVCOP_global_sku_change_export extends OVCOP {

	/**
	 * Operation Key
	 * 
	 * @var string
	 */
	public $type = 'global_sku_change_export';

	public $export_field_set = array(); // OVC Field Map (with fieldmeta information)

	// Semi-magic properties via $op_data['data']
	public $last_id_exported;
	public $exported_ids;

	public static function extra_init( $ovcop ) {

		if( 'csv' == $ovcop->config( 'format' ) ) {

			$ovcop->new_file( 'csv', 'csv', true );

		} else {
			$ovcop->results->success = false;
			$ovcop->results->main_msg = "initialization failed. Invalid output format type.";
			return false;
		}

		$ovcop->log( "Exporting changed SKUs. ( " . OVC_Sync_Manager::get_auto_sync_count( 'oms_global_change_skus' ) . " SKUs to export )" );

		$ovcop->last_id_exported	= 0;
		$ovcop->exported_ids		= array();

		// Initialize CSV
		$ovcop->new_file( 'csv', 'csv', true );

		// Send initialization response 
		$ovcop->results->success = true;
		$ovcop->results->main_msg = "Global SKU Change Export operation initialized successfully";

		// Tell OVCOP to automatically commence the operation
		return true;
	}

	public function __construct( $request, $args ) {
		$this->child_construct( $request, $args );

		// Initialize OVC Field Set
		$this->export_field_set = OVCSC::get_field_set( 'ovc_products' );

		// Set Line Endings
		$this->op_file()->line_endings = "\r\n";

		$this->export();
	}

	/**
	 * SKU Update function
	 * 
	 * @return boolean|void
	 */
	public function export() {

		if( !$this->jumped ) {
			// Write headers line to CSV
			$this->write_csv_line_from_array( array( 'OLD', 'NEW' ) );
		}

		$ovc_ids = $this->get_ovc_ids();

		$skus_exported = 0;

		foreach( $ovc_ids as $ovc_id ) {
			$results = new OVC_Results;
			$sku_row = OVCDB()->get_row( $this->export_field_set, $ovc_id, $results );

			$csv_row_values = array();

			// Old SKU
			$csv_row_values[] = $sku_row->oms_sku_confirmed;

			// New SKU
			$csv_row_values[] = $sku_row->sku;

			foreach( $results->get_msgs() as $msg ) {
				$this->log( $msg );
			}

			$this->write_csv_line_from_array( $csv_row_values );

			$this->exported_ids[] = $sku_row->ID;

			$this->check_user_abort();

			$skus_exported++;

			// Check if we need to jump operation
			if( 55 < ox_exec_time() ) {
				$this->log( "Preparing to jump Global SKU Export operation. Last SKU Processed: " );

				$this->last_id_exported = $ovc_id;

				$this->jump_operation();
				return false;
			}
		}

		$this->log( "Gobal SKU Export completed successfully. Number of SKUs exported: {$skus_exported} - Current request elapsed exec time (seconds): " . ox_exec_time() );

		$this->successful_finish();

	}

	public function get_ovc_ids() {
		global $wpdb;

		return $wpdb->get_col( "
			SELECT skuexport.ID 
			FROM (
				SELECT pr.ID
				FROM wp_ovc_products pr
				WHERE pr.sku != pr.oms_sku_confirmed
				AND pr.oms_sku_confirmed != ''
			) skuexport 
			LEFT JOIN wp_ovc_products pr 
				ON skuexport.ID = pr.ID 
			WHERE pr.ID IS NOT NULL 
				AND skuexport.ID > {$this->last_id_exported}
		" );
	}

}