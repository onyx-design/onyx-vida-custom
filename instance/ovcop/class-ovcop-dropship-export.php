<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVCOP_dropship_export extends OVCOP {
	public $type = 'dropship_export';
	public $op_step = 3;

	// Simple operation-specific properties (re-initialized for each commence_operation request)
	public $export_field_set = array(); // Dropship Field Map (with fieldmeta information)

	// Semi-magic properties via $op_data['data']
	public $parent_skus;
	public $exported_ids;
	

	// Operation Type Specific Initialization
	public static function extra_init( $ovcop ) {
		global $wpdb;

		// Initialize operation data based on configuration options
		$ovcop->parent_skus = false;
		if( 'parent_skus' == $ovcop->config( 'sku_filter' ) ) {
			$ovcop->parent_skus = array_map( 'sanitize_text_field', array_map( 'trim', explode( ",", $ovcop->config( 'filter_parent_skus' ) ) ) );
		}
		$ovcop->log( "SKU Filtering: {$ovcop->config( 'sku_filter' )}" );

		$ovcop->exported_ids = array();

		// Initialize Export CSV
		$ovcop->new_file( 'csv', 'csv', true );

		// Send initialization response 
		$ovcop->results->success = true;
		$ovcop->results->main_msg = "Dropship Export operation initialized successfully";

		// Tell OVCOP to automatically commence the operation
		return true;
	}


	public function __construct( $request, $args ) {
		$this->child_construct( $request, $args );

		// Initialize Dropship Field Set
		$this->export_field_set = OVCSC::get_field_set( $this->optype_meta( 'field_set' ) );

		$this->export();	
	}

	public function export() {

		if ( !$this->jumped ) {
			// Write headers line to CSV
			$this->write_csv_line_from_array( array_map( array( $this, 'csv_wrap_value' ), $this->export_field_set->get_fields( 'local_name' ) ) );
		}
		
		// Build query and get OVC IDs for export
		global $wpdb;
		
		$qry = "SELECT ID FROM wp_ovc_products WHERE ";

		if( 'parent_skus' == $this->config( 'sku_filter' ) ) {
			$qry .= "parent_sku IN(\"" . implode( "\",\"", $this->parent_skus ) . "\") AND ";
		}
		else if ( 'custom_selection' == $this->config( 'sku_filter' ) ) {
			$qry .= "ID IN(" . implode( ',', get_ovc_user_custom_selection( $this->user_id, 'ovc_products' ) ) . ") AND ";
		}
		else {
			$this->abort_operation( 'Invalid SKU Filtering Configuration' );
		}

		$qry .= "sync_oms = 1 ORDER BY parent_sku ASC";

		$ovc_ids = $wpdb->get_col( $qry );

		$this->log( 'Beginning Export. ' . count( $ovc_ids ) . ' OVC IDs remaining to be exported.' );

		$prfs = OVCSC::get_field_set( 'ovc_products' );

		// Loop through and export IDs 
		foreach( $ovc_ids as $ovc_id ) {
			$results = new OVC_Results;
			$ovcp = OVCDB()->get_row( $prfs, $ovc_id, $results );
			$csv_row_values = array();
			
			foreach( $this->export_field_set->get_fields() as $field ) {
				$ds_value = $ovcp->get_external_value( $field, $results );

				if( false === $ds_value ) {
					$results->error( "ERROR! OVC ID: {$ovc_id} - dropship value is false for field {$field}" );
				}

				$escape_csv_value = ! (bool) $field->meta( 'csv_no_escape' );

				$csv_row_values[] = $this->csv_wrap_value( $ds_value, $escape_csv_value );
			}

			foreach( $results->get_msgs() as $msg ) {
				$this->log( $msg );
			}

			$this->write_csv_line_from_array( $csv_row_values );
			$this->exported_ids[] = $ovcp->ID;
		}

		$this->successful_finish();
	}

	public function before_successful_finish() {
		// Set stat for final report
		$this->init_stat( 'exported_products', '# of Products Exported' );
		$this->stat( 'exported_products', count( $this->exported_ids ) );
	}

	public function after_successful_finish() {
		// Output link to download CSV
		$this->log();
		$this->log( "Download exported CSV here: " . $this->op_file()->url() );
		$this->log();
	}
}	