<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVCOP_data_scan extends OVCOP {
	public $type = 'ovc_data_scan';

	// Semi-magic properties via $op_data['data']
	public $last_ovc_id;
	public $chunk_query;
	public $ovc_assemblies = array();
	public $skipped_case_skus = array();

	// Request-level properties (not stored in $op_data['data'])


	public static function extra_init( $ovcop ) {
		
		if( $ovcop->config( 'data_errors' ) ) {

			// Init semi-magic properties
			$ovcop->last_ovc_id = 0;

			// Determine sync mode
			if( 'scan_all' == $ovcop->config( 'scan_mode' ) ) {

				$total_ids = OVCDB()->count_results( 'ovc_products' );
				$ovcop->chunk_query = "SELECT ID FROM wp_ovc_products WHERE ID > %d ORDER BY ID ASC LIMIT 0, 1000";
			}
			else if( 'auto_scan' == $ovcop->config( 'scan_mode' ) ) {

				$total_ids = OVC_Sync_Manager::get_auto_sync_count( 'ovc_data_scan' );
				$ovcop->chunk_query = "
					SELECT pr_view.ID 
					FROM (
						SELECT pr.ID
						FROM wp_ovc_products pr
						WHERE pr._meta_updated > (
							SELECT MAX( ol.start_time ) 
							FROM wp_ovcop_log ol 
							WHERE ol.status = 'complete' 
							ORDER BY ol.ID DESC 
							LIMIT 0,1
						)
						ORDER BY pr.ID ASC
					)  pr_view
					WHERE pr_view.ID > %d
					ORDER BY pr_view.ID ASC LIMIT 0, 1000
				";
			}

			$ovcop->log( "Total OVC IDs to check: {$total_ids}" );
			$ovcop->init_stat( 'ids_checked', 'IDs Checked' );
			$ovcop->init_stat( 'errors_found', 'Errors Found' );
			$ovcop->init_stat( 'errors_resolved', 'Errors Resolved' );
		}

		if( $ovcop->config( 'clean_archive_ovcops' ) ) {

			$ovcop->init_stat( 'ovcop_logs_archived', 'OVCOP Logs Archives' );
		}

		if( $ovcop->config( 'setup_assemblies' ) ) {

			$ovcop->ovc_assemblies = array();
			$ovcop->skipped_case_skus = array();

			$ovcop->init_stat( 'assemblies_setup', 'Assemblies Setup' );
			$ovcop->init_stat( 'assemblies_failed_setup', 'Assemblies Failed Setup' );
		}

		// Send initialization response 
		$ovcop->results->success = true;
		$ovcop->results->main_msg = "OVC Data Scan operation initialized successfully";

		// Tell OVCOP to automatically commence the operation
		return true;
	}

	public function __construct( $request, $args ) {
		$this->child_construct( $request, $args );

		$this->scan_data();	
	}

	public function scan_data() {

		$this->check_user_abort( true );

		global $wpdb;

		// Preload active errors and error codes
		OVC_Row_error_codes::get_all_error_codes();
		OVC_Row_data_errors::enable();
		OVC_Row_data_errors::load_active_errors();
		$error_codes_to_check = OVCDB()->get_loaded_rows_where(FS('errcd'),array('group'=>'ovc_products'));

		/*
		 * 	Expected error_codes to check: (updated 2018-10-02)
		 * 	
		 *	sync_oms_required_fields
		 *	sync_walmart_required_fields
		 *	sync_wc_required_fields
		 *	sync_shopify_required_fields
		 *	invalid_upc_code
		 *	duplicate_upc_code
		 **/

		if( $this->config( 'data_errors' ) ) {

			$this->log( "Commencing Data Scan for Errors. Last OVC ID checked: {$this->last_ovc_id}" );

			while( $ovc_ids_chunk = $wpdb->get_col( $wpdb->prepare( $this->chunk_query, $this->last_ovc_id ) ) ) {

				$this->log( "Beginning chunk of next 1000 IDs. First ID in chunk: {$ovc_ids_chunk[0]}" );

				foreach( $ovc_ids_chunk as $ovc_id ) {

					// Check this OVC ID
					$results = new OVC_Results();

					$ovc_product = OVCDB()->get_row(FS('ovc_products'),$ovc_id,$results,false);

					$errors_found_or_resolved = null;

					foreach( $error_codes_to_check as $error_code ) {

						$error_check_result = $error_code->check_ovc_product( $ovc_product, $results );

						if( isset( $error_check_result ) ) {

							$this->log( $error_code->error_code . ' for ID ' . $ovc_product->ID . ' - ' . ox_boolstr( $error_check_result ) );

							$errors_found_or_resolved = true;

							if( true === $error_check_result ) {

								$this->stat( 'errors_found' );
							}
							else if( false === $error_check_result ) {

								$this->stat( 'errors_resolved' );
							}
						}
					}

					if( $errors_found_or_resolved ) {

						$this->log( "Error(s) found/resolved for OVC ID: {$ovc_id} - SKU: {$ovc_product->sku}" );
						$this->log_results_msgs( $results );
					}
					// self::static_check_fields_to_fix( $ovc_id, $results );

					// if( $results->has_error ) {
					// 	$product = OVCDB()->get_row( 'ovc_products', $ovc_id, $results );
					// 	$this->log( "Error(s) found for OVC ID: {$ovc_id} - SKU: {$product->sku}" );
					// 	$this->log_results_msgs( $results );
					// }

					// Update tracking & stats
					$this->last_ovc_id = $ovc_id;

					$this->stat( 'ids_checked' );

					// OVCOP Check-in (maybe abort or jump)
					if( 50 < ox_exec_time() ) {
						
						$this->jump_operation();
					}

					$this->check_user_abort();
				}

				// $this->log( 'Memory Usage before OVCDB Flush: ' . number_format( ovc_memory_usage_percentage() * 100, 2 ) . '%' . ' Number of queries: ' . get_num_queries() . ' Exec Time: ' . ox_exec_time() );
				OVCDB()->flush_db_data();
				// $this->log( 'Memory Usage after OVCDB Flush: ' . number_format( ovc_memory_usage_percentage() * 100, 2 ) . '%' );
			}
		}

		/**
		 * Clean and Archive OVC Operation Logs
		 */
		if( $this->config( 'clean_archive_ovcops' ) ) {

			if( !$this->jumped ) {
				$this->log( 'Beginning cleanup and archival process of OVC Operation logs.' );
			} else {
				$this->log( 'Continuing cleanup and archival process of OVC Operation logs.' );
			}

			$month_indices = $wpdb->get_col(
				"
					SELECT ( YEAR( _meta_created ) * 12 + MONTH( _meta_created ) ) month_index
					FROM {$wpdb->prefix}ovcop_log oplog
					WHERE DATE( oplog._meta_created ) <= LAST_DAY( CURRENT_DATE - INTERVAL 2 MONTH )
						AND oplog.archived = 0
					GROUP BY month_index
					ORDER BY month_index DESC
				"
			);

			foreach( $month_indices as $month_index ) {

				$month = DateTime::createFromFormat( '!m', $month_index % 12 )->format( 'F' );
				$year = intval( $month_index / 12 );

				$this->log( "Archiving Operation logs from {$month} {$year}" );

				$this->archive_ovcops( $month_index );

				$this->stat( 'ovcop_logs_archived' );

				if( 50 < ox_exec_time() ) {

					$this->jump_operation();
				}
			}
		}

		/**
		 * Initialize OVC Assemblies that don't already exist
		 * 
		 * Convert OVC Products into OVC Assemblies and OVC Assembly Items
		 */
		if( $this->config( 'setup_assemblies' ) ) {

			/*
			 * Setup the Assemblies
			 */
			$cases = $wpdb->get_results(
				"
					SELECT pr.case_sku, MAX( pr.unit_per_case ) units_per_case
					FROM {$wpdb->prefix}ovc_products pr
					LEFT JOIN {$wpdb->prefix}ovc_products cs
						ON cs.sku = pr.case_sku
					LEFT JOIN {$wpdb->prefix}ovc_assemblies asm
						ON asm.ovc_id = cs.ID
					WHERE pr.case_sku != ''
						AND pr.case_sku != pr.sku
						AND asm.ID IS NULL
						AND pr.case_sku NOT IN ('" . implode( "','", $this->skipped_case_skus ) . "')
					GROUP BY pr.case_sku
				"
			);

			if( !$this->jumped ) {
				$this->log( 'Beginning setup of OVC Assemblies. ' . count( $cases ) . ' OVC IDs remaining.' );
			} else {
				$this->log( 'Continuing setup of OVC Assemblies. ' . count( $cases ) . ' OVC IDs remaining.' );
			}

			foreach( $cases as $case ) {

				$assembly_results = new OVC_Results();

				$case_row = OVCDB()->get_row_by_valid_id( FS( 'ovc_products' ), 'sku', $case->case_sku, $assembly_results );

				if( $case_row->exists() ) {

					$assembly_data = array(
						'ovc_id'		=> $case_row->data( 'pr.ID' ),
						// 'total_quantity'=> $case->units_per_case,
					);
					$ovc_assembly = OVCDB()->new_row( FS( 'ovc_assemblies' ), $assembly_data );

					if( $ovc_assembly->save( $assembly_results ) ) {

						$this->stat( 'assemblies_setup' );
						$init_assembly_items = $ovc_assembly->init_assembly_items( $assembly_results );

						if( empty( $init_assembly_items ) ) {

							$this->stat( 'assemblies_failed_setup' );
							$this->log( "Case SKU: {$case->case_sku} children have failed to initialize" );
						} elseif( $assembly_results->has_error ) {

							$this->log( "Case SKU: {$case->case_sku} has some children that have failed to initialize" );
						}
					}
				} else {

					$this->skipped_case_skus[] = $case->case_sku;

					$this->stat( 'assemblies_failed_setup' );
					$this->log( "Case SKU: {$case->case_sku} does not exist" );
				}

				$this->log_results_msgs( $assembly_results );

				if( 50 < ox_exec_time() ) {
					$this->jump_operation();
				}
			}

			/*
			 * Validate the Assemblies
			 */
			// if( empty( $this->ovc_assemblies ) ) {
			// 	$this->ovc_assemblies = OVCDB()->load_rows_where( FS( 'asm' ), array(), $assembly_results );
			// }

			// $this->log( 'Validating OVC Assemblies. ' . count( $this->ovc_assemblies ) . ' remaining.' );

			// while( $ovc_assembly = array_shift( $this->ovc_assemblies ) ) {

			// 	$validate_results = new OVC_Results();

			// 	if( !$ovc_assembly->validate_total_quantity( $validate_results ) ) {
			// 		$this->log( 'OVC Assembly ID: ' . $ovc_assembly->ID . ' has an invalid total quantity.' );
			// 	}

			// 	if( 50 < ox_exec_time() ) {
			// 		$this->jump_operation();
			// 	}
			// }
		}

		// UPDATE OPTION FOR SETTING AUTO-DATA SCAN INTERVAL //dev:improve
		update_option( 'ovcop__last_ovc_data_scan', time() );

		$this->successful_finish();
	}


	public static function static_check_fields_to_fix( $ovc_id, &$results = false ) {
		OVC_Results::maybe_init( $results );

		$fs_pr = OVCSC::get_field_set( 'ovc_products' );

		$product = OVCDB()->get_row( $fs_pr, $ovc_id, $results );

		$fix_row = OVCDB()->get_row_by_id( $ovc_id, 'ovc_id', 'wp_ovc_data_fixes' );

		

		// If marketplaces were unsynced already, simulate their data checks by marking them as synced
		$product_data = $product->data_set( $fs_pr, false );

		$unsynced_marketplaces = array();
		if( is_array( $fix_row ) && isset( $fix_row['unsynced'] ) ) {

			$unsynced_marketplaces = explode( ',', $fix_row['unsynced'] );
			foreach( $unsynced_marketplaces as $index => $unsynced_marketplace ) {

				// Safety check to make sure spaces dont end up as empty array values
				if( !$unsynced_marketplace ) {
					unset( $unsynced_marketplaces[ $index ] );
				}
				else {
					$product_data[ $unsynced_marketplace ] = 1;	
				}
			}
		}

		OVCDB()->save_ovc_row( $fs_pr, $product_data, $results, true ); // Pretend to save the row (check_only mode is on)

		$field_errors_by_type = array();

		foreach( $results->field_errors as $error_type => $error_fields ) {

			if( count( $error_fields ) ) {

				if( 'unsynced' == $error_type ) {

					global $wpdb;
				
					foreach( $error_fields as $newly_unsynced ) {

						if( !in_array( $newly_unsynced, $unsynced_marketplaces ) ) {

							$unsynced_marketplaces[] = $newly_unsynced;

							$results->notice( "OVC ID {$product->ID}, SKU {$product->SKU} will be unsynced from {$newly_unsynced}. It will automatically re-sync when its data is fixed." );

							if( false === $wpdb->query( "UPDATE wp_ovc_products SET {$newly_unsynced} = 0 WHERE ID = {$product->ID}" ) ) {
								$results->error( "ERROR trying to set {$newly_unsynced} to zero for OVC ID {$product->ID}." );
							}	
						}

					}
				}
				else {
					$error_type_prefix = strtoupper( str_replace( '_', ' ', $error_type ) ) . ': ';

					$field_errors_by_type[ $error_type ] = $error_type_prefix . implode( ', ', $error_fields );
				}

				
			}
		}

		$field_errors_str = implode( ' - ', $field_errors_by_type );



		//global $wpdb;

		//$fix_id = $wpdb->get_var( "SELECT ID FROM wp_ovc_data_fixes WHERE ovc_id = {$product->ID}" );

		//$fix_row = OVCDB()->get_row_by_id( $ovc_id, 'ovc_id', 'wp_ovc_data_fixes' );


		// There are fields to fix
		if( $field_errors_str ) {

			

			/*
			$data_fixes_row = array(
				'fields_to_fix'	=> implode( ', ', $fields_to_fix )
			);
			* /
			

			if( !$fix_row ) { // insert

				$insert_data = array(
					'ovc_id'		=> $product->ID,
					'fields_to_fix'	=> $field_errors_str
				);

				OVCDB()->save_ovc_row( 'data_fixes', $insert_data );

				//$fields_to_fix = implode( ', ', $fields_to_fix );

				$qry = "INSERT INTO wp_ovc_data_fixes (`ovc_id`,`fields_to_fix`) VALUES({$product->ID},'{$field_errors_str}')";


				$wpdb->query( $qry );
				* /
			}
			else { // update
				$where_data = array(
					'ovc_id'	=> $product->ID
				);

				$wpdb->update( 'wp_ovc_data_fixes', array( 'fields_to_fix' => $field_errors_str ), $where_data, '%s', '%d' );
			}
			/* */

			// Insert/Update data fixes row
			$updated_data = array(
				'ovc_id'		=> $product->ID,
				'fields_to_fix'	=> $field_errors_str,
				'unsynced'		=> implode( ',', $unsynced_marketplaces )
			);

			/*/ Maybe merge unsynced_marketplaces
			if( $fix_row && $fix_row['unsynced'] ) {
				$already_unsynced = explode( ',', $fix_row['unsynced'] );
				$updated_data['unsynced'] = implode( ',', array_unique( $already_unsynced + $results->field_errors['unsynced'] ) );
			}*/

			OVCDB()->save_ovc_row( 'data_fixes', $updated_data );

			// Get updated data fixes row
			return OVCDB()->get_row_by_id( $ovc_id, 'ovc_id', 'wp_ovc_data_fixes' );

		

			//return $wpdb->get_row( "SELECT * FROM wp_ovc_data_fixes WHERE ID = {$fix_id}", ARRAY_A );
		}
		// If no fields to fix on a row with existing data fixes, delete from data fixes and restore any sync settings
		else if( $fix_row ) {

			$results->notice( 'All data has been fixed for OVC ID: {$product->ID}, SKU: {$product->SKU}' );

			if( $fix_row['unsynced'] ) {

				foreach( explode( ',', $fix_row['unsynced'] ) as $sync_field ) {

					if( $product->update_field( "pr.{$sync_field}", 1 ) ) {

						$results->notice( "SKU {$product->SKU} has been set to resync to marketplace: {$sync_field}" );	
					}
					else {

						$results->error( "ERROR setting {$product->sku} to resync to marketplace: {$sync_field}" );
					}
					
				}
			}

			global $wpdb;
			if( $wpdb->delete( 'wp_ovc_data_fixes', array( 'ID' => $fix_row['ID'] ), '%d' ) ) {
				return 'deleted';
			}
			else {
				$results->error( "ERROR trying to delete data_fix row {$fix_row['ID']} for OVC ID: {$product->ID}, SKU {$product->sku}" );
			}
		}
		else {
			return null;
		}
	}

	/*
	 * OVCOP Archival Functions
	 */

	/**
	 * Archive OVC OPs based on $month_index
	 * 		$month_index: ( YEAR * 12 ) + MONTH
	 * 
	 * @param 	int 	$month_index
	 * 
	 * @return 	bool
	 */
	public function archive_ovcops( $month_index = 0 ) {
		global $wpdb;

		$archive_ovcops = $wpdb->get_results(
			$wpdb->prepare(
				"
					SELECT oplog.*
					FROM {$wpdb->prefix}ovcop_log oplog
					WHERE ( YEAR( _meta_created ) * 12 + MONTH( _meta_created ) ) = %d
						AND oplog.archived = 0
					ORDER BY oplog._meta_created DESC
				",
				$month_index
			)
		);

		$ops_to_save = array();

		// Get the last OVCOP OPs for each type to save the files
		foreach( $archive_ovcops as $archive_ovcop ) {

			if( !isset( $ops_to_save[ $archive_ovcop->type ] ) ) {
				$ops_to_save[ $archive_ovcop->type ] = $archive_ovcop->ID;
			}
		}

		foreach( $archive_ovcops as $archive_ovcop ) {

			$ovcop_row = OVCDB()->get_row( FS( 'ovcop_log' ), $archive_ovcop->ID );

			$save_files = $archive_ovcop->ID == $ops_to_save[ $archive_ovcop->type ];
			$ovcop_row->archive_ovcop( $save_files );
		}

		return true;
	}

	public function log_prefix() {
		return "ID: " . str_pad( $this->ovc_product->ID, 10 ) . "SKU: " . str_pad( $this->ovc_product->sku, 28 ) . " - ";
	}
}

