<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use Cache\Adapter\Filesystem\FilesystemCachePool;

class OVCOP_ovc_inventory_export extends OVCOP {
	public $type = 'ovc_inventory_export';

	// Simple operation-specific properties (re-initialized for each commence_operation request)
	public $inventory_export_field_set = array(); // OVC Inventory Field Map (with fieldmeta information)
	public $assembly_export_field_set = array(); // OVC Inventory Field Map (with fieldmeta information)
	public $assembly_item_export_field_set = array(); // OVC Inventory Field Map (with fieldmeta information)
	public $inventory_assembly = false;

	// Semi-magic properties via $op_data['data']
	public $last_inventory_id_exported;
	public $last_assembly_id_exported;
	public $last_assembly_item_id_exported;
	public $exported_inventory_ids;
	public $exported_assembly_ids;
	public $exported_assembly_item_ids;
	public $export_files;

	public static function extra_init( $ovcop ) {

		if( !in_array( $ovcop->config( 'export_format' ), array( 'inventory', 'inventory_assembly' ) ) ) {

			$ovcop->results->success = false;
			$ovcop->results->main_msg = 'Initialization failed. Invalid output format type.';

			return false;
		}

		// Initialize semi-magic properties
		$ovcop->last_inventory_id_exported = 0;
		$ovcop->last_assembly_id_exported = 0;
		$ovcop->last_assembly_item_id_exported = 0;
		$ovcop->exported_inventory_ids = array();
		$ovcop->exported_assembly_ids = array();
		$ovcop->exported_assembly_item_ids = array();
		$ovcop->export_files = array();

		$ovcop->results->success = true;
		$ovcop->results->main_msg = 'OVC Inventory Export operation initialized successfully';

		// Tell OVCOP to automatically commence the operation
		return true;
	}

	public function __construct( $request, $args ) {
		$this->child_construct( $request, $args );

		// Initialize OVC Inventory Export Field Sets
		$this->inventory_export_field_set = OVCSC::get_field_set( 'ovc_inventory_export' );
		$this->assembly_export_field_set = OVCSC::get_field_set( 'ovc_assembly_parents_export' );
		$this->assembly_item_export_field_set = OVCSC::get_field_set( 'ovc_assembly_items_export' );

		$this->export();
	}

	public function export() {

		$this->check_user_abort( true );

		if( empty( $this->meta( 'op_step' ) ) ) {
			$this->meta( 'op_step', 0 );
		}

		/*
		 * Step 1
		 * Build Inventory CSV
		 */
		if( 1 >= $this->meta( 'op_step' ) ) {

			$this->meta( 'op_step', 1 );

			$this->log();
			$this->log( "Sync Step {$this->meta( 'op_step' )}: Building Inventory CSV." );

			$this->build_inventory_csv();
		}

		/*
		 * Step 2
		 * Build Assembly Parents CSV
		 */
		if( 2 >= $this->meta( 'op_step' ) && ( 'inventory_assembly' == $this->config( 'export_format' ) ) ) {

			$this->meta( 'op_step', 2 );

			$this->log();
			$this->log( "Sync Step {$this->meta( 'op_step' )}: Building Assembly Parents CSV." );

			$this->build_assembly_parents_csv();
		}

		/*
		 * Step 3
		 * Build Assembly Items CSV
		 */
		if( 3 >= $this->meta( 'op_step' ) && ( 'inventory_assembly' == $this->config( 'export_format' ) ) ) {

			$this->meta( 'op_step', 3 );

			$this->log();
			$this->log( "Sync Step {$this->meta( 'op_step' )}: Building Assembly Items CSV." );

			$this->build_assembly_items_csv();
		}

		$this->successful_finish();
	}

	public function build_inventory_csv() {
		global $wpdb;

		if( !isset( $this->export_files['inventory'] ) ) {

			$inventory_csv = $this->new_file( 'inventory_export', 'csv', true );
			$this->export_files['inventory'] = $inventory_csv;

			$header_row_data = $this->inventory_export_field_set->get_fields();

			if( !$this->config( 'use_in_stock_quantity' ) ) {
				unset( $header_row_data['potential_qty_instock'] );
			}

			// Maybe write headers line to csv
			$this->write_csv_line_from_array( array_map( array( $this, 'csv_wrap_value' ), $header_row_data ) );
		}

		$query = "
			SELECT ID
			FROM wp_ovc_products
			WHERE ID > {$this->last_inventory_id_exported}
			ORDER BY ID ASC
		";

		$ovc_ids = $wpdb->get_col( $query );

		$this->log( 'Beginning Export: ' . count( $ovc_ids ) . ' OVC IDs remaining to be exported.' );

		$prfs = OVCSC::get_field_set( 'ovc_products' );

		$jump_lines_exported = 0;

		// Loop through and export IDs
		foreach( $ovc_ids as $ovc_id ) {

			$results = new OVC_Results;
			$ovcp = OVCDB()->get_row( $prfs, $ovc_id, $results );
			$csv_row_values = array();

			foreach( $this->inventory_export_field_set->get_fields() as $field ) {

				if( !$this->config( 'use_in_stock_quantity' ) 
					&& ( 'potential_qty_instock' == $field )
				) {
					continue;
				}
				$ovc_value = $ovcp->get_external_value( $field, $results );

				if( false === $ovc_value ) {
					$results->error( "ERROR! OVC ID: {$ovc_id} - ovc_value is false for field {$field}" );
				}

				$escape_csv_value = !(bool) $field->meta( 'csv_no_escape' );

				$csv_row_values[] = $this->csv_wrap_value( $ovc_value, $escape_csv_value );
			}

			foreach( $results->get_msgs() as $msg ) {
				$this->log( $msg );
			}

			$this->write_csv_line_from_array( $csv_row_values );

			$jump_lines_exported++;

			$this->exported_inventory_ids[] = $ovcp->ID;

			if( !( $jump_lines_exported % 50 ) ) {

				if( !( $jump_lines_exported % 1000 ) ) {

					if( !( $jump_lines_exported % 2000 ) ) {

						$this->log( "Exported {$jump_lines_exported} this jump. Last ID: {$ovcp->ID} - Remaining IDs: " . ( count( $ovc_ids ) - $jump_lines_exported ) );
					}

					OVCDB()->flush_db_data();
				}

				$this->last_inventory_id_exported = $ovc_id;

				$this->check_user_abort();
			}

			// Check if we need to jump
			if( 55 < ox_exec_time() ) {

				$this->log( "Preparing to jump OVC Inventory Export operation. Last ID Processed: {$ovc_id}", false );
				$this->log( "Exported {$jump_lines_exported} this jump. Last ID: {$ovcp->ID} - Remaining IDs: " . ( count( $ovc_ids ) - $jump_lines_exported ) );

				$this->last_inventory_id_exported = $ovc_id;

				$this->jump_operation();

				return false;
			}
		}

		$this->export_files['inventory']->complete_file();
	}

	public function build_assembly_parents_csv() {
		global $wpdb;

		if( !isset( $this->export_files['assembly_parents'] ) ) {

			$assembly_parents_csv = $this->new_file( 'assembly_parents_export', 'csv', true );
			$this->export_files['assembly_parents'] = $assembly_parents_csv;

			// Maybe write headers line to csv
			$this->write_csv_line_from_array( array_map( array( $this, 'csv_wrap_value' ), $this->assembly_export_field_set->get_fields() ) );
		}

		$query = "
			SELECT ID
			FROM wp_ovc_assemblies
			WHERE ID > {$this->last_assembly_id_exported}
			ORDER BY ID ASC
		";

		$assembly_ids = $wpdb->get_col( $query );

		$this->log( 'Beginning Export: ' . count( $assembly_ids ) . ' Assembly Parents remaining to be exported.' );

		$asm_fs = OVCSC::get_field_set( 'ovc_assemblies' );

		$jump_lines_exported = 0;

		// Loop through and export IDs
		foreach( $assembly_ids as $assembly_id ) {

			$results = new OVC_Results;
			$ovc_asm = OVCDB()->get_row( $asm_fs, $assembly_id, $results );
			$csv_row_values = array();

			foreach( $this->assembly_export_field_set->get_fields() as $field ) {

				$ovc_value = $ovc_asm->get_external_value( $field, $results );

				if( false === $ovc_value ) {
					$results->error( "ERROR! Assembly ID: {$assembly_id} - ovc_value is false for field {$field}" );
				}

				$escape_csv_value = !(bool) $field->meta( 'csv_no_escape' );

				$csv_row_values[] = $this->csv_wrap_value( $ovc_value, $escape_csv_value );
			}

			foreach( $results->get_msgs() as $msg ) {
				$this->log( $msg );
			}

			$this->write_csv_line_from_array( $csv_row_values );

			$jump_lines_exported++;

			$this->exported_assembly_ids[] = $ovc_asm->ID;

			if( !( $jump_lines_exported % 50 ) ) {

				if( !( $jump_lines_exported % 1000 ) ) {

					if( !( $jump_lines_exported % 2000 ) ) {

						$this->log( "Exported {$jump_lines_exported} this jump. Last ID: {$ovc_asm->ID} - Remaining IDs: " . ( count( $assembly_ids ) - $jump_lines_exported ) );
					}

					OVCDB()->flush_db_data();
				}

				$this->last_assembly_id_exported = $assembly_id;

				$this->check_user_abort();
			}

			// Check if we need to jump
			if( 55 < ox_exec_time() ) {

				$this->log( "Preparing to jump OVC Inventory Export operation. Last ID Processed: {$assembly_id}", false );
				$this->log( "Exported {$jump_lines_exported} this jump. Last ID: {$ovc_asm->ID} - Remaining IDs: " . ( count( $assembly_ids ) - $jump_lines_exported ) );

				$this->last_assembly_id_exported = $assembly_id;

				$this->jump_operation();

				return false;
			}
		}

		$this->export_files['assembly_parents']->complete_file();
	}

	public function build_assembly_items_csv() {
		global $wpdb;

		if( !isset( $this->export_files['assembly_items'] ) ) {

			$assembly_items_csv = $this->new_file( 'assembly_items_export', 'csv', true );
			$this->export_files['assembly_items'] = $assembly_items_csv;

			// Maybe write headers line to csv
			$this->write_csv_line_from_array( array_map( array( $this, 'csv_wrap_value' ), $this->assembly_item_export_field_set->get_fields() ) );
		}

		$query = "
			SELECT ID
			FROM wp_ovc_assembly_items
			WHERE ID > {$this->last_assembly_item_id_exported}
			ORDER BY ID ASC
		";

		$assembly_item_ids = $wpdb->get_col( $query );

		$this->log( 'Beginning Export: ' . count( $assembly_item_ids ) . ' Assembly Items remaining to be exported.' );

		$asmi_fs = OVCSC::get_field_set( 'ovc_assembly_items' );

		$jump_lines_exported = 0;

		// Loop through and export IDs
		foreach( $assembly_item_ids as $ovc_id ) {

			$results = new OVC_Results;
			$ovc_asmi = OVCDB()->get_row( $asmi_fs, $ovc_id, $results );
			$csv_row_values = array();

			foreach( $this->assembly_item_export_field_set->get_fields() as $field ) {

				$ovc_value = $ovc_asmi->get_external_value( $field, $results );

				if( false === $ovc_value ) {
					$results->error( "ERROR! OVC ID: {$ovc_id} - ovc_value is false for field {$field}" );
				}

				$escape_csv_value = !(bool) $field->meta( 'csv_no_escape' );

				$csv_row_values[] = $this->csv_wrap_value( $ovc_value, $escape_csv_value );
			}

			foreach( $results->get_msgs() as $msg ) {
				$this->log( $msg );
			}

			$this->write_csv_line_from_array( $csv_row_values );

			$jump_lines_exported++;

			$this->exported_assembly_item_ids[] = $ovc_asmi->ID;

			if( !( $jump_lines_exported % 50 ) ) {

				if( !( $jump_lines_exported % 1000 ) ) {

					if( !( $jump_lines_exported % 2000 ) ) {

						$this->log( "Exported {$jump_lines_exported} this jump. Last ID: {$ovc_asmi->ID} - Remaining IDs: " . ( count( $assembly_item_ids ) - $jump_lines_exported ) );
					}

					OVCDB()->flush_db_data();
				}

				$this->last_assembly_item_id_exported = $ovc_id;

				$this->check_user_abort();
			}

			// Check if we need to jump
			if( 55 < ox_exec_time() ) {

				$this->log( "Preparing to jump OVC Inventory Export operation. Last ID Processed: {$ovc_id}", false );
				$this->log( "Exported {$jump_lines_exported} this jump. Last ID: {$ovc_asmi->ID} - Remaining IDs: " . ( count( $assembly_item_ids ) - $jump_lines_exported ) );

				$this->last_assembly_item_id_exported = $ovc_id;

				$this->jump_operation();

				return false;
			}
		}

		$this->export_files['assembly_items']->complete_file();
	}

	public function before_succeessful_finish() {
		// Set stat for final report
		$this->init_stat( 'exported_products', '# of Products Exported' );
		$this->stat( 'exported_products', count( $this->exported_inventory_ids ) );

		$this->init_stat( 'exported_assemblies', '# of Assemblies Exported' );
		$this->stat( 'exported_assemblies', count( $this->exported_inventory_ids ) );

		$this->init_stat( 'exported_assembly_items', '# of Assembly Items Exported' );
		$this->stat( 'exported_assembly_items', count( $this->exported_inventory_ids ) );
	}
}