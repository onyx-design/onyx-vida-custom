<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Admin_WC_Product {
	
	public function __construct() {
		 add_action( 'woocommerce_variation_options', array( $this, 'wc_variation_options_ovc_wc_status' ), 10, 3 );
	}

	// Add OVC WC Status to each variation's data in the WC Product Data Meta Box on the WP Admin - Edit Product page
	public function wc_variation_options_ovc_wc_status( $loop, $variation_data, $variation ) {
		//include( 'views/ovc-wc-meta-box-variation-options.php' );

		$ovc_wc_status = get_post_meta( $variation->ID, '_ovc_wc_status', true ); 
		$ovc_wc_status = ( strlen( $ovc_wc_status ) ? strtoupper( $ovc_wc_status ) : 'ERROR! Invalid OVC WC Status' );

		echo '<label style="font-weight:bold;">OVC WC Status: ' . $ovc_wc_status;
		echo '<a class="tips" style="font-weight:normal;" data-tip="Shows whether this variation has been enabled in OVC" href="#">[?]</a>';
		echo '</label>';

	}
}