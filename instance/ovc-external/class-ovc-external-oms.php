<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_External_oms {


	// OVC->OMS FILTER FUNCTIONS

	public static function filter_ovc_oms_sku( OVC_Row $row ) {
		return $row->oms_sku_confirmed ? $row->oms_sku_confirmed : $row->sku ;
	}

	// Filter OVC Product Title (by Style) into OMS Descrip
	public static function filter_ovc_product_title_oms_descrip( OVC_Row $row ) {

		$ovc_title = trim( stripslashes( $row->data( 'st.product_title' ) ) );

		$pkqtytext = '';
		if( 1 < intval( $row->sku_pkqty ) ) {
			$pk_string = ( 'Pairs' == $row->pk_str ? 'pr' : 'pk' );
			$pk_string = ( 'CS' == $row->size ? 'dz' : $pk_string );
			$pkqtytext = " (" . $row->sku_pkqty . $pk_string . ")";
		}

		$oms_title = $ovc_title . $pkqtytext;

		if( 60 < strlen( $oms_title ) ) {
			$oms_title = $ovc_title . trim( $pkqtytext ); // Remove leading space from $pkqtytext

			if( 60 < strlen( $oms_title ) ) {
				
				$pkqtytext = str_replace( " ", "", $pkqtytext ); // Smallest version of $pkqtytext
				$oms_title = substr( $oms_title, 0, ( 59 - strlen( $pkqtytext ) ) ) . "~" . $pkqtytext;
			}
		}

		return $oms_title;
	}

	public static function filter_oms_note1( OVC_Row $row ) {
		$note1_value = array();

		if( $row->data( 'pr.size_ratio' ) ) {
			$note1_value[] = $row->data( 'pr.size_ratio' );
		}

		if( $row->data( 'st.internal_notes' ) ) {
			$note1_value[] = $row->data( 'st.internal_notes' );
		}

		$note1_value = implode( " -- ", $note1_value );
		
		if( strlen( $note1_value ) > 70 ) {
			
			$note1_value = substr( $note1_value, 0, 69 ) . "~";
		}
		
		
		return $note1_value;
	}

	// dev:list //dev:ovc_list
	public static function filter_oms_division( OVC_Row $row ) {
		$division = $row->brand;

		if( strlen( $division ) > 8 ) {

			switch( $division ) {
				case 'Angelina Hosiery':
					$division = 'AnglHsry';
				break;
				case 'Maria Rosa':
					$division = 'MriaRosa';
				break;
				default:
					$division = substr( str_replace( ' ', '', $division ), 0, 8 );
				break;
			}
		}

		return $division;
	}

	// Get OMS image path 1
	public static function filter_oms_image_name_1( OVC_Row $row ) {
		return ( $row->oms_img_1 ? 'O:\\omsimg\\' . $row->oms_img_1 . '.jpg' : '' );
	}

	// Get OMS image path 2
	public static function filter_oms_image_name_2( OVC_Row $row ) {
		return ( $row->oms_img_2 ? 'O:\\omsimg\\' . $row->oms_img_2 . '.jpg' : '' );
	}

	// Build list of synced marketplaces
	public static function filter_ovc_marketplaces_oms_notes_line_5( OVC_Row $row ) {
		$marketplaces_synced = array();

		$marketplace_fields = array( 'pr.sync_wc' => 'WC', 'pr.sync_walmart' => 'WA', 'pr.sync_zulily' => 'ZU', 'pr.sync_groupon' => 'GR', 'pr.sync_amazon' => 'AZ' );

		foreach( $marketplace_fields as $field => $code ) {
			if( $row->data( $field ) ) {
				$marketplaces_synced[] = $code;
			}
		}

		return implode( ', ', $marketplaces_synced );
	}

	// Custom OVC Tracking information into OMS Notes Line 7
	public static function filter_ovc_tracking_oms_notes_line_7( OVC_Row $row ) {
		return OVCOP::get_running_ovcop_id();
	}

	// Filter OVC data into OMS Unit
	public static function filter_oms_unit( OVC_Row $row ) {
		return ( 'CS' == $row->size ? 'CS' : 'PK' );
	}

	public static function filter_oms_default_unit( OVC_Row $row ) {
		return ( 'CS' == $row->size ? 'CS' : 'PC' );
	}
	
}