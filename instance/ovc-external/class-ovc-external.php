<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_External {
	
	/**
	 * Filter for the External Product Name
	 * 
	 * Returns "[brand] [title] [pack_quantity_string]"
	 * 
	 * @param 	OVC_Row 	$row
	 * 
	 * @return 	string
	 */
	public static function filter_external_product_name( OVC_Row $row, $siblings = null ) {

		$brand = $row->data( 'pa.brand' );

		$ovc_title = stripslashes( $row->data( 'st.product_title' ) );

		$external_product_name = "{$row->brand} {$ovc_title}";

		// If siblings isn't set, we're getting the variant title
		if( is_null( $siblings ) ) {

			$pack_qty_str = self::filter_external_product_variant_pack_quantity( $row );
		// If siblings is set, we're getting the parent title
		} else {

			$pack_qty_str = self::filter_external_product_parent_pack_quantity( $row, $siblings );
		}

		return trim( preg_replace( '/\s+/', ' ', "{$external_product_name} {$pack_qty_str}" ) );
	}

	/**
	 * Filter for the External Product Variant Pack Quantity
	 * 
	 * @param 	OVC_Row 	$row
	 * 
	 * @return 	string
	 */
	public static function filter_external_product_variant_pack_quantity( OVC_Row $row ) {

		$pack_qty_str = '';

		if( 1 < intval( $row->sku_pkqty ) ) {

			$pack_qty_str = "({$row->sku_pkqty}-{$row->pk_str})";
		}

		return trim( str_replace( "  ", " ", "{$pack_qty_str}" ) );
	}

	/**
	 * Filter for the External Product Parent Pack Quantity Range
	 * 
	 * @param 	OVC_Row 	$row
	 * 
	 * @return 	string
	 */
	public static function filter_external_product_parent_pack_quantity( OVC_Row $row, $siblings = array() ) {

		// Get all of the sibling products to $row not including the case product
		if( empty( $siblings ) ) {

			$results = new OVC_Results;

			$siblings = $row->get_parent()->get_children( 'ovc_products', 'raw', $results, array( 'var' => '::[size]', 'op' => '!=', 'value' => 'CS' ) );
		} 

		// $pack_qty_str defaults to blank
		$pack_qty_str = '';
		// $pk_str defaults to the "Pack"
		$pk_str = 'Pack';

		/*
		 * This will only happen if there is only a case product
		 *
		 * If there aren't any siblings, just return the pkqty of the primary case
		 */
		if( ( 0 === count( $siblings ) ) || !is_array( $siblings ) ) {

			$case_product = OVCDB()->get_row_by_valid_id( FS( 'ovc_products' ), 'sku', $row->case_sku );

			$pack_qty_str = "({$case_product->sku_pkqty}-{$case_product->pk_str})";
		} else {

			/*
			 * If there aren't multiple pack strings (Pairs,Pack,...), use the pk_str,
			 * else we're using "Pack" above
			 */
			if( 1 === count( array_unique( array_column( $siblings, 'pr.pk_str' ) ) ) ) {
				 $pk_str = current( array_unique( array_column( $siblings, 'pr.pk_str' ) ) );
			}

			/*
			 * If there are siblings, and they have the same pack quantity, return that pack quantity
			 */
			if( 1 === count( array_unique( array_column( $siblings, 'pr.sku_pkqty' ) ) ) ) {
				$pack_qty_str = '(' . current( array_unique( array_column( $siblings, 'pr.sku_pkqty' ) ) ) . "-{$pk_str})";
			}
			/*
			 * This is the instance where there are multiple pack quantities synced to WC
			 *
			 * If there are siblings, but they do not have the same pack quantity, 
			 * let's return the range of pack quantities
			 */
			elseif( 1 !== count( array_unique( array_column( $siblings, 'pr.sku_pkqty' ) ) ) ) {

				$sibling_pkqties = array_column( $siblings, 'pr.sku_pkqty' );

				$pack_qty_str = '(' . $siblings[ array_search( min( $sibling_pkqties ), $sibling_pkqties ) ]['pr.sku_pkqty'] . '-' . $siblings[ array_search( max( $sibling_pkqties ), $sibling_pkqties ) ]['pr.sku_pkqty'] . " {$pk_str})";
			}
		}

		return trim( str_replace( "  ", " ", "{$pack_qty_str}" ) );
	}

	/**
	 * Filter for the External product Name with Parent Sku
	 * Return "[product_name], [parent_sku]"
	 * 
	 * Used for:
	 * WooCommerce
	 * 
	 * @param 	OVC_Row 	$row
	 * 
	 * @return 	string
	 */
	public static function filter_external_product_name_with_parent_sku( OVC_Row $row, $siblings = null ) {

		$parent_sku = $row->data( 'pa.parent_sku' );

		$external_product_name = self::filter_external_product_name( $row, $siblings );

		return trim( str_replace( "  ", " ", "{$external_product_name}, #{$parent_sku}" ) );
	}

	/**
	 * Filter for the Eternal Product Image with Color:
	 * [product_name][separator] [color]
	 * 
	 * Used for:
	 * 
	 * @param 	OVC_Row 	$row
	 * @param 	string 		$separator
	 * 
	 * @return 	string
	 */
	public static function filter_external_product_name_with_color( OVC_Row $row, $siblings = null, $separator = '' ) {

		// Special formatting for color in product name
		$color = preg_replace( '/( \([ 0-9a-zA-Z]+\))$/', '', $row->get_best_color_name() );

		$external_product_name = self::filter_external_product_name( $row, $siblings );

		return trim( str_replace( "  ", " ", "{$external_product_name}{$separator} {$color}" ) );
	}

	/*
	 * Used for: 
	 * Shopify Variant (title)
	 */
	public static function filter_external_product_name_with_color_size( OVC_Row $row, $siblings = null ) {

		$external_product_name_with_color = self::filter_external_product_name_with_color( $row, $siblings );

		return trim( str_replace( "  ", " ", "{$external_product_name_with_color}, {$row->get_full_size_name()}" ) );

	}

	/*
	 * Used for: 
	 */
	public static function filter_external_product_name_with_style_size( OVC_Row $row, $siblings = null ) {

		$external_product_name = self::filter_external_product_name( $row, $siblings );

		return trim( str_replace( "  ", " ", "{$external_product_name}, {$row->data('pr.sku_style')}_{$row->data('pr.size')}" ) );
	}

	/**
	 * Filter for getting the bra size of the current product
	 * If product is not a bra, bra size will return empty string
	 * 
	 * @param 	OVC_Row 	$row 
	 * @param 	string $type 
	 * @return type
	 */
	public static function get_bra_size( OVC_Row $row, $type = 'band' ) {

		if( 'bras' !== self::filter_external_product_type( $row ) ) {

			return '';
		}

		$size = $row->data( 'pr.size' );

		switch( $type ) {

			case 'band':
				return preg_replace( '/[^0-9,.]/', '', $size );
				break;

			case 'cup':
				return preg_replace( '/[0-9]+/', '', $size );
				break;

			default:
				return '';
				break;
		}
	}

	/**
	 * Filter for getting the band size of the bra
	 * 
	 * @param 	OVC_Row 	$row
	 * 
	 * @return 	string
	 */
	public static function filter_band_size( OVC_Row $row ) {

		return self::get_bra_size( $row, 'band' );
	}

	/**
	 * Filter for getting the cup size of the bra
	 * 
	 * @param 	OVC_Row 	$row
	 * 
	 * @return 	string
	 */
	public static function filter_cup_size( OVC_Row $row ) {

		return self::get_bra_size( $row, 'cup' );
	}

	/**
	 * Get the Product Type
	 * 
	 * @param 	OVC_Row 	$row
	 * 
	 * @return 	string
	 */
	public static function filter_external_product_type( OVC_Row $row ) {

		$product_term = get_term_by( 'name', $row->data( 'st.wc_product_category' ), 'product_cat' );

		if( $product_term ) {

			if( $parent_id = $product_term->parent ) {

				$parent_term = get_term_by( 'id', $parent_id, 'product_cat' );

				return $parent_term->slug;
			}

			return $product_term->slug;
		}

		return '';
	}

	/*
	 * SHOPIFY FUNCTIONS
	 */

	public static function filter_shopify_parent_product_name( OVC_Row $row ) {

		$product_title = stripslashes( $row->ovc_parent()->data( 'pa.product_title' ) );

		$pack_qty_str = self::filter_external_product_parent_pack_quantity( $row, $row->get_variants() );

		return "{$product_title} {$pack_qty_str}";
	}

	/**
	 * Filter for the Shopify Variant Product Name
	 * 
	 * @param 	OVC_Row 	$row
	 * 
	 * @return 	string
	 */
	public static function filter_shopify_variant_product_name( OVC_Row $row ) {

		return self::filter_external_product_name_with_color_size( $row );
	}

	/**
	 * Filter for getting the available quantity of the curet
	 * 
	 * @param 	OVC_Row 	$row
	 * 
	 * @return 	strin
	 */
	public static function filter_shopify_avail_qty( OVC_Row $row ) {

		return floor( ( $row->data( 'pr.avail_qty' ) - $row->data( 'pr.reserve_qty' ) ) );
	}

	public static function filer_ovc_shopify_price( OVC_ROW $row ) {

		return !empty( $row->data( 'pr.shopify_sale_price' ) ) && ( $row->data( 'pr.shopify_sale_price' ) < $row->data( 'pr.price_retail' ) ) ? $row->data( 'pr.shopify_sale_price' ) : $row->data( 'pr.price_retail' );
	}

	public static function filter_ovc_shopify_compare_at_price( OVC_ROW $row ) {

		return !empty( $row->data( 'pr.shopify_sale_price' ) ) && ( $row->data( 'pr.shopify_sale_price' ) < $row->data( 'pr.price_retail' ) ) ? $row->data( 'pr.price_retail' ) : null;
	}

	/*
	 * AMAZON FUNCTIONS
	 */

	/*
	 * Used for: 
	 * Amazon (item_name)

	 * Filter for the Amazon Product Name
	 * 
	 * @param 	OVC_Row 	$row
	 * 
	 * @return 	string
	 */
	public static function filter_amazon_product_name( OVC_Row $row, $siblings = null ) {

		$ovc_title = stripslashes( $row->data( 'st.product_title' ) );

		$department = '';
		switch( $row->data( 'pr.department' ) ) {

			case 'Women':
				$department = "Women's";
				break;
			case 'Men':
				$department = "Men's";
				break;
			case 'Boys':
				$department = "Boy's";
				break;
			case 'Girls':
				$department = "Girl's";
				break;
		}

		$external_product_name = "{$row->brand} {$department} {$ovc_title}";

		// If siblings isn't set, we're getting the variant title
		if( is_null( $siblings ) ) {

			$pack_qty_str = self::filter_external_product_variant_pack_quantity( $row );
		// If siblings is set, we're getting the parent title
		} else {

			$pack_qty_str = self::filter_external_product_parent_pack_quantity( $row, $siblings );
		}

		return trim( preg_replace( '/\s+/', ' ', "{$external_product_name} {$pack_qty_str}, {$row->data('pr.sku_style')}_{$row->data('pr.size')}" ) );
	}

	public static function filter_feed_product_type_amazon( OVC_Row $row ) {

		$feed_product_type = $row->data( 'st.amazon_feed_product_type' );

		return $feed_product_type;
	}

	public static function filter_style_name_amazon( OVC_Row $row ) {

		$style_name = $row->data( 'st.amazon_style_name' );

		return $style_name;
	}

	public static function filter_pattern_type_amazon( OVC_Row $row ) {

		$pattern_type = $row->data( 'st.amazon_pattern_type' );

		return $pattern_type;
	}

	/**
	 * Filter for getting the Amazon Color Name
	 * 
	 * @param 	OVC_Row $row
	 * 
	 * @return 	string
	 */
	public static function filter_color_name_amazon( OVC_Row $row ) {

		$color = $row->get_best_color_name();
		$size = $row->sku_pkqty . "-" . $row->pk_str;

		$color_name = $size . " " . $color;
		return $color_name;
	}

	/**
	 * Filter for getting the Amazon Material
	 * 
	 * @param 	OVC_Row 	$row
	 * 
	 * @return 	string
	 */
	public static function filter_material_string_amazon( OVC_Row $row ) {

		$material_content_array = $row->get_material_content_array();
		$material_content_strings = array();

		foreach( $material_content_array as $material_name => $material_percent ) {

			$material_content_strings[] = "{$material_percent}% {$material_name}";
		}

		return implode( ', ', $material_content_strings );
	}

	/**
	 * Filter for getting the Amazon Keywords
	 * 
	 * @param 	OVC_Row 	$row
	 * @param 	int 		$limit
	 * 
	 * @return 	string
	 */
	public static function filter_keywords_amazon( OVC_Row $row, $limit = 250 ) {

		$keywords = $row->data( 'st.keywords' );

		// Make sure this doesn't have unexpected white space
		$keywords = preg_replace( '/([\s\n\r]+)/', ' ', $keywords );

		return substr( $keywords, 0, strpos( wordwrap( $keywords, $limit ), "\n" ) );
	}

	/**
	 * dev:todo Figure this out
	 * 
	 * @param 	OVC_Row 	$row
	 * 
	 * @return 	string
	 */
	public static function filter_department_name_amazon( OVC_Row $row ) {

		$department = $row->data( 'pr.department' );

		switch( $department ) {
		
			case 'Women':				
				return 'womens';
				break;

			case 'Men':				
				return 'mens';
				break;

			case 'Boys':				
				return 'boys';
				break;

			case 'Girls':				
				return 'girls';
				break;

			default:
				return '';
				break;
		}
	}

	/**
	 * Filter for getting the Amazon Size Name
	 * 
	 * @param 	OVC_Row 	$row
	 * 
	 * @return 	string
	 */
	public static function filter_size_name_amazon( OVC_Row $row ) {

		$amazon_size_name = $row->data( 'st.amazon_size_name' );
		
		return $amazon_size_name ?: $row->get_full_size_name();
	}

	/**
	 * dev:todo Map the sizes
	 * 
	 * @param 	OVC_Row 	$row
	 * 
	 * @return 	string
	 */
	public static function filter_size_map_amazon( OVC_Row $row ) {

		if( !( $product_size = self::get_bra_size( $row, 'cup' ) ) ) {
			$product_size = $row->data( 'pr.size' );
		}

		$size_mapping = array(
			'CS'		=> '',
			'XS'		=> 'X-Small',
			'S'			=> 'Small',
			'M'			=> 'Medium',
			'L'			=> 'Large',
			'XL'		=> 'X-Large',
			'2XL'		=> 'XX-Large',
			'3XL'		=> 'XXX-Large',
			'4XL'		=> 'XXXX-Large',
			'1XB'		=> 'X-Large',
			'2XB'		=> 'XX-Large',
			'3XB'		=> 'XXX-Large',
			'4XB'		=> 'XXXX-Large',
			'2T'		=> 'Small',
			'2T-3T'		=> 'Small',
			'3T'		=> 'Medium',
			'3T-4T'		=> 'Medium',
			'4T'		=> 'Medium',
			'4T-5T'		=> 'Large',
			'5T'		=> 'Large',
			'XS-S'		=> 'X-Small',
			'S-M'		=> 'Small',
			'S-L'		=> 'Medium',
			'M-L'		=> 'Medium',
			'L-XL'		=> 'Large',
			'XL-2X'		=> 'X-Large',
			'Q'			=> 'Large',
			'2Q'		=> 'XX-Large',
			'3Q'		=> 'XXX-Large',
			'PS'		=> 'X-Large',
			'TW'		=> '',
			'OS'		=> 'Medium',
			'OSA'		=> 'Medium',
			'OSW'		=> 'Medium',
			'OSG'		=> 'Small',
			'OSM'		=> 'Medium',
			'OSB'		=> 'Small',
			'OSK'		=> 'Small',
			'OSKT'		=> 'Medium',
			'OSQ'		=> 'Large',
			'OSP'		=> 'X-Large',
			'US'		=> 'Small',
			'UM'		=> 'Medium',
			'UL'		=> 'Large',
			'UX'		=> 'X-Large',
			'U2X'		=> 'XX-Large',
			'U3X'		=> 'XXX-Large',
			'U4X'		=> 'XXXX-Large',
			'U5X'		=> 'XXXXX-Large',
			'0-6M'		=> 'XXX-Small',
			'0-12'		=> 'XX-Small',
			'0-9'		=> 'XX-Small',
			'6-12M'		=> 'X-Small',
			'12-24'		=> 'Small',
			'9-18'		=> 'Small',
			'1-3'		=> 'Small',
			'2-4'		=> '',
			'4-6'		=> 'Medium',
			'6-8'		=> 'Large',
			'7-10'		=> '',
			'8-10'		=> 'X-Large',
			'8-12'		=> '',
			'10-12'		=> '',
			'11-14'		=> '',
			'teen'		=> '',
			'3-4'		=> '',
			'5-6'		=> '',
			'7-8'		=> '',
			'9-10'		=> '',
			'11-12'		=> '',
			'13-14'		=> '',
			'9-11'		=> 'Medium',
			'10-13'		=> 'Large',
			'5-65'		=> '',
			'7-85'		=> '',
			'4-5'		=> 'X-Small',
			'6-7'		=> 'Small',
			'8-9'		=> 'Medium',
			'10-11'		=> 'Large',
			'12-13'		=> 'X-Large',
			'14-16'		=> 'XX-Large',
			'1x'		=> 'X-Large',
			'1x-2x'		=> 'XX-Large',
			'3x-4x'		=> 'XXX-Large',
			'A'			=> 'Small',
			'B'			=> 'Medium',
			'C'			=> 'Large',
			'D'			=> 'X-Large',
			'28A'		=> 'Small',
			'30A'		=> 'Small',
			'32A'		=> 'Small',
			'34A'		=> 'Small',
			'36A'		=> 'Small',
			'38A'		=> 'Small',
			'32B'		=> 'Medium',
			'34B'		=> 'Medium',
			'36B'		=> 'Medium',
			'38B'		=> 'Medium',
			'40B'		=> 'Medium',
			'42B'		=> 'Medium',
			'44B'		=> 'Medium',
			'32C'		=> 'Large',
			'34C'		=> 'Large',
			'36C'		=> 'Large',
			'38C'		=> 'Large',
			'40C'		=> 'Large',
			'42C'		=> 'Large',
			'44C'		=> 'Large',
			'34D'		=> 'X-Large',
			'36D'		=> 'X-Large',
			'38D'		=> 'X-Large',
			'40D'		=> 'X-Large',
			'42D'		=> 'X-Large',
			'44D'		=> 'X-Large',
			'34DD'		=> 'XX-Large',
			'36DD'		=> 'XX-Large',
			'38DD'		=> 'XX-Large',
			'40DD'		=> 'XX-Large',
			'42DD'		=> 'XX-Large',
			'44DD'		=> 'XX-Large',
			'46DD'		=> 'XX-Large',
			'34DDD'		=> 'XXX-Large',
			'36DDD'		=> 'XXX-Large',
			'38DDD'		=> 'XXX-Large',
			'40DDD'		=> 'XXX-Large',
			'42DDD'		=> 'XXX-Large',
			'44DDD'		=> 'XXX-Large',
			'46DDD'		=> 'XXX-Large',
			'32B-S'		=> 'Small',
			'34B-M'		=> 'Medium',
			'34C-M'		=> 'Medium',
			'36B-L'		=> 'Large',
			'36C-L'		=> 'Large',
			'38B-XL'	=> 'X-Large',
			'38C-XL'	=> 'X-Large',
			'40C-XL'	=> 'X-Large',
		);

		// If we have the size mapped, return it
		if( array_key_exists( $product_size, $size_mapping ) ) {

			return $size_mapping[ $product_size ];
		}

		// $size_mapping = array(
		// 	'XXXXX-Small'	=> array(),
		// 	'XXXX-Small'	=> array(),
		// 	'XXX-Small'		=> array(
		//		'0-6M', 
		//	),
		// 	'XX-Small'		=> array(
		//		'0-12','0-9',
		//	),
		// 	'X-Small'		=> array(
		//		'XS','XS-S','6-12M','4-5',
		// 	),
		// 	'Small'			=> array(
		//		'S','2T','2T-3T','S-M','OSG','OSB','OSK','US','12-24','9-18','1-3','6-7','A','28A','30A','32A','34A','36A','38A','32B-S',
		// 	),
		// 	'Medium'		=> array(
		//		'M','3T','3T-4T','4T','S-L','M-L','OS','OSA','OSW','OSM','OSKT','UM','4-6','9-11','8-9','B','32B','34B','36B','38B','40B','42B','44B','34B-M','34C-M',
		// 	),
		// 	'Large'			=> array(
		//		'L','4T-5T','5T','L-XL','Q','OSQ','UL','6-8','10-13','10-11','C','32C','34C','36C','38C','40C','42C','44C','36B-L','36C-L',
		// 	),
		// 	'X-Large'		=> array(
		//		'XL','1XB','XL-2X','PS','OSP','UX','8-10','12-13','1x','D','34D','36D','38D','40D','42D','44D','38B-XL','38C-XL','40C-XL',
		// 	),
		// 	'XX-Large'		=> array(
		//		'2XL','2XB','2Q','U2X','14-16','1x-2x','34DD','36DD','38DD','40DD','42DD','44DD','46DD',
		// 	),
		// 	'XXX-Large'		=> array(
		//		'3XL','3XB','3Q','U3X','3x-4x','34DDD','36DDD','38DDD','40DDD','42DDD','44DDD',
		// 	),
		// 	'XXXX-Large'	=> array(
		//		'4XL','4XB','U4X',
		// 	),
		// 	'XXXXX-Large'	=> array(
		//		'U5X',
		//	)
		// );

		// foreach( $size_mapping as $amazon_size => $vida_sizes ) {

		// 	if( in_array( $product_size, $vida_sizes ) ) {

		// 		return $amazon_size;
		// 	}
		// }

		// Return an empty string is we don't recognize the product size
		return '';
	}

	public static function filter_color_map_amazon( OVC_Row $row ) {

		$color_map = $row->data( 'st.amazon_color_map' );

		return !empty( $color_map ) ? $color_map : 'multicoloured';
	}

	public static function filter_underwire_type_amazon( OVC_Row $row ) {

		$style_underwire_type = 'Wire Free';

		switch( $style_underwire_type ) {

			case 'Underwire':
				$style_underwire_type = 'Underwire';
				break;
			case 'Wireless':
				$style_underwire_type = 'Wire Free';
				break;
		}

		return $style_underwire_type;
	}

	public static function filter_strap_type_amazon( OVC_Row $row ) {

		$strap_type = $row->data( 'st.amazon_strap_type' );

		return $strap_type;
	}

	public static function filter_closure_type_amazon( OVC_Row $row ) {

		$closure_type = $row->data( 'st.amazon_closure_type' );

		return $closure_type;
	}

	public static function filter_top_style_amazon( OVC_Row $row ) {

		$top_style = $row->data( 'st.amazon_top_style' );

		return $top_style;
	}

	public static function filter_bottom_style_amazon( OVC_Row $fow ) {

		$bottom_style = $fow->data( 'st.amazon_bottom_style' );

		return $bottom_style;
	}

	/**
	 * Filter Function to get an Amazon Image URL:
	 * Main Image (main_image_url) 		- img_main
	 * Other Image 1 (other_image_url1) - img_gallery_ids[0]
	 * Other Image 2 (other_image_url2) - img_gallery_ids[1]
	 * Other Image 3 (other_image_url3) - img_gallery_ids[2]
	 * Other Image 4 (other_image_url4) - img_gallery_ids[3]
	 * Other Image 5 (other_image_url5) - img_amazon
	 * Other Image 6 (other_image_url6) - img_sizeguide
	 * Swatch Image (swatch_image_url) 	- img_swatch
	 * 
	 * If there aren't enough gallery image, 
	 * the images will be moved down and the higher 
	 * numbered images will be left blank, ex:
	 * Other Image 1 (other_image_url1) - img_gallery_ids[0]
	 * Other Image 2 (other_image_url2) - img_gallery_ids[1]
	 * Other Image 3 (other_image_url3) - img_amazon
	 * Other Image 4 (other_image_url4) - img_sizeguide
	 * Other Image 5 (other_image_url5) - 
	 * Other Image 6 (other_image_url6) - 
	 * 
	 * @param 	OVC_Row 	$row
	 * 
	 * @return 	string
	 */
	public static function filter_image_url_amazon( OVC_Row $row ) {

		$img_gallery_ids = explode( ',', $row->data( 'img.img_gallery_ids' ) );

		return $row->data( 'img.img_case' );
	}

	public static function get_other_image_amazon( $row, $number ) {

		$img_gallery_ids = explode( ',', $row->data( 'img.img_gallery_ids' ) );

		$img_ids = array_fill( 0, 5, '' );
		$img_ids[4] = $row->data( 'img.img_amazon' );
		$img_ids[5] = $row->data( 'img.img_sizeguide' );

		foreach( $img_ids as $key => $img_id ) {

			if( $img_id ) {
				continue;
			}

			$img_ids[ $key ] = isset( $img_gallery_ids[ $key ] ) ? $img_gallery_ids[ $key ] : '';
		}

		// Remove empty values and reset keys
		$img_ids = array_values( array_filter( $img_ids ) );

		return isset( $img_ids[ $number - 1 ] ) ? wp_get_attachment_url( intval( $img_ids[ $number - 1 ] ) ) : '';
	}

	public static function filter_main_image_amazon( OVC_Row $row ) {

		$main_img = $row->data( 'img.img_main' );

		return wp_get_attachment_url( intval( $main_img ) );
	}

	public static function filter_other_image_1_amazon( OVC_Row $row ) {

		return self::get_other_image_amazon( $row, 1 );
	}

	public static function filter_other_image_2_amazon( OVC_Row $row ) {

		return self::get_other_image_amazon( $row, 2 );
	}

	public static function filter_other_image_3_amazon( OVC_Row $row ) {

		return self::get_other_image_amazon( $row, 3 );
	}

	public static function filter_other_image_4_amazon( OVC_Row $row ) {

		return self::get_other_image_amazon( $row, 4 );
	}

	public static function filter_other_image_5_amazon( OVC_Row $row ) {

		return self::get_other_image_amazon( $row, 5 );
	}

	public static function filter_other_image_6_amazon( OVC_Row $row ) {

		return self::get_other_image_amazon( $row, 6 );
	}

	public static function filter_other_image_7_amazon( OVC_Row $row ) {

		return self::get_other_image_amazon( $row, 7 );
	}

	public static function filter_other_image_8_amazon( OVC_Row $row ) {

		return self::get_other_image_amazon( $row, 8 );
	}

	public static function filter_swatch_image_amazon( OVC_Row $row ) {

		$swatch_img = $row->data( 'img.img_swatch' );

		return wp_get_attachment_url( intval( $swatch_img ) );
	}

	public static function filter_amazon_bullet_point_1( OVC_Row $row ) {
		
		return $row->data( 'st.amazon_bullet_point_1' ) ?: ( $row->data( 'st.fast_facts1' ) ?: $row->data( 'pa.fast_facts1' ) );
	}

	public static function filter_amazon_bullet_point_2( OVC_Row $row ) {
		
		return $row->data( 'st.amazon_bullet_point_2' ) ?: ( $row->data( 'st.fast_facts2' ) ?: $row->data( 'pa.fast_facts2' ) );
	}

	public static function filter_amazon_bullet_point_3( OVC_Row $row ) {
		
		return $row->data( 'st.amazon_bullet_point_3' ) ?: ( $row->data( 'st.fast_facts3' ) ?: $row->data( 'pa.fast_facts3' ) );
	}

	public static function filter_amazon_bullet_point_4( OVC_Row $row ) {
		
		return $row->data( 'st.amazon_bullet_point_4' ) ?: ( $row->data( 'st.fast_facts4' ) ?: $row->data( 'pa.fast_facts4' ) );
	}

	public static function filter_amazon_bullet_point_5( OVC_Row $row ) {
		
		return $row->data( 'st.amazon_bullet_point_5' ) ?: $row->data( 'st.care' );
	}

	public static function filter_amazon_product_description( OVC_Row $row ) {
		
		return $row->data( 'st.amazon_product_description' ) ?: $row->data( 'pa.description' );
	}

	public static function filter_amazon_fabric_type( OVC_Row $row ) {

		return $row->data( 'st.amazon_fabric_type' ) ?: self::filter_material_string_amazon( $row );
	}

	/*
	 * WALMART FUNCTIONS
	 */

	/**
	 * Filter for the Walmart Product Name
	 * 
	 * @param 	OVC_Row 	$row
	 * 
	 * @return 	string
	 */
	public static function filter_walmart_product_name( OVC_Row $row ) {

		$results = new OVC_Results;

		// Filter by products syncing to Walmart
		$siblings = $row->get_parent()
			->get_children(
				'ovc_products',
				'raw',
				$results,
				array(
					'conditions' => array(
						array(
							'var' => '::[sync_walmart]',
							'value' => '1'
						),
						array(
							'var' => '::[size]',
							'op' => '!=',
							'value' => 'CS'
						)
					),
					'relation' => 'AND'
				)
			);

		return self::filter_external_product_name( $row, $siblings );
	}

	public static function filter_walmart_main_image( OVC_Row $row ) {

		$main_img = OVCDB()->get_row_by_valid_id( 'ovc_images', 'post_id', $row->img_main );

		return $main_img->filter_ovc_image_url() ? : wp_get_attachment_url( $product->img_main );
	}

	/**
	 * Filter to return whether or not we need to send a SKU change
	 * 
	 * @param 	OVC_Row 	$row
	 * 
	 * @return 	string 		$sku_change
	 */
	public static function filter_walmart_sku_update( OVC_Row $row, $feed_type = '' ) {

		$sku_change = 'No';

		if( 'CREATE' == $row->data('wa.manual_action') ) {

			$sku_change = 'No';
		}
		elseif( $row->data( 'wa.override_sku' ) ) {

			$sku_change = 'Yes';
		} elseif( 'UPDATE_SKU' === $row->data( 'wa.manual_action' ) ) {

			$sku_change = 'Yes';
		} elseif( $row->data( 'pr.sku' ) !== $row->data( 'wa.sku' ) ) {

			$sku_change = 'Yes';
		}

		return $sku_change;
	}

	/**
	 * Filter to return whether or not we need to send a UPC change
	 * 
	 * @param 	OVC_Row 	$row
	 * 
	 * @return 	string 		$upc_change
	 */
	public static function filter_walmart_upc_update( OVC_Row $row, $feed_type = '' ) {

		$upc_change = 'No';

		if( 'CREATE' == $row->data('wa.manual_action') ) {

			$upc_change = 'No';
		}
		elseif( $row->data( 'wa.override_upc' ) ) {

			$upc_change = 'Yes';
		} elseif( 'UPDATE_UPC' === $row->data( 'wa.manual_action' ) ) {

			$upc_change = 'Yes';
		} elseif( $row->data( 'pr.upc_code' ) !== $row->data( 'wa.upc' ) ) {

			$upc_change = 'Yes';
		}

		return $upc_change;
	}

	/**
	 * Filter to get the Walmart SKU
	 * 
	 * @param 	OVC_Row 	$row
	 * 
	 * @return 	string 		$wa_sku
	 */
	public static function filter_walmart_sku( OVC_Row $row ) {

		$wa_sku = $row->data( 'wa.override_sku' );

		if( !$wa_sku ) {
			$wa_sku = $row->data( 'wa.sku' );
		}

		if( !$wa_sku ) {
			$wa_sku = $row->data( 'pr.sku' );
		}

		return $wa_sku;
	}

	/**
	 * Filter to get the Walmart UPC
	 * 
	 * @param 	OVC_Row 	$row
	 * 
	 * @return 	string 		$wa_upc
	 */
	public static function filter_walmart_upc( OVC_Row $row ) {

		$wa_upc = $row->data( 'wa.override_upc' );

		if( !$wa_upc ) {
			$wa_upc = $row->data( 'wa.upc' );
		}

		if( !$wa_upc ) {
			$wa_upc = $row->data( 'pr.upc_code' );
		}

		return $wa_upc;
	}

	/*
	 * WOOCOMMERCE FUNCTIONS
	 */

	/**
	 * Filter for the WC Parent Product Name
	 * 
	 * @param 	OVC_Row 	$row
	 * 
	 * @return 	string
	 */
	public static function filter_wc_parent_product_name( OVC_Row $row ) {

		$results = new OVC_Results;

		// Filter by products syncing to WC
		$siblings = $row->get_parent()->get_children( 'ovc_products', 'raw', $results, array( 'conditions' => array( array( 'var' => '::[sync_wc]', 'value' => '1' ), array( 'var' => '::[size]', 'op' => '!=', 'value' => 'CS' ) ), 'relation' => 'AND' ) );

		return self::filter_external_product_name_with_parent_sku( $row, $siblings );
	}

	/*
	 * ZULILY FUNCTIONS
	 */

	/**
	 * Filter for the Zulily Product Name
	 * 
	 * @param 	OVC_Row 	$row
	 * 
	 * @return 	string
	 */
	public static function filter_zulily_product_name( OVC_Row $row ) {

		return self::filter_external_product_name( $row );
	}

	/*
	 * GROUPON FUNCTIONS
	 */

	/**
	 * Filter for the Groupon Product Name
	 * 
	 * @param 	OVC_Row 	$row
	 * 
	 * @return 	string
	 */
	public static function filter_groupon_product_name( OVC_Row $row ) {

		return self::filter_external_product_name( $row );
	}

	/**
	 * Filter for the Groupon Retail Link
	 * 
	 * @param 	OVC_Row 	$row
	 * 
	 * @return 	string
	 */
	public static function filter_retail_link_groupon( OVC_Row $row ) {

		return $row->asin ? "http://www.amazon.com/dp/" . $row->asin : '';
	}

	/*
	 * DROPSHIP FUNCTIONS
	 */

	/**
	 * Filter for the Dropship Product Name
	 * 
	 * @param 	OVC_Row 	$row
	 * 
	 * @return 	string
	 */
	public static function filter_dropship_product_name( OVC_Row $row ) {

		return self::filter_external_product_name( $row );
	}

	/*
	 * OVC INVENTORY FUNCTIONS
	 */

	public static function filter_potential_qty_instock( OVC_Row $row ) {

		return $row->get_potential_stock( false, true );
	}
}