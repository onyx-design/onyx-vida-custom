<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Table_ovcop_files extends OVC_Table {

	public $field_set = 'ovcop_files';
	public $ovcdt_set = 'ovcop_files';
	
	public $pre_data_col = 'Actions';


	public function get_pre_data_column_header_html() { // dev:generalize better!
		return '<th class="actions" data-col="actions">Actions</th>';
	}

	public function get_pre_data_column_row_html( $row = array() ) {
		$pre_data_row_html = '';

		$file = new OVC_File( $row['file.ID'] );

		// Add 'Actions' column before table data
		$pre_data_row_html = '<td class="actions">';

		// View file button
		if( $file->exists() && in_array( $file->extension(), array( 'txt', 'log', 'xml' ) ) ) {

			$pre_data_row_html .= '<i class="fa fa-eye can-view" title="View file in browser."><a href="' . $file->url() . '" target="_blank"></a></i>';
		}
		else {
			$pre_data_row_html .= '<i class="fa fa-eye" title="File not found or file type cannot be shown in browser."></i>';
		}

		// Download file button
		if( $file->exists() ) {

			$pre_data_row_html .= '<i class="fa fa-download can-download" title="Download file."><a href="' . $file->url() . '" download></a></i>';
		}
		else {
			$pre_data_row_html .= '<i class="fa fa-download" title="File not found."></i>';
		}
		

		$pre_data_row_html .= '</td>';

		return $pre_data_row_html;
	}
}