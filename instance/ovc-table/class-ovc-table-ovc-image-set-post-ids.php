<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Table_ovc_image_set_post_ids extends OVC_Table {

	public $field_set = 'ovc_image_set_post_ids';
	public $ovcdt_set = 'ovc_image_set_post_ids';
	
	public $pre_data_col = 'Actions';

	public $wait_to_save = false;
}