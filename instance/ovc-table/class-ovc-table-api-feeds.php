<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Table_api_feeds extends OVC_Table {

	public $field_set = 'api_feeds';
	public $ovcdt_set = 'api_feeds';
	
	public $pre_data_col = 'Actions';

	
	public function get_pre_data_column_header_html() { // dev:generalize better!
		return '<th class="actions" data-col="actions">Actions</th>';
	}

	public function get_pre_data_column_row_html( $row = array() ) {
		$pre_data_row_html = '';

		// Add 'Actions' column before table data
		$pre_data_row_html = '<td class="actions">';

		$pre_data_row_html .= '<i class="fa fa-refresh api-refresh" title="Refresh feed data."></i>';

		// Errors icon
		$has_error = $row['feed.ingestion_errors'] ? ' has-error' : '';
		$has_error_title = $has_error ? "View feed ingestion errors." : "No feed ingestion errors.";
		$pre_data_row_html .= '<i class="fa fa-warning action-show_feed_errors' . $has_error . '" title="' . $has_error_title . '"></i>';

		// View Feed File icon
		$feed_file = new OVC_File( $row['feed.file_id'] );

		if( $feed_file->exists() ) {
			$pre_data_row_html .= '<i class="fa fa-eye can-view" title="View file in browser."><a href="' . $feed_file->url() . '" target="_blank"></a></i>';
		}
		else {
			$pre_data_row_html .= '<i class="fa fa-eye" title="File not found."></i>';
		}

		// Download file button
		if( $feed_file->exists() ) {

			$pre_data_row_html .= '<i class="fa fa-download can-download" title="Download file."><a href="' . $feed_file->url() . '" download></a></i>';
		}
		else {
			$pre_data_row_html .= '<i class="fa fa-download" title="File not found."></i>';
		}
		

		$pre_data_row_html .= '</td>';

		return $pre_data_row_html;
	}
}