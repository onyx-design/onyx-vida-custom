<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;



class OVC_Table_walmart_products extends OVC_Table {

	public $field_set = 'walmart_product';
	public $ovcdt_set = 'walmart_product';

	public $header_overrides = array(
		'wa.ovc_id'		=> 'OVC ID [Walmart]',
		'wa.sku'		=> 'SKU [Walmart]',
		'wa.upc'		=> 'UPC [Walmart]',
		'pr.ID'			=> 'OVC ID [OVC]',
		'pr.sku'		=> 'SKU [OVC]',
		'pr.upc_code'	=> 'UPC [OVC]'
	);
	
	public $wait_to_save = false;
	public $advanced_controls = true;

	public function custom_advanced_controls() {

		return $this->load_template( 'walmart_show_duplicates' );
	}

	

	public function qry_scope__show_duplicate_skus() {

		// Make sure we have a WHERE statement initialize
		if( !strlen( $this->qry['where'] ) ) {
			$this->qry['where'] = " WHERE ";
		}
		else {
			$this->qry['where'] .= "AND "; 
		}

		$this->qry['where'] .= "wa.sku in ( SELECT sku FROM wp_ovc_walmart_products GROUP BY sku HAVING count(sku) > 1 ) ";
		$this->sorting = "wa.sku ASC";
		$this->qry['order'] = " ORDER BY {$this->sorting} ";

		$this->qry['sql'] = "{$this->qry['select']}{$this->qry['from']}{$this->qry['where']}{$this->qry['order']}LIMIT {$this->offset},{$this->per_page}";
	}

	public function qry_scope__show_duplicate_upcs() {

		// Make sure we have a WHERE statement initialize
		if( !strlen( $this->qry['where'] ) ) {
			$this->qry['where'] = " WHERE ";
		}
		else {
			$this->qry['where'] .= "AND "; 
		}

		$this->qry['where'] .= "wa.upc in ( SELECT upc FROM wp_ovc_walmart_products GROUP BY upc HAVING count(upc) > 1 ) ";
		$this->sorting = "wa.upc ASC";
		$this->qry['order'] = " ORDER BY {$this->sorting} ";

		$this->qry['sql'] = "{$this->qry['select']}{$this->qry['from']}{$this->qry['where']}{$this->qry['order']}LIMIT {$this->offset},{$this->per_page}";
	}

	public function qry_scope__show_duplicate_waids() {

		// Make sure we have a WHERE statement initialize
		if( !strlen( $this->qry['where'] ) ) {
			$this->qry['where'] = " WHERE ";
		}
		else {
			$this->qry['where'] .= "AND "; 
		}

		$this->qry['where'] .= "wa.walmart_id in ( SELECT walmart_id FROM wp_ovc_walmart_products GROUP BY walmart_id HAVING count(walmart_id) > 1 ) ";
		$this->sorting = "wa.walmart_id ASC";
		$this->qry['order'] = " ORDER BY {$this->sorting} ";

		$this->qry['sql'] = "{$this->qry['select']}{$this->qry['from']}{$this->qry['where']}{$this->qry['order']}LIMIT {$this->offset},{$this->per_page}";
	}

	public function qry_scope__show_duplicate_ovc_ids() {

		// Make sure we have a WHERE statement initialize
		if( !strlen( $this->qry['where'] ) ) {
			$this->qry['where'] = " WHERE ";
		}
		else {
			$this->qry['where'] .= "AND "; 
		}

		$this->qry['where'] .= "wa.ovc_id in ( SELECT ovc_id FROM wp_ovc_walmart_products GROUP BY ovc_id HAVING count(ovc_id) > 1 ) ";
		$this->sorting = "wa.ovc_id ASC";
		$this->qry['order'] = " ORDER BY {$this->sorting} ";

		$this->qry['sql'] = "{$this->qry['select']}{$this->qry['from']}{$this->qry['where']}{$this->qry['order']}LIMIT {$this->offset},{$this->per_page}";
	}
}