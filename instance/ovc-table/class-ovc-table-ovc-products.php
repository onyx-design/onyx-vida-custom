<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Table_ovc_products extends OVC_Table {

	public $field_set = 'ovc_products';
	public $ovcdt_set = 'ovc_products';

	public $pre_data_col = 'Actions'; // dev:generalize

	public $custom_selection = true;
	public $advanced_controls = true;
	public $live_data = false;




	// TABLE-SPECIFC AJAX ACTIONS
	public function save_productmeta( $requestData = array() ) {
		$data = $requestData['data'];

		if( !isset( $data['ovc_id'] ) || !is_numeric( $data['ovc_id'] ) ) {
			$this->results->error( 'Missing or Invalid OVC ID supplied with save_productmeta request.' );
			$this->results->main_msg = "Update Product Meta Data Failed.";
		}
		else {
			// Get OVC ID then remove it from Data to avoid errors
			$ovc_id = $data['ovc_id'];
			unset( $data['ovc_id'] );

			$ovcpr = OVCDB()->get_row( $this->field_set, $ovc_id, $this->results );

			$this->results->success = $ovcpr->update_data( $data, $this->results );
		}
		
		$this->results->main_msg = $this->results->success ? "Product Data Updated" : "Update Product Data Failed.";
		$this->request_status = $this->results->success ? 'success' : 'fail';
		$this->response_data['result_sets'][] = get_object_vars( $this->results );
		$this->response_data['force_reload'] = $this->results->success;
	}

	protected function pricing_helper_save( $requestData = array() ) {
		$price_helper_data = $requestData['data'];

		// Build updated prices array
		$new_prices = array();
		foreach( $price_helper_data as $key => $value ) {
			if( 0 === strpos( $key, 'save-' ) ) {
				$price_field = str_replace( 'save-', 'pr.', $key );
				$new_prices[ $price_field ] = $value;
			}
		}

		// Determine OVC IDs to update
		$ovc_ids = array( intval( $price_helper_data['ovc_id'] ) );
		if( $price_helper_data['update_cs_prices'] ) {
			$ovc_ids = array_unique( array_merge( $ovc_ids, $this->get_user_custom_selection() ) );
		}

		// Update Prices
		foreach( $ovc_ids as $ovc_id ) {
			$updated_product_row = $new_prices;
			$updated_product_row['pr.ID'] = $ovc_id;

			$ovc_product = OVCDB()->get_row( FS( 'ovc_products' ), $ovc_id, $this->results );

			if( $ovc_product->exists() ) {
				$ovc_product->update_data( $updated_product_row, $this->results );
			}
			else {
				$results->notice( "Skipping price tool update of OVC ID {$ovc_id} - ID does not exist!" );
			}

			if( $this->results->has_error ) {
				break;
			}
		}

		$this->results->success = !$this->results->has_error;

		if ( $this->results->success ) {
			$this->results->main_msg = "Prices saved successfully for " . count( $ovc_ids ) . " OVC IDs!";
		}
		else {
			$this->results->main_msg = "Error updating prices.";
		}

		$this->request_status = $this->results->success ? 'success' : 'fail';
		$this->response_data['result_sets'][] = get_object_vars( $this->results );

		$this->response_data['force_reload'] = true;
	}
}