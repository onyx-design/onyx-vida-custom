<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Table_ovc_images extends OVC_Table {

	public $field_set = 'ovc_images';
	public $ovcdt_set = 'ovc_images';
	
	public $pre_data_col = 'Actions';

	public $wait_to_save = false;
	
	public function get_pre_data_column_header_html() { // dev:generalize better!

		$pre_data_header_html = '';

		if( in_array( 'images', $this->field_groups ) ) {

			$pre_data_header_html .= '<th class="ovc_images-preview" data-col="ovc_images-preview">Preview</th>';

			$pre_data_header_html .= '<th class="ovc_images-3_4_preview" data-col="ovc_images-3_4_preview">3:4 Preview</th>';

			$pre_data_header_html .= '<th class="ovc_images-override_preview" data-col="ovc_images-override_preview">Override Preview</th>';
		}

		return $pre_data_header_html;
	}

	public function get_pre_data_column_filters_html() {

		$pre_data_filters_html = '';

		if( in_array( 'images', $this->field_groups ) ) {

			$pre_data_filters_html .= '<th></th>';

			$pre_data_filters_html .= '<th></th>';

			$pre_data_filters_html .= '<th></th>';
		}

		return $pre_data_filters_html;
	}

	public function get_pre_data_column_row_html( $row = array(), $header = false ) {

		if( $header ) {
			return $this->get_pre_data_column_header_html();
		}
		
		$ovc_row = OVCDB()->get_row( FS( 'ovc_images' ), $row['pic.ID'], $this->results );

		$pre_data_row_html = '';

		// Add 'Actions' column before table data
		if( in_array( 'images', $this->field_groups ) ) {

			// Show Image Preview
			$pre_data_row_html .= '<td class="ovc_images-preview">';

			$pre_data_row_html .= get_image_tag( $ovc_row->data( 'pic.post_id' ), '', '', 'center', 'shop_thumbnail' );

			$pre_data_row_html .= '</td>';

			// Show 3:4 Image Preview
			$pre_data_row_html .= '<td class="ovc_images-3_4_preview">';

			if( $image_3_4_path = $ovc_row->data( 'pic.image_3_4_path' ) ) {

				$pre_data_row_html .= '<img src="' . get_ovc_image_url( $image_3_4_path ) . '">';
			}

			$pre_data_row_html .= '</td>';

			// Show Image Override Preview
			$pre_data_row_html .= '<td class="ovc_images-override_preview">';

			if( $image_override_path = $ovc_row->data( 'pic.image_override_path' ) ) {

				$pre_data_row_html .= '<img src="' . get_ovc_image_url( $image_override_path ) . '">';
			}

			$pre_data_row_html .= '</td>';
		}

		return $pre_data_row_html;
	}

	// TABLE-SPECIFC AJAX ACTIONS
	public function upload_override_image( $requestData = array() ) {

		$data = $requestData['data'];
		$file = $data['files'];

		if( !isset( $data['ovc_id'] ) || !is_numeric( $data['ovc_id'] ) ) {

			$this->results->error( 'Missing or Invalid OVC ID supplied with upload_override_image request.' );
			$this->results->main_msg = "Upload Override Image Failed.";
		}
		else {
			// Get OVC ID then remove it from Data to avoid errors
			$ovc_id = $data['ovc_id'];
			unset( $data['ovc_id'] );

			$ovc_image = OVCDB()->get_row( $this->field_set, $ovc_id, $this->results );

			$this->results->success = $ovc_image->upload_override( $file, $this->results );
		}
		
		$this->results->main_msg = $this->results->success ? "Override Image Uploaded" : "Override Image Upload Failed.";
		$this->request_status = $this->results->success ? 'success' : 'fail';
		$this->response_data['result_sets'][] = get_object_vars( $this->results );
		$this->response_data['force_reload'] = $this->results->success;
	}
}