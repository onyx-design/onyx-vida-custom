<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Table_ovc_unattached_img_post_ids extends OVC_Table {

	public $field_set = 'ovc_unattached_img_post_ids';
	public $ovcdt_set = 'ovc_unattached_img_post_ids';
	
	public $pre_data_col = 'Actions';

	public $wait_to_save = false;

	public $advanced_controls = true;

	public function custom_advanced_controls() {

		return $this->load_template( 'show_in_use_imgs' );
	}
	
	public function get_pre_data_column_header_html() { // dev:generalize better!

		$pre_data_header_html = '<th class="ovc_unattached_img_post_ids-preview" data-col="ovc_unattached_img_post_ids-preview">Preview</th>';

		$pre_data_header_html .= '<th class="ovc_unattached_img_post_ids-filename" data-col="ovc_unattached_img_post_ids-filename">Filename</th>';

		return $pre_data_header_html;
	}

	public function get_pre_data_column_filters_html() {

		// Image Preview Filters
		$pre_data_filters_html = '<th></th>';

		// Image Filename Filters
		$pre_data_filters_html .= '<th></th>';

		return $pre_data_filters_html;
	}

	public function get_pre_data_column_row_html( $row = array(), $header = false ) {

		if( $header ) {
			return $this->get_pre_data_column_header_html();
		}

		$image_filename = basename( get_attached_file( $row['unimgs.post_id'] ) );

		// Show Image Preview
		$pre_data_row_html = '<td class="ovc_unattached_img_post_ids-preview">';

		$pre_data_row_html .= get_image_tag( $row['unimgs.post_id'], $image_filename, $image_filename, 'center', 'shop_single' );

		$pre_data_row_html .= '</td>';

		// Show Image Filename
		$pre_data_row_html .= '<td class="ovc_unattached_img_post_ids-filename">';

		$pre_data_row_html .= $image_filename;

		$pre_data_row_html .= '</td>';

		return $pre_data_row_html;
	}

	public function delete_image( $ID = 0 ) {

		$this->results->main_msg = "Unable to delete Image Row."; // This will be changed below if the request was successful

		if( $this->check_only ) {
			$this->results->notice( "Cannot delete rows while 'Check Only' mode is activated." );
		} else {

			$row = OVCDB()->get_row( $this->field_set, $ID, $this->results );

			if( false === wp_delete_attachment( $row->data( 'unimgs.post_id' ), true ) ) {

				$this->results->main_msg = "There was a problem deleting Image Post ID: {$row->data( 'unimgs.post_id' )}";
			} else {

				$this->results->success = $row->delete( $this->results );

				$this->results->main_msg = $this->results->success ? "Image Row ID: {$ID} Deleted" : "Error deleting Image Row ID: {$ID}";

				$this->request_status = $this->results->success ? 'success' : 'fail';

				if( $this->results->success ) {
					$this->response_data['force_reload'] = true;
				}
			}
		}
	}
}