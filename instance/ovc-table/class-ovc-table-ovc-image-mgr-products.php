<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Table_ovc_image_mgr_products extends OVC_Table {

	public $field_set = 'ovc_products';
	public $ovcdt_set = 'ovc_image_mgr_products';

	public $pre_data_col = 'Actions'; // dev:generalize

	public $live_data = false;

	public $pagination = false;
	public $per_page 	= 1000;

	public $show_filters = true;


	public function get_pre_data_column_header_html() { // dev:generalize better!
		return '<th class="actions">Actions</th><th class="freeze-pane" data-col="cs-toggle"></th>';
	}



	public function save_productimages( $requestData = array() ) {

		$img_set_request_data = $requestData['data'];
		$img_sets = $img_set_request_data['image_sets'];

		// Save parent images (via the target variation //dev:improve )
		$product = OVCDB()->get_row( FS( 'ovc_products' ), $img_set_request_data['targetID'], $this->results );

		$parent_img_data = array(
			'pa.img_main' 			=> $img_sets['parent']['img_main'],
			'pa.img_gallery_ids'	=> isset( $img_sets['parent']['img_gallery_ids'] ) ? implode( ',', $img_sets['parent']['img_gallery_ids'] ) : ''
		);

		$product->get_parent()->update_data( $parent_img_data, $this->results );

		// Remove image_set ids from specified OVC IDs
		if( isset( $img_set_request_data['ovc_ids_no_img_set'] ) ) {
			foreach( $img_set_request_data['ovc_ids_no_img_set'] as $ovc_id ) {

				$product = OVCDB()->get_row( FS( 'ovc_products' ), $ovc_id , $this->results );
				$product->update_field( 'pr.image_set', 0, $this->results );
			}
		}

		// Loop through image_sets
		foreach( $img_sets as $img_set_id => $img_set ) {

			// Skip parent and image sets that don't have any OVC IDs (they will be cleaned up later)
			if( 'parent' == $img_set_id || !isset( $img_set['ovc_ids'] ) || !count( $img_set['ovc_ids'] ) ) {
				continue;
			}

			
			$img_set_id = is_numeric( $img_set_id ) ? intval( $img_set_id ) : null;

			// Workaround for empty arrays being removed from JSON
			$img_gallery_ids = isset( $img_set['img_gallery_ids'] ) && is_array( $img_set['img_gallery_ids'] ) ? implode( ',', $img_set['img_gallery_ids'] ) : '';

			// Prepare updated img set data
			$updated_img_set_data = array(
				'img.img_main' 						=> $img_set['img_main'],
				'img.img_oms1'						=> $img_set['img_oms1'],
				'img.img_oms2'						=> $img_set['img_oms2'],
				'img.img_swatch'					=> $img_set['img_swatch'],
				'img.img_case'						=> $img_set['img_case'],
				'img.img_amazon'					=> $img_set['img_amazon'],
				'img.img_sizeguide'					=> $img_set['img_sizeguide'],
				'img.img_gallery_ids'				=> $img_gallery_ids,
				'img.img_marketplace_featured' 		=> $img_set['img_marketplace_featured'],
			);

			//$row_results = new OVC_Results();

			$image_set = OVCDB()->get_row( FS( 'image_sets' ), $img_set_id, $this->results );

			$image_set->update_data( $updated_img_set_data, $this->results );

			$img_set_id = $image_set->ID;
			

			// Update the image sets for the rest of the products
			foreach( $img_set['ovc_ids'] as $ovc_id ) {

				$product = OVCDB()->get_row( FS( 'ovc_products' ), $ovc_id, $this->results );
				$product->update_field( 'pr.image_set', $img_set_id, $this->results );
			}
		}


		$this->results->success = !$this->results->has_error;
		$this->results->main_msg = $this->results->success ? "Image Sets Saved Successfully" : "Error saving image sets.";

		$this->response_data['request_data'] = $requestData;

		$this->request_status = $this->results->success ? 'success' : 'fail';
		$this->response_data['result_sets'][] = get_object_vars( $this->results );
		$this->response_data['hello_there'] = 'General Kenobi';
		$this->response_data['force_reload'] = $this->results->success;
	}

	
}