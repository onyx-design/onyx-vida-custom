<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;



class OVC_Table_ovc_img_orphans extends OVC_Table {

	public $field_set = 'ovc_img_orphans';
	public $ovcdt_set = 'ovc_img_orphans';
	
	public $advanced_controls = true;

	public function custom_advanced_controls() {

		return $this->load_template( 'show_imgs' );
	}

	public function delete_img_orphan( $row_id ) {
		return;
		if( false !== wp_delete_attachment( $row_id, true ) ) {
			$this->results->success = true;	
		}
		else {
			$this->results->success = false;
		}		

		$this->results->main_msg = $this->results->success ? "Image Deleted" : "Image Deletion Failed.";
		$this->request_status = $this->results->success ? 'success' : 'fail';
		$this->response_data['result_sets'][] = get_object_vars( $this->results );
		$this->response_data['force_reload'] = $this->results->success;

	}


}