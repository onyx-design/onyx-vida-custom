<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

error_reporting(E_ALL); 
ini_set("log_errors", 1); 
ini_set("memory_limit", '512M' );
ini_set("max_execution_time", 300 );

class OVC_Table_data_fixes extends OVC_Table {

	public $field_set = 'data_fixes';
	public $ovcdt_set = 'data_fixes';
	
	public $pre_data_col = 'Actions';


	public function check_fields_to_fix( $ovc_id, $check_siblings = true ) {

		$check_result = OVCOP_data_scan::static_check_fields_to_fix( $ovc_id, $this->results );

		if( is_array( $check_result ) ){
			$this->results->success = true;
			$this->results->main_msg = ":/ There are still some fields to fix...";

			$this->merge_updated_data_row( $check_result['ID'], $check_result );
		}
		else if( 'deleted' == $check_result ) {
			$this->results->success = true;
			$this->results->main_msg = "Woo! All required fields are fixed for that one!";

			$this->response_data['force_reload'] = true;
		}
		else {
			$this->results->success = false;
			$this->results->main_msg = "Error checking fields to fix";
		}

		if( $check_siblings ) {
			$product = OVCDB()->get_row( FS( 'ovc_products' ), $ovc_id, $this->results );

			$parent = $product->get_parent();

			$ovc_ids = $parent->get_children( $produc->field_set, 'ID', $this->results, false );

			foreach( $ovc_ids as $ovc_id  ) {
				OVCOP_data_scan::static_check_fields_to_fix( $ovc_id );
			}

			$this->response_data['force_reload'] = true;
		}
	}


}