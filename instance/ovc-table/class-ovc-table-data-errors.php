<?php
 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


class OVC_Table_data_errors extends OVC_Table {

	public $field_set = 'data_errors';
	public $ovcdt_set = 'data_errors';
	
	public $pre_data_col = 'Actions';

	public function __construct( $request = '', $args = array(), OVC_Field $dt_meta ) {

		OVC_Row_data_errors::enable();
		OVC_Row_data_errors::load_active_errors();

		parent::__construct( $request, $args, $dt_meta );
	}

	public function resolve_error( $row_id ) {

		$row = OVCDB()->get_row( $this->field_set, $row_id, $this->results, false );

		if( $row->exists() ) {

			if ( !$row->active || $row->resolve_error( $this->results, true ) ) {

				$this->results->success = true;
				$this->results->main_msg = "Error resolved!";
				$this->merge_updated_data_row( $row->ID, $row->data_set( false, true ) );
			}
			else { 
				$this->results->success = false;
				$this->results->main_msg = "Unable to resolve error!";
			}
		}
		else {
			$this->results->success = false;
			$this->results->main_msg = "Error does not exist!";
			$this->response_data['force_reload'] = true;
		}

		$this->response_data['result_sets'][] = get_object_vars( $this->results );
	}
	

	public function check_error( $row_id ) {

		$data_error = OVCDB()->get_row( $this->field_set, $row_id, $this->results, false );

		if( $data_error->exists() ) {

			$data_error->check_error( $this->results );

			$error_status = $data_error->active ? 'Active' : 'Resolved!';

			$this->results->success = !$this->results->has_error;
			$this->results->main_msg = $this->results->success ? "Error ID {$row_id} status: {$error_status}" : "Check Error {$row_id} failed!";
			
		}
		else {
			$this->results->success = false;
			$this->results->main_msg = "Error does not exist!";
			$this->response_data['force_reload'] = true;
		}

		$this->response_data['result_sets'][] = get_object_vars( $this->results );	
	}
}