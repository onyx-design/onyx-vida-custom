<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OVC_Table_shopify_images extends OVC_Table {

	public $field_set = 'shopify_images';
	public $ovcdt_set = 'shopify_images';

	public $pre_data_col = 'Actions';

	public $custom_selection = true;
	public $advanced_controls = true;
	public $live_data = false;

}