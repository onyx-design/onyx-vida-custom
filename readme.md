# ONYX Vida Custom
> Adds various functionality, tweaks, and fixes for vida-us.com.

This readme is a list of steps for commonly requests tasks, such as creating a new operation

## OVC Operation Setup Steps

These steps don't necessarily need to be done in this order, but all the steps must be completed for the operation to work

Data required before creating an operation that will be used below
* {Operation Name}
* {operation-name}
	* kebab-case format of the Operation Name
	* lowercase
	* ex. amazon-export, analyze-images, data-scan
* {operation_name}
	* snake_case format of the Operation Name
	* lowercase
	* ex. amazon_export, analyze_images, data_scan
* {operation_order}
	* The max ovcop order number in OVC Schema
* {Operation Description}
	* Description of what the operation does, will be displays on the OVC Operations screen
* {Allowed Roles}
	* This is an array of allowed roles that the operation can be run from

1. Create a new class in `/onyx-vida-custom/instance/ovcop/` named `class-ovcop-{operation-name}`
	1. Class name should be in the format OVCOP_{operation_name}
	2. Needs to extend class `OVCOP`
	3. Must have a public string property called `$type` and have a value equal to '{operation_name}'
	4. Class constructor should accept `$request` and `$args` as parameters and always call `$this->child_construct( $request, $args )`
	* todo: Create OVCOP template class to be copied from
2. In the class OVCOP, add {operation_name} to the OVCOP::$valid_op_types array list
3. Add the operation to the OVC Schema by either creating a migration, or running the code below in `ovc-page-ovc-dev.php`

```
$ovc_schema = array(
	'ovcop'	=> array(
		'{operation_name}'	=> array(
			'order'			=> '{operation_order}',
			'nice_name'		=> '{Operation Name}',
			'descrip'		=> '{Operation Description}',
			'classname'		=> 'OVCOP_{operation_name}',
			'allowed_roles'	=> {Allowed Roles}
		)
	)
);

OVCSC::multi_update_field_meta( $ovc_schema );
```